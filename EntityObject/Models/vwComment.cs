using System;
using System.Collections.Generic;

namespace EntityObject.Models
{
    public partial class vwComment
    {
        public int CommentId { get; set; }
        public Nullable<int> PropertyId { get; set; }
        public string FHANumber { get; set; }
        public string Subject { get; set; }
        public string Comments { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public Nullable<int> OnBehalfOfBy { get; set; }
        public int LDI_ID { get; set; }
    }
}
