﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.ComponentModel.DataAnnotations;

namespace EntityObject.Entities.HCP_live
{
    [Table("Prod_FHANumberRequest")]
    public partial class Prod_FHANumberRequest
    {
        [Key]
        public Guid FHANumberRequestId { get; set; }
        public string FHANumber { get; set; }
        public string ProjectName { get; set; }
        public int ProjectTypeId { get; set; }
        public int CurrentLoanTypeId { get; set; }
        public int ActivityTypeId { get; set; }
        public int LoanTypeId { get; set; }
        public int BorrowerTypeId { get; set; }
        public bool? IsAddressCorrect { get; set; }
        public int PropertyAddressId { get; set; }
        public int CongressionalDtId { get; set; }
        public int LenderId { get; set; }
        public string LenderContactName { get; set; }
        public int LenderContactAddressId { get; set; }
        public decimal LoanAmount { get; set; }
        public int SkilledNursingBeds { get; set; }
        public int SkilledNursingUnits { get; set; }
        public int AssistedLivingBeds { get; set; }
        public int AssistedLivingUnits { get; set; }
        public int BoardCareBeds { get; set; }
        public int BoardCareUnits { get; set; }
        public int MemoryCareBeds { get; set; }
        public int MemoryCareUnits { get; set; }
        public int IndependentBeds { get; set; }
        public int IndependentUnits { get; set; }
        public int OtherBeds { get; set; }
        public int OtherUnits { get; set; }
        public bool? IsMidLargePortfolio { get; set; }
        public bool? IsMasterLeaseProposed { get; set; }
        public string Portfolio_Name { get; set; }
        public int Portfolio_Number { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int ConstructionStageTypeId { get; set; }
        public int CMSStarRating { get; set; }
        public decimal ProposedInterestRate { get; set; }
        public int IsNewOrExistOrNAPortfolio { get; set; }
        public bool IsPortfolioRequired { get; set; }
        public bool IsCreditReviewRequired { get; set; }
        public bool IsFhaInsertComplete { get; set; }
        public bool IsPortfolioComplete { get; set; }
        public bool IsCreditReviewAttachComplete { get; set; }
        public string InsertFHAComments { get; set; }
        public string PortfolioComments { get; set; }
        public string CreditreviewComments { get; set; }
        public DateTime? CreditReviewDate { get; set; }
        public bool IsReadyForApplication { get; set; }
        public bool RequestDeny { get; set; }
        public Guid TaskinstanceId { get; set; }
        public bool? IsLIHTC { get; set; }
        public int PropertyId { get; set; }
        public int RequestStatus { get; set; }
        public string CurrentFHANumber { get; set; }
        public DateTime? RequestSubmitDate { get; set; }
        public string DenyComments { get; set; }
        public string LenderContactEmail { get; set; }
        public string LenderContactPhone { get; set; }
        public string CancelComments { get; set; }
        public bool IsPortfolioCancelled { get; set; }
        public bool IsCreditCancelled { get; set; }
        public DateTime? AssignedDate { get; set; }

        //add naresh prodqueue phase2 19122019

        public bool opportunity_zone { get; set; }//naresh prod queue phase 2
        //add naresh





    }
}
