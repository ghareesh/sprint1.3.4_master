﻿namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    //using System.Data.Entity.Spatial;

    [Table("Fedralholidaylist")]
    public partial class Fedralholidaylist
    {
        [Key]
        public int Holiday_id { get; set; }
        public string FedralHoliday { get; set; }
        public DateTime Date { get; set; }


    }
}
