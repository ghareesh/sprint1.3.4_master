﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
   public partial  class usp_HCP_GetProjectDetailReport_PT_Result
    {


        public string FHANumber { get; set; }
        public string ProjectName { get; set; }
        public string ServiceName { get; set; }
        public string HUD_WorkLoad_Manager_Name { get; set; }
        public string HUD_Project_Manager_Name { get; set; }
        public Nullable<int> UnitsInFacility { get; set; }
        public string PeriodEnding { get; set; }
        public int MonthsInPeriod { get; set; }
        public Nullable<decimal> NOICumulative { get; set; }
        public string QuaterlyNoi { get; set; }
        public string NOIQuaterbyQuater { get; set; }
        public string NOIPercentageChange { get; set; }
        public Nullable<decimal> CumulativeRevenue { get; set; }
        public Nullable<decimal> CumulativeExpenses { get; set; }
        public string QuaterlyOperatingRevenue { get; set; }
        public Nullable<decimal> QuarterlyOperatingExpense { get; set; }
        public string RevenueQuarterbyQuarter { get; set; }
        public string RevenuePercentageChange { get; set; }
        public string DSCRDifferencePerQuarter { get; set; }
        public string DSCRPercentageChange { get; set; }
        public Nullable<int> CumulativeResidentDays { get; set; }
        public Nullable<int> QuarterlyResidentDays { get; set; }
        public string ADRperQuarter { get; set; }
        public string ADRDifferencePerQuarter { get; set; }
        public string ADRPercentageChange { get; set; }
        public Nullable<decimal> DebtCoverageRatio2 { get; set; }
        public Nullable<decimal> AverageDailyRateRatio { get; set; }
        public string QuarterlyDSCR { get; set; }
        public Nullable<int> IsProjectError { get; set; }
        public Nullable<int> IsQtrlyNOIPTError { get; set; }
        public Nullable<int> IsQtrlyDSCRPTError { get; set; }

        public Nullable<int> IsQtrlyORevError { get; set; }
        public Nullable<int> IsQtrlyNOIError { get; set; }
        public Nullable<int> IsQtrlyDSCRError { get; set; }
        public Nullable<int> IsADRError { get; set; }
    }
}
