//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EntityObject.Entities.HCP_live
{
    using System;
    
    public partial class usp_HCP_GetLenderUsersByAeUserId_Result
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PreferredTimeZone { get; set; }
        public string RoleName { get; set; }
        public int LenderID { get; set; }
        public string Lender_Name { get; set; }
        public Nullable<int> ServicerID { get; set; }
        public int UserID { get; set; }
    }
}
