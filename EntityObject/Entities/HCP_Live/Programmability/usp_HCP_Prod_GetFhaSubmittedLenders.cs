﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live.Programmability
{
   public partial class usp_HCP_Prod_GetFhaSubmittedLenders
    {
        public int LenderID { get; set; }
        public string Lender_Name { get; set; }
    }
}
