﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live.Programmability
{
    public class usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result
    {
        public string actionTaken { get; set; }
        public string name { get; set; }
        public string userRole { get; set; }
        public string comment { get; set; }
        public string fileName { get; set; }
        public double? fileSize { get; set; }
        public DateTime? uploadDate { get; set; }
        public DateTime? submitDate { get; set; }
        public int? fileId { get; set; }
        public int? childFileId { get; set; }
        public bool editable { get; set; }
        public DateTime actionDate { get; set; }
        public string nfrComment { get; set; }
        public bool isFileHistory { get; set; }
        public bool isFileComment { get; set; }
        public bool isApprove { get; set; }
        public string childFileName { get; set; }
        // harish added doctypeid 03042020
        public string DocTypeID { get; set; }
    }
}
