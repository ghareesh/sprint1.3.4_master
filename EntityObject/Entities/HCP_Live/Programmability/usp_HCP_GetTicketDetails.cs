﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace EntityObject.Entities.HCP_live.Programmability
{
    public partial class usp_HCP_GetTicketDetails_Latest
    {
        [Key]
        public int TICKET_ID { get; set; }
        public string STATUS { get; set; }//open or closed etc
        //[StringLength(255)]//title
        public string ISSUE { get; set; }//MAPS TO TITLE ON THE UI
        //[Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }
        public int? CATEGORY { get; set; }//type of issue

        public int? SUBCATEGORY { get; set; }//type of issue

        public int? PRIORITY { get; set; }
        //[Column(TypeName = "text")]
        public string RECREATION_STEPS { get; set; }
        //[Column(TypeName = "text")]
        public string CONVERSATION { get; set; }
        public int? CREATED_BY { get; set; }
        public DateTime CREATED_ON { get; set; }
        public int? ASSIGNED_TO { get; set; }
        public string ASSIGNED_TO_USERNAME { get; set; }//userid, email
        public DateTime? ASSIGNED_DATE { get; set; }
        public int? LAST_MODIFIED_BY { get; set; }
        public DateTime? LAST_MODIFIED_ON { get; set; }
        public int? ONACTION { get; set; }//hdnew
        public string ROLENAME { get; set; }

        public int Numberofdays { get; set; }

        public int Numberofweeks { get; set; }
        //public int UserID { get; set; }
        //Added by siddesh 15102019//
        public string Helpdesk_name { get; set; }
        //Added by siddesh 15102019//
        [XmlIgnore]
        public IList<SelectListItem> TicketHelpdesknameList { get; set; }
        //Added by siddesh 15102019//
        public usp_HCP_GetTicketDetails_Latest()
        {
            TicketHelpdesknameList = new List<SelectListItem>();
        }
    }
}
