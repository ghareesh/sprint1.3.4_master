﻿namespace EntityObject.Entities.HCP_live
{
    using System;
    public partial class usp_HCP_GetPAMReportDetail_Result
    {
        public string FHANumber { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string HudProjectManagerName { get; set; }

        public string HUDPM { get; set; }

        public string HudWorkloadManagerName { get; set; }
        public string ProjectActionName { get; set; }
        public string Comment { get; set; }
        public DateTime? ProjectActionSubmitDate { get; set; }
        public DateTime? ProjectActionCompletionDate { get; set; }
        public int? ProjectActionDaysToComplete { get; set; }
        public bool IsProjectActionOpen { get; set; }
        public Decimal? RequestedAmount { get; set; }
        public Decimal? ApprovedAmount { get; set; }
        public string LenderUserName { get; set; }
        public string LenderEmail { get; set; }
        public Guid? Id { get; set; }
        public string ReAssignedTo { get; set; }
        public string ReAssignedFrom { get; set; }
        public string ReAssignedBy { get; set; }
        public DateTime? ReAssignedDate { get; set; }
        public string RequestStatus { get; set; }
        public string ProcessedBy { get; set; }
        public int? AdditionalInfoCount { get; set; }
        public string UserRoleName { get; set; } // User Story 1904
        public string RAI { get; set; }
    }
}
