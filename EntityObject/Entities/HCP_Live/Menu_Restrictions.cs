﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Menu_Restrictions")]
    public partial class Menu_Restrictions
    {
        [Key]
        public int Restriction_Id { get; set; }
        public string Menu_Name { get; set; }
        public int Lender_Id { get; set; }

    }
}

