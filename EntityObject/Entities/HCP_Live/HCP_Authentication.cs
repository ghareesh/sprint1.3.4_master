namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HCP_Authentication
    {
        public HCP_Authentication()
        {
            HCP_User_FunctionPoint = new HashSet<HCP_User_FunctionPoint>();
            Lender_DataUpload_Live = new HashSet<Lender_DataUpload_Live>();
            User_Lender = new HashSet<User_Lender>();
            User_SecurityQuestion = new HashSet<User_SecurityQuestion>();
        }

        [Key]
        public int UserID { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string UserMInitial { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public int? AddressID { get; set; }

        public bool? Deleted_Ind { get; set; }

        [Required]
        [StringLength(50)]
        public string PreferredTimeZone { get; set; }

        public string PasswordHist { get; set; }

        public bool? IsRegisterComplete { get; set; }
        public int? TitleId { get; set; }

        public virtual Address Address { get; set; }

        public virtual ICollection<HCP_User_FunctionPoint> HCP_User_FunctionPoint { get; set; }

        public virtual ICollection<Lender_DataUpload_Live> Lender_DataUpload_Live { get; set; }

        public virtual ICollection<User_Lender> User_Lender { get; set; }

        public virtual ICollection<User_SecurityQuestion> User_SecurityQuestion { get; set; }

        public virtual User_WLM_PM User_WLM_PM { get; set; }
    }
}
