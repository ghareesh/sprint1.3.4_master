﻿//HDNOTIFICATIONS

namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    //hd_ticket_notifications// [Table("HD_TICKET_NOTIFICATIONS")]

    [Table("HD_TICKET_NOTIFICATIONS")]
    public partial class HD_TICKET_NOTIFICATIONS
    {
        [Key]
        public int NOTIFICATIONID { get; set; }
        public int TICKET_ID { get; set; }      
        public int TARGETUSERID { get; set; }
        public string TITLE { get; set; }//MAPS TO TITLE ON THE UI
        public string MESSAGE { get; set; }
        public bool ISREAD { get; set; }
        public DateTime LOGDATE { get; set; }
    }
}
