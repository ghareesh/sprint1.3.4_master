namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PropertyInfo_iREMS
    {
        public PropertyInfo_iREMS()
        {
            ProjectInfoes = new HashSet<ProjectInfo>();
            PropertyID_FHANumber_iREMS = new HashSet<PropertyID_FHANumber_iREMS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PropertyID { get; set; }

        [Required]
        [StringLength(100)]
        public string PropertyName { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public virtual ICollection<ProjectInfo> ProjectInfoes { get; set; }

        public virtual ICollection<PropertyID_FHANumber_iREMS> PropertyID_FHANumber_iREMS { get; set; }
    }
}
