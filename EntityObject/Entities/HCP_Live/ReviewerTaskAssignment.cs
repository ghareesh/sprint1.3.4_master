﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityObject.Entities.HCP_live
{


    [Table("ReviewerTaskAssignment")]

   public partial class ReviewerTaskAssignment
   {
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public int ReviewerTaskAssignmentId { get; set;}
       public Guid TaskInstanceId { get; set; }
       public string FHANumber { get; set; }
       public int ReviwerUserId { get; set; }
       public int Status { get; set; }
       public DateTime ModifiedOn { get; set; }
       public int ModifiedBy { get; set; }



   }
}
