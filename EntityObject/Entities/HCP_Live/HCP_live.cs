namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Core;

    public partial class HCP_live : DbContext
    {
        public HCP_live()
            //: base("HCP_Live")//karrinew;commented hardcoded string
            : base(WebUiConstants.LiveConnectionName)
        {
        }
        public virtual DbSet<Fedralholidaylist> Fedralholidaylist { get; set; } //siddu #825 @10042020
        public virtual DbSet<HD_TICKET> HD_TICKET { get; set; }//karri:#149
        public virtual DbSet<HD_TICKET_NOTIFICATIONS> HD_TICKET_NOTIFICATIONS { get; set; }//HDNOTIFICATIONS

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<FHAInfo_iREMS> FHAInfo_iREMS { get; set; }
        public virtual DbSet<HCP_Attornies> HCP_Attornies { get; set; }
        public virtual DbSet<HCP_Authentication> HCP_Authentication { get; set; }
        public virtual DbSet<HCP_Email> HCP_Email { get; set; }
        public virtual DbSet<HCP_EmailType> HCP_EmailType { get; set; }
        public virtual DbSet<HCP_FunctionPoint_Ref> HCP_FunctionPoint_Ref { get; set; }
        public virtual DbSet<HCP_Project_Action> HCP_Project_Action { get; set; }
        public virtual DbSet<HCP_Sections> HCP_Sections { get; set; }
        public virtual DbSet<HCP_User_FunctionPoint> HCP_User_FunctionPoint { get; set; }
        public virtual DbSet<HUD_Project_Manager> HUD_Project_Manager { get; set; }
        public virtual DbSet<HUD_Project_Manager_Portfolio> HUD_Project_Manager_Portfolio { get; set; }
        public virtual DbSet<HUD_WorkLoad_Manager> HUD_WorkLoad_Manager { get; set; }
        public virtual DbSet<HUDAccountExecutiveTitle> HUDAccountExecutiveTitles { get; set; }
        public virtual DbSet<Lender_DataUpload_Live> Lender_DataUpload_Live { get; set; }
        public virtual DbSet<Lender_FHANumber> Lender_FHANumber { get; set; }
        public virtual DbSet<LenderFiscalYearDetails> LenderFiscalYearDetails { get; set; }
        public virtual DbSet<LenderInfo> LenderInfoes { get; set; }
        public virtual DbSet<ProjectInfo> ProjectInfoes { get; set; }
        public virtual DbSet<PropertyID_FHANumber_iREMS> PropertyID_FHANumber_iREMS { get; set; }
        public virtual DbSet<PropertyInfo_iREMS> PropertyInfo_iREMS { get; set; }
        public virtual DbSet<SecurityQuestion> SecurityQuestions { get; set; }
        public virtual DbSet<ServicerInfo> ServicerInfoes { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<User_Lender> User_Lender { get; set; }
        public virtual DbSet<User_SecurityQuestion> User_SecurityQuestion { get; set; }
        public virtual DbSet<User_WLM_PM> User_WLM_PM { get; set; }
        public virtual DbSet<webpages_Membership> webpages_Membership { get; set; }
        public virtual DbSet<webpages_OAuthMembership> webpages_OAuthMembership { get; set; }
        public virtual DbSet<webpages_Roles> webpages_Roles { get; set; }
        public virtual DbSet<webpages_UsersInRoles> webpages_UsersInRoles { get; set; }
        public virtual DbSet<TempReqUser> TempReqUsers { get; set; }
        public virtual DbSet<NonCriticalRepairsRequest> NonCriticalRepairsRequest { get; set; }
        public virtual DbSet<HUD_WorkLoad_Manager_Portfolio_Manager> HUD_WorkLoad_Manager_Portfolio_Manager { get; set; }
        public virtual DbSet<HUD_WorkloadManager_InternalSpecialOptionUser> HUD_WorkloadManager_InternalSpecialOptionUser { get; set; }
        //public virtual DbSet<ProjectSummary> ProjectSummary { get; set; }
        public virtual DbSet<Reserve_For_Replacement_Form_Data> Reserve_For_Replacement_Form_Data { get; set; }
        // public virtual DbSet<Disclaimer_CertifyLanguage> Disclaimer_CertifyLanguage { get; set; }
        public virtual DbSet<HCP_EmailTemplateMessages> HCP_EmailTemplateMessages { get; set; }
        public virtual DbSet<NonCritical_ProjectInfo> NonCritical_ProjectInfo { get; set; }
        public virtual DbSet<NonCriticalLateAlerts> NonCriticalLateAlerts { get; set; }
        public virtual DbSet<NonCriticalLateTimeSpan> NonCriticalLateTimeSpan { get; set; }
        public virtual DbSet<NonCriticalRequestExtension> NonCriticalRequestExtension { get; set; }
        public virtual DbSet<Transaction> Transaction { get; set; }
        public virtual DbSet<NonCriticalRulesChecklist> NonCriticalRulesChecklist { get; set; }
        public virtual DbSet<NonCriticalReferReason> NonCriticalReferReason { get; set; }
        public virtual DbSet<CheckListQuestion> CheckListQuestion { get; set; }
        public virtual DbSet<ProjectActionForm> ProjectActionForm { get; set; }
        public virtual DbSet<RequestAdditionalInfo> RequestAdditionalInfo { get; set; }
        public virtual DbSet<PamProjectActionType> PamProjectActionType { get; set; }
        public virtual DbSet<ProjectActionAEFormStatus> ProjectActionAEFormStatus { get; set; }
        public virtual DbSet<AdditionalInformation> AdditionalInformation { get; set; }
        public virtual DbSet<OpaForm> OpaForm { get; set; }
        public virtual DbSet<HCP_User_LoginTime> HCP_User_LoginTime { get; set; }
        public virtual DbSet<ReviewerTitles> ReviewerTitles { get; set; }
        public virtual DbSet<ReviewerTaskAssignment> ReviewerTaskAssignment { get; set; }
        public virtual DbSet<HCP_PageType> HCP_PageType { get; set; }
        public virtual DbSet<HCP_DisclaimerText> HCP_DisclaimerText { get; set; }
        public virtual DbSet<PageType_DisclaimerText> PageType_DisclaimerText { get; set; }
        public virtual DbSet<Prod_FHANumberRequest> Prod_FHANumberRequest { get; set; }
        public virtual DbSet<Prod_BorrowerType> Prod_BorrowerType { get; set; }
        public virtual DbSet<Prod_ActivityType> Prod_ActivityType { get; set; }
        public virtual DbSet<Prod_CongressionalDistrict> Prod_CongressionalDistrict { get; set; }
        public virtual DbSet<Prod_ProjectType> Prod_ProjectType { get; set; }
        public virtual DbSet<Prod_LoanType> Prod_LoanType { get; set; }
        public virtual DbSet<Prod_AdditionalFinancingResources> Prod_AdditionalFinancingResources { get; set; }
        public virtual DbSet<Prod_SelectedAdditionalFinancingResources> Prod_SelectedAdditionalFinancingResources { get; set; }
        public virtual DbSet<Prod_CMSStarRating> Prod_CMSStarRating { get; set; }
        public virtual DbSet<Prod_NextStage> Prod_NextStages { get; set; }
        public virtual DbSet<Menu_Restrictions> Menu_Restrictions { get; set; }
        public virtual DbSet<PDRColumn> PdrColumns { get; set; }
        public virtual DbSet<PDRUserPreference> PdrUserPreferences { get; set; }
        public virtual DbSet<PamStatus> PamStatus { get; set; }
        public virtual DbSet<Prod_SharepointScreen> ProdSSharepointScreen { get; set; }
        public virtual DbSet<Prod_SharePointAccountExecutives> ProdSSharePointAccountExecutives { get; set; }
        public virtual DbSet<InternalExternalTask> InternalExternalTasks { get; set; }
        public virtual DbSet<HUD_PamReportFilters> HUD_PamReportFilters { get; set; }

		public virtual DbSet<HUDSignature> HUDSignature { get; set; }
        public virtual DbSet<Prod_LoanCommittee> Prod_LoanCommittee { get; set; }

        public virtual DbSet<HUD_ASSETPamReportFilters> HUD_ASSETPamReportFilters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Menu_Restrictions>()
                .HasKey(e => e.Restriction_Id);
            modelBuilder.Entity<Prod_FHANumberRequest>()
                .HasKey(e => e.FHANumberRequestId);
            modelBuilder.Entity<Prod_ActivityType>()
           .HasKey(e => e.ActivityTypeId);
            modelBuilder.Entity<Prod_CongressionalDistrict>()
             .HasKey(e => e.CongressionalDTId);
            modelBuilder.Entity<Prod_BorrowerType>()
               .HasKey(e => e.BorrowerTypeId);
            modelBuilder.Entity<Prod_ProjectType>()
              .HasKey(e => e.ProjectTypeId);
            modelBuilder.Entity<Prod_LoanType>()
            .HasKey(e => e.LoanTypeId);
            modelBuilder.Entity<Prod_AdditionalFinancingResources>()
              .HasKey(e => e.ResourceId);
            modelBuilder.Entity<Prod_SelectedAdditionalFinancingResources>()
              .HasKey(e => e.Id);
            modelBuilder.Entity<Prod_CMSStarRating>()
                .HasKey(e => e.RatingId);

            //karri:#149
            modelBuilder.Entity<HD_TICKET>()
               .HasKey(e => e.TICKET_ID);

             //HDNOTIFICATIONS
            modelBuilder.Entity<HD_TICKET_NOTIFICATIONS>()
               .HasKey(e => e.NOTIFICATIONID);

            modelBuilder.Entity<HCP_PageType>()
                .HasKey(e => e.PageTypeId);

            modelBuilder.Entity<HCP_DisclaimerText>()
                .HasKey(e => e.DisclaimerTextId);

            modelBuilder.Entity<PageType_DisclaimerText>()
                .HasKey(e => e.PageTypeId)
                .HasKey(e => e.DisclaimerTextId);



            modelBuilder.Entity<FHAInfo_iREMS>()
                .HasMany(e => e.HUD_Project_Manager_Portfolio)
                .WithOptional(e => e.FHAInfo_iREMS)
                .HasForeignKey(e => e.ProjectID_FHANumber);

            modelBuilder.Entity<FHAInfo_iREMS>()
                .HasMany(e => e.Lender_DataUpload_Live)
                .WithRequired(e => e.FHAInfo_iREMS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FHAInfo_iREMS>()
                .HasMany(e => e.PropertyID_FHANumber_iREMS)
                .WithRequired(e => e.FHAInfo_iREMS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HCP_Attornies>()
                .Property(e => e.AttorneyName)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_Attornies>()
                .Property(e => e.AttorneyTitle)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_Attornies>()
                .Property(e => e.Office)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_Attornies>()
                .Property(e => e.Region)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_Authentication>()
                .Property(e => e.UserMInitial)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_Authentication>()
                .HasMany(e => e.HCP_User_FunctionPoint)
                .WithRequired(e => e.HCP_Authentication)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HCP_Authentication>()
                .HasMany(e => e.Lender_DataUpload_Live)
                .WithRequired(e => e.HCP_Authentication)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HCP_Authentication>()
                .HasMany(e => e.User_Lender)
                .WithRequired(e => e.HCP_Authentication)
                .HasForeignKey(e => e.User_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HCP_Authentication>()
                .HasMany(e => e.User_SecurityQuestion)
                .WithRequired(e => e.HCP_Authentication)
                .HasForeignKey(e => e.User_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HCP_Authentication>()
                .HasOptional(e => e.User_WLM_PM)
                .WithRequired(e => e.HCP_Authentication);

            modelBuilder.Entity<HCP_EmailType>()
                .Property(e => e.EmailTypeDescription)
                .IsFixedLength();

            modelBuilder.Entity<HCP_FunctionPoint_Ref>()
                .Property(e => e.HCP_FunctionPoint_Name)
                .IsFixedLength();

            modelBuilder.Entity<HCP_FunctionPoint_Ref>()
                .Property(e => e.HCP_FunctionPoint_Desc)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_FunctionPoint_Ref>()
                .HasMany(e => e.HCP_User_FunctionPoint)
                .WithRequired(e => e.HCP_FunctionPoint_Ref)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HCP_Project_Action>()
                .Property(e => e.ProjectActionName)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_Project_Action>()
                .Property(e => e.ProjectActionLongName)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_Sections>()
                .Property(e => e.SectionName)
                .IsUnicode(false);

            modelBuilder.Entity<HCP_User_FunctionPoint>()
                .Property(e => e.ModifiedOn)
                .IsFixedLength();

            modelBuilder.Entity<HUD_Project_Manager>()
                .HasMany(e => e.HUD_WorkLoad_Manager)
                .WithMany(e => e.HUD_Project_Manager)
                .Map(m => m.ToTable("HUD_WorkLoad_Manager_Portfolio_Manager1").MapLeftKey("HUD_Project_Manager_ID").MapRightKey("HUD_WorkLoad_Manager_ID"));

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.OperatingCash)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.Investments)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.ReserveForReplacementEscrowBalance)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.AccountsReceivable)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.CurrentAssets)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.CurrentLiabilities)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.TotalRevenues)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.RentLeaseExpense)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.DepreciationExpense)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.AmortizationExpense)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.TotalExpenses)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.NetIncome)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.ReserveForReplacementDeposit)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.FHAInsuredPrincipalPayment)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.FHAInsuredInterestPayment)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.MortgageInsurancePremium)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.ReserveForReplacementBalancePerUnit)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.WorkingCapital)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.DebtCoverageRatio)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.DaysCashOnHand)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.DaysInAcctReceivable)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.AvgPaymentPeriod)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.WorkingCapitalScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.DebtCoverageRatioScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.DaysCashOnHandScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.DaysInAcctReceivableScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.AvgPaymentPeriodScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Live>()
                .Property(e => e.ScoreTotal)
                .HasPrecision(19, 2);

            modelBuilder.Entity<LenderInfo>()
                .HasMany(e => e.Lender_DataUpload_Live)
                .WithRequired(e => e.LenderInfo)
                .HasForeignKey(e => e.LenderID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LenderInfo>()
                .HasMany(e => e.Lender_DataUpload_Live1)
                .WithRequired(e => e.LenderInfo1)
                .HasForeignKey(e => e.LenderID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LenderInfo>()
                .HasMany(e => e.User_Lender)
                .WithOptional(e => e.LenderInfo)
                .HasForeignKey(e => e.Lender_ID);

            modelBuilder.Entity<ProjectInfo>()
                .Property(e => e.Original_Loan_Amount)
                .HasPrecision(19, 2);

            modelBuilder.Entity<ProjectInfo>()
                .Property(e => e.Original_Interest_Rate)
                .HasPrecision(4, 2);

            modelBuilder.Entity<ProjectInfo>()
                .Property(e => e.Amortized_Unpaid_Principal_Bal)
                .HasPrecision(19, 2);

            modelBuilder.Entity<ProjectInfo>()
                .Property(e => e.Owner_Organization_Name)
                .IsUnicode(false);

            modelBuilder.Entity<PropertyInfo_iREMS>()
                .HasMany(e => e.ProjectInfoes)
                .WithRequired(e => e.PropertyInfo_iREMS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PropertyInfo_iREMS>()
                .HasMany(e => e.PropertyID_FHANumber_iREMS)
                .WithRequired(e => e.PropertyInfo_iREMS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SecurityQuestion>()
                .HasMany(e => e.User_SecurityQuestion)
                .WithRequired(e => e.SecurityQuestion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServicerInfo>()
                .HasMany(e => e.Lender_DataUpload_Live)
                .WithOptional(e => e.ServicerInfo)
                .HasForeignKey(e => e.Servicer_ID);

            modelBuilder.Entity<webpages_Roles>()
                .HasMany(e => e.webpages_UsersInRoles)
                .WithRequired(e => e.webpages_Roles)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TempReqUser>()
                .Property(e => e.UserMInitial)
                .IsUnicode(false);

            modelBuilder.Entity<NonCriticalRepairsRequest>()
                .HasKey(e => e.NonCriticalRepairsRequestID)
                // .HasMany(e=>e.NonCriticalReferReasons)
                ;

            modelBuilder.Entity<Reserve_For_Replacement_Form_Data>()
                .Property(e => e.ServicerRemarks)
                .IsUnicode(false);

            modelBuilder.Entity<HUD_WorkLoad_Manager_Portfolio_Manager>()
                .Property(x => x.HUD_WorkLoad_Manager_ID);

            modelBuilder.Entity<HUD_WorkloadManager_InternalSpecialOptionUser>()
                .Property(x => x.HUD_WorkloadManagerId);

            modelBuilder.Entity<HCP_EmailTemplateMessages>()
                .HasKey(e => e.MessageTemplateId);

            //  modelBuilder.Entity<Disclaimer_CertifyLanguage>()
            //      .HasKey(e => e.MessageID);

            modelBuilder.Entity<NonCritical_ProjectInfo>()
                .HasKey(e => e.PropertyID)
                .HasKey(e => e.LenderID)
                .HasKey(e => e.FHANumber);

            modelBuilder.Entity<NonCriticalLateTimeSpan>()
                .HasKey(e => e.TimeSpanId);

            modelBuilder.Entity<NonCriticalRequestExtension>()
                .HasKey(e => e.NonCriticalRequestExtensionId);

            modelBuilder.Entity<NonCriticalLateAlerts>()
                .HasKey(e => e.NonCriticalLateAlertId);

            modelBuilder.Entity<NonCriticalRulesChecklist>()
                .HasKey(e => e.RuleId);


            modelBuilder.Entity<NonCriticalReferReason>()
                 .HasKey(e => e.ReferId);

            modelBuilder.Entity<Transaction>()
          .HasKey(e => e.TransactionId);

            modelBuilder.Entity<CheckListQuestion>()
                .HasKey(e => e.CheckListId);
            modelBuilder.Entity<ProjectActionForm>()
                .HasKey(e => e.ProjectActionFormId);
            modelBuilder.Entity<RequestAdditionalInfo>()
                .HasKey(e => e.RequestAdditionalInfoId);
            modelBuilder.Entity<PamProjectActionType>()
               .HasKey(e => e.PamProjectActionId);
            modelBuilder.Entity<ProjectActionAEFormStatus>()
               .HasKey(e => e.ProjectActionAEFormStatusId);
            modelBuilder.Entity<AdditionalInformation>()
                .HasKey(e => e.AdditionalInformationId);
            modelBuilder.Entity<OpaForm>()
                .HasKey(e => e.ProjectActionFormId);
            modelBuilder.Entity<HCP_PageType>().HasKey(e => e.PageTypeId);
            modelBuilder.Entity<PDRColumn>()
                .HasKey(e => e.PDRColumnID);
            modelBuilder.Entity<PDRUserPreference>()
                .HasKey(e => e.PDRUserPreferenceID);
            modelBuilder.Entity<PamStatus>()
               .HasKey(e => e.PamStatusId);
            modelBuilder.Entity<Prod_SharepointScreen>()
                .HasKey(e => e.ScreenId);
            modelBuilder.Entity<Prod_SharePointAccountExecutives>()
                .HasKey(e => e.SharePointAEId);
            modelBuilder.Entity<InternalExternalTask>()
                .HasKey(e =>e.InternalExternalTaskID);
            modelBuilder.Entity<HUD_PamReportFilters>()
                .HasKey(e => e.FilterID);
            modelBuilder.Entity<HUDSignature>()
			   .HasKey(e => e.Id);
            modelBuilder.Entity<Prod_LoanCommittee>()
                .HasKey(e => e.LoanCommitteeId);


        }
    }
}
