namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HUDAccountExecutiveTitle")]
    public partial class HUDAccountExecutiveTitle
    {
        [Key]
        public int AccountExecutiveTitleID { get; set; }

        [Required]
        [StringLength(100)]
        public string AccountExecutiveTitle { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }
    }
}
