﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Prod_LoanType")]
    public class Prod_LoanType
    {
        [Key]
        public int LoanTypeId { get; set;}
        public string LoanTypeName { get; set; }

    }
}
