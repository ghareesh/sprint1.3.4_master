﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityObject.Entities.HCP_live
{
  [Table("PDRColumn")]
  public partial  class PDRColumn
    {   
        [Key]
        public int  PDRColumnID { get; set; }

        public string PDRColumnName { get; set; }
    }
}
