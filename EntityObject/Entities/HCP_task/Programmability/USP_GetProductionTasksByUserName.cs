﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task.Programmability
{
   public partial class USP_GetProductionTasksByUserName
    {
        public Guid FhaRequestInstanceId { get; set; }
        public Guid ParentChildInstanceId { get; set; }
        public int FhaRequestType { get; set; }
        public string TaskName { get; set; }
        public string AssignedTo { get; set; }
        public DateTime LastUpdated { get; set; }
        public string LenderName { get; set; }
        public string ProductionType { get; set; }
        public string Comments { get; set; }
        public int Duration { get; set; }
        public string Status { get; set; }
        public string ProjectName { get; set; }
        public string ProductionTaskType { get; set; }
        public int StatusId { get; set; }
        public Guid? groupid { get; set; }
        public int? orderby { get; set; }
        public string IsReadyForApplication { get; set; }
    }
}
