﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task.Programmability
{
   public class EmailRecipient_Result
    {
        public string Fullname { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string LenderEmail { get; set; }
    }
}
