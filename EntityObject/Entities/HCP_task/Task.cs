namespace EntityObject.Entities.HCP_task
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Task")]
    public partial class Task
    {
        public int TaskId { get; set; }

        public Guid TaskInstanceId { get; set; }

        public int SequenceId { get; set; }

        [Required]
        [StringLength(150)]
        public string AssignedBy { get; set; }

        //[Required]
        //[StringLength(150)]
        public string AssignedTo { get; set; }

        public DateTime? DueDate { get; set; }

        public DateTime StartTime { get; set; }

        [StringLength(1000)]
        public string Notes { get; set; }

        public int TaskStepId { get; set; }

        public int? DataKey1 { get; set; }

        public int? DataKey2 { get; set; }

        [Column(TypeName = "xml")]
        public string DataStore1 { get; set; }

        [Column(TypeName = "xml")]
        public string DataStore2 { get; set; }

        [Column(TypeName = "xml")]
        public string TaskOpenStatus { get; set; }

        public virtual TaskStep TaskStep { get; set; }

        public bool? IsReAssigned { get; set;}

        public int? PageTypeId { get; set; }

        public string FhaNumber { get; set; }
    }
}
