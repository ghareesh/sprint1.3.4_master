﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    
    [Table("AM_R4RAndNCREDocument")]
    public partial class AM_R4RAndNCREDocument
    {
        [Key]
        public int DocumentId { get; set; }
        [Required]
        public string FileName { get; set; }
        public string ActionName { get; set; }
        public int TransactionDocumentId { get; set; }
        public string DocumentDescription { get; set; }


    }
}
