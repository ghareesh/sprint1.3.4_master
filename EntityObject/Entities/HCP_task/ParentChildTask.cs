﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("ParentChildTask")]
    public class ParentChildTask
    {
        [Key]
        public Guid ParentChildTaskId { get; set;}
        public Guid ChildTaskInstanceId { get; set; }
        public Guid ParentTaskInstanceId { get; set;}
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
    }
}
