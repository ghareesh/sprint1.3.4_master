﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EntityObject.Entities.HCP_task
{
    [Table("ChildTaskNewFiles")]
    public class ChildTaskNewFiles
    {
        [Key]
        public Guid ChildTaskNewFileId { get; set; }
        public Guid ChildTaskInstanceId { get; set; }
        public Guid TaskFileInstanceId { get; set; }
    }
}