﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("AM_Document")]
    public partial class AM_Document
    {
        [Key]
        public int ta_doc_id { get; set; }
        [Required]
        public string Transaction_type { get; set; }
        [Required]
        public string Document_Section { get; set; }
        public string document_shotname { get; set; }

        public string document { get; set; }

        public string shortname_id { get; set; }

        public int section { get; set; }
        public string Required { get; set; }
        public int ProjectActionID { get; set; }
        public int doc_type_id { get; set; }
        //AM_Document
    }

}
