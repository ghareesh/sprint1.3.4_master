namespace EntityObject.Entities.HCP_task
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TaskStep")]
    public partial class TaskStep
    {
        public TaskStep()
        {
            Tasks = new HashSet<Task>();
            TaskFlows = new HashSet<TaskFlow>();
            TaskFlows1 = new HashSet<TaskFlow>();
        }

        public int TaskStepId { get; set; }

        [Required]
        [StringLength(20)]
        public string TaskType { get; set; }

        [Required]
        [StringLength(20)]
        public string TaskStepNm { get; set; }

        [Required]
        [StringLength(50)]
        public string TaskDesc { get; set; }

        public TimeSpan? TaskDuration { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }

        public virtual ICollection<TaskFlow> TaskFlows { get; set; }

        public virtual ICollection<TaskFlow> TaskFlows1 { get; set; }
    }
}
