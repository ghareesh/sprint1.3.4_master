﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("NewFileRequest")]
    public class NewFileRequest
    {
        [Key]
        public int NewFileRequestId { get; set; }
        public Guid ChildTaskInstanceId { get; set; }
        public string Comments { get; set; }
        public int RequestedBy { get; set; }
        public DateTime RequestedOn { get; set; }
        public int? ReviewerProdViewId { get; set; }

    }
}
