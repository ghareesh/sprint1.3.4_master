﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    public partial class USP_GetOPAFiles_Result
    {
        public string FileName { get; set; }
        public byte[] FileData { get; set; }
        public double? FileSize { get; set; }
        public DateTime UploadTime { get; set; }
        public string UploadedBy { get; set; }
        public string RoleName { get; set; }


        public int FileId { get; set; }

        public Guid TaskFileId { get; set; }

        public string FileType { get; set; }

        public Guid TaskInstanceId { get; set; }

        public Guid? GroupTaskInstanceId { get; set; }

        public string SystemFileName { get; set; }

        public Guid TaskId { get; set; }

        // #605 Siddu
        public int? FolderKey { get; set; }




    }
}
