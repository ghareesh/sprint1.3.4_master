 

 

namespace EntityObject.Entities.HCP_task
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using EntityObject.Entities.HCP_live;
    using Core;

    public partial class HCP_task : DbContext
    {
        public HCP_task()
            //: base("HCP_task")//karrinew;commented hard coded string
            : base(WebUiConstants.TaskConnectionName)
        {
        }

        // #868 harish added 07052020 for R4R and NCRE
        public virtual DbSet<AM_R4RAndNCREDocument> AMR4RAndNCREDocument { get; set; }
        //harish added 17-12-2019
        public virtual DbSet<AM_Document> AmDocuments { get; set; }
        public virtual DbSet<DocumentTypes> DocumentTypes { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<Prod_View> Prod_View { get; set; }
        public virtual DbSet<TaskConcurrency> TaskConcurrencies { get; set; }
        public virtual DbSet<TaskFile> TaskFiles { get; set; }
        public virtual DbSet<TaskFlow> TaskFlows { get; set; }
        public virtual DbSet<TaskStep> TaskSteps { get; set; }
        public virtual DbSet<GroupTask> GroupTask { get; set; }
        public virtual DbSet<TaskAge> TaskAges { get; set; }
        public virtual DbSet<TaskReAssignment> TaskReAssignment { get; set; }
        public virtual DbSet<ParentChildTask> ParentChildTask { get; set; }
        public virtual DbSet<ReviewFileStatus> ReviewFileStatus { get; set; }
        public virtual DbSet<RequestAdditionalInfoFiles> RequestAdditionalInfoFiles { get; set; }
        public virtual DbSet<TaskFileHistory> TaskFileHistory { get; set; } 
        public virtual DbSet<ReviewerTaskAssignment> ReviewerTaskAssignment { get; set; }
        public virtual DbSet<ReviewFileComment> ReviewFileComment { get; set; }
        public virtual DbSet<NewFileRequest> NewFileRequest { get; set; }
        public virtual DbSet<ChildTaskNewFiles> ChildTaskNewFiles { get; set; }
        public virtual DbSet<Prod_TaskXref> ProdTaskXrefs { get; set; }
        //Production

        public virtual DbSet<RFORRANDNCR_GroupTasks> RFORRANDNCR_GroupTasks { get; set; }
        public virtual DbSet<Prod_GroupTasks> Prod_GroupTasks { get; set; }
        public virtual DbSet<Prod_FolderStructure> Prod_FolderStructure { get; set; }
        public virtual DbSet<Prod_SubFolderStructure> Prod_SubFolderStructure { get; set; }
        public virtual DbSet<TaskFile_FolderMapping> TaskFile_FolderMapping { get; set; }
        public virtual DbSet<Prod_Note> Prod_Notes { get; set; }

        public virtual DbSet<Prod_Form290Task> Prod_Form290Tasks{ get; set; }

		public virtual DbSet<Prod_FormAmendmentTask> Prod_FormAmendmentTask { get; set; }
		public virtual DbSet<FirmCommitmentAmendmentType> FirmCommitmentAmendmentType { get; set; }
		public virtual DbSet<SaluatationType> SaluatationType { get; set; }
		public virtual DbSet<PartyType> PartyType { get; set; }
		public virtual DbSet<DepositType> DepositType { get; set; }
		public virtual DbSet<EscrowEstimateType> EscrowEstimateType { get; set; }
		public virtual DbSet<RepairCostEstimateType> RepairCostEstimateType { get; set; }
		public virtual DbSet<SpecialConditionType> SpecialConditionType { get; set; }
		public virtual DbSet<ExhibitLetterType> ExhibitLetterType { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskConcurrency>()
                .Property(e => e.Concurrency)
                .IsFixedLength();

            modelBuilder.Entity<TaskStep>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.TaskStep)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaskStep>()
                .HasMany(e => e.TaskFlows)
                .WithRequired(e => e.TaskStep)
                .HasForeignKey(e => e.TaskStepId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaskStep>()
                .HasMany(e => e.TaskFlows1)
                .WithRequired(e => e.TaskStep1)
                .HasForeignKey(e => e.NxtTaskStepId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GroupTask>()
              .HasKey(e => e.TaskId);

            modelBuilder.Entity<TaskReAssignment>()
                .HasKey(e => e.TaskReAssignId);

            modelBuilder.Entity<ParentChildTask>()
                .HasKey(e => e.ChildTaskInstanceId);


            modelBuilder.Entity<ReviewFileComment>()
              .HasKey(e => e.ReviewFileCommentId);

            modelBuilder.Entity<ReviewFileStatus>()
              .HasKey(e => e.ReviewFileStatusId);

            modelBuilder.Entity<TaskFileHistory>()
              .HasKey(e => e.TaskFileHistoryId);

            modelBuilder.Entity<RequestAdditionalInfoFiles>()
                .HasKey(e => e.RequestAdditionalFileId);

            modelBuilder.Entity<NewFileRequest>()
                .HasKey(e => e.NewFileRequestId);

            modelBuilder.Entity<ChildTaskNewFiles>()
                .HasKey(e => e.ChildTaskNewFileId);
            modelBuilder.Entity<Prod_TaskXref>()
                .HasKey(e => e.TaskXrefid);

            modelBuilder.Entity<Prod_GroupTasks>()
             .HasKey(e => e.TaskId);
            modelBuilder.Entity<Prod_View>()
            .HasKey(e => e.ViewId);

            modelBuilder.Entity<Prod_Note>()
                .HasKey(e => e.NoteId);
            modelBuilder.Entity<DocumentTypes>()
               .HasKey(e => e.DocumentTypeId);
            modelBuilder.Entity<Prod_Form290Task>()
                .HasKey(e => e.Form290TaskID);

			modelBuilder.Entity<Prod_FormAmendmentTask>()
				.HasKey(e => e.AmendmentTemplateID);
			modelBuilder.Entity<FirmCommitmentAmendmentType>()
				.HasKey(e => e.FirmCommitmentAmendmentTypeId);
			modelBuilder.Entity<SaluatationType>()
				.HasKey(e => e.SaluatationTypeId);
			modelBuilder.Entity<PartyType>()
				.HasKey(e => e.PartyTypeId);
			modelBuilder.Entity<DepositType>()
				.HasKey(e => e.DepositTypeId);
			modelBuilder.Entity<EscrowEstimateType>()
				.HasKey(e => e.EscrowEstimateTypeId);
			modelBuilder.Entity<RepairCostEstimateType>()
				.HasKey(e => e.RepairCostEstimateTypeId);
			modelBuilder.Entity<SpecialConditionType>()
				.HasKey(e => e.SpecialConditionTypeId);
			modelBuilder.Entity<ExhibitLetterType>()
				.HasKey(e => e.ExhibitLetterTypeId);
		}
    }
}
