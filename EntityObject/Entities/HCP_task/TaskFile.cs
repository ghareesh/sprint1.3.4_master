namespace EntityObject.Entities.HCP_task
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TaskFile")]
    public partial class TaskFile
    {
        [Key]
        public int FileId { get; set; }

        public Guid TaskFileId { get; set; }

        [Required]
        public string FileName { get; set; }
        
        public string FileType { get; set; }

       // [Required]
        public byte[] FileData { get; set; }

        public Guid TaskInstanceId { get; set; }
        public double FileSize { get; set;}
        public DateTime UploadTime { get; set; }

        public string SystemFileName { get; set;}
        public int CreatedBy { get; set; }

        
        public string API_upload_status { get; set; }  
        public int? Version { get; set; }
        public string  DocId { get; set; }
        public string DocTypeID { get; set; }

        public string RoleName { get; set; }
        // harish added new method to save foldername in db 31-01-2020
        public string FolderName { get; set; }
        // harish added new method to save foldername in db 18-02-2020
        public int? FolderStructureKey { get; set; }
    }
}
