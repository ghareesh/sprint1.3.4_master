﻿using System;
using System.Collections.Generic;
using BusinessService;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using Model;

namespace HUDEmailNotificationApp.Classes
{
    public class NonCriticalLateNotification : INonCriticalLateNotification
    {
        //private readonly NonCritical_ProjectInfo_Model _ncrProjectInfo
        private readonly IEmailNotification emailNotification;
        private readonly INonCriticalLateAlertsManager nonCriticalLateAlertsManager;
        private IList<NonCriticalLateAlertsModel> nonCriticalLateAlertsModels;

        public NonCriticalLateNotification()
        {
            emailNotification = new EmailNotification();
            nonCriticalLateAlertsManager = new NonCriticalLateAlertsManager();
            nonCriticalLateAlertsModels = new List<NonCriticalLateAlertsModel>();
        }

        public int NotifyUsers(string date)
        {

            int numberOfAlerts = 0;

            nonCriticalLateAlertsModels = nonCriticalLateAlertsManager.GetNonCriticalLateAlertsToSend(date);

            foreach (var nonCriticalLateAlertsModel in nonCriticalLateAlertsModels)
            {
                string keywords = nonCriticalLateAlertsModel.LenderName + "|" + nonCriticalLateAlertsModel.FHANumber + "|" + nonCriticalLateAlertsModel.PropertyName;
                keywords += nonCriticalLateAlertsModel.TimeSpanId == 3 ? "|50" : nonCriticalLateAlertsModel.TimeSpanId == 2 ? "|270" : nonCriticalLateAlertsModel.TimeSpanId == 1 ? "|180" : "|90";
                EmailType emailType = nonCriticalLateAlertsModel.TimeSpanId == 3 ? EmailType.NCRENA2 : EmailType.NCRENA1;
                nonCriticalLateAlertsModel.HCP_EmailId = emailNotification.SendEmails(emailType, keywords, nonCriticalLateAlertsModel.RecipientIds);
                numberOfAlerts++;
            }

            RecordLateAlertHistory(nonCriticalLateAlertsModels);

            return numberOfAlerts;
        }

        public void RecordLateAlertHistory(IList<NonCriticalLateAlertsModel> model)
        {
            nonCriticalLateAlertsManager.SaveNonCriticalLateAlerts(model);
        }
    }
}