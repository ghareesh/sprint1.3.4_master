﻿using System.Collections.Generic;
using Model;

namespace HUDEmailNotificationApp.Classes
{
    public interface INonCriticalLateNotification
    {
        int NotifyUsers(string date);
        void RecordLateAlertHistory(IList<NonCriticalLateAlertsModel> model);
    }
}