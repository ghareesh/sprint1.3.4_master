﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Elmah;
using System.IO;
using BusinessService.Interfaces;
using HUDEmailNotificationApp.Classes;
using System.Net.Mail;


namespace JobLogger
{
    public static class Logger
    {
        static List<JobRunnerList> jobResult = new List<JobRunnerList>();

        /// <summary>
        /// Helper method to logs an error
        /// </summary>
        /// <param name="ex">Error during execution of job </param>
        public static void Log(Exception ex)
        {
            ErrorLog errorLog = ErrorLog.GetDefault(null);
            //Writes log into the Elmah_Error table
            errorLog.Log(new Error(ex));
            //Writes log to the file
            WriteLogToFile(ex);

        }
        /// <summary>
        /// Writes log to file
        /// </summary>
        /// <param name="e">exception</param>
        public static void WriteLogToFile(Exception e)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {

                string logFilePath = GetLogFilePath();

                if (logFilePath.Equals("")) return;
                //Creates error log folder if not exists
                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();


                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                streamWriter = new StreamWriter(fileStream);

                //streamWriter.WriteLine(DateTime.Now + Environment.NewLine + " ==> " + "Exception Details : " + Environment.NewLine + e.ToString() + Environment.NewLine + Environment.NewLine);
                streamWriter.WriteLine(DateTime.UtcNow + Environment.NewLine + " ==> " + "Exception Details : " + Environment.NewLine + e.ToString() + Environment.NewLine + Environment.NewLine);
            }
            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }
        }

        /// <summary>
        /// Builds the log file path 
        /// </summary>
        /// <returns>log file path</returns>
        public static string GetLogFilePath()
        {
            string logFilePath = System.Configuration.ConfigurationManager.AppSettings["logFilePath"];

            logFilePath = logFilePath + "NotificationJobRunner_ErrorLog_" + DateTime.Today.ToString("yyyyMMdd") + "." + "txt";

            return logFilePath;
        }
        /// <summary>
        /// Sends email on success/failure of the AAR job
        /// </summary>
        /// <param name="alertRepository"></param>
        public static void SendJobEmail(IEmailManager emailManager)
        {
            StringBuilder builder = new StringBuilder();
            string logFilePath = string.Empty;
            string attachmentPath = string.Empty;

            string fromEmail = HUDEmailNotificationApp.Program.C3SupportEmail;//System.Configuration.ConfigurationManager.AppSettings["fromEmail"];
            string toEmail = System.Configuration.ConfigurationManager.AppSettings["toEmail"];
            //string subject = "Email Notification Job Runner Status as of " + DateTime.Now;
            string subject = "Email Notification Job Runner Status as of " + DateTime.UtcNow;

            //Get log file path
            logFilePath = GetLogFilePath();
            FileInfo logFileInfo = new FileInfo(logFilePath);
            //Checks log file exists on specified path
            if (logFileInfo.Exists)
            {
                //Sets attachment to log file path
                attachmentPath = logFilePath;
            }

            //Check condition to see job failed before initializing any of the job
            if (jobResult.Count == 0)
            {
                builder.Append("Error occured while initializing notification. Please see attached log for details.");
            }
            else
            {
                builder.Append("Hello HHCP Support Team," + Environment.NewLine + Environment.NewLine);

                int jobCount = 0;
                string status = string.Empty;

                //loops through JobRunnerList to generate email body
                foreach (var jobBody in jobResult)
                {
                    jobCount++;
                    status = (jobBody.JobStatus == 0) ? "executed successfully. (" + jobBody.NotificationCount + " email notifications are sent)" :
                                                        "failed to execute. Please see attached log for details.";
                    builder.AppendFormat("{0}) {1} {2}", jobCount, jobBody.JobName, status);
                    builder.Append(Environment.NewLine);
                }

                bool isAnyJobFailed = jobResult.Where(q => q.JobStatus != 0).Count() > 0;

                //Checks any job failed during execution.
                if (!isAnyJobFailed)
                {
                    //Sets attachment path to empty 
                    //because all jobs executed successfully
                    attachmentPath = string.Empty;
                }

                builder.Append(Environment.NewLine);
                builder.Append("Thank You," + Environment.NewLine);
                builder.Append("HHCP Dev team");

            }

            emailManager.SendEmail(fromEmail, builder.ToString(), toEmail, subject, attachmentPath);
        }
        /// <summary>
        /// Writes job status after each job run
        /// </summary>
        /// <param name="jobName">Name of the job</param>
        /// <param name="jobStatus">Status of the job</param>

        public static void WriteJobResult(string jobName, int jobStatus, int nCount)
        {
            jobResult.Add(new JobRunnerList
                {
                    JobName = jobName,
                    JobStatus = jobStatus,
                    NotificationCount = nCount
                });
        }

    }

    /// <summary>
    /// Holds the result of each job 
    /// </summary>
    public class JobRunnerList
    {
        public string JobName { get; set; }
        public int JobStatus { get; set; }
        public int NotificationCount { get; set; }
    }
}
