﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Repository.AssetManagement;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;
using Model;
using Repository;
using Repository.Interfaces;

namespace BusinessService.AssetManagement
{
    public class NonCriticalRepairsRequestManager : INonCriticalRepairsRequestManager
    {
        private IProjectInfoRepository projectInfoRepository;
        private INonCriticalRepairsRepository nonCriticalRepairsRepository;
        private UnitOfWork unitOfWorkLive;
        private LenderInfoRepository lenderInfoRepository;
        private IUserRepository userRepository;
        private ITaskRepository taskRepository;
        private IUserLenderServicerRepository userLenderServicerRepository;
        private ILenderFhaRepository lenderFhaRepository;
        private INonCriticalProjectInfoRepository nonCriticalProjectInfoRepository;
        private ITransactionRepository  transactionRepository;
        private IDisclaimerMsgRepository disclaimerMsgRepository;// User Story 1901


        public NonCriticalRepairsRequestManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            projectInfoRepository = new ProjectInfoRepository(unitOfWorkLive);
            lenderInfoRepository = new LenderInfoRepository(unitOfWorkLive);
            nonCriticalRepairsRepository = new NonCriticalRepairsRepository(unitOfWorkLive);
            userRepository = new UserRepository(unitOfWorkLive);
            taskRepository = new TaskRepository();
            userLenderServicerRepository = new UserLenderServicerRepository(unitOfWorkLive);
            lenderFhaRepository = new LenderFhaRepository(unitOfWorkLive);
            nonCriticalProjectInfoRepository = new NonCriticalProjectInfoRepository(unitOfWorkLive);
            transactionRepository = new TransactionRepository(unitOfWorkLive);
            disclaimerMsgRepository = new DisclaimerMsgRepository(unitOfWorkLive);// User Story 1901
        }

        // User Story 1901
        public string GetDisclaimerMsgByPageType(string pageTypeName)
        {
            return disclaimerMsgRepository.GetDisclaimerMsgByPageType(pageTypeName);
        }

        public PropertyInfoModel GetPropertyInfo(string fhaNumber)
        {
            return projectInfoRepository.GetPropertyInfo(fhaNumber);
        }

        public string GetLenderName(int lenderId)
        {
            var keyVal = lenderInfoRepository.GetLenderDetail(lenderId);
            return (keyVal != null) ? keyVal.FirstOrDefault().Value : string.Empty;
        }

        public int GetAeUserIdByFhaNumber(string fhaNumber)
        {
            return projectInfoRepository.GetAeUserIdByFhaNumber(fhaNumber);
        }

        public string GetAeEmailByFhaNumber(string fhaNumber)
        {
            return projectInfoRepository.GetAeEmailByFhaNumber(fhaNumber);
        }

        
        public Guid SaveNonCriticalRepairsRequest(NonCriticalRepairsViewModel  model)
        {
            var ncrId = model.NonCriticalRequestId;
            var nonCriticalReferReasons = new List<NonCriticalReferReasonModel>();
            var nonCriticalProjectInfo = nonCriticalProjectInfoRepository.GetNonCriticalProjectInfo(model.PropertyId,
                model.LenderId, model.FHANumber);
            if (!model.IsRequestAutoApproved)
            {
                NonCriticalReferReasonModel nonCriticalReferReasonModel;
                if (model.IsScopeOfWorkChanged)
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 1
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                if (model.IsThisAnAdvance)
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 2
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                if (!model.IsFile92117Uploaded)
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 3
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                if (!model.IsFile92464Uploaded)
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 4
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                if (model.NonCriticalAccountBalance != 0 && model.PaymentAmountRequested > model.NCRECurrentBalance)
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 5
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                //if (model.ClosingDate != null && ((DateTime.Now - (DateTime)model.ClosingDate).Days > 365 && !model.IsFileReqExtnUploaded))
                if (model.ClosingDate != null && ((DateTime.UtcNow - (DateTime)model.ClosingDate).Days > 365 && !model.IsFileReqExtnUploaded))
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 6
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                if (model.NonCriticalAccountBalance < 1000000)
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 7
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                if (model.TroubledCode == "T")
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 8
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                if (model.PaymentAmountRequested > 50000)
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 9
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
                if (!model.IsNCREAmountValid || model.NonCriticalAccountBalance == 0)
                {
                    nonCriticalReferReasonModel = new NonCriticalReferReasonModel
                    {
                        NonCriticalRepairsRequestID = ncrId,
                        RuleId = 10
                    };
                    nonCriticalReferReasons.Add(nonCriticalReferReasonModel);
                }
            }
            
            var nonCriticalRequestId = nonCriticalRepairsRepository.SaveNonCriticalRepairsRequest(model, nonCriticalReferReasons);
            nonCriticalProjectInfo.IsAmountConfirmed = model.IsNCREAmountValid;
            nonCriticalProjectInfoRepository.UpdateNonCriticalProjectInfo(nonCriticalProjectInfo);
            if(model.IsRequestAutoApproved)
            {
               
                var transactionModel = new TransactionModel();
                transactionModel.TransactionId = Guid.NewGuid();
                transactionModel.FormId = nonCriticalRequestId;
                transactionModel.FormType = (int) FormType.Ncr;
                transactionModel.Amount = model.PaymentAmountRequested;
                transactionModel.CurrentBalance = nonCriticalProjectInfo.CurrentBalance;
                transactionModel.TransactionType = (int) TransactionType.NCRReimbursement;
                transactionModel.CreatedOn = DateTime.UtcNow;
                transactionModel.CreatedBy = UserPrincipal.Current.UserId;
                transactionModel.Status = (int?) RequestStatus.AutoApprove;
                transactionRepository.AddTransaction(transactionModel);
                nonCriticalProjectInfo.CurrentBalance = nonCriticalProjectInfo.CurrentBalance -
                                                       model.PaymentAmountRequested;
            }
            unitOfWorkLive.Save();
            return nonCriticalRequestId;
        }

        public void UpdateNonCriticalRepairsRequest(NonCriticalRepairsViewModel model)
        {
            nonCriticalRepairsRepository.UpdateNonCriticalRepairsRequest(model,nonCriticalProjectInfoRepository, transactionRepository);
            unitOfWorkLive.Save();
        }

        public UserViewModel GetUserById(int userId)
        {
            return userRepository.GetUserById(userId);
        }

      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateTaskId(NonCriticalRepairsViewModel model)
        {
            nonCriticalRepairsRepository.UpdateTaskId(model);
            unitOfWorkLive.Save();
        }

        public TaskModel GetLatestTaskId(Guid taskInstanceId)
        {
            return taskRepository.GetLatestTaskByTaskInstanceId(taskInstanceId); 
        }

        public List<string> GetAllowedNonCriticalFhaByLenderUserId(int userid)
        {
            return  userLenderServicerRepository.GetAllowedNonCriticalFhaByLenderUserId(userid, lenderFhaRepository,
                nonCriticalProjectInfoRepository);
        }

        public NonCriticalPropertyModel GetNonCriticalPropertyInfo(string fhanumber)
        {
            return nonCriticalProjectInfoRepository.GetNonCriticalPropertyInfo(fhanumber);
        }

        public NonCriticalPropertyModel GetNonCriticalStatusAndDrawInfo(NonCriticalPropertyModel model, string fhanumber)
        {
            return nonCriticalRepairsRepository.GetNonCriticalStatusAndDrawInfo(model, fhanumber);
        }

        public IList<NonCriticalRulesChecklistModel> GetNonCrticalRulesCheckList(FormType formType)
        {
            return nonCriticalRepairsRepository.GetNonCrticalRulesCheckList(formType);
        }

        public IList<NonCriticalReferReasonModel> GetNonCrticalReferReasons(Guid ncrId)
        {
            return nonCriticalRepairsRepository.GetNonCrticalReferReasons(ncrId);
        }
        public NonCriticalPropertyModel GetNonCrticalNonCrticalTransactions(int propertyId, int lenderId, string fhaNumber)
        {
            return nonCriticalRepairsRepository.GetNonCrticalNonCrticalTransactions(propertyId, lenderId, fhaNumber, nonCriticalProjectInfoRepository);
        }

        public bool GetTransactionLedgerStatus(string fhanumber)
        {
            return nonCriticalRepairsRepository.GetTransactionLedgerStatus(fhanumber);
        }      
    }
}
