﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Repository.Interfaces.ProjectAction;
using HUDHealthcarePortal.Repository.ProjectAction;
using Model.ProjectAction;
using Repository;
using Repository.Interfaces;
using Repository.Interfaces.ProjectAction;
using Repository.ProjectAction;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core.Utilities;
using System.Collections;

namespace BusinessService.ProjectAction
{
    public class GroupTaskManager : IGroupTaskManager
    {
        private UnitOfWork unitOfWorkTask;
        private UnitOfWork unitOfWorkLive;
        private IGroupTaskRepository groupTaskRepository;
        private ILenderFhaRepository lenderFhaRepository;
        private IProjectActionRepository projectActionRepository;
        private IUserRepository userRepository;
        private ITaskConcurrencyRepository taskConcurrencyRepository;
        private ITaskManager _taskManager;
        //Naveen hareesh 19-11-2019
        private IOPAManager _OpaManage;
        //karri#205
        private PaginateSortModel<GroupTaskModel> _GroupTasksForLenderResultset = null;
        private string _lookupFHANumber = string.Empty;

        public GroupTaskManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            groupTaskRepository = new GroupTaskRepository(unitOfWorkTask);
            lenderFhaRepository = new LenderFhaRepository(unitOfWorkLive);
            projectActionRepository = new ProjectActionRepository(unitOfWorkLive);
            userRepository = new UserRepository(unitOfWorkLive);
            taskConcurrencyRepository = new TaskConcurrencyRepository(unitOfWorkTask);
            _taskManager = new TaskManager();
            _OpaManage = new OPAManager();
        }

        public int AddNewGroupTask(GroupTaskModel groupTaskModel)
        {
            var groupTask = groupTaskRepository.AddNewGroupTask(groupTaskModel);
            if (groupTaskModel.Concurrency == null)
            {
                var taskModel = new TaskModel { TaskInstanceId = groupTaskModel.TaskInstanceId };
                groupTaskModel.Concurrency = taskConcurrencyRepository.AddTaskInstance(taskModel);
            }
            unitOfWorkTask.Save();
            return groupTask.TaskId;
        }

        public void UpdateGroupTask(ProjectActionViewModel projectActionViewModel)
        {
            groupTaskRepository.UpdateGroupTask(projectActionViewModel);
            unitOfWorkTask.Save();
        }

        public bool CheckIfProjectActionExistsForFhaNumber(int projectActionTypeId, string fhaNumber)
        {
            return groupTaskRepository.CheckIfProjectActionExistsForFhaNumber(projectActionTypeId, fhaNumber);
        }

        //karri#205
        public IList<String> getFHAList(int lenderId)
        {
            ArrayList arrColl = new ArrayList();
            IList<String> iListColl = new List<string>();

            if (_GroupTasksForLenderResultset == null || _GroupTasksForLenderResultset.TotalRows == 0)
                _GroupTasksForLenderResultset = groupTaskRepository.GetGroupTasksForLender(lenderId, lenderFhaRepository, projectActionRepository, userRepository);

            if (_GroupTasksForLenderResultset != null)
            {
                foreach (var r in _GroupTasksForLenderResultset.Entities)
                {
                    if (!arrColl.Contains(r.FhaNumber))
                        arrColl.Add(r.FhaNumber);
                }

                arrColl.Sort();//by default ASC
                foreach (string value in arrColl)
                    iListColl.Add(value);
            }

            return iListColl;
        }

        //karri#205
        public void setLookUpValues(string lookupFHANumber)
        {
            if (lookupFHANumber != null && !string.IsNullOrEmpty(lookupFHANumber))
                _lookupFHANumber = lookupFHANumber;
        }

        public PaginateSortModel<GroupTaskModel> GetGroupTasksForLender(int lenderId, int page, string sort, SqlOrderByDirecton sortDir)
        {
            var results = groupTaskRepository.GetGroupTasksForLender(lenderId, lenderFhaRepository, projectActionRepository, userRepository);
            
            if (results != null)
            {
                foreach (var r in results.Entities)
                {
                    //r.CreatedOn = TimezoneManager.GetPreferredTimeFromUtc(r.CreatedOn);
                    //r.ModifiedOn = TimezoneManager.GetPreferredTimeFromUtc(r.ModifiedOn);
                    //r.ProjectActionStartDate = TimezoneManager.GetPreferredTimeFromUtc(r.ProjectActionStartDate.Value);
                    r.CreatedOn = r.CreatedOn;
                    r.ModifiedOn = r.ModifiedOn;
                    r.ProjectActionStartDate = r.ProjectActionStartDate.Value;


                    var task = _taskManager.GetLatestTaskByTaskInstanceId((Guid)r.TaskInstanceId);
                    if (task != null)
                        if (task.DataStore1.Contains("OPAViewModel"))
                        {

                            var formUploadData = XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1, Encoding.Unicode) as OPAViewModel;
                            if (formUploadData != null)
                            {
                                r.ServicerSubmissionDate = formUploadData.ServicerSubmissionDate;
                            }

                        }


                }

                foreach (var model in results.Entities)
                {
                    if (model != null)
                    {

                        if (model.InUse == UserPrincipal.Current.UserId)
                        {
                            model.IsEditMode = true;
                        }
                        /*if (model.RequestStatus == (int)RequestStatus.Draft)
                        {
                            model.ServicerSubmissionDate = null;
                        }*/
                    }
                }
                //karri#205
                if (_lookupFHANumber != null && !string.IsNullOrEmpty(_lookupFHANumber))
                {
                    var query1 = results.Entities.Where(m => m.FhaNumber == _lookupFHANumber).AsQueryable();
                    return PaginateSort.SortAndPaginate(query1, sort, sortDir, results.PageSize, page);
                }

                var query = results.Entities.AsQueryable();
                return PaginateSort.SortAndPaginate(query, sort, sortDir, results.PageSize, page);
            }

            return null;
        }

        public GroupTaskModel GetGroupTaskById(int taskId)
        {
            var groupTaskModel = groupTaskRepository.GetGroupTaskById(taskId);
            groupTaskModel.Concurrency = taskConcurrencyRepository.GetConcurrencyTimeStamp(groupTaskModel.TaskInstanceId);
            return groupTaskModel;
        }

        public void UnlockGroupTask(int taskId)
        {
            groupTaskRepository.UnlockGroupTask(taskId);
            unitOfWorkTask.Save();
        }

        public void UpdateGroupTaskForCheckout(GroupTaskModel groupTaskModel)
        {
            if (groupTaskModel.Concurrency != null)
            {
                var taskModel = new TaskModel
                {
                    TaskInstanceId = groupTaskModel.TaskInstanceId,
                    Concurrency = groupTaskModel.Concurrency
                };
                taskConcurrencyRepository.UpdateTaskInstance(taskModel);
                groupTaskRepository.UpdateGroupTaskForCheckout(groupTaskModel);
                unitOfWorkTask.Save();
            }
        }

        public ProjectActionViewModel GetProjectActionViewModelFromGroupTask(int taskId)
        {
            var groupTask = GetGroupTaskById(taskId);
            var projectActionViewModel = new ProjectActionViewModel();

            projectActionViewModel.ProjectActionFormId = Guid.NewGuid();
            projectActionViewModel.FhaNumber = groupTask.FhaNumber;
            projectActionViewModel.PropertyName = groupTask.PropertyName;
            projectActionViewModel.RequestStatus = groupTask.RequestStatus;
            projectActionViewModel.RequestDate = groupTask.RequestDate;
            projectActionViewModel.ServicerSubmissionDate = groupTask.ServicerSubmissionDate;
            if (groupTask.ProjectActionStartDate != null)
                projectActionViewModel.ProjectActionDate = (DateTime)groupTask.ProjectActionStartDate;
            projectActionViewModel.ProjectActionTypeId = groupTask.ProjectActionTypeId;
            projectActionViewModel.RequesterName = groupTask.RequesterName;
            projectActionViewModel.IsAgreementAccepted = groupTask.IsDisclimerAccepted;
            projectActionViewModel.GroupTaskId = groupTask.TaskId;
            projectActionViewModel.GroupTaskInstanceId = groupTask.TaskInstanceId;
            projectActionViewModel.GroupTaskCreatedOn = groupTask.CreatedOn;
            projectActionViewModel.CreatedOn = groupTask.CreatedOn;
            projectActionViewModel.CreatedBy = groupTask.CreatedBy;
            projectActionViewModel.ModifiedBy = groupTask.ModifiedBy;
            projectActionViewModel.ModifiedOn = groupTask.ModifiedOn;
            projectActionViewModel.GroupTaskCreatedOn = groupTask.CreatedOn;
            projectActionViewModel.Concurrency = groupTask.Concurrency;
            projectActionViewModel.ServicerComments = groupTask.ServicerComments;
            if (UserPrincipal.Current.LenderId != null)
                projectActionViewModel.LenderId = (int)UserPrincipal.Current.LenderId;
            if (groupTask.InUse == UserPrincipal.Current.UserId && groupTask.RequestStatus == (int)RequestStatus.Draft)
            {
                projectActionViewModel.IsEditMode = groupTask.IsEditMode = true;
            }
            if (groupTask.InUse == 0 && groupTask.RequestStatus == (int)RequestStatus.Draft)
            {
                projectActionViewModel.IsDisplayCheckoutBtn = true;
            }
            return projectActionViewModel;
        }

        //OPA Changes

        public OPAViewModel GetOPAProjectActionViewModelFromGroupTask(int taskId)
        {
            var groupTask = GetGroupTaskById(taskId);
            var OPAActionViewModel = new OPAViewModel();

            OPAActionViewModel.ProjectActionFormId = Guid.NewGuid();
            OPAActionViewModel.FhaNumber = groupTask.FhaNumber;
            OPAActionViewModel.PropertyName = groupTask.PropertyName;
            OPAActionViewModel.RequestStatus = groupTask.RequestStatus;
            OPAActionViewModel.RequestDate = groupTask.RequestDate;
            OPAActionViewModel.ServicerSubmissionDate = groupTask.ServicerSubmissionDate;
            if (groupTask.ProjectActionStartDate != null)
                OPAActionViewModel.ProjectActionDate = (DateTime)groupTask.ProjectActionStartDate;
            OPAActionViewModel.ProjectActionTypeId = groupTask.ProjectActionTypeId;
            OPAActionViewModel.RequesterName = groupTask.RequesterName;
            OPAActionViewModel.IsAgreementAccepted = groupTask.IsDisclimerAccepted;
            OPAActionViewModel.GroupTaskId = groupTask.TaskId;
            OPAActionViewModel.GroupTaskInstanceId = groupTask.TaskInstanceId;
            OPAActionViewModel.TaskInstanceId = groupTask.TaskInstanceId;//To avoid multiple  files after approving
            OPAActionViewModel.GroupTaskCreatedOn = groupTask.CreatedOn;
            OPAActionViewModel.CreatedOn = groupTask.CreatedOn;
            OPAActionViewModel.CreatedBy = groupTask.CreatedBy;
            OPAActionViewModel.ModifiedBy = groupTask.ModifiedBy;
            OPAActionViewModel.ModifiedOn = groupTask.ModifiedOn;
            OPAActionViewModel.GroupTaskCreatedOn = groupTask.CreatedOn;
            OPAActionViewModel.Concurrency = groupTask.Concurrency;
            OPAActionViewModel.ServicerComments = groupTask.ServicerComments;
            OPAActionViewModel.IsAddressChange = groupTask.IsAddressChanged;
            if (UserPrincipal.Current.LenderId != null)
                OPAActionViewModel.LenderId = (int)UserPrincipal.Current.LenderId;
            if (groupTask.InUse == UserPrincipal.Current.UserId && groupTask.RequestStatus == (int)RequestStatus.Draft)
            {
                OPAActionViewModel.IsEditMode = groupTask.IsEditMode = true;
            }
            if (groupTask.InUse == 0 && groupTask.RequestStatus == (int)RequestStatus.Draft)
            {
                OPAActionViewModel.IsDisplayCheckoutBtn = true;
            }
            return OPAActionViewModel;
        }


        public void UpdateOPAGroupTask(OPAViewModel OPAActionViewModel)
        {
            if (OPAActionViewModel.viewId == 1)
            {
                groupTaskRepository.UpdateOPAGroupTask(OPAActionViewModel);
            }
            else
            {
                _OpaManage.UpdateOPAGroupTask1(OPAActionViewModel);
            }
            unitOfWorkTask.Save();
            
        }

        public GroupTaskModel GroupTaskbyFHAProjectAction(int projectActionTypeId, string fhaNumber)
        {
            return groupTaskRepository.GroupTaskbyFHAProjectAction(projectActionTypeId, fhaNumber);
        }


        public void LockGroupTask(int taskId)
        {
            groupTaskRepository.LockGroupTask(taskId);
            unitOfWorkTask.Save();
        }

        public bool CheckIfFileExistsByGroupTaskId(int groupTaskId)
        {
            return groupTaskRepository.CheckIfFileExistsByGroupTaskId(groupTaskId);
        }
        // harish added to get grouptask @20032020
        public GroupTaskModel GroupTaskbyTaskinstanceid(Guid taskIInstanceId)
        {
            return groupTaskRepository.GetGrouptaskbyTaskinstanceid(taskIInstanceId);
        }


        // Harish added to update grouptask 20032020
        public void UpdateOPAGroupTaskByModel(OPAViewModel OPAActionViewModel)
        {
            groupTaskRepository.UpdateOPAGroupTask(OPAActionViewModel);
            unitOfWorkTask.Save();

        }
    }
}
