﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using BusinessService.Interfaces;
using Core;
using Elmah.ContentSyndication;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Model.ProjectAction;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Repository.AssetManagement;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;
using Model;
using Repository;
using Repository.Interfaces;
using HUDHealthcarePortal.Repository.Interfaces;
using Repository.Interfaces.ProjectAction;
using Repository.ProjectAction;
using Repository.Interfaces.OPAForm;
using Repository.OPAForm;
using Model.Production;


namespace BusinessService.ProjectAction
{
    public class ProjectActionFormManager : IProjectActionFormManager
    {
		private IHUDSignatureRepository hudSignatureRepository;
		private IProjectInfoRepository projectInfoRepository;
        private IProjectActionFormRepository projectActionFormRepository;
        private UnitOfWork unitOfWorkLive;
        private LenderInfoRepository lenderInfoRepository;
        private IUserRepository userRepository;
        private ITaskRepository taskRepository;
        private ITaskFileRepository taskFileRepository;
        private IUserLenderServicerRepository userLenderServicerRepository;
        private ILenderFhaRepository lenderFhaRepository;
        private IQuestionRepository questionRepository;
        private IAddressRepository addressRepository;
        private IProjectActionAEFormStatusRepository projectActionAeFormStatusRepository;
        private IOPARepository opaRepository;
        private IPageTypeRepository pageTypeRepository;
        private ITaskFileHistoryRepository taskFileHistoryRepository;
        private INewFileRequestRepository newFileRequestRepository;
        private IChildTaskNewFileRepository childTaskNewFileRepository;
		private IFirmCommitmentAmendmentTypeRepository firmCommitmentAmendmentTypeRepository;
		private ISaluatationTypeRepository saluatationTypeRepository;
		private IPartyTypeRepository partyTypeRepository;
		private IDepositTypeRepository depositTypeRepository;
		private IEscrowEstimateTypeRepository escrowEstimateTypeRepository;
		private IRepairCostEstimateTypeRepository repairCostEstimateTypeRepository;
		private ISpecialConditionTypeRepository specialConditionTypeRepository;
		private IExhibitLetterTypeRepository exhibitLetterTypeRepository;
		private IProd_FormAmendmentTaskRepository prod_FormAmendmentTaskRepository;
		private UnitOfWork unitOfWorkTask;


        public ProjectActionFormManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            projectInfoRepository = new ProjectInfoRepository(unitOfWorkLive);
            projectActionFormRepository = new ProjectActionFormRepository(unitOfWorkLive);
            lenderInfoRepository = new LenderInfoRepository(unitOfWorkLive);
            userRepository = new UserRepository(unitOfWorkLive);
            taskRepository = new TaskRepository();
            taskFileRepository = new TaskFileRepository();
            userLenderServicerRepository = new UserLenderServicerRepository(unitOfWorkLive);
            lenderFhaRepository = new LenderFhaRepository(unitOfWorkLive);
            questionRepository = new QuestionRepository();
            addressRepository = new AddressRepository();
            projectActionAeFormStatusRepository = new ProjectActionAEFormStatusRepositoy(unitOfWorkLive);
            opaRepository = new OPARepository(unitOfWorkLive);
            pageTypeRepository = new PageTypeRepository(unitOfWorkLive);
			hudSignatureRepository = new HUDSignatureRepository(unitOfWorkLive);
			unitOfWorkTask = new UnitOfWork(DBSource.Task);
            taskFileHistoryRepository = new TaskFileHistoryRepository(unitOfWorkTask);
            newFileRequestRepository = new NewFileRequestRepository(unitOfWorkTask);
            childTaskNewFileRepository = new ChildTaskNewFileRepository(unitOfWorkTask);
			firmCommitmentAmendmentTypeRepository = new FirmCommitmentAmendmentTypeRepository(unitOfWorkTask);
			saluatationTypeRepository = new SaluatationTypeRepository(unitOfWorkTask);
			partyTypeRepository = new PartyTypeRepository(unitOfWorkTask);
			depositTypeRepository = new DepositTypeRepository(unitOfWorkTask);
			escrowEstimateTypeRepository = new EscrowEstimateTypeRepository(unitOfWorkTask);
			repairCostEstimateTypeRepository = new RepairCostEstimateTypeRepository(unitOfWorkTask);
			specialConditionTypeRepository = new SpecialConditionTypeRepository(unitOfWorkTask);
			exhibitLetterTypeRepository = new ExhibitLetterTypeRepository(unitOfWorkTask);
			prod_FormAmendmentTaskRepository = new Prod_FormAmendmentTaskRepository(unitOfWorkTask);
		}

        public List<string> GetAllowedFhaByLenderUserId(int userid)
        {
            return userLenderServicerRepository.GetAllowedFhaByLenderUserId(userid, lenderFhaRepository);
        }

        public string GetLenderName(int lenderId)
        {
            var keyVal = lenderInfoRepository.GetLenderDetail(lenderId);
            return (keyVal != null) ? keyVal.FirstOrDefault().Value : string.Empty;
        }

        public PropertyInfoModel GetPropertyInfo(string fhaNumber)
        {
            return projectInfoRepository.GetPropertyInfo(fhaNumber);
        }

        public ProjectActionViewModel GetContactAddressInfo(ProjectActionViewModel projectActionViewModel)
        {

            var projectInfo = projectInfoRepository.GetProjectInfoByFhaNumber(projectActionViewModel.FhaNumber);
            if (projectInfo != null)
            {
                if (projectInfo.Servicer_AddressID != null)
                    projectActionViewModel.ContactAddressViewModel.ServicerAddress =
                        addressRepository.GetAddressByID((int)projectInfo.Servicer_AddressID);
                projectActionViewModel.ContactAddressViewModel.ServicerAddress.Organization =
                    HelpManager.RemoveZeros(projectInfo.Servicing_Mortgagee_Name);
                if (projectInfo.Lender_AddressID != null)
                    projectActionViewModel.ContactAddressViewModel.LenderAddress =
                        addressRepository.GetAddressByID((int)projectInfo.Lender_AddressID);
                projectActionViewModel.ContactAddressViewModel.LenderAddress.Organization =
                    HelpManager.RemoveZeros(projectInfo.Lender_Mortgagee_Name);
                if (projectInfo.Owner_Contact_AddressID != null)
                    projectActionViewModel.ContactAddressViewModel.OwnerAddress =
                        addressRepository.GetAddressByID((int)projectInfo.Owner_Contact_AddressID);
                projectActionViewModel.ContactAddressViewModel.OwnerAddress.Organization =
                     HelpManager.RemoveZeros(projectInfo.Owner_Organization_Name);
                projectActionViewModel.ContactAddressViewModel.OwnerAddress.ContactName =
                     HelpManager.RemoveZeros(projectInfo.Owner_Contact_Indv_Fullname);
                if (projectInfo.Mgmt_Contact_AddressID != null)
                    projectActionViewModel.ContactAddressViewModel.ManagementAgentAddress =
                        addressRepository.GetAddressByID((int)projectInfo.Mgmt_Contact_AddressID);
                projectActionViewModel.ContactAddressViewModel.ManagementAgentAddress.Organization =
                     HelpManager.RemoveZeros(projectInfo.Mgmt_Agent_org_name);
                projectActionViewModel.ContactAddressViewModel.ManagementAgentAddress.ContactName =
                     HelpManager.RemoveZeros(projectInfo.Mgmt_Contact_FullName);
                // once operator is included in Project Info, need to add code for the same 


            }

            return projectActionViewModel;
        }

        public IEnumerable<KeyValuePair<int, string>> GetProjectActionTypes()
        {
            return projectActionFormRepository.GetProjectActionTypes();
        }

        public IDictionary<string, IList<QuestionViewModel>> GetCheckListByActionType(Guid projectActionFormId, int projectActionTypeId, bool submitted = false, Guid taskInstanceID = new Guid())
        {
            var result = questionRepository.GetAllQuestionsBySections(projectActionTypeId);

            if (result != null)
            {
                var taskFiles = taskFileRepository.GetGroupTaskFilesByTaskInstance(taskInstanceID).ToList();
                var projectActionFormStatuses =
                    projectActionAeFormStatusRepository.GetProjectActionAEFormStatus(projectActionFormId);

                foreach (var sectionName in result.Keys)
                {
                    foreach (QuestionViewModel question in result[sectionName])
                    {
                        var tempTaskFile =
                            taskFiles.Find(t => t.GroupFileType.ToString().Trim() == question.CheckListId.ToString());
                        //taskFileRepository.GetGroupTaskFileByTaskInstanceAndFileTypeId(taskInstanceID,// question.CheckListId);

                        if (tempTaskFile != null)
                        {
                            question.FileName = tempTaskFile.FileName;
                            question.IsLenderUploadedDocument = true;
                            question.SystemFileName = tempTaskFile.SystemFileName;
                        }

                        int? status = projectActionFormStatuses.ToList().Find(t => t.Key == question.CheckListId).Value;
                        question.Status = status != null
                            ? Enum.GetName(typeof(StatusForProjectActionAEForm), status)
                            : "";

                    }

                }


            }

            return result;
        }

        public string GetProjectActionName(int projectActionId)
        {
            return projectActionFormRepository.GetProjectActionName(projectActionId);
        }

        public Guid SaveProjectActionRequestForm(ProjectActionViewModel projectActionViewModel)
        {
            projectActionViewModel.ProjectActionFormId = Guid.NewGuid();
            var formId = projectActionFormRepository.SaveProjectActionRequestForm(projectActionViewModel);
            unitOfWorkLive.Save();
            return formId;
        }

        public string GetAeEmailByFhaNumber(string fhaNumber)
        {
            return projectInfoRepository.GetAeEmailByFhaNumber(fhaNumber);
        }

        public TaskModel GetLatestTaskId(Guid taskInstanceId)
        {
            return taskRepository.GetLatestTaskByTaskInstanceId(taskInstanceId);
        }
        public void UpdateTaskId(ProjectActionViewModel model)
        {
            projectActionFormRepository.UpdateTaskId(model);
            unitOfWorkLive.Save();
        }

        public void UpdateProjectActionRequestForm(ProjectActionViewModel projectActionViewModel)
        {
            projectActionFormRepository.UpdateProjectActionRequestForm(projectActionViewModel);
            unitOfWorkLive.Save();
        }

        public ProjectActionViewModel GetProjectActionFormById(Guid projectActionFormId)
        {
            return projectActionFormRepository.GetProjectActionRequestId(projectActionFormId);
        }

        public void InsertAEChecklistStatus(ProjectActionViewModel projectActionViewModel)
        {
            projectActionFormRepository.InsertAEChecklistStatus(projectActionViewModel);
        }

        public bool IsApprovalStatusTrueForAllQuestions(Guid projectActionFormId)
        {
            return projectActionAeFormStatusRepository.IsApprovalStatusTrueForAllQuestions(projectActionFormId);
        }

        public void UpdateAECheckList(ProjectActionViewModel projectActionViewModel)
        {
            projectActionFormRepository.UpdateAEChecklistStatus(projectActionViewModel,
                projectActionAeFormStatusRepository);
            unitOfWorkLive.Save();
        }

        public bool IsProjectActionApprovedOrDenied(Guid projectActionFormId)
        {
            return projectActionFormRepository.IsProjectActionApprovedOrDenied(projectActionFormId);
        }

        public OPAViewModel GetContactAddressInfo(OPAViewModel opaViewModel)
        {

            var projectInfo = projectInfoRepository.GetProjectInfoByFhaNumber(opaViewModel.FhaNumber);
            if (projectInfo != null)
            {
                //if (projectInfo.Servicer_AddressID != null)
                //    opaViewModel.ContactAddressViewModel.ServicerAddress =
                //        addressRepository.GetAddressByID((int)projectInfo.Servicer_AddressID);
                //opaViewModel.ContactAddressViewModel.ServicerAddress.Organization =
                //    HelpManager.RemoveZeros(projectInfo.Servicing_Mortgagee_Name);
                //if (projectInfo.Lender_AddressID != null)
                //    opaViewModel.ContactAddressViewModel.LenderAddress =
                //        addressRepository.GetAddressByID((int)projectInfo.Lender_AddressID);
                //opaViewModel.ContactAddressViewModel.LenderAddress.Organization =
                //    HelpManager.RemoveZeros(projectInfo.Lender_Mortgagee_Name);
                //if (projectInfo.Owner_Contact_AddressID != null)
                //    opaViewModel.ContactAddressViewModel.OwnerAddress =
                //        addressRepository.GetAddressByID((int)projectInfo.Owner_Contact_AddressID);
                //opaViewModel.ContactAddressViewModel.OwnerAddress.Organization =
                //     HelpManager.RemoveZeros(projectInfo.Owner_Organization_Name);
                //opaViewModel.ContactAddressViewModel.OwnerAddress.ContactName =
                //     HelpManager.RemoveZeros(projectInfo.Owner_Contact_Indv_Fullname);
                //if (projectInfo.Mgmt_Contact_AddressID != null)
                //    opaViewModel.ContactAddressViewModel.ManagementAgentAddress =
                //        addressRepository.GetAddressByID((int)projectInfo.Mgmt_Contact_AddressID);
                //opaViewModel.ContactAddressViewModel.ManagementAgentAddress.Organization =
                //     HelpManager.RemoveZeros(projectInfo.Mgmt_Agent_org_name);
                //opaViewModel.ContactAddressViewModel.ManagementAgentAddress.ContactName =
                //     HelpManager.RemoveZeros(projectInfo.Mgmt_Contact_FullName);
                // once operator is included in Project Info, need to add code for the same 
            }

            return opaViewModel;
        }

        public void UpdateTaskId(OPAViewModel model)
        {
            opaRepository.UpdateTaskId(model);
            unitOfWorkLive.Save();
        }

        public string GetOPAName(int opaId)
        {
            return opaRepository.GetOPAName(opaId);
        }


        public Guid SaveOPAForm(OPAViewModel model)
        {
            model.ProjectActionFormId = Guid.NewGuid();
            var formId = opaRepository.SaveOPAForm(model);
            unitOfWorkLive.Save();
            return formId;
        }

        public void UpdateProjectActionRequestForm(OPAViewModel opaViewModel)
        {
            opaRepository.UpdatOPARequestForm(opaViewModel);
            unitOfWorkLive.Save();
        }
        public List<TaskModel> GetChildTasks(Guid taskInstanceId)
        {
            return opaRepository.GetChildTasks(taskInstanceId);
        }

        public List<OPAHistoryViewModel> GetOpaHistory(Guid taskInstanceId)
        {
            return opaRepository.GetOpaHistory(taskInstanceId);
        }

        public List<OPAHistoryViewModel> GetOpaFileCommentHistory(int taskFileId)
        {
            return opaRepository.GetOpaFileCommentHistory(taskFileId);
        }
        public List<OPAHistoryViewModel> GetOpaFileUploadHistory(int taskFileId)
        {
            return opaRepository.GetOpaFileUploadHistory(taskFileId);
        }

        /*public void UpdateAdditionalInfo(OPAViewModel opaViewModel)
        {
             opaRepository.UpdateAdditionalInfo(opaViewModel);
             unitOfWorkLive.Save();
        }
        */

        public OPAViewModel OPATaskbyFHAProjectAction(int projectActionTypeId, string fhaNumber)
        {
            return opaRepository.OPATaskbyFHAProjectAction(projectActionTypeId, fhaNumber);

        }

        public int GetPageTypeIdByName(string pageTypeName)
        {
            return pageTypeRepository.GetPageTypeIdByName(pageTypeName);
        }

        public string GetPageTypeNameById(int pPageTypeId)
        {
            return pageTypeRepository.GetPageTypeNameById(pPageTypeId);
        }

        public OPAViewModel GetOPAByTaskId(int taskId)
        {
            return opaRepository.GetOPAByTaskId(taskId);
        }

        public OPAViewModel GetOPAByChildTaskId(Guid childTaskInstanceId)
        {
            return opaRepository.GetOPAByChildTaskId(childTaskInstanceId);
        }

        public List<OPAHistoryViewModel> GetOpaHistoryForRequestAdditionalInfo(Guid childTaskInstanceId)
        {
            return opaRepository.GetOpaHistoryForRequestAdditionalInfo(childTaskInstanceId);
        }

        public void SaveLenderRequestAdditionalFiles(TaskFileHistoryModel model)
        {
            taskFileHistoryRepository.SaveToTaskFileHistory(model);
            unitOfWorkTask.Save();
        }

        public OPAViewModel GetOPAByTaskInstanceId(Guid taskIInstanceId)
        {
            return opaRepository.GetOPAByTaskInstanceId(taskIInstanceId);
        }

        public int DeleteChildTaskFile(Guid childTaskFileId)
        {
            var success = taskFileHistoryRepository.DeleteChildTaskFile(childTaskFileId);
            if (success == 1)
                unitOfWorkTask.Save();
            return success;
        }

        public void SaveNewFileRequest(NewFileRequestModel model)
        {
            newFileRequestRepository.SaveNewFileRequest(model);
            unitOfWorkTask.Save();
        }


        public List<Guid> GeetChildTaskforReassignment(List<Guid> Joinlist, string assignedto)
        {
            return opaRepository.GeetChildTaskforReassignment(Joinlist, assignedto);
        }

        public bool IsFileHistoryExists(Guid taskFileId)
        {
            return taskFileHistoryRepository.IsFileHistoryExists(taskFileId);
        }

        public void SaveChildTaskNewFiles(ChildTaskNewFileModel newFile)
        {
            childTaskNewFileRepository.SaveChildTaskNewFiles(newFile);
        }

        public bool IsFileExistsForChildTask(Guid childTaskInstanceId)
        {
            return childTaskNewFileRepository.IsFileExistsForChildTask(childTaskInstanceId);
        }

        #region Production Application
  //      public List<OPAHistoryViewModel> GetProdOpaHistoryFileList(Guid taskInstanceId, int userId, int viewId, int folderKey, int notAssigned)
		//{
		//	return opaRepository.GetProdOpaHistoryFileList(taskInstanceId, userId, viewId, folderKey, notAssigned);
		//}

		public List<OPAHistoryViewModel> GetProdOpaHistoryFileList(Guid taskInstanceId, int userId, int viewId, int folderKey)
		{
			return opaRepository.GetProdOpaHistoryFileList(taskInstanceId, userId, viewId, folderKey);
			//return opaRepository.GetProdOpaHistoryFileList(taskInstanceId, userId, viewId, folderKey, notAssigned);
		}

		public List<OPAHistoryViewModel> GetProdOpaHistoryReviewersList(int userId, int viewId, int fileId)
        {
            return opaRepository.GetProdOpaHistoryReviewersList(userId, viewId, fileId);
        }
        public List<Prod_OPAHistoryViewModel> GetProdOpaHistory(Guid taskInstanceId, string PropertyId, string FhaNumber, int userId, int viewId, bool isEdit)
        {
            return opaRepository.GetProdOpaHistory(taskInstanceId, PropertyId, FhaNumber, userId, viewId, isEdit);
        }

        public List<Prod_OPAHistoryViewModel> GetProdLenderUpload(Guid taskInstanceId, string PropertyId, string FhaNumber)
        {
            return opaRepository.GetProdLenderUpload(taskInstanceId, PropertyId, FhaNumber);
        }
      
        public List<Prod_OPAHistoryViewModel> GetProdLenderUploadForAmendments(Guid taskInstanceId, string PropertyId, string FhaNumber)
        {
            return opaRepository.GetProdLenderUploadForAmendments(taskInstanceId, PropertyId, FhaNumber);
        }
        
        public List<Prod_SubFolderStructureModel> GetProdSubFolders(int folderkey, string PropertyID, string FhaNumber)
        {
            return opaRepository.GetSubFolder(folderkey, PropertyID, FhaNumber);
        }

        public Prod_Assainedto AssignedTo(int Pageid, string FhaNumber)
        {
            return opaRepository.AssignedTo(Pageid, FhaNumber);
        }
        public string GetTaskId(int pageTypeId, string fHANumber, Guid taskInstanceId)
        {
            return opaRepository.GetTaskId(pageTypeId, fHANumber, taskInstanceId);
        }
        public List<Prod_SubFolderStructureModel> GetFolderInfoByKey(int folderkey)
        {
            return opaRepository.GetFolderInfoByKey(folderkey);
        }

        public object FormNameBYPageTypeID(int pageTypeId)
        {
            return opaRepository.FormNameBYPageTypeID(pageTypeId);
        }
        public List<int> GetFileIDByFolderKey(int folderkey)
        {
            return opaRepository.GetFileIDByFolderKey(folderkey);
        }

        public int SaveProdSubFolders(Prod_SubFolderStructureModel model)
        {
            return opaRepository.SaveSubFolder(model);
        }
        public OPAViewModel GetProdOPAByChildTaskId(Guid childTaskInstanceId)
        {
            return opaRepository.GetProdOPAByChildTaskId(childTaskInstanceId);
        }

        public OPAViewModel GetProdOPAByTaskXrefId(Guid taskXrefId)
        {
            return opaRepository.GetProdOPAByChildTaskId(taskXrefId);
        }


        public PropertyInfoModel GetProdPropertyInfo(string fhaNumber)
        {
            return projectInfoRepository.GetProdPropertyInfo(fhaNumber);
        }
        public PropertyInfoModel GetAssetManagementPropertyInfo(string fhaNumber)
        {
            return projectInfoRepository.GetAssetManagementPropertyInfo(fhaNumber);
        }


        public List<TaskModel> GetProdChildTasksByXrefId(Guid taskXrefId, bool isXref)
        {
            return opaRepository.GetProdChildTasksByXrefId(taskXrefId, isXref);
        }

        #endregion


        public OPAViewModel GetProdOPAByFha(string FhaNumber)
        {
            return opaRepository.GetProdOPAByFha(FhaNumber);
        }


        public List<Prod_OPAHistoryViewModel> GetGetWPUpload(Guid taskInstanceId, int Status, int PageTypeId)
        {
            return opaRepository.GetGetWPUpload(taskInstanceId, Status, PageTypeId);
        }

		#region Web Signature
		public int AddNewSignature(HUDSignatureModel pModel)
		{
			return hudSignatureRepository.AddNewSignature(pModel);
		}
		public HUDSignatureModel GetHUDSignatureByUserId(int pId)
		{
			return hudSignatureRepository.GetHUDSignatureByUserId(pId);

		}
		public void UpdateHUDSignatureByUserId(HUDSignatureModel pModel)
		{
			hudSignatureRepository.UpdateHUDSignatureByUserId(pModel);
		}
		public bool SaveAmendment(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel)
		{
			prod_FormAmendmentTaskRepository.SaveAmendment(pProd_FormAmendmentTaskModel);
			return true;
		}

		public bool SaveDapFromAmendmentSP(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel)
		{
			prod_FormAmendmentTaskRepository.SaveDapFromAmendmentSP(pProd_FormAmendmentTaskModel);
			return true;
		}
		public Prod_FormAmendmentTaskModel GetFormAmendmentById(int pAmendmentTemplateID)
		{
			return prod_FormAmendmentTaskRepository.GetFormAmendmentById(pAmendmentTemplateID);
		}

		public IList<Prod_FormAmendmentTaskModel> GetFormAmendmentByTaskInstanceId(Guid pTaskInstanceId)
		{
			return prod_FormAmendmentTaskRepository.GetFormAmendmentByTaskInstanceId(pTaskInstanceId);
		}

		public IList<Prod_FormAmendmentTaskModel> GetFormAmendmentByFHANumber(string pFHANumber)
		{
			return prod_FormAmendmentTaskRepository.GetFormAmendmentByFHANumber(pFHANumber);
		}
		#endregion

		#region ammendments lookups
		public List<FirmCommitmentAmendmentTypeModel> GetAllFirmCommitmentAmendmentTypes()
		{
			List<FirmCommitmentAmendmentTypeModel> objFirmCommitmentAmendmentTypeModel = new List<FirmCommitmentAmendmentTypeModel>();
			objFirmCommitmentAmendmentTypeModel = firmCommitmentAmendmentTypeRepository.GetFirmCommitmentAmendmentTypes();
			return objFirmCommitmentAmendmentTypeModel;
		}
		public List<SaluatationTypeModel> GetAllSaluatationTypes()
		{
			List<SaluatationTypeModel> objSaluatationTypeModel = new List<SaluatationTypeModel>();
			objSaluatationTypeModel = saluatationTypeRepository.GetSaluatationTypes();
			return objSaluatationTypeModel;
		}

		public string GetSaluatationTypeName(int pId)
		{
			return saluatationTypeRepository.GetSaluatationTypeName(pId);			 
		}

		public List<PartyTypeModel> GetAllPartyTypes()
		{
			List<PartyTypeModel> objPartyTypeModel = new List<PartyTypeModel>();
			objPartyTypeModel = partyTypeRepository.GetPartyTypes();
			return objPartyTypeModel;
		}
		public string GetPartyTypeName(int pId)
		{
			return partyTypeRepository.GetPartyTypeName(pId);
		}

		public List<DepositTypeModel> GetAllDepositTypes()
		{
			List<DepositTypeModel> objDepositTypeModel = new List<DepositTypeModel>();
			objDepositTypeModel = depositTypeRepository.GetDepositTypes();
			return objDepositTypeModel;
		}

		public string GetDepositTypeName(int pId)
		{
			return depositTypeRepository.GetDepositTypeName(pId);
		}

		public List<EscrowEstimateTypeModel> GetAllEscrowEstimateTypes()
		{
			List<EscrowEstimateTypeModel> objEscrowEstimateTypeModel = new List<EscrowEstimateTypeModel>();
			objEscrowEstimateTypeModel = escrowEstimateTypeRepository.GetEscrowEstimateTypes();
			return objEscrowEstimateTypeModel;
		}
		public string GetEscrowEstimateTypeName(int pId)
		{
			return escrowEstimateTypeRepository.GetEscrowEstimateTypeName(pId);
		}

		public List<RepairCostEstimateTypeModel> GetAllRepairCostEstimateTypes()
		{
			List<RepairCostEstimateTypeModel> objRepairCostEstimateTypeModel = new List<RepairCostEstimateTypeModel>();
			objRepairCostEstimateTypeModel = repairCostEstimateTypeRepository.GetRepairCostEstimateTypes();
			return objRepairCostEstimateTypeModel;
		}
		public string GetRepairCostEstimateTypeName(int pId)
		{
			return repairCostEstimateTypeRepository.GetRepairCostEstimateTypeName(pId);
		}


		public List<SpecialConditionTypeModel> GetAllSpecialConditionTypes()
		{
			List<SpecialConditionTypeModel> objSpecialConditionTypeModel = new List<SpecialConditionTypeModel>();
			objSpecialConditionTypeModel = specialConditionTypeRepository.GetSpecialConditionTypes();
			return objSpecialConditionTypeModel;
		}
		public string GetSpecialConditionTypeName(int pId)
		{
			return specialConditionTypeRepository.GetSpecialConditionTypeName(pId);
		}



		public List<ExhibitLetterTypeModel> GetAllExhibitLetterTypes()
		{
			List<ExhibitLetterTypeModel> objExhibitLetterTypeModel = new List<ExhibitLetterTypeModel>();
			objExhibitLetterTypeModel = exhibitLetterTypeRepository.GetExhibitLetterTypes();
			return objExhibitLetterTypeModel;
		}
		public string GetExhibitLetterTypeName(int pId)
		{
			return exhibitLetterTypeRepository.GetExhibitLetterTypeName(pId);
		}

        #endregion
//#293 Hareesh

        public List<string> GetAllowedFhaByLender()
        {
            return userLenderServicerRepository.GetAllowedFhaByLender( lenderFhaRepository);
        }
        // helpdesk fha number hareesh
        public List<string> GetAllowedFhaByLenderdetails(int lenderUserId)
        {
            return userLenderServicerRepository.GetAllowedFhaByLenderDetails(lenderUserId, lenderFhaRepository);
        }
        //#664
        public List<OPAHistoryViewModel> GetOpaHistoryRAI(Guid taskInstanceId)
        {
            return opaRepository.GetOpaHistory(taskInstanceId);
        }

        //harish added new line of code for updating project action type id
        public int Updateprojectactiontypeid(OPAViewModel model, int Projectactiontypeid)
        {
            return opaRepository.Updateprojectactiontypeid(model, Projectactiontypeid);


        }
        // harish added new method to get data

        public List<Prod_OPAHistoryViewModel> GetProdLenderUploadForAssetManagement(Guid taskInstanceId, string PropertyId, string FhaNumber)
        {
            return opaRepository.GetProdLenderUpload(taskInstanceId, PropertyId, FhaNumber);
        }
        // harish added for getting project action type for 11032020
        public string GetProjectActionNameforAssetmanagement(int projectActionId)
        {
            return projectActionFormRepository.GetProjectActionNameforAssetmanagement(projectActionId);
        }

        //karri, venkat#832
        public int GetForm290ClosingPreChecksValue(string FHANumber)
        {
            return opaRepository.GetForm290ClosingPreChecksValue(FHANumber);

        }
    }
}
