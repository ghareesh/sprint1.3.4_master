﻿using System;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;


namespace HUDHealthcarePortal.BusinessService
{
    public class AdhocReportManager : IAdhocReportManager
    {
        private IAdhocReportRepository adhocReportRepository;
        private UnitOfWork unitOfWorkLive;


        public AdhocReportManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);

            adhocReportRepository = new AdhocReportRepository(unitOfWorkLive);
        }

        public IEnumerable<AdhocReportModel> GetAdhocReport(string wlmList,
                                                            string aeList,
                                                            DateTime dMinPeriodEnding,
                                                            DateTime dMaxPeriodEnding,
                                                            string partialFhaNumber,
                                                            string lenderList,
                                                            int activeAllSelected)
        {
            var results = adhocReportRepository.GetAdhocReport(wlmList,
                                                               aeList,
                                                               dMinPeriodEnding,
                                                               dMaxPeriodEnding,
                                                               partialFhaNumber,
                                                               lenderList,
                                                               activeAllSelected);
            return results;
        }

        public IEnumerable<KeyValuePair<int, string>> GetAEsbyWLMList(string wlmList)
        {
            var results = adhocReportRepository.GetAEsbyWLMList(wlmList);
            return results;
        }

        public IEnumerable<KeyValuePair<int, string>> GetLendersbyWLMAEList(string wlmList, string aeList)
        {
            var results = adhocReportRepository.GetLendersbyWLMAEList(wlmList, aeList);
            return results;
        }
    }
}
