﻿using System;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Repository.Interfaces;
using Repository.ManagementReport;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Repository.AssetManagement;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;
using Repository.Interfaces.OPAForm;
using Repository.Interfaces.ProjectAction;
using Repository.OPAForm;
using Repository.ProjectAction;

namespace BusinessService.ManagementReport
{
    public class PAMReportsManager : IPAMReportManager
    {

        private IPAMRepository _iplmRepository;
        private HUDWorkLoadManagerRepository _wlmRepositoty;
        private UnitOfWork unitOfWorkLive;
        private IReserveForReplacementRepository _r4rRepository;
        private INonCriticalRepairsRepository _ncrRepository;
        private INonCriticalRequestExtensionRepository _ncrExRepository;
        private IProjectActionFormRepository _projectActionFormRepository;
        private IOPARepository _oPARepository;
        private DisclaimerMsgRepository disclaimerMsgRepository;// User Story 1901


        public PAMReportsManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            _iplmRepository = new PAMReportRepository(unitOfWorkLive);
            _wlmRepositoty = new HUDWorkLoadManagerRepository();
            _r4rRepository = new ReserveForReplacementRepository();
            _ncrRepository = new NonCriticalRepairsRepository(unitOfWorkLive);
            _ncrExRepository = new NonCriticalRequestExtensionRepository(unitOfWorkLive);
            _projectActionFormRepository = new ProjectActionFormRepository(unitOfWorkLive);
            _oPARepository = new OPARepository(unitOfWorkLive);
            disclaimerMsgRepository = new DisclaimerMsgRepository(unitOfWorkLive);// User Story 1901
        }

        //public PAMReportModel GetPAMSummary(string userName,string isouIds, string aeIds, DateTime? fromDate, DateTime? toDate, int wlmId, int aeId,int isouId, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds)
        //karri:D#610;modified
        public PAMReportModel GetPAMSummary(string userName, string isouIds, string aeIds, DateTime? fromDate, DateTime? toDate, int wlmId, int aeId, int isouId, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds
            , string propertyName, string fhaNumber, string assignedTO, int? hasOpened, int? daysOpened)
        {
            var reportModel = _iplmRepository.GetPAMReportSummary(userName, isouIds, aeIds, fromDate, toDate, wlmId, aeId, isouId, status, projectAction, page, sort, sortdir, wlmIds
                                                                    , propertyName, fhaNumber, assignedTO, hasOpened, daysOpened);
            reportModel.PAMReportGridlList.ToList().ForEach(r =>
            {
                //r.ProjectActionCompletionDate = TimezoneManager.GetPreferredTimeFromUtc(r.ProjectActionCompletionDate);
                //r.ProjectActionSubmitDate = TimezoneManager.GetPreferredTimeFromUtc(r.ProjectActionSubmitDate);
                r.ProjectActionCompletionDate = r.ProjectActionCompletionDate;
                r.ProjectActionSubmitDate = r.ProjectActionSubmitDate;
            });

            return reportModel;
        }

        /// <summary>
        /// Get all Subordinate AEs of a Work load Manager id
        /// </summary>
        /// <param name="wlmId"></param>
        /// <returns></returns>
        public IList<HUDManagerModel> GetSubordinateAEsByWLMId(string wlmId)
        {
            return _wlmRepositoty.GetSubordinateAEsByWLMId(wlmId);
        }

        public IList<HUDManagerModel> GetSubordinateISOUsByWLMId(string wlmId)
        {
            return _wlmRepositoty.GetSubordinateISOUsByWLMId(wlmId);
        }

        public int GetWlmId(int userId)
        {
            return _iplmRepository.GetWlmId(userId);
        }

        public int GetAeId(int userId)
        {
            return _iplmRepository.GetAeId(userId);
        }

        public int GetIsouId(int userId)
        {
            return _iplmRepository.GetIsouId(userId);
        }

        public ReserveForReplacementFormModel GetR4RRequest(Guid r4rId)
        {
            return _r4rRepository.GetR4RFormById(r4rId);
        }

        public IEnumerable<KeyValuePair<int, string>> GetAllWorkLoadManagers()
        {
            return _wlmRepositoty.GetWorkloadManagers();
        }

        public NonCriticalRepairsViewModel GetNCRRequest(Guid ncrId)
        {
            return _ncrRepository.GetNCRFormById(ncrId);
        }

        public NonCriticalRequestExtensionModel GetNCRExtensionRequest(Guid ncrexId)
        {
            return _ncrExRepository.GetNCRExtensionFormById(ncrexId);
        }

        public PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string aeIds, string isouIds, string wlmIds,
            int? status,
            string projectAction, int wlmId, int aeId, int isouId, DateTime? fromDate, DateTime? toDate)
        {
            //var fromUTCDate = fromDate.HasValue ? TimezoneManager.GetUtcTimeFromPreferred(fromDate.Value) : fromDate;
            //var toUTCDate = toDate.HasValue ? TimezoneManager.GetUtcTimeFromPreferred(toDate.Value) : toDate;
            var fromUTCDate = fromDate.HasValue ? fromDate.Value : fromDate;
            var toUTCDate = toDate.HasValue ? toDate.Value : toDate;
            return _iplmRepository.GetSearchCriteriaDetails(ref reportModel, aeIds, isouIds, wlmIds, status, projectAction, wlmId,
                aeId, isouId, fromUTCDate, toUTCDate);
        }

        public IEnumerable<KeyValuePair<int, string>> GetPamProjectionTypes()
        {
            return _iplmRepository.GetPamProjectActionTypes();
        }

        public ProjectActionViewModel GetProjectActionRequestId(Guid projectActionFormId)
        {
            return _projectActionFormRepository.GetProjectActionRequestId(projectActionFormId);
        }

        public OPAViewModel GetOPARequestId(Guid opaFormId)
        {
            return _oPARepository.GetProjectActionRequestId(opaFormId);
        }
        // User Story 1901
        public string GetDisclaimerMsgByPageType(string pageTypeName)
        {
            return disclaimerMsgRepository.GetDisclaimerMsgByPageType(pageTypeName);
        }
    }
}
