﻿using System;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Repository.Interfaces;
using Repository.ManagementReport;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BusinessService.ProjectAction;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Repository.AssetManagement;
using HUDHealthcarePortal.Repository.Interfaces;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;
using Model;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using BusinessService.Interfaces.Production;

namespace BusinessService.ManagementReport
{
    public class ProductionPAMReportManager : IProductionPAMReportManager
    {
          private IPAMRepository _iplmProdRepository;
          private ILenderPAMRepository _iLenderPamRepository;
          private UnitOfWork unitOfWorkLive;
          private IUserInfoRepository userInfoRepository;
          private ITaskRepository taskRepository;
          private IProd_TaskXrefRepository prodTaskXrefRepository;
          private UnitOfWork uitOfWorkTask;
          private IFormTypeRepository   fromTypeRepository;
          private IPamStatusRepository ipamStatusRepository;
          private IProd_ProjectTypeRepository iProdProjectTypeRepository;
          private IProductionPAMReportRepository prodpamRepository;
          private IFHANumberRequestRepository fhaRequestRepository;
        private PAMReportRepository PAMReportRepository;
        private IHUDPamFiltersRepository HUDPamFiltersRepository;
        private HUDAssetPamFiltersRepository HUDAssetPamFiltersRepository;//naresh 28012020  below code check once


        public ProductionPAMReportManager()
       {
             unitOfWorkLive = new UnitOfWork(DBSource.Live);
              uitOfWorkTask = new UnitOfWork(DBSource.Task); ;
              _iplmProdRepository = new PAMReportRepository(unitOfWorkLive);
              userInfoRepository = new UserInfoRepository(unitOfWorkLive);
              taskRepository = new TaskRepository(uitOfWorkTask);
              prodTaskXrefRepository = new Prod_TaskXrefRepository(uitOfWorkTask);
              fromTypeRepository = new FormTypeRepository(unitOfWorkLive);
              ipamStatusRepository = new PamStatusRepository(unitOfWorkLive);
              iProdProjectTypeRepository = new Prod_ProjectTypeRepository(unitOfWorkLive);
              prodpamRepository = new ProductionPamReportRepository(uitOfWorkTask);
              _iLenderPamRepository = new LenderPAMReportRepository(unitOfWorkLive);
              fhaRequestRepository = new FHANumberRequestRepository(unitOfWorkLive);
            PAMReportRepository = new PAMReportRepository(unitOfWorkLive);
            HUDPamFiltersRepository = new HUDPamFiltersRepository(unitOfWorkLive);
            //naresh 28012020
            HUDAssetPamFiltersRepository = new HUDAssetPamFiltersRepository(unitOfWorkLive);
        }

        //   private IPAMRepository _iplmRepository;
        //private HUDWorkLoadManagerRepository _wlmRepositoty;
        //private UnitOfWork unitOfWorkLive;
        //private IReserveForReplacementRepository _r4rRepository;
        //private INonCriticalRepairsRepository _ncrRepository;
        //private INonCriticalRequestExtensionRepository _ncrExRepository;
        //private  IProjectActionFormRepository  _projectActionFormRepository;
        //private IOPARepository  _oPARepository;
        //private DisclaimerMsgRepository disclaimerMsgRepository;// User Story 1901
        

        //public PAMReportsManager()
        //{
        //    unitOfWorkLive = new UnitOfWork(DBSource.Live);
        //    _iplmRepository = new PAMReportRepository(unitOfWorkLive);
        //    _wlmRepositoty = new HUDWorkLoadManagerRepository();
        //    _r4rRepository = new ReserveForReplacementRepository();
        //    _ncrRepository = new NonCriticalRepairsRepository(unitOfWorkLive);
        //    _ncrExRepository = new NonCriticalRequestExtensionRepository(unitOfWorkLive);
        //    _projectActionFormRepository = new ProjectActionFormRepository(unitOfWorkLive);
        //    _oPARepository = new OPARepository(unitOfWorkLive);
        //    disclaimerMsgRepository = new DisclaimerMsgRepository(unitOfWorkLive);// User Story 1901
        //}

        //public PAMReportModel GetPAMSummary(string userName, string aeIds, DateTime? fromDate, DateTime? toDate, int wlmId, int aeId, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds)
        //{
        //    var reportModel = _iplmRepository.GetPAMReportSummary(userName, aeIds, fromDate, toDate, wlmId, aeId, status, projectAction, page, sort, sortdir, wlmIds);
        //    reportModel.PAMReportGridlList.ToList().ForEach(r =>
        //    {
        //         r.ProjectActionCompletionDate   = TimezoneManager.GetPreferredTimeFromUtc(r.ProjectActionCompletionDate);
        //         r.ProjectActionSubmitDate = TimezoneManager.GetPreferredTimeFromUtc(r.ProjectActionSubmitDate);
        //    });

        //    return reportModel;
        //}

        public IList<UserInfoModel> GetProductionUsers()
        {
            var productionUser = userInfoRepository.GetProductionUsers().ToList();
            return productionUser;
        }

        public  IList<FormTypeModel> GetProductionPageTypes()
        {
           return fromTypeRepository.GetProductionPageTypes(2).ToList();
        }

        public IList<PamStatusViewModel> GetPAMStatusList()
        {
            return ipamStatusRepository.GetAllPamStausesByModuleId(2).ToList();
        }

        public IList<ProjectTypeModel> GetProjectTypeModels()
        {
            return iProdProjectTypeRepository.GetAllProjectTypes();
        }

        public IList<ProjectTypeModel> GetProjectTypesByAppType(string typeIds)
        {
            return iProdProjectTypeRepository.GetProjectTypesByAppType(typeIds);
        }
        public string GetUserInfoByID(int UserID)
        {
            return iProdProjectTypeRepository.GetUserInfoByID(UserID);
        }
        ///// <summary>
        ///// Get all Subordinate AEs of a Work load Manager id
        ///// </summary>
        ///// <param name="wlmId"></param>
        ///// <returns></returns>
        //public IList<HUDManagerModel> GetSubordinateAEsByWLMId(string wlmId)
        //{
        //    return _wlmRepositoty.GetSubordinateAEsByWLMId(wlmId);
        //}

        //public int GetWlmId(int userId)
        //{
        //    return _iplmRepository.GetWlmId(userId);
        //}

        //public int GetAeId(int userId)
        //{
        //    return _iplmRepository.GetAeId(userId);
        //}

        //public ReserveForReplacementFormModel GetR4RRequest(Guid r4rId)
        //{
        //    return _r4rRepository.GetR4RFormById(r4rId);
        //}

        //public IEnumerable<KeyValuePair<int, string>> GetAllWorkLoadManagers()
        //{
        //    return _wlmRepositoty.GetWorkloadManagers();
        //}

        //public NonCriticalRepairsViewModel GetNCRRequest(Guid ncrId)
        //{
        //    return _ncrRepository.GetNCRFormById(ncrId);
        //}

        //public NonCriticalRequestExtensionModel GetNCRExtensionRequest(Guid ncrexId)
        //{
        //    return _ncrExRepository.GetNCRExtensionFormById(ncrexId);
        //}

        //public PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string aeIds, string wlmIds,
        //    int? status,
        //    string projectAction, int wlmId, int aeId, DateTime? fromDate, DateTime? toDate)
        //{
        //    var fromUTCDate = fromDate.HasValue ? TimezoneManager.GetUtcTimeFromPreferred(fromDate.Value) : fromDate;
        //    var toUTCDate = toDate.HasValue ? TimezoneManager.GetUtcTimeFromPreferred(toDate.Value) : toDate;
        //    return _iplmRepository.GetSearchCriteriaDetails(ref reportModel, aeIds, wlmIds, status, projectAction, wlmId,
        //        aeId, fromUTCDate, toUTCDate);
        //}

        //public IEnumerable<KeyValuePair<int, string>> GetPamProjectionTypes()
        //{
        //    return _iplmRepository.GetPamProjectActionTypes();
        //}

        //public ProjectActionViewModel GetProjectActionRequestId(Guid projectActionFormId)
        //{
        //    return _projectActionFormRepository.GetProjectActionRequestId(projectActionFormId);
        //}

        //public OPAViewModel  GetOPARequestId(Guid opaFormId)
        //{
        //    return _oPARepository.GetProjectActionRequestId(opaFormId);
        //}
        //// User Story 1901
        //public string GetDisclaimerMsgByPageType(string pageTypeName)
        //{
        //    return disclaimerMsgRepository.GetDisclaimerMsgByPageType(pageTypeName);
        //}



        public ProductionPAMReportModel GetProductionPAMReportSummary(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate, string status, string Level, Guid? grouptaskinstanceid, string Mode = "Other")
        {
            return prodpamRepository.GetProductionPAMReportSummary(appType, programType, prodUserIds, fromDate, toDate, status, Level, grouptaskinstanceid, Mode);
        }

        public ProductionPAMReportModel GetProductionLenderPAMReportSummary(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes)
        {
            return prodpamRepository.GetProductionLenderPAMReportSummary ( lamIds,  bamIds,  larIds,  roleIds,  fromDate,  toDate,  status,  appType, programTypes);
        }
        public ProductionPAMReportModel GetLenderPAMSubGridChildTasks(Guid taskInstanceId)
        {
            return prodpamRepository.GetLenderPAMSubGridChildTasks( taskInstanceId);
        }
         public ProductionPAMReportModel GetProductionLenderPAMExcel(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes)
        {
            return prodpamRepository.GetProductionLenderPAMExcel(lamIds, bamIds, larIds, roleIds, fromDate, toDate, status, appType, programTypes);
        }
      

        public IEnumerable<UserInfoModel> GetLenderUsersByRoleLenderId(int lenderId, string roleName)
        {
            return _iLenderPamRepository.GetLenderUsersByRoleLenderId(lenderId, roleName);
        }

         public IEnumerable<KeyValuePair<int, string>> GetUserNameByIDs(string userIDList)
        {
            return _iLenderPamRepository.GetUserNameByIDs(userIDList);
        }

        public IList<FHANumberRequestViewModel> GetAllFHARequests()
        {
            return fhaRequestRepository.GetAllFHARequests();
        }

        public PAMReportTestGrid1ViewModel GetProdPAMGridOneResults(Guid taskInstanceId)
        {
            return prodpamRepository.GetProdPAMGridOneResults(taskInstanceId);
        }

        public PAMReportTestGrid2ViewModel GetProdPAMGridTwoResults(Guid taskInstanceId)
        {
            return prodpamRepository.GetProdPAMGridTwoResults(taskInstanceId);
        }

        public IList<PAMReportV3ViewModel> GetProdHUDPAMReportResults()
        {
            return prodpamRepository.GetProdHUDPAMReportResults();
        }

		/// <summary>
		/// Prod Pam report - sub tasks 
		/// </summary>
		/// <param name="pTaskInstanceId"></param>
		/// <param name="pFHANumber"></param>
		/// <returns></returns>
		public IList<PAMReportV3ViewModel> GetProdHUDPAMSubReportResults(Guid pTaskInstanceId, string pFHANumber)
		{//skumar-2838
			return prodpamRepository.GetProdHUDPAMSubReportResults(pTaskInstanceId, pFHANumber);
		}

		public ProdPAMReportStatusGridViewModel GetProdPAMReportStatusGridResults()
        {
            return prodpamRepository.GetProdPAMReportStatusGridResults();
        }
        public string SaveHUDPamFilters(HUDPamReportFiltersModel Model)
        {
            return HUDPamFiltersRepository.SaveHUDPamFilters(Model);
        }
        public HUDPamReportFiltersModel GetSavedFilter()
        {
            return HUDPamFiltersRepository.GetSavedFilter();
        }

        //naresh add 28012020
        public HUDAssetpamreportfiltermodel GetAssetSavedFilter()
        {
            return HUDAssetPamFiltersRepository.GetAssetSavedFilter();
        }

        //naresh add 28012020
        public string SaveAssetPamReportFilterDetails(HUDAssetpamreportfiltermodel model)
        {
            return HUDAssetPamFiltersRepository.SaveAssetPamReportFilterDetails(model);

        }

        // harish added 15042020 to get lender based on lender id 

        public IList<UserInfoModel> GetAllLenderUsers(int? lenderid)
        {
            var productionUser = userInfoRepository.GetAllLenderUsers(lenderid).ToList();
            return productionUser;
        }


    }
}
