﻿using System.Configuration;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Model.AssetManagement;
using Repository.Interfaces;
using Core.Utilities;
using System.Web.Mvc;
using BusinessService;
using Model;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using BusinessService.ProjectAction;
using Core;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using System.IO;

namespace HUDHealthcarePortal.BusinessService
{
    public class EmailManager : IEmailManager
    {
        private readonly IEmailRepository emailRepository;
        private readonly ILookupManager lookupMgr;
        private readonly IAccountManager accountMgr;
        private readonly IEmailTemplateMessagesManager _emailTemplateMessagesManager;
        private readonly IHUDAccountExecutiveRepository _hudAccountExecutiveRepository;
        private readonly IProd_ProjectTypeRepository projectTypeRepository;
        private readonly IUserRepository userRepository;
        private readonly UnitOfWork unitOfWorkLive;
        private IProjectActionFormManager _projectActionFormManager;
        private IPDRUserPreferenceManager _pdrUserPreferenceManager;
        private IProd_SharepointScreenManager sharepointScreenManager;
        private IProd_TaskXrefManager prod_TaskXrefManager;
        private IProductionQueueManager productionQueue;
        private readonly string _notificationEmailAddress =
            ConfigurationManager.AppSettings["NotificationEmailAddress"] ??
            ConfigurationSettings.AppSettings["NotificationEmailAddress"];

        private readonly string _commentsEmailAddress = ConfigurationManager.AppSettings["CommentsEmailAddress"] ??
                                                        ConfigurationSettings.AppSettings["CommentsEmailAddress"];

        public string C3SupportEmail = ConfigurationManager.AppSettings["C3SupportEmailAddress"] ??
                                       ConfigurationSettings.AppSettings["C3SupportEmailAddress"];
        public string HCPContactUSEmail = ConfigurationManager.AppSettings["HCPContactUSEmail"] ??
                                      ConfigurationSettings.AppSettings["HCPContactUSEmail"];
        private readonly string HHcPEmail = ConfigurationManager.AppSettings["HHcPEmailAddress"] ??
                                            ConfigurationSettings.AppSettings["HHcPEmailAddress"];

        private readonly bool _isAeEmailAlertEnabled =
            Convert.ToBoolean(ConfigurationManager.AppSettings["IsAEEmailAlertEnabled"] ??
                              ConfigurationSettings.AppSettings["IsAEEmailAlertEnabled"]);
        private IProd_RestfulWebApiTokenRequest webApiTokenRequest;
        private IProd_RestfulWebApiDownload webApiDownload;
        private ITaskManager taskManager;
        private ITaskFile_FolderMappingManager taskFileFolderMap;
        public EmailManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            emailRepository = new EmailRepository(unitOfWorkLive);
            lookupMgr = new LookupManager();
            accountMgr = new AccountManager();
            _emailTemplateMessagesManager = new EmailTemplateMessagesManager();
            _hudAccountExecutiveRepository = new HUDAccountExecutiveRepository(unitOfWorkLive);
            projectTypeRepository = new Prod_ProjectTypeRepository(unitOfWorkLive);
            userRepository = new UserRepository(unitOfWorkLive);
            _projectActionFormManager = new ProjectActionFormManager();
            sharepointScreenManager = new Prod_SharepointScreenManager();
            _pdrUserPreferenceManager = new PDRUserPreferenceManager();
            prod_TaskXrefManager = new Prod_TaskXrefManager();
            productionQueue = new ProductionQueueManager();
            webApiTokenRequest = new Prod_RestfulWebApiTokenRequest();
            webApiDownload = new Prod_RestfulWebApiDownload();
            taskManager = new TaskManager();
            taskFileFolderMap = new TaskFile_FolderMappingManager();
        }

        /// <summary>
        /// change later to return bool, false/true indicate success save or not
        /// </summary>
        /// <param name="email"></param>
        public void SaveEmail(EmailModel email)
        {
            try
            {
                emailRepository.SaveEmail(email);
                unitOfWorkLive.Save();
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(
                    string.Format("Error occurred while saving email {0}", email.ToString()), e.InnerException);
            }
        }

        public bool SendRegisterUserEmail(UserViewModel model, string htmlEmbedLink)
        {
            try
            {
                MailMessage mail = new MailMessage();

                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(model.EmailAddress);
                mail.Subject = "Please complete registration of your account for HUD Healthcare Portal";
                mail.Body =
                    string.Format(
                        @"Your account is created with username: {0}, password: {1} <br/><br/>Please click the <a href='{2}'>link</a> to complete registration.<br/><br/>Please do not reply to this message.",
                        model.EmailAddress, model.Password, htmlEmbedLink);
                mail.IsBodyHtml = true;

                var emailToSave = new EmailModel();
                emailToSave.EmailTo = TextUtils.TokenDelimitedText(mail.To.ToList().Select(p => p.Address), ";");
                emailToSave.EmailFrom = mail.From.Address;
                emailToSave.Subject = mail.Subject;
                emailToSave.ContentHtml = emailToSave.ContentText = mail.Body;
                emailToSave.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "REGISTER").EmailTypeId;
                var emailMgr = new EmailManager();
                emailMgr.SaveEmail(emailToSave);
                smtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendResetPasswordEmail(string userName, string htmlEmbedLink)
        {
            try
            {
                if (string.IsNullOrEmpty(userName))
                    return false;
                MailMessage mail = new MailMessage();

                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(userName);
                mail.Subject = "Please reset your password within 48 hours upon receiving this email";
                mail.Body =
                    string.Format(
                        @"To reset your password, <a href='{0}'> please click the link and enter new password.</a><br/><br/>Please do not reply to this message.",
                        htmlEmbedLink);
                mail.IsBodyHtml = true;

                var emailToSave = new EmailModel();
                emailToSave.EmailTo = TextUtils.TokenDelimitedText(mail.To.ToList().Select(p => p.Address), ";");
                emailToSave.EmailFrom = mail.From.Address;
                emailToSave.Subject = mail.Subject;
                emailToSave.ContentHtml = emailToSave.ContentText = mail.Body;
                emailToSave.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PWDRC").EmailTypeId;
                var emailMgr = new EmailManager();
                emailMgr.SaveEmail(emailToSave);

                smtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool SendCommentEmail(Dictionary<string, string> authors, string replyToUserName, string subject,
            string htmlEmailContent, string rawUrl)
        {
            string replyToCommentsLink = "<p>Please <a href='" + rawUrl +
                                         "'>click here</a> to add your response to the comment thread. </p><br/><br/>Please do not reply to this message.";
            try
            {
                if (authors == null || authors.Count() == 0 || string.IsNullOrEmpty(replyToUserName))
                    return false;
                MailMessage mail = new MailMessage();

                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_commentsEmailAddress);
                mail.To.Add(replyToUserName);
                foreach (var email in authors.Keys)
                {
                    if (email != replyToUserName)
                        mail.CC.Add(email);
                }
                mail.Subject = subject;
                mail.Body = htmlEmailContent + replyToCommentsLink;
                mail.IsBodyHtml = true;

                var emailToSave = new EmailModel();
                emailToSave.EmailTo = TextUtils.TokenDelimitedText(mail.To.ToList().Select(p => p.Address), ";");
                emailToSave.EmailCC = TextUtils.TokenDelimitedText(mail.CC.ToList().Select(p => p.Address), ";");
                emailToSave.EmailFrom = mail.From.Address;
                emailToSave.Subject = mail.Subject;
                emailToSave.ContentHtml = emailToSave.ContentText = mail.Body;
                emailToSave.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "COMMENT").EmailTypeId;
                var emailMgr = new EmailManager();
                emailMgr.SaveEmail(emailToSave);

                smtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void SendUploadNotificationEmailsDelegate(Object threadContext)
        {
            var fhaNumbers = threadContext as IEnumerable<string>;
            if (fhaNumbers == null)
                throw new InvalidCastException("threadContext is not IEnumrable<string> type for fha numbers");
            SendUploadNotificationEmails(fhaNumbers);
        }

        public bool SendSubmitNotificationEmails(string fhaNumber, string senderEmail, string receiverEmail,
            string myTaskUrl)
        {
            try
            {
                var dictAEFhaMap = GetAEEmailsByFhaNumbers(new String[] { fhaNumber });
                var dictValue = dictAEFhaMap[dictAEFhaMap.Keys.First()];
                Dictionary<string, string> lenders = new Dictionary<string, string>();
                foreach (var item in dictValue.Keys)
                {
                    // dictinct lenders
                    if (!lenders.ContainsKey(dictValue[item]))
                        lenders.Add(dictValue[item], dictValue[item]);
                }
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(receiverEmail);

                // get AE email address and send email to AE
                if (dictAEFhaMap.Keys.Any())
                {
                    //AE Email Alert is disabled currently as per HUD Request
                    if (_isAeEmailAlertEnabled)
                    {
                        mail.To.Add(dictAEFhaMap.Keys.First());
                    }
                }
                var userInfo = accountMgr.GetUserByUsername(receiverEmail);
                // if this AE does not exist in db, use eastern standard time zone, as HUD locates in DC
                var timeZoneNameId = userInfo == null ? "3" : userInfo.SelectedTimezoneId;
                string timeZoneName = EnumUtils.Parse<HUDTimeZone>(timeZoneNameId).ToName();

                // must be one singular property for submission
                mail.Subject = string.Format("{0}; FHA Number: {1}; Submission: {2}",
                    TextUtils.TokenDelimitedText(lenders.Keys, ","), TextUtils.TokenDelimitedText(dictValue.Keys, ","),
                    TimezoneManager.FormatDate(TimezoneManager.GetPreferredTimeFromUtc(DateTime.UtcNow, timeZoneName),
                        "{0:MM/dd/yyyy}"));
                mail.Body =
                    string.Format(
                        "{0}; FHA Number: {1} has been submitted. Please log into the 232 HUD Healthcare Portal to complete processing or <a href='{2}'>click here</a>.<br/><br/>Please do not reply to this message.",
                        TextUtils.TokenDelimitedText(lenders.Keys, ","),
                        TextUtils.TokenDelimitedText(dictValue.Keys, ","), myTaskUrl);

                mail.IsBodyHtml = true;

                var emailToSave = new EmailModel();
                emailToSave.EmailTo = receiverEmail;
                emailToSave.EmailFrom = _notificationEmailAddress;
                emailToSave.Subject = mail.Subject;
                emailToSave.ContentHtml = emailToSave.ContentText = mail.Body;
                emailToSave.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "UPLDNOTIFY").EmailTypeId;
                var emailMgr = new EmailManager();
                emailMgr.SaveEmail(emailToSave);

                smtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendUploadNotificationEmails(IEnumerable<string> fhaNumbers)
        {
            try
            {
                var dictAEFhaMap = GetAEEmailsByFhaNumbers(fhaNumbers);
                foreach (var aeEmail in dictAEFhaMap.Keys)
                {
                    var dictValue = dictAEFhaMap[aeEmail];
                    Dictionary<string, string> lenders = new Dictionary<string, string>();
                    foreach (var item in dictValue.Keys)
                    {
                        // dictinct lenders
                        if (!lenders.ContainsKey(dictValue[item]))
                            lenders.Add(dictValue[item], dictValue[item]);
                    }
                    MailMessage mail = new MailMessage();
                    SmtpClient smtpServer = new SmtpClient();
                    mail.From = new MailAddress(_notificationEmailAddress);
                    //Dictionary<string, string> dictHudTest = new Dictionary<string, string>();
                    //dictHudTest.Add("April.j.edmunds@hud.gov", "April.j.edmunds@hud.gov");
                    //dictHudTest.Add("Jeremiah.Bitting@hud.gov", "Jeremiah.Bitting@hud.gov");
                    //dictHudTest.Add("Patricia.V.FukudaLewis@hud.gov", "Patricia.V.FukudaLewis@hud.gov");
                    //dictHudTest.Add("Tiffani.W.Tyer@hud.gov", "Tiffani.W.Tyer@hud.gov");
                    //dictHudTest.Add("Vance.T.Morris@hud.gov", "Vance.T.Morris@hud.gov");
                    //AE Email Alert is disabled currently as per HUD Request

                    mail.To.Add(aeEmail);
                    var userInfo = accountMgr.GetUserByUsername(aeEmail);
                    // if this AE does not exist in db, use eastern standard time zone, as HUD locates in DC
                    var timeZoneNameId = userInfo == null ? "3" : userInfo.SelectedTimezoneId;
                    string timeZoneName = EnumUtils.Parse<HUDTimeZone>(timeZoneNameId).ToName();
                    //if (dictHudTest.ContainsKey(aeEmail))
                    //{
                    //    mail.To.Add(aeEmail);
                    //    var userInfo = accountMgr.GetUserByUsername(aeEmail);
                    //    // if this AE does not exist in db, use eastern standard time zone, as HUD locates in DC
                    //    var timeZoneNameId = userInfo == null ? "3" : userInfo.SelectedTimezoneId;
                    //    timeZoneName = EnumUtils.Parse<HUDTimeZone>(timeZoneNameId).ToName();
                    //}
                    //else
                    //{
                    //    var userInfo = accountMgr.GetUserByUsername("cgan@c3-systems.com");
                    //    mail.To.Add("cgan@c3-systems.com");
                    //    mail.CC.Add("srahman@c3-systems.com");
                    //    mail.CC.Add("clee@c3-systems.com");
                    //    //timeZoneName = "Eastern Standard Time";
                    //    timeZoneName = EnumUtils.Parse<HUDTimeZone>(userInfo.SelectedTimezoneId).ToName();
                    //}
                    // plural and singular properties
                    if (lenders.Keys.Count > 1)
                    {
                        mail.Subject = string.Format("{0}; FHA Numbers: {1}; Upload: {2}",
                            TextUtils.TokenDelimitedText(lenders.Keys, ","),
                            TextUtils.TokenDelimitedText(dictValue.Keys, ","),
                            TimezoneManager.FormatDate(
                                TimezoneManager.GetPreferredTimeFromUtc(DateTime.UtcNow, timeZoneName), "{0:MM/dd/yyyy}"));
                        mail.Body =
                            string.Format(
                                "{0}; FHA Numbers: {1} have been uploaded.<br/><br/>Please do not reply to this message.",
                                TextUtils.TokenDelimitedText(lenders.Keys, ","),
                                TextUtils.TokenDelimitedText(dictValue.Keys, ","));
                    }
                    else
                    {
                        mail.Subject = string.Format("{0}; FHA Number: {1}; Upload: {2}",
                            TextUtils.TokenDelimitedText(lenders.Keys, ","),
                            TextUtils.TokenDelimitedText(dictValue.Keys, ","),
                            TimezoneManager.FormatDate(
                                TimezoneManager.GetPreferredTimeFromUtc(DateTime.UtcNow, timeZoneName), "{0:MM/dd/yyyy}"));
                        mail.Body =
                            string.Format(
                                "{0}; FHA Number: {1} has been uploaded.<br/><br/>Please do not reply to this message.",
                                TextUtils.TokenDelimitedText(lenders.Keys, ","),
                                TextUtils.TokenDelimitedText(dictValue.Keys, ","));
                    }
                    mail.IsBodyHtml = true;

                    var emailToSave = new EmailModel();
                    emailToSave.EmailTo = aeEmail;
                    emailToSave.EmailFrom = _notificationEmailAddress;
                    emailToSave.Subject = mail.Subject;
                    emailToSave.ContentHtml = emailToSave.ContentText = mail.Body;
                    emailToSave.MailTypeId =
                        lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "UPLDNOTIFY").EmailTypeId;
                    var emailMgr = new EmailManager();
                    emailMgr.SaveEmail(emailToSave);

                    smtpServer.Send(mail);

                }
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }
        /// <summary>
        /// dictionary is in key: aeEmail, value: dictionary of key: fha number, value: lender name
        /// </summary>
        /// <param name="fhaNumbers"></param>
        /// <returns></returns>
        public Dictionary<string, Dictionary<string, string>> GetAEEmailsByFhaNumbers(IEnumerable<string> fhaNumbers)
        {
            Dictionary<string, Dictionary<string, string>> dictAEFhaMap =
                new Dictionary<string, Dictionary<string, string>>();
            var hudMgrRepo = new HUDManagerRepository();
            foreach (string fhaNumber in fhaNumbers)
            {
                var tuple = hudMgrRepo.GetHUDManagersByFhaNumber(fhaNumber);
                // second is AE
                string aeEmail = tuple.Item2.AddressModel.Email;
                // third is lender name
                string lenderName = tuple.Item3;
                if (!dictAEFhaMap.ContainsKey(aeEmail))
                    dictAEFhaMap.Add(aeEmail, new Dictionary<string, string>() { { fhaNumber, lenderName } });
                else if (!dictAEFhaMap[aeEmail].ContainsKey(fhaNumber))
                    dictAEFhaMap[aeEmail].Add(fhaNumber, lenderName);
            }
            return dictAEFhaMap;
        }

        public bool SendR4RAutoApprovalNotificationEmail(string aeEmail, string requestorEmail,
            ReserveForReplacementFormModel model)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                var hudMgrRepo = new HUDManagerRepository();
                var tuple = hudMgrRepo.GetHUDManagersByFhaNumber(model.FHANumber);
                var aeName = tuple.Item2.Manager_Name;
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(requestorEmail);
                mail.To.Add(aeEmail);
                string ServicerRemarks = string.IsNullOrEmpty(model.ServicerRemarks) ? "<p>Comments: None</p>" : "<p>Comments: " + model.ServicerRemarks + "</p>";
                if (model.IsLenderDelegate == true)
                {
                    mail.Subject = "Lender Delegate Approval Notification for " + model.PropertyName + ", " + model.FHANumber;
                    mail.Body = "<p>Property Name " + model.PropertyName + ", FHA Number " + model.FHANumber +
                            string.Format(", was processed in accordance with the Multifamily Housing Lender Delegated Notice H2013-14 for Optional Lender Review and Delegated Approval of Reserve for Replacement Escrow administration.</p>" +
                                                  "<p>This request has been Approved. The requested amount for the Reserve for Replacement is {0:c}. The approved amount for the Reserve for Replacement refund is {1:c}.</p>" +
                                                  "<p>Please contact your Servicing Representative for any further questions regarding this request.</p>", model.TotalRequestedAmount, model.TotalApprovedAmount) + ServicerRemarks;
                }
                else
                {
                    mail.Subject = "Auto Approval Notification for " + model.PropertyName + ", " + model.FHANumber;
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                                "<p>FHA Number: " + model.FHANumber + "</p>" + "<p>" +
                                string.Format(
                                    "Your Reserve for Replacement request in the amount of {0:c} Submitted on {1} has been auto-approved on {2:MM/dd/yyyy}" +
                                    " in the amount of {3:c}. Please maintain a copy of this for proof of approval." +
                                    "<br>" +
                                    " Please feel free to contact your Account Executive {4} at {5} if you should have any questions regarding this project.",
                                    model.TotalRequestedAmount,
                                    TimezoneManager.FormatDate(model.ServicerSubmissionDate, "{0:MM/dd/yyyy}"),
                                    DateTime.Now, model.TotalRequestedAmount, aeName, aeEmail) + ".</p>" +
                                ServicerRemarks;
                }

                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "AUTOAPPR").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = aeEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "AUTOAPPR").EmailTypeId;
                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendNonCriticalRequestExtensionApprovalNotificationEmail(string aeEmail, string requestorEmail, NonCriticalRequestExtensionModel model)
        {
            try
            {
                //  ITaskManager taskMgr= new TaskManager();
                //  Guid? TaskInstanceid;
                //  var newtask = taskMgr.GetTaskById((int)model.TaskId);
                //  var ServicerNotes=string.Empty;

                // TaskInstanceid=  newtask.TaskInstanceId;
                // var TaskInstancelist = taskMgr.GetTasksByTaskInstanceId((Guid)TaskInstanceid);

                //if (TaskInstancelist != null)
                //{
                //    var result = TaskInstancelist.OrderByDescending(a => a.TaskId);


                //    ServicerNotes = result.ToList()[1].Notes;
                //}
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                var hudMgrRepo = new HUDManagerRepository();
                var tuple = hudMgrRepo.GetHUDManagersByFhaNumber(model.FHANumber);
                var aeName = tuple.Item2.Manager_Name;
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(requestorEmail);
                mail.To.Add(aeEmail);

                var dateSubmitted = string.Format("{0:MM/dd/yyyy}", model.CreatedOn);
                var approvedDate = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
                string comments = string.IsNullOrEmpty(model.Comments) ? "<p>Comments: None</p>" : "<p>Comments: " + model.Comments + "</p>";
                if (model.ExtensionRequestStatus == (int)RequestExtensionStatus.Approve)
                {
                    aeName = UserPrincipal.Current.FullName;
                    aeEmail = UserPrincipal.Current.UserName;
                    mail.Subject = "Non-Critical Request Escrow Extension Approval Notification for " + model.PropertyName + ", " + model.FHANumber;
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                    "<p>FHA Number: " + model.FHANumber + "</p>" + "<p>" +
                    "Your Non-Critical Request Escrow Extension  Submitted on " + dateSubmitted +
                    " has been Approved on " + approvedDate + " by  " + aeName + " for a period of 6 months." +
                    " Please maintain a copy of this for proof of approval.</p>" +
                     string.Format("<p>Please feel free to contact your {0} {1} at {2}.</p>", UserPrincipal.Current.UserRole, aeName, aeEmail) + comments;
                }
                else if (model.ExtensionRequestStatus == (int)RequestExtensionStatus.Deny)
                {
                    aeName = UserPrincipal.Current.FullName;
                    aeEmail = UserPrincipal.Current.UserName;
                    mail.Subject = "Non-Critical Request Escrow Extension Denial Notification for " + model.PropertyName + ", " + model.FHANumber;
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                    "<p>FHA Number: " + model.FHANumber + "</p>" + "<p>" +
                    "Your Non-Critical Request Escrow  Extension " + dateSubmitted +
                    " has been denied on " + approvedDate + " by  " + aeName + " for a period of 6 months " +
                    " Please maintain a copy of this for proof of denial.</p>" +
                    string.Format("<p>Please feel free to contact your {0} {1} at {2}.</p>", UserPrincipal.Current.UserRole, aeName, aeEmail) + comments;
                }
                else if (model.ExtensionRequestStatus == (int)RequestExtensionStatus.Submit)
                {
                    mail.Subject = "Non-Critical Request Escrow Extension Submit Notification for " + model.PropertyName + ", " + model.FHANumber;
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                    "<p>FHA Number: " + model.FHANumber + "</p>" + "<p>" +
                    "Your Non-Critical Request Escrow Extension  Submitted on " + dateSubmitted + "</p>" +
                    string.Format("<p>Please feel free to contact your Account Executive {0} at {1}.</p>", aeName, aeEmail) + comments;
                }

                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "NCRRtExten").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = aeEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "NCRRtExten").EmailTypeId;
                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendNonCriticalRequestApprovalNotificationEmail(string aeEmail, string requestorEmail, NonCriticalRepairsViewModel model)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                var hudMgrRepo = new HUDManagerRepository();
                var tuple = hudMgrRepo.GetHUDManagersByFhaNumber(model.FHANumber);
                var aeName = tuple.Item2.Manager_Name;
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(requestorEmail);
                mail.To.Add(aeEmail);

                var paymentRequestedAmount = model.PaymentAmountRequested.ToString("C2");
                var accpetedAmount = model.ApprovedAmount != null ? string.Format("{0:C2}", model.ApprovedAmount) : "$0.00";
                var dateSubmitted = string.Format("{0:MM/dd/yyyy}", model.SubmittedDate.Value);
                var approvedDate = string.Format("{0:MM/dd/yyyy}", DateTime.Now);

                if (model.RequestStatus == (int)RequestStatus.ApproveWithChanges)
                {
                    aeName = UserPrincipal.Current.FullName;
                    aeEmail = UserPrincipal.Current.UserName;
                    string reason = String.IsNullOrEmpty(model.Reason) ? "<p>Comments: None</p>" : "<p>Comments: " + model.Reason + "</p>";
                    mail.Subject = "Non-Critical Repair Request Escrow Approval Notification for " + model.PropertyName + ", " + model.FHANumber + ", Draw Number: " + model.NumberDraw;
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                    "<p>FHA Number: " + model.FHANumber + "</p>" +
                    "<p>Your Non-Critical request escrow in the amount of " + paymentRequestedAmount + " Submitted on " + dateSubmitted +
                  " has been approved on " + approvedDate + " by  " + aeName + " for the amount of " + accpetedAmount + "." +
                  " Please maintain a copy of this for proof of approval.</p>" +
                    string.Format("<p>Please feel free to contact your {0} {1} at {2}.</p>", UserPrincipal.Current.UserRole, aeName, aeEmail) + reason;
                }
                else if (model.RequestStatus == (int)RequestStatus.AutoApprove)
                {
                    string ServicerRemarks = String.IsNullOrEmpty(model.ServicerRemarks) ? "<p>Comments: None</p>" : "<p>Comments: " + model.ServicerRemarks + "</p>";
                    if (model.IsLenderDelegate == true)
                    {
                        mail.Subject = "Lender Delegate Approval Notification for " + model.PropertyName + ", " + model.FHANumber + ", Draw Number: " + model.NumberDraw;
                        mail.Body = "<p>Property Name " + model.PropertyName + ", FHA Number " + model.FHANumber +
                            string.Format(", was processed in accordance with the Multifamily Housing Lender Delegated Notice H2013-14 for Optional Lender Review and Delegated Approval of Non-Critical Repair Escrow administration.</p>" +
                                                  "<p>This request has been Approved. The requested amount of the Non-Critical Repair Escrow refund is {0:c}. The approved amount for the Non-Critical Repair Escrow refund is {1:c}.</p>" +
                                                  "<p>Please contact your Servicing Representative for any further questions regarding this request.</p>", paymentRequestedAmount, accpetedAmount) + ServicerRemarks;
                    }
                    else
                    {
                        mail.Subject = "Non-Critical Repair Request Escrow Auto Approval Notification for " + model.PropertyName + ", " + model.FHANumber + ", Draw Number: " + model.NumberDraw;
                        mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                        "<p>FHA Number: " + model.FHANumber + "</p>" +
                        "<p>Your Non-Critical request escrow in the amount of " + paymentRequestedAmount + " Submitted on " + dateSubmitted +
                      " has been approved on " + approvedDate + "." +
                      " Please maintain a copy of this for proof of approval.</p>" +
                        string.Format("<p>Please feel free to contact your Account Executive {0} at {1}.</p>", aeName, aeEmail) + ServicerRemarks;
                    }
                }
                else if (model.RequestStatus == (int)RequestStatus.Deny)
                {
                    aeName = UserPrincipal.Current.FullName;
                    aeEmail = UserPrincipal.Current.UserName;
                    string reason = String.IsNullOrEmpty(model.Reason) ? "<p>Comments: None" : "<p>Comments: " + model.Reason + "</p>";
                    mail.Subject = "Non-Critical Repair Request Escrow Denial Notification for " + model.PropertyName + ", " + model.FHANumber + ", Draw Number: " + model.NumberDraw;
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                    "<p>FHA Number: " + model.FHANumber + "</p>" +
                    "<p>Your Non-Critical request escrow in the amount of " + paymentRequestedAmount + " Submitted on " + dateSubmitted +
                  " has been denied on " + TimezoneManager.FormatDate(model.ModifiedOn.Value, "{0:MM/dd/yyyy}") + "." +
                  " Please maintain a copy of this for proof of denial.</p>" +
                    string.Format("<p>Please feel free to contact your {0} {1} at {2}.</p>", UserPrincipal.Current.UserRole, aeName, aeEmail) + reason;
                }
                else if (model.RequestStatus == (int)RequestStatus.Submit)
                {
                    string ServicerRemarks = String.IsNullOrEmpty(model.ServicerRemarks) ? "<p>Comments: None</p>" : "<p>Comments: " + model.ServicerRemarks + "</p>";
                    mail.Subject = "Non Critical Request Escrow Submit Notification for " + model.PropertyName + ", " + model.FHANumber + ", Draw Number: " + model.NumberDraw;
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                                "<p>FHA Number: " + model.FHANumber + "</p>" +
                                "<p>Your request has been Forwarded to a HUD AE for further review." + string.Format(" Please feel free to contact your Account Executive {0} at {1}.</p>", aeName, aeEmail) + ServicerRemarks;
                }
                else
                {
                    aeName = UserPrincipal.Current.FullName;
                    aeEmail = UserPrincipal.Current.UserName;
                    string reason = String.IsNullOrEmpty(model.Reason) ? "<p>Comments: None" : "<p>Comments: " + model.Reason + "</p>";
                    mail.Subject = "Non-Critical Repair Request Escrow Approval Notification for " + model.PropertyName + ", " + model.FHANumber + ", Draw Number: " + model.NumberDraw;
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                    "<p>FHA Number: " + model.FHANumber + "</p>" +
                    "<p>Your Non-Critical request escrow in the amount of " + paymentRequestedAmount + " Submitted on " + dateSubmitted +
                  " has been approved on " + approvedDate + " by  " + aeName + " for the amount of " + paymentRequestedAmount + "." +
                  " Please maintain a copy of this for proof of approval.</p>" +
                    string.Format("<p>Please feel free to contact your {0} {1} at {2}.</p>", UserPrincipal.Current.UserRole, aeName, aeEmail) + reason;
                }


                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "NonCrAPPR").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = aeEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "NonCrAPPR").EmailTypeId;
                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendMissingFhaNotificationEmail(MissingFhaNumberEmailModel model)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(HHcPEmail);
                mail.Subject = "Missing FHA Number Notification";
                mail.Body = string.Format("<table>" +
                                          "<tr><td><b>Subject</b></td>" +
                                          "<td><b>FHA Number Not Found</b></td></tr>" +
                                          "<tr><td>User Name</td>" +
                                          "<td>{0}</td></tr>" +
                                          "<tr><td>FHA Number</td>" +
                                          "<td>{1}</td></tr>" +
                                          "<tr><td>User Email address</td>" +
                                          "<td>{2}</td></tr>" +
                                          "<tr><td>Property Name</td>" +
                                          "<td>{3}</td></tr>" +
                                          "<tr><td>Street Address</td>" +
                                          "<td>{4}</td></tr>" +
                                          "<tr><td>City</td>" +
                                          "<td>{5}</td></tr>" +
                                          "<tr><td>State</td>" +
                                          "<td>{6}</td></tr>" +
                                          "<tr><td>Zip Code</td>" +
                                          "<td>{7}</td></tr>" +
                                          "</table>", model.UserName, model.FhaNumber, model.EmailAddress,
                    model.PropertyName, model.Address.AddressLine1, model.Address.City, model.Address.StateCode,
                    model.Address.ZIP);


                mail.IsBodyHtml = true;

                var emailToHHcP = new EmailModel();
                emailToHHcP.EmailFrom = _notificationEmailAddress;
                emailToHHcP.Subject = mail.Subject;
                emailToHHcP.ContentHtml = emailToHHcP.ContentText = mail.Body;
                emailToHHcP.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "MISNGFHA").EmailTypeId;
                emailToHHcP.EmailTo = HHcPEmail;
                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToHHcP);

                smtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendR4RSubmitNotificationEmail(string aeEmail, string requestorEmail, ReserveForReplacementFormModel model)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                var hudMgrRepo = new HUDManagerRepository();
                var aeName = hudMgrRepo.GetHUDManagersByFhaNumber(model.FHANumber).Item2.Manager_Name;
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(requestorEmail);
                mail.To.Add(aeEmail);

                string ServicerRemarks = String.IsNullOrEmpty(model.ServicerRemarks) ? "<p>Comments: None</p>" : "<p>Comments: " + model.ServicerRemarks + "</p>";
                mail.Subject = "R4R Form Submit Notification for " + model.PropertyName + ", " + model.FHANumber;
                if (model.IsSuspension ?? false)
                {
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                                "<p>FHA Number: " + model.FHANumber + "</p>" +
                                "<p>This R4R submission is a suspension request. This type of submission requires Account Executive's review. " +
                                string.Format("<p>Due to the suspension, this request has been followed to an Account Executive for approval. Your request has been assigned to {0} for further review", aeName) +
                                "<p>" + ServicerRemarks;
                }
                else
                {
                    mail.Body = "<p>Property Name: " + model.PropertyName + "</p>" +
                                "<p>FHA Number: " + model.FHANumber + "</p>" +
                                "<p>Your request has been Forwarded to a HUD AE for further review." +
                                string.Format(" Please feel free to contact your Account Executive {0} at {1}.</p>", aeName, aeEmail) + ServicerRemarks;
                }

                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId = lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "R4RSUBMIT").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = aeEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "R4RSUBMIT").EmailTypeId;
                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);
                smtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendR4RApprovalNotificationEmail(string requestorEmail, string aeEmail,
            ReserveForReplacementFormModel model)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(requestorEmail);
                mail.To.Add(aeEmail);
                string comments = String.IsNullOrEmpty(model.AddnRemarks) ? "<p>Additional Comments: None</p>" : "<p>Comments: " + model.AddnRemarks + "</p>";
                mail.Subject = "R4R Approval Notification for " + model.PropertyName + ", " + model.FHANumber;
                mail.Body = "<p>Property Name: " + model.PropertyName + "</p>"
                    + "<p>FHA Number: " + model.FHANumber + "</p>"
                    + "<p>" + model.HudRemarks + "</p>"
                    + comments;

                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "R4RAPPR").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = aeEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "R4RAPPR").EmailTypeId;
                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendUpdateiREMSNotificationEmail(string emailText, string propertyName, string fhaNumber)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(HHcPEmail);
                const string emailSignature = "<p>This is an automated message. Please do not respond to this e-mail.</p><br/>"
                                              + "<p>Respectfully,</p>"
                                              + "<p>232 Healthcare Portal Support Team</p>";
                mail.Subject = "Update iREMS Contact Information for " + propertyName + ", " + fhaNumber;
                mail.Body =
                    "<p>The subject property has been identified as having updates to its address. Please update your system(s) accordingly.</p>" +
                    emailText
                    + emailSignature;

                mail.IsBodyHtml = true;

                var emailToHhcp = new EmailModel();
                emailToHhcp.EmailTo = HHcPEmail;
                emailToHhcp.EmailFrom = _notificationEmailAddress;
                emailToHhcp.Subject = mail.Subject;
                emailToHhcp.ContentHtml = emailToHhcp.ContentText = mail.Body;
                emailToHhcp.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "UPDTIREMS").EmailTypeId;


                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToHhcp);

                smtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendTPALightPreliminarySubmissionEmail(EmailType emailTypeID, string requestorEmail, string aeEmail, OPAViewModel projectActionViewModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(requestorEmail);
                mail.To.Add(aeEmail);
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel = null;

                emailTemplateMessagesModel =
                _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                var hudMgrRepo = new HUDManagerRepository();
                var aeName = hudMgrRepo.GetHUDManagersByFhaNumber(projectActionViewModel.FhaNumber).Item2.Manager_Name;
                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');

                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.FhaNumber);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.FhaNumber);
                    }
                    else if (keywordsArray[i] == "[CreatedOn]")
                    {

                        body = body.Replace(keywordsArray[i], TimezoneManager.FormatDate(projectActionViewModel.CreatedOn, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[ProjectActionName]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.ProjectActionName);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.ProjectActionName);
                    }
                    else if (keywordsArray[i] == "[PropertyName]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.PropertyName);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.PropertyName);
                    }
                    else if (keywordsArray[i] == "[AEName]")
                    {
                        body = body.Replace(keywordsArray[i], aeName);
                        subject = subject.Replace(keywordsArray[i], aeName);
                    }
                    else if (keywordsArray[i] == "[AEEmail]")
                    {
                        body = body.Replace(keywordsArray[i], aeEmail);
                        subject = subject.Replace(keywordsArray[i], aeEmail);
                    }
                    else if (keywordsArray[i] == "[Comment]")
                    {
                        var comment = projectActionViewModel.ServicerComments != null ? projectActionViewModel.ServicerComments.Trim().ToString() : string.Empty;
                        body = body.Replace(keywordsArray[i], comment);
                        // subject = subject.Replace(keywordsArray[i], aeEmail);
                    }
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                //naveen
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PRACTION").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = aeEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                //naveen
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PRACTION").EmailTypeId;

                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }

        public bool
            SendOPASubmissionEmail(EmailType emailTypeID, string requestorEmail, string aeEmail, OPAViewModel projectActionViewModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                //karri:05112019
                //smtpServer.Host = "localhost";
                //smtpServer.Port = 587;
                mail.To.Add(requestorEmail);
                mail.To.Add(aeEmail);
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel = null;
                if (projectActionViewModel.ProjectActionName.Equals("TPA (Light) - Preliminary approval "))
                {
                    emailTemplateMessagesModel =
                   _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);
                }
                else
                {
                    emailTemplateMessagesModel =
                       _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);
                }
                var hudMgrRepo = new HUDManagerRepository();
                var aeName = hudMgrRepo.GetHUDManagersByFhaNumber(projectActionViewModel.FhaNumber).Item2.Manager_Name;
                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');

                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.FhaNumber);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.FhaNumber);
                    }
                    else if (keywordsArray[i] == "[CreatedOn]")
                    {

                        body = body.Replace(keywordsArray[i], TimezoneManager.FormatDate(projectActionViewModel.CreatedOn, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[ProjectActionName]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.ProjectActionName);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.ProjectActionName);
                    }
                    else if (keywordsArray[i] == "[PropertyName]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.PropertyName);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.PropertyName);
                    }
                    else if (keywordsArray[i] == "[AEName]")
                    {
                        body = body.Replace(keywordsArray[i], aeName);
                        subject = subject.Replace(keywordsArray[i], aeName);
                    }
                    else if (keywordsArray[i] == "[AEEmail]")
                    {
                        body = body.Replace(keywordsArray[i], aeEmail);
                        subject = subject.Replace(keywordsArray[i], aeEmail);
                    }
                    else if (keywordsArray[i] == "[Comment]")
                    {
                        var comment = projectActionViewModel.ServicerComments != null ? projectActionViewModel.ServicerComments.Trim().ToString() : string.Empty;
                        body = body.Replace(keywordsArray[i], comment);
                        // subject = subject.Replace(keywordsArray[i], aeEmail);
                    }
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PRACTION").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = aeEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PRACTION").EmailTypeId;

                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }
        public bool SendProjectActionSubmissionEmail(EmailType emailTypeID, string requestorEmail, string aeEmail, ProjectActionViewModel projectActionViewModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(requestorEmail);
                mail.To.Add(aeEmail);
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);
                var hudMgrRepo = new HUDManagerRepository();
                var aeName = hudMgrRepo.GetHUDManagersByFhaNumber(projectActionViewModel.FhaNumber).Item2.Manager_Name;
                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');

                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.FhaNumber);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.FhaNumber);
                    }
                    else if (keywordsArray[i] == "[CreatedOn]")
                    {

                        body = body.Replace(keywordsArray[i], TimezoneManager.FormatDate(projectActionViewModel.CreatedOn, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[ProjectActionName]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.ProjectActionName);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.ProjectActionName);
                    }
                    else if (keywordsArray[i] == "[PropertyName]")
                    {
                        body = body.Replace(keywordsArray[i], projectActionViewModel.PropertyName);
                        subject = subject.Replace(keywordsArray[i], projectActionViewModel.PropertyName);
                    }
                    else if (keywordsArray[i] == "[AEName]")
                    {
                        body = body.Replace(keywordsArray[i], aeName);
                        subject = subject.Replace(keywordsArray[i], aeName);
                    }
                    else if (keywordsArray[i] == "[AEEmail]")
                    {
                        body = body.Replace(keywordsArray[i], aeEmail);
                        subject = subject.Replace(keywordsArray[i], aeEmail);
                    }
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = false;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PRACTION").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = aeEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PRACTION").EmailTypeId;

                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }
        public bool SendProdTempTaskReassignmentEmail(EmailType emailTypeID, string AssigneFromEmail, string AssigneFromName, string AssignetoEmail, string AssignetoName, string FHANumber, string ProjectName, string ProductionTaskType, string viewname)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(AssigneFromEmail);
                mail.To.Add(AssignetoEmail);
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel = _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsArray = keywords.Split(',');
                string body = emailTemplateMessagesModel.MessageBody;
                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[ProjectActionName]")
                    {
                        body = body.Replace(keywordsArray[i], ProductionTaskType);
                    }
                    else if (keywordsArray[i] == "[PropertyName]")
                    {
                        body = body.Replace(keywordsArray[i], ProjectName);
                    }
                    else if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], FHANumber);
                    }
                    else if (keywordsArray[i] == "[FromName]")
                    {
                        body = body.Replace(keywordsArray[i], AssigneFromName);
                    }
                    else if (keywordsArray[i] == "[ToName]")
                    {
                        body = body.Replace(keywordsArray[i], AssignetoName);
                    }
                    else if (keywordsArray[i] == "[ReassignedDate]")
                    {
                        body = body.Replace(keywordsArray[i], TimezoneManager.FormatDate(DateTime.Today, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[ViewName]")
                    {
                        body = body.Replace(keywordsArray[i], viewname);
                    }
                }
                if (FHANumber.Trim() == "" || FHANumber == null)
                {
                    body = body.Replace(", FHA Number - ", "");
                }
                mail.Body += body + "<br/><br/>";

                mail.Subject = subject;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();

                var emailToFromAe = new EmailModel();
                emailToFromAe.EmailTo = AssigneFromEmail;
                emailToFromAe.EmailFrom = _notificationEmailAddress;
                emailToFromAe.Subject = mail.Subject;
                emailToFromAe.ContentHtml = emailToFromAe.ContentText = mail.Body;
                emailToFromAe.MailTypeId = lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PTSKRASSGN").EmailTypeId;

                var emailToReassignedAe = new EmailModel();
                emailToReassignedAe.EmailTo = AssignetoEmail;
                emailToReassignedAe.EmailFrom = _notificationEmailAddress;
                emailToReassignedAe.Subject = mail.Subject;
                emailToReassignedAe.ContentHtml = emailToReassignedAe.ContentText = mail.Body;
                emailToReassignedAe.MailTypeId = lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "PTSKRASSGN").EmailTypeId;

                emailManager.SaveEmail(emailToFromAe);
                emailManager.SaveEmail(emailToReassignedAe);
                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }
        public bool SendTaskReassignmentEmail(EmailType emailTypeID, TaskReAssignmentViewModel model)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(model.SelectedAEEmail);
                mail.To.Add(model.SelectedReassignAEEmail);
                var IsSingleWlmEmail = false;
                if (model.SelectedWLMEmail.Equals(model.SelectedReassignedWLMEmail))
                {
                    mail.To.Add(model.SelectedWLMEmail);
                    IsSingleWlmEmail = true;
                }
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsArray = keywords.Split(',');
                foreach (var item in model.TaskDetailPerAeList.Entities)
                {
                    string body = emailTemplateMessagesModel.MessageBody;
                    for (var i = 0; i < keywordsArray.Length; i++)
                    {
                        if (keywordsArray[i] == "[ProjectActionName]")
                        {
                            body = body.Replace(keywordsArray[i], item.TaskName);
                        }
                        else if (keywordsArray[i] == "[PropertyName]")
                        {
                            body = body.Replace(keywordsArray[i], item.PropertyName);
                        }
                        else if (keywordsArray[i] == "[FHANumber]")
                        {
                            body = body.Replace(keywordsArray[i], item.FhaNumber);
                        }
                        else if (keywordsArray[i] == "[FromAeName]")
                        {
                            body = body.Replace(keywordsArray[i], model.SelectedAE);
                        }
                        else if (keywordsArray[i] == "[ToAeName]")
                        {
                            body = body.Replace(keywordsArray[i], model.SelectedReassignAE);
                        }
                        else if (keywordsArray[i] == "[ReassignedDate]")
                        {

                            body = body.Replace(keywordsArray[i], TimezoneManager.FormatDate(DateTime.Today, "{0:MM/dd/yyyy}"));
                        }
                    }
                    mail.Body += body + "<br/><br/>";
                }


                mail.Subject = subject;

                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToFromAe = new EmailModel();
                emailToFromAe.EmailTo = model.SelectedAEEmail;
                emailToFromAe.EmailFrom = _notificationEmailAddress;
                emailToFromAe.Subject = mail.Subject;
                emailToFromAe.ContentHtml = emailToFromAe.ContentText = mail.Body;
                emailToFromAe.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "TSKREASSGN").EmailTypeId;

                var emailToReassignedAe = new EmailModel();
                emailToReassignedAe.EmailTo = model.SelectedReassignAEEmail;
                emailToReassignedAe.EmailFrom = _notificationEmailAddress;
                emailToReassignedAe.Subject = mail.Subject;
                emailToReassignedAe.ContentHtml = emailToReassignedAe.ContentText = mail.Body;
                emailToReassignedAe.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "TSKREASSGN").EmailTypeId;

                var emailToFromWlm = new EmailModel();
                emailToFromWlm.EmailTo = model.SelectedWLMEmail;
                emailToFromWlm.EmailFrom = _notificationEmailAddress;
                emailToFromWlm.Subject = mail.Subject;
                emailToFromWlm.ContentHtml = emailToFromWlm.ContentText = mail.Body;
                emailToFromWlm.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "TSKREASSGN").EmailTypeId;

                if (!IsSingleWlmEmail)
                {
                    var emailToReassignedWlm = new EmailModel();
                    emailToReassignedWlm.EmailTo = model.SelectedReassignedWLMEmail;
                    emailToReassignedWlm.EmailFrom = _notificationEmailAddress;
                    emailToReassignedWlm.Subject = mail.Subject;
                    emailToReassignedWlm.ContentHtml = emailToFromAe.ContentText = mail.Body;
                    emailToReassignedWlm.MailTypeId =
                        lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "TSKREASSGN").EmailTypeId;
                    emailManager.SaveEmail(emailToReassignedWlm);
                }
                emailManager.SaveEmail(emailToFromAe);
                emailManager.SaveEmail(emailToReassignedAe);
                emailManager.SaveEmail(emailToFromWlm);
                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }

        public int SendEmail(EmailType emailTypeID, string keywords, string recipientIds, bool firstIdIsPMId = false, bool isBodyHtml = false)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpServer = new SmtpClient();
            mail.From = new MailAddress(HHcPEmail);
            EmailTemplateMessagesModel emailTemplateMessagesModel = _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);
            string body = emailTemplateMessagesModel.MessageBody;
            string subject = emailTemplateMessagesModel.MessageSubject;
            string emailAddresses = string.Empty;
            string aeEmailAddress = string.Empty;
            var keywordsValueArray = keywords.Split('|');
            var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');
            var recipientIDsArray = recipientIds.Split(',');

            for (var i = 0; i < keywordsArray.Length; i++)
            {
                body = body.Replace(keywordsArray[i], keywordsValueArray[i]);
                subject = subject.Replace(keywordsArray[i], keywordsValueArray[i]);
                if (keywordsArray[i].Contains("FHANUMBER") && firstIdIsPMId)
                {
                    var hudManagerRepository = new HUDManagerRepository();
                    aeEmailAddress = (hudManagerRepository.GetAccountExecutiveByFHANumber(keywordsValueArray[i])).AddressModel.Email;
                    mail.To.Add(aeEmailAddress);
                    emailAddresses = aeEmailAddress + ";";
                    recipientIDsArray[0] = string.Empty;
                }
            }

            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = isBodyHtml;

            foreach (var userId in recipientIDsArray)
            {
                if (userId == string.Empty)
                {
                    continue;
                }
                UserViewModel userModel = accountMgr.GetUserById(Convert.ToInt32(userId.Trim()));
                var emailAddress = userModel.AddressModel == null ? userModel.UserName.Trim() : string.IsNullOrEmpty(userModel.AddressModel.Email.Trim()) ? userModel.UserName.Trim() : userModel.AddressModel.Email.Trim();
                emailAddresses += emailAddress + ";";
                mail.To.Add(emailAddress);
            }
#if !DEBUG
            smtpServer.Send(mail);
#endif
            var emailToSave = new EmailModel();
            emailToSave.EmailTo = emailAddresses;
            emailToSave.EmailFrom = mail.From.Address;
            emailToSave.Subject = mail.Subject;
            emailToSave.ContentText = mail.Body;
            emailToSave.ContentHtml = mail.Body;
            emailToSave.MailTypeId = Convert.ToByte(emailTypeID);
            emailToSave.ModifiedBy = 0;

            var emailManager = new EmailManager();

            return emailRepository.SaveEmailAndGetID(emailToSave);
        }

        public void SendEmail(string fromEmail, string body, string toEmail, string subject, string attachementPath)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtpServer = new SmtpClient();
            string[] emailAddresses = toEmail.Split(';');

            mail.From = new MailAddress(fromEmail);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = false;

            foreach (string email in emailAddresses)
            {
                if (email.Trim().Length > 0)
                    mail.To.Add(email.Trim());
            }

            if (!string.IsNullOrEmpty(attachementPath))
            {
                mail.Attachments.Add(new Attachment(attachementPath));
            }

            smtpServer.Send(mail);
        }

        public bool SendFHARequestCompletionEmail(EmailType emailTypeID, string lenderEmail, string prodUserEmail,
            FHANumberRequestViewModel fhaRequestModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(lenderEmail);
                mail.To.Add(prodUserEmail);
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');
                var prodUserName = userRepository.GetNameByUserName(prodUserEmail);
                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], fhaRequestModel.FHANumber);
                        subject = subject.Replace(keywordsArray[i], fhaRequestModel.FHANumber);
                    }
                    else if (keywordsArray[i] == "[RequestSubmitDate]")
                    {

                        body = body.Replace(keywordsArray[i],
                            TimezoneManager.FormatDate(fhaRequestModel.RequestSubmitDate, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[ProjectType]")
                    {
                        var projectType = projectTypeRepository.GetProjectTypebyID(fhaRequestModel.ProjectTypeId);
                        body = body.Replace(keywordsArray[i], projectType);
                        subject = subject.Replace(keywordsArray[i], projectType);
                    }
                    else if (keywordsArray[i] == "[PropertyName]")
                    {
                        body = body.Replace(keywordsArray[i], fhaRequestModel.ProjectName);
                        subject = subject.Replace(keywordsArray[i], fhaRequestModel.ProjectName);
                    }
                    else if (keywordsArray[i] == "[ProdUserName]")
                    {
                        body = body.Replace(keywordsArray[i], prodUserName);
                        subject = subject.Replace(keywordsArray[i], prodUserName);
                    }
                    else if (keywordsArray[i] == "[ProdUserEmail]")
                    {
                        body = body.Replace(keywordsArray[i], prodUserEmail);
                        subject = subject.Replace(keywordsArray[i], prodUserEmail);
                    }
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = lenderEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "FHARQST").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = prodUserEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "FHARQST").EmailTypeId;

                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }

        public bool SendFHAInsertCompletionEmail(EmailType emailTypeID, string lenderEmail, string prodUserEmail,
            FHANumberRequestViewModel fhaRequestModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(lenderEmail);
                mail.To.Add(prodUserEmail);
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');
                var prodUserName = userRepository.GetNameByUserName(prodUserEmail);
                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], fhaRequestModel.FHANumber);
                        subject = subject.Replace(keywordsArray[i], fhaRequestModel.FHANumber);
                    }
                    else if (keywordsArray[i] == "[RequestSubmitDate]")
                    {

                        body = body.Replace(keywordsArray[i],
                            TimezoneManager.FormatDate(fhaRequestModel.RequestSubmitDate, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[ProjectType]")
                    {
                        var projectType = projectTypeRepository.GetProjectTypebyID(fhaRequestModel.ProjectTypeId);
                        body = body.Replace(keywordsArray[i], projectType);
                        subject = subject.Replace(keywordsArray[i], projectType);
                    }
                    else if (keywordsArray[i] == "[PropertyName]")
                    {
                        body = body.Replace(keywordsArray[i], fhaRequestModel.ProjectName);
                        subject = subject.Replace(keywordsArray[i], fhaRequestModel.ProjectName);
                    }
                    else if (keywordsArray[i] == "[ProdUserName]")
                    {
                        body = body.Replace(keywordsArray[i], prodUserName);
                        subject = subject.Replace(keywordsArray[i], prodUserName);
                    }
                    else if (keywordsArray[i] == "[ProdUserEmail]")
                    {
                        body = body.Replace(keywordsArray[i], prodUserEmail);
                        subject = subject.Replace(keywordsArray[i], prodUserEmail);
                    }
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = lenderEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "FHAINSERT").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = prodUserEmail;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "FHAINSERT").EmailTypeId;

                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }


        public bool SendAppProcessSubmitEmail(EmailType emailTypeID, OPAViewModel AppProcessModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();

                mail.To.Add(UserPrincipal.Current.UserName);

                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');

                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]" || keywordsArray[i] == "[Project FHA Number]")
                    {
                        subject = subject.Replace(keywordsArray[i], AppProcessModel.FhaNumber);

                    }
                    else if (keywordsArray[i] == "[CreatedOn]")
                    {

                        body = body.Replace(keywordsArray[i],
                            TimezoneManager.FormatDate(AppProcessModel.ServicerSubmissionDate, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[LoanType]")
                    {

                        body = body.Replace(keywordsArray[i], AppProcessModel.ProjectActionName);

                    }
                    else if (keywordsArray[i] == "[PropertyName]" || keywordsArray[i] == "[ProjectName]")
                    {
                        body = body.Replace(keywordsArray[i], AppProcessModel.PropertyName);
                        subject = subject.Replace(keywordsArray[i], AppProcessModel.PropertyName);
                    }
                    else if (keywordsArray[i] == "[Comment]")
                    {
                        body = body.Replace(keywordsArray[i], AppProcessModel.ServicerComments);

                    }
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                if (AppProcessModel.pageTypeId == 17)
                    emailToRequester.EmailTo = UserPrincipal.Current.UserName + "; " + AppProcessModel.AssignedTo;
                else
                    emailToRequester.EmailTo = UserPrincipal.Current.UserName;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == Enum.GetName(typeof(EmailType), emailTypeID)).EmailTypeId;



                emailManager.SaveEmail(emailToRequester);


                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }
        public bool SendAmmendmentAssignmentEmailToUWC(EmailType emailTypeID, string toemail, OPAViewModel AppProcessModel)
        {
            try
            {
                if (AppProcessModel.pageTypeId == 18)
                {
                    emailTypeID = EmailType.AMUWCLOSER;
                }

                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(toemail);


                mail.From = new MailAddress(HHcPEmail);

                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');


                if (AppProcessModel.pageTypeId == 18)
                {
                    try
                    {
                        for (var i = 0; i < keywordsArray.Length; i++)
                        {
                            if (keywordsArray[i] == "[ProjectFHANumber]" || keywordsArray[i] == "[Project FHA Number]" || keywordsArray[i] == "[FHANumber]")

                            {
                                subject = subject.Replace(keywordsArray[i], AppProcessModel.FhaNumber);

                            }
                            else if (keywordsArray[i] == "[SubmittedDate]")
                            {

                                body = body.Replace(keywordsArray[i],
                                    TimezoneManager.FormatDate(AppProcessModel.ServicerSubmissionDate, "{0:MM/dd/yyyy}"));
                            }

                            else if (keywordsArray[i] == "[ProjectName]")
                            {
                                subject = subject.Replace(keywordsArray[i], AppProcessModel.PropertyName);

                            }

                        }
                    }
                    catch (Exception e)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                        return false;
                    }
                }

                try
                {
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;

                    var emailManager = new EmailManager();

                    var emailToRequester = new EmailModel();
                    emailToRequester.EmailTo = toemail;
                    emailToRequester.EmailFrom = _notificationEmailAddress;
                    emailToRequester.Subject = mail.Subject;
                    emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;


                    emailManager.SaveEmail(emailToRequester);


                    smtpServer.Send(mail);
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    return false;
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }


        public bool SendAppProcessAssignmentEmail(EmailType emailTypeID, string toemail, OPAViewModel AppProcessModel)
        {
            try
            {
                if (AppProcessModel.pageTypeId == 10)
                {
                    emailTypeID = EmailType.DCFROMQ;
                }
                var projectType = projectTypeRepository.GetProjectTypebyID(AppProcessModel.ProjectActionTypeId);

                AppProcessModel.ProjectActionName = projectType != null ? projectType : "";
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(toemail);

                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');

                if (AppProcessModel.pageTypeId == 10)
                {
                    for (var i = 0; i < keywordsArray.Length; i++)
                    {
                        if (keywordsArray[i] == "[FHANumber]")
                        {
                            subject = subject.Replace(keywordsArray[i], AppProcessModel.FhaNumber);

                        }
                        else if (keywordsArray[i] == "[ProdUserName]")
                        {
                            ProductionQueueManager GetNameBYEmail = new ProductionQueueManager();
                            var username = GetNameBYEmail.GetUserInfoByUsername(toemail);
                            body = body.Replace(keywordsArray[i], username.FirstName + " " + username.LastName);
                        }
                        else if (keywordsArray[i] == "[ProjectName]")
                        {
                            subject = subject.Replace(keywordsArray[i], AppProcessModel.PropertyName);
                        }
                    }
                }
                else
                {
                    for (var i = 0; i < keywordsArray.Length; i++)
                    {
                        if (keywordsArray[i] == "[FHANumber]")
                        {
                            body = body.Replace(keywordsArray[i], AppProcessModel.FhaNumber);

                        }
                        else if (keywordsArray[i] == "[CreatedOn]")
                        {

                            body = body.Replace(keywordsArray[i],
                                TimezoneManager.FormatDate(AppProcessModel.ServicerSubmissionDate, "{0:MM/dd/yyyy}"));
                        }
                        else if (keywordsArray[i] == "[LoanType]")
                        {

                            body = body.Replace(keywordsArray[i], AppProcessModel.ProjectActionName);

                        }
                        else if (keywordsArray[i] == "[PropertyName]")
                        {
                            body = body.Replace(keywordsArray[i], AppProcessModel.PropertyName);

                        }
                        else if (keywordsArray[i] == "[Comment]")
                        {
                            body = body.Replace(keywordsArray[i], AppProcessModel.ServicerComments);

                        }

                    }
                }
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = toemail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;

                if (AppProcessModel.pageTypeId == 10)
                {
                    emailTypeID = EmailType.DCFROMQ;
                    emailToRequester.MailTypeId = lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "DCFROMQ").EmailTypeId;
                }
                else
                {
                    //karri:05112019
                    //    emailToRequester.MailTypeId =
                    //lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "APPPRASSIG").EmailTypeId;
                }



                emailManager.SaveEmail(emailToRequester);


                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }



        public bool SendAppProcessLenderRAISubmitEmail(EmailType emailTypeID, List<string> toemails,
    OPAViewModel AppProcessModel)
        {
            try
            {
                var projectType = projectTypeRepository.GetProjectTypebyID(AppProcessModel.ProjectActionTypeId);

                AppProcessModel.ProjectActionName = projectType != null ? projectType : "";
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                foreach (string value in toemails)
                {
                    mail.To.Add(value);
                }

                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');

                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], AppProcessModel.FhaNumber);

                    }
                    else if (keywordsArray[i] == "[CreatedOn]")
                    {

                        body = body.Replace(keywordsArray[i],
                            TimezoneManager.FormatDate(AppProcessModel.ServicerSubmissionDate, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[LoanType]")
                    {

                        body = body.Replace(keywordsArray[i], AppProcessModel.ProjectActionName);

                    }
                    else if (keywordsArray[i] == "[PropertyName]")
                    {
                        body = body.Replace(keywordsArray[i], AppProcessModel.PropertyName);

                    }
                    else if (keywordsArray[i] == "[Comment]")
                    {
                        body = body.Replace(keywordsArray[i], AppProcessModel.ServicerComments);

                    }

                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = string.Join(",", toemails.ToArray());
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "APPPRLCSUB").EmailTypeId;



                emailManager.SaveEmail(emailToRequester);


                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }
        public string SendLenderNotificationsEmail(EmailType emailTypeID, Prod_EmailToLenderModel LenderNotificationsEmailModel)
        {
            try
            {
                var currentUser = UserPrincipal.Current.FullName;
                TaskManager tm = new TaskManager();
                var sharepointscreen = sharepointScreenManager.GetSharepointDataById(LenderNotificationsEmailModel.TaskInstanceID);
                var sharepointData = sharepointScreenManager.GetSharePointClosingInfo(LenderNotificationsEmailModel.TaskInstanceID);
                string AssignedCloser = string.Empty;
                string CCName = string.Empty;
                string InitialClosingDate = string.Empty;
                int firmAmendment = 0;
                string firmCommitmentAmendment = string.Empty;
                DateTime? dteSignedDate = DateTime.UtcNow;
                if (sharepointData != null)
                {
                    if (tm.GetAssignedCloserByFHANumber(sharepointscreen.FHANumber) != null)
                    {
                        var GetAssignedCloser = tm.GetAssignedCloserByFHANumber(sharepointscreen.FHANumber);
                        if (GetAssignedCloser != null)
                        {
                            AssignedCloser = GetAssignedCloser.ToString();

                        }
                    }
                    if (sharepointData.InitialClosingDate != DateTime.MinValue)
                    {
                        InitialClosingDate = sharepointData.InitialClosingDate.ToString();
                    }

                }
                if (sharepointscreen != null)
                {
                    var user = productionQueue.GetUserById(sharepointscreen.ClosingCloserId);
                    if (user != null)
                        CCName = String.Format("{0} {1}", user.FirstName, user.LastName);

                }



                string Attorney = string.Empty;
                string BackupOGC = string.Empty;
                string Underwriter = string.Empty;
                var GetAttorney = prod_TaskXrefManager.GetProductionSubTasks(LenderNotificationsEmailModel.TaskInstanceID).Where(m => m.ViewId == (int)ProductionView.Attorney).FirstOrDefault();
                if (GetAttorney != null)
                {
                    var user = productionQueue.GetUserById(GetAttorney.AssignedTo);
                    if (user != null)
                        Attorney = String.Format("{0} {1}", user.FirstName, user.LastName);

                }

                var GetBackupOGC = prod_TaskXrefManager.GetProductionSubTasks(LenderNotificationsEmailModel.TaskInstanceID).Where(m => m.ViewId == (int)ProductionView.BackupOGC).FirstOrDefault();
                if (GetBackupOGC != null)
                {
                    var BackupOGCuser = productionQueue.GetUserById(GetBackupOGC.AssignedTo);
                    if (BackupOGCuser != null)
                        BackupOGC = String.Format("{0} {1}", BackupOGCuser.FirstName, BackupOGCuser.LastName);
                }

                string assignedHUDAttorney = Attorney;
                if (!string.IsNullOrEmpty(Attorney) && !string.IsNullOrEmpty(BackupOGC))
                {
                    assignedHUDAttorney = Attorney + "/" + BackupOGC;
                }
                else if (!string.IsNullOrEmpty(BackupOGC))
                {
                    assignedHUDAttorney = BackupOGC;
                }

                string address = string.Empty;

                if (sharepointscreen != null && sharepointscreen.OgcAddressId != 0)
                {
                    var addressModel = sharepointScreenManager.GetAddressById(sharepointscreen.OgcAddressId);
                    if (addressModel != null)
                    {
                        if (!string.IsNullOrEmpty(addressModel.AddressLine1)
                            && !string.IsNullOrEmpty(addressModel.City)
                            && !string.IsNullOrEmpty(addressModel.StateCode)
                            && !string.IsNullOrEmpty(addressModel.ZIP))
                            address = addressModel.AddressLine1 + " " + addressModel.AddressLine2 + ", " + addressModel.City + ", " + addressModel.StateCode + " " + addressModel.ZIP;
                    }


                }
                if (sharepointscreen != null && sharepointscreen.BackupOgcAddressId != 0)
                {
                    var BackupOgcaddressModel = sharepointScreenManager.GetAddressById(sharepointscreen.OgcAddressId);
                    if (BackupOgcaddressModel != null)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            address = address + "/" + BackupOgcaddressModel.AddressLine1 + " " + BackupOgcaddressModel.AddressLine2 + ", " + BackupOgcaddressModel.City + ", " + BackupOgcaddressModel.StateCode + " " + BackupOgcaddressModel.ZIP;

                        }
                        else
                        {
                            address = BackupOgcaddressModel.AddressLine1 + " " + BackupOgcaddressModel.AddressLine2 + ", " + BackupOgcaddressModel.City + ", " + BackupOgcaddressModel.StateCode + " " + BackupOgcaddressModel.ZIP;

                        }
                    }

                }
                var OPAViewData = _projectActionFormManager.GetOPAByTaskInstanceId(LenderNotificationsEmailModel.TaskInstanceID);
                int propertyId = _projectActionFormManager.GetProdPropertyInfo(OPAViewData.FhaNumber).PropertyId;
                var TaskData = prod_TaskXrefManager.GetProductionSubTasks(LenderNotificationsEmailModel.TaskInstanceID).FirstOrDefault();
                if (TaskData != null && TaskData.AssignedTo > 0)
                {
                    var user = productionQueue.GetUserById(TaskData.AssignedTo);
                    if (user != null)
                        Underwriter = String.Format("{0} {1}", user.FirstName, user.LastName);

                }
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(LenderNotificationsEmailModel.SendToEmail);
                List<string> ccemails = new List<string>();
                if (LenderNotificationsEmailModel.CCEmails != null)
                {
                    foreach (string value in LenderNotificationsEmailModel.CCEmails)
                    {
                        mail.CC.Add(value);
                        ccemails.Add(value);
                    }
                }


                //Amendmemts 
                if (LenderNotificationsEmailModel.EmailTemplateId.Equals(EmailType.WLMAMEND1.ToString()))
                {
                    var objAmendment = _projectActionFormManager.GetFormAmendmentByTaskInstanceId(LenderNotificationsEmailModel.TaskInstanceID).OrderByDescending(o => o.ModifiedOn).FirstOrDefault();
                    firmAmendment = objAmendment.FirmAmendmentNum;
                    if (firmAmendment > 0)
                    {
                        firmCommitmentAmendment = firmAmendment.ToString();
                    }
                    else
                    {
                        firmCommitmentAmendment = "#";
                    }

                }
                if (LenderNotificationsEmailModel.EmailTemplateId.Equals(EmailType.WLMAMEND2.ToString()))
                {
                    var objAmendment = _projectActionFormManager.GetFormAmendmentByTaskInstanceId(LenderNotificationsEmailModel.TaskInstanceID).OrderByDescending(o => o.ModifiedOn).FirstOrDefault();
                    firmAmendment = objAmendment.FirmAmendmentNum;
                    dteSignedDate = objAmendment.FirmCommitmentSignedDate;
                    if (firmAmendment > 0)
                    {
                        firmCommitmentAmendment = firmAmendment.ToString();
                    }
                    else
                    {
                        firmCommitmentAmendment = "#";
                    }

                }

                mail.From = new MailAddress(HHcPEmail);

                if (string.IsNullOrEmpty(assignedHUDAttorney) && LenderNotificationsEmailModel.EmailTemplateId == "FIRMAP")
                {
                    emailTypeID = EmailType.FIRMAP2;
                    LenderNotificationsEmailModel.EmailTemplateId = "FIRMAP2";
                }

                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                if (!string.IsNullOrEmpty(emailTemplateMessagesModel.KeyWords))
                {
                    var keywords = emailTemplateMessagesModel.KeyWords;
                    var keywordsValueArray = keywords.Split('|');
                    var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');
                    for (var i = 0; i < keywordsArray.Length; i++)
                    {
                        switch (keywordsArray[i])
                        {
                            case "[PROJECTNAME]":
                                if (!string.IsNullOrEmpty(OPAViewData.PropertyName))
                                {
                                    body = body.Replace(keywordsArray[i], OPAViewData.PropertyName);
                                    subject = subject.Replace(keywordsArray[i], OPAViewData.PropertyName);

                                }
                                else
                                {
                                    return "false";
                                }

                                break;
                            case "[ProjectName]":
                                if (!string.IsNullOrEmpty(OPAViewData.PropertyName))
                                {
                                    subject = subject.Replace(keywordsArray[i], OPAViewData.PropertyName);
                                }
                                else
                                {
                                    return "false";
                                }

                                break;
                            case "[ProjectActionName]":
                                if (!string.IsNullOrEmpty(OPAViewData.PropertyName))
                                {
                                    subject = subject.Replace(keywordsArray[i], OPAViewData.PropertyName);
                                }
                                else
                                {
                                    return "false";
                                }

                                break;
                            case "[PROJECTNMBER]":
                                if (propertyId > 0)
                                {
                                    body = body.Replace(keywordsArray[i], propertyId.ToString());
                                }
                                else
                                {
                                    return "false";
                                }


                                break;
                            case "[PORTALADDRESS]":
                                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PortalAddress"]))
                                {
                                    body = body.Replace(keywordsArray[i], ConfigurationManager.AppSettings["PortalAddress"].ToString());
                                }
                                else
                                {
                                    return "false";
                                }

                                break;
                            case "[CMNAME]":
                                if (!string.IsNullOrEmpty(currentUser))
                                {
                                    body = body.Replace(keywordsArray[i], currentUser);
                                }
                                else
                                {
                                    return "false";
                                }

                                break;
                            case "[DATE]":

                                if (!string.IsNullOrEmpty(InitialClosingDate))
                                {
                                    body = body.Replace(keywordsArray[i], InitialClosingDate);
                                }
                                else
                                {
                                    return "Please enter the Initial Closing Date in the Closing Section of the Sharepoint Screen, before you send the email.";
                                }
                                break;
                            case "[CCNAME]":
                                if (!string.IsNullOrEmpty(CCName))
                                {
                                    body = body.Replace(keywordsArray[i], CCName);
                                }
                                else
                                {
                                    return "Please select the Closing Coordinator in the Closing Section of the Sharepoint Screen, before you send the email.";
                                }

                                break;
                            case "[assigned Closer name]":
                                //if (!string.IsNullOrEmpty(AssignedCloser))
                                //{
                                //    body = body.Replace(keywordsArray[i], AssignedCloser);
                                //}
                                //else
                                //{
                                //    return  "Assigned Closer name is null.";
                                //}
                                body = body.Replace(keywordsArray[i], AssignedCloser);
                                break;
                            case "[assigned HUD Attorney]":
                                if (!string.IsNullOrEmpty(assignedHUDAttorney))
                                {
                                    body = body.Replace(keywordsArray[i], assignedHUDAttorney);
                                }
                                else
                                {
                                    return "HUD Attorney is not assigned to this process. Please assign Attorney as an External Reviewer or Assign a Backup OGC in the Backup OGC section of the Sharepoint Screen, before you send the email.";
                                }

                                break;
                            case "[address]":
                                if (!string.IsNullOrEmpty(address))
                                {
                                    body = body.Replace(keywordsArray[i], address);
                                }
                                else
                                {
                                    return "Please enter Attorney address in the OGC Section of Sharepoint Screen, before you send the email.";
                                }

                                break;
                            case "[Underwriter]":
                                if (!string.IsNullOrEmpty(Underwriter))
                                {
                                    body = body.Replace(keywordsArray[i], Underwriter);
                                }
                                else
                                {
                                    return "false";
                                }

                                break;
                            case "[FHANumber]":
                                if (!string.IsNullOrEmpty(OPAViewData.FhaNumber))
                                {
                                    body = body.Replace(keywordsArray[i], OPAViewData.FhaNumber);
                                    subject = subject.Replace(keywordsArray[i], OPAViewData.FhaNumber);
                                }
                                else
                                {
                                    return "false";
                                }

                                break;
                            case "[AccountExecutive]":
                                if (!string.IsNullOrEmpty(LenderNotificationsEmailModel.AccountExecutive))
                                {
                                    body = body.Replace(keywordsArray[i], LenderNotificationsEmailModel.AccountExecutive);
                                }
                                else
                                {
                                    return "Please select Account Executive  from Miscellaneous tab or Closing  tab under the Production App type dropdown item  such as  Application or Final Closing in Project Details Screen, before you send the email.";

                                }

                                break;
                            case "[SignedDate]":
                                if (dteSignedDate.HasValue)
                                {
                                    body = body.Replace(keywordsArray[i], dteSignedDate.Value.ToString());
                                    subject = subject.Replace(keywordsArray[i], dteSignedDate.Value.ToString());
                                }
                                else
                                {
                                    return "false";
                                }
                                break;
                            case "[FirmCommitmentAmendment]":
                                if (!string.IsNullOrEmpty(firmCommitmentAmendment))
                                {
                                    body = body.Replace(keywordsArray[i], firmCommitmentAmendment);
                                    subject = subject.Replace(keywordsArray[i], firmCommitmentAmendment);
                                }
                                else
                                {
                                    return "false";
                                }
                                break;
                        }

                    }
                }


                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = mail.To.ToString();
                if (ccemails != null)
                    emailToRequester.EmailCC = string.Join(",", ccemails.ToArray());
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == LenderNotificationsEmailModel.EmailTemplateId).EmailTypeId;



                emailManager.SaveEmail(emailToRequester);


                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return "false";
            }

            return "true";
        }

        public bool SendIrrInitiationSuccess(EmailType emailTypeID, FHANumberRequestViewModel fhaRequestModel, string lenderEmail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(lenderEmail);
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');
                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], fhaRequestModel.FHANumber);
                        subject = subject.Replace(keywordsArray[i], fhaRequestModel.FHANumber);
                    }
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = lenderEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == EnumType.EnumToValue(EmailType.IRRINIT)).EmailTypeId;


                emailManager.SaveEmail(emailToRequester);

                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }

        public bool SendFirmCommitmentAppSuccess(int Pagetypeid, Guid Taskinstanceid)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();

                EmailTemplateMessagesModel emailTemplateMessagesModel =
                   _emailTemplateMessagesManager.GetEmailTemplate(EmailType.FIRMAP);
                var AE = _emailTemplateMessagesManager.GetAeEmailbyInstanceid(Taskinstanceid);
                if (AE != null)
                {
                    mail.To.Add(AE.ToString());
                }
                var Lender = _emailTemplateMessagesManager.GetRecipentbyView(Taskinstanceid, 1);
                if (Lender != null)
                {
                    mail.To.Add(Lender.LenderEmail);
                    emailTemplateMessagesModel.MessageBody = emailTemplateMessagesModel.MessageBody.Replace("[Underwriter]", Lender.Fullname);
                }

                var OGCAtorney = _emailTemplateMessagesManager.GetRecipentbyView(Taskinstanceid, 4);
                if (OGCAtorney != null)
                {
                    mail.To.Add(OGCAtorney.Email);
                    emailTemplateMessagesModel.MessageBody = emailTemplateMessagesModel.MessageBody.Replace("[assigned HUD Attorney]", OGCAtorney.Fullname).Replace("[address]", OGCAtorney.Address);
                }

                var backupOgc = _emailTemplateMessagesManager.GetRecipentbyView(Taskinstanceid, 12);
                if (backupOgc != null)
                {
                    mail.To.Add(backupOgc.Email);
                }

                mail.From = new MailAddress(HHcPEmail);


                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();
                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;

        }
        public string SendIRRApprovalNotificationEmail(EmailType emailTypeID, Prod_EmailToLenderModel LenderNotificationsEmailModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(LenderNotificationsEmailModel.SendToEmail);


                List<string> ccemails = new List<string>();
                if (LenderNotificationsEmailModel.CCEmails != null)
                {
                    foreach (string value in LenderNotificationsEmailModel.CCEmails)
                    {
                        mail.CC.Add(value);
                        ccemails.Add(value);
                    }
                }

                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel =
                    _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                var applicationDetail = _projectActionFormManager.GetOPAByTaskInstanceId(LenderNotificationsEmailModel.TaskInstanceID);

                var reviewers = prod_TaskXrefManager.GetProductionSubTasks(LenderNotificationsEmailModel.TaskInstanceID);
                var underWriter = reviewers.Where(m => m.ViewId == (int)ProductionView.UnderWriter).FirstOrDefault();
                var oGCAttorney = reviewers.Where(m => m.ViewId == (int)ProductionView.Attorney).FirstOrDefault();

                if (oGCAttorney == null) return "OGC is not Assigned ! Please assign OGC.";
                var underwriterInfo = productionQueue.GetUserById(underWriter.AssignedTo);
                var oGCAttorneyInfo = productionQueue.GetUserById(oGCAttorney.AssignedTo);// var user = productionQueue.GetUserById(subTask.AssignedTo);


                //var taskFile = taskManager.GetAllTaskFileByTaskInstanceId(LenderNotificationsEmailModel.TaskInstanceID);
                // Firm comitmment Folderkey -- 13
                var taskFileFolderMapping = taskFileFolderMap.GetMappedFilesbyFolder(new int[] { (int)ProdFolderStructure.WorkProductLoanCommittee }, LenderNotificationsEmailModel.TaskInstanceID);
                if (taskFileFolderMapping.Any())
                {
                    var workproductTaskFile = taskFileFolderMapping.Where(m => m.FileType == FileType.WP);
                    if (workproductTaskFile.Any())
                    {
                        var workProductWithPALName = workproductTaskFile.Where(m => m.FileName.StartsWith("PAL.")).LastOrDefault();
                        if (workProductWithPALName != null)
                        {
                            //mail.Attachments.Add(data);
                            //string filePath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/061-43137_31_f3f3939c-d9b5-406f-bf3c-53d4ee8fb381_1711.xlsx");

                            //Attachment data = new Attachment(filePath, System.Net.Mime.MediaTypeNames.Application.Octet);

                            RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();

                            if (!string.IsNullOrEmpty(workProductWithPALName.API_upload_status) && workProductWithPALName.API_upload_status.ToLower() == "success")
                            {
                                request = webApiTokenRequest.RequestToken();
                                var jsonData = "{\"docId\":" + workProductWithPALName.DocId + ",\"version\":" + workProductWithPALName.Version + "}";
                                var byteResult = webApiDownload.DownloadByteFile(jsonData, request.access_token, workProductWithPALName.FileName);
                                mail.Attachments.Add(new Attachment(new MemoryStream(byteResult), workProductWithPALName.FileName));
                            }

                        }
                        else
                        {
                            return "Please Attach Work Product with File Name 'PAL' ! Email Can not be send without Work Product Attachement";
                        }

                    }
                    else
                    {
                        return "Please Attach Work Product ! Email Can not be send without Work Product Attachement in Firm Comitmment Folder";
                    }


                }
                else
                {
                    return "Please Attach Work Product ! Email Can not be send without Work Product Attachement";
                }

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');
                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        //body = body.Replace(keywordsArray[i], "001-123456");
                        subject = subject.Replace(keywordsArray[i], applicationDetail.FhaNumber);
                    }
                    else if (keywordsArray[i] == "[ProjectName]")
                    {
                        subject = subject.Replace(keywordsArray[i], String.Format("({0}){1}", applicationDetail.FhaNumber, "IRR"));
                    }
                    else if (keywordsArray[i] == "[Underwriter]")
                    {
                        body = body.Replace(keywordsArray[i], String.Format("{0} {1}", underwriterInfo.FirstName, underwriterInfo.LastName));
                    }
                    else if (keywordsArray[i] == "[OGCName]")
                    {
                        body = body.Replace(keywordsArray[i], String.Format("{0} {1}", oGCAttorneyInfo.FirstName, oGCAttorneyInfo.LastName));
                    }
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = mail.To.ToString();
                if (ccemails != null)
                    emailToRequester.EmailCC = string.Join(",", ccemails.ToArray());
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == LenderNotificationsEmailModel.EmailTemplateId).EmailTypeId;


                smtpServer.Timeout = int.MaxValue;
                emailManager.SaveEmail(emailToRequester);


                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                //throw e;
                return "false";
            }
            return "true";
        }
        public string SendIRRFinalApprovalEmail(EmailType emailTypeID, Prod_EmailToLenderModel LenderNotificationsEmailModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(LenderNotificationsEmailModel.SendToEmail);


                List<string> ccemails = new List<string>();
                if (LenderNotificationsEmailModel.CCEmails != null)
                {
                    foreach (string value in LenderNotificationsEmailModel.CCEmails)
                    {
                        mail.CC.Add(value);
                        ccemails.Add(value);
                    }
                }

                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel = _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                var applicationDetail = _projectActionFormManager.GetOPAByTaskInstanceId(LenderNotificationsEmailModel.TaskInstanceID);


                var taskFileFolderMapping = taskFileFolderMap.GetMappedFilesbyFolder(new int[] { (int)ProdFolderStructure.WorkProductClosing }, LenderNotificationsEmailModel.TaskInstanceID);
                if (taskFileFolderMapping.Any())
                {
                    var workproductTaskFile = taskFileFolderMapping.Where(m => m.FileType == FileType.WP);
                    if (workproductTaskFile.Any())
                    {
                        var workProductWithFALName = workproductTaskFile.Where(m => m.FileName.StartsWith("FAL.")).LastOrDefault();
                        if (workProductWithFALName != null)
                        {
                            //mail.Attachments.Add(data);
                            //string filePath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/061-43137_31_f3f3939c-d9b5-406f-bf3c-53d4ee8fb381_1711.xlsx");

                            //Attachment data = new Attachment(filePath, System.Net.Mime.MediaTypeNames.Application.Octet);

                            RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();

                            if (!string.IsNullOrEmpty(workProductWithFALName.API_upload_status) && workProductWithFALName.API_upload_status.ToLower() == "success")
                            {
                                request = webApiTokenRequest.RequestToken();
                                var jsonData = "{\"docId\":" + workProductWithFALName.DocId + ",\"version\":" + workProductWithFALName.Version + "}";
                                var byteResult = webApiDownload.DownloadByteFile(jsonData, request.access_token, workProductWithFALName.FileName);
                                mail.Attachments.Add(new Attachment(new MemoryStream(byteResult), workProductWithFALName.FileName));
                            }

                        }
                        else
                        {
                            return "Please Attach Work Product with File Name 'FAL' ! Email Can not be send without Work Product Attachement";
                        }

                    }
                    else
                    {
                        return "Please Attach Work Product ! Email Can not be send without Work Product Attachement in Closing Folder";
                    }


                }
                else
                {
                    return "Please Attach Work Product ! Email Can not be send without Work Product Attachement";
                }


                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');
                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        //body = body.Replace(keywordsArray[i], "001-123456");
                        body = body.Replace(keywordsArray[i], applicationDetail.FhaNumber);
                    }
                    else if (keywordsArray[i] == "[ProjectName]")
                    {
                        body = body.Replace(keywordsArray[i], String.Format("({0}){1}", applicationDetail.FhaNumber, "IRR"));
                    }

                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = mail.To.ToString();
                if (ccemails != null)
                    emailToRequester.EmailCC = string.Join(",", ccemails.ToArray());
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == LenderNotificationsEmailModel.EmailTemplateId).EmailTypeId;


                smtpServer.Timeout = int.MaxValue;
                emailManager.SaveEmail(emailToRequester);


                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return "false";
            }
            return "true";
        }


        public string SendIRRLoanModificationEmail(EmailType emailTypeID, prod_EmailToProdUser prodUserNotificationsEmailMode)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                //mail.To.Add(LenderNotificationsEmailModel.SendToEmail);
                List<string> emailToProdUsers = new List<string>();
                if (prodUserNotificationsEmailMode.ToProdUsers.Count() > 0)
                {
                    foreach (var toEmail in prodUserNotificationsEmailMode.ToProdUsers)
                    {
                        mail.To.Add(toEmail);
                        emailToProdUsers.Add(toEmail);
                    }
                }
                List<string> ccemails = new List<string>();
                if (prodUserNotificationsEmailMode.CCEmails.Any())
                {
                    foreach (string value in prodUserNotificationsEmailMode.CCEmails)
                    {
                        mail.CC.Add(value);
                        ccemails.Add(value);
                    }
                }

                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel = _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);

                var applicationDetail = _projectActionFormManager.GetOPAByTaskInstanceId(prodUserNotificationsEmailMode.TaskInstanceID);

                string body = emailTemplateMessagesModel.MessageBody;
                var subject = emailTemplateMessagesModel.MessageSubject;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsValueArray = keywords.Split('|');
                var keywordsArray = emailTemplateMessagesModel.KeyWords.Split(',');
                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[FHANumber]")
                    {
                        //body = body.Replace(keywordsArray[i], "001-123456");
                        subject = subject.Replace(keywordsArray[i], applicationDetail.FhaNumber);
                    }
                    else if (keywordsArray[i] == "[ProjectName]")
                    {
                        subject = subject.Replace(keywordsArray[i], String.Format("({0}){1}", applicationDetail.FhaNumber, "IRR"));
                    }

                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();


                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = string.Join(",", emailToProdUsers.ToArray());
                if (ccemails.Any())
                    emailToRequester.EmailCC = string.Join(",", ccemails.ToArray());
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.ModifiedBy = UserPrincipal.Current.UserId;
                emailToRequester.MailTypeId =
                lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == prodUserNotificationsEmailMode.EmailTemplateId).EmailTypeId;
                emailToRequester.ModifiedOn = DateTime.Now;


                smtpServer.Timeout = int.MaxValue;
                emailManager.SaveEmail(emailToRequester);


                smtpServer.Send(mail);


            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return "false";
            }
            return "true";
        }

        public bool SendR4RDenyNotificationEmail(ReserveForReplacementFormModel model, string denyReason)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(model.AssignedBy);
                mail.To.Add(model.AssignedTo);
                //string comments = String.IsNullOrEmpty(model.AddnRemarks) ? "<p>Additional Comments: None</p>" : "<p>Comments: " + model.AddnRemarks + "</p>";
                mail.Subject = "R4R Denial Notification for " + model.PropertyName + ", " + model.FHANumber;
                mail.Body = "<p>Property Name: " + model.PropertyName + "</p>"
                    + "<p>FHA Number: " + model.FHANumber + "</p>"
                    + "<p>" + "Your R4R Request has been denied! " + "</p>"
                    + "Deny Reason: " + denyReason;

                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = model.AssignedBy;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                emailToRequester.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "R4RAPPR").EmailTypeId;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = model.AssignedTo;
                emailToAE.EmailFrom = _notificationEmailAddress;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                emailToAE.MailTypeId =
                    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "R4RDeny").EmailTypeId;
                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);

                return true;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        //#149;karri#214;Note: two DLs has to be created, one for HUD HelpDesk1 and another for HUD HelpDesk2(Tech Team)


        public bool SendHelpDeskNotificationEmail(string toEmail, string requestorEmail, HD_TICKET_ViewModel model)
        {
            try
            {
                toEmail = model.MAILNOTIFICATION_TOEMAIL;
                requestorEmail = model.MAILNOTIFICATION_REQUESTOREMAIL;

                //requestorEmail=who created the ticket, a mail is sent to him also
                //toEmail: collection of emails to whom the mail is additionally sent (could be seperate mail or..)
                //Note:Who Created Ticket/Submitter
                MailMessage mailSubmitter = new MailMessage();//usually HUDHDADMIN USer

                //Note:Internal Users CC mails must be attached to mailHelpDesk object only
                MailMessage mailHelpDesk = new MailMessage();//to whom Ticket is intended to or associated to
                MailMessage mailTechDesk = new MailMessage();
                //karri;sup#187
                MailMessage mailToRequestor = new MailMessage();//usually Ticket OWNER

                SmtpClient smtpServer = new SmtpClient();

                if (!string.IsNullOrEmpty(requestorEmail))
                    mailSubmitter.To.Add(requestorEmail);//created//logged user
                //mailSubmitter.To.Add(toEmail);
                mailSubmitter.From = new MailAddress(model.MAILNOTIFICATION_FROMBASEEMAIL);//this is where all kinds of mails are sent FROM address              

                if (!string.IsNullOrEmpty(toEmail))
                    mailHelpDesk.To.Add(toEmail);
                mailHelpDesk.From = new MailAddress(model.MAILNOTIFICATION_FROMBASEEMAIL);//this is where all kinds of mails are sent FROM address 

                if (!string.IsNullOrEmpty(toEmail))
                    mailTechDesk.To.Add(toEmail);
                mailTechDesk.From = new MailAddress(model.MAILNOTIFICATION_FROMBASEEMAIL);//this is where all kinds of mails are sent FROM address 

                //karri;sup#187
                if (toEmail.Contains("hud232portal@penielsolutions.com"))
                {
                    if (!string.IsNullOrEmpty(model.CREATED_BY_USERNAME))
                        mailToRequestor.To.Add(model.CREATED_BY_USERNAME);
                    mailToRequestor.From = new MailAddress(model.MAILNOTIFICATION_FROMBASEEMAIL);//this is where all kinds of mails are sent FROM address 
                }


                //CREATE MODE
                if (model.MAILNOTIFICATION_TICKETMODE.Equals(1))
                {
                    //mail.CC.Add(email);
                    if (model.MAILNOTIFICATION_CCEMAIL != null && !string.IsNullOrEmpty(model.MAILNOTIFICATION_CCEMAIL))
                        mailHelpDesk.CC.Add(model.MAILNOTIFICATION_CCEMAIL);

                    //first comments
                    if (string.IsNullOrEmpty(model.MAILNOTIFICATION_SUBJECT))
                        mailSubmitter.Subject = "Help Desk Ticket Create Notification: #" + model.TICKET_ID.ToString() + "; For: " + model.ISSUE;
                    else
                        mailSubmitter.Subject = model.MAILNOTIFICATION_SUBJECT;

                    mailSubmitter.Body = "<p>Ticket Id            : " + model.TICKET_ID + "</p>" +
                                "<p>Ticket Current Status: Open </p>" +
                                "<p>Ticket Title         : " + model.ISSUE + "</p>" +
                                "<p>Created On           : " + model.CREATED_ON + "</p>" +
                                "<p>Created By           : " + model.CREATED_BY_USERNAME + "</p>" +
                                "<p>Nuber of Attachments : " + model.MAILNOTIFICATION_NUMBEROFFILESATTACHED + "</p>" +
                                "<p>Issue Replication    : " + model.RECREATION_STEPS + "</p>" +
                                "<p>Issue Description    : " + model.DESCRIPTION + "</p>" +
                                "<p>User Comments        : " + model.USERCOMMENTS + "</p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p>Thanks & Regards     : Help Desk Team!</p>";

                }
                else //UPDATE MODE
                {
                    //mail.CC.Add(email);
                    if (model.MAILNOTIFICATION_CCEMAIL != null && !string.IsNullOrEmpty(model.MAILNOTIFICATION_CCEMAIL))
                        mailHelpDesk.CC.Add(model.MAILNOTIFICATION_CCEMAIL);

                    //first comments
                    if (string.IsNullOrEmpty(model.MAILNOTIFICATION_SUBJECT))
                        mailSubmitter.Subject = "Help Desk Ticket Update Notification: #" + model.TICKET_ID.ToString() + "; For: " + model.ISSUE;
                    else
                        mailSubmitter.Subject = model.MAILNOTIFICATION_SUBJECT;

                    mailSubmitter.Body = "<p>Ticket Id            : " + model.TICKET_ID + "</p>" +
                                "<p>Ticket Current Status: " + model.MAILNOTIFICATION_STATUS + "</p>" +
                                "<p>Ticket Title         : " + model.ISSUE + "</p>" +
                                "<p>Created On           : " + model.CREATED_ON + "</p>" +

                                "<p>Assigned Date        : " + model.ASSIGNED_DATE + "</p>" +

                                "<p>Issue Replication    : " + model.RECREATION_STEPS + "</p>" +
                                "<p>Issue Description    : " + model.DESCRIPTION + "</p>" +
                                "<p>User Comments        : " + model.USERCOMMENTS + "</p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p>Thanks & Regards     : Help Desk Team!</p>";
                }

                mailSubmitter.IsBodyHtml = true;
                mailHelpDesk.IsBodyHtml = true;
                mailTechDesk.IsBodyHtml = true;
                //karri;sup#187
                mailToRequestor.IsBodyHtml = true;

                mailHelpDesk.Subject = mailSubmitter.Subject;
                mailHelpDesk.Body = mailSubmitter.Body;

                mailTechDesk.Subject = mailHelpDesk.Subject;
                mailTechDesk.Body = mailHelpDesk.Body;

                //karri;sup#187
                mailToRequestor.Subject = mailSubmitter.Subject;
                mailToRequestor.Body = mailSubmitter.Body;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = model.MAILNOTIFICATION_FROMBASEEMAIL;
                emailToRequester.Subject = mailSubmitter.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mailSubmitter.Body;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = toEmail;

                if (model.MAILNOTIFICATION_CCEMAIL != null && !string.IsNullOrEmpty(model.MAILNOTIFICATION_CCEMAIL))
                    emailToAE.EmailCC = model.MAILNOTIFICATION_CCEMAIL;

                emailToAE.EmailFrom = model.MAILNOTIFICATION_FROMBASEEMAIL;
                emailToAE.Subject = mailSubmitter.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mailSubmitter.Body;

                //karri;sup#187
                var emailToTicketCreator = new EmailModel();
                if (toEmail.Contains("hud232portal@penielsolutions.com"))
                {
                    emailToTicketCreator.EmailTo = model.CREATED_BY_USERNAME;
                    emailToTicketCreator.EmailFrom = model.MAILNOTIFICATION_FROMBASEEMAIL;
                    emailToTicketCreator.Subject = mailSubmitter.Subject;
                    emailToTicketCreator.ContentHtml = emailToRequester.ContentText = mailSubmitter.Body;
                }


                var emailManager = new EmailManager();

                if (!string.IsNullOrEmpty(emailToRequester.EmailTo))
                    emailManager.SaveEmail(emailToRequester);

                if (!string.IsNullOrEmpty(emailToAE.EmailTo))
                    emailManager.SaveEmail(emailToAE);
                //karri;sup#187
                if (toEmail.Contains("hud232portal@penielsolutions.com"))
                {
                    if (!string.IsNullOrEmpty(emailToTicketCreator.EmailTo))
                        emailManager.SaveEmail(emailToTicketCreator);
                }


                ////karri;sup#187
                MailAddressCollection mac1 = mailSubmitter.To;//requestor/hudhdadmin user
                MailAddressCollection mac2 = mailHelpDesk.To;//to whom the ticket is intended or now to be associated
                MailAddressCollection mac3 = mailToRequestor.To;//usually ticket owner/creator
                //Note: mac3.Count>0

                ////karri;sup#187
                bool isHudHDAdminCreatedTicket = false;
                if (mac1.Count.Equals(1) && mac3.Count.Equals(1))
                {
                    if (mac1[0].Address.ToUpper().Equals(mac3[0].Address.ToUpper()))
                        isHudHDAdminCreatedTicket = true;
                }

                if (mailSubmitter.To.Count > 0) ////karri;sup#187
                    smtpServer.Send(mailSubmitter);

                if (mailHelpDesk.To.Count > 0) ////karri;sup#187
                    smtpServer.Send(mailHelpDesk);
                
                ////karri;sup#187
                if (toEmail.Contains("hud232portal@penielsolutions.com"))
                {

                    if (!isHudHDAdminCreatedTicket && mailToRequestor.To.Count > 0)
                        smtpServer.Send(mailToRequestor);
                }
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        // #234
        // Firmcommitment mail trigger hareesh
        public bool SendFirmcommitmentNotificationEmail(string toEmail, string requestorEmail, string userInfoDataString)
        {
            //also required taskname,projname, duration
            string[] userinfo = userInfoDataString.Split(new[] { '$' });
            // toEmail = userinfo[2];

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.From = new MailAddress(_notificationEmailAddress);
                mail.To.Add(toEmail);
                //string comments = String.IsNullOrEmpty(model.AddnRemarks) ? "<p>Additional Comments: None</p>" : "<p>Comments: " + model.AddnRemarks + "</p>";
                mail.Subject = "Firm Commitment Alert " + userinfo[0] + ", " + userinfo[1];
                mail.Body = "<p>Task Name: " + userinfo[0] + "</p>"
                    + "<p>Project Name: " + userinfo[1] + "</p>"
                + "<p>Notifcation Message: Your Firm Committment Request have Reached to 90 days or above  " + userinfo[1] + "</p>";


                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = toEmail;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;
                //emailToRequester.MailTypeId =
                //    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "MPTS").EmailTypeId;

                //var emailToAE = new EmailModel();
                //emailToAE.EmailTo = model.AssignedTo;
                //emailToAE.EmailFrom = _notificationEmailAddress;
                //emailToAE.Subject = mail.Subject;
                //emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;
                //emailToAE.MailTypeId =
                //    lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "R4RDeny").EmailTypeId;
                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);
                // emailManager.SaveEmail(emailToAE);

                smtpServer.Send(mail);

                return true;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }



        }
        // #234 
        public bool SendFirmCommittmentNotificationEmail(IEmailManager emailManager, ProductionMyTaskModel model)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();

                mail.To.Add(model.AssignedTo);//created//logged user
                                              // mail.To.Add(model.as);
                mail.From = new MailAddress(_notificationEmailAddress);

                mail.Subject = "Firm Commitment Alert " + model.TaskName + ", " + model.ProjectName;
                mail.Body = "<p>Task Name: " + model.TaskName + "</p>"
                    + "<p>Project Name: " + model.ProjectName + "</p>"
                + "<p>Notifcation Message: Your Firm Committment Request have Reached to 90 days or above  " + model.ProjectName + "</p>";


                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = model.AssignedTo;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;



                // var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);

                smtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        //karri:31082019
        public bool SendExternalHelpDeskNotificationEmail(string toEmail, string requestorEmail, HD_TICKET_ViewModel model)
        {
            try
            {
                toEmail = model.MAILNOTIFICATION_TOEMAIL;
                requestorEmail = model.MAILNOTIFICATION_REQUESTOREMAIL;
                model.MAILNOTIFICATION_TICKETMODE = 2;

                //requestorEmail=who created the ticket, a mail is sent to him also
                //toEmail: collection of emails to whom the mail is additionally sent (could be seperate mail or..)
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();

                mail.To.Add(requestorEmail);//created//logged user
                mail.To.Add(toEmail);
                mail.From = new MailAddress(model.MAILNOTIFICATION_FROMBASEEMAIL);//this is where all kinds of mails are sent FROM address              

                //CREATE MODE
                mail.Subject = "Help Desk Ticket Notification: #" + model.TICKET_ID.ToString() + "; For: " + model.ISSUE;

                mail.Body = "<p>FHA Number              : " + model.FHANumber + "</p>" +
                            "<p>Project Number          : " + model.PropertyId + "</p>" +
                            "<p>Project Name            : " + model.PropertyName + "</p>" +

                             "<p>Issue Title            : " + model.ISSUE + "</p>" +
                             "<p>Issue Category         : " + model.CATEGORY_TEXT + "</p>" +
                             "<p>Issue SubCategory      : " + model.SUBCATEGORY_TEXT + "</p>" +
                             "<p>Action Performed       : " + model.ONACTION_TEXT + "</p>" +
                             "<p>Created BY             : " + model.CREATED_BY_USERNAME + "</p>" +
                            "<p>Created On              : " + model.CREATED_ON + "</p>" +


                            "<p>Assigned Date           : " + model.ASSIGNED_DATE + "</p>" +

                            "<p>Issue Description       : " + model.DESCRIPTION + "</p>" +
                            "<p>Issue Replication       : " + model.RECREATION_STEPS + "</p>" +
                            "<p>Issue Status            : " + model.STATUS_TEXT + "</p>" +

                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p>Thanks & Regards     : Help Desk Team!</p>";

                if (!string.IsNullOrEmpty(model.HELPDESK_UPLOADS_PATH))
                {
                    var files = Directory.GetFiles(model.HELPDESK_UPLOADS_PATH);
                    int filescount = files.Length;
                    if (filescount > 0)
                    {
                        foreach (var iFile in files)
                        {
                            mail.Attachments.Add(new Attachment(iFile.ToString()));
                        }

                    }

                }

                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = model.MAILNOTIFICATION_FROMBASEEMAIL;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = toEmail;
                emailToAE.EmailFrom = model.MAILNOTIFICATION_FROMBASEEMAIL;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;

                var emailManager = new EmailManager();
                //emailManager.SaveEmail(emailToRequester);
                //emailManager.SaveEmail(emailToAE);
                smtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendExternalHelpDeskNotificationEmail_01(string toEmail, string requestorEmail, HD_TICKET_ViewModel model)
        {
            try
            {
                toEmail = model.MAILNOTIFICATION_TOEMAIL;
                requestorEmail = model.MAILNOTIFICATION_REQUESTOREMAIL;

                //requestorEmail=who created the ticket, a mail is sent to him also
                //toEmail: collection of emails to whom the mail is additionally sent (could be seperate mail or..)
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();

                mail.To.Add(requestorEmail);//created//logged user
                mail.To.Add(toEmail);
                mail.From = new MailAddress(model.MAILNOTIFICATION_FROMBASEEMAIL);//this is where all kinds of mails are sent FROM address             

                //first comments
                if (string.IsNullOrEmpty(model.MAILNOTIFICATION_SUBJECT))
                    mail.Subject = "Help Desk Ticket Update Notification: #" + model.TICKET_ID.ToString() + "; For: " + model.ISSUE;
                else
                    mail.Subject = model.MAILNOTIFICATION_SUBJECT;

                mail.Body = "<p>FHA Number              : " + model.FHANumber + "</p>" +
                            "<p>Project Number          : " + model.PropertyId + "</p>" +
                            "<p>Project Name            : " + model.PropertyName + "</p>" +

                             "<p>Issue Title            : " + model.ISSUE + "</p>" +
                             "<p>Issue Category         : " + model.CATEGORY_TEXT + "</p>" +
                             "<p>Issue SubCategory      : " + model.SUBCATEGORY_TEXT + "</p>" +
                             "<p>Action Performed       : " + model.ONACTION_TEXT + "</p>" +
                             "<p>Created BY             : " + model.CREATED_BY_USERNAME + "</p>" +
                            "<p>Created On              : " + model.CREATED_ON + "</p>" +


                            "<p>Assigned Date           : " + model.ASSIGNED_DATE + "</p>" +

                            "<p>Issue Description       : " + model.DESCRIPTION + "</p>" +
                            "<p>Issue Replication       : " + model.RECREATION_STEPS + "</p>" +
                            "<p>Issue Status            : " + model.STATUS_TEXT + "</p>" +

                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p></p>" +
                            "<p>Thanks & Regards     : Help Desk Team!</p>";

                if (!string.IsNullOrEmpty(model.HELPDESK_UPLOADS_PATH))
                {
                    var files = Directory.GetFiles(model.HELPDESK_UPLOADS_PATH);
                    int filescount = files.Length;
                    if (filescount > 0)
                    {
                        foreach (var iFile in files)
                        {
                            mail.Attachments.Add(new Attachment(iFile.ToString()));
                        }

                    }

                }

                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = requestorEmail;
                emailToRequester.EmailFrom = model.MAILNOTIFICATION_FROMBASEEMAIL;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;

                var emailToAE = new EmailModel();
                emailToAE.EmailTo = toEmail;
                emailToAE.EmailFrom = model.MAILNOTIFICATION_FROMBASEEMAIL;
                emailToAE.Subject = mail.Subject;
                emailToAE.ContentHtml = emailToAE.ContentText = mail.Body;

                var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);
                emailManager.SaveEmail(emailToAE);
                smtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        // harish added new method to send RIS request to servicer
        //RIS(Request Information from Servicer) 07-01-2020
        public bool SendRISNotificationEmail(IEmailManager emailManager, TaskModel model)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();

                mail.To.Add(model.AssignedTo);//created//logged user
                                              // mail.To.Add(model.as);
                mail.From = new MailAddress(_notificationEmailAddress);

                mail.Subject = "RIS Alert " + model.FHANumber + model.TaskName + ", " + model.PropertyName;
                mail.Body = "<p>Task Name: " + model.TaskName + "</p>"
                    + "<p>Project Name: " + model.PropertyName + "</p>"
                + "<p>Notifcation Message: Your RIS Request have Requested  " + model.PropertyName + "</p>";


                mail.IsBodyHtml = true;

                var emailToRequester = new EmailModel();
                emailToRequester.EmailTo = model.AssignedTo;
                emailToRequester.EmailFrom = _notificationEmailAddress;
                emailToRequester.Subject = mail.Subject;
                emailToRequester.ContentHtml = emailToRequester.ContentText = mail.Body;



                // var emailManager = new EmailManager();
                emailManager.SaveEmail(emailToRequester);

                smtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        // harish added 23042020 lender task reassignment mail
        public bool SendLenderProdTempTaskReassignmentEmail(EmailType emailTypeID, string AssigneFromEmail, string AssigneFromName, string AssignetoEmail, string AssignetoName, string FHANumber, string ProjectName, string ProductionTaskType, string viewname)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient();
                mail.To.Add(AssigneFromEmail);
                mail.To.Add(AssignetoEmail);
                mail.From = new MailAddress(HHcPEmail);
                EmailTemplateMessagesModel emailTemplateMessagesModel = _emailTemplateMessagesManager.GetEmailTemplate(emailTypeID);
                var subject = emailTemplateMessagesModel.MessageSubject;
                subject = subject + ',' + viewname;
                var keywords = emailTemplateMessagesModel.KeyWords;
                var keywordsArray = keywords.Split(',');
                string body = emailTemplateMessagesModel.MessageBody;
                for (var i = 0; i < keywordsArray.Length; i++)
                {
                    if (keywordsArray[i] == "[ProjectActionName]")
                    {
                        body = body.Replace(keywordsArray[i], ProductionTaskType);
                    }
                    else if (keywordsArray[i] == "[PropertyName]")
                    {
                        body = body.Replace(keywordsArray[i], ProjectName);
                    }
                    else if (keywordsArray[i] == "[FHANumber]")
                    {
                        body = body.Replace(keywordsArray[i], FHANumber);
                    }
                    else if (keywordsArray[i] == "[FromName]")
                    {
                        body = body.Replace(keywordsArray[i], AssigneFromName);
                    }
                    else if (keywordsArray[i] == "[ToName]")
                    {
                        body = body.Replace(keywordsArray[i], AssignetoName);
                    }
                    else if (keywordsArray[i] == "[ReassignedDate]")
                    {
                        body = body.Replace(keywordsArray[i], TimezoneManager.FormatDate(DateTime.Today, "{0:MM/dd/yyyy}"));
                    }
                    else if (keywordsArray[i] == "[ViewName]")
                    {
                        body = body.Replace(keywordsArray[i], viewname);
                    }
                }
                if (FHANumber.Trim() == "" || FHANumber == null)
                {
                    body = body.Replace(", FHA Number - ", "");
                }
                mail.Body += body + "<br/><br/>";

                mail.Subject = subject;
                mail.IsBodyHtml = true;

                var emailManager = new EmailManager();

                var emailToFromAe = new EmailModel();
                emailToFromAe.EmailTo = AssigneFromEmail;
                emailToFromAe.EmailFrom = _notificationEmailAddress;
                emailToFromAe.Subject = mail.Subject;
                emailToFromAe.ContentHtml = emailToFromAe.ContentText = mail.Body;
                // emailToFromAe.ContentText = mail.Body;
                emailToFromAe.MailTypeId = lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "LTSKRASSGN").EmailTypeId;

                var emailToReassignedAe = new EmailModel();
                emailToReassignedAe.EmailTo = AssignetoEmail;
                emailToReassignedAe.EmailFrom = _notificationEmailAddress;
                emailToReassignedAe.Subject = mail.Subject;
                emailToReassignedAe.ContentHtml = emailToReassignedAe.ContentText = mail.Body;
                //emailToReassignedAe.ContentText = mail.Body;
                emailToReassignedAe.MailTypeId = lookupMgr.GetAllEmailTypes().FirstOrDefault(p => p.EmailTypeCd == "LTSKRASSGN").EmailTypeId;

                emailManager.SaveEmail(emailToFromAe);
                emailManager.SaveEmail(emailToReassignedAe);
                //emailManager
                smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

            return true;
        }


    }
}