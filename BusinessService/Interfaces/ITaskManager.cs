﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Model.Production;
using Model;

namespace BusinessService.Interfaces
{
    public interface ITaskManager
    {
        IEnumerable<TaskFileModel> GetAllTaskFileByTaskInstanceId(Guid taskInstanceId);
        TaskFileModel GetTaskFileByTaskInstanceId(Guid taskInstanceId);
        TaskFileModel GetTaskFileByTaskFileId(Guid taskFileId);
        TaskFileModel GetTaskFileByTaskFileType(string taskFileType); 
        TaskFileModel GetTaskFileByTaskFileName(string FileName);
        TaskFileModel GetTaskFileByTaskInstanceAndFileTypeId(Guid taskInstanceId, FileType fileType);
        TaskFileModel GetTaskFileByTaskInstanceAndFileType(Guid TaskInstanceId, string FileType);
        string GetTaskFileNameByTaskInstanceId(Guid taskInstanceId);
        TaskFileModel GetTaskFileById(int taskFileId);
        void SaveTask(List<TaskModel> taskList, TaskFileModel fileModel);
        void SaveTask(List<TaskModel> taskList, IList<TaskFileModel> fileModel);
        PaginateSortModel<TaskModel> GetTasksByUserName(string userName, int page, string sort, SqlOrderByDirecton sortDir);
        TaskModel GetTaskById(int taskId);
        int GetMyUnOpenedTaskCount();
        void UpdateTask(TaskModel task);
        IEnumerable<TaskModel> GetTasksByTaskInstanceId(Guid taskInstanceId);
        byte[] GetConcurrencyTimeStamp(Guid taskInstanceId);
        TaskModel GetLatestTaskByTaskInstanceId(Guid taskInstanceId);
        void SetModelWithFileAttachmentsForR4R(ReserveForReplacementFormModel model);
        Guid SaveGroupTaskFileModel(TaskFileModel taskFileModel);
        TaskFileModel GetGroupTaskFileByTaskInstanceAndFileTypeId(Guid taskInstanceId, Guid fileType);
        int DeleteGroupTaskFile(Guid taskInstanceId, Guid fileType);
        void UpdateTaskNotes(int TaskId, string HudRemarks);
        void UpdateDataStore1(int TaskId, string dataStoreXML);
        //OPA Changes
        TaskFileModel GetGroupTaskFileByTaskInstanceAndTaskFileID(Guid taskFileId, Guid taskInstanceID);
        int DeleteGroupTaskFileByTaskInstanceAndTaskFileID(Guid taskFileId, Guid taskInstanceID);
        int DeleteTaskFileByFileID(int FileID);
        void UpdateTaskFile(TaskFileModel taskFile);
        void SaveReviewFileStatus(Guid taskInstanceID, int reviewerUserId, int userId, int? ReviewerProdViewId = null);
        void RenameFiles(List<Tuple<Guid, string, string>> fileRenameList);
        int GetReviewerUserIdByTaskInstanceId(Guid taskInstanceId);
        int CreateFHARequestTasks(string assignedBy, Guid? taskInstanceID, int userId, string userRoleName,bool isPortfolioRequired, bool isCreditReviewRequired);

        #region Production Application
        Prod_TaskXrefModel GetReviewerViewIdByXrefTaskInstanceId(Guid xrefTaskInstanceId);
        TaskModel GetLatestTaskByTaskXrefid(Guid taskXrefId);
         void UpdateTaskAssignment(TaskModel task);
         int GetFolderKeyForChildFileId(Guid ParentTaskFileId);
         void UpdateDocType(TaskFileModel task);
         void UpdateTaskStatus(Guid taskInstanceId);
         bool IsFirmCommitmentExists(Guid taskInstanceId);
        bool IsFirmCommitmentCompleted(Guid taskInstanceId);
        #endregion 

        //Sharepoint
        IList<TaskFileModel> GetFilesByTaskInstanceIdAndFileType(Guid taskInstanceId, FileType fileType);
        IList<SharepointAttachmentsModel> GetTaskFileForSharepoint(Guid taskInstanceId, string fileType);
        void SaveInternalExternalTask(TaskModel task);
        int CheckAmendmentExist(string selectedFhaNumber, int PageTypeId, int ViewId);
        string GetFHANumberByTaskInstanceId(Guid taskInstanceId);
		IEnumerable<TaskModel> GetTasksByFHANumber(string pFHANumber);
		string GetAssignedCloserByFHANumber(string FHANumber);

        //karri#205
        IList<String> getFHAList(string username);
        void setLookUpValues(string lookupFHANumber);
        //Naveen Hareesh 19-11-2019
        IEnumerable<Prod_TaskXrefModel> UpdateTaskStatus1(Guid taskInstanceId);
        // #664harish added new method for latest update 06-02-2020
        //#664 harish added new method to get all taskfiles based on taskinstanceid
        TaskFileModel GetTaskFileByTaskInstance(Guid TaskInstanceId);
        // #664 harish added new method to update folder structure in task file db 31-01-2020
        void UpdateFolderinTaskfile(TaskFileModel task);

        // harish added new method to save OPA files 17-02-2020
        void SaveOPAReviewFileStatus(Guid taskInstanceID, int reviewerUserId, int userId, int? ReviewerProdViewId = null);
        // harish added 06042020
        TaskFileModel GetTaskFileByIdToUpdateDocid(int taskFileId);
        // harish added 06042020
        void UpdateDoctyidinTaskfile(TaskFileModel task);
        // harish added 06042020
        IEnumerable<TaskFileModel> GetAllTaskFileByTaskinstanceidinsteadofTaskGuid(Guid? taskInstanceId);
        //harish added 15052020
        AM_R4RAndNCREDocumentModel GetTransactionId(string actionname);
        //harish added 15052020
        void NcreSaveTask(List<TaskModel> taskList);
    }
}
