﻿using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces
{
    public interface IReviewFileStatusManager
    {
        void SaveReviewFileStatus(ReviewFileStatusModel model);
        IList<Guid> GetRequestAdditionalFileIdByParentTaskInstanceId(Guid parentTaskInstanceId);
        IList<RequestAdditionalInfoFileModel> GetTaskFileIdByChildTaskInstanceId(Guid ChildTaskInstanceId);
        bool UpdateReviewFileStatusForRequestAdditionalResponse(Guid parentTaskFileId);
        bool UpdateReviewFileStatusForTaskFileId(Guid parentTaskFileId);
        bool UpdateReviewFileStatusForFileDownload(Guid parentTaskFileId);
        ReviewFileStatusModel GetReviewFileStatusByTaskFileId(Guid taskFileId);
        bool UpdateReviewFileStatus(IList<ReviewFileStatusModel> reviewStatusList);
        #region Production Application 
        ReviewFileStatusModel GetReviewFileStatusByTaskFileIdandViewId(Guid taskFileId,int viewid);
        #endregion
    }
}
