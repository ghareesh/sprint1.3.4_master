﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.BusinessService.Interfaces
{
    public interface IParentChildTaskManager
    {
        void AddParentChildTask(ParentChildTaskModel model);
        bool IsParentTaskAvailable(Guid childTaskInstanceId);
        ParentChildTaskModel GetParentTask(Guid childTaskInstanceId);
        // #664 harish added new method to get rai request 10-2-2020
        ParentChildTaskModel GetChildTaskbytaskinstanceid(Guid childTaskInstanceId);
    }
}
