﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
    

    public interface ILenderProductionReassignReportManager
    {
       
        // harish added 21042020
        LenderProductionReasignReportModel GetLenderProdReassignmentTasks(string appType, string programType, string lenderUserIds, DateTime? fromDate, DateTime? toDate);

        //harish added 21042020
        void LenderReassignTasks(List<string> kvp, int AssignUserId);
    }
}
