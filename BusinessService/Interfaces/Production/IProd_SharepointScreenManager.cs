﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;
using HUDHealthcarePortal.Model;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_SharepointScreenManager
    {
        bool SaveOrUpdateGeneralInformationDetailsForSharepoint(GeneralInformationViewModel generalInfoModel);
        IList<Prod_SharePointAccountExecutivesViewModel> GetAllSharePointAEs();
        SharePointCommentsModel GetSharepointComments(Guid taskInstanceId);
        bool AddOrUpdateComments(Guid taskInstanceId, string fhaNumber, string comment, int reviewerType);
        MiscellaneousInformationViewModel GetSharePointDetailsForMiscellaneousInfo(
            MiscellaneousInformationViewModel miscInfoModel);
        bool SaveOrUpdateMiscellaneousInfoForSharepoint(MiscellaneousInformationViewModel miscInfoModel);
        void SaveOrUpdateContractorsPayment(Guid taskInstanceId,string fhaNumber, decimal contractorPrice, decimal secondaryAmntPaid, decimal amountPaid);
        SharePointClosingInfoModel GetSharePointClosingInfo(Guid taskInstanceId);
        bool SaveOrUpdateClosingInfo(ClosingInfo model, string fhaNumber);
        string GetNameAndRoleForUser(int userid);
        AllSharepointData GetSharepointDataById(Guid taskInstanceId);
        void SaveOrUpdateOgcAddress(AddressModel address, string fhaNumber, Guid taskInstanceId,int reviewerTypeId);
        AddressModel GetAddressById(int addressId);

        string GetAccountExecutiveByTaskInstanceId(Guid pTaskinstanceId);

        //karri#672
        bool SaveAuditTrialNCREInitBalanceInfo(string fhaNumber_ref, int userid, int attribid, string oldvalue, string newvalue);
    }
}

