﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_RestfulWebApiDelete
    {
        RestfulWebApiResultModel DeleteDocumentUsingWebApi(string DocID, string token);
        RestfulWebApiResultModel DeleteDocumentFolder(string JsonStr, string token);
    }
}
