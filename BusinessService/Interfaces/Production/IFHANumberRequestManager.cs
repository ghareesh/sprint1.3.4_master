﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;

namespace BusinessService.Interfaces.Production
{
    public interface IFHANumberRequestManager
    {
        IList<ActivityTypeModel> GetAllActivityTypes();
        IList<BorrowerTypeModel> GetAllBorrowerTypes();
        IList<ProjectTypeModel> GetAllProjectTypes();
        IList<ProjectTypeModel> GetAllProjectTypesForLC();
         IList<LoanTypeModel> GetALlMortgageInsuranceTypes();
        string GetLenderName(int lenderId);
        Guid AddFHARequest(FHANumberRequestViewModel model);
        IList<CMSStarRatingModel> GetCMSStarRatings();
        bool UpdateFHARequest(FHANumberRequestViewModel model);
        FHANumberRequestViewModel GetFhaRequestById(Guid instanceId);
        List<string> GetReadyforAppFhas();
        List<string> GetReadyforConSingleStageFhas();
        List<string> GetReadyforConTwoStageFhas();
        string GetDisclaimerMsgByPageType(string pageTypeName);
        FHANumberRequestViewModel GetFhaRequestByTaskInstanceId(Guid taskInstanceId);
        IEnumerable<FhaSubmittedLendersModel> GetFhaSubmittedLenders();
        IEnumerable<AssignedunderwriternamesModel> GetprodAssignedUnderwriternames();
        ApplicationDetailViewModel GetApplicationDetailsForSharePointScreen(Guid taskInstanceId);
        
        // bool UpdateLoanAmount(string fhaNumber, decimal loanAmount);///karri#672
        bool UpdateLoanAmount(string fhaNumber, decimal loanAmount, string projName, string origFHANumber, string origProjName, decimal origLoanAmount);
        //bool UpdateLoanAmount(string fhaNumber, decimal loanAmount, string projName, string origFHANumber, string origProjName, decimal origLoanAmount);

        int GetProjectTypebyName(string projectTypeName);
        void AddFhaRequestForIrr(FHANumberRequestViewModel model);
        bool IsRequestExistsForFhaNumber(string fhaNumber);
        IList<Prod_FhaAgingReportViewModel> GetFhaAgingReport();
        FHANumberRequestViewModel GetFhaRequestByFhaNumber(string fhaNumber);
        bool FhataskAssignedfromQueDateUpdate(Guid TaskinstanceId);
        string GetProjectTypeById(int projectTypeId);
        string GetBorrowerTypeById(int borrowerTypeId);
    }
}
