﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;
using Model.Production;

namespace BusinessService.Interfaces.Production
{
   public  interface IProductionQueueManager
   {
       List<UserInfoModel> GetProductionUsers();
		List<UserInfoModel> GetProductionWLMUsers();

        IEnumerable<TaskModel> GetCompletedTasks();//karri#434
        IEnumerable<TaskModel> GetCompletedTasksByType(int type);//karri#434

        IEnumerable<TaskModel> GetProductionTasks();
       IEnumerable<TaskModel> GetProductionQueues();
       IEnumerable<TaskModel> GetProductionTaskByType(int type);
       IEnumerable<TaskModel> GetProductionTaskByStatus(int status);
       IEnumerable<TaskModel> GetProductionTaskByTypeAndStatus(int type , int status);
       IEnumerable<TaskModel> GetProductionQueueByType(int type);
       IEnumerable<TaskModel> GetCompletedFHAsAndNotAssignedChild();
       
       UserViewModel GetUserById(int? userId);
       UserInfoModel GetUserInfoByUsername(string sUserName);
       bool AssignProductionFhaInsert(ProductionTaskAssignmentModel model);
       IEnumerable<ProductionQueueLenderInfo> GetFhaRequests(List<Guid> taskInstances);
       IEnumerable<ProductionMyTaskModel> GetProductionTaskByUserName(string userName, string role);
       //App Process
       List<UserInfoModel> GetUnAssignedProductionUsers();
       List<Prod_ViewModel> GetUnAssignedViewsbyTaskInstanceId(int TypeID, Guid taskinstanceid);
       List<UserInfoModel> GetUnAssignedExternalReviewers();
       List<UserInfoModel> GetALLInternalSpecialOptionUsers();
       IEnumerable<FilteredProductionTasksModel> GetFilteredProductionTasks(int productionType, DateTime dateFrom, DateTime dateTo, int lenderId, int loanType);
       UserViewModel GetUserById(int userId);

       //Sharepoint screen
       IEnumerable<ApplicationAndClosingTypeModel> GetApplicationAndClosingType(Guid taskInstanceId);


        // Form290 Task
        Guid CreateForm290Task(Prod_Form290TaskModel model);
        Guid AddTask(TaskModel model);
        Prod_Form290TaskModel GetForm290TaskBySingleTaskInstanceID(Guid taskInstanceID);
        List<Prod_Form290TaskModel> GetForm290TaskByTaskInstanceID(List<Guid> taskInstanceID);
        Prod_Form290TaskModel GetFrom290TaskByClosingTaskInstanceID(Guid ClosingTaskInstanceID);
        bool AssignForm290Task(ProductionTaskAssignmentModel model);



    }
}
