﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_RestfulWebApiUpdate
    {
        RestfulWebApiResultModel UpdateDocument(RestfulWebApiUpdateModel fileInfo, string token);

        //#664 harish added new method for update folder path in TA 30-01-2020
        RestfulWebApiResultModel UpdateFolderDocument(RestfulWebApiUpdateModel[] fileInfo, string token);
    }
}
