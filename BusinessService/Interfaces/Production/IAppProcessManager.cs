﻿using HUDHealthcarePortal.Model;
using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;

namespace BusinessService.Interfaces.Production
{
  public   interface IAppProcessManager
    {
      string GetProjectTypebyID(int projecttypeid);
      IEnumerable<Prod_FolderStructureModel> GetFolderbyViewId(int ViewId);
      IEnumerable<TaskFileModel> GetMappedFilesbyFolder(int[] folderkeys,Guid InstanceId);
      IEnumerable<DocumentTypeModel> GetMappedDocTypesbyFolder(int folderkey);
      IEnumerable<Prod_FolderStructureModel> GetFolderListbyTaskInstanceId(Guid taskInstanceId);
      IEnumerable<ProductionQueueLenderInfo> GetAppRequests();
      IList<ProjectTypeModel> GetAllotherloanTypes();
      GeneralInformationViewModel GetGeneralInfoDetailsForSharepoint(Guid taskInstanceId);
      MiscellaneousInformationViewModel GetContractUWDetails(Guid taskInstanceId);
      IEnumerable<OPAViewModel> GetProdReviewersTaskStatus(Guid taskInstanceId);
      IEnumerable<OPAViewModel> GetProdPendingRAI(Guid taskInstanceId);
      List<DocumentTypeModel> GetAllDocTypes();

        string GetProjectActionTypebyID(int projecttypeid);
        //harish added new method to get assetmanagementdocs
        IEnumerable<AM_Document> GetAm_Assetmanagement(int Projectiontypeid);

        // harishb added new method to get pending rai
        IEnumerable<OPAViewModel> GetOPAPendingRAI(Guid taskInstanceId);
    }
}
