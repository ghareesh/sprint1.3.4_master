﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Model;
using Model.AssetManagement;
using Model.Production;
using System;

namespace BusinessService.Interfaces
{
    public interface IBackgroundJobMgr
    {
        bool SendUploadNotificationEmails(IEmailManager emailMgr, IEnumerable<string> fhaNumbers);
        bool SendSubmitNotificationEmails(IEmailManager emailMgr, string fhaNumber, string senderEmail, string receiverEmail, string myTaskUrl);
        bool SendR4RAutoApprovalNotificationEmail(IEmailManager emailManager, string aeEmail, string requestorEmail,
            ReserveForReplacementFormModel model);
        bool SendR4RSubmitNotificationEmail(IEmailManager emailManager, string aeEmail, string requestorEmail, ReserveForReplacementFormModel model);
        bool SendR4RApprovalNotificationEmail(IEmailManager emailManager, string requestorEmail, string aeEmail,
            ReserveForReplacementFormModel model);
        bool SendR4RDenyNotificationEmail(IEmailManager emailManager, ReserveForReplacementFormModel model, string denyReason);
        bool SendNonCriticalRequestApprovalNotificationEmail(IEmailManager emailManager, string aeEmail, string requestorEmail,
            NonCriticalRepairsViewModel model);
        bool SendNonCriticalRequestExtensionApprovalNotificationEmail(IEmailManager emailManager, string aeEmail, string requestorEmail,
        NonCriticalRequestExtensionModel model);
        bool SendMissingFhaNotificationEmail(IEmailManager emailManager, MissingFhaNumberEmailModel model);
        bool SendUpdateiREMSNotificationEmail(IEmailManager emailManager, string emailText, string propertyName,
            string fhaNumber);
        bool SendProjectActionSubmissionEmail(IEmailManager emailManager, string assignedTo, string assignedBy, ProjectActionViewModel projectActionViewModel);
        bool SendProjectActionSubmissionResponseEmail(IEmailManager emailManager, string assignedTo, string assignedBy, ProjectActionViewModel projectActionViewModel);
        bool SendTaskReassignmentEmail(IEmailManager emailManager, TaskReAssignmentViewModel model);

        //OPA
        bool SendTPALightPreliminarySubmissionEmail(IEmailManager emailManager, string assignedTo, string assignedBy, OPAViewModel projectActionViewModel);
        bool SendOPASubmissionEmail(IEmailManager emailManager, string assignedTo, string assignedBy, OPAViewModel projectActionViewModel);
        bool SendProjectActionSubmissionResponseEmail(IEmailManager emailManager, string assignedTo, string assignedBy, OPAViewModel opaViewModel);

        //Production - FHA# Request
        bool SendFHARequestCompletionEmail(IEmailManager emailManager, string lenderEmail, string prodUserEmail,
            FHANumberRequestViewModel fhaRequestModel);
        bool SendFHAInsertCompletionEmail(IEmailManager emailManager, string lenderEmail, string prodUserEmail,
            FHANumberRequestViewModel fhaRequestModel);

        //Production-App Process
        bool SendAppProcessSubmitEmail(IEmailManager emailManager,
         OPAViewModel AppProcessModel);

        bool SendAmmendmentAssignmentEmailToUWCloser(IEmailManager emailManager, string userName,
      OPAViewModel AppProcessModel);

        bool SendAppProcessAssignmentEmail(IEmailManager emailManager,string toemail,
      OPAViewModel AppProcessModel);
        bool SendAppProcessLenderRAISubmitEmail(IEmailManager emailManager, List<string> toemails,
    OPAViewModel AppProcessModel);
        string SendLenderNotificationsEmail(IEmailManager emailManager, Prod_EmailToLenderModel NotificationsToLenderModel);

        bool SendIrrInitiationSuccess(IEmailManager emailManager, FHANumberRequestViewModel fhaRequestModel,
            string lenderEmail);
        bool SendFirmCommitmentAppSuccess(IEmailManager emailManager,int Pagetypeid, Guid Taskinstanceid);

        string SendEmailToProdUsers(IEmailManager emailManager, prod_EmailToProdUser notificationEmailToProdUser);
        string SendEmailToProdWLMUsers(IEmailManager emailManager, prod_EmailToProdUser notificationEmailToProdUser);
        bool SendProdTempTaskReassignmentEmail(IEmailManager emailManager, string AssigneFromEmail, string AssigneFromName, string AssignetoEmail, string AssignetoName, string FHANumber, string ProjectName,string ProductionTaskType,string viewname);
        //karri:#149
        bool SendHelpDeskNotificationEmail(IEmailManager emailManager, HD_TICKET_ViewModel model);
        // #234
        //hareewhnew  SendFirmcommittmenttaskEmail
        bool SendFirmcommittmenttaskEmail(IEmailManager emailManager, string assignedTo, string assignedBy, string userInfoData);
        // 234 notification sender
        bool SendFirmCommitmentNotificationEmail(IEmailManager emailManager, ProductionMyTaskModel model);

        //karri:31082019
        bool SendExternalHelpDeskNotificationEmail(IEmailManager emailManager, HD_TICKET_ViewModel model);
        // harish :07-01-2020 
        bool SendRISNotificationEmail(IEmailManager emailManager, TaskModel model);
        // harish :24042020
        bool SendLenderProdTempTaskReassignmentEmail(IEmailManager emailManager, string AssigneFromEmail, string AssigneFromName, string AssignetoEmail, string AssignetoName, string FHANumber, string ProjectName, string ProductionTaskType, string viewname);


    }
}
