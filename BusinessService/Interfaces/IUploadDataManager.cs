﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using EntityObject.Entities.HCP_live.Programmability;
using EntityObject.Entities.HCP_live;

namespace BusinessService.Interfaces
{
    public interface IUploadDataManager
    {
        void CalcAndUpdateUploadedData();
        IEnumerable<ReportExcelUpload_Model> GetDataUploadSummaryByUserId(int iUserId);
        ExcelUploadView_Model GetProjectDetailByID(int ldiID, DBSource source);
        IEnumerable<DerivedUploadData> GetUploadDataByRole(int iUserId, string[] userRoles);
        IEnumerable<DerivedUploadData> GetUploadDataBySearch(int iUserId, string[] userRoles);
        IEnumerable<DerivedUploadData> GetUploadDataByTime(DateTime myTimeInserted);
        IEnumerable<DerivedUploadData> GetUploadDataByTimeForSearch(DateTime myTimeInserted);
        IEnumerable<DerivedUploadData> GetUploadDataByCategory(string category);
        PaginateSortModel<DerivedUploadData> GetUploadDataByCategoryForProjectReport(string category);
        IEnumerable<DerivedUploadData> GetUploadDataByCategoryAndQuarter(string category, string quarter);
        IEnumerable<DerivedUploadData> GetUploadDataByCategoryAndQuarterBySearch(string category, string quarter);
        IEnumerable<UploadStatusViewModel> GetUploadStatusByLenderId(int lenderId);
        IEnumerable<UploadStatusDetailModel> GetUploadStatusDetailByLenderId(int lenderId, string selectedQtr);
        PaginateSortModel<DerivedUploadData> GetUploadDataByRoleWithPageSort(int iUserId, string[] userRoles,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum);
        PaginateSortModel<DerivedUploadData> GetUploadLiveDataByCategoryPageSort(string category, string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, int monthsInPeriod, int year);
        PaginateSortModel<DerivedUploadData> GetUploadLiveDataByCategoryPageSort(string category, string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, string quarter);
        PaginateSortModel<DerivedUploadData> GetUploadedDataByTimePageSort(DateTime timeInserted, int userId,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum);
        List<string> GetAllowedFhaByLenderUserId(int lenderUserId);
        string GetErrorMsgForEmptyFields(int rowNumber, string columnName);
        string GetErrorMsgForInvalidFields(int rowNumber, string columnName, string dataType);
        List<string> GetCurrentQuarters();
        IEnumerable<UploadStatusViewModel> GetUploadStatusDetailByQuarter(string quarter, int lenderId);
        IEnumerable<FhaInfoModel> GetFhasByLenderIds(string lenderIds);
        void UpdateLenderPropertyInfo(int userId, int lenderId);
        int SaveFormUploadToIntermediateTbl(FormUploadModel model);
        IEnumerable<ExcelUploadView_Model> GetUploadedDataByQuarter(int monthsInPeriod, int year);
        IEnumerable<ReportViewModel> GetMissingProjectsForLender(int lenderId, string selectedQtr);
        IDictionary<string, string> GetQuartersList();
        string GetLatestQuarter(string type);
        IEnumerable<string> GetAllowedFhasForUser(int userId);
        bool IsValidLender(int lenderId);
        IEnumerable<ProjectInfoViewModel> GetProjectInfoReport(string username);
        int DistributeQuarterly();
        int ApplyCalculations(string QryDate = "4/21/2016");
        int CalculateErrors();
        IEnumerable<int> GetLenderOperatorsbyFHA(string fhanumber);
        IEnumerable<string> GetAllowedFhasForUsers(List<int> UserIds);
        IEnumerable<KeyValuePair<int, string>> GetPeriodEnding(string QryDate = "4/21/2016");
        List<string> GetExistingFHAs(List<string> uploadItems);
        Boolean CheckLenderFiscalYear(long lenderId, string fha, DateTime periodEnding, int monthsInPeriod,
            out bool existInSpreadSheet, out bool existInIntermidiate, string queryDate);
        IEnumerable<FhaInfoModel> GetReadyFhasForInspectionContracotr();
        IEnumerable<FhaInfoModel> GetSelectedFhasForInspector(int userId);
        //written by siddesh  29082019 #344//
        IEnumerable<usp_HCP_GetTicketDetails_Latest> GetUploadStatusDetailByhelpdeskreport(string Status, string FromDate, string ToDate, string HelpDeskName);

        // harish added #584
        //harish added #584
        // int? lenderrowdata(long lenderid, string fha);
        //harish added #584
        LenderFiscalYearDetails lenderrowdata(long lenderid, string fha);
        //harish added #584
        int CreateLenderFisicalyear(LenderFiscalYearDetails model);
        //harish added #584
        int UpdateLenderFisicalyear(LenderFiscalYearDetails model);

    }
}
