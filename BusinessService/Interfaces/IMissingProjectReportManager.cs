﻿using HUDHealthcarePortal.Core;
using Model;

namespace BusinessService.Interfaces
{
    public interface IMissingProjectReportManager
    {
        ReportModel GetMissingProjectReport(HUDRole hudRole, string userName, int year);
        ReportModel GetSuperUserMissingProjectReport(int year);
        ReportModel GetSuperUserMissingProjectReportWithWLM(string userName, int year);
        ReportModel GetWorkloadManagerMissingProjectReport(string userName, int year);
        ReportModel GetAccountExecutiveMissingProjectReport(string userName, int year);
    }
}
