﻿using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces
{
    public interface IRequestAdditionalInfoFileManager
    {
        void SaveRequestAdditionalInfoFiles(IList<RequestAdditionalInfoFileModel> fileList);
    }
}
