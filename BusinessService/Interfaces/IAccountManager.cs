﻿using System.Collections.Generic;
using System.Linq;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace BusinessService.Interfaces
{
    public interface IAccountManager
    {
        string CreateUser(UserViewModel userModel);
        List<KeyValuePair<string, string>> GetAllStates();
        IEnumerable<KeyValuePair<int, string>> GetInstitutionsByRoles(string[] roles);
        void UnlockUser(int userId);
        void InitForceChangePassword(int userId);
        UserViewModel GetUserById(int userId);
        void SavePasswordHist(string userName, string passwordNew);
        bool IsPasswordInHistory(string userName, string passwordNew);
        int? GetUserIdByToken(string userToken);
        int? GetUserIdByResetPwdToken(string resetPwdToken);
        SecurityQuestionViewModel GetSecQuestionsByUserId(int userId);
        void SaveUserSecQuestions(int userid, SecurityQuestionViewModel securityQuesionModel);
        IEnumerable<UserViewModel> GetDataAdminManageUser(string searchText, int page, string sort, SqlOrderByDirecton sortdir);
        IEnumerable<ReportsModel> GetMultipleLoanProperty();
        IEnumerable<UserViewModel> GetAllUsers(string searchText, int page, string sort, SqlOrderByDirecton sortdir);
        IEnumerable<UserViewModel> SearchByFirstOrLastName(string searchText, int maxResults);
        PaginateSortModel<UserViewModel> SearchByFirstOrLastNamePageSort(string searchTerm, string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum);
        List<HUDRole> GetHUDRoles();
        void UpdateUser(UserViewModel model);
        IEnumerable<FhaInfoModel> GetFHAsByLenders(string lenderIds);
        UserViewModel GetUserByUsername(string username);
        string GetUserNameById(int userId);
        IEnumerable<FhaInfoModel> GetFhasByWLMId(int wlmid);
        IEnumerable<UserInfoModel> GetOperatorsByLenderId(int lenderId);
        IEnumerable<UserInfoModel> GetLenderUsersByLenderId(int lenderId);
        IEnumerable<UserInfoModel> GetLenderUsersByAeUserId(int aeUserId);
        IEnumerable<KeyValuePair<int, string>> GetLendersByUserRole();
        IEnumerable<KeyValuePair<int, string>> GetWorkloadManagers();
        IEnumerable<KeyValuePair<int, string>> GetAccountExecutives();
        UserViewModel GetWorkloadManagerDetail(int id);
        UserViewModel GetAccountExecutiveDetail(int id);
        IEnumerable<int> GetLenderIdsByAeOrWlmForQtr(string userName, string quarter);
        void ActivateOrInactivateUser(int userId, bool isUserInactive);
        bool IsUserInactive(string userName);
        bool IsUserInactive(int userId);
        bool UpdateOarFhas(UserViewModel originalUserViewModel, IEnumerable<KeyValuePair<string, string>> originalFhas, IEnumerable<KeyValuePair<string, string>> selectedFhas);
        IEnumerable<int?> GetLendersByUserId(int userId);
        IEnumerable<KeyValuePair<int, string>> GetLenderDetail(int lenderId);
        List<int?> GetLenderIdsByUser(int userId);
        IList<FormTypeModel> GetAllFormTypesForProjectAction();
        int GetUserId(string userName);

        string GetFullUserNameById(int pUserId);//skumar-form290 - get full name by userid

        string GetUserLastNameByFirstName(string firstname);
        bool checkUserEmailId(string email);

        IEnumerable<FhaInfoModel> GetFHAsForServicer(string lenderId, string servicerId);
        bool deleteuser(List<string> Fhanumbs, int Userid);

        //Naveen#543
        IEnumerable<UserViewModel> GetServicer(int? lenderid);
        //void deleteuser(List<string> fhas);

    }
}
