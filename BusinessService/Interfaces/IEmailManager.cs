﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Model;
using Model.AssetManagement;
using Model.Production;

namespace BusinessService.Interfaces
{
    public interface IEmailManager
    {
        void SaveEmail(HUDHealthcarePortal.Model.EmailModel email);
        bool SendUploadNotificationEmails(IEnumerable<string> fhaNumbers);
        bool SendSubmitNotificationEmails(string fhaNumber, string senderEmail, string receiverEmail, string myTaskUrl);
        void SendUploadNotificationEmailsDelegate(Object threadContext);
        bool SendCommentEmail(Dictionary<string, string> authors, string replyToUserName, string subject, string htmlEmailContent, string rawUrl);

        /// <summary>
        /// dictionary is in key: aeEmail, value: dictionary of key: fha number, value: lender name
        /// </summary>
        /// <param name="fhaNumbers"></param>
        /// <returns></returns>
        Dictionary<string, Dictionary<string, string>> GetAEEmailsByFhaNumbers(IEnumerable<string> fhaNumbers);

        bool SendR4RAutoApprovalNotificationEmail(string aeEmail, string requestorEmail,
            ReserveForReplacementFormModel model);

        bool SendR4RSubmitNotificationEmail(string aeEmail, string requestorEmail, ReserveForReplacementFormModel model);

        bool SendR4RApprovalNotificationEmail(string requestorEmail, string aeEmail, ReserveForReplacementFormModel model);

        bool SendNonCriticalRequestApprovalNotificationEmail(string aeEmail, string requestorEmail,
            NonCriticalRepairsViewModel  model);
        
		bool SendNonCriticalRequestExtensionApprovalNotificationEmail(string aeEmail, string requestorEmail,
         	NonCriticalRequestExtensionModel  model);

        bool SendMissingFhaNotificationEmail(MissingFhaNumberEmailModel model);

        bool SendUpdateiREMSNotificationEmail(string emailText, string propertyName, string fhaNumber);

        int SendEmail(EmailType emailTypeID, string keywords, string recipientIds, bool firstIdIsPMId = false, bool isBodyHtml = false);
        void SendEmail(string fromEmail, string body, string toEmail, string subject, string attachmentPath);
        bool SendProjectActionSubmissionEmail(EmailType emailTypeID, string requestorEmail, string aeEmail, ProjectActionViewModel projectActionViewModel);

        bool SendTaskReassignmentEmail(EmailType emailTypeID, TaskReAssignmentViewModel model);

        bool SendTPALightPreliminarySubmissionEmail(EmailType emailTypeID, string requestorEmail, string aeEmail, OPAViewModel projectActionViewModel);
        bool SendOPASubmissionEmail(EmailType emailTypeID, string requestorEmail, string aeEmail, OPAViewModel projectActionViewModel);

        bool SendFHARequestCompletionEmail(EmailType emailTypeID, string lenderEmail, string prodUserEmail,
            FHANumberRequestViewModel fhaRequestModel);

        bool SendFHAInsertCompletionEmail(EmailType emailTypeID, string lenderEmail, string prodUserEmail,
            FHANumberRequestViewModel fhaRequestModel);
        //Production App Process
        bool SendAppProcessSubmitEmail(EmailType emailTypeID,
       OPAViewModel AppProcessModel);

        bool SendAmmendmentAssignmentEmailToUWC(EmailType emailTypeID, string toemail, OPAViewModel AppProcessModel);
        bool SendAppProcessAssignmentEmail(EmailType emailTypeID,string toemail,
    OPAViewModel AppProcessModel);
        bool SendAppProcessLenderRAISubmitEmail(EmailType emailTypeID, List<string> toemails,
    OPAViewModel AppProcessModel);
        string SendLenderNotificationsEmail(EmailType emailTypeID, Prod_EmailToLenderModel LenderNotificationsEmailModel);

        //IRR
        bool SendIrrInitiationSuccess(EmailType emailTypeID, FHANumberRequestViewModel fhaRequestModel,
            string lenderEmail);

        //Firm Commitment App Process)
        bool SendFirmCommitmentAppSuccess(int Pagetypeid, Guid Taskinstanceid);

        //IRR App and Closing emails
        string SendIRRApprovalNotificationEmail(EmailType emailTypeID, Prod_EmailToLenderModel LenderNotificationsEmailModel);
        string SendIRRFinalApprovalEmail(EmailType emailTypeID, Prod_EmailToLenderModel LenderNotificationsEmailModel);
        string SendIRRLoanModificationEmail(EmailType emailTypeID, prod_EmailToProdUser prodUserNotificationsEmailMode);
        /// <summary>
        /// Reserve for replacement form deny email notification 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="denyReason"></param>
        /// <returns></returns>
        bool SendR4RDenyNotificationEmail(ReserveForReplacementFormModel model, string denyReason);
        bool SendProdTempTaskReassignmentEmail(EmailType emailTypeID, string AssigneFromEmail, string AssigneFromName, string AssignetoEmail, string AssignetoName, string FHANumber, string ProjectName, string ProductionTaskType, string viewname);

        //karri:#149
        bool SendHelpDeskNotificationEmail(string toEmail, string requestorEmail, HD_TICKET_ViewModel model);
        //#234
        bool SendFirmcommitmentNotificationEmail(string toEmail, string requestorEmail, string userInfoDataString);
        // 234 email notification hareesh
       // bool SendFirmCommittmentNotificationEmail(string toEmail, string requestorEmail, ProductionMyTaskModel model);
        bool SendFirmCommittmentNotificationEmail(IEmailManager emailManager, ProductionMyTaskModel model);

        //karri:31082019
        bool SendExternalHelpDeskNotificationEmail(string toEmail, string requestorEmail, HD_TICKET_ViewModel model);

        // harish :07-01-2020
        bool SendRISNotificationEmail(IEmailManager emailManager, TaskModel model);
        // Harish added this below method to send Lender reassignement Task to reassign user 24-04-2020
        bool SendLenderProdTempTaskReassignmentEmail(EmailType emailTypeID, string AssigneFromEmail, string AssigneFromName, string AssignetoEmail, string AssignetoName, string FHANumber, string ProjectName, string ProductionTaskType, string viewname);
    }
}
