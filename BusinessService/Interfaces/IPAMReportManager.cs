﻿using System;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Core;
using Model;
using HUDHealthcarePortal.Model.AssetManagement;
using System.Collections;
using System.Collections.Generic;
namespace BusinessService.Interfaces
{
    public interface IPAMReportManager
    {
        //PAMReportModel GetPAMSummary(string userName,string isouIds, string aeIds,DateTime? fromDate, DateTime? toDate, int wlmId, int aeId,int isouId, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds);
        PAMReportModel GetPAMSummary(string userName, string isouIds, string aeIds, DateTime? fromDate, DateTime? toDate, int wlmId, int aeId, int isouId, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds
            , string propertyName, string fhaNumber, string assignedTO, int? hasOpened, int? daysOpened);//karri:D#610

        IList<HUDManagerModel> GetSubordinateAEsByWLMId(string wlmId);
        IList<HUDManagerModel> GetSubordinateISOUsByWLMId(string wlmId);
        int GetWlmId(int userId);
        int GetAeId(int userId);
        int GetIsouId(int userId);
        ReserveForReplacementFormModel GetR4RRequest(Guid r4rId);
        IEnumerable<KeyValuePair<int, string>> GetAllWorkLoadManagers();
        NonCriticalRepairsViewModel GetNCRRequest(Guid ncrId);
        NonCriticalRequestExtensionModel GetNCRExtensionRequest(Guid ncrexId);

        PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string aeIds, string isouIds, string wlmIds, int? status,
            string projectAction, int wlmId, int aeId, int isouId, DateTime? fromDate, DateTime? toDate);

        IEnumerable<KeyValuePair<int, string>> GetPamProjectionTypes();
        ProjectActionViewModel GetProjectActionRequestId(Guid formId);
        OPAViewModel GetOPARequestId(Guid opaFormId);
        string GetDisclaimerMsgByPageType(string pageTypeName);// User Story 1901
    }
}
