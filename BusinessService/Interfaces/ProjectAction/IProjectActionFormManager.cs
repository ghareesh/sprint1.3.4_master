﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using Repository.Interfaces.ProjectAction;
using Model;
using Model.Production;

namespace BusinessService.ProjectAction
{
    public interface IProjectActionFormManager
    {
        //#293 Hareesh
        List<string> GetAllowedFhaByLender();
        // helpdesk fha number hareesh
        List<string> GetAllowedFhaByLenderdetails(int lenderUserId);
        List<string> GetAllowedFhaByLenderUserId(int userId);
        string GetLenderName(int lenderId);
        PropertyInfoModel GetPropertyInfo(string fhaNumber);
        IEnumerable<KeyValuePair<int, string>> GetProjectActionTypes();
        IDictionary<string, IList<QuestionViewModel>> GetCheckListByActionType(Guid projectActionFormId, int projectActionTypeId, bool submitted = false, Guid taskInstanceID = new Guid());
        ProjectActionViewModel GetContactAddressInfo(ProjectActionViewModel projectActionViewModel);
        string GetProjectActionName(int proejctActionId);
        Guid SaveProjectActionRequestForm(ProjectActionViewModel projectActionViewModel);
        string GetAeEmailByFhaNumber(string fhaNumber);
        TaskModel GetLatestTaskId(Guid taskInstanceId);
        void UpdateTaskId(ProjectActionViewModel model);
        void UpdateProjectActionRequestForm(ProjectActionViewModel projectActionViewModel);
        ProjectActionViewModel GetProjectActionFormById(Guid projectActionFormId);
        void InsertAEChecklistStatus(ProjectActionViewModel projectActionViewModel);
        bool IsApprovalStatusTrueForAllQuestions(Guid projectActionFormId);
        void UpdateAECheckList(ProjectActionViewModel projectActionViewModel);
        bool IsProjectActionApprovedOrDenied(Guid projectActionFormId);

        //OPA
        Guid SaveOPAForm(OPAViewModel model);
        void UpdateProjectActionRequestForm(OPAViewModel opaViewModel);
        OPAViewModel GetContactAddressInfo(OPAViewModel opaViewModel);
        void UpdateTaskId(OPAViewModel model);
        string GetOPAName(int opaId);
        List<OPAHistoryViewModel> GetOpaHistory(Guid taskInstanceId);
        List<OPAHistoryViewModel> GetOpaFileCommentHistory(int taskFileId);
        List<OPAHistoryViewModel> GetOpaFileUploadHistory(int taskFileId);
        List<TaskModel> GetChildTasks(Guid taskInstanceId);

        //void UpdateAdditionalInfo(OPAViewModel opaViewModel);
        OPAViewModel OPATaskbyFHAProjectAction(int projectActionTypeId, string fhaNumber);
        int GetPageTypeIdByName(string pageTypeName);
        string GetPageTypeNameById(int pPageTypeId);

        OPAViewModel GetOPAByTaskId(int taskId);
        OPAViewModel GetOPAByChildTaskId(Guid childTaskInstanceId);
        List<OPAHistoryViewModel> GetOpaHistoryForRequestAdditionalInfo(Guid childTaskInstanceId);
        OPAViewModel GetOPAByTaskInstanceId(Guid taskIInstanceId);
        void SaveLenderRequestAdditionalFiles(TaskFileHistoryModel model);
        int DeleteChildTaskFile(Guid childTaskFileId);
        void SaveNewFileRequest(NewFileRequestModel model);
        List<Guid> GeetChildTaskforReassignment(List<Guid> Joinlist, string assignedto);
        bool IsFileHistoryExists(Guid taskFileId);
        void SaveChildTaskNewFiles(ChildTaskNewFileModel newFile);
        Prod_Assainedto AssignedTo(int Pageid, string FhaNumber);
        // harish added new method 09-12-2019
        List<Prod_OPAHistoryViewModel> GetProdLenderUploadForAssetManagement(Guid taskInstanceId, string PropertyId, string FhaNumber);
        //#664 Harish added 02-01-2020
        List<OPAHistoryViewModel> GetOpaHistoryRAI(Guid taskInstanceId);

        // harish added new line of code 29-01-2020
        int Updateprojectactiontypeid(OPAViewModel model, int Projectactiontypeid);
        // harish added new method to get assetmanagement projectactiontypes 11032020
        string GetProjectActionNameforAssetmanagement(int projectActionId);

        #region Production Application
        List<OPAHistoryViewModel> GetProdOpaHistoryReviewersList(int userId, int viewId, int fileId);
        List<OPAHistoryViewModel> GetProdOpaHistoryFileList(Guid taskInstanceId, int userId, int viewId, int folderKey);
        //List<OPAHistoryViewModel> GetProdOpaHistoryFileList(Guid taskInstanceId, int userId, int viewId, int folderKey, int notAssigned);
        List<Prod_OPAHistoryViewModel> GetProdOpaHistory(Guid taskInstanceId, string PropertyId, string FhaNumber, int userId, int viewId, bool isEdit);
        List<Prod_OPAHistoryViewModel> GetProdLenderUpload(Guid taskInstanceId, string PropertyId, string FhaNumber);
        List<Prod_OPAHistoryViewModel> GetProdLenderUploadForAmendments(Guid taskInstanceId, string PropertyId, string FhaNumber);
        List<Prod_SubFolderStructureModel> GetProdSubFolders(int folderkey, string PropertyID, string FhaNumber);
        List<Prod_SubFolderStructureModel> GetFolderInfoByKey(int folderkey);
        List<int> GetFileIDByFolderKey(int folderkey);
        int SaveProdSubFolders(Prod_SubFolderStructureModel model);
        PropertyInfoModel GetProdPropertyInfo(string fhaNumber);
        PropertyInfoModel GetAssetManagementPropertyInfo(string fhaNumber);
        OPAViewModel GetProdOPAByChildTaskId(Guid childTaskInstanceId);
        OPAViewModel GetProdOPAByTaskXrefId(Guid taskXrefId);
        List<TaskModel> GetProdChildTasksByXrefId(Guid taskXrefId, bool isXref);
        OPAViewModel GetProdOPAByFha(string FhaNumber);
        List<Prod_OPAHistoryViewModel> GetGetWPUpload(Guid taskInstanceId, int Status, int PageTypeId);
        bool IsFileExistsForChildTask(Guid childTaskInstanceId);
        string GetTaskId(int pageTypeId, string FHANumber, Guid TaskInstanceId);
        object FormNameBYPageTypeID(int pageTypeId);

        #endregion

        #region Amendments
        List<FirmCommitmentAmendmentTypeModel> GetAllFirmCommitmentAmendmentTypes();
        List<SaluatationTypeModel> GetAllSaluatationTypes();
        List<PartyTypeModel> GetAllPartyTypes();
        List<DepositTypeModel> GetAllDepositTypes();
        List<EscrowEstimateTypeModel> GetAllEscrowEstimateTypes();
        List<RepairCostEstimateTypeModel> GetAllRepairCostEstimateTypes();
        List<SpecialConditionTypeModel> GetAllSpecialConditionTypes();
        List<ExhibitLetterTypeModel> GetAllExhibitLetterTypes();

        string GetSaluatationTypeName(int pId);
        string GetPartyTypeName(int pId);
        string GetDepositTypeName(int pId);
        string GetEscrowEstimateTypeName(int pId);
        string GetExhibitLetterTypeName(int pId);
        string GetRepairCostEstimateTypeName(int pId);
        string GetSpecialConditionTypeName(int pId);



        #endregion
        #region Web Signature
        int AddNewSignature(HUDSignatureModel pModel);
        HUDSignatureModel GetHUDSignatureByUserId(int pId);
        void UpdateHUDSignatureByUserId(HUDSignatureModel pModel);

        bool SaveAmendment(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel);
        Prod_FormAmendmentTaskModel GetFormAmendmentById(int pAmendmentTemplateID);
        //Prod_FormAmendmentTaskModel GetFormAmendmentByTaskInstanceId(Guid pTaskInstanceId);
        IList<Prod_FormAmendmentTaskModel> GetFormAmendmentByTaskInstanceId(Guid pTaskInstanceId);
        IList<Prod_FormAmendmentTaskModel> GetFormAmendmentByFHANumber(string pFHANumber);
        bool SaveDapFromAmendmentSP(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel);


        #endregion

        //karri, venkat#832
        int GetForm290ClosingPreChecksValue(string FHANumber);
    }
}
