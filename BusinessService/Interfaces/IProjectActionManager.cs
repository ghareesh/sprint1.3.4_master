﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;

namespace HUDHealthcarePortal.BusinessService.Interfaces
{
    public interface IProjectActionManager
    {
        IList<ProjectActionTypeViewModel> GetAllProjectActions();
        IList<ProjectActionTypeViewModel> GetAllProjectActionsByPageId(int pageTypeId);
        IList<ProjectActionTypeViewModel> GetAllProjectActionsByProjecttype(int projectActionTypeId);
    }
}
