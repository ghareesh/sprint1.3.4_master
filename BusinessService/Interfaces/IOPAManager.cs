﻿using HUDHealthcarePortal.Model;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces
{
    public interface IOPAManager
    {

        List<OPAWorkProductModel> getAllOPAWorkProducts(Guid taskInstanceId, string fileType);
        //List<OPAWorkProductModel> getAllOPAWorkProducts(Guid taskInstanceId);
        //Naveen Hareesh 19-11-2019
        void UpdateOPAGroupTask1(OPAViewModel OPAActionViewModel);

        //#605 siddu
        List<OPAWorkProductModel> getAllOPAWorkInternalProducts(Guid taskInstanceId);
        // harish added new method to get upload files 07-01-2020
        List<OPAWorkProductModel> RISgetAllOPAWorkInternalProducts(Guid taskInstanceId);
    }
}
