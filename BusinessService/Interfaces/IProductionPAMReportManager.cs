﻿using System;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Core;
using Model;
using HUDHealthcarePortal.Model.AssetManagement;
using System.Collections;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using Model.Production;

namespace BusinessService.Interfaces
{
    public interface IProductionPAMReportManager
    {
        //PAMReportModel GetPAMSummary(string userName, string aeIds, DateTime? fromDate, DateTime? toDate, int wlmId, int aeId, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds);
        //IList<HUDManagerModel> GetSubordinateAEsByWLMId(string wlmId);
        //int GetWlmId(int userId);
        //int GetAeId(int userId);
        //ReserveForReplacementFormModel GetR4RRequest(Guid r4rId);
        //IEnumerable<KeyValuePair<int, string>> GetAllWorkLoadManagers();
        //NonCriticalRepairsViewModel GetNCRRequest(Guid ncrId);
        //NonCriticalRequestExtensionModel GetNCRExtensionRequest(Guid ncrexId);

        //PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string aeIds, string wlmIds, int? status,
        //    string projectAction, int wlmId, int aeId, DateTime? fromDate, DateTime? toDate);

        //IEnumerable<KeyValuePair<int, string>> GetPamProjectionTypes();
        //ProjectActionViewModel GetProjectActionRequestId(Guid formId);
        //OPAViewModel GetOPARequestId(Guid opaFormId);
        //string GetDisclaimerMsgByPageType(string pageTypeName);// User Story 1901

          IList<UserInfoModel> GetProductionUsers();
          IList<FormTypeModel> GetProductionPageTypes();
          IList<PamStatusViewModel> GetPAMStatusList();
          IList<ProjectTypeModel> GetProjectTypeModels();
          IList<ProjectTypeModel> GetProjectTypesByAppType(string typeIds);
          ProductionPAMReportModel GetProductionPAMReportSummary(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate, string status, string Level, Guid? grouptaskinstanceid, string Mode = "Other");
          ProductionPAMReportModel GetProductionLenderPAMReportSummary(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes);
          ProductionPAMReportModel GetLenderPAMSubGridChildTasks(Guid taskInstanceId);
          ProductionPAMReportModel GetProductionLenderPAMExcel(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes);
          IEnumerable<UserInfoModel> GetLenderUsersByRoleLenderId(int lenderId, string roleName);
          IEnumerable<KeyValuePair<int, string>> GetUserNameByIDs(string userIDList);
        // IList<ProdLoanTypeViewModel> GetProductionProgramTypes();

        IList<FHANumberRequestViewModel> GetAllFHARequests();
        PAMReportTestGrid1ViewModel GetProdPAMGridOneResults(Guid taskInstanceId);
        PAMReportTestGrid2ViewModel GetProdPAMGridTwoResults(Guid taskInstanceId);
        IList<PAMReportV3ViewModel> GetProdHUDPAMReportResults();
		IList<PAMReportV3ViewModel> GetProdHUDPAMSubReportResults(Guid pTaskInstanceId, string pFHANumber);//skumar-2838
		ProdPAMReportStatusGridViewModel GetProdPAMReportStatusGridResults();
         string SaveHUDPamFilters(HUDPamReportFiltersModel Model);
        HUDPamReportFiltersModel GetSavedFilter();
        string GetUserInfoByID(int UserID);

        string SaveAssetPamReportFilterDetails(HUDAssetpamreportfiltermodel model);
        HUDAssetpamreportfiltermodel GetAssetSavedFilter();
        // harish added 21042020
        IList<UserInfoModel> GetAllLenderUsers(int? lenderid);
    }
}
