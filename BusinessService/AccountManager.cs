﻿using BusinessService.Interfaces;
using Core.Utilities;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.ExceptionHandling;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Security;
using HUDHealthcarePortal.Repository.Interfaces;
using Repository;
using Repository.Interfaces;
using WebMatrix.WebData;

namespace HUDHealthcarePortal.BusinessService
{
    /// <summary>
    /// This class has business logic related to managing all the user accounts
    /// </summary>
    public class AccountManager : IAccountManager
    {
        private IAddressRepository addressRepository;
        private IUserLenderServicerRepository userLenderServicerRepository;
        private IUserRepository userRepository;
        private ILenderInfoRepository lenderInfoRepository;
        private IServicerInfoRepository servicerInfoRepository;
        private IStatesRepository statesRepository;
        private IWebMembershipRepository webMembershipRepository;
        private IUserSecQuestionRepository userSecQuestionRepository;
        private IAdminManagerUsersRepository adminManagerUsersRepository;
        private IReportsRepository reportsRepository;
        private IFhaRepository fhaRepository;
        private IHUDWorkloadManagerRepository workloadManagerRepository;
        private IHUDAccountExecutiveRepository accountExecutiveRepository;
        private IUserWorkloadProjectManagerRepository userWorkloadProjectManagerRepository;
        private IHUDWorkloadInternalSpecialOptionUserRepository hudWorkloadInternalSpecialOptionUserRepository;
        private IProjectInfoRepository projectInfoRepository;
        private ILenderFhaRepository lenderFhaRepository;
        private IFormTypeRepository formTypeRepository;

        // don't use static unit of work, aka context, context should only last per http request due to entity framework identity pattern
        // may get stale data
        private UnitOfWork unitOfWork;
        private const string PASSWORD_DELIMITER = @"{{}}";
        private const int PASSWORD_HIST_COUNT = 8;

        public static readonly int MaxLoginTrials = int.Parse(ConfigurationManager.AppSettings["MaxLoginTrials"] ?? ConfigurationSettings.AppSettings["MaxLoginTrials"]);
        public static readonly int LockAccountInSeconds = int.Parse(ConfigurationManager.AppSettings["LockAccountInSeconds"] ?? ConfigurationSettings.AppSettings["LockAccountInSeconds"]);
        public static readonly int ChangePasswordInDays = int.Parse(ConfigurationManager.AppSettings["ChangePasswordInDays"] ?? ConfigurationSettings.AppSettings["ChangePasswordInDays"]);
        public static readonly int DaysToAskChangePassword = int.Parse(ConfigurationManager.AppSettings["DaysToAskChangePassword"] ?? ConfigurationSettings.AppSettings["DaysToAskChangePassword"]);

        public AccountManager()
        {
            unitOfWork = new UnitOfWork(DBSource.Live);
            addressRepository = new AddressRepository(unitOfWork);
            userLenderServicerRepository = new UserLenderServicerRepository(unitOfWork);
            userRepository = new UserRepository(unitOfWork);
            lenderInfoRepository = new LenderInfoRepository(unitOfWork);
            servicerInfoRepository = new ServicerInfoRepository(unitOfWork);
            statesRepository = new StatesRepository(unitOfWork);
            webMembershipRepository = new WebMembershipRepository(unitOfWork);
            adminManagerUsersRepository = new AdminManagerUsersRepository(unitOfWork);
            userSecQuestionRepository = new UserSecQuestionRepository(unitOfWork);
            reportsRepository = new ReportsRepository(unitOfWork);
            fhaRepository = new FhaRepository(unitOfWork);
            workloadManagerRepository = new HUDWorkLoadManagerRepository(unitOfWork);
            accountExecutiveRepository = new HUDAccountExecutiveRepository(unitOfWork);
            userWorkloadProjectManagerRepository = new UserWorkloadProjectManagerRepository(unitOfWork);
            projectInfoRepository = new ProjectInfoRepository(unitOfWork);
            lenderFhaRepository = new LenderFhaRepository(unitOfWork);
            formTypeRepository = new FormTypeRepository(unitOfWork);
            hudWorkloadInternalSpecialOptionUserRepository = new HUDWorkloadInternalSpecialOptionUserRepository(unitOfWork);
        }

        /// <summary>
        /// return value is user token for email confirmation of created user
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public string CreateUser(UserViewModel userModel)
        {
            try
            {
                if (userModel.IsUserExist == false)
                {
                    ExceptionManager.WriteToEventLog(userModel.ToString(), "Application",
                        System.Diagnostics.EventLogEntryType.Error);
                    if (userModel.AddressID == null)
                    {
                        userModel.AddressID = addressRepository.AddNewAddress(userModel.AddressModel);
                    }
                    else
                    {
                        addressRepository.UpdateAddress(userModel.AddressModel);
                    }

                    var userInforRepository = new UserInfoRepository(unitOfWork);
                    // store hashed and salted password
                    string passwordHist = PasswordHash.CreateHash(userModel.Password);
                    ExceptionManager.WriteToEventLog("passwordHist:" + passwordHist, "Application",
                        System.Diagnostics.EventLogEntryType.Error);
                    string userToken = WebSecurity.CreateUserAndAccount(userModel.UserName, userModel.Password, new
                    {
                        userModel.FirstName,
                        userModel.LastName,
                        UserMInitial = userModel.MiddleName,
                        userModel.AddressID,
                        PreferredTimeZone = userModel.SelectedTimezone,
                        ModifiedBy = UserPrincipal.Current.UserId,
                        ModifiedOn = DateTime.UtcNow,
                        PasswordHist = passwordHist,
                        IsRegisterComplete = false
                    }, true);
                    ExceptionManager.WriteToEventLog("usertoken:" + userToken, "Application",
                        System.Diagnostics.EventLogEntryType.Error);
                    var newRoles = new List<string>();
                    //  retrieve roles from session and save update user roles
                    newRoles = SessionHelper.SessionExtract<List<string>>(SessionHelper.SESSION_KEY_NEW_ROLES);
                    if (newRoles == null)
                    {
                        newRoles = new List<string>();
                        if (userModel.RoleName == "Reviewer")
                        {
                            newRoles.Add(userModel.RoleName);
                        }
                    }

                    if (newRoles.Count > 0)
                        Roles.AddUserToRoles(userModel.UserName, newRoles.ToArray());
                    var fhasAssigned = new List<string>();
                    string WorkLoadManagerId = "";
                    if (newRoles.Contains("OperatorAccountRepresentative") || newRoles.Contains("Servicer") ||
                        newRoles.Contains("InternalSpecialOptionUser"))
                    {
                        var fhasSelected =
                            SessionHelper.SessionExtract<IDictionary<string, string>>(
                                SessionHelper.SESSION_KEY_LAR_FHA_LINKS);
                        WorkLoadManagerId = SessionHelper.SessionExtract<string>(
                               SessionHelper.SESSION_KEY_WORKLOADMANAGERID);
                        if (fhasSelected != null)
                            fhasSelected.ToList().ForEach(p => fhasAssigned.Add(p.Key));

                        if (newRoles.Contains("Servicer"))
                        {
                            userModel.SelectedLenderId = UserPrincipal.Current.LenderId;
                        }
                    }

                    if (newRoles.Contains("InspectionContractor"))
                    {
                        var fhasSelected =
                            SessionHelper.SessionExtract<IDictionary<string, string>>(
                                SessionHelper.SESSION_KEY_IC_FHA_LINKS);
                        if (fhasSelected != null)
                            fhasSelected.ToList().ForEach(p => fhasAssigned.Add(p.Key));
                    }
                    var user = userInforRepository.GetUserInfoByUsername(userModel.UserName);
                    if (user != null)
                    {
                        ExceptionManager.WriteToEventLog("user:" + user, "Application",
                       System.Diagnostics.EventLogEntryType.Error);
                        AddNewUserInfoBasedOnRole(user.UserID, userModel, newRoles.ToArray(),//inserting into user_lender table if selected role is servicer
                            fhasAssigned);
                    }

                    if ((WorkLoadManagerId != "" && WorkLoadManagerId != null) || userModel.SelectedAccountExecutiveId != null)
                    {
                        if (user != null) userModel.UserID = user.UserID;
                        if (WorkLoadManagerId != "" && WorkLoadManagerId != null) userModel.SelectedWorkloadManagerId = Convert.ToInt32(WorkLoadManagerId);
                        userWorkloadProjectManagerRepository.AddNewUser(userModel);
                    }
                    if (WorkLoadManagerId != "" && WorkLoadManagerId != null)
                    {
                        hudWorkloadInternalSpecialOptionUserRepository.AddNewUser(userModel);
                    }
                    unitOfWork.Save();
                    return userToken;
                }
                else
                {
                    var userInforRepository = new UserInfoRepository(unitOfWork);

                    var newRoles = new List<string>();
                    //  retrieve roles from session and save update user roles
                    newRoles = SessionHelper.SessionExtract<List<string>>(SessionHelper.SESSION_KEY_NEW_ROLES);

                    if (newRoles.Contains("Servicer"))
                    {
                        if (newRoles.Count > 0)
                        {
                            //string[] userfullroles = Roles.FindUsersInRole("Servicer", userModel.UserName);
                            string[] userfullroles = Roles.GetRolesForUser(userModel.UserName);

                            foreach (string i in newRoles.ToArray())
                                if (!userfullroles.Contains(i))
                                    Roles.AddUserToRoles(userModel.UserName, newRoles.ToArray());
                        }
                        var fhasAssigned = new List<string>();
                        string WorkLoadManagerId = "";
                        if (newRoles.Contains("Servicer"))
                        {
                            var fhasSelected =
                                SessionHelper.SessionExtract<IDictionary<string, string>>(
                                    SessionHelper.SESSION_KEY_LAR_FHA_LINKS);
                            WorkLoadManagerId = SessionHelper.SessionExtract<string>(
                                   SessionHelper.SESSION_KEY_WORKLOADMANAGERID);
                            if (fhasSelected != null)
                                fhasSelected.ToList().ForEach(p => fhasAssigned.Add(p.Key));

                            if (newRoles.Contains("Servicer"))
                            {
                                userModel.SelectedLenderId = UserPrincipal.Current.LenderId;
                            }
                        }

                        var user = userInforRepository.GetUserInfoByUsername(userModel.UserName);
                        if (user != null)
                        {
                            ExceptionManager.WriteToEventLog("user:" + user, "Application",
                           System.Diagnostics.EventLogEntryType.Error);
                            AddNewUserInfoBasedOnRole(user.UserID, userModel, newRoles.ToArray(),//inserting into user_lender table if selected role is servicer
                                fhasAssigned);
                            //return user.ToString();
                        }


                        //if ((WorkLoadManagerId != "" && WorkLoadManagerId != null))
                        //{
                        //    if (user != null) userModel.UserID = user.UserID;
                        //    if (WorkLoadManagerId != "" && WorkLoadManagerId != null) userModel.SelectedWorkloadManagerId = Convert.ToInt32(WorkLoadManagerId);
                        //    userWorkloadProjectManagerRepository.AddNewUser(userModel);
                        //}
                        //if (WorkLoadManagerId != "" && WorkLoadManagerId != null)
                        //{
                        //    hudWorkloadInternalSpecialOptionUserRepository.AddNewUser(userModel);
                        //}
                        //return user;
                    }
                    else if (newRoles.Contains("InternalSpecialOptionUser"))
                    {
                        if (newRoles.Count > 0)
                        {
                            //string[] userfullroles = Roles.FindUsersInRole("Servicer", userModel.UserName);
                            string[] userfullroles = Roles.GetRolesForUser(userModel.UserName);

                            foreach (string i in newRoles.ToArray())
                                if (!userfullroles.Contains(i))
                                    Roles.AddUserToRoles(userModel.UserName, newRoles.ToArray());

                            hudWorkloadInternalSpecialOptionUserRepository.AddNewUser(userModel);
                        }
                    }
                    unitOfWork.Save();
                    return userModel.UserName;

                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("Error happened while creating new user", e.InnerException);
            }

        }


        public IEnumerable<FhaInfoModel> GetFhasByWLMId(int wlmid)
        {
            var results = fhaRepository.GetFhasByWLMIds(wlmid);
            return results;
        }

        private void AddNewUserInfoBasedOnRole(int iUserId, UserViewModel model, string[] userRoles, IEnumerable<string> fhasAssigned)
        {
            if (userRoles.Contains("WorkflowManager"))
            {
                if (model.SelectedWorkloadManagerId != null)
                {
                    var projectInfoList =
                        projectInfoRepository.GetProjectInfoListAssociatedToWorkloadManager(
                            model.SelectedWorkloadManagerId.Value);
                    foreach (var projectInfo in projectInfoList)
                    {
                        var userLender = new User_Lender
                        {
                            User_ID = iUserId,
                            FHANumber = projectInfo.FHANumber
                        };
                        userLenderServicerRepository.AddNewUserLenderRecord(userLender);
                    }
                }
            }
            else if (userRoles.Contains("AccountExecutive"))
            {
                if (model.SelectedAccountExecutiveId != null)
                {
                    var projectInfoList =
                        projectInfoRepository.GetProjectInfoListAssociatedToAccountExecutive(
                            model.SelectedAccountExecutiveId.Value);
                    foreach (var projectInfo in projectInfoList)
                    {
                        var userLender = new User_Lender
                        {
                            User_ID = iUserId,
                            FHANumber = projectInfo.FHANumber
                        };
                        userLenderServicerRepository.AddNewUserLenderRecord(userLender);
                    }
                }
            }
            else if (userRoles.Contains("LenderAccountRepresentative") ||
                     (userRoles.Contains("LenderAccountManager") || userRoles.Contains("BackupAccountManager")))
            {
                var userLender = new User_Lender
                {
                    User_ID = iUserId,
                    Lender_ID = model.SelectedLenderId,
                    ServicerID = null
                };
                userLenderServicerRepository.AddNewUserLenderRecord(userLender);
            }
            else if (userRoles.Contains("OperatorAccountRepresentative"))
            {
                foreach (var fha in fhasAssigned)
                {
                    var userLender = new User_Lender { User_ID = iUserId, FHANumber = fha };
                    userLenderServicerRepository.AddNewUserLenderRecord(userLender);
                }
            }
            else if (userRoles.Contains("Servicer"))
            {
                foreach (var fha in fhasAssigned)
                {
                    var userLender = new User_Lender
                    {
                        User_ID = iUserId,
                        Lender_ID = model.SelectedLenderId,
                        ServicerID = null,
                        FHANumber = fha
                    };
                    userLenderServicerRepository.AddNewUserLenderRecord(userLender);
                }

            }
            else if (userRoles.Contains("InternalSpecialOptionUser"))
            {
                foreach (var fha in fhasAssigned)
                {
                    var userLender = new User_Lender
                    {
                        User_ID = iUserId,
                        Lender_ID = model.SelectedLenderId,
                        ServicerID = null,
                        FHANumber = fha
                    };
                    userLenderServicerRepository.AddNewUserLenderRecord(userLender);
                }
            }
            else if (userRoles.Contains("InspectionContractor"))
            {
                foreach (var fha in fhasAssigned)
                {
                    var userLender = new User_Lender
                    {
                        User_ID = iUserId,
                        Lender_ID = null,
                        ServicerID = null,
                        FHANumber = fha
                    };
                    userLenderServicerRepository.AddNewUserLenderRecord(userLender);
                }

            }
        }

        public IEnumerable<KeyValuePair<int, string>> GetLendersByUserRole()
        {
            IEnumerable<KeyValuePair<int, string>> results;
            if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName))
            {
                results = lenderInfoRepository.GetAllLenders();
            }
            else if (RoleManager.IsCurrentUserLenderRoles())
            {
                results = lenderInfoRepository.GetAllLenders().Where(p => p.Key == UserPrincipal.Current.LenderId);
            }
            else
            {
                throw new InvalidOperationException(string.Format("User {0} are not lender or servicer or hud admin", UserPrincipal.Current.UserName));
            }
            return results;
        }

        public IEnumerable<KeyValuePair<int, string>> GetWorkloadManagers()
        {
            return workloadManagerRepository.GetWorkloadManagers();
        }

        public IEnumerable<KeyValuePair<int, string>> GetAccountExecutives()
        {
            return accountExecutiveRepository.GetAccountExecutives();
        }

        public UserViewModel GetWorkloadManagerDetail(int id)
        {
            var userViewModel = workloadManagerRepository.GetWorkloadManagerDetail(id);
            FormatUserNameForRegistration(userViewModel);
            return userViewModel;
        }

        public UserViewModel GetAccountExecutiveDetail(int id)
        {
            var userViewModel = accountExecutiveRepository.GetAccountExecutiveDetail(id);
            FormatUserNameForRegistration(userViewModel);
            return userViewModel;
        }
        private static void FormatUserNameForRegistration(UserViewModel userViewModel)
        {
            var userFullName = userViewModel.FirstName.Trim().Split(' ');
            if (!string.IsNullOrEmpty(userFullName[0]))
            {
                userViewModel.FirstName = userFullName[0];
            }

            if (!string.IsNullOrEmpty(userFullName[1]) && userFullName.Count() > 2)
            {
                userViewModel.MiddleName = userFullName[1];
                if (!string.IsNullOrEmpty(userFullName[2]))
                {
                    userViewModel.LastName = userFullName[2];
                }
            }
            else if (!string.IsNullOrEmpty(userFullName[1]))
            {
                userViewModel.LastName = userFullName[1];
            }
        }
        public IEnumerable<KeyValuePair<int, string>> GetInstitutionsByRoles(string[] roles)
        {
            IEnumerable<KeyValuePair<int, string>> results;

            if (roles.Contains("LenderAccountRepresentative") || RoleManager.IsLenderAdmin(roles))
            {
                results = lenderInfoRepository.GetAllLenders();
            }
            else if (roles.Contains("Servicer"))
            {
                results = servicerInfoRepository.GetAllServicers();
            }
            else
            {
                throw new InvalidOperationException(string.Format("Roles {0} are not lender or servicer", TextUtils.TokenDelimitedText(roles, ",")));
            }
            return results;
        }

        public IEnumerable<FhaInfoModel> GetFHAsByLenders(string lenderIds)
        {
            var results = fhaRepository.GetFhasByLenderIds(lenderIds);
            return results;
        }

        public IEnumerable<int> GetLenderIdsByAeOrWlmForQtr(string userName, string quarter)
        {
            if (!string.IsNullOrEmpty(quarter))
            {
                var splitStr = quarter.Split(new[] { '/' });
                int monthsInPeriod = int.Parse(splitStr[0]);
                int year = int.Parse(splitStr[1]);
                var results = userLenderServicerRepository.GetLenderIdsByAeOrWlm(userName, monthsInPeriod, year);
                return results;
            }
            return userLenderServicerRepository.GetLenderIdsByAeOrWlm(userName, 0, 0);

        }

        public List<KeyValuePair<string, string>> GetAllStates()
        {
            return statesRepository.GetAllStates();
        }

        public UserViewModel GetUserById(int userId)
        {
            return userRepository.GetUserById(userId);
        }

        public UserViewModel GetUserByUsername(string username)
        {
            return userRepository.GetUserByUsername(username);
        }

        public string GetUserNameById(int userId)
        {
            return userRepository.GetUserNameById(userId);
        }


        /// <summary>
        /// Get user name by id from hcp authentication
        /// </summary>
        /// <param name="pUserId"></param>
        /// <returns></returns>
        public string GetFullUserNameById(int pUserId)
        {
            return userRepository.GetFullNameById(pUserId);
        }


        public void UnlockUser(int userId)
        {
            webMembershipRepository.UnlockUserAccount(userId);
            unitOfWork.Save();
        }

        public void InitForceChangePassword(int userId)
        {
            webMembershipRepository.InitForceChangePassword(userId);
            unitOfWork.Save();
        }

        public int? GetUserIdByToken(string userToken)
        {
            return webMembershipRepository.GetUserIdByToken(userToken);
        }

        public int? GetUserIdByResetPwdToken(string resetPwdToken)
        {
            return webMembershipRepository.GetUserIdByResetPwdToken(resetPwdToken);
        }

        public void SavePasswordHist(string userName, string passwordNew)
        {
            int userId = WebSecurity.GetUserId(userName);
            var user = GetUserById(userId);
            string passwordHistHashDb = user.PasswordHist;
            List<string> passwordsHash = new List<string>();
            if (!string.IsNullOrEmpty(passwordHistHashDb))
                passwordsHash = passwordHistHashDb.Split(new string[] { PASSWORD_DELIMITER }, StringSplitOptions.RemoveEmptyEntries).ToList();
            string passwordNewHash = PasswordHash.CreateHash(passwordNew);
            if (passwordsHash.Count < PASSWORD_HIST_COUNT)
            {
                passwordsHash.Add(passwordNewHash);
            }
            else
            {
                // remove the oldest password hash 
                passwordsHash.RemoveAt(0);
                // add newest password hash
                passwordsHash.Add(passwordNewHash);
            }
            var passwordsHashString = TextUtils.TokenDelimitedText(passwordsHash, PASSWORD_DELIMITER);
            userRepository.SavePasswordHist(userId, passwordsHashString);
            unitOfWork.Save();
        }

        public bool IsPasswordInHistory(string userName, string passwordNew)
        {
            int userId = WebSecurity.GetUserId(userName);
            var user = GetUserById(userId);
            string passwordHistHashDb = user.PasswordHist;
            List<string> passwordsHash = new List<string>();
            if (!string.IsNullOrEmpty(passwordHistHashDb))
                passwordsHash = passwordHistHashDb.Split(new string[] { PASSWORD_DELIMITER }, StringSplitOptions.RemoveEmptyEntries).ToList();
            else
                return false;
            //string passwordNewHash = PasswordHash.CreateHash(passwordNew);
            foreach (var passwordHash in passwordsHash)
            {
                if (PasswordHash.ValidatePassword(passwordNew, passwordHash))
                    return true;
            }
            return false;
        }

        public SecurityQuestionViewModel GetSecQuestionsByUserId(int userId)
        {
            return userSecQuestionRepository.GetSecQuestionsByUserId(userId);
        }

        public void SaveUserSecQuestions(int userid, SecurityQuestionViewModel securityQuesionModel)
        {
            userSecQuestionRepository.SaveUserSecQuestions(userid, securityQuesionModel);
            unitOfWork.Save();
        }

        public IEnumerable<UserViewModel> GetDataAdminManageUser(string searchText, int page, string sort, SqlOrderByDirecton sortdir)
        {
            var users = RemoveDuplicateEntriesAndPopulateOtherUserInfo(adminManagerUsersRepository.GetDataAdminManageUser(searchText, page, sort, sortdir)).ToList();
            // lender admin can only view his lender users
            if (RoleManager.IsCurrentUserLenderAdmin())
            {

                int adminLenderId = UserPrincipal.Current.LenderId ?? 0;
                //users.Where(p => p.LenderID == adminLenderId);
                users.RemoveAll(p => p.LenderID != adminLenderId);//#64: 1268-breach of Lender users across the hud portal

            }
            return users;
        }

        private IEnumerable<UserViewModel> RemoveDuplicateEntriesAndPopulateOtherUserInfo(IEnumerable<UserViewModel> users)
        {
            var filteredList = users.GroupBy(x => x.UserName).Select(y => y.First()).ToList();
            var lamBamFilteredList = new List<UserViewModel>();
            foreach (UserViewModel user in filteredList)
            {
                user.IsAccountLocked = WebSecurity.IsAccountLockedOut(user.UserName, MaxLoginTrials, LockAccountInSeconds);
                user.Deleted_Ind = userRepository.IsUserInactive(user.UserID);
                var loggedInUserRole = EnumUtils.Parse<HUDRole>(UserPrincipal.Current.UserRole);
                var roles = Roles.GetRolesForUser(user.UserName);
                if (loggedInUserRole == HUDRole.LenderAccountManager || loggedInUserRole == HUDRole.BackupAccountManager)
                {
                    if (roles.Any(role => role.Contains("SuperUser") || role.Contains("HUDAdmin") || role.Contains("HUDDirector") ||
                                          role.Contains("AccountExecutive") || role.Contains("WorkflowManager") ||
                                          role.Contains("Attorney")))
                    {
                        lamBamFilteredList.Add(user);
                        continue;
                    }
                }

                if (roles != null && roles.Any())
                {
                    HudUserUtil.ReassignRoleNameForDisplay(roles);
                    user.SelectedRoles = TextUtils.TokenDelimitedText(roles, ",");
                    if (user.SelectedRoles.Contains("Workload Manager") || user.SelectedRoles.Contains("Account Executive"))
                    {
                        user.LenderName = string.Empty;
                        user.ServicerName = string.Empty;
                    }
                    //Restricting the LAM/BAM to Inactivate/Unlock the users with similar role or higher
                    if (UserPrincipal.Current.UserName != user.UserName)
                    {
                        if ((UserPrincipal.Current.UserRole == "LenderAccountManager" &&
                             user.SelectedRoles.Contains("Lender Account Manager")) ||
                            (UserPrincipal.Current.UserRole == "BackupAccountManager" &&
                             (user.SelectedRoles.Contains("Lender Account Manager") ||
                              user.SelectedRoles.Contains("Backup Account Manager"))))
                        {
                            user.IsAllowUserEdit = false;
                        }
                    }
                }
                user.IsRegisterComplete = WebSecurity.IsConfirmed(user.UserName);
            }

            if (lamBamFilteredList.Any())
            {
                return filteredList.Except(lamBamFilteredList);
            }

            return filteredList;
        }

        public IEnumerable<ReportsModel> GetMultipleLoanProperty()
        {
            return reportsRepository.GetMultipleLoanProperty();
        }

        public List<HUDRole> GetHUDRoles()
        {
            var userRoles = UserPrincipal.Current.Roles.ToList();
            List<HUDRole> roles = new List<HUDRole>();
            userRoles.ForEach(p => roles.Add(EnumUtils.Parse<HUDRole>(p)));
            return roles;
        }

        public IEnumerable<UserViewModel> GetAllUsers(string searchText, int page, string sort, SqlOrderByDirecton sortdir)
        {
            var dataSet = RemoveDuplicateEntriesAndPopulateOtherUserInfo(adminManagerUsersRepository.GetDataAdminManageUser(searchText, page, sort, sortdir)).ToList();

            return dataSet;
        }

        public IEnumerable<UserInfoModel> GetOperatorsByLenderId(int lenderId)
        {
            return userRepository.GetOperatorsByLenderId(lenderId);
        }

        public IEnumerable<UserInfoModel> GetLenderUsersByLenderId(int lenderId)
        {
            return userRepository.GetLenderUsersByLenderId(lenderId);
        }

        public IEnumerable<UserInfoModel> GetLenderUsersByAeUserId(int aeUserId)
        {
            return userRepository.GetLenderUsersByAeUserId(aeUserId);
        }

        public IEnumerable<UserViewModel> SearchByFirstOrLastName(string searchText, int maxResults)
        {
            return RemoveDuplicateEntriesAndPopulateOtherUserInfo(userRepository.SearchByFirstOrLastName(searchText, maxResults, addressRepository));
        }

        public PaginateSortModel<UserViewModel> SearchByFirstOrLastNamePageSort(string searchText, string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum)
        {
            return userRepository.SearchByFirstOrLastNamePageSort(searchText, strSortBy, sortOrder, pageSize, pageNum, addressRepository);
        }

        public void UpdateUser(UserViewModel model)
        {
            addressRepository.UpdateAddress(model.AddressModel);
            model.SelectedTimezone = EnumUtils.Parse<HUDTimeZone>(model.SelectedTimezoneId).ToString();
            userRepository.UpdateUser(model);
            ExceptionManager.WriteToEventLog(model.ToString(), "Application", System.Diagnostics.EventLogEntryType.Error);
            unitOfWork.Save();
            if (RoleManager.IsInternalSpecialOptionUser(model.UserName))
            {
                hudWorkloadInternalSpecialOptionUserRepository.UpdateUser(model);
            }

        }

        /// <summary>
        /// To activate or inactivate user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isUserInactive"></param>
        public void ActivateOrInactivateUser(int userId, bool isUserInactive)
        {
            userRepository.ActivateOrInactivateUser(userId, isUserInactive);
        }

        /// <summary>
        /// To verify is the user is inactivated or not
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool IsUserInactive(string userName)
        {
            int userId = userRepository.GetUserByUsername(userName).UserID;
            var isUserDeleted = userRepository.IsUserInactive(userId);
            return isUserDeleted != null && (bool)isUserDeleted;
        }

        public bool IsUserInactive(int userId)
        {
            var isUserDeleted = userRepository.IsUserInactive(userId);
            return isUserDeleted != null && (bool)isUserDeleted;
        }

        public int GetUserId(string userName)
        {
            var user = userRepository.GetUserByUsername(userName);
            if (user != null)
                return user.UserID;
            return 0;
        }

        /// <summary>
        /// To make OAR FHAs match the passed updates
        /// </summary>
        /// <param name="originalUserViewModel">The original user view model.</param>
        /// <param name="originalFhas">The original fhas.</param>
        /// <param name="selectedFhas">The selected fhas.</param>
        /// <returns></returns>
        public bool UpdateOarFhas(UserViewModel originalUserViewModel, IEnumerable<KeyValuePair<string, string>> originalFhas, IEnumerable<KeyValuePair<string, string>> selectedFhas)
        {
            var newFhas = selectedFhas.Where(p => !originalFhas.Contains(p));
            var removeFhas = originalFhas.Where(p => !selectedFhas.Contains(p));

            if (newFhas.Any() || removeFhas.Any())
            {
                userLenderServicerRepository = new UserLenderServicerRepository(unitOfWork);
                foreach (var addUser in newFhas)
                {
                    var userLender = new User_Lender
                    {
                        User_ID = originalUserViewModel.UserID,
                        FHANumber = addUser.Key
                    };
                    userLenderServicerRepository.AddNewUserLenderRecord(userLender);
                }

                foreach (var removeUser in removeFhas)
                {
                    var userLender = new User_Lender
                    {
                        User_ID = originalUserViewModel.UserID,
                        FHANumber = removeUser.Key
                    };
                    List<int> userLenderId = userLenderServicerRepository.GetUserLenderRow(userLender);
                    if (userLenderId.Any())
                    {
                        userLenderServicerRepository.RemoveUserLenderRow(userLenderId[0]);
                    }
                }
                unitOfWork.Save();
            }

            return true;
        }

        public IEnumerable<int?> GetLendersByUserId(int userId)
        {
            return userLenderServicerRepository.GetLenderIdsByUserId(userId);
        }

        public List<int?> GetLenderIdsByUser(int userId)
        {
            return userLenderServicerRepository.GetLenderIdsByUser(userId, lenderFhaRepository);
        }

        public IEnumerable<KeyValuePair<int, string>> GetLenderDetail(int lenderId)
        {
            return lenderInfoRepository.GetLenderDetail(lenderId);
        }

        public IList<FormTypeModel> GetAllFormTypesForProjectAction()
        {
            return formTypeRepository.GetAllFormTypeWithProjectAction();
        }

        /// <summary>
        /// Get last name by comparing first name 
        /// </summary>
        /// <param name="pFirstName"></param>
        /// <returns></returns>
        public string GetUserLastNameByFirstName(string pFirstName)
        {
            return userRepository.GetUserLastNameByFirstName(pFirstName);
        }

        // naveen
        public bool checkUserEmailId(string email)
        {

            try
            {
                return userRepository.IsEmailExists(email);

            }
            catch (Exception ex)
            {

                return false;
            }
        }

        //karri#354
        public IEnumerable<FhaInfoModel> GetFHAsForServicer(string lenderId, string servicerId)
        {
            var results = fhaRepository.GetServicersFhasByLenderIds(lenderId, servicerId);
            return results;
        }

        public bool deleteuser(List<string> Fhanumbs, int Userid)
        {
            return hudWorkloadInternalSpecialOptionUserRepository.DeleteUser(Fhanumbs, Userid);
        }

        //Naveen#543
        public IEnumerable<UserViewModel> GetServicer(int? lenderid)
        {
            var result = adminManagerUsersRepository.GetServicersrepo(lenderid);
            return result;
        }
    }
}
