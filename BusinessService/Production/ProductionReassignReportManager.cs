﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class ProductionReassignReportManager : IProductionReassignReportManager
    {
        private UnitOfWork unitOfWorkTask;
        private IProductionReassignReportRepository reassignRepository;


        public ProductionReassignReportManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);

            reassignRepository = new ProductionReassignReportRepository(unitOfWorkTask);

        }


        public ProductionReasignReportModel GetProdReassignmentTasks(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate)
        {
            return reassignRepository.GetProdReassignmentTasks(appType, programType, prodUserIds, fromDate, toDate);
        }


        public void ReassignTasks(List<string> kvp, int AssignUserId)
        {
            reassignRepository.ReassignTasks(kvp, AssignUserId);
        }
        //harish added 15042020 to get grid data based on parameters
        public ProductionReasignReportModel GetLenderProdReassignmentTasks(string appType, string programType, string lenderUserIds, DateTime? fromDate, DateTime? toDate)
        {
            return reassignRepository.GetLenderProdReassignmentTasks(appType, programType, lenderUserIds, fromDate, toDate);
        }

        // harish added 21042020
        public void LenderReassignTasks(List<string> kvp, int AssignUserId)
        {
            reassignRepository.LenderReassignTasks(kvp, AssignUserId);
        }
    }
}
