﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;
using Repository.Interfaces;

namespace BusinessService.Production
{
    public class Prod_TaskXrefManager : IProd_TaskXrefManager
    {
        private UnitOfWork unitOfWorkTask;
        private IProd_TaskXrefRepository prod_taskXrefRepository;
        private ITaskRepository taskRepository;


        public Prod_TaskXrefManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);

            prod_taskXrefRepository = new Prod_TaskXrefRepository(unitOfWorkTask);
            taskRepository = new TaskRepository(unitOfWorkTask);
            
        }

        public Guid AddTaskXref(Model.Production.Prod_TaskXrefModel model)
        {
            return prod_taskXrefRepository.AddTaskXref(model);
        }

        public List<Prod_TaskXrefModel> GetProductionSubTasks(Guid parentInstanceId)
        {
            return prod_taskXrefRepository.GetProductionSubTasks(parentInstanceId);
        }

        public bool AssignProductionFhaRequest(ProductionTaskAssignmentModel model)
        {
            var parentInstanceId= prod_taskXrefRepository.AssignProductionFhaRequest(model);
            if (parentInstanceId != Guid.Empty)
            {
                var unAssignedSubtasks = prod_taskXrefRepository.GetProductionSubTasks(parentInstanceId).Count(m => m.AssignedTo==null);
                if (unAssignedSubtasks < 1)
                {
                    var parentTask = taskRepository.GetTasksByTaskInstanceId(parentInstanceId).FirstOrDefault();
                    if (parentTask != null)
                    {
                        if (parentTask.AssignedTo != "Queue" && parentTask.AssignedTo != null && parentTask.TaskStepId != (int)ProductionFhaRequestStatus.Completed)
                        {
                            parentTask.TaskStepId = (int)ProductionFhaRequestStatus.InProcess;
                            taskRepository.UpdateTaskStatus(parentTask);                           
                            return true;
                        }
                    }

                }

            }
            return false;
        }

        public int UpdateTaskXrefAndFHARequest(Guid taskInstanceId, int viewId, string comments, string fhaNumber, string portfolioName, int portfolioNumber)
        {
            return prod_taskXrefRepository.UpdateTaskXrefAndFHARequest(taskInstanceId, viewId, comments, fhaNumber, portfolioName, portfolioNumber);
        }

         public bool UpdateTasXrefCompleteStatus(Guid taskXrefid,string comment)
        {
            return prod_taskXrefRepository.UpdateTasXrefCompleteStatus(taskXrefid, comment);
        }

		public void UpdateTaskXrefForAmendments(Guid pTaskInstanceId, int pUserId)
		{
			prod_taskXrefRepository.UpdateTaskXrefForAmendments(pTaskInstanceId, pUserId);
		}

		public int GetCountOfReviewersTaskPendingForUW(Guid taskInstanceId)
         {
            return prod_taskXrefRepository.GetCountOfReviewersTaskPendingForUW( taskInstanceId);
         }



        public Prod_TaskXrefModel GetProductionSubtaskById(Guid taskXrefId)
        {
            return prod_taskXrefRepository.GetProductionSubtaskById(taskXrefId);
        }

        public void UpdateTaskXrefForFHARequestDeny(Guid taskInstanceId, string comments)
        {
            prod_taskXrefRepository.UpdateTaskXrefForFHARequestDeny(taskInstanceId, comments);
        }

        public void UpdateTaskXrefForFHARequestCancel(Guid taskInstanceId, string comments, int fhaRequestType)
        {
            prod_taskXrefRepository.UpdateTaskXrefForFHARequestCancel(taskInstanceId, comments, fhaRequestType);
        }


        public bool UpdateTasXrefComment(Guid taskXrefid, string comment)
        {
           return prod_taskXrefRepository.UpdateTasXrefComment(taskXrefid, comment);
        }

		public bool UpdateTasXref(Prod_TaskXrefModel pProd_TaskXrefModel)
		{
			return prod_taskXrefRepository.UpdateTasXref(pProd_TaskXrefModel);
		}


		public List<Prod_TaskXrefModel> GetProductionSubTasksforIR(Guid parentInstanceId)
        {
            return prod_taskXrefRepository.GetProductionSubTasksforIR(parentInstanceId);
        }

        public Guid GetUnderwriterTaskXrefIdByParentTaskInstanceId(Guid taskInstanceId)
        {
            return prod_taskXrefRepository.GetUnderwriterTaskXrefIdByParentTaskInstanceId(taskInstanceId);
        }

        public int GetReviewerUserIdByTaskInstanceId(Guid taskInstanceId, int viewId)
        {
            return prod_taskXrefRepository.GetReviewerUserIdByTaskInstanceId(taskInstanceId, viewId);
        }

        public int GetWLMUserIdByTaskInstanceId(Guid taskInstanceId)
        {
            return prod_taskXrefRepository.GetWLMUserIdByTaskInstanceId(taskInstanceId);
        }

		public bool FindTasXrefForAmendmentWLMView(Guid pTaskXrefid)
		{
			return prod_taskXrefRepository.FindTasXrefForAmendmentWLMView(pTaskXrefid);
		}

		public List<Guid> GetTasXrefForAmendmentWLMView(Guid pTaskXrefid)
		{
			return prod_taskXrefRepository.GetTasXrefForAmendmentWLMView(pTaskXrefid);
		}
	}
}
