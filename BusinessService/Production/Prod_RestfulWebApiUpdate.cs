﻿using BusinessService.Interfaces.Production;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class Prod_RestfulWebApiUpdate : IProd_RestfulWebApiUpdate
    {
        private IProd_RestfulWebApiUpdateRepository WebApiDocumentUpdateRepository;
        public Prod_RestfulWebApiUpdate()
        {

            WebApiDocumentUpdateRepository = new Prod_RestfulWebApiUpdateRepository();
        }

        public RestfulWebApiResultModel UpdateDocument(RestfulWebApiUpdateModel fileInfo, string token)
        {
            return WebApiDocumentUpdateRepository.UpdateDocumentInfo(fileInfo, token);
        }

        // harish added to update folder structure 
        public RestfulWebApiResultModel UpdateFolderDocument(RestfulWebApiUpdateModel[] fileInfo, string token)
        {
            return WebApiDocumentUpdateRepository.UpdateFolderDocumentInfo(fileInfo, token);
        }
    }
}
