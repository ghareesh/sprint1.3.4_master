﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Repository.Interfaces;
using Model.Production;
using Repository;
using Repository.Interfaces;
using Repository.Interfaces.Production;
using Repository.Production;

namespace BusinessService.Production
{
    public class FHANumberRequestManager : IFHANumberRequestManager
    {
        private IProd_ActivityTypeRepository activityTypeRepository;
        private IProd_BorrowerTypeRepository borrowerTypeRepository;
        private IProd_ProjectTypeRepository projectTypeRepository;
        private IProd_LoanTypeRepository mortgageInsuranceTypeRepository;
        private ILenderInfoRepository lenderInfoRepository;
        private IFHANumberRequestRepository fhaNumberRequestRepository;
        private IProd_SelectedAdditionalFinancingRepository selectedAdditionalFinancingRepository;
        private IProd_CMSStarRatingRepository cmsStarRatingRepository;
        private IAddressRepository addressRepository;
        private IDisclaimerMsgRepository disclaimerMsgRepository;
        private readonly UnitOfWork unitOfWork;
        public FHANumberRequestManager()
        {
            unitOfWork = new UnitOfWork(DBSource.Live);
            activityTypeRepository = new Prod_ActivityTypeRepository(unitOfWork);
            borrowerTypeRepository = new Prod_BorrowerTypeRepository(unitOfWork);
            projectTypeRepository = new Prod_ProjectTypeRepository(unitOfWork);
            mortgageInsuranceTypeRepository = new Prod_LoanTypeRepository(unitOfWork);
            lenderInfoRepository = new LenderInfoRepository(unitOfWork);
            fhaNumberRequestRepository = new FHANumberRequestRepository(unitOfWork);
            cmsStarRatingRepository = new Prod_CMSStarRatingRepository(unitOfWork);
            selectedAdditionalFinancingRepository = new Prod_SelectedAdditionalFinancingRepository(unitOfWork);
            addressRepository = new AddressRepository(unitOfWork);
            disclaimerMsgRepository = new DisclaimerMsgRepository(unitOfWork);
        }

        public IList<ActivityTypeModel> GetAllActivityTypes()
        {
            return activityTypeRepository.GetAllActivityTypes();
        }

        public IList<BorrowerTypeModel> GetAllBorrowerTypes()
        {
            return borrowerTypeRepository.GetAllBorrowerTypes();
        }

        public IList<ProjectTypeModel> GetAllProjectTypesForLC()
        {
            return projectTypeRepository.GetAllProjectTypesForLC();
        }
        public IList<ProjectTypeModel> GetAllProjectTypes()
        {
            return projectTypeRepository.GetAllProjectTypes();
        }

        public IList<LoanTypeModel> GetALlMortgageInsuranceTypes()
        {
            return mortgageInsuranceTypeRepository.GetAllMortgageInsuranceTypes();
        }

        public string GetLenderName(int lenderId)
        {
            return lenderInfoRepository.GetLenderName(lenderId);
        }



        public Guid AddFHARequest(FHANumberRequestViewModel model)
        {
            //if (!model.IsAddressCorrect)
            //{
            AddPropertyAddress(model);
            //}
            //AddLenderContactAddress(model);

            var id = fhaNumberRequestRepository.AddFHARequest(model);
            model.FHANumberRequestId = id;
            var additionalFinancingList = SetSelectedAdditionalFinancing(model);
            selectedAdditionalFinancingRepository.AddSelectedAdditionalFinancing(additionalFinancingList);
            unitOfWork.Save();
            return id;
        }

        private void AddPropertyAddress(FHANumberRequestViewModel model)
        {
            if (model.PropertyAddressModel != null && (!string.IsNullOrEmpty(model.PropertyAddressModel.AddressLine1) ||
                                                       !string.IsNullOrEmpty(model.PropertyAddressModel.City)
                                                       || !string.IsNullOrEmpty(model.PropertyAddressModel.StateCode) ||
                                                       !string.IsNullOrEmpty(model.PropertyAddressModel.ZIP)))
            {
                model.PropertyAddressId = addressRepository.AddNewAddress(model.PropertyAddressModel);
            }
        }

        /*private void AddLenderContactAddress(FHANumberRequestViewModel model)
        {
            if (model.LenderContactAddressModel != null && (!string.IsNullOrEmpty(model.LenderContactAddressModel.ContactName)
                                                            || !string.IsNullOrEmpty(model.LenderContactAddressModel.Email) ||
                                                            !string.IsNullOrEmpty(model.LenderContactAddressModel.PhonePrimary)))
            {
                model.LenderContactAddressModel.StateCode = "00";
                model.LenderContactAddressId = addressRepository.AddNewAddress(model.LenderContactAddressModel);
            }
        }*/

        public IList<CMSStarRatingModel> GetCMSStarRatings()
        {
            return cmsStarRatingRepository.GetCMSStarRatings();
        }

        private static IList<SelectedAdditionalFinancingModel> SetSelectedAdditionalFinancing(FHANumberRequestViewModel model)
        {
            SelectedAdditionalFinancingModel selectedAdditionalModel;
            var additionalFinancingList = new List<SelectedAdditionalFinancingModel>();
            if (model.IsLIHTC != null && (bool)model.IsLIHTC)
            {
                selectedAdditionalModel = new SelectedAdditionalFinancingModel();
                selectedAdditionalModel.ResourceId = (int)AdditionalFinancingResources.LIHTC;
                selectedAdditionalModel.FHANumberRequestId = model.FHANumberRequestId;
                additionalFinancingList.Add(selectedAdditionalModel);
            }
            if (model.IsTaxExempted)
            {
                selectedAdditionalModel = new SelectedAdditionalFinancingModel();
                selectedAdditionalModel.ResourceId = (int)AdditionalFinancingResources.TAX;
                selectedAdditionalModel.FHANumberRequestId = model.FHANumberRequestId;
                additionalFinancingList.Add(selectedAdditionalModel);
            }
            if (model.IsHome)
            {
                selectedAdditionalModel = new SelectedAdditionalFinancingModel();
                selectedAdditionalModel.ResourceId = (int)AdditionalFinancingResources.HOME;
                selectedAdditionalModel.FHANumberRequestId = model.FHANumberRequestId;
                additionalFinancingList.Add(selectedAdditionalModel);
            }
            if (model.IsCDBG)
            {
                selectedAdditionalModel = new SelectedAdditionalFinancingModel();
                selectedAdditionalModel.ResourceId = (int)AdditionalFinancingResources.CDBG;
                selectedAdditionalModel.FHANumberRequestId = model.FHANumberRequestId;
                additionalFinancingList.Add(selectedAdditionalModel);
            }
            if (model.IsOther)
            {
                selectedAdditionalModel = new SelectedAdditionalFinancingModel();
                selectedAdditionalModel.ResourceId = (int)AdditionalFinancingResources.OTHER;
                selectedAdditionalModel.FHANumberRequestId = model.FHANumberRequestId;
                additionalFinancingList.Add(selectedAdditionalModel);
            }
            return additionalFinancingList;
        }

        public bool UpdateFHARequest(FHANumberRequestViewModel model)
        {
            if (model.PropertyAddressId != 0)
            {
                var addressInDB = addressRepository.GetAddressByID(model.PropertyAddressId);
                var isAddressChanged = false;
                if (addressInDB.AddressLine1 != model.PropertyStreetAddress)
                {
                    addressInDB.AddressLine1 = model.PropertyStreetAddress;
                    isAddressChanged = true;
                }
                if (addressInDB.City != model.PropertyCity)
                {
                    addressInDB.City = model.PropertyCity;
                    isAddressChanged = true;
                }
                if (addressInDB.StateCode != model.PropertyState)
                {
                    addressInDB.StateCode = model.PropertyState;
                    isAddressChanged = true;
                }
                if (addressInDB.ZIP != model.PropertyZip)
                {
                    addressInDB.ZIP = model.PropertyZip;
                    isAddressChanged = true;
                }
                if (isAddressChanged)
                {
                    addressRepository.UpdateAddress(addressInDB);
                }
            }
            else
            {
                //Adds new addressid for this property
                //if (!model.IsAddressCorrect)
                //{
                AddPropertyAddress(model);
                //}
            }

            //if (model.LenderContactAddressId != 0)
            //{
            //    var addressInDB = addressRepository.GetAddressByID(model.LenderContactAddressId);

            //    var isAddressChanged = false;

            //    if (addressInDB.Email != model.LenderContactEmail)
            //    {
            //        addressInDB.Email = model.LenderContactEmail;
            //        isAddressChanged = true;
            //    }
            //    if (addressInDB.PhonePrimary != model.LenderContactPhone)
            //    {
            //        addressInDB.PhonePrimary = model.LenderContactPhone;
            //        isAddressChanged = true;
            //    }
            //    if (isAddressChanged)
            //    {
            //        addressInDB.StateCode = "00";
            //        addressRepository.UpdateAddress(addressInDB);
            //    }
            //}
            //else { AddLenderContactAddress(model);}
            unitOfWork.Save();
            var additionalFinancingList = SetSelectedAdditionalFinancing(model);
            selectedAdditionalFinancingRepository.UpdateSelectedAdditionalFinancingByFHARequestId(model.FHANumberRequestId, additionalFinancingList);

            return fhaNumberRequestRepository.UpdateFHARequest(model);
        }

        public FHANumberRequestViewModel GetFhaRequestById(Guid instanceId)
        {
            return fhaNumberRequestRepository.GetFhaRequestById(instanceId);
        }
        public string GetDisclaimerMsgByPageType(string pageTypeName)
        {
            return disclaimerMsgRepository.GetDisclaimerMsgByPageType(pageTypeName);
        }


        public List<string> GetReadyforAppFhas()
        {
            return fhaNumberRequestRepository.GetReadyforAppFhas();
        }
        public List<string> GetReadyforConSingleStageFhas()
        {
            return fhaNumberRequestRepository.GetReadyforConSingleStageFhas();
        }
        public List<string> GetReadyforConTwoStageFhas()
        {
            return fhaNumberRequestRepository.GetReadyforConTwoStageFhas();
        }


        public FHANumberRequestViewModel GetFhaRequestByTaskInstanceId(Guid taskInstanceId)
        {
            var fhaRequestModel = fhaNumberRequestRepository.GetFhaRequestByTaskInstanceId(taskInstanceId);
            if (fhaRequestModel.PropertyAddressId != 0)
            {
                fhaRequestModel.PropertyAddressModel =
                    addressRepository.GetAddressByID(fhaRequestModel.PropertyAddressId);
                fhaRequestModel.PropertyStreetAddress = fhaRequestModel.PropertyAddressModel.AddressLine1;
                fhaRequestModel.PropertyCity = fhaRequestModel.PropertyAddressModel.City;
                fhaRequestModel.PropertyState = fhaRequestModel.PropertyAddressModel.StateCode;
                fhaRequestModel.PropertyZip = fhaRequestModel.PropertyAddressModel.ZIP;
            }
            if (fhaRequestModel.LenderContactAddressId != 0)
            {
                fhaRequestModel.LenderContactAddressModel =
                addressRepository.GetAddressByID(fhaRequestModel.LenderContactAddressId);
                fhaRequestModel.LenderContactEmail = fhaRequestModel.LenderContactAddressModel.Email;
                fhaRequestModel.LenderContactPhone = fhaRequestModel.LenderContactAddressModel.PhonePrimary;
            }

            var additionalFinancingList =
                selectedAdditionalFinancingRepository.GetSelectedAdditionalFinancingByFHARequestId(
                    fhaRequestModel.FHANumberRequestId);
            foreach (var item in additionalFinancingList)
            {
                if (item.ResourceId == (int)AdditionalFinancingResources.LIHTC)
                {
                    fhaRequestModel.IsLIHTC = true;
                }
                else if (item.ResourceId == (int)AdditionalFinancingResources.CDBG)
                {
                    fhaRequestModel.IsCDBG = true;
                }
                else if (item.ResourceId == (int)AdditionalFinancingResources.HOME)
                {
                    fhaRequestModel.IsHome = true;
                }
                else if (item.ResourceId == (int)AdditionalFinancingResources.TAX)
                {
                    fhaRequestModel.IsTaxExempted = true;
                }
                else if (item.ResourceId == (int)AdditionalFinancingResources.OTHER)
                {
                    fhaRequestModel.IsOther = true;
                }
            }
            return fhaRequestModel;
        }



        public IEnumerable<FhaSubmittedLendersModel> GetFhaSubmittedLenders()
        {
            return fhaNumberRequestRepository.GetFhaSubmittedLenders();
        }
        public IEnumerable<AssignedunderwriternamesModel> GetprodAssignedUnderwriternames()
        {
            return fhaNumberRequestRepository.GetprodAssignedUnderwriternames();
        }

        public ApplicationDetailViewModel GetApplicationDetailsForSharePointScreen(Guid taskInstanceId)
        {
            var applicationDetails = fhaNumberRequestRepository.GetApplicationDetailsForSharePointScreen(taskInstanceId);
            applicationDetails.TaskInstanceId = taskInstanceId;
            var propertyInfo = addressRepository.GetAddressByID(applicationDetails.PropertyAddressId);
            if (propertyInfo != null)
            {
                applicationDetails.PropertyStreetAddress = propertyInfo.AddressLine1;
                applicationDetails.PropertyCity = propertyInfo.City;
                applicationDetails.PropertyState = propertyInfo.StateCode;
                applicationDetails.PropertyZip = propertyInfo.ZIP;
            }
            applicationDetails.LenderName = lenderInfoRepository.GetLenderName(applicationDetails.LenderId);
            return applicationDetails;
        }

        public bool UpdateLoanAmount(string fhaNumber, decimal loanAmount, string projName, string origFHANumber, string origProjName, decimal origLoanAmount)
        {
            //karri#672
            return fhaNumberRequestRepository.UpdateLoanAmount(fhaNumber, loanAmount, projName, origFHANumber, origProjName, origLoanAmount);
        }

        public int GetProjectTypebyName(string projectTypeName)
        {
            return projectTypeRepository.GetProjectTypebyName(projectTypeName);
        }

        public void AddFhaRequestForIrr(FHANumberRequestViewModel model)
        {
            fhaNumberRequestRepository.AddFHARequest(model);
        }

        public bool IsRequestExistsForFhaNumber(string fhaNumber)
        {
            return fhaNumberRequestRepository.IsRequestExistsForFhaNumber(fhaNumber);
        }

        public IList<Prod_FhaAgingReportViewModel> GetFhaAgingReport()
        {
            return fhaNumberRequestRepository.GetFhaAgingReport();
        }
        public FHANumberRequestViewModel GetFhaRequestByFhaNumber(string fhaNumber)
        {
            return fhaNumberRequestRepository.GetFhaRequestByFhaNumber(fhaNumber);
        }
        public bool FhataskAssignedfromQueDateUpdate(Guid TaskinstanceId)
        {
            return fhaNumberRequestRepository.FhataskAssignedfromQueDateUpdate(TaskinstanceId);
        }

        public string GetProjectTypeById(int projectTypeId)
        {
            return projectTypeRepository.GetProjectTypebyID(projectTypeId);
        }

        public string GetBorrowerTypeById(int borrowerTypeId)
        {
            return borrowerTypeRepository.GetBorrowerTypeById(borrowerTypeId);
        }
    }
}
