﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;

namespace BusinessService.Production
{
   

    public class LenderProductionReassignReportManager : ILenderProductionReassignReportManager
    {
        private UnitOfWork unitOfWorkTask;
        private ILenderProductionReassignReportRepository reassignRepository;


        public LenderProductionReassignReportManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);

            reassignRepository = new LenderProductionReassignReportRepository(unitOfWorkTask);

        }


       
        //harish added 15042020 to get grid data based on parameters
        public LenderProductionReasignReportModel GetLenderProdReassignmentTasks(string appType, string programType, string lenderUserIds, DateTime? fromDate, DateTime? toDate)
        {
            return reassignRepository.GetLenderProdReassignmentTasks(appType, programType, lenderUserIds, fromDate, toDate);
        }

        // harish added 21042020
        public void LenderReassignTasks(List<string> kvp, int AssignUserId)
        {
            reassignRepository.LenderReassignTasks(kvp, AssignUserId);
        }
    }
}
