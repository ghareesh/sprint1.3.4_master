﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces;
using Repository.Interfaces.Production;
using Repository.Production;
using HUDHealthcarePortal.Model;
using Repository;

namespace BusinessService.Production
{
    public class Prod_SharepointScreenManager: IProd_SharepointScreenManager
    {
        private IProd_SharepointScreenRepository sharepointScreenRepository;
        private IProd_SharePointAccountExecutivesRepository sharePointAccountExecutivesRepository;
        private IUserRepository userRepository;
        private UnitOfWork unitOfWorkLive;
        private IAddressRepository addressRepository;

        public Prod_SharepointScreenManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            sharepointScreenRepository = new Prod_SharepointScreenRepository(unitOfWorkLive);
            sharePointAccountExecutivesRepository = new Prod_SharePointAccountExecutivesRepository(unitOfWorkLive);
            userRepository = new UserRepository(unitOfWorkLive);
            addressRepository = new AddressRepository(unitOfWorkLive);
        }
        
        
        public bool SaveOrUpdateGeneralInformationDetailsForSharepoint(GeneralInformationViewModel generalInfoModel)
        {
            return sharepointScreenRepository.SaveOrUpdateGeneralInformationDetailsForSharepoint(generalInfoModel);
        }

        public IList<Prod_SharePointAccountExecutivesViewModel> GetAllSharePointAEs()
        {
            return sharePointAccountExecutivesRepository.GetAllSharePointAEs();
        } 


        public SharePointCommentsModel GetSharepointComments(Guid taskInstanceId)
        {
            return sharepointScreenRepository.GetSharepointComments(taskInstanceId);
        }


        public bool AddOrUpdateComments(Guid taskInstanceId, string fhaNumber, string comment, int reviewerType)
        {
            return sharepointScreenRepository.AddOrUpdateComments(taskInstanceId, fhaNumber, comment, reviewerType);
        }

        public MiscellaneousInformationViewModel GetSharePointDetailsForMiscellaneousInfo(
            MiscellaneousInformationViewModel miscInfoModel)
        {
            return sharepointScreenRepository.GetSharePointDetailsForMiscellaneousInfo(miscInfoModel);
        }

        public bool SaveOrUpdateMiscellaneousInfoForSharepoint(MiscellaneousInformationViewModel miscInfoModel)
        {
            return sharepointScreenRepository.SaveOrUpdateMiscellaneousInfoForSharepoint(miscInfoModel);
        }


        public void SaveOrUpdateContractorsPayment(Guid taskInstanceId, string fhaNumber, decimal contractorPrice, decimal secondaryAmntPaid, decimal amountPaid)
        {
            sharepointScreenRepository.SaveOrUpdateContractorsPayment(taskInstanceId,fhaNumber, contractorPrice, secondaryAmntPaid, amountPaid);
        }


        public SharePointClosingInfoModel GetSharePointClosingInfo(Guid taskInstanceId)
        {
           return sharepointScreenRepository.GetSharePointClosingInfo(taskInstanceId);
        }


        public bool SaveOrUpdateClosingInfo(ClosingInfo model, string fhaNumber)
        {
           return sharepointScreenRepository.SaveOrUpdateClosingInfo(model, fhaNumber);
        }

        //karri#672
        public bool SaveAuditTrialNCREInitBalanceInfo(string fhaNumber_ref, int userid, int attribid, string oldvalue, string newvalue)
        {
            return sharepointScreenRepository.SaveAuditTrialNCREInitBalanceInfo(fhaNumber_ref, userid, attribid, oldvalue, newvalue);
        }

        public string GetNameAndRoleForUser(int userid)
        {
           return userRepository.GetNameAndRoleForUser(userid);
        }


        public AllSharepointData GetSharepointDataById(Guid taskInstanceId)
        {
            return sharepointScreenRepository.GetSharepointDataById(taskInstanceId);
        }


        public void SaveOrUpdateOgcAddress(AddressModel address, string fhaNumber, Guid taskInstanceId,int reviewerTypeId)
        {
            if(address !=null || fhaNumber!=null || taskInstanceId != null)
            {
                if(address.AddressID ==null)
                {
                    //Address repositroy Add
                    var addressId = addressRepository.AddNewAddress(address);
                    sharepointScreenRepository.SaveOrUpdateOgcAddress(fhaNumber, taskInstanceId, addressId, reviewerTypeId);
                }
                else
                {   
                    //Update Address Info
                    addressRepository.updateAddressSP(address);
                }
            }
        }


        public AddressModel GetAddressById(int addressId)
        {
            return addressRepository.GetAddressByID(addressId);
        }
        public void CreateForm290Task(Guid taskInstanceId , DateTime initialClosingDate)
        {
            // Check initalClosing if created already in sharepoint db
            //if not there create task with pagetypeId form290
            //with assigned to inqeue
            //

        }
        /// <summary>
        /// get Account Executive name  from Prod_SharepointScreen
        /// </summary>
        /// <param name="pTaskinstanceId"></param>
        /// <returns></returns>
        public string GetAccountExecutiveByTaskInstanceId(Guid pTaskinstanceId)
        {
            int accountExecuteId =  sharepointScreenRepository.GetAccountExecutiveById(pTaskinstanceId);
            IList<Prod_SharePointAccountExecutivesViewModel> lstAE = GetAllSharePointAEs();
            if(accountExecuteId > 0 )
                return lstAE.Where(p => p.SharePointAEId == accountExecuteId).FirstOrDefault().SharePointAEName;
            return string.Empty;
        }
    }
}
