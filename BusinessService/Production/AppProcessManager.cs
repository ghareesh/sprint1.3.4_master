﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
namespace BusinessService.Production
{
    public class AppProcessManager : IAppProcessManager
    {
        private readonly UnitOfWork unitOfWork;
        private readonly UnitOfWork unitOfWorktask;
        private IProd_ProjectTypeRepository projectetypeRepository;
        private ITaskFile_FolderMappingRepository foldermappingRepository;
        private IAppProcessRepository appprocessRepository;
        private IDocumentTypeRespository documentTypeRepository;

        public  AppProcessManager()
        {
              unitOfWork = new UnitOfWork(DBSource.Live);
              unitOfWorktask = new UnitOfWork(DBSource.Task);
              appprocessRepository = new AppProcessRepository(unitOfWork);
              foldermappingRepository = new TaskFile_FolderMappingRepository(unitOfWorktask);
              projectetypeRepository = new Prod_ProjectTypeRepository(unitOfWork);
              documentTypeRepository = new DocumentTypeRepository(unitOfWorktask);
        }

        public string GetProjectTypebyID(int projecttypeid)
        {
            return projectetypeRepository.GetProjectTypebyID(projecttypeid);
        }


        public IEnumerable<Prod_FolderStructureModel> GetFolderbyViewId(int ViewId)
        {
            return foldermappingRepository.GetFolderbyViewId(ViewId);
        }


        public IEnumerable<TaskFileModel> GetMappedFilesbyFolder(int[] folderkeys, Guid InstanceId)
        {
            return foldermappingRepository.GetMappedFilesbyFolder(folderkeys, InstanceId);
        }
        public IEnumerable<DocumentTypeModel> GetMappedDocTypesbyFolder(int folderkey)
        {
            return foldermappingRepository.GetMappedDocTypesbyFolder(folderkey);
        }
        public IEnumerable<Prod_FolderStructureModel> GetFolderListbyTaskInstanceId(Guid taskInstanceId)
        {
            return foldermappingRepository.GetFolderListbyTaskInstanceId(taskInstanceId);
        }

         public IEnumerable<OPAViewModel> GetProdReviewersTaskStatus(Guid taskInstanceId)
        {
            return foldermappingRepository.GetProdReviewersTaskStatus(taskInstanceId);
        }

         public IEnumerable<OPAViewModel> GetProdPendingRAI(Guid taskInstanceId)
         {
             return foldermappingRepository.GetProdPendingRAI(taskInstanceId);
         }

        public IEnumerable<ProductionQueueLenderInfo> GetAppRequests()
        {
            return appprocessRepository.GetAppRequests();
        }


        public IList<ProjectTypeModel> GetAllotherloanTypes()
        {
            return projectetypeRepository.GetAllotherloanTypes();
        }

        public GeneralInformationViewModel GetGeneralInfoDetailsForSharepoint(Guid taskInstanceId)
        {
            return appprocessRepository.GetGeneralInfoDetailsForSharepoint(taskInstanceId);
        }

        public MiscellaneousInformationViewModel GetContractUWDetails(Guid taskInstanceId)
        {
            return appprocessRepository.GetContractUWDetails(taskInstanceId);
        }
        public List<DocumentTypeModel> GetAllDocTypes()
        {
            return documentTypeRepository.GetAllDocTypes();
        }

        //harish added new method to get projectaction Name
        public string GetProjectActionTypebyID(int projecttypeid)
        {
            return projectetypeRepository.GetProjectActionTypebyID(projecttypeid);
        }

        //harish added 
        public IEnumerable<AM_Document> GetAm_Assetmanagement(int Projectiontypeid)
        {
            return foldermappingRepository.GetAM_Asstmanagement(Projectiontypeid);
        }
        // harish added 12-01-2020
        public IEnumerable<OPAViewModel> GetOPAPendingRAI(Guid taskInstanceId)
        {
            return foldermappingRepository.GetOPAPendingRAI(taskInstanceId);
        }
    }
}

