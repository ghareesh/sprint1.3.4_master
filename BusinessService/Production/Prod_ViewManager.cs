﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class Prod_ViewManager : IProd_ViewManager
    {
        private UnitOfWork unitOfWorkTask;
        private IProd_ViewRepository prod_ViewRepository;


        public Prod_ViewManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);

            prod_ViewRepository = new Prod_ViewRepository(unitOfWorkTask);
            
        }


        public List<Model.Production.Prod_ViewModel> GetViewsbyType(int TypeID)
        {
            return prod_ViewRepository.GetViewsbyType(TypeID);
        }
    }
}
