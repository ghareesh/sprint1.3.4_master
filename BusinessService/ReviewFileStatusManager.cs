﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.BusinessService
{
    public class ReviewFileStatusManager:IReviewFileStatusManager
    {
        private IReviewFileStatusRepository reviewFileStatusRepository;
        private UnitOfWork unitOfWork;

        public ReviewFileStatusManager(){
            unitOfWork = new UnitOfWork(DBSource.Task);
            reviewFileStatusRepository = new ReviewFileStatusRepository(unitOfWork);
        }

        public void SaveReviewFileStatus(ReviewFileStatusModel model)
        {
            reviewFileStatusRepository.SaveReviewFileStatus(model);
            unitOfWork.Save();
        }

        public IList<Guid> GetRequestAdditionalFileIdByParentTaskInstanceId(Guid parentTaskInstanceId)
        {
            return reviewFileStatusRepository.GetRequestAdditionalFileIdByParentTaskInstanceId(parentTaskInstanceId);
        }

        public IList<RequestAdditionalInfoFileModel> GetTaskFileIdByChildTaskInstanceId(Guid ChildTaskInstanceId)
        {
             var newResult = reviewFileStatusRepository.GetTaskFileIdByChildTaskInstanceId(ChildTaskInstanceId);
             return newResult;
        }

        public bool UpdateReviewFileStatusForRequestAdditionalResponse(Guid parentTaskFileId)
        {
            return reviewFileStatusRepository.UpdateReviewFileStatusForRequestAdditionalResponse(parentTaskFileId);
        }

        public bool UpdateReviewFileStatusForTaskFileId(Guid parentTaskFileId)
        {
            return reviewFileStatusRepository.UpdateReviewFileStatusForTaskFileId(parentTaskFileId);
        } 
 
        public bool UpdateReviewFileStatusForFileDownload(Guid parentTaskFileId)
        {
            return reviewFileStatusRepository.UpdateReviewFileStatusForFileDownload(parentTaskFileId);
        }

        public ReviewFileStatusModel GetReviewFileStatusByTaskFileId(Guid taskFileId)
        {
            return reviewFileStatusRepository.GetReviewFileStatusByTaskFileId(taskFileId);
        }

        public bool UpdateReviewFileStatus(IList<ReviewFileStatusModel> reviewStatusList)
        {
            return reviewFileStatusRepository.UpdateReviewFileStatus(reviewStatusList);
        }
        #region Production Application
        public ReviewFileStatusModel GetReviewFileStatusByTaskFileIdandViewId(Guid taskFileId, int viewid)
        {
            return reviewFileStatusRepository.GetReviewFileStatusByTaskFileIdandViewId( taskFileId,  viewid);
        }
        #endregion


    }
}
