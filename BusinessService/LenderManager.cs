﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//#189
namespace HUDHealthcarePortal.Lenderprocess
{
    public class LenderManager : ILenderMgr
    {
        LenderRepository _LenderRepository = null;
        public LenderManager()
        {
            _LenderRepository = new LenderRepository();
        }

        public void Addlender(Lenderdata obj)
        {
            _LenderRepository.Addlender(obj);
        }
    }

}
