﻿using BusinessService.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading;
using BusinessService;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository;
using Model;
using Model.AssetManagement;
using Model.Production;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using System.Configuration;

namespace HUDHealthcarePortal.BusinessService
{
    public class BackgroundJobMgr : IBackgroundJobMgr
    {
        private IAccountManager accountManager;
        //karri;#62
        private static bool _IsUATTTestInProgress;
        private static string _UATTestersMails;

        public BackgroundJobMgr()
        {
            accountManager = new AccountManager();

            //karri#62
            //to stop the mailing services during UAT Testing but to only FEW UAT Testers constantly
            //since there are 36 custom functional mailings, need to validate for each of the custom mail below before sending to TO:
            bool isUATTest;
            _IsUATTTestInProgress = false;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["IsUATTesting"])
                && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["UATMailingList"]))
                if (bool.TryParse(ConfigurationManager.AppSettings["IsUATTesting"].ToString(), out isUATTest))
                {
                    if (isUATTest)
                    {
                        _IsUATTTestInProgress = true;
                        _UATTestersMails = ConfigurationManager.AppSettings["UATMailingList"].ToString();
                    }
                    else
                        _IsUATTTestInProgress = false;
                }

        }

        public bool SendUploadNotificationEmails(IEmailManager emailMgr, IEnumerable<string> fhaNumbers)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var fhas = threadContext as IEnumerable<string>;
                    if (fhas == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendUploadNotificationEmails is not IEnumrable<string> type for fha numbers");
#if !DEBUG                    
                    emailMgr.SendUploadNotificationEmails(fhaNumbers);
#endif
                },
                fhaNumbers);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendSubmitNotificationEmails(IEmailManager emailMgr, string fhaNumber, string senderEmail, string receiverEmail, string myTaskUrl)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    receiverEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendSubmitNotificationEmails is not object[] type for parameters");
#if !DEBUG
                    emailMgr.SendSubmitNotificationEmails(parameters[0].ToString(), parameters[1].ToString(), parameters[2].ToString(), parameters[3].ToString());
#endif
                },
                new object[] { fhaNumber, senderEmail, receiverEmail, myTaskUrl });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendR4RAutoApprovalNotificationEmail(IEmailManager emailManager, string aeEmail, string requestorEmail,
            ReserveForReplacementFormModel model)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    aeEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendR4RAutoApprovalNotificationEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendR4RAutoApprovalNotificationEmail(parameters[0].ToString(), parameters[1].ToString(), parameters[2] as ReserveForReplacementFormModel);
#endif
                },
                new object[] { aeEmail, requestorEmail, model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendR4RSubmitNotificationEmail(IEmailManager emailManager, string aeEmail, string requestorEmail, ReserveForReplacementFormModel model)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    aeEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendR4RAutoApprovalNotificationEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendR4RSubmitNotificationEmail(parameters[0].ToString(), parameters[1].ToString(),parameters[2] as ReserveForReplacementFormModel);
#endif
                },
                new object[] { aeEmail, requestorEmail, model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendR4RApprovalNotificationEmail(IEmailManager emailManager, string requestorEmail, string aeEmail,
            ReserveForReplacementFormModel model)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    aeEmail = requestorEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendR4RAutoApprovalNotificationEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendR4RApprovalNotificationEmail(parameters[0].ToString(), parameters[1].ToString(), parameters[2] as ReserveForReplacementFormModel);
#endif
                },
                new object[] { aeEmail, requestorEmail, model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }


        public bool SendNonCriticalRequestExtensionApprovalNotificationEmail(IEmailManager emailManager, string aeEmail, string requestorEmail,
            NonCriticalRequestExtensionModel model)
        {
            try
            {
                //karri
                //karri#62
                if (_IsUATTTestInProgress)
                    requestorEmail = aeEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendNonCriticalRequestExtensionApprovalNotificationEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendNonCriticalRequestExtensionApprovalNotificationEmail(parameters[0].ToString(), parameters[1].ToString(), parameters[2] as NonCriticalRequestExtensionModel);
#endif
                },
                new object[] { aeEmail, requestorEmail, model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendNonCriticalRequestApprovalNotificationEmail(IEmailManager emailManager, string aeEmail, string requestorEmail,
            NonCriticalRepairsViewModel model)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    requestorEmail = aeEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendNonCriticalRequestApprovalNotificationEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendNonCriticalRequestApprovalNotificationEmail(parameters[0].ToString(), parameters[1].ToString(), parameters[2] as NonCriticalRepairsViewModel);
#endif
                },
                new object[] { aeEmail, requestorEmail, model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendMissingFhaNotificationEmail(IEmailManager emailManager, MissingFhaNumberEmailModel model)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendR4RAutoApprovalNotificationEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendMissingFhaNotificationEmail(parameters[0] as MissingFhaNumberEmailModel);
#endif
                },
                new object[] { model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }


        public bool SendUpdateiREMSNotificationEmail(IEmailManager emailManager, string emailText, string propertyName, string fhaNumber)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendR4RAutoApprovalNotificationEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendUpdateiREMSNotificationEmail(parameters[0].ToString(), parameters[1].ToString(), parameters[2].ToString());
#endif
                },
                new object[] { emailText, propertyName, fhaNumber });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendProjectActionSubmissionEmail(IEmailManager emailManager, string assignedTo, string assignedBy, ProjectActionViewModel projectActionViewModel)
        {

            // emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    assignedTo = assignedBy = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendProjectActionSubmissionEmail is not object[] type for parameters");
#if !DEBUG
                    var emailPASubmit =
                        new EmailTemplateMessagesManager().GetEmailTemplate(EmailType.ProjectActionSubmission);

                    emailManager.SendProjectActionSubmissionEmail(EmailType.ProjectActionSubmission, assignedBy, assignedTo,projectActionViewModel);
#endif
                },
                new object[] { projectActionViewModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }

        public bool SendTPALightPreliminarySubmissionEmail(IEmailManager emailManager, string assignedTo, string assignedBy, OPAViewModel projectActionViewModel)
        {

            //emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    assignedTo = assignedBy = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendProjectActionSubmissionEmail is not object[] type for parameters");
#if !DEBUG
                    //var emailPASubmit = new EmailTemplateMessagesManager().GetEmailTemplate(EmailType.TPALIGHT);

                    //emailManager.SendTPALightPreliminarySubmissionEmail(EmailType.TPALIGHT, assignedBy, assignedTo,projectActionViewModel);

                    //naveen
                    var emailPASubmit = new EmailTemplateMessagesManager().GetEmailTemplate(EmailType.ProjectActionSubmission);
                    //naveen
                    emailManager.SendTPALightPreliminarySubmissionEmail(EmailType.ProjectActionSubmission, assignedBy, assignedTo, projectActionViewModel);
#endif
                },
                new object[] { projectActionViewModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }



        public bool SendOPASubmissionEmail(IEmailManager emailManager, string assignedTo, string assignedBy, OPAViewModel projectActionViewModel)
        {

            //emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    assignedTo = assignedBy = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendProjectActionSubmissionEmail is not object[] type for parameters");
#if !DEBUG
                    var emailPASubmit =
                        new EmailTemplateMessagesManager().GetEmailTemplate(EmailType.ProjectActionSubmission);

                    emailManager.SendOPASubmissionEmail(EmailType.ProjectActionSubmission, assignedBy, assignedTo,projectActionViewModel);
#endif
                },
                new object[] { projectActionViewModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }


        public bool SendProjectActionSubmissionResponseEmail(IEmailManager emailManager, string assignedTo, string assignedBy, ProjectActionViewModel projectActionViewModel)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendProjectActionSubmissionResponseEmail is not object[] type for parameters");

                    var assignedToUser = accountManager.GetUserByUsername(assignedTo);
                    var assignedByUser = accountManager.GetUserByUsername(assignedBy);

                    // keywords: [SubmittedDate],[ProjectActionName],[PropertyName],[FHANumber],[ApprovalStatus],[AEName],[AEEmail],[ApprovedDate]
                    string keywords = TimezoneManager.FormatDate(projectActionViewModel.CreatedOn, "{0:MM/dd/yyyy}") + "|" + projectActionViewModel.ProjectActionName +
                                      "|" + projectActionViewModel.PropertyName + "|" + projectActionViewModel.FhaNumber +
                                      "|" +
                                      (projectActionViewModel.RequestStatus == (int)RequestStatus.Approve
                                          ? "approved"
                                          : "denied") + "|" + assignedByUser.FirstName + " " + assignedByUser.LastName +
                                      "|" + assignedByUser.EmailAddress + "|" + TimezoneManager.FormatDate(DateTime.Now, "{0:MM/dd/yyyy}") + "|" +
                                      (projectActionViewModel.RequestStatus == (int)RequestStatus.Approve
                                          ? "Approval"
                                          : "Denial");
#if !DEBUG
                    emailManager.SendEmail(EmailType.PARESPONSE, keywords, assignedToUser.UserID + "," + assignedByUser.UserID);
#endif
                },
                new object[] { projectActionViewModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }
        public bool SendProdTempTaskReassignmentEmail(IEmailManager emailManager, string AssigneFromEmail, string AssigneFromName, string AssignetoEmail, string AssignetoName, string FHANumber, string ProjectName, string ProductionTaskType, string viewname)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    AssigneFromEmail = AssignetoEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendTaskReassignmentEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendProdTempTaskReassignmentEmail(EmailType.PTSKRASSGN, AssigneFromEmail, AssigneFromName, AssignetoEmail, AssignetoName, FHANumber, ProjectName, ProductionTaskType, viewname);
#endif


                },
                new object[] { AssigneFromEmail, AssigneFromName, AssignetoEmail, AssignetoName, FHANumber, ProjectName, ProductionTaskType, viewname
            });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }
        public bool SendTaskReassignmentEmail(IEmailManager emailManager, TaskReAssignmentViewModel model)
        {

            // emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    model.SelectedAEEmail = model.SelectedReassignAEEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendTaskReassignmentEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendTaskReassignmentEmail(EmailType.TSKREASSGN, model);
#endif


                },
                new object[] { model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }

        public static bool Excecute(WaitCallback fpCallback, Object threadContext)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(fpCallback, threadContext);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendProjectActionSubmissionResponseEmail(IEmailManager emailManager, string assignedTo, string assignedBy, OPAViewModel opaViewModel)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendProjectActionSubmissionResponseEmail is not object[] type for parameters");

                    var assignedToUser = accountManager.GetUserByUsername(assignedTo);
                    var assignedByUser = accountManager.GetUserByUsername(assignedBy);
                    string approvalStatus = string.Empty;
                    string requestStatus = string.Empty;
                    string comment = string.Empty;
                    if (opaViewModel.RequestStatus == (int)RequestStatus.Approve)
                    {
                        approvalStatus = "approved";
                        requestStatus = "Approval";
                        comment = opaViewModel.AEComments != null ? opaViewModel.AEComments.Trim().ToString() : string.Empty;
                    }
                    else if (opaViewModel.RequestStatus == (int)RequestStatus.RequestAdditionalInfo)
                    {
                        approvalStatus = "requested for additional information";
                        requestStatus = "Request Additional Information";
                        comment = opaViewModel.RequestAdditionalComment != null ? opaViewModel.RequestAdditionalComment.Trim().ToString() : string.Empty;
                    }
                    else
                    {
                        approvalStatus = "denied";
                        requestStatus = "Denial";
                        comment = opaViewModel.AEComments != null ? opaViewModel.AEComments.Trim().ToString() : string.Empty;
                    }
                    #if !DEBUG                  
                    if (opaViewModel.pageTypeId == 10)
                    {
                        string keywords = opaViewModel.ProjectActionName + "|" + opaViewModel.FhaNumber;
                        if (approvalStatus == "denied")
                        {
                            emailManager.SendEmail(EmailType.DCREJECT, keywords, assignedToUser.UserID + "," + assignedByUser.UserID, false, true);
                        }
                    }
					if (opaViewModel.pageTypeId == 18)
					{
						string keywords = opaViewModel.FhaNumber + "|" + opaViewModel.ProjectActionName ;
						if (approvalStatus == "denied")
						{
							emailManager.SendEmail(EmailType.DNYAMEND, keywords, assignedToUser.UserID + "," + assignedByUser.UserID, false, true);
						}
					}
					else
                    {
                        comment = comment != string.Empty ? "Comments: " + comment : string.Empty;
                        // keywords: [SubmittedDate],[ProjectActionName],[PropertyName],[FHANumber],[ApprovalStatus],[AEName],[AEEmail],[ApprovedDate]
                        string keywords = TimezoneManager.FormatDate(opaViewModel.CreatedOn, "{0:MM/dd/yyyy}") + "|" + opaViewModel.ProjectActionName +
                                          "|" + opaViewModel.PropertyName + "|" + opaViewModel.FhaNumber +
                                          "|" + approvalStatus + "|" + assignedByUser.FirstName + " " + assignedByUser.LastName +
                                          "|" + assignedByUser.EmailAddress + "|" + TimezoneManager.FormatDate(DateTime.Now, "{0:MM/dd/yyyy}") + "|" + requestStatus + "|<p>" + comment + "</p>";

                        emailManager.SendEmail(EmailType.PARESPONSE, keywords, assignedToUser.UserID + "," + assignedByUser.UserID, false, true);
                    }
					#endif
				},
                new object[] { opaViewModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendFHARequestCompletionEmail(IEmailManager emailManager, string lenderEmail, string prodUserEmail,
            FHANumberRequestViewModel fhaRequestModel)
        {

            // emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    lenderEmail = prodUserEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendFHARequestCompletionEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendFHARequestCompletionEmail(EmailType.FHARQST, lenderEmail, prodUserEmail, fhaRequestModel);
#endif


                },
                new object[] { fhaRequestModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }

        public bool SendFHAInsertCompletionEmail(IEmailManager emailManager, string lenderEmail, string prodUserEmail,
            FHANumberRequestViewModel fhaRequestModel)
        {

            // emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    lenderEmail = prodUserEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendFHAInsertCompletionEmail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendFHAInsertCompletionEmail(EmailType.FHAINSERT, lenderEmail, prodUserEmail, fhaRequestModel);
#endif


                },
                new object[] { fhaRequestModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }



        public bool SendAmmendmentAssignmentEmailToUW(IEmailManager emailManager, OPAViewModel AppProcessModel)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendAppProcessSubmitEmail is not object[] type for parameters");
#if !DEBUG

                    if (AppProcessModel.pageTypeId == 17)
                        emailManager.SendAppProcessSubmitEmail(EmailType.NONCEC, AppProcessModel);
                    else if (AppProcessModel.pageTypeId == 18)
                        emailManager.SendAppProcessSubmitEmail(EmailType.LENSUBAMND, AppProcessModel);
                    else
                        emailManager.SendAppProcessSubmitEmail(EmailType.APPPRSUB, AppProcessModel);
#endif


                },
                new object[] { AppProcessModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendAmmendmentAssignmentEmailToUWCloser(IEmailManager emailManager, string toemail, OPAViewModel AppProcessModel)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                   toemail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: l SendAppProcessAssignmentEmail is not object[] type for parameters");
                    #if !DEBUG

                    emailManager.SendAmmendmentAssignmentEmailToUWC(EmailType.AMUWCLOSER, toemail, AppProcessModel);
                    #endif


                },
                new object[] { AppProcessModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }



        public bool SendAppProcessAssignmentEmail(IEmailManager emailManager, string toemail, OPAViewModel AppProcessModel)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    toemail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: l SendAppProcessAssignmentEmail is not object[] type for parameters");
                    #if !DEBUG

                    emailManager.SendAppProcessAssignmentEmail(EmailType.APPRQSTASSIGN, toemail, AppProcessModel);
                    #endif


                },
                new object[] { AppProcessModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendAppProcessSubmitEmail(IEmailManager emailManager, OPAViewModel AppProcessModel)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendAppProcessSubmitEmail is not object[] type for parameters");
#if !DEBUG
                    if(AppProcessModel.pageTypeId == 17)
                        emailManager.SendAppProcessSubmitEmail(EmailType.NONCEC, AppProcessModel);
                    else if(AppProcessModel.pageTypeId == 18)
                        emailManager.SendAppProcessSubmitEmail(EmailType.LENSUBAMND, AppProcessModel);
                    else
                        emailManager.SendAppProcessSubmitEmail(EmailType.APPPRSUB, AppProcessModel);
#endif


                },
                new object[] { AppProcessModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        public bool SendAppProcessLenderRAISubmitEmail(IEmailManager emailManager, List<string> toemails, OPAViewModel AppProcessModel)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                {
                    var list = new List<string>() { };
                    string[] groupmails = _UATTestersMails.Split(',');
                    foreach (string eachmail in groupmails)
                    {
                        list.Add(eachmail);
                    }

                    toemails = list;
                }
               
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: AppProcessLenderRAISubmitEmail is not object[] type for parameters");
                    #if !DEBUG

                    emailManager.SendAppProcessLenderRAISubmitEmail(EmailType.APPPRLCSUB, toemails, AppProcessModel);
                    #endif


                },
                new object[] { AppProcessModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }
        public string SendEmailToProdUsers(IEmailManager emailManager, prod_EmailToProdUser notificationEmailToProdUser)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                {
                    prod_EmailToProdUser locallist = new prod_EmailToProdUser();
                    
                    var list = new List<string>() { };
                    string[] groupmails = _UATTestersMails.Split(',');
                    foreach (string eachmail in groupmails)
                    {
                        locallist.ToProdUsers.Add(eachmail);
                    }

                    notificationEmailToProdUser = locallist;
                }

                EmailType emailtype = new EmailType();
                emailtype = EmailType.IRRLMAR;
                return emailManager.SendIRRLoanModificationEmail(emailtype, notificationEmailToProdUser);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return "false";
            }
        }

		public string SendEmailToProdWLMUsers(IEmailManager emailManager, prod_EmailToProdUser notificationEmailToProdUser)
		{
			try
			{
                //karri#62
                if (_IsUATTTestInProgress)
                {
                    prod_EmailToProdUser locallist = new prod_EmailToProdUser();

                    var list = new List<string>() { };
                    string[] groupmails = _UATTestersMails.Split(',');
                    foreach (string eachmail in groupmails)
                    {
                        locallist.ToProdUsers.Add(eachmail);
                    }

                    notificationEmailToProdUser = locallist;
                }

                EmailType emailtype = new EmailType();
				emailtype = EmailType.WLMAMEND;
				return emailManager.SendIRRLoanModificationEmail(emailtype, notificationEmailToProdUser);
			}
			catch (Exception e)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(e);
				return "false";
			}
		}
		public string SendLenderNotificationsEmail(IEmailManager emailManager, Prod_EmailToLenderModel NotificationsToLenderModel)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                {
                    NotificationsToLenderModel.CCEmails.Clear();
                    NotificationsToLenderModel.SendToEmail = _UATTestersMails;
                }

                EmailType emailtype = new EmailType();

                switch (NotificationsToLenderModel.EmailTemplateId)
                {
                    case "FRMCMMTMNT":
                        emailtype = EmailType.FRMCMMTMNT;
                        break;
                    case "ERSTRAPP":
                        emailtype = EmailType.ERSTRAPP;
                        break;
                    case "INCLDATE":
                        emailtype = EmailType.INCLDATE;
                        break;
                    case "FIRMAP":
                        emailtype = EmailType.FIRMAP;
                        break;
                    case "SSTGS":
                        emailtype = EmailType.SSTGS;
                        break;
                    case "INISUB":
                        emailtype = EmailType.INISUB;
                        break;
                    case "FNLS":
                        emailtype = EmailType.FNLS;
                        break;
                    case "IRRAP":
                        emailtype = EmailType.IRRAP;
                        break;
                    case "FRM290CLOS":
                        emailtype = EmailType.FRM290CLOS;
                        break;
					case "WLMAMEND":
						emailtype = EmailType.WLMAMEND;
						break;
					case "WLMAMEND1":
						emailtype = EmailType.WLMAMEND1;
						break;
					case "WLMAMEND2":
						emailtype = EmailType.WLMAMEND2;
						break;
					case "IRRCLSG":
                        emailtype = EmailType.IRRCLSG;
                        return emailManager.SendIRRFinalApprovalEmail(emailtype, NotificationsToLenderModel);
                }
                //return ThreadPool.QueueUserWorkItem(delegate(object threadContext)
                //{
                //    var parameters = threadContext as object[];
                //    if (parameters == null)
                //        throw new InvalidCastException("BackgroundJobMgr: SendLenderNotificationsEmail is not object[] type for parameters");

                //    emailManager.SendLenderNotificationsEmail(emailtype, NotificationsToLenderModel);
                //},
                //new object[] { NotificationsToLenderModel });
                return emailtype == EmailType.IRRAP ? emailManager.SendIRRApprovalNotificationEmail(emailtype, NotificationsToLenderModel) : emailManager.SendLenderNotificationsEmail(emailtype, NotificationsToLenderModel);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return "false";
            }
        }

        public bool SendIrrInitiationSuccess(IEmailManager emailManager, FHANumberRequestViewModel fhaRequestModel,
            string lenderEmail)
        {

            // emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    lenderEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendIrrInitiationSuccess is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendIrrInitiationSuccess(EmailType.IRRINIT, fhaRequestModel, lenderEmail);
#endif


                },
                new object[] { fhaRequestModel });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }


        public bool SendFirmCommitmentAppSuccess(IEmailManager emailManager, int Pagetypeid, Guid Taskinstanceid)
        {

            // emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                //could not do as it is too complex

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendIrrInitiationSuccess is not object[] type for parameters");
#if !DEBUG
                   emailManager.SendFirmCommitmentAppSuccess(Pagetypeid, Taskinstanceid);
#endif


                },
                new object[] { null });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }
        
        public bool SendR4RDenyNotificationEmail(IEmailManager emailManager, ReserveForReplacementFormModel model, string denyReason)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    model.AssignedBy = model.AssignedTo = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendIrrInitiationSuccess is not object[] type for parameters");
#if !DEBUG
                   emailManager.SendR4RDenyNotificationEmail(model, denyReason);
#endif


                },
                new object[] { null });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        //karri:#149,#214
        public bool SendHelpDeskNotificationEmail(IEmailManager emailManager, HD_TICKET_ViewModel model)
        {
            try
            {
                //karri#62//30082019;karri
                if (_IsUATTTestInProgress)
                {
                    //karri;sup#187
                    model.MAILNOTIFICATION_TOEMAIL = _UATTestersMails;
                    model.MAILNOTIFICATION_REQUESTOREMAIL = _UATTestersMails;
                    model.CREATED_BY_USERNAME = _UATTestersMails;
                    // model.AssignedBy = model.AssignedTo = _UATTestersMails;
                }

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: Help Desk Notificaton mail is not object[] type for parameters");
                    #if !DEBUG
                    emailManager.SendHelpDeskNotificationEmail("", "", parameters[0] as HD_TICKET_ViewModel);
                    #endif
                },
                new object[] { model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }
        // #234
        // Harish
        public bool SendFirmcommittmenttaskEmail(IEmailManager emailManager, string toEmail, string requestorEmail, string userInfoDataString)
        {
           // var model = new ProductionMyTaskModel();
           // toEmail = model.AssignedTo;
            //toEmail = UserPrincipal.Current.UserName;
            //emailManager.SendEmail(emailType, keywords, userIds, true);
            try
            {
                //karri#62
                //if (_IsUATTTestInProgress)
                //    toEmail = requestorEmail = _UATTestersMails;
                if (_IsUATTTestInProgress)
                    requestorEmail= toEmail  ;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendProjectActionSubmissionEmail is not object[] type for parameters");
#if !DEBUG
                    //var emailPASubmit =
                    //    new EmailTemplateMessagesManager().GetEmailTemplate(EmailType.ProjectActionSubmission);

                    emailManager.SendFirmcommitmentNotificationEmail(toEmail, requestorEmail, userInfoDataString);
#endif
                },
                new object[] { userInfoDataString });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }

        // #234 

        public bool SendFirmCommitmentNotificationEmail(IEmailManager emailManager, ProductionMyTaskModel model)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: Help Desk Notificaton mail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendFirmCommittmentNotificationEmail(emailManager, model);
#endif
                },
                new object[] { model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        //karri:31082019
        public bool SendExternalHelpDeskNotificationEmail(IEmailManager emailManager, HD_TICKET_ViewModel model)
        {
            try
            {
                if (_IsUATTTestInProgress)
                {
                    model.MAILNOTIFICATION_TOEMAIL = _UATTestersMails;
                    model.MAILNOTIFICATION_REQUESTOREMAIL = _UATTestersMails;
                    // model.AssignedBy = model.AssignedTo = _UATTestersMails;
                }
                else
                {
                    model.MAILNOTIFICATION_TOEMAIL = model.EXTERNALMAILID;
                    model.MAILNOTIFICATION_REQUESTOREMAIL = model.EXTERNALMAILID;
                }

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: Help Desk Notificaton mail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendExternalHelpDeskNotificationEmail("", "", parameters[0] as HD_TICKET_ViewModel);
#endif
                },
                new object[] { model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        // harish added new line of code to send mail to servicer
        //07-01-2020
        public bool SendRISNotificationEmail(IEmailManager emailManager, TaskModel model)
        {
            try
            {
                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: Help Desk Notificaton mail is not object[] type for parameters");
#if !DEBUG
                    emailManager.SendRISNotificationEmail(emailManager, model);
#endif
                },
                new object[] { model });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        // harish added 23042020
        public bool SendLenderProdTempTaskReassignmentEmail(IEmailManager emailManager, string AssigneFromEmail, string AssigneFromName, string AssignetoEmail, string AssignetoName, string FHANumber, string ProjectName, string ProductionTaskType, string viewname)
        {
            try
            {
                //karri#62
                if (_IsUATTTestInProgress)
                    AssigneFromEmail = AssignetoEmail = _UATTestersMails;

                return ThreadPool.QueueUserWorkItem(delegate (object threadContext)
                {
                    var parameters = threadContext as object[];
                    if (parameters == null)
                        throw new InvalidCastException("BackgroundJobMgr: SendLenderProdTempTaskReassignmentEmail is not object[] type for parameters");
                 #if !DEBUG
                    emailManager.SendLenderProdTempTaskReassignmentEmail(EmailType.LTSKRASSGN, AssigneFromEmail, AssigneFromName, AssignetoEmail, AssignetoName, FHANumber, ProjectName, ProductionTaskType, viewname);
                   #endif


                },
                new object[] { AssigneFromEmail, AssigneFromName, AssignetoEmail, AssignetoName, FHANumber, ProjectName, ProductionTaskType, viewname
            });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }

        }


    }
}
