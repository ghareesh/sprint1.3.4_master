﻿using System.Threading.Tasks;
using BusinessService.Interfaces;
using Core;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Repository.Interfaces;
using Model;
using Repository;
using Repository.Interfaces;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;
using System.Linq;
using Repository.Interfaces.ProjectAction;
using Repository.ProjectAction;
using Repository.Interfaces.OPAForm;
using Repository.OPAForm;
using System.Collections;

namespace HUDHealthcarePortal.BusinessService
{
    public class TaskReAssignmentManager : ITaskReAssignmentManager
    {
        //karri#205
        private string _lookupFHANumber = string.Empty;

        private ITaskReAssignmentRepository taskReAssignmentRepository;
        private IHUDAccountExecutiveRepository aeRepository;
        private IHUDWorkloadManagerRepository wlmRepository;
        private IHUDWorkloadInternalSpecialOptionUserRepository isouRepository;
        private ITaskRepository taskRepository;
        private IHUDWorkloadPortfolioManagerRepository wlmpmRepository;
        private UnitOfWork unitOfWorkLive;
        private UnitOfWork unitOfWorkTask;
        private IProjectActionFormRepository projectActionFormRepository;
        private IParentChildTaskRepository ParentChildTaskRepository;
        private IOPARepository OPARepository;

        public TaskReAssignmentManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
            taskReAssignmentRepository = new TaskReAssignmentRepository(unitOfWorkTask);
            aeRepository = new HUDAccountExecutiveRepository(unitOfWorkLive);
            taskRepository = new TaskRepository(unitOfWorkTask);
            wlmRepository = new HUDWorkLoadManagerRepository(unitOfWorkLive);
            isouRepository = new HUDWorkloadInternalSpecialOptionUserRepository(unitOfWorkLive);
            wlmpmRepository = new HUDWorkloadPortfolioManagerRepository(unitOfWorkLive);
            projectActionFormRepository = new ProjectActionFormRepository(unitOfWorkLive);
            ParentChildTaskRepository = new ParentChildTaskRepository(unitOfWorkTask);
            OPARepository = new OPARepository(unitOfWorkLive);
        }

        public IEnumerable<KeyValuePair<string, string>> GetAllTaskAgeIntervals()
        {
            return taskReAssignmentRepository.GetAllTaskAgeIntervals();
        }

        public PaginateSortModel<TaskDetailPerAeModel> GetTaskDetailsForAE(string aeIds, string fromDate, string toDate, string taskAge,
            string projectAction, int? page, string sort, SqlOrderByDirecton sortDir)
        {
            var fromdate = string.IsNullOrEmpty(fromDate) ? (DateTime?)null : Convert.ToDateTime(fromDate);
            var todate = string.IsNullOrEmpty(toDate) ? (DateTime?)null : Convert.ToDateTime(toDate);
            int fromTaskAge = 0;
            int toTaskAge = 0;
            if (!string.IsNullOrEmpty(taskAge))
            {
                if (taskAge.Contains('-'))
                {
                    var taskStr = taskAge.Split('-');
                    fromTaskAge = int.Parse(taskStr[0]);
                    toTaskAge = int.Parse(taskStr[1]);
                }
                else
                {
                    fromTaskAge = int.Parse(taskAge);
                }

            }
            return taskReAssignmentRepository.GetTaskDetailsForAE(aeIds, fromdate, todate, fromTaskAge, toTaskAge, projectAction, page, sort, sortDir);
        }

        //karri#205
        //GETTING FHA NUMBERS IS COMPLEX HERE, HENCE DERIVIED THE CODE FROM BELOW FUNCTION:public PaginateSortModel<TaskModel> GetTasksByUserName(string userName, int page, string sort, SqlOrderByDirecton sortDir)
        public IList<String> getFHAList(string userName)
        {
            ArrayList arrColl = new ArrayList();
            IList<String> iListColl = new List<string>();

            var results = taskReAssignmentRepository.GetReAssginedTasksByUserName(userName).ToList();
            foreach (var item in results)
            {
                if (item.FHANumber == null || string.IsNullOrEmpty(item.FHANumber))
                {
                    var taskType = EnumType.Parse<TaskStep>(item.TaskStepId.ToString());
                    item.TaskName = EnumType.GetEnumDescription(taskType);
                    if (item.DataStore1.Contains("NonCriticalRequestExtensionModel"))
                    {
                        var formUploadData =
                            XmlHelper.Deserialize(typeof(NonCriticalRequestExtensionModel), item.DataStore1, Encoding.Unicode)
                                as NonCriticalRequestExtensionModel;
                        if (formUploadData != null)
                            item.FHANumber = formUploadData.FHANumber;
                    }
                    else if (item.DataStore1.Contains("NonCriticalRepairsViewModel"))
                    {
                        var formUploadData =
                            XmlHelper.Deserialize(typeof(NonCriticalRepairsViewModel), item.DataStore1, Encoding.Unicode)
                                as NonCriticalRepairsViewModel;
                        if (formUploadData != null)
                            item.FHANumber = formUploadData.FHANumber;
                    }
                    else if (item.DataStore1.Contains("ReserveForReplacementFormModel"))
                    {
                        var formUploadData =
                            XmlHelper.Deserialize(typeof(ReserveForReplacementFormModel), item.DataStore1, Encoding.Unicode)
                                as ReserveForReplacementFormModel;
                        if (formUploadData != null)
                            item.FHANumber = formUploadData.FHANumber;
                    }
                    else if (item.DataStore1.Contains("OPAViewModel"))
                    {
                        var formUploadData =
                            XmlHelper.Deserialize(typeof(OPAViewModel), item.DataStore1, Encoding.Unicode)
                                as OPAViewModel;
                        if (formUploadData != null)
                            item.FHANumber = formUploadData.FhaNumber;
                    }
                    else
                    {
                        var formUploadData =
                            XmlHelper.Deserialize(typeof(FormUploadModel), item.DataStore1, Encoding.Unicode) as FormUploadModel;
                        if (formUploadData != null)
                            item.FHANumber = formUploadData.FHANumber;
                    }
                }
            }

            var model = new PaginateSortModel<TaskReAssignmentViewModel>();
            model.Entities = results.ToList();

            if (results != null)
            {
                foreach (var r in results)
                {
                    if (!arrColl.Contains(r.FHANumber))
                        arrColl.Add(r.FHANumber);
                }

                arrColl.Sort();//by default ASC
                foreach (string value in arrColl)
                    iListColl.Add(value);
            }

            return iListColl;
        }

        //karri#205
        public void setLookUpValues(string lookupFHANumber)
        {
            if (lookupFHANumber != null && !string.IsNullOrEmpty(lookupFHANumber))
                _lookupFHANumber = lookupFHANumber;
        }

        public PaginateSortModel<TaskReAssignmentViewModel> GetReAssignedTasksByUserName(string userName, int page, string sort,
            SqlOrderByDirecton sortDir)
        {
            var results = taskReAssignmentRepository.GetReAssginedTasksByUserName(userName).ToList();
            foreach (var item in results)
            {
                var taskType = EnumType.Parse<TaskStep>(item.TaskStepId.ToString());
                item.TaskName = EnumType.GetEnumDescription(taskType);
                if (item.DataStore1.Contains("NonCriticalRequestExtensionModel"))
                {
                    var formUploadData =
                        XmlHelper.Deserialize(typeof(NonCriticalRequestExtensionModel), item.DataStore1, Encoding.Unicode)
                            as
                            NonCriticalRequestExtensionModel;
                    if (formUploadData != null)
                    {
                        item.ItemName = formUploadData.PropertyName;
                        item.FHANumber = formUploadData.FHANumber;
                    }
                }
                else if (item.DataStore1.Contains("NonCriticalRepairsViewModel"))
                {
                    var formUploadData =
                        XmlHelper.Deserialize(typeof(NonCriticalRepairsViewModel), item.DataStore1, Encoding.Unicode)
                            as
                            NonCriticalRepairsViewModel;
                    if (formUploadData != null)
                    {
                        item.ItemName = formUploadData.PropertyName;
                        item.FHANumber = formUploadData.FHANumber;
                    }
                }
                else if (item.DataStore1.Contains("ReserveForReplacementFormModel"))
                {
                    var formUploadData =
                        XmlHelper.Deserialize(typeof(ReserveForReplacementFormModel), item.DataStore1, Encoding.Unicode)
                            as
                            ReserveForReplacementFormModel;
                    if (formUploadData != null)
                    {
                        item.ItemName = formUploadData.PropertyName;
                        item.FHANumber = formUploadData.FHANumber;
                    }
                }
                else if (item.DataStore1.Contains("OPAViewModel"))
                {
                    var formUploadData =
                        XmlHelper.Deserialize(typeof(OPAViewModel), item.DataStore1, Encoding.Unicode)
                            as
                            OPAViewModel;
                    if (formUploadData != null)
                    {
                        var projectActionName =
    projectActionFormRepository.GetProjectActionNameforAssetmanagement(formUploadData.ProjectActionTypeId);
                        //var projectActionName =
                        //    projectActionFormRepository.GetProjectActionName(formUploadData.ProjectActionTypeId);
                        item.TaskName = item.TaskName.Replace("Project Action Request", projectActionName);
                        item.ItemName = formUploadData.PropertyName;
                        item.FHANumber = formUploadData.FhaNumber;
                    }
                }
                else
                {
                    var formUploadData =
                        XmlHelper.Deserialize(typeof(FormUploadModel), item.DataStore1, Encoding.Unicode) as
                            FormUploadModel;
                    if (formUploadData != null)
                    {
                        item.ItemName = formUploadData.ProjectName;
                        item.FHANumber = formUploadData.FHANumber;
                    }
                }
                //item.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(item.StartTime);
                //item.CreatedOn = TimezoneManager.GetPreferredTimeFromUtc(item.CreatedOn);
                item.MyStartTime = item.StartTime;
                item.CreatedOn = item.CreatedOn;
            }
            SetStatusForTasks(results);
            var model = new PaginateSortModel<TaskReAssignmentViewModel>();
            model.Entities = results.ToList();
            model.PageSize = 10;
            model.TotalRows = results.ToList().Count();

            //karri#205
            if (_lookupFHANumber != null && !string.IsNullOrEmpty(_lookupFHANumber))
            {
                var query1 = model.Entities.Where(m => m.FHANumber == _lookupFHANumber).AsQueryable();
                return PaginateSort.SortAndPaginate(query1, sort, sortDir, model.PageSize, page);
            }

            var query = model.Entities.AsQueryable();
            return PaginateSort.SortAndPaginate(query, sort, sortDir, model.PageSize, page);
        }

        private static void SetStatusForTasks(IEnumerable<TaskReAssignmentViewModel> results)
        {

            foreach (var taskModel in results)
            {
                if (taskModel.TaskStepId == (int)TaskStep.Final)
                    taskModel.Status = FormUploadStatus.Done;
                else if (taskModel.AssignedTo.ToLower() != UserPrincipal.Current.UserName.ToLower())
                {
                    if (taskModel.AssignedBy != taskModel.ReAssignedTo &&
                       RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
                    {
                        taskModel.Status = FormUploadStatus.NotSpecified;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.R4RComplete && (RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)
                        || RoleManager.IsInternalSpecialOptionUser(taskModel.AssignedTo)))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRepairComplete && (RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)
                        || RoleManager.IsInternalSpecialOptionUser(taskModel.AssignedTo)))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && (RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)
                        || RoleManager.IsInternalSpecialOptionUser(taskModel.AssignedTo)))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete &&
                        (RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo) || RoleManager.IsInternalSpecialOptionUser(taskModel.AssignedTo)))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRepairComplete && taskModel.SequenceId == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.R4RComplete && taskModel.SequenceId == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete && taskModel.SequenceId == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && taskModel.SequenceId == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else
                    {
                        taskModel.Status = FormUploadStatus.AssignedToOther;
                    }
                }
                else
                {
                    if (taskModel.TaskStepId == (int)TaskStep.R4RComplete && !(RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)) && taskModel.SequenceId == 1)
                    {
                        taskModel.TaskStepId = (int)TaskStep.R4RRequest;
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRepairComplete && !(RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo))
                        && !(RoleManager.IsInternalSpecialOptionUser(taskModel.AssignedTo)) && taskModel.SequenceId == 1)
                    {
                        taskModel.TaskStepId = (int)TaskStep.NonCriticalRequest;
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && !(RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo))
                        && !(RoleManager.IsInternalSpecialOptionUser(taskModel.AssignedTo)) && taskModel.SequenceId == 1)
                    {
                        taskModel.TaskStepId = (int)TaskStep.ProjectActionRequest;
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete && !(RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)) && !(RoleManager.IsInternalSpecialOptionUser(taskModel.AssignedTo)) && taskModel.SequenceId == 1)
                    {
                        taskModel.TaskStepId = (int)TaskStep.NonCriticalRequestExtension;
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRepairComplete && (taskModel.SequenceId == 2 || taskModel.SequenceId == 0))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.R4RComplete && (taskModel.SequenceId == 2 || taskModel.SequenceId == 0))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && (taskModel.SequenceId == 2 || taskModel.SequenceId == 0))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete && (taskModel.SequenceId == 2 || taskModel.SequenceId == 0))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else
                    {
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                }
            }
        }

        public PaginateSortModel<TaskDetailPerAeModel> ReassignAE(string aeId, string reassignAeId, string reassignISOUId,
            string selectedTaskIds, string fromDate, string toDate, string taskAge, string projectAction, int page, string sort,
            SqlOrderByDirecton sortDir)
        {
            var taskReassignedList = new List<TaskReAssignmentViewModel>();
            var taskIdList = new List<Guid>();
            string toEmail = null;
            if (reassignAeId != null) toEmail = aeRepository.GetAccountExecutiveDetail(int.Parse(reassignAeId)).EmailAddress;
            else if (reassignISOUId != null) toEmail = isouRepository.GetISOUEmailById(int.Parse(reassignISOUId));
            var fromAE = aeRepository.GetAccountExecutiveDetail(int.Parse(aeId));
            if (selectedTaskIds != null)
            {
                var str = selectedTaskIds.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (str.Any())
                {
                    foreach (var id in str)
                    {
                        var selectedStr = id.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        if (selectedStr.Any() && selectedStr.Length == 2)
                        {
                            if (Convert.ToBoolean(selectedStr[1]))
                            {
                                var model = new TaskReAssignmentViewModel();
                                model.TaskInstanceId = Guid.Parse(selectedStr[0]);
                                taskIdList.Add(model.TaskInstanceId);
                                if (fromAE != null)
                                {
                                    model.FromAE = fromAE.EmailAddress;
                                }
                                model.ReAssignedTo = toEmail;
                                model.CreatedBy = UserPrincipal.Current.UserId;
                                model.CreatedOn = DateTime.UtcNow;
                                taskReassignedList.Add(model);
                            }
                        }
                    }
                }
            }

            //Code for adding childtask to the reassignment list
            var assignedto = string.Empty;
            if (fromAE != null)
            {
                assignedto = fromAE.EmailAddress;
            }
            var Childtasklist = OPARepository.GeetChildTaskforReassignment(taskIdList, assignedto);
            if (Childtasklist != null)
            {
                var distinctlist = Childtasklist.Distinct();
                foreach (var child in distinctlist)
                {
                    var model = new TaskReAssignmentViewModel();
                    model.TaskInstanceId = child;
                    taskIdList.Add(model.TaskInstanceId);
                    if (fromAE != null)
                    {
                        model.FromAE = fromAE.EmailAddress;
                    }
                    model.ReAssignedTo = toEmail;
                    model.CreatedBy = UserPrincipal.Current.UserId;
                    model.CreatedOn = DateTime.UtcNow;
                    taskReassignedList.Add(model);
                }
            }
            taskReAssignmentRepository.UpdateTaskReassignmentWithDeleted_Ind(taskIdList);
            taskReAssignmentRepository.InsertReassignedTasks(taskReassignedList);
            taskRepository.UpdateTaskReassigned(taskIdList);
            unitOfWorkTask.Save();
            return GetTaskDetailsForAE(aeId, fromDate, toDate, taskAge, projectAction, page, sort, sortDir);
        }

        public string GetAENameById(string aeId)
        {
            return aeRepository.GetAENamesListByIds(aeId).FirstOrDefault();
        }

        public string GetISOUNameById(int isouId)
        {
            return isouRepository.GetISOUNameById(isouId);
        }

        public string GetISOUEmailById(int isouId)
        {
            return isouRepository.GetISOUEmailById(isouId);
        }

        public List<string> GetInvalidTaskFHAsForIsou(int isouId, List<string> instanceIds)
        {
            List<string> isouFHAs = isouRepository.GetISOUFhaNumbers(isouId);
            List<string> reassignFHAs = taskRepository.GetFHANumbersByInstanceIds(instanceIds);
            var result = reassignFHAs.Where(p => !string.IsNullOrEmpty(p) && !isouFHAs.Any(p2 => p2 == p))
                .ToList<string>();
            return result;
        }

        public Boolean IsAllTaskNCRE(List<string> instanceIds)
        {
            List<int> taskIds = taskRepository.GetTaskIds(instanceIds);
            List<int> ncreIds = isouRepository.GetNCREs(taskIds);
            List<int> ncreExtIds = isouRepository.GetNCREExts(taskIds);
            if (taskIds.Count == ncreIds.Count + ncreExtIds.Count) return true;
            else return false;
        }

        public IList<HUDManagerModel> GetRegisteredAEsByWLMId(string wlmId)
        {
            return wlmRepository.GetRegisteredAEsByWLMId(wlmId, aeRepository);
        }

        public IList<HUDManagerModel> GetISOUIdsByWLMId(string wlmId)
        {
            return isouRepository.GetISOUIdsByWLMId(wlmId);
        }

        public UserViewModel GetAccountExecutiveDetail(int id)
        {
            return aeRepository.GetAccountExecutiveDetail(id);
        }

        public IList<TaskDetailPerAeModel> GetReassignedTasksForAE(string reAssignedTo, string selectedTaskIds)
        {
            var strArray = selectedTaskIds.Split(',');
            var selectedTaskInstanceIds = new List<Guid>();
            if (strArray.Any())
            {
                selectedTaskInstanceIds.AddRange(strArray.Select(Guid.Parse));
            }
            return taskReAssignmentRepository.GetReassignedTasksForAE(reAssignedTo, selectedTaskInstanceIds);
        }

        public string GetWorkloadManagerEmailFromAeId(int aeId)
        {
            return wlmpmRepository.GetWorkloadManagerEmailFromAeId(aeId);
        }

        public string GetWorkloadManagerEmailFromInternalSpecialOptionUserId(int isouId)
        {
            return isouRepository.GetWorkloadManagerEmailFromInternalSpecialOptionUserId(isouId);
        }

        public int GetWorkloadManagerIdFromInternalSpecialOptionUserId(int isouId)
        {
            return isouRepository.GetWorkloadManagerIdFromInternalSpecialOptionUserId(isouId);
        }

        public int GetReAssignedTasksCountByUserName(string userName)
        {
            var myReAssignedtasks = taskReAssignmentRepository.GetReAssginedTasksByUserName(userName);
            int count = 0;
            foreach (var task in myReAssignedtasks)
            {
                if ((RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) ||
                    RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName)) &&
                   (task.TaskStepId == 4 || task.TaskStepId == 5 || task.TaskStepId == 12 ||
                   task.TaskStepId == 14 || task.TaskStepId == 16) &&
                   (task.DataStore1.Contains("NonCriticalRepairsViewModel") ||
                   task.DataStore1.Contains("NonCriticalRequestExtensionModel") ||
                    task.DataStore1.Contains("ReserveForReplacementFormModel") ||
                    task.DataStore1.Contains("OPAViewModel")
                   )
                  )
                {
                    count++;
                }
            }
            return count;
        }

        public string GetReassignedAE(Guid TaskInstanceId)
        {
            return taskReAssignmentRepository.GetReassignedAE(TaskInstanceId);
        }
    }
}
