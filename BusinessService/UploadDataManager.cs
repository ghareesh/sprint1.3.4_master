﻿using System.Globalization;
using BusinessService;
using BusinessService.Interfaces;
using Core;
using Core.Utilities;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Interfaces;
using EntityObject.Entities.HCP_intermediate;
using Repository;
using EntityObject.Entities.HCP_live.Programmability;
using EntityObject.Entities.HCP_live;

namespace HUDHealthcarePortal.BusinessService
{
    public class UploadDataManager : IUploadDataManager

    {
        private IDataUploadIntermediateRepository dataUploadIntermediateRepository;
        private IUploadDataSummaryRepository uploadDataSummaryRepository;
        private IUserLenderServicerRepository userLenderServicerRepository;
        private IDataUploadLiveRepository dataUploadLiveRepository;
        private IUploadStatusRepository uploadStatusRepository;
        private IUploadStatusDetailRepository uploadStatusDetailRepository;
        private ICommentRepository commentRepository;
        private ICommentViewRepository commentViewRepository;
        private IScoreManager scoreManager;
        //written by siddesh  29082019 #344//
        private IUploadStatusHelpdeskreportsRepository uploadStatusHelpdeskreportsRepository;
        // don't use static unit of work, aka context, context should only last per http request due to entity framework identity pattern
        // may get stale data
        private UnitOfWork unitOfWorkIntermediate;
        private UnitOfWork unitOfWorkLive;
        private IFhaRepository fhaRepository;
        private ILenderFhaRepository lenderFhaRepository;
        private LenderInfoRepository lenderInfoRepository;
        private LenderFiscalYearDetailsRepository lenderFiscalYearDetailsRepository;
        //written by siddesh  29082019 #344//
        private UploadStatusHelpdeskreportsRepository UploadStatusHelpdeskreportsRepository;



        public UploadDataManager()
        {
            unitOfWorkIntermediate = new UnitOfWork(DBSource.Intermediate);
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            dataUploadIntermediateRepository = new DataUploadIntermediateRepository(unitOfWorkIntermediate);
            uploadDataSummaryRepository = new UploadDataSummaryRepository(unitOfWorkLive);
            uploadStatusRepository = new UploadStatusRepository(unitOfWorkIntermediate);
            uploadStatusDetailRepository = new UploadStatusDetailRepository(unitOfWorkIntermediate);
            userLenderServicerRepository = new UserLenderServicerRepository(unitOfWorkLive);
            dataUploadLiveRepository = new DataUploadLiveRepository(unitOfWorkLive);
            commentRepository = new CommentRepository(unitOfWorkLive);
            commentViewRepository = new CommentViewRepository(unitOfWorkIntermediate);
            fhaRepository = new FhaRepository(unitOfWorkLive);
            lenderFhaRepository = new LenderFhaRepository(unitOfWorkLive);
            lenderInfoRepository = new LenderInfoRepository(unitOfWorkLive);
            lenderFiscalYearDetailsRepository = new LenderFiscalYearDetailsRepository(unitOfWorkLive);
            //written by siddesh  29082019 #344//
            UploadStatusHelpdeskreportsRepository = new UploadStatusHelpdeskreportsRepository(unitOfWorkLive);
        }

        public ExcelUploadView_Model GetProjectDetailByID(int ldiID, DBSource source)
        {
            return source == DBSource.Intermediate ? dataUploadIntermediateRepository.GetProjectDetail(ldiID)
                : dataUploadLiveRepository.GetProjectDetailByLDI_ID(ldiID);
        }

        public IEnumerable<ReportExcelUpload_Model> GetDataUploadSummaryByUserId(int iUserId)
        {
            var results = uploadDataSummaryRepository.GetDataUploadSummaryByUserId(iUserId)
                .OrderByDescending(p => p.DataInserted);
            foreach (var item in results)
            {
                //item.DataInserted = TimezoneManager.GetPreferredTimeFromUtc(item.DataInserted);
                item.DataInserted = item.DataInserted;
            }
            return results;
        }

        public List<string> GetAllowedFhaByLenderUserId(int lenderUserId)
        {
            return userLenderServicerRepository.GetAllowedFhaByLenderUserId(lenderUserId, lenderFhaRepository);
        }

        public IEnumerable<string> GetAllowedFhasForUser(int userId)
        {
            return userLenderServicerRepository.GetAllowedFhasForUser(userId);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByRole(int iUserId, string[] userRoles)
        {
            int? iLenderId = UserPrincipal.Current.LenderId;
            if ((userRoles.Contains("Lender") || RoleManager.IsCurrentUserLenderRoles()) && !iLenderId.HasValue)
            {
                throw new InvalidOperationException(string.Format("User id: {0} is a lender, but does not have corresponding lender id in UserLender table", UserPrincipal.Current.UserId));
            }

            var results = dataUploadIntermediateRepository.GetUploadedData(iLenderId);
            return results.Select(p =>
                    new DerivedUploadData()
                    {
                        LDI_ID = p.LDI_ID,
                        ProjectName = p.ProjectName,
                        ServiceName = p.ServiceName,
                        PeriodEnding = p.PeriodEnding,

                        //UnitsInFacility = p.UnitsInFacility,
                        MonthsInPeriod = p.MonthsInPeriod,
                        FHANumber = p.FHANumber,

                        TotalRevenues = p.TotalRevenues,
                        TotalExpenses = p.TotalExpenses,
                        MortgageInsurancePremium = p.MortgageInsurancePremium,
                        FHAInsuredPrincipalInterestPayment = p.FHAInsuredPrincipalInterestPayment,

                        HasComment = p.HasComment
                    });
        }

        public IEnumerable<DerivedUploadData> GetUploadDataBySearch(int iUserId, string[] userRoles)
        {
            int? iLenderId = UserPrincipal.Current.LenderId;
            if ((userRoles.Contains("Lender") || RoleManager.IsCurrentUserLenderRoles()) && !iLenderId.HasValue)
            {
                throw new InvalidOperationException(string.Format("User id: {0} is a lender, but does not have corresponding lender id in UserLender table", UserPrincipal.Current.UserId));
            }
            if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName))
            {
                return dataUploadIntermediateRepository.GetUploadedDataByRole(null, commentViewRepository, null);
            }
            if (RoleManager.IsCurrentOperatorAccountRepresentative()
                || RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName)
                || RoleManager.IsCurrentSpecialOptionUser() || RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName))
            {
                var allowedFhas = GetAllowedFhasForUser(UserPrincipal.Current.UserId);
                return dataUploadIntermediateRepository.GetUploadedDataByRole(null, commentViewRepository, allowedFhas.ToList());
            }
            return dataUploadIntermediateRepository.GetUploadedDataByRole(iLenderId, commentViewRepository, null);
        }

        public PaginateSortModel<DerivedUploadData> GetUploadDataByRoleWithPageSort(int iUserId, string[] userRoles,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum)
        {
            int? iLenderId = UserPrincipal.Current.LenderId;
            if ((userRoles.Contains("Lender") || RoleManager.IsCurrentUserLenderRoles()) && !iLenderId.HasValue)
            {
                throw new InvalidOperationException(string.Format("User id: {0} is a lender, but does not have corresponding lender id in UserLender table", UserPrincipal.Current.UserId));
            }

            PaginateSortModel<DerivedUploadData> resultsRaw;
            if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName))
            {
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(null, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, null);
            }
            else if (RoleManager.IsCurrentOperatorAccountRepresentative()
                || RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName))
            {
                var allowedFhas = GetAllowedFhasForUser(UserPrincipal.Current.UserId);
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(null, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, allowedFhas.ToList());
            }
            else if (RoleManager.IsCurrentSpecialOptionUser())
            {
                var allowedFhas = GetAllowedFhasForUser(UserPrincipal.Current.UserId);
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(null, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, allowedFhas.ToList());
            }
            else if (RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName))
            {
                var allowedFhas = GetAllowedFhasForUser(UserPrincipal.Current.UserId);
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(null, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, allowedFhas.ToList());
            }
            else
            {
                resultsRaw = dataUploadIntermediateRepository.GetUploadedDataWithPageSort(iLenderId, strSortBy,
                    sortOrder, pageSize, pageNum, commentViewRepository, null);
            }
            var results = PaginateSortModel<DerivedUploadData>.CreateInstance();
            results.TotalRows = resultsRaw.TotalRows;
            var entities = new List<DerivedUploadData>();

            if (resultsRaw.Entities.Any())
            {
                resultsRaw.Entities.ToList().ForEach(p =>
                    entities.Add(
                        new DerivedUploadData()
                        {
                            DataSource = DBSource.Intermediate,
                            LDI_ID = p.LDI_ID,
                            DataInserted = p.DataInserted,
                            ProjectName = p.ProjectName,
                            ServiceName = p.ServiceName,
                            PeriodEnding = p.PeriodEnding,
                            PeriodEnding_Disp = p.PeriodEnding_Disp,
                            ActualNumberOfResidentDays = p.ActualNumberOfResidentDays,
                            //UnitsInFacility = p.UnitsInFacility,
                            MonthsInPeriod = p.MonthsInPeriod,
                            MonthsInPeriod_Disp = p.MonthsInPeriod_Disp,

                            FHANumber = p.FHANumber,
                            TotalRevenues = p.TotalRevenues,
                            TotalExpenses = p.TotalExpenses,
                            MortgageInsurancePremium = p.MortgageInsurancePremium,
                            FHAInsuredPrincipalInterestPayment = p.FHAInsuredPrincipalInterestPayment,
                            DebtCoverageRatio2 = p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.DebtCoverageRatio]
                                    : p.DebtCoverageRatio2,
                            NOIRatio = p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.NOI]
                                    : p.NOIRatio,
                            AverageDailyRateRatio = p.HasCalculated == false
                                    ? p.DerivedFinDict[DerivedFinancial.AverageDailyRate]
                                    : p.AverageDailyRateRatio,

                            HasComment = p.HasComment
                        }));
            }
            results.Entities = entities;
            results.PageSize = resultsRaw.PageSize;
            return results;
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByTime(DateTime myTimeInserted)
        {
            //return dataUploadIntermediateRepository.GetUploadedDataByTime(TimezoneManager.GetUtcTimeFromPreferred(myTimeInserted));
            return dataUploadIntermediateRepository.GetUploadedDataByTime(myTimeInserted);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByTimeForSearch(DateTime myTimeInserted)
        {
            //return dataUploadIntermediateRepository.GetUploadedDataByTimeForSearch(TimezoneManager.GetUtcTimeFromPreferred(myTimeInserted), commentViewRepository);
            return dataUploadIntermediateRepository.GetUploadedDataByTimeForSearch(myTimeInserted, commentViewRepository);
        }

        public PaginateSortModel<DerivedUploadData> GetUploadedDataByTimePageSort(DateTime myTimeInserted, int userId,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum)
        {
            //return dataUploadIntermediateRepository.GetUploadedDataByTimePageSort(TimezoneManager.GetUtcTimeFromPreferred(myTimeInserted), userId,
            //    strSortBy, sortOrder, pageSize, pageNum, commentViewRepository);
            return dataUploadIntermediateRepository.GetUploadedDataByTimePageSort(myTimeInserted, userId,
               strSortBy, sortOrder, pageSize, pageNum, commentViewRepository);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByCategory(string category)
        {
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            //return dataUploadIntermediateRepository.GetUploadedDataByScoreRange(minVal, maxVal);
            return dataUploadLiveRepository.GetUploadedDataByScoreRange(minVal, maxVal);
        }

        public PaginateSortModel<DerivedUploadData> GetUploadDataByCategoryForProjectReport(string category)
        {
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            //return dataUploadIntermediateRepository.GetUploadedDataByScoreRange(minVal, maxVal);
            return dataUploadLiveRepository.GetUploadedDataByScoreRangeForProjectReport(minVal, maxVal);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByCategoryAndQuarter(string category, string quarter)
        {
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            //return dataUploadIntermediateRepository.GetUploadedDataByScoreRange(minVal, maxVal);
            var result = quarter.Split(new[] { '/' });
            return dataUploadLiveRepository.GetUploadedDataByScoreRangeAndQuarter(minVal, maxVal, int.Parse(result[0]), int.Parse(result[1]), lenderFhaRepository);
        }

        public IEnumerable<DerivedUploadData> GetUploadDataByCategoryAndQuarterBySearch(string category, string quarter)
        {
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            //return dataUploadIntermediateRepository.GetUploadedDataByScoreRange(minVal, maxVal);
            var result = quarter.Split(new[] { '/' });
            return dataUploadLiveRepository.GetUploadedDataByScoreRangeAndQuarterBySearch(minVal, maxVal, commentRepository, int.Parse(result[0]), int.Parse(result[1]));
        }

        public PaginateSortModel<DerivedUploadData> GetUploadLiveDataByCategoryPageSort(string category, string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, int monthsInPeriod, int year)
        {

            int minVal = Int32.MinValue;
            int maxVal = Int32.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }

            var result = dataUploadLiveRepository.GetUploadedDataByScoreRangeWithPageSort(minVal, maxVal,
                strSortBy, sortOrder, pageSize, pageNum, commentRepository, lenderFhaRepository, monthsInPeriod, year);
            IQueryable<DerivedUploadData> query = result.Entities.AsQueryable();
            return PaginateSort.SortAndPaginate(query, strSortBy, sortOrder, pageSize, pageNum);
        }

        public PaginateSortModel<DerivedUploadData> GetUploadLiveDataByCategoryPageSort(string category, string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, string quarter)
        {
            DateTime selectedQtr = Convert.ToDateTime(quarter);
            int quarterNumber = (selectedQtr.Month - 1) / 3 + 1;
            decimal minVal = decimal.MinValue;
            decimal maxVal = decimal.MaxValue;
            // to do: change to enum
            switch (category)
            {
                case "Low Risk":
                    minVal = 0;
                    maxVal = 10;
                    break;
                case "Medium Risk":
                    minVal = 10;
                    maxVal = 20;
                    break;
                case "High Risk":
                    minVal = 20;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("{0} is out of expected range", category));
            }
            var result = dataUploadLiveRepository.GetUploadedDataByScoreRangeWithPageSort(minVal, maxVal,
                strSortBy, sortOrder, pageSize, pageNum, commentRepository, selectedQtr);
            return result;
        }

        public void CalcAndUpdateUploadedData()
        {
            //System.Diagnostics.Debug.WriteLine(string.Format("Start load uncalculated intermediate at: {0}", DateTime.Now.ToLongTimeString()));
            System.Diagnostics.Debug.WriteLine(string.Format("Start load uncalculated intermediate at: {0}", DateTime.UtcNow.ToLongTimeString()));
            var uncalculated = dataUploadIntermediateRepository.GetUnCalculated();
            //System.Diagnostics.Debug.WriteLine(string.Format("End load uncalculated intermediate at: {0}", DateTime.Now.ToLongTimeString()));
            System.Diagnostics.Debug.WriteLine(string.Format("End load uncalculated intermediate at: {0}", DateTime.UtcNow.ToLongTimeString()));

            // you can apply different set of score algorithm with new IScoreManager and IScoreAgent
            // ScoreAgentV2, effective 09/04/2014
            // e.g. scoreManager = new ScoreManagerV1(new ScoreAgentV1)
            //scoreManager = new ScoreManager(new ScoreAgent());

            scoreManager = new ScoreManagerV3(new ScoreAgent());
            List<ExcelUploadView_Model> dataToUpdate = new List<Model.ExcelUploadView_Model>();
            //System.Diagnostics.Debug.WriteLine(string.Format("Start to calculate ratio loop at: {0}", DateTime.Now.ToLongTimeString()));
            System.Diagnostics.Debug.WriteLine(string.Format("Start to calculate ratio loop at: {0}", DateTime.UtcNow.ToLongTimeString()));
            foreach (var item in uncalculated)
            {
                scoreManager.GetScores(item);
                item.DebtCoverageRatio2 = item.DerivedFinDict[DerivedFinancial.DebtCoverageRatio2];
                item.AverageDailyRateRatio = item.DerivedFinDict[DerivedFinancial.AverageDailyRate];
                item.NOIRatio = item.DerivedFinDict[DerivedFinancial.NOI];
                item.HasCalculated = true;
                dataToUpdate.Add(item);
            }
            //System.Diagnostics.Debug.WriteLine(string.Format("End calculate ratio loop at: {0}", DateTime.Now.ToLongTimeString()));
            System.Diagnostics.Debug.WriteLine(string.Format("End calculate ratio loop at: {0}", DateTime.UtcNow.ToLongTimeString()));
            dataUploadIntermediateRepository.UpdateData(dataToUpdate);
            unitOfWorkIntermediate.Save();
            //System.Diagnostics.Debug.WriteLine(string.Format("End save calculated intermediate at: {0}", DateTime.Now.ToLongTimeString()));
            System.Diagnostics.Debug.WriteLine(string.Format("End save calculated intermediate at: {0}", DateTime.UtcNow.ToLongTimeString()));
        }

        public IEnumerable<ExcelUploadView_Model> GetUploadedDataByQuarter(int monthsInPeriod, int year)
        {
            var uploadedData = dataUploadLiveRepository.GetUploadedData(monthsInPeriod, year);
            uploadedData = uploadedData.ToList();//.Where((p => int.Parse(p.MonthsInPeriod) == monthsInPeriod && Convert.ToDateTime(p.PeriodEnding).Date.Year == year));
            return uploadedData;
        }

        public IEnumerable<UploadStatusViewModel> GetUploadStatusByLenderId(int lenderId)
        {
            return uploadStatusRepository.GetUploadStatusByLenderId(lenderId);
        }

        public IEnumerable<UploadStatusDetailModel> GetUploadStatusDetailByLenderId(int lenderId, string selectedQtr)
        {
            if (!string.IsNullOrEmpty(selectedQtr))
            {
                var result = selectedQtr.Split(new[] { '/' });
                var username = UserPrincipal.Current.UserName;
                var usertype = GetUsertypeBasedOnRole(username);
                return uploadStatusDetailRepository.GetUploadStatusDetailByLenderId(username, usertype,
                    lenderId, int.Parse(result[0]), int.Parse(result[1]));
            }
            return null;
        }

        private static string GetUsertypeBasedOnRole(string username)
        {
            if (RoleManager.IsSuperUserRole(username))
            {
                return EnumType.EnumToValue(EnumType.Parse<HUDRole>("1"));
            }
            if (RoleManager.IsWorkloadManagerRole(username))
            {
                return EnumType.EnumToValue(EnumType.Parse<HUDRole>("5"));
            }
            if (RoleManager.IsAccountExecutiveRole(username))
            {
                return EnumType.EnumToValue(EnumType.Parse<HUDRole>("4"));
            }
            if (RoleManager.IsInternalSpecialOptionUser(username))
            {
                return EnumType.EnumToValue(EnumType.Parse<HUDRole>("12"));
            }
            if (RoleManager.IsCurrentSpecialOptionUser())
            {
                return EnumType.EnumToValue(EnumType.Parse<HUDRole>("9"));
            }
            return EnumType.EnumToValue(EnumType.Parse<HUDRole>("0")); ;
        }

        public string GetErrorMsgForEmptyFields(int rowNumber, string columnName)
        {
            string errorMsg = "Check the value for '" + columnName + "' at row# " + rowNumber + ".";
            return errorMsg;
        }

        public string GetErrorMsgForInvalidFields(int rowNumber, string columnName, string dataType)
        {
            string errorMsg = "The '" + columnName + "' column accepts only  " + dataType + ".  Please correct the upload file and repeat the upload process.  Row#: " + rowNumber + ".";
            return errorMsg;
        }

        public List<string> GetCurrentQuarters()
        {
            var quarters = new List<string>();
            var latestQtr = Convert.ToDateTime(DateHelper.GetCurrentQuarter());
            var month = latestQtr.Month;
            var year = latestQtr.Year;
            var q1 = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("1"));
            var q2 = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("2"));
            var q3 = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("3"));
            var q4 = EnumType.GetEnumDescription(EnumType.Parse<FinancialQuarter>("4"));
            if (month <= 3)
            {
                quarters.Add(q4 + "/" + Convert.ToString(year - 1));
                quarters.Add(q3 + "/" + Convert.ToString(year - 1));
                quarters.Add(q2 + "/" + Convert.ToString(year - 1));
                quarters.Add(q1 + "/" + Convert.ToString(year - 1));
            }
            else if (month <= 6)
            {
                quarters.Add(q1 + "/" + Convert.ToString(year));
                quarters.Add(q4 + "/" + Convert.ToString(year - 1));
                quarters.Add(q3 + "/" + Convert.ToString(year - 1));
                quarters.Add(q2 + "/" + Convert.ToString(year - 1));
            }
            else if (month <= 9)
            {
                quarters.Add(q2 + "/" + Convert.ToString(year));
                quarters.Add(q1 + "/" + Convert.ToString(year));
                quarters.Add(q4 + "/" + Convert.ToString(year - 1));
                quarters.Add(q3 + "/" + Convert.ToString(year - 1));
            }
            else if (month <= 12)
            {
                quarters.Add(q3 + "/" + Convert.ToString(year));
                quarters.Add(q2 + "/" + Convert.ToString(year));
                quarters.Add(q1 + "/" + Convert.ToString(year));
                quarters.Add(q4 + "/" + Convert.ToString(year - 1));
            }

            return quarters;

        }

        public IEnumerable<UploadStatusViewModel> GetUploadStatusDetailByQuarter(string quarter, int lenderId)
        {
            var username = UserPrincipal.Current.UserName;
            var usertype = GetUsertypeBasedOnRole(username);

            if (quarter != null)
            {
                var result = quarter.Split(new[] { '/' });
                var uploadStatusReportResults = uploadStatusRepository.GetUploadStatusDetailByQuarter(username, usertype, lenderId,
                    int.Parse(result[0]),
                    int.Parse(result[1]));
                if (uploadStatusReportResults.Any())
                {
                    return uploadStatusReportResults;
                }
                return uploadStatusRepository.GetUploadStatusDetailByQuarter(username, usertype, lenderId, int.Parse(result[0]),
                    int.Parse(result[1])).OrderBy(p => p.LenderName);
            }
            return uploadStatusRepository.GetUploadStatusDetailByQuarter(username, usertype, lenderId, 0, 0).OrderBy(p => p.LenderName);
        }

        public IEnumerable<FhaInfoModel> GetFhasByLenderIds(string lenderIds)
        {
            return fhaRepository.GetFhasByLenderIds(lenderIds);
        }

        public void UpdateLenderPropertyInfo(int userId, int lenderId)
        {
            dataUploadLiveRepository.UpdateLenderPropertyInfo(userId, lenderId);
        }

        public int SaveFormUploadToIntermediateTbl(FormUploadModel model)
        {
            int ldiID = dataUploadIntermediateRepository.SaveFormUploadToIntermediateTbl(model);
            unitOfWorkIntermediate.Save();
            return ldiID;
        }

        public IEnumerable<ReportViewModel> GetMissingProjectsForLender(int lenderId, string selectedQtr)
        {
            if (!string.IsNullOrEmpty(selectedQtr))
            {
                var result = selectedQtr.Split(new[] { '/' });
                var username = UserPrincipal.Current.UserName;
                var usertype = GetUsertypeBasedOnRole(username);
                return dataUploadIntermediateRepository.GetMissingProjectsForLender(username, usertype, lenderId, int.Parse(result[0]), int.Parse(result[1]));
            }
            return null;
        }

        public IDictionary<string, string> GetQuartersList()
        {
            Tuple<DateTime, int> tuple;
            var quarters = new Dictionary<string, string>();
            if (RoleManager.IsCurrentUserLenderRoles())
            {
                var iLenderId = UserPrincipal.Current.LenderId;
                if (!iLenderId.HasValue)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "User id: {0} is a lender, but does not have corresponding lender id in UserLender table",
                            UserPrincipal.Current.UserId));
                }
                tuple = dataUploadIntermediateRepository.GetLatestPeriodEnding(iLenderId);
            }
            else
            {
                tuple = dataUploadIntermediateRepository.GetLatestPeriodEnding(null);
            }
            if (tuple == null) return quarters;
            var year = tuple.Item1.Year;
            var monthsInPeriod = tuple.Item2;

            if (monthsInPeriod == 3)
            {
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("12")), year - 1), "12/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("9")), year - 1), "9/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("6")), year - 1), "6/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("3")), year - 1), "3/" + (year - 1));

            }
            else if (monthsInPeriod == 6)
            {
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("3")), year), "3/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("12")), year - 1), "12/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("9")), year - 1), "9/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("6")), year - 1), "6/" + (year - 1));
            }
            else if (monthsInPeriod == 9)
            {
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("6")), year), "6/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("3")), year), "3/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("12")), year - 1), "12/" + (year - 1));
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("9")), year - 1), "9/" + (year - 1));
            }
            else if (monthsInPeriod == 12)
            {
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("9")), year), "9/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("6")), year), "6/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("3")), year), "3/" + year);
                quarters.Add(string.Format(CultureInfo.InvariantCulture, EnumType.GetEnumDescription(EnumType.Parse<ReportingPeriod>("12")), year - 1), "12/" + (year - 1));
            }

            return quarters;
        }

        public string GetLatestQuarter(string type)
        {
            Tuple<DateTime, int> tuple;
            if (RoleManager.IsCurrentUserLenderRoles())
            {
                var iLenderId = UserPrincipal.Current.LenderId;
                if (!iLenderId.HasValue)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "User id: {0} is a lender, but does not have corresponding lender id in UserLender table",
                            UserPrincipal.Current.UserId));
                }
                tuple = dataUploadIntermediateRepository.GetLatestPeriodEnding(iLenderId);
            }
            else
            {
                tuple = dataUploadIntermediateRepository.GetLatestPeriodEnding(null);
            }
            if (tuple != null)
            {
                var year = tuple.Item1.Year;
                var monthsInPeriod = tuple.Item2;
                var quarter = Enum.GetName(typeof(ReportingPeriod), monthsInPeriod);
                if (string.Equals(type, "key"))
                {
                    return quarter + " " + year;
                }
                if (string.Equals(type, "value"))
                {
                    return monthsInPeriod + "/" + year;
                }
            }
            return null;
        }

        public bool IsValidLender(int lenderId)
        {
            return lenderInfoRepository.GetLenderDetail(lenderId).Count() > 0 ? true : false;
        }

        public IEnumerable<ProjectInfoViewModel> GetProjectInfoReport(string username)
        {
            return dataUploadIntermediateRepository.GetProjectInfoReport(username);
        }

        public int DistributeQuarterly()
        {
            return dataUploadIntermediateRepository.DistributeQuarterly();

        }

        public int ApplyCalculations(string QryDate = "4/21/2016")
        {
            return dataUploadIntermediateRepository.ApplyCalculations(QryDate);
        }

        public int CalculateErrors()
        {
            return dataUploadIntermediateRepository.CalculateErrors();

        }

        public IEnumerable<int> GetLenderOperatorsbyFHA(string fhanumber)
        {
            return userLenderServicerRepository.GetLenderOperatorsbyFHA(fhanumber);
        }

        public IEnumerable<string> GetAllowedFhasForUsers(List<int> UserIds)
        {
            return userLenderServicerRepository.GetAllowedFhasForUsers(UserIds);
        }

        public IEnumerable<KeyValuePair<int, string>> GetPeriodEnding(string QryDate = "4/21/2016")
        {
            return dataUploadIntermediateRepository.GetPeriodEnding(QryDate);
        }

        public List<string> GetExistingFHAs(List<string> uploadItems)
        {
            return dataUploadIntermediateRepository.GetExistingFHAs(uploadItems);
        }

        public Boolean CheckLenderFiscalYear(long lenderId, string fha, DateTime periodEnding, int monthsInPeriod,
            out bool existInSpreadSheet, out bool existInIntermidiate, string queryDate)
        {
            existInSpreadSheet = false;
            existInIntermidiate = false;
            Boolean isRegularFY = false;
            Tuple<string, int> t;
            DateTime date;
            int months;
            Dictionary<int, int> dic = null;
            //1 check LenderFiscalYearDetails 
            int? endingMonth = lenderFiscalYearDetailsRepository.GetFiscalYearEndingMonth((int)lenderId, fha);
            if (endingMonth == 12)
            {
                isRegularFY = true;
                existInSpreadSheet = true;
            }
            else
            {
                isRegularFY = false;
                if (endingMonth == null || endingMonth == 0) //didn't find in spreadsheet
                {
                    //2 check Intermediate 
                    t = dataUploadIntermediateRepository.GetLatestPeriodEnding((int)lenderId, fha, queryDate);
                    if (t != null)
                    {
                        existInIntermidiate = true;
                        date = Convert.ToDateTime(t.Item1);
                        months = t.Item2;
                        dic = GetQuartersFromDate(date, months);
                        if (date.Month == months) isRegularFY = true;
                        else isRegularFY = false;
                    }
                    else
                    {
                        return true; //didn't find in both spreadsheet or imtermediate table
                    }
                }
                else //find in spreadsheet
                {
                    existInSpreadSheet = true;
                    DateTime today = DateTime.Today;
                    DateTime endOfMonth = new DateTime(today.Year, (int)endingMonth,
                        DateTime.DaysInMonth(today.Year, (int)endingMonth));
                    dic = GetQuartersFromDate(endOfMonth, 12);
                }
            }
            if (isRegularFY)
            {
                if (periodEnding.Month == monthsInPeriod) return true;
                else return false;
            }
            else
            {
                if (dic != null && dic.ContainsKey(periodEnding.Month) && dic[periodEnding.Month] == monthsInPeriod)
                    return true;
                else return false;
            }
        }

        Dictionary<int, int> GetQuartersFromDate(DateTime dt, int monthsInPeriod)
        {
            Dictionary<int, int> dic = new Dictionary<int, int>();
            DateTime dt1, dt2, dt3;
            if (monthsInPeriod == 3)
            {
                dt1 = dt.AddMonths(3);
                dt2 = dt.AddMonths(6);
                dt3 = dt.AddMonths(9);
                dic.Add(dt.Month, 3);
                dic.Add(dt1.Month, 6);
                dic.Add(dt2.Month, 9);
                dic.Add(dt3.Month, 12);
            }
            else if (monthsInPeriod == 6)
            {
                dt1 = dt.AddMonths(-3);
                dt2 = dt.AddMonths(3);
                dt3 = dt.AddMonths(6);
                dic.Add(dt1.Month, 3);
                dic.Add(dt.Month, 6);
                dic.Add(dt2.Month, 9);
                dic.Add(dt3.Month, 12);
            }
            else if (monthsInPeriod == 9)
            {
                dt1 = dt.AddMonths(-6);
                dt2 = dt.AddMonths(-3);
                dt3 = dt.AddMonths(3);
                dic.Add(dt1.Month, 3);
                dic.Add(dt2.Month, 6);
                dic.Add(dt.Month, 9);
                dic.Add(dt3.Month, 12);
            }
            else if (monthsInPeriod == 12)
            {
                dt1 = dt.AddMonths(-9);
                dt2 = dt.AddMonths(-6);
                dt3 = dt.AddMonths(-3);
                dic.Add(dt1.Month, 3);
                dic.Add(dt2.Month, 6);
                dic.Add(dt3.Month, 9);
                dic.Add(dt.Month, 12);
            }
            return dic;
        }

        public IEnumerable<FhaInfoModel> GetReadyFhasForInspectionContracotr()
        {
            return fhaRepository.GetReadyFhasForInspectionContracotr();
        }

        public IEnumerable<FhaInfoModel> GetSelectedFhasForInspector(int userId)
        {
            return userLenderServicerRepository.GetSelectedFhasForInspector(userId);
        }
        //written by siddesh  29082019 #344//
        public IEnumerable<usp_HCP_GetTicketDetails_Latest> GetUploadStatusDetailByhelpdeskreport(string Status, string FromDate, string ToDate, string HelpDeskName)
        {
            var username = UserPrincipal.Current.UserName;
            var usertype = GetUsertypeBasedOnRole(username);
            var uploadStatusReportResults = UploadStatusHelpdeskreportsRepository.GetUploadStatusDetailByhelpdeskreport(Status, FromDate, ToDate, HelpDeskName);
            if (uploadStatusReportResults.Any())
            {
                return uploadStatusReportResults;
            }
            return UploadStatusHelpdeskreportsRepository.GetUploadStatusDetailByhelpdeskreport(Status, FromDate, ToDate, HelpDeskName);

        }

        //harish added 04-12-2019 #584
        public LenderFiscalYearDetails lenderrowdata(long lenderid, string fha)
        {
            return lenderFiscalYearDetailsRepository.getlenderrowdata((int)lenderid, fha);
        }
        //#584
        public int CreateLenderFisicalyear(LenderFiscalYearDetails model)
        {
            return lenderFiscalYearDetailsRepository.save(model);
        }
        // harish added new method for update #584
        public int UpdateLenderFisicalyear(LenderFiscalYearDetails model)
        {
            return lenderFiscalYearDetailsRepository.Updateyear(model);
        }


    }


}
