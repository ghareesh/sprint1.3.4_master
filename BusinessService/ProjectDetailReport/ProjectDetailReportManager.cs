﻿using BusinessService.Interfaces;
using Core;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.ProjectDetailReport;

namespace BusinessService.ProjectDetailReport
{
    public class ProjectDetailReportManager:IProjectDetailReportManager
    {
     
         private ProjectDetailReportRepository _projectDetailReportRepository;

         public ProjectDetailReportManager()
        {
            _projectDetailReportRepository = new ProjectDetailReportRepository();
        }


         public ReportModel GetProjectDetailReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetProjectDetailReportForSuperUser(userName, userType, QryDate);
        }

         public ReportModel GetProjectDetailReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetProjectDetailReportForWorkloadManager(userName, userType, QryDate);
        }

         public ReportModel GetProjectDetailReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetProjectDetailReportForAccountExecutive(userName, userType, QryDate);
        }

        public ReportModel GetProjectDetailReportForSuperUserHighLevel(string userName, string userType, string SortOrder = "ASC",string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetProjectDetailReportForSuperUserHighLevel(userName, userType, SortOrder, QryDate);
        }


        public ReportModel GetErrorReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetErrorReportForSuperUser(userName, userType, QryDate);
        }

        public ReportModel GetErrorReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetErrorReportForSuperUser(userName, userType, QryDate);
        }

        public ReportModel GetErrorReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetErrorReportForSuperUser(userName, userType, QryDate);
        }


        public ReportModel GetPTReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetPTReportForSuperUser(userName, userType, QryDate);
        }

        public ReportModel GetPTReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetPTReportForWorkloadManager(userName, userType, QryDate);
        }

        public ReportModel GetPTReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetPTReportForAccountExecutive(userName, userType, QryDate);
        }

        public ReportModel GetCurrentQuarterReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetCurrentQuarterReportForAccountExecutive(userName, userType, QryDate);
        }
 //InternalSpecialOptionUser

        public ReportModel GetProjectDetailReportForInternalSpecialOptionUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetProjectDetailReportForAccountExecutive(userName, userType, QryDate);
        }

        public ReportModel GetErrorReportForInternalSpecialOptionUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetErrorReportForSuperUser(userName, userType, QryDate);
        }

        public ReportModel GetPTReportForInternalSpecialOptionUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetPTReportForAccountExecutive(userName, userType, QryDate);
        }

        public ReportModel GetCurrentQuarterReportForInternalSpecialOptionUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            return _projectDetailReportRepository.GetCurrentQuarterReportForAccountExecutive(userName, userType, QryDate);
        }
    }
}
