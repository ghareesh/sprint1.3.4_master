﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository;
using Repository.Interfaces;

namespace BusinessService
{
    public class PDRUserPreferenceManager : IPDRUserPreferenceManager
    {
        private IPDRColumnUserPreferenceRepository pdrColumnUserPreferenceRepository;
        private IPDRColumnRepository pdrColumnRepository;
        private UnitOfWork unitOfWork;

        public PDRUserPreferenceManager()
        {
            unitOfWork = new UnitOfWork(DBSource.Live);
            pdrColumnRepository = new PDRColumnRepository(unitOfWork);
            pdrColumnUserPreferenceRepository = new PDRColumnUserPreferenceRepository(unitOfWork);
        }
        public int AddPDRUserPreference(Model.PDRUserPreferenceViewModel model)
        {
          return  pdrColumnUserPreferenceRepository.AddNewColumn(model);
        }

        public void UpdatePDRUserPreference(Model.PDRUserPreferenceViewModel model)
        {
            pdrColumnUserPreferenceRepository.UpdatePDRUserPreference(model);
        }

        public IEnumerable<Model.PDRUserPreferenceViewModel> GetUserPreference(int userID)
        {
           return pdrColumnUserPreferenceRepository.GetUserPreferenceByUser(userID);
        }

        public IEnumerable<PDRColumnViewModel> GetAllColumns()
        {
            return pdrColumnRepository.GetAllPdrColumns();
        }

        public void DeletePDRUserPreference(int userID, int pdrColumnID)
        {
            pdrColumnUserPreferenceRepository.DeletePDRUserPreference(userID,pdrColumnID);
        }

        public void SetDefaultUserPreference(int userID)
        {
            pdrColumnUserPreferenceRepository.SetDefaultUserPreference(userID);
            /**
             *  These are list of default columns for Project Detail Report Columns.
                12 Servicer Name 
                13	Workload Manager
                14 Account Executive
                17 Qtrly NOI
                19	NOI Percentage Change
                20 Qtrly Rev
                21 Qtrly Operating Exp
                23 Rev Percentage Change
                25 Qrtly DSCR
                27 DSCR Percentage Change
             */
            var defaultColumns = new int[] { 12, 13, 14, 17,19,20,21,23,25,27};
            foreach (var pdrColumn in GetAllColumns().Where(m=>!defaultColumns.Contains(m.ColumnID)))
            {
                var model = new PDRUserPreferenceViewModel()
                {
                    PDRColumnID = pdrColumn.ColumnID,
                    UserID = userID,
                    Visibility = false

                };
                AddPDRUserPreference(model);

            }
        }
    }
}
