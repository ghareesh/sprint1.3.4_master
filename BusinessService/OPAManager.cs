﻿using BusinessService.Interfaces;
using Repository.Interfaces.OPAForm;
using Repository.OPAForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Repository.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.BusinessService
{
    public class OPAManager : IOPAManager
        
    {
        
        private UnitOfWork unitOfWorkLive;
        private UnitOfWork unitOfWorkTask;
        private IOPARepository oPARepository;
        private IOPARepository OPARepo;

        // #605
        private IOPARepository OPAInternalwork;

        public OPAManager()
        {
            
            unitOfWorkLive = new UnitOfWork(DBSource.Live);      
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
            oPARepository = new OPARepository(unitOfWorkTask);
            //Naveen hareesha 19-11-2019
            OPARepo = new OPARepository(unitOfWorkLive);
            //#605
            OPAInternalwork = new OPARepository(unitOfWorkTask);
        }

        public List<Model.OPAWorkProductModel> getAllOPAWorkProducts(Guid taskInstanceId, string fileType)
        {
            var opaList = oPARepository.getAllOPAWorkProducts(taskInstanceId, fileType);
            if (opaList!= null && opaList.Count() > 0)
            {
                return opaList.OrderByDescending(p => p.UploadTime).ToList();//<OPAWorkProductModel>();
            }
            return null;
            //throw new NotImplementedException();
        }

        //Naveen Hareesh 19-11-2019
        public void UpdateOPAGroupTask1(OPAViewModel OPAActionViewModel)
        {
            OPARepo.UpdateOPAGroupTask(OPAActionViewModel);
        }

        //#605Added by siddu 23122019 for internal workproduct//
        public List<Model.OPAWorkProductModel> getAllOPAWorkInternalProducts(Guid taskInstanceId)
        {
            var opaList = OPAInternalwork.getAllOPAWorkInternalProducts(taskInstanceId);
            if (opaList != null && opaList.Count() > 0)
            {
                return opaList.OrderByDescending(p => p.UploadTime).ToList();//<OPAWorkProductModel>();
            }
            return null;
            //throw new NotImplementedException();
        }
        // harish added new method to get RIS upload files 07-01-2020
        public List<Model.OPAWorkProductModel> RISgetAllOPAWorkInternalProducts(Guid taskInstanceId)
        {
            var opaList = OPAInternalwork.RISgetAllOPAWorkInternalProducts(taskInstanceId);
            if (opaList != null && opaList.Count() > 0)
            {
                return opaList.OrderByDescending(p => p.UploadTime).ToList();//<OPAWorkProductModel>();
            }
            return null;
            //throw new NotImplementedException();
        }
    }
}
