﻿
using System;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Repository.Interfaces;

namespace HUDHealthcarePortal.BusinessService
{
    public class ParentChildTaskManager:IParentChildTaskManager
    {
        private IParentChildTaskRepository parentChildTaskRepository;
        private UnitOfWork unitOfWorkTask;

        public ParentChildTaskManager()
        {
            parentChildTaskRepository = new ParentChildTaskRepository();
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
        }

        public void AddParentChildTask(ParentChildTaskModel model)
        {
            parentChildTaskRepository.AddParentChildTask(model);
            unitOfWorkTask.Save();
        }

        public bool IsParentTaskAvailable(Guid childTaskInstanceId)
        {
            return parentChildTaskRepository.IsParentTaskAvailable(childTaskInstanceId);
        }


        public ParentChildTaskModel GetParentTask(Guid childTaskInstanceId)
        {
            return parentChildTaskRepository.GetParentTask(childTaskInstanceId);
        }

        //#664 harish added new line of code for RAI
        public ParentChildTaskModel GetChildTaskbytaskinstanceid(Guid childTaskInstanceId)
        {
            return parentChildTaskRepository.GetRAIParentTask(childTaskInstanceId);
        }
    }
}
