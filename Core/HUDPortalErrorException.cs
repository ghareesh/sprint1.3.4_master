﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace HUDHealthcarePortal.Core
{
    [Serializable]
    public class HUDPortalErrorException : Exception
    {
        public static string HUDUserContext
        {
            get
            {
                if (UserPrincipal.Current.UserData == null)
                    return string.Format(CultureInfo.InvariantCulture, "UserName: {0}, UserId: {1}", "Anonymous", -1);
                return string.Format(CultureInfo.InvariantCulture, "UserName: {0}, UserId: {1}", UserPrincipal.Current.UserName, UserPrincipal.Current.UserId);
            }
        }

        public HUDPortalErrorException()
        {
        }

        public HUDPortalErrorException(string message)
            : base(message)
        {
        }

        public HUDPortalErrorException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected HUDPortalErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            //HUDUserContext = info.GetString("HUDUserContext");
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            info.AddValue("HUDUserContext", HUDUserContext);
            base.GetObjectData(info, context);
        }

    }
}
