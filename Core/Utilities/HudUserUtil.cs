﻿using HUDHealthcarePortal.Core;

namespace Core.Utilities
{
    public static class HudUserUtil
    {
        public static int GetHudUserId
        {
            get
            {
                return UserPrincipal.Current.UserId;
            }
        }

        public static void ReassignRoleNameForDisplay(string[] roles)
        {
            for (int i = 0; i < roles.Length; i++)
            {
                switch (roles[i])
                {
                    case "AccountExecutive":
                        roles[i] = "Account Executive";
                        break;
                    case "BackupAccountManager":
                        roles[i] = "Backup Account Manager";
                        break;
                    case "HUDAdmin":
                        roles[i] = "HUD Administrator";
                        break;
                    case "HUDDirector":
                        roles[i] = "HUD Director";
                        break;
                    case "LenderAccountManager":
                        roles[i] = "Lender Account Manager";
                        break;
                    case "LenderAccountRepresentative":
                        roles[i] = "Lender Account Representative";
                        break;
                    case "LenderAdmin":
                        roles[i] = "Lender Administrator";
                        break;
                    case "OperatorAccountRepresentative":
                        roles[i] = "Operator Account Representative";
                        break;
                    case "SuperUser":
                        roles[i] = "Super User";
                        break;
                    case "WorkflowManager":
                        roles[i] = "Workload Manager";
                        break;
                    case "Servicer":
                        roles[i] = "Special Option User";
                        break;
                }
            }
        }
    }
}
