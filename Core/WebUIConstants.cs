﻿namespace Core
{
    public static class WebUiConstants
    {
        //karri;Hareesh;Naveen:#121: donno why this HCP_Intermediate value is not loading in
        //public partial class HCP_intermediate : DbContext
        //hence have to overide the value in public partial class HCP_intermediate : DbContext 
        //with base("HCP_intermediate")
        //karri;
        //the names of connection strings in web.config should match here below declared string values.
        //these are used to connect to DB directly without referencing the Entity Framework Context
        public const string IntermediateConnectionName = "HCP_Intermediate";
        public const string LiveConnectionName = "HCP_Live";
        public const string Comma = ",";
        public const bool ShowPrintIcon = false;
        //karrinew
        public const string TaskConnectionName = "HCP_Task";
        public const string ElmahConnectionName = "Elmah.Sql";
        public const string Excel03ConString = "Excel03ConString";
        public const string Excel07ConString = "Excel07ConString";
    }
}