﻿using System;
using Core.ExceptionHandling;

namespace HUDHealthcarePortal.Core.ExceptionHandling
{
    public class HUDPortalException : ExceptionBase
    {
        public HUDPortalException(ExceptionCategory category, string message)
            : base(category, message)
        {
        }

        public HUDPortalException(ExceptionCategory category, Exception innerException, string message)
            : base(category, innerException, message)
        {
        }

        public HUDPortalException(ExceptionCategory category, Exception innerException, string format, params object[] values)
            : base(category, innerException, string.Format(format, values))
        {
        }
    }
}