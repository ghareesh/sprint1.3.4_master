﻿using System;
using Core.Interfaces;

namespace HUDHealthcareUnitTest
{
    public class CommentsIdentity : IUserPrincipal
    {
        public int? LenderId
        {
            get { throw new NotImplementedException(); }
        }

        public int? ServicerId
        {
            get { throw new NotImplementedException(); }
        }

        public string Timezone
        {
            get { throw new NotImplementedException(); }
        }

        public int UserId
        {
            get { throw new NotImplementedException(); }
        }

        public string UserName
        {
            get { return "Admin"; }
        }

        public string UserRole
        {
            get { return "Administrator"; }
        }
    }
}
