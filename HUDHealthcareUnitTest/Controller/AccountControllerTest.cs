﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using HUDHealthCarePortal.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BusinessService.ProjectAction;
using BusinessService.Interfaces.UserLogin;

namespace HUDHealthcareUnitTest.Controller
{
    [TestClass]
    public class AccountControllerTest
    {
   //     private Mock<IAccountManager> accountMgr;

   //     [TestInitialize]
   //     public void Initialize()
   //     {
   //         accountMgr = new Mock<IAccountManager>();
   //         AutoMapperInitialize.Initialize();
   //     }

   //     [TestMethod]
   //     public void Register_Valid_User_Should_Redirect()
   //     {
   //         // Arrange
   //         var user = new UserViewModel();
   //         accountMgr.Setup(p => p.CreateUser(user)).Returns("user token");
   //         var controller = new AccountController(accountMgr.Object, new Mock<ILookupManager>().Object, new Mock<IWebSecurityWrapper>().Object, new Mock<IQuestionManager>().Object, new Mock<IProjectActionManager>().Object, new Mock<IUserLoginManager>().Object, new Mock<IReviewerTitlesManager>().Object, new Mock<ITaskReAssignmentManager>().Object, new Mock<IProjectActionFormManager>().Object);

   //         // Act
   //         var result = (RedirectToRouteResult)controller.Register(user);

   //         // Assert
   //         Assert.AreEqual("ListUsers", result.RouteValues["action"]);
   //   }


   //     [TestMethod]
   //     public void ListUsers_Should_Have_Correct_View()
   //     {
   //         // Arrange
   //         List<UserViewModel> users = new List<UserViewModel>();
   //         users.Add(new UserViewModel(){FirstName = "user1"});
   //         users.Add(new UserViewModel(){FirstName = "user2"});
   //         users.Add(new UserViewModel(){FirstName = "user3"});

   //         accountMgr.Setup(p => p.GetDataAdminManageUser(string.Empty, 0, string.Empty, SqlOrderByDirecton.ASC)).Returns(users);
   //         var controller = new AccountController(accountMgr.Object, new Mock<ILookupManager>().Object, new Mock<IWebSecurityWrapper>().Object, new Mock<IQuestionManager>().Object, new Mock<IProjectActionManager>().Object, new Mock<IUserLoginManager>().Object, new Mock<IReviewerTitlesManager>().Object, new Mock<ITaskReAssignmentManager>().Object, new Mock<IProjectActionFormManager>().Object);

   //         // Act
   //         var result = (ViewResult)controller.ListUsers(string.Empty, 0, string.Empty, string.Empty);

   //         // Assert
   //         Assert.IsInstanceOfType(result.ViewData.Model, typeof(IEnumerable<UserViewModel>));
   //         Assert.AreEqual(3, (result.ViewData.Model as IEnumerable<UserViewModel>).Count());
   //     }

   //     [TestMethod]
   //     public void User_Name_Should_be_Email()
   //     {
   //         var userRepo = new UserRepository();
   //         var user = userRepo.GetAllUsers().Where(p => p.UserName != "admin").FirstOrDefault();
   //         var regexUtils = new RegexUtils();
   //         Assert.IsNotNull(user.UserName);
   //         Assert.IsTrue(regexUtils.IsValidEmail(user.UserName));
   //     }

   //     [TestMethod]
   //     public void Retrieve_Password_Should_Fail_If_Answers_Incorrect()
   //     {
   //         var accountMgr = new Mock<IAccountManager>();
   //         var lookupMgr = new Mock<ILookupManager>();
   //         var webSecurity = new Mock<IWebSecurityWrapper>();
   //         var questionMgr = new Mock<IQuestionManager>();
   //         var projectActionMgr = new Mock<IProjectActionManager>();
   //         var uselogiMgr = new Mock<IUserLoginManager>();
   //         var useRevMgr = new Mock<IReviewerTitlesManager>();
   //         var taskMgr = new Mock<ITaskReAssignmentManager>();
			//var projectActionFormMgr= new Mock<IProjectActionFormManager>();

			//accountMgr.Setup(p => p.GetSecQuestionsByUserId(It.IsAny<int>())).Returns(
   //             new SecurityQuestionViewModel()
   //             {
   //                 FirstQuestionId = "1",
   //                 FirstAnswer = "First Answer",
   //                 SecondQuestionId = "2",
   //                 SecondAnswer = "Second Answer",
   //                 ThirdQuestionId = "3",
   //                 ThirdAnswer = "Third Answer",
   //                 UserAnswer1 = "First Answer",
   //                 UserAnswer2 = "Second Answer",
   //                 UserAnswer3 = "Third Wrong Answer",
   //                 UserName = "currentUser"
   //             });            
   //         var answerModel = accountMgr.Object.GetSecQuestionsByUserId(1);
   //         var accountController = new AccountController(accountMgr.Object, lookupMgr.Object, webSecurity.Object, questionMgr.Object, projectActionMgr.Object, uselogiMgr.Object, useRevMgr.Object, taskMgr.Object,projectActionFormMgr.Object);
   //         var result = accountController.RetrievePassword(answerModel) as ViewResult;
   //         Assert.AreEqual("One or more answers are incorrect.", result.ViewData["Message"]);
   //     }

   //     //[TestMethod]
   //     //public void Retrieve_Password_Should_Succeed_If_Answers_Correct()
   //     //{
   //     //    var accountMgr = new Mock<IAccountManager>();
   //     //    var lookupMgr = new Mock<ILookupManager>();
   //     //    var webSecurity = new Mock<IWebSecurityWrapper>();
   //     //    webSecurity.Setup(p => p.GeneratePasswordResetToken(It.IsAny<string>(), It.IsAny<int>())).Returns("resetToken");
   //     //    accountMgr.Setup(p => p.GetSecQuestionsByUserId(It.IsAny<int>())).Returns(
   //     //        new SecurityQuestionViewModel()
   //     //        {
   //     //            FirstQuestionId = "1",
   //     //            FirstAnswer = "First Answer",
   //     //            SecondQuestionId = "2",
   //     //            SecondAnswer = "Second Answer",
   //     //            ThirdQuestionId = "3",
   //     //            ThirdAnswer = "Third Answer",
   //     //            UserAnswer1 = "First Answer",
   //     //            UserAnswer2 = "Second Answer",
   //     //            UserAnswer3 = "Third Answer",
   //     //            UserName = "currentUser"
   //     //        });
   //     //    var answerModel = accountMgr.Object.GetSecQuestionsByUserId(1);
   //     //    var accountController = new AccountController(accountMgr.Object, lookupMgr.Object, webSecurity.Object);
   //     //    var routes = new RouteCollection();
   //     //    var context = new Mock<HttpContextBase>();
   //     //    var request = new Mock<HttpRequestBase>();
   //     //    request.SetupGet(p => p.Url).Returns(new Uri("mockUri"));
   //     //    context.SetupGet(x => x.Request).Returns(request.Object);
   //     //    accountController.ControllerContext = new ControllerContext(context.Object, new RouteData(), accountController);
   //     //    accountController.Url = new UrlHelper(
   //     //        new RequestContext(
   //     //            accountController.HttpContext, new RouteData()
   //     //        ),
   //     //        routes
   //     //    );
   //     //    var result = accountController.RetrievePassword(answerModel) as ViewResult;
   //     //    Assert.AreEqual("One or more answers are incorrect.", result.ViewData["Message"]);
   //     //}

   //     [TestMethod]
   //     public void Test_Comment_Serialization()
   //     {
   //         var comment = new CommentModel(new CommentsIdentity());
   //         comment.IsCommentPublic = true;
   //         comment.Comment = "This is parent comment";
   //         var commentChild1 = new CommentModel();
   //         var commentChild2 = new CommentModel();
   //         commentChild1.Comment = "Child comment1";
   //         commentChild2.Comment = "Child comment2";
   //         comment.ChildComments.Add(commentChild1);
   //         comment.ChildComments.Add(commentChild2);
   //         string strResult = XmlHelper.SerializeObject(comment);
   //         var comment2 = XmlHelper.DeserializeObject(typeof(CommentModel), strResult) as CommentModel;
   //         Assert.AreEqual(comment.Comment, comment2.Comment);
   //         Assert.AreEqual(2, comment.ChildComments.Count);
   //         Assert.AreEqual(comment.ChildComments.Count, comment2.ChildComments.Count);
   //     }

    }
}
