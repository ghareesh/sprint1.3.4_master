﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Repository.Interfaces;

namespace HUDHealthcareUnitTest.BusinessService
{
    [TestClass]
    public class UploadDataManagerTest
    {
        private IDataUploadIntermediateRepository dataUploadIntermediateRepository;
        private IUploadDataSummaryRepository uploadDataSummaryRepository;
        private IUserLenderServicerRepository userLenderServicerRepository;
        private IDataUploadLiveRepository dataUploadLiveRepository;
        private UnitOfWork unitOfWorkIntermediate = new UnitOfWork(DBSource.Intermediate);
        private UnitOfWork unitOfWorkLive = new UnitOfWork(DBSource.Live);
        private Mock<IDataUploadIntermediateRepository> dataUploadIntermediateRepoMock;
        private Mock<IUploadDataSummaryRepository> uploadDataSummaryRepoMock;
        private Mock<IUserLenderServicerRepository> userLenderServicerRepoMock;
        private Mock<IDataUploadLiveRepository> dataUploadLiveRepoMock;
        private Mock<IUploadDataManager> uploadDataManagerMock;
        private Mock<UserPrincipal> identityMock;
        //private UserDataC

        [TestInitialize]
        public void Initialize()
        {
            dataUploadIntermediateRepoMock = new Mock<IDataUploadIntermediateRepository>();
            uploadDataSummaryRepoMock = new Mock<IUploadDataSummaryRepository>();
            userLenderServicerRepoMock = new Mock<IUserLenderServicerRepository>();
            dataUploadLiveRepoMock = new Mock<IDataUploadLiveRepository>();
            uploadDataManagerMock = new Mock<IUploadDataManager>();

            identityMock = new Mock<UserPrincipal>();

            unitOfWorkIntermediate = new UnitOfWork(DBSource.Intermediate);
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
        }

        //[TestMethod]
        //public void GetDataUploadSummaryByUserId_Should_Return_My_Data()
        //{
        //    var myUserName = "me";
        //    var otherUserName = "other";
            

        //    List<ReportExcelUpload_Model> data = new List<ReportExcelUpload_Model>();
        //    data.Add(new ReportExcelUpload_Model() { UserName = myUserName });
        //    data.Add(new ReportExcelUpload_Model() { UserName = myUserName });
        //    data.Add(new ReportExcelUpload_Model() { UserName = otherUserName });
        //    uploadDataSummaryRepoMock
        //        .Setup(p => p.GetDataUploadSummaryByUserId(It.IsAny<int>()))
        //        .Returns(data.Where(p => p.UserName == myUserName));

        //   var login = WebSecurity.Login("admin", "admin");
        //    FormsAuthentication.SetAuthCookie("admin", false);

        //    var uploadManager = new UploadDataManager(null, uploadDataSummaryRepoMock.Object, null, null);
        //    var result = uploadManager.GetDataUploadSummaryByUserId(1);
        //    Assert.IsInstanceOfType(result, typeof(IEnumerable<ReportExcelUpload_Model>));
        //    //Assert.AreEqual(2, result.Count());

        //    FormsAuthentication.SignOut();
        //}
    }
}
