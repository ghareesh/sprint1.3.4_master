﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessService.Interfaces.Production;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_live.Programmability;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces;
using EntityObject.Entities.HCP_task;
using EntityObject.Entities.HCP_task.Programmability;

namespace Repository.Production
{
    public class ProductionPamReportRepository : BaseRepository<PAMReportProductionViewModel>, IProductionPAMReportRepository
    {

        //  private ProductionPAMReportModel _plmReportModel;

        const int TrueInt = 1;
        const int FalseInt = 0;


        public ProductionPamReportRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
            // _plmReportModel = new ProductionPAMReportModel(ReportType.HudProductionReport);
        }

        public ProductionPamReportRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public ProductionPAMReportModel GetProductionPAMReportSummary(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate, string status, string Level, Guid? grouptaskinstanceid, string Mode = "Other")
        {
            if (fromDate == null && toDate == null)
            {
                //fromDate = DateTime.Now.AddYears(-1);
                fromDate = DateTime.UtcNow.AddYears(-1);
            }
            var reportModel = new ProductionPAMReportModel(ReportType.HudProductionReport);
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db Live in GetProductionPAMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                string Spname = string.Empty;
                if (Mode == "Other")
                {
                    Spname = "USP_GetProductionPamReport";
                }
                else
                {
                    Spname = "USP_GetProductionPamReportExcl";
                }


                var results = context.Database.SqlQuerySimple<usp_HCP_GetProdPAMReportDetail_Result>(Spname,
                  new
                  {
                      Username = UserPrincipal.Current.UserName,
                      AppType = string.IsNullOrEmpty(appType) ? null : appType,
                      ProgramType = string.IsNullOrEmpty(programType) ? null : programType,
                      Role = UserPrincipal.Current.UserRole,
                      Userlist = string.IsNullOrEmpty(prodUserIds) ? null : prodUserIds,
                      StatusId = string.IsNullOrEmpty(status) ? null : status,
                      startTime = fromDate,
                      endTime = toDate,
                      Level = Level,
                      grouptaskinstanceid = grouptaskinstanceid


                  }).ToList();

                if (Mode == "Other" && Level == "High")
                {
                    results = results.Where(a => a.viewname == "Underwriter/Closer" || a.viewname == "InsertFha").Where(a => a.RAI != "YES").ToList();
                    // results = results.Where(a => a.viewname == "Underwriter/Closer").Where(a => a.RAI != "YES").ToList();
                }
                else
                {

                }
                var Vm = Mapper.Map<IList<usp_HCP_GetProdPAMReportDetail_Result>, IList<PAMReportProductionViewModel>>(results);



                reportModel.PAMReportGridProductionlList = Vm;

            }
            catch (Exception ex)
            {

            }
            return reportModel;
        }


        public ProductionPAMReportModel GetProductionLenderPAMReportSummary(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes)
        {

            var reportModel = new ProductionPAMReportModel(ReportType.HudProductionReport);
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db Live in GetProductionPAMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                string Spname = string.Empty;

                var results = context.Database.SqlQuerySimple<usp_HCP_GetProdPAMReportDetail_Result>("usp_HCP_GetProdLenderPAMReport",
                  new
                  {
                      RoleList = roleIds,
                      LAMList = lamIds,
                      BAMList = bamIds,
                      LARList = larIds,
                      StartDate = fromDate,
                      EndDate = toDate,
                      Status = status,
                      UserId = UserPrincipal.Current.UserId,
                      UserLenderId = UserPrincipal.Current.LenderId,
                      AppType = appType,
                      ProgramType = programTypes,
                      MultiroleUserLoggedInRole = UserPrincipal.Current.UserRole

                  }).ToList();
                var Vm = Mapper.Map<IList<usp_HCP_GetProdPAMReportDetail_Result>, IList<PAMReportProductionViewModel>>(results);
                reportModel.PAMReportGridProductionlList = Vm;

            }
            catch (Exception ex)
            {

            }
            return reportModel;
        }

        public ProductionPAMReportModel GetLenderPAMSubGridChildTasks(Guid taskInstanceId)
        {

            var reportModel = new ProductionPAMReportModel(ReportType.HudProductionReport);
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db Live in GetProductionPAMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                string Spname = string.Empty;

                var results = context.Database.SqlQuerySimple<usp_HCP_GetProdPAMReportDetail_Result>("usp_HCP_GetProdLenderPAMReportSubGridChildData",
                  new
                  {
                      taskInstanceId = taskInstanceId

                  }).ToList();
                var Vm = Mapper.Map<IList<usp_HCP_GetProdPAMReportDetail_Result>, IList<PAMReportProductionViewModel>>(results);
                reportModel.PAMReportGridProductionlList = Vm;

            }
            catch (Exception ex)
            {

            }
            return reportModel;
        }

        public ProductionPAMReportModel GetProductionLenderPAMExcel(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes)
        {

            var reportModel = new ProductionPAMReportModel(ReportType.HudProductionReport);
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db Live in GetProductionPAMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                string Spname = string.Empty;

                var results = context.Database.SqlQuerySimple<usp_HCP_GetProdPAMReportDetail_Result>("usp_HCP_GetProdLenderPAMExcel",
                  new
                  {
                      RoleList = roleIds,
                      LAMList = lamIds,
                      BAMList = bamIds,
                      LARList = larIds,
                      StartDate = fromDate,
                      EndDate = toDate,
                      Status = status,
                      UserId = UserPrincipal.Current.UserId,
                      UserLenderId = UserPrincipal.Current.LenderId,
                      AppType = appType,
                      ProgramType = programTypes

                  }).ToList();
                var Vm = Mapper.Map<IList<usp_HCP_GetProdPAMReportDetail_Result>, IList<PAMReportProductionViewModel>>(results);
                reportModel.PAMReportGridProductionlList = Vm;

            }
            catch (Exception ex)
            {

            }
            return reportModel;
        }


        public PAMReportTestGrid1ViewModel GetProdPAMGridOneResults(Guid taskInstanceId)
        {

            var reportModel = new PAMReportTestGrid1ViewModel();
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db task in GetProdPAMGridOneResults, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;


                reportModel = context.Database.SqlQuerySimple<PAMReportTestGrid1ViewModel>("usp_HCP_Prod_GetPAMGridOneResults",
                  new
                  {
                      TaskInstanceId = taskInstanceId

                  }).FirstOrDefault();


            }
            catch (Exception ex)
            {

            }
            return reportModel;
        }

        public PAMReportTestGrid2ViewModel GetProdPAMGridTwoResults(Guid taskInstanceId)
        {

            var reportModel = new PAMReportTestGrid2ViewModel();
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db task in GetProdPAMGridTwoResults, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;


                reportModel = context.Database.SqlQuerySimple<PAMReportTestGrid2ViewModel>("usp_HCP_Prod_GetPAMGridTwoResults",
                  new
                  {
                      TaskInstanceId = taskInstanceId

                  }).FirstOrDefault();


            }
            catch (Exception ex)
            {

            }
            return reportModel;
        }

        /// <summary>
        /// Prod Pam report - sub tasks
        /// </summary>
        /// <param name="pTaskInstanceId"></param>
        /// <param name="pFHANumber"></param>
        /// <returns></returns>
        public IList<PAMReportV3ViewModel> GetProdHUDPAMSubReportResults(Guid pTaskInstanceId, string pFHANumber)
        {//skumar-2838
            var reportList = new List<PAMReportV3ViewModel>();
            try
            {
                var context = (HCP_task)this.Context;
                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db task in usp_HCP_Prod_GetProdHUDPAMSubReportV3, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;

                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetProdHUDPAMReportResult>("usp_HCP_Prod_GetProdHUDPAMSubReportV3"
                                                                                                        , new
                                                                                                        {
                                                                                                            CurrentTaskInstanceId = pTaskInstanceId,
                                                                                                            FhaNumber = pFHANumber
                                                                                                        }).ToList();
                reportList = Mapper.Map<IList<PAMReportV3ViewModel>>(results).ToList();
                reportList = reportList.GroupBy(item => item.ProjectStage).Select(grouping => grouping.FirstOrDefault()).ToList();
            }
            catch (Exception ex)
            {
            }
            return reportList;
        }
        public IList<PAMReportV3ViewModel> GetProdHUDPAMReportResults()
        {
            List<PAMReportV3ViewModel> lstPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
            var reportList = new List<PAMReportV3ViewModel>();
            try
            {

                var context = (HCP_task)this.Context;
                //# 701 siddu changed sp name to usp_HCP_Prod_GetProdHUDPAMReportV3_new31dec2019//
                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db task in usp_HCP_Prod_GetProdHUDPAMReportV3_new31dec2019, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;


                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetProdHUDPAMReportResult>("usp_HCP_Prod_GetProdHUDPAMReportV3_new31dec2019", null).ToList();
                reportList = Mapper.Map<IList<PAMReportV3ViewModel>>(results).ToList();

                reportList.ForEach(p => p.Amendment = (p.AmendmentCount.HasValue && p.AmendmentCount.Value > 0) ? p.AmendmentCount.Value : 0);
                lstPAMReportV3ViewModel = reportList.Where(o => o.FhaNumber == "N/A").ToList();
                reportList = reportList.GroupBy(item => item.FhaNumber).Select(grouping => grouping.FirstOrDefault()).ToList();
                reportList = reportList.Except(lstPAMReportV3ViewModel).ToList();
                lstPAMReportV3ViewModel = lstPAMReportV3ViewModel.Concat(reportList).ToList();
                lstPAMReportV3ViewModel = lstPAMReportV3ViewModel.Where(o => o.FHARequestStatus != (int)RequestStatus.Draft).ToList();
            }
            catch (Exception ex)
            {

            }
            return lstPAMReportV3ViewModel;
        }

        public ProdPAMReportStatusGridViewModel GetProdPAMReportStatusGridResults()
        {
            var results = new ProdPAMReportStatusGridViewModel();
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db task in usp_HCP_Prod_GetPAMReportStatusGrid, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;


                results = context.Database.SqlQuerySimple<ProdPAMReportStatusGridViewModel>("usp_HCP_Prod_GetPAMReportStatusGrid", null).FirstOrDefault();


            }
            catch (Exception ex)
            {

            }
            return results;
        }
    }
}
