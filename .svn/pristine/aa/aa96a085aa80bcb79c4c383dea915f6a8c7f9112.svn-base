﻿@using AutoMapper.Internal
@using HUDHealthcarePortal.BusinessService
@model PaginateSortModel<DerivedUploadData>
<script type="text/javascript">
    $(document).ready(function () {
        //$("#bcHolder").text("Home > Financial Analysis > Upload Status > My Upload Report List");

        styleGrid();

        var selectedquarter = '@TempData["ReportingPeriod"]';
        var strValue = selectedquarter.split("/");
        var reportingPeriod = "";
        if (strValue.length > 0) {
            if (strValue[0] == "3") {
                reportingPeriod = "Q1 " + strValue[1];
            } else if (strValue[0] == "6") {
                reportingPeriod = "Q2 " + strValue[1];
            } else if (strValue[0] == "9") {
                reportingPeriod = "Q3 " + strValue[1];
            } else if (strValue[0] == "12") {
                reportingPeriod = "Q4 " + strValue[1];
            }
        }
        var labelValue = $("#reportingPeriodLabel").text() + reportingPeriod;
        $("#reportingPeriodLabel").text(labelValue);
    }
);

    function styleGrid() {
        $('.webGrid .color').each(function (index, element) { $(element).parent().css('background-color', $(element).html()); });
        $('.webGrid').fixedHeaderTable({ height: '490', width: '1260', fixedColumn: true, footer: true, themeClass: 'fixedHeader' });
    }
</script>
@{
    var grid = new WebGrid(canPage: true, rowsPerPage: 10, ajaxUpdateContainerId: "gridContent",
        ajaxUpdateCallback: "styleGrid");
    //grid.Pager(WebGridPagerModes.All);
    grid.Bind(Model.Entities, autoSortAndPage: false, rowCount: Model.TotalRows);
}
<div>
    @if (TempData["ReportingPeriod"] != null && TempData["ReportingPeriod"] != "")
    {
        @Html.Label("Reporting Period: ", new { id = "reportingPeriodLabel", style = "padding-left: 5px;" })
    }
    <div style="float:right;margin-right:0px; margin-bottom: 5px;" class="content-wrapper">
        @if (Model.Entities.Count() > 0 && !string.IsNullOrEmpty(ViewBag.ExcelExportAction))
        {
            @Html.ImageActionLink(UrlHelper.GenerateContentUrl(@"~/Images/icon_excelBg.jpg", Html.ViewContext.HttpContext), "Export to Excel", (string)ViewBag.ExcelExportAction, "ExcelExport", (object)ViewBag.ExcelActionParam, new { @class = "excelExport" }, null)
        }
        @if (Model.Entities.Count() > 0 && !string.IsNullOrEmpty(ViewBag.PrintAction) && Core.WebUiConstants.ShowPrintIcon)
        {
            @Html.ImageActionLink(UrlHelper.GenerateContentUrl(@"~/Images/icon_print.jpg", Html.ViewContext.HttpContext), "Print", (string)ViewBag.PrintAction, "Print", (object)ViewBag.ExcelActionParam, new { target = "_blank" }, null)
        }
    </div>
</div>
<div id="gridContent">
            @{
        var gridColumns = new List<WebGridColumn>();
        if (ViewBag.Context == "timestamp")
        {
            gridColumns.Add(grid.Column("ProjectName", "Project Name", format: (item) => (object)item.ProjectName != null ? Html.ActionLink(((object)item.ProjectName).ToString(), "ProjectDetailTime", "ExcelUploadView_", new { id = item.LDI_ID, source = item.DataSource, contextParam = ViewBag.ExcelActionParam }, null) : Html.Raw(string.Empty)));
        }
        else if (ViewBag.Context == "category")
        {
            gridColumns.Add(grid.Column("ProjectName", "Project Name", format: (item) => (object)item.ProjectName != null ? Html.ActionLink(((object)item.ProjectName).ToString(), "ProjectDetailCategory", "ExcelUploadView_", new { id = item.LDI_ID, source = item.DataSource, contextParam = ViewBag.ExcelActionParam, quarter = TempData["ReportingPeriod"].ToNullSafeString() }, null) : Html.Raw(string.Empty)));
        }
        else
        {
            gridColumns.Add(grid.Column("ProjectName", "Project Name", format: (item) => (object)item.ProjectName != null ? Html.ActionLink(((object)item.ProjectName).ToString(), "ProjectDetailProject", "ExcelUploadView_", new { id = item.LDI_ID, source = item.DataSource, contextParam = ViewBag.ExcelActionParam }, null) : Html.Raw(string.Empty)));
        }
        gridColumns.Add(grid.Column("ServiceName", "Lender Name", style: "description"));

        if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName) || RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
        {
            //gridColumns.Add(grid.Column("ScoreTotal", "Score", (item) => HtmlHelperExt.ScoreColorFormatter(item.ScoreTotal, "{0:0}"), style: "number"));
        }
        gridColumns.Add(grid.Column("HasComment", "Comments", format: (item) =>
        {
            if (item.HasComment == true)
            {
                if (ViewBag.Context == "timestamp")
                {
                    return Html.ImageActionLink(UrlHelper.GenerateContentUrl(@"~/Content/images/comments.png", Html.ViewContext.HttpContext), "Comments", "ProjectDetailTime", "ExcelUploadView_", new { id = item.LDI_ID, source = item.DataSource, contextParam = ViewBag.ExcelActionParam }, null, null);
                }
                if (ViewBag.Context == "category")
                {
                    return Html.ImageActionLink(UrlHelper.GenerateContentUrl(@"~/Content/images/comments.png", Html.ViewContext.HttpContext), "Comments", "ProjectDetailCategory", "ExcelUploadView_", new { id = item.LDI_ID, source = item.DataSource, contextParam = ViewBag.ExcelActionParam, quarter = TempData["ReportingPeriod"].ToNullSafeString() }, null, null);
                }
                return Html.ImageActionLink(UrlHelper.GenerateContentUrl(@"~/Content/images/comments.png", Html.ViewContext.HttpContext), "Comments", "ProjectDetailProject", "ExcelUploadView_", new { id = item.LDI_ID, source = item.DataSource, contextParam = ViewBag.ExcelActionParam }, null, null);
            }
            return Html.Raw(string.Empty);
        }
            ));

        //gridColumns.Add(grid.Column("OperatorOwner", "Operator/Owner", style: "description"));
        //gridColumns.Add(grid.Column("FinancialStatementType", "Financial Statement Type", style: "description"));
        //gridColumns.Add(grid.Column("UnitsInFacility", "Units in Facility", style: "number"));
        gridColumns.Add(grid.Column("PeriodEnding_Disp", "Period Ending", (item) => HtmlHelperExt.DateFormatter(item.PeriodEnding_Disp, "d")));
        gridColumns.Add(grid.Column("MonthsInPeriod_Disp", "Months In Period", style: "description"));
        gridColumns.Add(grid.Column("FHANumber", "FHA Number", style: "description"));
        //gridColumns.Add(grid.Column("OperatingCash", "Operating Cash", (item) => HtmlHelperExt.CurrencyFormatter(item.OperatingCash, "0"), style: "number"));
        //gridColumns.Add(grid.Column("Investments", "Investments", (item) => HtmlHelperExt.CurrencyFormatter(item.Investments, "0"), style: "number"));
        //gridColumns.Add(grid.Column("ReserveForReplacementEscrowBalance", "RFR Escrow Balance", (item) => HtmlHelperExt.CurrencyFormatter(item.ReserveForReplacementEscrowBalance, "0"), style: "number"));
        //gridColumns.Add(grid.Column("AccountsReceivable", "Accounts Receivable", (item) => HtmlHelperExt.CurrencyFormatter(item.AccountsReceivable, "0"), style: "number"));

        //gridColumns.Add(grid.Column("CurrentAssets", "Current Assets", (item) => HtmlHelperExt.CurrencyFormatter(item.CurrentAssets, "0"), style: "number"));
        //gridColumns.Add(grid.Column("CurrentLiabilities", "Current Liabilities", (item) => HtmlHelperExt.CurrencyFormatter(item.CurrentLiabilities, "0"), style: "number"));
        gridColumns.Add(grid.Column("TotalRevenues", "Total Revenues", (item) => HtmlHelperExt.CurrencyFormatter(item.TotalRevenues, "0"), style: "number"));
        //gridColumns.Add(grid.Column("RentLeaseExpense", "RentLease Expense", (item) => HtmlHelperExt.CurrencyFormatter(item.RentLeaseExpense, "0"), style: "number"));
        //gridColumns.Add(grid.Column("DepreciationExpense", "Depreciation Expense", (item) => HtmlHelperExt.CurrencyFormatter(item.DepreciationExpense, "0"), style: "number"));
        //gridColumns.Add(grid.Column("AmortizationExpense", "Amortization Expense", (item) => HtmlHelperExt.CurrencyFormatter(item.AmortizationExpense, "0"), style: "number"));
        gridColumns.Add(grid.Column("TotalExpenses", "Total Operating Expenses", (item) => HtmlHelperExt.CurrencyFormatter(item.TotalExpenses, "0"), style: "number"));
        //gridColumns.Add(grid.Column("NetIncome", "Net Income", (item) => HtmlHelperExt.CurrencyFormatter(item.NetIncome, "0"), style: "number"));
        //gridColumns.Add(grid.Column("ReserveForReplacementDeposit", "Reserve for Replacement Deposit", (item) => HtmlHelperExt.CurrencyFormatter(item.ReserveForReplacementDeposit, "0"), style: "number"));
        gridColumns.Add(grid.Column("FHAInsuredPrincipalInterestPayment", "FHA Insured Principal & Interest Payment", (item) => HtmlHelperExt.CurrencyFormatter(item.FHAInsuredPrincipalInterestPayment, "0"), style: "number"));
        //gridColumns.Add(grid.Column("FHAInsuredInterestPayment", "FHA Insured Interest Payment", (item) => HtmlHelperExt.CurrencyFormatter(item.FHAInsuredInterestPayment, "0"), style: "number"));
        gridColumns.Add(grid.Column("MortgageInsurancePremium", "Mortgage Insurance Premium (MIP)", (item) => HtmlHelperExt.CurrencyFormatter(item.MortgageInsurancePremium, "0"), style: "number"));
        gridColumns.Add(grid.Column("ActualNumberOfResidentDays", "Actual Number Of Resident Days", style: "number", format: (item) => string.Format("{0:0}", item.ActualNumberOfResidentDays ?? string.Empty)));
        
        @*gridColumns.Add(grid.Column("ReserveForReplacementBalancePerUnit", "Reserve for Replacement Balance per unit", style: "number", format: (item) => string.Format("{0:0.0}", item.ReserveForReplacementBalancePerUnit ?? string.Empty)));*@
        //gridColumns.Add(grid.Column("ReserveForReplacementBalancePerUnit", "Reserve for Replacement Balance per unit", (item) => HtmlHelperExt.CurrencyFormatter(item.ReserveForReplacementBalancePerUnit, "0"), style: "number"));

        gridColumns.Add(grid.Column("DebtCoverageRatio", "Debt Service Coverage Ratio", style: "number", format: (item) => string.Format("{0:0.0}", item.DebtCoverageRatio2 ?? string.Empty)));

        @*gridColumns.Add(grid.Column("WorkingCapital", "Working Capital", style: "number", format: (item) => string.Format("{0:0.0}", item.WorkingCapital ?? string.Empty)));*@
                //gridColumns.Add(grid.Column("WorkingCapital", "Working Capital", (item) => HtmlHelperExt.CurrencyFormatter(item.WorkingCapital, "0"), style: "number"));

                //gridColumns.Add(grid.Column("DaysCashOnHand", "Days Cash On Hand", style: "number", format: (item) => string.Format("{0:0}", item.DaysCashOnHand ?? string.Empty)));
                //gridColumns.Add(grid.Column("DaysInAcctReceivable", "Days in Accounts Receivable", style: "number", format: (item) => string.Format("{0:0}", item.DaysInAcctReceivable ?? string.Empty)));
                //gridColumns.Add(grid.Column("AvgPaymentPeriod", "Average Payment Period", style: "number", format: (item) => string.Format("{0:0}", item.AvgPaymentPeriod ?? string.Empty)));
                if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName))
                {
                    //gridColumns.Add(grid.Column("DebtCoverageRatioScore", "Debt Coverage Ratio Score", style: "number", format: (item) => string.Format("{0:0}", item.DebtCoverageRatioScore ?? string.Empty)));
                    //gridColumns.Add(grid.Column("WorkingCapitalScore", "Working Capital Score", style: "number", format: (item) => string.Format("{0:0}", item.WorkingCapitalScore ?? string.Empty)));
                    //gridColumns.Add(grid.Column("DaysCashOnHandScore", "Days Cash on Hand Score", style: "number", format: (item) => string.Format("{0:0}", item.DaysCashOnHandScore ?? string.Empty)));
                    //gridColumns.Add(grid.Column("DaysInAcctReceivableScore", "Days in Accounts Receivable Score", style: "number", format: (item) => string.Format("{0:0}", item.DaysInAcctReceivableScore ?? string.Empty)));
                    //gridColumns.Add(grid.Column("AvgPaymentPeriodScore", "Average Payment Period Score", style: "number", format: (item) => string.Format("{0:0}", item.AvgPaymentPeriodScore ?? string.Empty)));
                    //gridColumns.Add(grid.Column("ScoreTotal", "Score", (item) => HtmlHelperExt.ScoreColorFormatter(item.ScoreTotal, "{0:0}"), style: "number"));

                    gridColumns.Add(grid.Column("AverageDailyRate", "Average Daily Rate", style: "number", format: (item) => string.Format("{0:0.0}", item.AverageDailyRateRatio ?? string.Empty)));
                    gridColumns.Add(grid.Column("NOIRatio", "NOI Ratio", style: "number", format: (item) => string.Format("{0:0.0}", item.NOIRatio ?? string.Empty)));
                }
                else
                {
                    gridColumns.Add(grid.Column("AverageDailyRate", "Average Daily Rate", style: "number", format: (item) => string.Format("{0:0.0}", item.AverageDailyRateRatio ?? string.Empty)));
                    gridColumns.Add(grid.Column("NOIRatio", "NOI Ratio", style: "number", format: (item) => string.Format("{0:0.0}", item.NOIRatio ?? string.Empty)));
                }
                //gridColumns.Add(grid.Column("DataInserted", "Date Inserted", format: (item) => item.DataInserted == null ? string.Empty : TimezoneManager.GetPreferredTimeFromUtc(item.DataInserted).ToString()));
                gridColumns.Add(grid.Column("DataInserted", "Date Inserted", format: (item) => item.DataInserted == null ? string.Empty : item.DataInserted.ToString()));
    }
    @if (Model.Entities.Count() > 0)
    {
        @grid.GetHtml(tableStyle: "webGrid",
                headerStyle: "header_row",
                rowStyle: "item_row",
                alternatingRowStyle: "item_alt_row",
                selectedRowStyle: "select",

                //fillEmptyRows: false,
            //alternatingRowStyle: "alternate-row",
            //headerStyle: "grid-header",
            //footerStyle: "grid-footer",
               mode: WebGridPagerModes.All,
               firstText: "<< First",
               previousText: "< Prev",
               nextText: "Next >",
               lastText: "Last >>",

                columns: gridColumns.ToArray())
    }
    else
    {
        <p style="margin-left:20px;">@(string.IsNullOrEmpty(ViewBag.EmptyMessage) ? "No data found." : @ViewBag.EmptyMessage)</p>
    }
</div>

