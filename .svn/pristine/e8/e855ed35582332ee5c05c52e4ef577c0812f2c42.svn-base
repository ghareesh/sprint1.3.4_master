﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Xml.Serialization;
using HUDHealthcarePortal.Model.ProjectAction;
using HUDHealthcarePortal.Model;
using System.ComponentModel.DataAnnotations;
using Model;

namespace HUDHealthcarePortal.Model
{
    public class OPAViewModel
    {
        public string FormName { get; set; }
        public Guid ProjectActionFormId { get; set; }
        public int PropertyId { get; set; }
        public int LenderId { get; set; }
        [XmlIgnore]
        public string LenderName { get; set; }
        public string FhaNumber { get; set; }
        public string PropertyName { get; set; }
        public DateTime? RequestDate { get; set; }
        public DateTime? ServicerSubmissionDate { get; set; }
        public DateTime ProjectActionDate { get; set; }
        public int ProjectActionTypeId { get; set; }
        public int MytaskId { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string RequesterName { get; set; }
        public AddressModel PropertyAddress { get; set; }
        public OPAWorkProductModel PropertyWorkProduct { get; set; }
        public string ButtonName { get; set; }
        public string GridVisibility { get; set; }
        public int columnVisibility { get; set; }
        public bool ShowVisibility { get; set; }
        public bool IsIRRequest { get; set; }
        public bool TransAccessStatus { get; set; }
        public bool UploadFailed { get; set; }

        [XmlIgnore]
        public IList<QuestionViewModel> Questions { get; set; }
        [XmlIgnore]
        public IList<String> AvailableFHANumbersList { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> ProjectActionTypeList { get; set; }

        [XmlIgnore]
        public IList<TaskFileModel> TaskFileList { get; set; }

        // task related fields
        public Guid? TaskGuid { get; set; }
        public int? SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public TaskOpenStatusModel TaskOpenStatus { get; set; }
        public byte[] Concurrency { get; set; }

        public bool? IsConcurrentUserUpdateFound { get; set; }

        public int? TaskId { get; set; }
        public bool IsPAMReport { get; set; }
        public bool IsMortgagePaymentsCovered { get; set; }
        //public ContactAddressViewModel ContactAddressViewModel { get; set; }

        //  [Required(ErrorMessage = "This field is required")]// User Story 1901
        public bool IsAgreementAccepted { get; set; }

        public bool IsApprovedPopupDisplayedForAe { get; set; }
        public bool IsChangeDocTypePopupDisplayed { get; set; }
        //harish added new property for changing project action request form 06-02-2020
        public bool IsProjectActionChanged { get; set; }
        //Added by siddu #841 @21042020
        public bool IsProdQueuCompleted { get; set; }
        //Added by siddu #841 @21042020
        public bool IsProdQueuQueue{ get; set; }
        //Added by siddu #841 @21042020
        public bool IsProdQueuInprocess { get; set; }

        public OPAViewModel()
        {
            AvailableFHANumbersList = new List<string>();
            ProjectActionTypeList = new List<SelectListItem>();
            PropertyAddress = new AddressModel();
            PropertyWorkProduct = new OPAWorkProductModel();
            RequestAdditionalChildTask = new ParentChildTaskModel();
            //ContactAddressViewModel = new ContactAddressViewModel();

        }

        public int RequestStatus { get; set; }
        public int? GroupTaskId { get; set; }
        public Guid? GroupTaskInstanceId { get; set; }
        public DateTime GroupTaskCreatedOn { get; set; }

        public bool IsEditMode { get; set; }

        public string ProjectActionName { get; set; }
        [XmlIgnore]
        public bool IsDisplayCheckoutBtn { get; set; }
        [XmlIgnore]
        public bool IsViewFromGroupTask { get; set; }
        [XmlIgnore]
        public bool IsDisplayApproveBtn { get; set; }
        [XmlIgnore]
        public bool IsLoggedInUserLender { get; set; }
        public bool? IsReassigned { get; set; }
        public string ServicerComments { get; set; }
        public string AEComments { get; set; }


        [XmlIgnore]
        public bool IsRAI { get; set; }
        public List<OPAHistoryViewModel> OpaHistoryViewModel { get; set; }

        public List<OPAWorkProductModel> OpaWorkProductModel { get; set; }
        public Guid TaskInstanceId { get; set; }

        public bool IsAddressChange { get; set; }

        //User story 1904
        public bool IsLenderPAMReport { get; set; }

        //User Story 1901 
        public string DisclaimerText { get; set; }

        public ParentChildTaskModel RequestAdditionalChildTask { get; set; }
        public string RequestAdditionalComment { get; set; }
        public string LenderComment { get; set; }
        public Guid ChildTaskInstanceId { get; set; }

        //OPA TPA Reviewer
        public string userName { get; set; }

        public string userRole { get; set; }

        public bool hasReviewComments { get; set; }

        public int TaskStepId { get; set; }
        public int NoOfPendingRAI { get; set; }
        public bool? IschildTask { get; set; }
        public string DenyStatus { get; set; }
        [XmlIgnore]
        public bool IsRIS { get; set; }
        //karri-MH232-80
        public int isFCRaised { get; set; }//1=true, rset is false

        //karri, venkat#832
        public int Form290ClosingPreCheckConditionsValue { get; set; }

        #region Production Application

        public Guid taskxrefId { get; set; }
        public int viewId { get; set; }
        public bool isChildTaskOpen { get; set; }
        public int reviertaskStatus { get; set; }
        public int pageTypeId { get; set; }
        public string PopupTarget { get; set; }
        public int userRoleId { get; set; }
        public bool Isotherloantype { get; set; }
        public string status { get; set; }
        public DateTime? CompletedOn { get; set; }
        public string comments { get; set; }
        public string folderName { get; set; }
        public string fileName { get; set; }
        public Guid taskFileId { get; set; }
        public int fileId { get; set; }
        public int downloadFileId { get; set; }
        public bool isInOpenChildTask { get; set; }
        public int userId { get; set; }
        public string DocTypeId { get; set; }

        public bool IsFileUploaded { get; set; }
		#endregion


		#region Amendments
		//[UIHint("SignaturePad")]
		//public byte[] MySignature { get; set; }

		//[UIHint("SignaturePad")]
		//public byte[] CurrentSignature { get; set; }

		//public bool IsNewSignatureRequired { get; set; }

		public List<Prod_FormAmendmentTaskModel> Prod_FormAmendmentTaskModels { get; set; }
		public Prod_FormAmendmentTaskModel Prod_FormAmendmentTaskModel { get; set; }
		#endregion
	}
}
