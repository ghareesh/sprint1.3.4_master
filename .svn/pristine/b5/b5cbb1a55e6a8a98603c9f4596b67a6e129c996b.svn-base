﻿using System;
using System.Collections.Generic;
using EntityObject.Entities.HCP_intermediate;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IDataUploadIntermediateRepository : IJoinable<Lender_DataUpload_Intermediate>
    {
        HUDHealthcarePortal.Model.ExcelUploadView_Model GetProjectDetail(int ldiID);
        System.Collections.Generic.IEnumerable<ExcelUploadView_Model> GetUnCalculated();
       IEnumerable<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedData(int? iLenderId);
       IEnumerable<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataByRole(int? iLenderId, ICommentViewRepository commentViewRepo,
            List<string> limitToFhas);
        IEnumerable<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataByTimeForSearch(DateTime dateInserted,
            ICommentViewRepository commentViewRepo);
        IEnumerable<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataByTime(DateTime dateInserted);
        System.Collections.Generic.IEnumerable<ExcelUploadView_Model> GetUploadedDataByScoreRange(decimal minVal, decimal maxVal);
        void UpdateData(System.Collections.Generic.IEnumerable<ExcelUploadView_Model> model);
        PaginateSortModel<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataWithPageSort(int? iLenderId,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, ICommentViewRepository commentRepo, List<string> limitToFhas);
        PaginateSortModel<HUDHealthcarePortal.Model.DerivedUploadData> GetUploadedDataByTimePageSort(DateTime dateInserted, int userId,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, ICommentViewRepository commentViewRepo);
        int SaveFormUploadToIntermediateTbl(FormUploadModel model);
        System.Collections.Generic.IEnumerable<ReportViewModel> GetMissingProjectsForLender(string username,
            string usertype, int lenderId, int monthsInPeriod, int year);
        Tuple<DateTime, int> GetLatestPeriodEnding(int? iLenderId);

        IEnumerable<ProjectInfoViewModel> GetProjectInfoReport(string username);
        int DistributeQuarterly(string QryDate = "4/21/2016");

        int ApplyCalculations(string QryDate = "4/21/2016");
        int CalculateErrors(string QryDate = "4/21/2016");

        IEnumerable<KeyValuePair<int, string>> GetPeriodEnding(string QryDate = "4/21/2016");
        List<string> GetExistingFHAs(List<string> uploadItems);
        Tuple<string, int> GetLatestPeriodEnding(int lenderId, string fha, string queryDate);
    }
}
