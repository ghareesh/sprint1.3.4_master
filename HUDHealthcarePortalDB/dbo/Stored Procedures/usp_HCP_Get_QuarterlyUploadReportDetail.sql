﻿CREATE PROCEDURE [dbo].[usp_HCP_Get_QuarterlyUploadReportDetail]
(
@LenderId int	-- lender id: -1 gets all lenders
)
AS
DECLARE @dtNow DATETIME
DECLARE @firstDayOfQrtRecent DATETIME
SET @dtNow = GETDATE()
SELECT @firstDayOfQrtRecent = DATEADD(quarter,DATEDIFF(quarter,0,@dtNow),0);

SELECT	
LF.LenderID, 
LI1.Lender_Name,
PI1.ProjectName,
LF.FHANumber,
CASE WHEN LDI.LenderID IS NOT NULL THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS Received
FROM [HCP_Live_db].[dbo].[Lender_FHANumber] LF
LEFT JOIN 
(SELECT LDI1.LenderID, LDI1.DataInserted, LDI1.FHANumber 
FROM [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate] LDI1 
INNER JOIN
(
	SELECT MAX(FHANumber) AS MaxFHA, MAX(DataInserted) as MaxDate
	FROM [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate]
	GROUP BY LenderID, FHANumber
) b
ON LDI1.FHANumber = b.MaxFHA AND LDI1.DataInserted = b.MaxDate) LDI
ON LF.LenderID = LDI.LenderID
AND LF.FHANumber = LDI.FHANumber
LEFT JOIN [HCP_Live_db].[dbo].[LenderInfo] LI1
ON LF.LenderID = LI1.LenderID
LEFT JOIN [HCP_Live_db].[dbo].[ProjectInfo] PI1
ON PI1.FHANumber = LF.FHANumber
LEFT JOIN [HCP_Live_db].[dbo].[LenderInfo] LI
ON LF.LenderID = LI.LenderID AND ISNULL(LI.Deleted_Ind, 0) = 0
AND LDI.DataInserted >= @firstDayOfQrtRecent
WHERE (@LenderId = -1 OR LF.LenderID = @LenderId)
ORDER BY Received
  
