﻿using System;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDUtilities
{
    public interface ILogger
    {
        void WriteLog(string logMessage);
    }

    public sealed class Logger : ILogger
    {
        //propeties required: ISTEXTLOGREQUIRED(if yes then only writes log), TXTLOGGERFILEPATH, TXTLOGGERFILENAME, ISPERDAYTXTLOG(suffix of date after file name)
        private static bool istxtlogrequired = false;
        private static string txtlogpath = string.Empty;
        private static string txtlogfilename = string.Empty;
        private static bool isdailylog = false;
        private static bool isreadytolog = false;//flag that checks and validates if log can be written or not or to discard the request

        //an dummy instance used to lock the thread/call to the method that writes an log for each of the individual call
        private static readonly object padlock = new object();
        private static readonly object writerlock = new object();

        //Our single instance of the singleton
        private static Logger instance = null;

        //Private constructor to deny instance creation of this class from outside
        private Logger()
        {
            //Note:all conditions has to satisfy then only isreadytolog = true
            //check and parse webconfig value for ISTEXTLOGREQUIRED
           
            if (ConfigurationManager.AppSettings["ISTEXTLOGREQUIRED"] != null
                && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ISTEXTLOGREQUIRED"].ToString()))
            {
                bool result;
                bool flag;
                result = Boolean.TryParse(ConfigurationManager.AppSettings["ISTEXTLOGREQUIRED"].ToString(), out flag);

                //if given value in webconfig is true/false then result=always true, but flag=webconfig value
                //ie if correct string is given as data, then result=true else resut=false. if result=false, webconfig value is null or wrong incompatible string value
                //flag = the value given in the webconfig after checking its authenticity

                if (result)
                {
                    istxtlogrequired = flag;

                    //authentication for rest of the fields, if any thing fails then the value is false and will no more write any logs
                    isreadytolog = true;
                }
                else
                    isreadytolog = false;
            }
            else
                isreadytolog = false;

            //logger file path
            if (istxtlogrequired && isreadytolog)
            {
                if (ConfigurationManager.AppSettings["TXTLOGGERFILEPATH"] != null
                && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TXTLOGGERFILEPATH"].ToString()))
                {
                    if (Directory.Exists(ConfigurationManager.AppSettings["TXTLOGGERFILEPATH"].ToString()))
                    {
                        txtlogpath = ConfigurationManager.AppSettings["TXTLOGGERFILEPATH"].ToString();

                        //authentication for rest of the fields, if any thing fails then the value is false and will no more write any logs
                        isreadytolog = true;
                    }
                    else
                        isreadytolog = false;
                }
            }

            //logger file name
            if (istxtlogrequired && isreadytolog)
            {
                if (ConfigurationManager.AppSettings["TXTLOGGERFILENAME"] != null
                && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TXTLOGGERFILENAME"].ToString()))
                {
                    if (ConfigurationManager.AppSettings["TXTLOGGERFILENAME"].ToString().ToUpper().EndsWith(".TXT".ToUpper()))
                    {
                        txtlogfilename = ConfigurationManager.AppSettings["TXTLOGGERFILENAME"].ToString();

                        //authentication for rest of the fields, if any thing fails then the value is false and will no more write any logs
                        isreadytolog = true;
                    }
                    else
                        isreadytolog = false;

                }
            }

            //decides if the log file is per day or a continous updated file
            if (istxtlogrequired && isreadytolog)
            {
                if (ConfigurationManager.AppSettings["ISPERDAYTXTLOG"] != null
                && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ISPERDAYTXTLOG"].ToString()))
                {
                    bool result;
                    bool flag;
                    result = Boolean.TryParse(ConfigurationManager.AppSettings["ISPERDAYTXTLOG"].ToString(), out flag);

                    if (result)
                    {
                        isdailylog = flag;

                        //authentication for rest of the fields, if any thing fails then the value is false and will no more write any logs
                        isreadytolog = true;
                    }
                    else
                        isreadytolog = false;

                }
            }

        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Logger();
                    }
                }
            }
            return instance;
        }

        public void WriteLog(string logMessage)
        {
            lock (padlock)
            {
                //if any of meta data is gone wrong... than cannot write the log, hence return
                if (!isreadytolog)
                    return;

                if (!istxtlogrequired)
                    return;

                string filename = getFileName(txtlogfilename, isdailylog);

                using (StreamWriter outputFile = new StreamWriter(Path.Combine(txtlogpath, filename), true))
                {
                    outputFile.WriteLine(logMessage);
                }

            }
        }

        private string getFileName(string filename, bool isdailylogtxt)
        {
            if (!isdailylogtxt)
                return filename;

            //if not
            int i = filename.LastIndexOf('.');
            string firstName = filename.Substring(0, i);
            string secondName = filename.Substring(i);

            //int day = DateTime.Now.Day;
            int day = DateTime.UtcNow.Day;
            string actualDay = string.Empty;

            if (!day.ToString().Length.Equals(2))
                actualDay = "0" + day.ToString();
            else
                actualDay = day.ToString();

            //int month = DateTime.Now.Month;
            int month = DateTime.UtcNow.Month;
            string actualMonth = string.Empty;

            if (!month.ToString().Length.Equals(2))
                actualMonth = "0" + month.ToString();
            else
                actualMonth = month.ToString();


            //string actualFileName = firstName + "_" + actualDay + actualMonth + DateTime.Now.Year + secondName;
            string actualFileName = firstName + "_" + actualDay + actualMonth + DateTime.UtcNow.Year + secondName;
            return actualFileName;
        }
    }

}
