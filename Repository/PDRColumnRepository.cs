﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces;

namespace Repository
{
   public class PDRColumnRepository:BaseRepository<PDRColumn>,IPDRColumnRepository
    {   
       public PDRColumnRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
       public PDRColumnRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<PDRColumn> DataToJoin
        {
            get { return DbQueryRoot; }
        }
        public int AddNewColumn(Model.PDRColumnViewModel model)
        {
            var pdrColumn = Mapper.Map<PDRColumn>(model);
            this.InsertNew(pdrColumn);
            UnitOfWork.Save();
           return pdrColumn.PDRColumnID;
        }

        public Model.PDRColumnViewModel GetColumnByID(int id)
        {
            var pdrColumn = this.GetByIDFast(id);
            return Mapper.Map<PDRColumn, PDRColumnViewModel>(pdrColumn);
        }

        public void UpdateColumn(Model.PDRColumnViewModel model)
        {
            var entityToModify = this.GetByIDFast(model.ColumnID);
            if (entityToModify != null)
            {
                entityToModify.PDRColumnName = model.ColumnName;
                this.Update(entityToModify);
            }
        }

        public IEnumerable<PDRColumnViewModel> GetAllPdrColumns()
        {
            var entitypdrColumns = this.GetAll();
            return Mapper.Map<IEnumerable<PDRColumnViewModel>>(entitypdrColumns);

        }
       
    }
}
