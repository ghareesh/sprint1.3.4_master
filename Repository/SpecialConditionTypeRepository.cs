﻿using HUDHealthcarePortal.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class SpecialConditionTypeRepository : BaseRepository<SpecialConditionType>, ISpecialConditionTypeRepository
	{
		public SpecialConditionTypeRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public SpecialConditionTypeRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<SpecialConditionType> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public List<SpecialConditionTypeModel> GetSpecialConditionTypes()
		{
			var specialConditionType = this.GetAll().ToList();
			return Mapper.Map<List<SpecialConditionTypeModel>>(specialConditionType);
		}

		public string GetSpecialConditionTypeName(int pId)
		{
			SpecialConditionType objSpecialConditionType = this.GetByID(pId);
			if (objSpecialConditionType != null)
				return objSpecialConditionType.SpecialCondition;
			return null;
		}

	}
}



