﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;
using TaskDB = EntityObject.Entities.HCP_task;
namespace HUDHealthcarePortal.Repository
{
    public class ReviewFileCommentRepository : BaseRepository<ReviewFileComment>, IReviewFileCommentRepository
    {
        public ReviewFileCommentRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

        public ReviewFileCommentRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public void SaveReviewFileComment(IList<ReviewFileCommentModel> reviewCommentList)
        {
            var context = (TaskDB.HCP_task)this.Context;
            foreach (var item in reviewCommentList)
            {
                if (!string.IsNullOrEmpty(item.Comment))
                {
                    var result = Mapper.Map<ReviewFileComment>(item);
                    this.InsertNew(result);
                }
            }
        }

    }
}

