﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class Prod_BorrowerTypeRepository : BaseRepository<Prod_BorrowerType>, IProd_BorrowerTypeRepository
    {
        public Prod_BorrowerTypeRepository()
            : base(new UnitOfWork(DBSource.Live))
        {

        }
        public Prod_BorrowerTypeRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IList<BorrowerTypeModel> GetAllBorrowerTypes()
        {
            var borrowerTypes = this.GetAll().ToList();
            return Mapper.Map<IList<Prod_BorrowerType>, IList<BorrowerTypeModel>>(borrowerTypes);
        }

        public string GetBorrowerTypeById(int borrowerTypeId)
        {
            return this.Find(m => m.BorrowerTypeId == borrowerTypeId).First().BorrowerTypeName;
        }
    }
}
