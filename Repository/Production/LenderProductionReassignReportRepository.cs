﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using AutoMapper;
using BusinessService.Interfaces.Production;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_live.Programmability;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;

using Repository.Interfaces;
using EntityObject.Entities.HCP_task;
using Repository.Interfaces.Production;
using AutoMapper;

namespace Repository.Production
{
    //class LenderProductionReassignReportRepository
    //{
    //}
    public class LenderProductionReassignReportRepository : BaseRepository<LenderProductionReassignViewModel>, ILenderProductionReassignReportRepository
    {

        //  private ProductionPAMReportModel _plmReportModel;

        const int TrueInt = 1;
        const int FalseInt = 0;


        public LenderProductionReassignReportRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
            // _plmReportModel = new ProductionPAMReportModel(ReportType.HudProductionReport);
        }

        public LenderProductionReassignReportRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

      

        // harish added 15042020 to get grid data 
        public LenderProductionReasignReportModel GetLenderProdReassignmentTasks(string appType, string programType, string lenderUserIds, DateTime? fromDate, DateTime? toDate)
        {
            if (fromDate == null && toDate == null)
            {
                //fromDate = DateTime.Now.AddYears(-1);
                fromDate = DateTime.UtcNow.AddYears(-1);
            }
            var reportModel = new LenderProductionReasignReportModel(ReportType.ProdTaskReassignment);
            try
            {

                var context = (HCP_task)this.Context;

                if (context == null)
                    throw new InvalidCastException(
                        "context is not from db Live in USP_GetProductionreassignment_task_externaluser_t, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                string Spname = string.Empty;

                //USP_GetProdReassignmentTasks_Hari18042020

                var results = context.Database.SqlQuerySimple<usp_HCP_GetProdPAMReportDetail_Result>("USP_GetProductionreassignment_task_externaluser_t",
                  new
                  {

                      AppType = string.IsNullOrEmpty(appType) ? null : appType,
                      ProgramType = string.IsNullOrEmpty(programType) ? null : programType,

                      Userlist = string.IsNullOrEmpty(lenderUserIds) ? null : lenderUserIds,

                      startTime = fromDate,
                      endTime = toDate

                  }).ToList();
                var Vm = Mapper.Map<IList<usp_HCP_GetProdPAMReportDetail_Result>, IList<LenderProductionReassignViewModel>>(results);
                reportModel.ReassignGridProductionlList = Vm;

            }
            catch (Exception ex)
            {

            }
            return reportModel;
        }




        // harisha added 21042020
        public void LenderReassignTasks(List<string> kvp, int AssignUserId)
        {
            var context = (HCP_task)this.Context;

            try
            {
                if (kvp != null)
                {

                    foreach (var item in kvp)
                    {
                        string[] pair = item.Split('|');

                        if (context == null)
                            throw new InvalidCastException("context is not from db live in InsertReviewerTaskFileData, please pass in correct context in unit of work.");
                        var results =
                            context.Database.ExecuteSqlCommandSimple(
                                "usp_Prod_UpdatesForTaskReassignment_externaluser",
                                new
                                {
                                    TaskInstanceId = new Guid(pair[0]),
                                    ViewId = int.Parse(pair[1]),
                                    ReassignedUserId = AssignUserId,
                                    UserId = int.Parse(pair[7])
                                });

                    }
                }
            }

            catch (Exception ex)
            {


            }


        }
    }
}
