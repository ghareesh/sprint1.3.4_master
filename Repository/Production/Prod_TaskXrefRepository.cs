﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Model.Production;
using Repository.Interfaces;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDB = EntityObject.Entities.HCP_task;

namespace Repository.Production
{
    public class Prod_TaskXrefRepository : BaseRepository<Prod_TaskXref>, IProd_TaskXrefRepository
    {

          public Prod_TaskXrefRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

          public Prod_TaskXrefRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        public Guid AddTaskXref(Prod_TaskXrefModel model)
        {
            var context = (HCP_task)this.Context;
            var taskxref = Mapper.Map<TaskDB.Prod_TaskXref>(model);
            this.InsertNew(taskxref);
            context.SaveChanges();
            return taskxref.TaskXrefid;
        }

        public bool UpdateTasXrefCompleteStatus(Guid taskXrefid,string comment)
        {
            try
            {
                var taskxref = this.Find(m => m.TaskXrefid == taskXrefid).FirstOrDefault();
                //var taskxref = Mapper.Map<TaskDB.Prod_TaskXref>(ProdXrefData);
              
                taskxref.Comments = comment;
                taskxref.CompletedOn = DateTime.UtcNow;
                taskxref.Status = (int)HUDHealthcarePortal.Core.TaskStep.ProjectActionRequestComplete;
                this.Update(taskxref);
                Context.SaveChanges();
                return true;
            }
           catch(Exception ex)
            {
                return false;
            }
        }

        public int GetCountOfReviewersTaskPendingForUW(Guid taskInstanceId)
        {
            var openReviewCount = this.Find(m => m.TaskInstanceId == taskInstanceId &&
                                            m.CompletedOn == null && 
                                            m.Status != (int) HUDHealthcarePortal.Core.TaskStep.ProjectActionRequestComplete &&
                                            m.ViewId != (int)ProductionView.UnderWriter).ToList().Count();

            return openReviewCount;
        }



        public List<Prod_TaskXrefModel> GetProductionSubTasks(Guid parentInstanceId)
        {
            var ChildProductionTasks= this.Find(m => m.TaskInstanceId == parentInstanceId).ToList();
            
          var result=  Mapper.Map<List<Prod_TaskXrefModel>>(ChildProductionTasks);
            return result;
        }

        public Guid AssignProductionFhaRequest(ProductionTaskAssignmentModel model)
        {   
            var fhaRequest = this.Find(m => m.TaskXrefid == model.TaskXrefid).FirstOrDefault();
            if (fhaRequest != null)
            {
                fhaRequest.AssignedBy = model.CurrentUserId;
                fhaRequest.AssignedTo = model.AssignedToUserId;
                fhaRequest.ModifiedBy = model.CurrentUserId;
                fhaRequest.Status = model.Status;
                //fhaRequest.ModifiedOn = DateTime.Now;
                fhaRequest.ModifiedOn = DateTime.UtcNow;
                this.Update(fhaRequest);
                UnitOfWork.Save();
                return fhaRequest.TaskInstanceId;

            }
            return Guid.Empty;
        }

        public int UpdateTaskXrefAndFHARequest(Guid taskInstanceId, int viewId, string comments, string fhaNumber, string portfolioName, int portfolioNumber)
        {

            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db task in usp_Prod_UpdateTaskXrefAndFHARequest, please pass in correct context in unit of work.");
            var returnCode = 0;
            returnCode = context.Database.ExecuteSqlCommandSimple(
                "usp_Prod_UpdateFHARequestAndTaskXref",
                new
                {
                    TaskInstanceId = taskInstanceId,
                    ViewId = viewId,
                    Comments = comments,
                    FhaNumber = fhaNumber,
                    PorfolioName = portfolioName,
                    PortfolioNumber = portfolioNumber
                });
            //if (returnCode != -1)
            //{
            //    throw new Exception("Error occured on executing the proc usp_Prod_UpdateTaskXrefAndFHARequest");
            //}
            return returnCode;
        }
        
        public Prod_TaskXrefModel GetProductionSubtaskById(Guid taskXrefId)
        {
            var xrefTask = this.Find(m => m.TaskXrefid == taskXrefId).FirstOrDefault();
            return Mapper.Map<Prod_TaskXrefModel>(xrefTask);
        }

        public void UpdateTaskXrefForFHARequestDeny(Guid taskInstanceId, string comments)
        {
            var xrefTasks = Find(m => m.TaskInstanceId == taskInstanceId).ToList();
            foreach (var item in xrefTasks)
            {
                item.Status = (int)ProductionFhaRequestStatus.Completed;
                item.Comments = comments;
                item.ModifiedOn = DateTime.UtcNow;
                item.ModifiedBy = UserPrincipal.Current.UserId;
                item.CompletedOn = DateTime.UtcNow;
                this.Update(item);
            }
            UnitOfWork.Save();
        }

        public void UpdateTaskXrefForFHARequestCancel(Guid taskInstanceId, string comments, int fhaRequestType)
        {
            var xrefTaskList = new List<Prod_TaskXref>();
            if (fhaRequestType == (int) ProductionView.CreditReview || fhaRequestType == (int) ProductionView.Portfolio)
            {
                var xrefTask = Find(m => m.TaskInstanceId == taskInstanceId && m.ViewId == fhaRequestType).FirstOrDefault();
                xrefTaskList.Add(xrefTask);
            }
            else if (fhaRequestType == (int)PageType.FhaRequest)
            {
                xrefTaskList = Find(m => m.TaskInstanceId == taskInstanceId).ToList();
            }
            foreach (var item in xrefTaskList)
            {
                if (item != null)
                {
                    item.Status = (int)ProductionFhaRequestStatus.Completed;
                    item.Comments = comments;
                    item.ModifiedOn = DateTime.UtcNow;
                    item.ModifiedBy = UserPrincipal.Current.UserId;
                    item.CompletedOn = DateTime.UtcNow;
                    this.Update(item);
                }
            }
            
            UnitOfWork.Save();
        }

        public bool UpdateTasXrefComment(Guid taskXrefid, string comment)
        {
            try
            {
                var taskxref = this.Find(m => m.TaskXrefid == taskXrefid).FirstOrDefault();
                //var taskxref = Mapper.Map<TaskDB.Prod_TaskXref>(ProdXrefData);

                taskxref.Comments = comment;
                //taskxref.ModifiedOn = DateTime.Now;
                taskxref.ModifiedOn = DateTime.UtcNow;
                this.Update(taskxref);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

		/// <summary>
		/// update record
		/// </summary>
		/// <param name="pProd_TaskXrefModel"></param>
		/// <returns></returns>
		public bool UpdateTasXref(Prod_TaskXrefModel pProd_TaskXrefModel)
		{
			try
			{
				var context = this.Context as HCP_task;
				var tblEnt = Mapper.Map<Prod_TaskXref>(pProd_TaskXrefModel);
				context.ProdTaskXrefs.Attach(tblEnt);
				context.Entry(tblEnt).State = System.Data.Entity.EntityState.Modified;
				Context.SaveChanges();
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}




		public List<Prod_TaskXrefModel> GetProductionSubTasksforIR(Guid parentInstanceId)
        {
             var context = this.Context as TaskDB.HCP_task;

             var ChildProductionTasks = (from xref in context.ProdTaskXrefs

                          join vw in context.Prod_View on xref.ViewId equals vw.ViewId
                          where xref.TaskInstanceId == parentInstanceId  && vw.ViewTypeId==1
                          select xref).ToList();

            var result = Mapper.Map<List<Prod_TaskXrefModel>>(ChildProductionTasks);
            return result;
        }

        public Guid GetUnderwriterTaskXrefIdByParentTaskInstanceId(Guid taskInstanceId)
        {
            var context = this.Context as TaskDB.HCP_task;
            var taskXref = (from xref in context.ProdTaskXrefs
                            where xref.TaskInstanceId == taskInstanceId && xref.ViewId == 1
                            select xref.TaskXrefid).FirstOrDefault();
            return taskXref;
        }

		public void UpdateTaskXrefForAmendments(Guid pTaskInstanceId, int pUserId)
		{
			var xrefAmendmentTasks = Find(m => m.TaskInstanceId == pTaskInstanceId).ToList();
			var objProd_TaskXref = xrefAmendmentTasks.Where(o => o.Status == Convert.ToInt32(ApplicationAndClosingType.Amendments)).OrderByDescending(o=>o.ModifiedOn).FirstOrDefault();
			objProd_TaskXref.AssignedTo = pUserId;
			objProd_TaskXref.ModifiedOn = DateTime.UtcNow;
			objProd_TaskXref.AssignedDate = DateTime.UtcNow;
			this.Update(objProd_TaskXref);			
			UnitOfWork.Save();
		}

		/// <summary>
		/// find amendmemt task for WLM signature is created already
		/// </summary>
		/// <param name="pTaskXrefid"></param>
		/// <returns></returns>
		public bool FindTasXrefForAmendmentWLMView(Guid pTaskXrefid)
		{
			try
			{
				var taskxref = this.Find(m => m.TaskXrefid == pTaskXrefid).FirstOrDefault();
				List<Prod_TaskXref> olistProd_TaskXref = this.Find(m => m.TaskInstanceId ==  taskxref.TaskInstanceId).ToList();
				List<Prod_TaskXref> clist =olistProd_TaskXref.Where(o => o.ViewId == Convert.ToInt32(ProductionView.WLM)).ToList();
				if (clist.Count == 0)
				{
					return true;
				}
				else
				{
					return false;
				}		
				
			}
			catch (Exception ex)
			{
				return true;
			}
			
		}
		/// <summary>
		/// Get respective TasXref ForAmendmentWLMView basedon previous taskref created by lender
		/// </summary>
		/// <param name="pTaskXrefid"></param>
		/// <returns></returns>
		public List<Guid> GetTasXrefForAmendmentWLMView(Guid pTaskXrefid)
		{
			List<Guid> olistTaskXrefids = new List<Guid>();
			try
			{
				var taskxref = this.Find(m => m.TaskXrefid == pTaskXrefid).FirstOrDefault();
				List<Prod_TaskXref> olistProd_TaskXref = this.Find(m => m.TaskInstanceId == taskxref.TaskInstanceId).ToList();
				//List<Prod_TaskXref> clist = olistProd_TaskXref.Where(o => o.ViewId == Convert.ToInt32(ProductionView.WLM)).ToList();
				//if (clist.Count == 0)
				//{
				//	return null;
				//}
				//else
				//{
				//	return clist.FirstOrDefault().TaskXrefid;
				//}
				olistTaskXrefids = olistProd_TaskXref.Select(l => l.TaskXrefid).ToList();
			}
			catch (Exception ex)
			{
				return null;
			}
			return olistTaskXrefids;
		}

		public int GetReviewerUserIdByTaskInstanceId(Guid taskInstanceId, int viewId)
		{
			var context = this.Context as TaskDB.HCP_task;
			var assignedTo = (from xref in context.ProdTaskXrefs
							  where xref.TaskInstanceId == taskInstanceId && xref.ViewId == viewId
							  select xref.AssignedTo).FirstOrDefault();
			return assignedTo ?? 0;
		}

		public int GetWLMUserIdByTaskInstanceId(Guid taskInstanceId)
		{
			var context = this.Context as TaskDB.HCP_task;
			var assignedBy = (from xref in context.ProdTaskXrefs
							  where xref.TaskInstanceId == taskInstanceId && xref.ViewId == 1
							  select xref.AssignedBy).FirstOrDefault();
			return assignedBy ?? 0;
		}
	}
	

       
    
}
