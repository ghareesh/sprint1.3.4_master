﻿using Model.Production;
using Newtonsoft.Json;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Production
{
    public class Prod_RestfulWebApiUpdateRepository : IProd_RestfulWebApiUpdateRepository
    {
        public Prod_RestfulWebApiUpdateRepository()
        {

        }
        public RestfulWebApiResultModel UpdateDocumentInfo(RestfulWebApiUpdateModel fileInfo, string token)
        {
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel rst = new RestfulWebApiResultModel();
            string URL = ConfigurationManager.AppSettings["UpdateDocument"].ToString() + token;
            using (var content = new MultipartFormDataContent())
            {
                var formData = new[]
                    {
                          new KeyValuePair<string, string>("propertyID", fileInfo.propertyId),
                          new KeyValuePair<string, string>("documentType", fileInfo.documentType),
                          new KeyValuePair<string, string>("indexType", fileInfo.indexType),
                          new KeyValuePair<string, string>("indexValue", fileInfo.indexValue),
                          new KeyValuePair<string, string>("documentStatus", fileInfo.documentStatus),
                          new KeyValuePair<string, string>("folderName", ""),
                          new KeyValuePair<string, string>("docId", fileInfo.docId),
               
                    };
                foreach (var KeyValuePair in formData)
                {
                    if (!string.IsNullOrEmpty(KeyValuePair.Value))
                    {
                        content.Add(new StringContent(KeyValuePair.Value.ToString()), KeyValuePair.Key.ToString());
                    }
                    else 
                    {
                        content.Add(new StringContent(""), KeyValuePair.Key.ToString());
                    }
                   
                }
                rst = helper.UpdateReplacePostRequest(URL, content);
                rst.documentType = fileInfo.documentType;
                return rst;

            }
        }

        // harish added new method to update folder structure in TA 30-01-2020
        public RestfulWebApiResultModel UpdateFolderDocumentInfo(RestfulWebApiUpdateModel[] fileInfo, string token)
        {
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel rst = new RestfulWebApiResultModel();
            string URL = ConfigurationManager.AppSettings["UpdateDocumentFolder"].ToString() + token;

            MultipartFormDataContent[] mfdc = new MultipartFormDataContent[1];
            MultipartFormDataContent cont1 = new MultipartFormDataContent();

            var formData = new[]
                    {
                          new KeyValuePair<string, string>("propertyID", fileInfo[0].propertyId),
                          new KeyValuePair<string, string>("documentType", fileInfo[0].documentType),
                          new KeyValuePair<string, string>("indexType", fileInfo[0].indexType),
                          new KeyValuePair<string, string>("indexValue", fileInfo[0].indexValue),
                          new KeyValuePair<string, string>("documentStatus", fileInfo[0].documentStatus),
                            new KeyValuePair<string, string>("folderName", fileInfo[0].folderName),
                          new KeyValuePair<string, string>("transactionId", fileInfo[0].transactionId),
                          new KeyValuePair<string, string>("docId", fileInfo[0].docId),

                    };
            foreach (var KeyValuePair in formData)
            {
                if (!string.IsNullOrEmpty(KeyValuePair.Value))
                {
                    // mfdc[0].Add(new StringContent(KeyValuePair.Value.ToString()), KeyValuePair.Key.ToString());
                    cont1.Add(new StringContent(KeyValuePair.Value.ToString()), KeyValuePair.Key.ToString());
                }
                else
                {
                    cont1.Add(new StringContent(""), KeyValuePair.Key.ToString());
                    // mfdc[0].Add(new StringContent(""), KeyValuePair.Key.ToString());
                }

            }

            mfdc[0] = cont1;

            MultipartFormDataContent cont2 = mfdc[0];

            rst = helper.UpdateFolderPostRequest(URL, fileInfo);
            rst.documentType = fileInfo[0].documentType;
            return rst;

        }

    }
}
