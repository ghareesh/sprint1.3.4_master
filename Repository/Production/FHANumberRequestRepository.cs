﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using EntityObject.Entities.HCP_live.Programmability;
using System.Data.Entity;

namespace Repository.Production
{
    public class FHANumberRequestRepository : BaseRepository<Prod_FHANumberRequest>, IFHANumberRequestRepository
    {
        public FHANumberRequestRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public FHANumberRequestRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public Guid AddFHARequest(FHANumberRequestViewModel model)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in AddFHARequest, please pass in correct context in unit of work.");

            var fhaRequest = Mapper.Map<FHANumberRequestViewModel, Prod_FHANumberRequest>(model);
            fhaRequest.CreatedBy = UserPrincipal.Current.UserId;
            fhaRequest.CreatedOn = DateTime.UtcNow;
            fhaRequest.ModifiedBy = UserPrincipal.Current.UserId;
            fhaRequest.ModifiedOn = DateTime.UtcNow;
            fhaRequest.FHANumberRequestId = Guid.NewGuid();
            //fhaRequest.IsLIHTC = false;
            this.InsertNew(fhaRequest);

            context.SaveChanges();
            return fhaRequest.FHANumberRequestId;
        }

        //karri#672
        public IEnumerable<AssignedunderwriternamesModel> GetprodAssignedUnderwriternames()
        {

            var context = this.Context as HCP_live;
            var tasks =
            context.Database.SqlQuerySimple<AssignedunderwriternamesModel>(
                "USP_Get_prodqueueassigned_underwriternames");
            return Mapper.Map<IEnumerable<AssignedunderwriternamesModel>>(tasks);

        }
        public bool UpdateFHARequest(FHANumberRequestViewModel model)
        {
            var fhaRequest = Find(p => p.TaskinstanceId == model.TaskinstanceId).First();
            if (fhaRequest != null)
            {
                fhaRequest.FHANumber = model.FHANumber;
                fhaRequest.ProjectName = model.ProjectName;
                fhaRequest.ProjectTypeId = model.ProjectTypeId;
                fhaRequest.CurrentLoanTypeId = (int)model.CurrentLoanTypeId;
                fhaRequest.ActivityTypeId = (int)model.ActivityTypeId;
                fhaRequest.LoanTypeId = model.LoanTypeId;
                fhaRequest.BorrowerTypeId = model.BorrowerTypeId;
                fhaRequest.IsAddressCorrect = model.IsAddressCorrect;
                fhaRequest.CongressionalDtId = model.CongressionalDtId;
                fhaRequest.LenderContactName = model.LenderContactName;
                fhaRequest.LoanAmount = model.LoanAmount;
                fhaRequest.SkilledNursingBeds = model.SkilledNursingBeds;
                fhaRequest.SkilledNursingUnits = model.SkilledNursingUnits;
                fhaRequest.AssistedLivingBeds = model.AssistedLivingBeds;
                fhaRequest.AssistedLivingUnits = model.AssistedLivingUnits;
                fhaRequest.BoardCareBeds = model.BoardCareBeds;
                fhaRequest.BoardCareUnits = model.BoardCareUnits;
                fhaRequest.MemoryCareBeds = model.MemoryCareBeds;
                fhaRequest.MemoryCareUnits = model.MemoryCareUnits;
                fhaRequest.IndependentBeds = model.IndependentBeds;
                fhaRequest.IndependentUnits = model.IndependentUnits;
                fhaRequest.OtherBeds = model.OtherBeds;
                fhaRequest.OtherUnits = model.OtherUnits;
                fhaRequest.IsMidLargePortfolio = model.IsMidLargePortfolio;
                fhaRequest.IsMasterLeaseProposed = model.IsMasterLeaseProposed;
                fhaRequest.Portfolio_Name = model.Portfolio_Name;
                fhaRequest.Portfolio_Number = (int)model.Portfolio_Number;
                fhaRequest.Comments = model.Comments;
                fhaRequest.ModifiedBy = UserPrincipal.Current.UserId;
                fhaRequest.ModifiedOn = DateTime.UtcNow;
                fhaRequest.ConstructionStageTypeId = model.ConstructionStageTypeId;
                fhaRequest.CMSStarRating = (int)model.CMSStarRating;
                fhaRequest.ProposedInterestRate = model.ProposedInterestRate;
                fhaRequest.IsNewOrExistOrNAPortfolio = model.IsNewOrExistOrNAPortfolio;
                fhaRequest.IsPortfolioRequired = model.IsPortfolioRequired;
                fhaRequest.IsCreditReviewRequired = model.IsCreditReviewRequired;
                fhaRequest.RequestStatus = model.RequestStatus;
                fhaRequest.LenderContactAddressId = model.LenderContactAddressId;
                fhaRequest.PropertyAddressId = model.PropertyAddressId;
                fhaRequest.IsLIHTC = model.IsLIHTC;
                fhaRequest.PropertyId = (int)model.PropertyId;
                fhaRequest.CurrentFHANumber = model.CurrentFHANumber;
                fhaRequest.RequestSubmitDate = model.RequestSubmitDate;
                fhaRequest.DenyComments = model.DenyComments;
                fhaRequest.LenderContactEmail = model.LenderContactEmail;
                fhaRequest.LenderContactPhone = model.LenderContactPhone;
                fhaRequest.IsReadyForApplication = model.IsReadyForApplication;
                fhaRequest.IsPortfolioComplete = model.IsPortfolioComplete;
                fhaRequest.IsFhaInsertComplete = model.IsFhaInsertComplete;
                fhaRequest.IsCreditReviewAttachComplete = model.IsCreditReviewAttachComplete;
                fhaRequest.CancelComments = model.CancelComments;
                fhaRequest.IsPortfolioCancelled = model.IsPortfolioCancelled;
                fhaRequest.IsCreditCancelled = model.IsCreditCancelled;

                this.Update(fhaRequest);
                Context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool FhataskAssignedfromQueDateUpdate(Guid TaskinstanceId)
        {
            try
            {
                var fhaRequest = Find(p => p.TaskinstanceId == TaskinstanceId).First();
                fhaRequest.AssignedDate = DateTime.UtcNow;
                this.Update(fhaRequest);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }

        public IEnumerable<ProductionQueueLenderInfo> GetFhaRequests(List<Guid> taskInstances)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_Prod_GetSubmittedFhaRequests, please pass in correct context in unit of work.");
            var instances = string.Join(",", taskInstances);
            var results = context.Database.SqlQuerySimple<ProductionQueueLenderInfo>("usp_HCP_Prod_GetSubmittedFhaRequests").ToList();

            var fhaRequest = (from result in results
                              select new ProductionQueueLenderInfo()
                              {
                                  PropertyName = result.projectName,
                                  Portfolio_Name = result.Portfolio_Name,
                                  opportunity_zone = result.opportunity_zone,//naresh prodqueue phase 2
                                  LenderName = result.LenderName,
                                  projectName = result.projectName,
                                  CreatedBy = result.CreatedBy,
                                  ModifiedOn = result.ModifiedOn,
                                  TaskinstanceId = result.TaskinstanceId,
                                  IsLIHTC = result.IsLIHTC,
                                  LoanType = result.LoanType,
                                  LoanAmount = result.LoanAmount
                              }).OrderByDescending(n => n.ModifiedOn).OrderByDescending(o => o.IsLIHTC);
            return fhaRequest;
        }

        public FHANumberRequestViewModel GetFhaRequestById(Guid instanceId)
        {
            var fhaRequest = this.Find(m => m.FHANumberRequestId == instanceId).FirstOrDefault();
            var result = new FHANumberRequestViewModel
            {
                Comments = fhaRequest.Comments,
                InsertFHAComments = fhaRequest.InsertFHAComments,
                CreditreviewComments = fhaRequest.CreditreviewComments,
                PortfolioComments = fhaRequest.PortfolioComments,
                LenderName = fhaRequest.LenderContactName,
                LenderId = fhaRequest.LenderId,
                FHANumberRequestId = fhaRequest.FHANumberRequestId,
                TaskinstanceId = fhaRequest.TaskinstanceId

            };
            return result;

        }


        public List<string> GetReadyforAppFhas()
        {
            var context = this.Context as HCP_live;
            //Naveen 24-10-2019 added else part and if cond
            if (!UserPrincipal.Current.UserRole.Contains("Servicer"))
            {
                //var results = (from a in context.Prod_FHANumberRequest

                //               where a.IsReadyForApplication == true && a.LenderId == UserPrincipal.Current.LenderId && a.ConstructionStageTypeId == 0 && DbFunctions.DiffDays(a.ModifiedOn, DateTime.Now) <= 365
                //               select a.FHANumber).Distinct().ToList();
                var results = (from a in context.Prod_FHANumberRequest

                               where a.IsReadyForApplication == true && a.LenderId == UserPrincipal.Current.LenderId && a.ConstructionStageTypeId == 0 && DbFunctions.DiffDays(a.ModifiedOn, DateTime.UtcNow) <= 365
                               select a.FHANumber).Distinct().ToList();
                var nxtstageresult = (from nxt in context.Prod_NextStages

                                      where nxt.NextPgTypeId == 5 && nxt.NextPgTaskInstanceId != null
                                      select nxt.FhaNumber).Distinct().ToList();
                var result = results.Except(nxtstageresult).ToList();
                return result;
            }
            else
            {
                //var results = (from a in context.Prod_FHANumberRequest
                //               join b in context.User_Lender.Where(x => x.User_ID == UserPrincipal.Current.UserId) on a.FHANumber equals b.FHANumber
                //               where a.IsReadyForApplication == true && a.LenderId == b.Lender_ID && a.ConstructionStageTypeId == 0 && DbFunctions.DiffDays(a.ModifiedOn, DateTime.Now) <= 365
                //               select b.FHANumber).Distinct().ToList();
                var results = (from a in context.Prod_FHANumberRequest
                               join b in context.User_Lender.Where(x => x.User_ID == UserPrincipal.Current.UserId) on a.FHANumber equals b.FHANumber
                               where a.IsReadyForApplication == true && a.LenderId == b.Lender_ID && a.ConstructionStageTypeId == 0 && DbFunctions.DiffDays(a.ModifiedOn, DateTime.UtcNow) <= 365
                               select b.FHANumber).Distinct().ToList();
                var nxtstageresult = (from nxt in context.Prod_NextStages

                                      where nxt.NextPgTypeId == 5 && nxt.NextPgTaskInstanceId != null
                                      select nxt.FhaNumber).Distinct().ToList();
                var result = results.Except(nxtstageresult).ToList();
                return result;
            }


        }

        public List<string> GetReadyforConSingleStageFhas()
        {
            var context = this.Context as HCP_live;
            //Naveen 24-10-2019 added else part and if cond
            if (!UserPrincipal.Current.UserRole.Contains("Servicer"))
            {
                var results = (from a in context.Prod_FHANumberRequest

                               where a.IsReadyForApplication == true && a.LenderId == UserPrincipal.Current.LenderId && a.ConstructionStageTypeId == 1
                               select a.FHANumber).Distinct().ToList();
                var nxtstageresult = (from nxt in context.Prod_NextStages

                                      where nxt.NextPgTypeId == 6 && nxt.NextPgTaskInstanceId != null
                                      select nxt.FhaNumber).Distinct().ToList();
                var result = results.Except(nxtstageresult).ToList();

                return result;
            }
            else
            {
                var results = (from a in context.Prod_FHANumberRequest
                               join b in context.User_Lender.Where(x => x.User_ID == UserPrincipal.Current.UserId) on a.FHANumber equals b.FHANumber
                               where a.IsReadyForApplication == true && a.LenderId == b.Lender_ID && a.ConstructionStageTypeId == 1
                               select a.FHANumber).Distinct().ToList();
                var nxtstageresult = (from nxt in context.Prod_NextStages

                                      where nxt.NextPgTypeId == 6 && nxt.NextPgTaskInstanceId != null
                                      select nxt.FhaNumber).Distinct().ToList();
                var result = results.Except(nxtstageresult).ToList();

                return result;
            }

        }
        public List<string> GetReadyforConTwoStageFhas()
        {
            var context = this.Context as HCP_live;
            //Naveen 24-10-2019 added else part and if cond
            if (!UserPrincipal.Current.UserRole.Contains("Servicer"))
            {
                var results = (from a in context.Prod_FHANumberRequest

                               where a.IsReadyForApplication == true && a.LenderId == UserPrincipal.Current.LenderId && a.ConstructionStageTypeId == 2
                               select a.FHANumber).Distinct().ToList();
                return results;
            }
            else
            {
                var results = (from a in context.Prod_FHANumberRequest
                               join b in context.User_Lender.Where(x => x.User_ID == UserPrincipal.Current.UserId) on a.FHANumber equals b.FHANumber
                               where a.IsReadyForApplication == true && a.LenderId == b.Lender_ID && a.ConstructionStageTypeId == 2
                               select a.FHANumber).Distinct().ToList();
                return results;
            }

        }

        public FHANumberRequestViewModel GetFhaRequestByTaskInstanceId(Guid taskInstanceId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_Prod_GetFhaRequestByTaskInstanceId, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<FHANumberRequestViewModel>("usp_HCP_Prod_GetFhaRequestByTaskInstanceId",
                new
                {
                    TaskInstanceId = taskInstanceId
                }).First();
            return results;
        }
        public IEnumerable<FhaSubmittedLendersModel> GetFhaSubmittedLenders()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_Prod_GetFhaSubmittedLenders, please pass in correct context in unit of work.");
            var lenders = Mapper.Map<IEnumerable<FhaSubmittedLendersModel>>(context.Database.SqlQuerySimple<usp_HCP_Prod_GetFhaSubmittedLenders>("usp_HCP_Prod_GetFhaSubmittedLenders")).ToList();

            return lenders;
        }

        //karri:D#672;changed the DBSP
        public ApplicationDetailViewModel GetApplicationDetailsForSharePointScreen(Guid taskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in usp_HCP_Prod_GetApplicationDetailsForSharePointScreen, please pass in correct context in unit of work.");
                var results = context.Database.SqlQuerySimple<FHANumberRequestViewModel>("usp_HCP_Prod_GetApplicationDetailsForSharePointScreen",
                     new
                     {
                         TaskInstanceId = taskInstanceId
                     }).FirstOrDefault();

                return Mapper.Map<FHANumberRequestViewModel, ApplicationDetailViewModel>(results);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //karri#672
        //public bool UpdateLoanAmount(string fhaNumber, decimal loanAmount)
        //{
        //    var fhaRequest = this.Find(m => m.FHANumber == fhaNumber).FirstOrDefault();
        //    if (fhaRequest != null)
        //    {
        //        fhaRequest.LoanAmount = loanAmount;
        //        this.Update(fhaRequest);
        //        UnitOfWork.Save();
        //        return true;
        //    }
        //    return false;
        //}
        //karri#672
        public bool UpdateLoanAmount(string fhaNumber, decimal loanAmount, string projName, string origFHANumber, string origProjName, decimal origLoanAmount)
        {
            try
            {
                if (loanAmount.Equals(origLoanAmount))
                    loanAmount = 0;

                if (fhaNumber.Equals(origFHANumber))
                    fhaNumber = null;

                if (projName.Equals(origProjName))
                    projName = null;

                var context = this.Context as HCP_live;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in usp_Set_FHANumberReqRepo_ProjDetScreenUpdate, please pass in correct context in unit of work.");
                var results = context.Database.SqlQuerySimple<FHANumberRequestViewModel>("usp_Set_FHANumberReqRepo_ProjDetScreenUpdate",
                     new
                     {
                         FHANumber = origFHANumber,
                         NewFHANumber = fhaNumber,
                         OldProjectName = origProjName,
                         ProjectName = projName,
                         OldLoanAmount = origLoanAmount,
                         LoanAmount = loanAmount,
                         UserID = UserPrincipal.Current.UserId
                     }).FirstOrDefault();

                return true;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            return false;
        }

        public bool IsRequestExistsForFhaNumber(string fhaNumber)
        {
            try
            {
                var fhaRequest = this.Find(m => m.FHANumber == fhaNumber).FirstOrDefault();
                return fhaRequest != null;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public IList<Prod_FhaAgingReportViewModel> GetFhaAgingReport()
        {
            try
            {
                var context = this.Context as HCP_live;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in usp_HCP_Prod_GetAgingFHAReport, please pass in correct context in unit of work.");
                var results = context.Database.SqlQuerySimple<Prod_FhaAgingReportViewModel>("usp_HCP_Prod_GetAgingFHAReport",
                     new
                     {

                     }).ToList();

                return results;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public FHANumberRequestViewModel GetFhaRequestByFhaNumber(string fhaNumber)
        {
            var result = this.Find(m => m.FHANumber == fhaNumber).FirstOrDefault();
            FHANumberRequestViewModel fhaNumberRequest = new FHANumberRequestViewModel();
            if (result != null)
            {
                fhaNumberRequest = new FHANumberRequestViewModel
                {
                    TaskinstanceId = result.TaskinstanceId,
                    ProjectName = result.ProjectName,
                    FHANumber = result.FHANumber,
                    ProjectTypeId = result.ProjectTypeId,
                    BorrowerTypeId = result.BorrowerTypeId,
                    LoanAmount = result.LoanAmount,
                    LenderId = result.LenderId,
                    LenderContactName = result.LenderContactName
                };
            }
            return fhaNumberRequest;
        }

        public IList<FHANumberRequestViewModel> GetAllFHARequests()
        {
            var results = Mapper.Map<IEnumerable<FHANumberRequestViewModel>>(this.GetAll()).ToList();
            IList<FHANumberRequestViewModel> fhaRequestList = new List<FHANumberRequestViewModel>();
            foreach (var item in results)
            {
                fhaRequestList.Add(new FHANumberRequestViewModel
                {
                    TaskinstanceId = item.TaskinstanceId,
                    FHANumber = item.FHANumber,
                    ProjectName = ""
                });
            }
            return fhaRequestList;
        }
    }
}
