﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Model.Production;
using Repository.Interfaces;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDB = EntityObject.Entities.HCP_task;


namespace Repository.Production
{

    public class Prod_ViewRepository : BaseRepository<TaskDB.Prod_View>, IProd_ViewRepository
    {
        public Prod_ViewRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

        public Prod_ViewRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public List<Prod_ViewModel> GetViewsbyType(int TypeID)
        {
            var context = this.Context as TaskDB.HCP_task;


            var UL = (from n in context.Prod_View where n.ViewTypeId == TypeID select n).ToList();

            return Mapper.Map<List<Prod_View>, List<Prod_ViewModel>>(UL);
        }



        public List<Prod_ViewModel> GetUnAssignedViewsbyTaskInstanceId(int TypeID, Guid taskinstanceid)
        {
            var context = this.Context as TaskDB.HCP_task;

           var query =
              ( from c in context.Prod_View
               where !(from o in context.ProdTaskXrefs
                       where o.TaskInstanceId == taskinstanceid
                       select o.ViewId)
                      .Contains(c.ViewId)
               select c).ToList().Where(p=>p.ViewTypeId==TypeID);
        
           return Mapper.Map<List<Prod_View>, List<Prod_ViewModel>>(query.ToList());
            //var prodviewlist = (from n in context.Prod_View where n.ViewTypeId == TypeID select n).ToList();
            //var Xreflist = (from xref in context.ProdTaskXrefs
            //                where xref.TaskInstanceId == taskinstanceid
            //                select xref.ViewId);
            //if (Xreflist != null)
            //{
            //    return Mapper.Map<List<Prod_View>, List<Prod_ViewModel>>(prodviewlist.Where(v => !Xreflist.Contains(v.ViewId)).ToList());
            //}
            //else
            //{
            //    return Mapper.Map<List<Prod_View>, List<Prod_ViewModel>>(prodviewlist);
            //}
           

        }
    }
}
