﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using TaskDB = EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Model;
using EntityObject.Entities.HCP_task.Programmability;

namespace Repository.Production
{
    public class Prod_GroupTasksRepository : BaseRepository<Prod_GroupTasks>, IProd_GroupTasksRepository
    {
       
        public Prod_GroupTasksRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

        public Prod_GroupTasksRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public Guid AddProdGroupTasks(Prod_GroupTasksModel model)
        {
            try
            {
            var context = (HCP_task)this.Context;
            var groupTask = Mapper.Map<Prod_GroupTasks>(model);
            this.InsertNew(groupTask);
            context.SaveChanges();
            return groupTask.TaskInstanceId;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            return new Guid();
        }

        public void UpdateGroupTask(Prod_GroupTasksModel model)
        {
            var groupTask = Find(p => p.TaskInstanceId == model.TaskInstanceId).FirstOrDefault();
            if (groupTask != null)
            {
                groupTask.InUse = model.InUse;
                groupTask.IsDisclaimerAccepted = model.IsDisclaimerAccepted;
                groupTask.RequestStatus = model.RequestStatus;
                groupTask.ModifiedOn = model.ModifiedOn;
                groupTask.ModifiedBy = model.ModifiedBy;
                this.Update(groupTask);
                Context.SaveChanges();
            }
        }

        public bool IsGroupTaskAvailable(Guid taskInstanceId)
        {
            var result = this.Find(p => p.TaskInstanceId == taskInstanceId).First();
            return result != null;
        }
       
        public Prod_GroupTasksModel GetGroupTaskByTaskInstanceId(Guid taskInstanceId)
        {
            var result = this.Find(p => p.TaskInstanceId == taskInstanceId).First();
            return Mapper.Map<Prod_GroupTasksModel>(result);
        }


        public Prod_GroupTasksModel GetGroupTaskAByTaskInstanceId(Guid TaskInstanceId)
        {
            var context = (HCP_task)this.Context;
            var Prodgrouptask = (from n in context.Prod_GroupTasks where n.TaskInstanceId == TaskInstanceId select n).FirstOrDefault();
            // var Prodgrouptask = this.Find(m => m.TaskInstanceId == TaskInstanceId).FirstOrDefault();
            return Mapper.Map<Prod_GroupTasksModel>(Prodgrouptask);
        }


        public void UpdateProdGroupTask(OPAViewModel AppProcessModel)
        {
        
            var context = (HCP_task)this.Context;
            var groupTask = (from g in context.Prod_GroupTasks
                             where g.TaskInstanceId == AppProcessModel.GroupTaskInstanceId
                             select g).FirstOrDefault();
            if (groupTask != null)
            {
                groupTask.RequestStatus = AppProcessModel.RequestStatus;

                groupTask.ServicerComments = AppProcessModel.ServicerComments;
                groupTask.ModifiedBy = UserPrincipal.Current.UserId;
                groupTask.IsDisclaimerAccepted = AppProcessModel.IsAgreementAccepted;
                groupTask.InUse = UserPrincipal.Current.UserId;
                this.Update(groupTask);
            }
            context.SaveChanges();
        }


        public void Checkin(OPAViewModel AppProcessModel)
        {
            var context = (HCP_task)this.Context;
            var groupTask = (from g in context.Prod_GroupTasks
                             where g.TaskInstanceId == AppProcessModel.GroupTaskInstanceId
                             select g).FirstOrDefault();
            if (groupTask != null)
            {
                groupTask.RequestStatus = AppProcessModel.RequestStatus;

                groupTask.ServicerComments = AppProcessModel.ServicerComments;
                groupTask.ModifiedBy = UserPrincipal.Current.UserId;
                groupTask.IsDisclaimerAccepted = AppProcessModel.IsAgreementAccepted;
                groupTask.InUse =0;
                this.Update(groupTask);
            }
            context.SaveChanges();
        }


       

        public List<Prod_GroupTasksModel> GetGroupTask(int userid)
        {
            try
            {
                var context = this.Context as HCP_task;
            
                if (context == null)
                    throw new InvalidCastException("context is not from db Live in USP_GetGroupTasksbyUser, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var grouptaskmodel = new List<Prod_GroupTasksModel>();
                var results = context.Database.SqlQuerySimple<USP_GetGroupTasksbyUser_Result>(
                   "USP_GetGroupTasksbyUser_new",
                   new
                   {
                       UserId = userid,
                       LenderId=UserPrincipal.Current.LenderId,
                       rolename = UserPrincipal.Current.UserRole
                   }).ToList();

                grouptaskmodel = Mapper.Map<IEnumerable<USP_GetGroupTasksbyUser_Result>, IEnumerable<Prod_GroupTasksModel>>(results).ToList();
                return grouptaskmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }


        public void UnlockGroupTask(int taskId)
        {
            var context = (HCP_task)this.Context;
            var groupTask = (from g in context.Prod_GroupTasks
                             where g.TaskId == taskId
                             select g).FirstOrDefault();
            groupTask.InUse = 0;
            groupTask.ModifiedBy = UserPrincipal.Current.UserId;
            groupTask.ModifiedOn = DateTime.UtcNow;
            this.Update(groupTask);
            context.SaveChanges();
        }

        public Prod_GroupTasksModel GetGroupTaskAById(int TaskId)
        {
            var context = (HCP_task)this.Context;
            var Prodgrouptask = this.Find(m => m.TaskId == TaskId).FirstOrDefault();
            return Mapper.Map<Prod_GroupTasksModel>(Prodgrouptask);
        }
    }
}
