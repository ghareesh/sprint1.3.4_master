﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class Prod_ProjectTypeRepository : BaseRepository<Prod_ProjectType>, IProd_ProjectTypeRepository
    {

        public Prod_ProjectTypeRepository()
            : base(new UnitOfWork(DBSource.Live))
        {

        }
        public Prod_ProjectTypeRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IList<ProjectTypeModel> GetAllProjectTypesForLC()
        {
            var context = (HCP_live)this.Context;
            var projectTypes = (from PL in context.Prod_LoanCommittee
                                join PT in context.Prod_ProjectType on PL.ProjectType equals PT.ProjectTypeId
                                select PT).ToList();
            //= this.GetAll().ToList().Where(a => a.IsotherloanType == false).OrderBy(a => a.ProjectTypeName).ToList();
            return Mapper.Map<IList<Prod_ProjectType>, IList<ProjectTypeModel>>(projectTypes);
        }

        public IList<ProjectTypeModel> GetAllProjectTypes()
        {
            var projectTypes = this.GetAll().ToList().Where(a => a.IsotherloanType == false).OrderBy(a => a.ProjectTypeName).ToList();
            return Mapper.Map<IList<Prod_ProjectType>, IList<ProjectTypeModel>>(projectTypes);
        }

        public string GetUserInfoByID(int UserID)
        {
            var context = (HCP_live)this.Context;
            var UserName = (from p in context.HCP_Authentication
                            where p.UserID == UserID
                            select p.UserName).FirstOrDefault();

            return UserName;
        }
        public IList<ProjectTypeModel> GetProjectTypesByAppType(string pageTypeIds)
        {
            String[] arrTypeIds = !string.IsNullOrEmpty(pageTypeIds) ? pageTypeIds.Split(',') : new string[] { };
            int[] n1 = new int[arrTypeIds.Length];
            for (int n = 0; n < arrTypeIds.Length; n++)
            {
                n1[n] = int.Parse(arrTypeIds[n]);
            }

            var context = (HCP_live)this.Context;
            var projectTypes = (from p in context.Prod_ProjectType
                                where n1.Contains(p.PageTypeId)
                                select p);

            return Mapper.Map<IList<Prod_ProjectType>, IList<ProjectTypeModel>>(projectTypes.ToList());
        }


        public string GetProjectTypebyID(int projecttypeid)
        {
            var context = (HCP_live)this.Context;
            return (from p in context.Prod_ProjectType
                    where p.ProjectTypeId == projecttypeid
                    select p).FirstOrDefault().ProjectTypeName;
        }


        public IList<ProjectTypeModel> GetAllotherloanTypes()
        {
            var projectTypes = this.GetAll().ToList().Where(a => a.IsotherloanType == true).ToList();
            return Mapper.Map<IList<Prod_ProjectType>, IList<ProjectTypeModel>>(projectTypes);
        }

        public int GetProjectTypebyName(string projectTypeName)
        {
            var context = (HCP_live)this.Context;
            return (from p in context.Prod_ProjectType
                    where p.ProjectTypeName == projectTypeName
                    select p).FirstOrDefault().ProjectTypeId;
        }

        //harish added 13-12-2019
        public string GetProjectActionTypebyID(int projecttypeid)
        {
            var context = (HCP_live)this.Context;
            return (from p in context.HCP_Project_Action
                    where p.ProjectActionID == projecttypeid
                    select p).FirstOrDefault().ProjectActionName;
        }
    }
}
