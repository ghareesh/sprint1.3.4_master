﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.Interfaces;
using Model.Production;
using System.IO;
using System.Net.Http;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using Repository.Interfaces.Production;
using System.Configuration;
using System.Net.Http.Headers;
namespace Repository.Production
{
    public class Prod_RestfulWebApiUploadRepository : IProd_RestfulWebApiUploadRepository
    {

        private IDocumentTypeRespository doumentTypeRepostory;
        public Prod_RestfulWebApiUploadRepository()
            : this(
                new DocumentTypeRepository())
        {

        }
        private Prod_RestfulWebApiUploadRepository(IDocumentTypeRespository _doumentTypeRepostory)
        {
            doumentTypeRepostory = _doumentTypeRepostory;
        }
        public RestfulWebApiResultModel UploadDocumentUsingWebApi(RestfulWebApiUploadModel DocumentInfo, string token, HttpPostedFileBase UplodFile, string requestType)
        {

            string url = ConfigurationManager.AppSettings["AddDocuments"].ToString() + token;
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            result = clientupload(DocumentInfo, UplodFile, url, requestType);
            return result;

        }
        //harish changed model to array class 04032020
        public RestfulWebApiResultModel AssetManagementUploadDocumentUsingWebApi(RestfulWebApiUploadModel DocumentInfo, string token, HttpPostedFileBase UplodFile, string requestType)
        {

            string url = ConfigurationManager.AppSettings["addAssetManagementDocuments"].ToString() + token;
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            result = AssetManagementClientUpload(DocumentInfo, UplodFile, url, requestType);
            return result;

        }
        public RestfulWebApiResultModel uploadSharepointPdfFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary)
        {
            string url = ConfigurationManager.AppSettings["AddDocuments"].ToString() + token;
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            var fhaNumber = documentInfo.indexValue == null ? "FHA" : documentInfo.indexValue;
            string sharepointPdf = "SharePointData.pdf";
            string fileName = String.Format("{0}_{1}", fhaNumber, sharepointPdf);
            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                        new KeyValuePair<string, string>("propertyID", documentInfo.propertyID),
                        new KeyValuePair<string, string>("documentType", documentInfo.documentType),
                        new KeyValuePair<string, string>("indexType", documentInfo.indexType),
                        new KeyValuePair<string, string>("indexValue", documentInfo.indexValue),
                        new KeyValuePair<string, string>("pdfConvertableValue", documentInfo.pdfConvertableValue),
                        new KeyValuePair<string, string>("folderNames", documentInfo.folderNames),
                    };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }
                    Stream stream = new MemoryStream(binary);
                    content.Add(new StreamContent(stream), "\"documents\"", string.Format("\"{0}\"", fileName));

                    result = helper.PostRequest(url, content);
                    result.documentType = documentInfo.documentType;
                    result.fileName = fileName;
                }
            }
            return result;
        }
        public RestfulWebApiResultModel uploadCopiedFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary, string filename)
        {
            string url = ConfigurationManager.AppSettings["AddDocuments"].ToString() + token;
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();

            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                        new KeyValuePair<string, string>("propertyID", documentInfo.propertyID),
                        new KeyValuePair<string, string>("documentType", documentInfo.documentType),
                        new KeyValuePair<string, string>("indexType", documentInfo.indexType),
                        new KeyValuePair<string, string>("indexValue", documentInfo.indexValue),
                        new KeyValuePair<string, string>("pdfConvertableValue", documentInfo.pdfConvertableValue),
                         new KeyValuePair<string, string>("folderNames", documentInfo.folderNames),
                    };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }
                    Stream stream = new MemoryStream(binary);
                    content.Add(new StreamContent(stream), "\"documents\"", string.Format("\"{0}\"", filename));

                    result = helper.PostRequest(url, content);

                }
            }
            return result;
        }
        private RestfulWebApiResultModel clientupload(RestfulWebApiUploadModel DocumentInfo, HttpPostedFileBase file, string url, string requestType)
        {
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel rslt = new RestfulWebApiResultModel();
            var DocType = new DocumentTypeModel();
            var fileName = Path.GetFileName(file.FileName);
            //if (requestType!="" && requestType!=null&& requestType !="OPA")
            //{
            //    if (fileName != "" || !string.IsNullOrEmpty(fileName))
            //    {
            //        fileName = requestType.Trim() + "!" + fileName;
            //    }
            //}
            if (string.IsNullOrEmpty(DocumentInfo.folderNames))
            {
                DocumentInfo.folderNames = "Production/WS";
            }
            if (DocumentInfo.folderKey > 0)
            {
                DocType = doumentTypeRepostory.GetDocumentTypeId(fileName, DocumentInfo.folderKey);
            }

            if (requestType == "FHACredit")
            {
                DocumentInfo.documentType = ConfigurationManager.AppSettings["FHACreditDocType"].ToString();
            }
            else if (DocType.DocumentTypeId > 0)
            {
                DocumentInfo.documentType = DocType.DocumentTypeId.ToString();
            }
            else
            {
                DocumentInfo.documentType = ConfigurationManager.AppSettings["DefaultDocId"].ToString();
            }

            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                        new KeyValuePair<string, string>("propertyID", DocumentInfo.propertyID),
                        new KeyValuePair<string, string>("documentType", DocumentInfo.documentType),
                        new KeyValuePair<string, string>("indexType", DocumentInfo.indexType),
                        new KeyValuePair<string, string>("indexValue", DocumentInfo.indexValue),
                        new KeyValuePair<string, string>("pdfConvertableValue", DocumentInfo.pdfConvertableValue),
                        new KeyValuePair<string, string>("folderNames", DocumentInfo.folderNames),
                    };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }
                    content.Add(new StreamContent(file.InputStream), "\"documents\"", string.Format("\"{0}\"", fileName));

                    rslt = helper.PostRequest(url, content);
                    rslt.documentType = DocumentInfo.documentType;
                }
            }
            return rslt;
        }
        //harish changed model to array class 04032020

        private RestfulWebApiResultModel AssetManagementClientUpload(RestfulWebApiUploadModel DocumentInfo, HttpPostedFileBase file, string url, string requestType)
        {
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel rslt = new RestfulWebApiResultModel();
            var DocType = new DocumentTypeModel();
            var fileName = Path.GetFileName(file.FileName);
                DocumentInfo.documentType = ConfigurationManager.AppSettings["DefaultDocId"].ToString();

            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                        new KeyValuePair<string, string>("propertyID", DocumentInfo.propertyID),
                        new KeyValuePair<string, string>("documentType", DocumentInfo.documentType),
                        new KeyValuePair<string, string>("indexType", DocumentInfo.indexType),
                        new KeyValuePair<string, string>("indexValue", DocumentInfo.indexValue),
                        new KeyValuePair<string, string>("pdfConvertableValue", DocumentInfo.pdfConvertableValue),
                        new KeyValuePair<string, string>("folderNames", DocumentInfo.folderNames),
                        new KeyValuePair<string, string>("transactionType", DocumentInfo.transactionType),
                        new KeyValuePair<string, string>("transactionStatus", DocumentInfo.transactionStatus),
                        new KeyValuePair<string, string>("transactionDate", DocumentInfo.transactionDate.ToString()),
                    };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }
                    content.Add(new StreamContent(file.InputStream), "\"documents\"", string.Format("\"{0}\"", fileName));

                    rslt = helper.PostAssetManagementRequest(url, content);
                    rslt.documentType = DocumentInfo.documentType;
                }
            }
            return rslt;
        }

        private void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        //#868 harish added  new api call after Submission of R4R Request 06052020
        public RestfulWebApiResultModel AssetManagementuploadSharepointPdfFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary)
        {
            string url = ConfigurationManager.AppSettings["addAssetManagementDocuments"].ToString() + token;
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            var fhaNumber = documentInfo.indexValue == null ? "FHA" : documentInfo.indexValue;
            string sharepointPdf = "SharePointData.pdf";
            string fileName = String.Format("{0}_{1}", fhaNumber, sharepointPdf);
            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                         new KeyValuePair<string, string>("propertyID", documentInfo.propertyID),
                        new KeyValuePair<string, string>("documentType", documentInfo.documentType),
                        new KeyValuePair<string, string>("indexType", documentInfo.indexType),
                        new KeyValuePair<string, string>("indexValue", documentInfo.indexValue),
                        new KeyValuePair<string, string>("pdfConvertableValue", documentInfo.pdfConvertableValue),
                        new KeyValuePair<string, string>("folderNames", documentInfo.folderNames),
                        new KeyValuePair<string, string>("transactionType", documentInfo.transactionType),
                        new KeyValuePair<string, string>("transactionStatus", documentInfo.transactionStatus),
                        new KeyValuePair<string, string>("transactionDate", documentInfo.transactionDate.ToString()),
                        //new KeyValuePair<string, string>("propertyID", documentInfo.propertyID),
                        //new KeyValuePair<string, string>("documentType", documentInfo.documentType),
                        //new KeyValuePair<string, string>("indexType", documentInfo.indexType),
                        //new KeyValuePair<string, string>("indexValue", documentInfo.indexValue),
                        //new KeyValuePair<string, string>("pdfConvertableValue", documentInfo.pdfConvertableValue),
                        //new KeyValuePair<string, string>("folderNames", documentInfo.folderNames),
                    };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }
                    Stream stream = new MemoryStream(binary);
                    content.Add(new StreamContent(stream), "\"documents\"", string.Format("\"{0}\"", fileName));

                    result = helper.PostAssetManagementRequest(url, content);
                    result.documentType = documentInfo.documentType;
                    result.fileName = fileName;
                }
            }
            return result;
        }
        //#868 harish added this api to call R4R File Upload 07052020
        public RestfulWebApiResultModel AssetManagementR4RAndNCREUploadDocumentUsingWebApi(RestfulWebApiUploadModel DocumentInfo, string token, HttpPostedFileBase UplodFile, string requestType)
        {

            string url = ConfigurationManager.AppSettings["addAssetManagementDocuments"].ToString() + token;
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            result = AssetManagementR4RAndNCREClientUpload(DocumentInfo, UplodFile, url, requestType);
            return result;

        }

        // #868 harish added Api call for R4R and NCRE
        private RestfulWebApiResultModel AssetManagementR4RAndNCREClientUpload(RestfulWebApiUploadModel DocumentInfo, HttpPostedFileBase file, string url, string requestType)
        {
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel rslt = new RestfulWebApiResultModel();
            var DocType = new DocumentTypeModel();
            var fileName = Path.GetFileName(file.FileName);
            // DocumentInfo.documentType = ConfigurationManager.AppSettings["DefaultDocId"].ToString();

            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                        new KeyValuePair<string, string>("propertyID", DocumentInfo.propertyID),
                        new KeyValuePair<string, string>("documentType", DocumentInfo.documentType),
                        new KeyValuePair<string, string>("indexType", DocumentInfo.indexType),
                        new KeyValuePair<string, string>("indexValue", DocumentInfo.indexValue),
                        new KeyValuePair<string, string>("pdfConvertableValue", DocumentInfo.pdfConvertableValue),
                        new KeyValuePair<string, string>("folderNames", DocumentInfo.folderNames),
                        new KeyValuePair<string, string>("transactionType", DocumentInfo.transactionType),
                        new KeyValuePair<string, string>("transactionStatus", DocumentInfo.transactionStatus),
                        new KeyValuePair<string, string>("transactionDate", DocumentInfo.transactionDate.ToString()),
                    };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }
                    content.Add(new StreamContent(file.InputStream), "\"documents\"", string.Format("\"{0}\"", fileName));

                    rslt = helper.PostAssetManagementRequest(url, content);
                    rslt.documentType = DocumentInfo.documentType;
                }
            }
            return rslt;
        }
    }
}
