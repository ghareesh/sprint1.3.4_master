﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using Model.Production;

namespace Repository.Production
{
    public class Prod_SharepointScreenRepository:BaseRepository<Prod_SharepointScreen>, IProd_SharepointScreenRepository
    {
        public Prod_SharepointScreenRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Prod_SharepointScreenRepository() : base(new UnitOfWork(DBSource.Live))
        {
        }

        public bool SaveOrUpdateGeneralInformationDetailsForSharepoint(GeneralInformationViewModel generalInfoModel)
        {
            try
            {
                var sharepointResult = Find(p => p.TaskinstanceId == generalInfoModel.TaskInstanceId).FirstOrDefault();
                if (sharepointResult != null)
                {
                    sharepointResult.IsLongTermHold = generalInfoModel.IsLongTermHold;
                    sharepointResult.DateOfHold = generalInfoModel.DateOfHold;
                    //sharepointResult.ReasonForHold = sharepointResult.ReasonForHold + generalInfoModel.ReasonForHold +
                    //                                 "-BY:" + UserPrincipal.Current.UserName + ">";
                    sharepointResult.ReasonForHold = generalInfoModel.ReasonForHold;
                    sharepointResult.IsLoanCommitteApproved = generalInfoModel.IsLoanCommitteApproved;
                    sharepointResult.LoanCommitteApprovedDate = generalInfoModel.LoanCommitteApprovedDate;
                    sharepointResult.IsFirmCommitmentIssued = generalInfoModel.IsFirmCommitmentIssued;
                    sharepointResult.FirmCommitmentIssuedDate = generalInfoModel.FirmCommitmentIssuedDate;
                    sharepointResult.ModifiedOn = DateTime.UtcNow;
                    sharepointResult.ModifiedBy = UserPrincipal.Current.UserId;
                    this.Update(sharepointResult);
                }
                else
                {
                    var prodSharepoint = Mapper.Map<Prod_SharepointScreen>(generalInfoModel);
                    //prodSharepoint.ReasonForHold = prodSharepoint.ReasonForHold + "-BY:" + UserPrincipal.Current.UserName + ">";
                    prodSharepoint.CreatedBy = UserPrincipal.Current.UserId;
                    prodSharepoint.CreatedON = DateTime.UtcNow;
                    prodSharepoint.ModifiedOn = DateTime.UtcNow;
                    prodSharepoint.ModifiedBy = UserPrincipal.Current.UserId;
                    this.InsertNew(prodSharepoint);
                }
                UnitOfWork.Save();
            }
            catch (Exception e)
            {

                throw;
            }
            return true;
        }

        public SharePointCommentsModel GetSharepointComments(Guid taskInstanceId)
        {
            var sharepointComment = this.Find(m => m.TaskinstanceId == taskInstanceId).FirstOrDefault();
            SharePointCommentsModel sharepointModelComment = null;
            if (sharepointComment != null)
            {
                sharepointModelComment = new SharePointCommentsModel()
                {
                    TaskInstanceId = sharepointComment.TaskinstanceId,
                    FHANumber = sharepointComment.FHANumber,
                    OGCComments = sharepointComment.OGCComment,
                    EnvironmentalComment = sharepointComment.EnvironmentalComment,
                    SurveyComment = sharepointComment.SurveyComment,
                    ContractorComment = sharepointComment.ContractorComment,
                    ClosingComment = sharepointComment.ClosingComment,
                    //contractors contract price 
                    ContractorContractPrice = sharepointComment.ContractorContractPrice,
                    ContractorSecondaryAmtPaid = sharepointComment.ContractorSecondaryAmtPaid,
                    ContractorAmountPaid = sharepointComment.ContractorAmountPaid

                };
       
            }
            return sharepointModelComment;
        }


        public bool AddOrUpdateComments(Guid taskInstanceId, string fhaNumber, string comment, int reviewerType)
        {
            var sharepointData = this.Find(m => m.TaskinstanceId == taskInstanceId).FirstOrDefault();
            var userName = UserPrincipal.Current.UserName;

            /***
             * if row is not created in sharepoint data screen , new row will be created using passed taskinstance id as a parameter.
             * 
             * */
            if(sharepointData == null)
            {
                var newSharepointData = new Prod_SharepointScreen()
                {
                    TaskinstanceId = taskInstanceId,
                    FHANumber = fhaNumber,
                    // ContractorComment = comment + "-BY:" + userName + ">",
                    //CreatedON = DateTime.Now,
                    CreatedON = DateTime.UtcNow,
                    CreatedBy = UserPrincipal.Current.UserId,
                    //ModifiedOn = DateTime.Now,
                    ModifiedOn = DateTime.UtcNow,
                    ModifiedBy = UserPrincipal.Current.UserId
                };              
                switch (reviewerType)
                {
                    case (int)ProductionView.ContractUW: newSharepointData.ContractorComment = comment + "-BY:" + userName + ">"; break;
                    case (int)ProductionView.Environmentalist: newSharepointData.EnvironmentalComment = comment + "-BY:" + userName + ">"; break;
                    case (int)ProductionView.Attorney: newSharepointData.OGCComment = comment + "-BY:" + userName + ">"; break;
                    case (int)ProductionView.Survey: newSharepointData.SurveyComment = comment + "-BY:" + userName + ">"; break;
                    case (int)ProductionView.ContractCloser: newSharepointData.ClosingComment = comment + "-BY:" + userName + ">"; break;
                    case (int)ProductionView.Appraiser: newSharepointData.AppraisalComment = comment + "-BY:" + userName + ">"; break;
                    default: break;
                }
                this.InsertNew(newSharepointData);
                UnitOfWork.Save();
                return true; 
            }
                // if row already created in Sharepoint screen for this taskInstance id , below logic will update the existing row.
            else
            {  
                
                var selectedColumn = getColumnProperty(reviewerType);
                /** Reflection to get column name and value 
                 *  to update reviewer comment column
                 **/
                var columnValue = sharepointData.GetType().GetProperty(selectedColumn); 
                var reviewerComment = columnValue.GetValue(sharepointData, null);
                var oldComment = reviewerComment;
                string updatedComment = "";
                if (oldComment != null)
                {
                    updatedComment = oldComment + "" + comment + "- BY:" + userName + ">";
                }
                else
                {
                    updatedComment += comment + "- BY:" + userName + ">";
                }
                columnValue.SetValue(sharepointData, updatedComment, null);
                // sharepointData.ContractorComment = updatedComment;
                //sharepointData.ModifiedOn = DateTime.Now;
                sharepointData.ModifiedOn = DateTime.UtcNow;
                sharepointData.ModifiedBy = UserPrincipal.Current.UserId;
                this.Update(sharepointData);
                UnitOfWork.Save();
                return true;
            }
            return false;
        }
        private string getColumnProperty(int reviewertype)
        {
            switch (reviewertype)
            {
                case (int)ProductionView.ContractUW: return "ContractorComment";
                case (int)ProductionView.Environmentalist:return "EnvironmentalComment";
                case (int)ProductionView.Attorney:return "OGCComment";
                case (int)ProductionView.Survey: return "SurveyComment";
                case (int)ProductionView.ContractCloser: return "ClosingComment"; break;
                case (int)ProductionView.Appraiser: return "AppraisalComment";
                default: return ""; break;
            }
        }

        public MiscellaneousInformationViewModel GetSharePointDetailsForMiscellaneousInfo(MiscellaneousInformationViewModel miscInfoModel)
        {
            try
            {
                if (miscInfoModel != null)
                {
                    var sharepointResult = Find(p => p.TaskinstanceId == miscInfoModel.TaskInstanceId).FirstOrDefault();
                    if (sharepointResult != null)
                    {
                        miscInfoModel.FHANumber = sharepointResult.FHANumber;
                        miscInfoModel.EarlyCommencementIssuedDate = sharepointResult.EarlyCommencementIssuedDate;
                        miscInfoModel.EarlyCommencementRequestedDate = sharepointResult.EarlyCommencementRequestedDate;
                        miscInfoModel.IsEarlyCommencementRequested = sharepointResult.IsEarlyCommencementRequested;
                        miscInfoModel.IsLDLIssued = sharepointResult.IsLDLIssued;
                        miscInfoModel.LDLIssuedDate = sharepointResult.LDLIssuedDate;
                        miscInfoModel.LDLCompleteResponseReceived = sharepointResult.LDLCompleteResponseReceived;
                        if (!string.IsNullOrEmpty(sharepointResult.MiscellaneousComments))
                            miscInfoModel.DisplayMiscellaneousComments = sharepointResult.MiscellaneousComments.Replace(">", Environment.NewLine);
                        miscInfoModel.SelectedAeId = sharepointResult.SelectedAEId;
                        miscInfoModel.IsIremsCompleted = sharepointResult.IsIremsCompleted;
                        miscInfoModel.IsAppsCompleted = sharepointResult.IsAppsCompleted;
                        miscInfoModel.IsIremsPrintoutSaved = sharepointResult.IsIremsPrintoutSaved;
                        miscInfoModel.IsMirandaCompletedCMSListCheck = sharepointResult.IsMirandaCompletedCMSListCheck;
                        if (!string.IsNullOrEmpty(sharepointResult.OtherQueueWorkComments))
                            miscInfoModel.DisplayOtherQueueWorkComments = sharepointResult.OtherQueueWorkComments.Replace(">", Environment.NewLine);
                    }
                }
            }
            catch (Exception e)
            {
                
                throw;
            }
            return miscInfoModel;
        }

        public bool SaveOrUpdateMiscellaneousInfoForSharepoint(MiscellaneousInformationViewModel miscInfoModel)
        {
            try
            {
                var sharepointResult = Find(p => p.TaskinstanceId == miscInfoModel.TaskInstanceId).FirstOrDefault();
                if (sharepointResult != null)
                {
                    sharepointResult.EarlyCommencementIssuedDate = miscInfoModel.EarlyCommencementIssuedDate;
                    sharepointResult.EarlyCommencementRequestedDate = miscInfoModel.EarlyCommencementRequestedDate;
                    sharepointResult.IsEarlyCommencementRequested = miscInfoModel.IsEarlyCommencementRequested;
                    sharepointResult.IsLDLIssued = miscInfoModel.IsLDLIssued;
                    sharepointResult.LDLIssuedDate = miscInfoModel.LDLIssuedDate;
                    sharepointResult.LDLCompleteResponseReceived = miscInfoModel.LDLCompleteResponseReceived;
                    if (!string.IsNullOrEmpty(miscInfoModel.MiscellaneousComments))
                        sharepointResult.MiscellaneousComments += "" + miscInfoModel.MiscellaneousComments + "- BY:" +
                                                              UserPrincipal.Current.UserName + ">";
                    sharepointResult.SelectedAEId = miscInfoModel.SelectedAeId;
                    sharepointResult.IsIremsCompleted = miscInfoModel.IsIremsCompleted;
                    sharepointResult.IsAppsCompleted = miscInfoModel.IsAppsCompleted;
                    sharepointResult.IsIremsPrintoutSaved = miscInfoModel.IsIremsPrintoutSaved;
                    sharepointResult.IsMirandaCompletedCMSListCheck = miscInfoModel.IsMirandaCompletedCMSListCheck;
                    if (!string.IsNullOrEmpty(miscInfoModel.OtherQueueWorkComments))
                        sharepointResult.OtherQueueWorkComments += "" + miscInfoModel.OtherQueueWorkComments + "- BY:" +
                                                               UserPrincipal.Current.UserName + ">";
                    sharepointResult.ModifiedOn = DateTime.UtcNow;
                    sharepointResult.ModifiedBy = UserPrincipal.Current.UserId;
                    this.Update(sharepointResult);
                }
                else
                {
                    var prodSharepoint = Mapper.Map<Prod_SharepointScreen>(miscInfoModel);
                    if (!string.IsNullOrEmpty(miscInfoModel.MiscellaneousComments))
                        prodSharepoint.MiscellaneousComments = miscInfoModel.MiscellaneousComments + "- BY:" +
                                                              UserPrincipal.Current.UserName + ">";
                    if (!string.IsNullOrEmpty(miscInfoModel.OtherQueueWorkComments))
                        prodSharepoint.OtherQueueWorkComments = miscInfoModel.OtherQueueWorkComments + "- BY:" +
                                                               UserPrincipal.Current.UserName + ">";
                    prodSharepoint.CreatedBy = UserPrincipal.Current.UserId;
                    prodSharepoint.CreatedON = DateTime.UtcNow;
                    prodSharepoint.ModifiedOn = DateTime.UtcNow;
                    prodSharepoint.ModifiedBy = UserPrincipal.Current.UserId;
                    this.InsertNew(prodSharepoint);
                }
                UnitOfWork.Save();
            }
            catch (Exception e)
            {
                
                throw;
            }
            return true;
        }


        public void SaveOrUpdateContractorsPayment(Guid taskInstanceId, string fhaNumber,decimal contractorPrice, decimal secondaryAmntPaid, decimal amountPaid)
        {
            var sharepointData = this.Find(m => m.TaskinstanceId == taskInstanceId).FirstOrDefault();
            if (sharepointData != null)
            {
                sharepointData.ContractorContractPrice = contractorPrice;
                sharepointData.ContractorSecondaryAmtPaid = secondaryAmntPaid;
                sharepointData.ContractorAmountPaid = amountPaid;
                //sharepointData.ModifiedOn = DateTime.Now;
                sharepointData.ModifiedOn = DateTime.UtcNow;
                sharepointData.ModifiedBy = UserPrincipal.Current.UserId;
                this.Update(sharepointData);
                UnitOfWork.Save();
            }
            else
            {
                var newSharepointData = new Prod_SharepointScreen()
                {
                    TaskinstanceId = taskInstanceId,
                    FHANumber = fhaNumber,
                    ContractorContractPrice=contractorPrice,
                    ContractorSecondaryAmtPaid = secondaryAmntPaid,
                    ContractorAmountPaid = amountPaid,
                    //CreatedON = DateTime.Now,
                    CreatedON = DateTime.UtcNow,
                    CreatedBy = UserPrincipal.Current.UserId,
                    //ModifiedOn = DateTime.Now,
                    ModifiedOn = DateTime.UtcNow,
                    ModifiedBy = UserPrincipal.Current.UserId
                };
                this.InsertNew(newSharepointData);
                UnitOfWork.Save();

            }
        }


        public SharePointClosingInfoModel GetSharePointClosingInfo(Guid taskInstanceId)
        {
            var SharePointData = this.Find(m => m.TaskinstanceId == taskInstanceId).FirstOrDefault();
            SharePointClosingInfoModel sharePointClosingInfo = null;

            if(SharePointData !=null){
              sharePointClosingInfo= new SharePointClosingInfoModel () 
                {
                    TaskInstanceId = SharePointData.TaskinstanceId,
                    FHANumber = SharePointData.FHANumber,
                    IsCloserContractor = SharePointData.IsCloserContractor,
                    ContractorContactName = SharePointData.ContractorContactName,
                    ClosingProgramSpecialistId = SharePointData.ClosingProgramSpecialistId,
                    IsCostCertRecieved = SharePointData.IsCostCertRecieved,
                    CostCertRecievedDate = SharePointData.CostCertRecievedDate ,
                    IsCostCertCompletedAndIssued = SharePointData.IsCostCertCompletedAndIssued,
                    CostCertIssueDate = SharePointData.CostCertIssueDate,
                    Is290Completed = SharePointData.Is290Completed,
                    Two90CompletedDate = SharePointData.Two90CompletedDate,
                    IsActiveNCRE = SharePointData.IsActiveNCRE,
                    NCREDueDate = SharePointData.NCREDueDate,
                    NCREInitalBalance = SharePointData.NCREInitalBalance,
                    ClosingPackageRecievedDate = SharePointData.ClosingPackageRecievedDate,
                    ClosingComment = SharePointData.ClosingComment,
                    ClosingAEId = SharePointData.ClosingAEId,
                    ClosingCloserId = SharePointData.ClosingCloserId,
                    InitialClosingDate = SharePointData.InitialClosingDate,
                    ClosingDateCreatedOn = SharePointData.ClosingDateCreatedOn,
                    ClosingDateModifiedOn = SharePointData.ClosingDateModifiedOn
                    
                };
            }
            return sharePointClosingInfo;
        }

        public bool SaveOrUpdateClosingInfo(ClosingInfo model,string fhaNumber)
        {
            //DateTime? ClosingDateCreatedOn = null;
            //if (model.InitialClosingDate != null)
            //    ClosingDateCreatedOn = model.InitialClosingDate;
            bool firstTimeClosingDate = false;
            var sharePointData = this.Find(m => m.TaskinstanceId == model.TaskInstanceId).FirstOrDefault();
            if (sharePointData == null)
            {
                if (model.InitialClosingDate != null)
                    firstTimeClosingDate = true;
                var newSharepointData = new Prod_SharepointScreen()
                {
                    TaskinstanceId = model.TaskInstanceId,
                    FHANumber = fhaNumber,
                    IsCloserContractor = model.IsCloserContractor,
                    ContractorContactName = model.IsCloserContractor == true ? model.ContractorContactName : "",
                    ClosingProgramSpecialistId = model.ClosingProgramSpecialistId,
                    IsCostCertRecieved = model.IsCostCertRecieved,
                    CostCertRecievedDate = model.IsCostCertRecieved == true ? model.CostCertRecievedDate : null,
                    IsCostCertCompletedAndIssued = model.IsCostCertCompletedAndIssued,
                    CostCertIssueDate = model.IsCostCertCompletedAndIssued == true ? model.CostCertIssueDate : null,
                    Is290Completed = model.Is290Completed,
                    Two90CompletedDate = model.Is290Completed == true ? model.Two90CompletedDate : null,
                    IsActiveNCRE = model.IsActiveNCRE,
                    NCREDueDate = model.IsActiveNCRE == true ? model.NCREDueDate : null,
                    NCREInitalBalance = model.NCREInitalBalance,
                    ClosingAEId = model.ClosingAEId,
                    ClosingCloserId = model.ClosingCloserId,
                    InitialClosingDate = model.InitialClosingDate,
                    ClosingPackageRecievedDate = model.ClosingPackageRecievedDate,
                    //CreatedON = DateTime.Now,
                    CreatedON = DateTime.UtcNow,
                    CreatedBy = UserPrincipal.Current.UserId,
                    //ModifiedOn = DateTime.Now,
                    ModifiedOn = DateTime.UtcNow,
                    ModifiedBy = UserPrincipal.Current.UserId,
                    //ClosingDateCreatedOn = model.InitialClosingDate == null ? model.InitialClosingDate : DateTime.Now,
                    //ClosingDateModifiedOn = model.InitialClosingDate==null ? model.InitialClosingDate : DateTime.Now
                    ClosingDateCreatedOn = model.InitialClosingDate == null ? model.InitialClosingDate : DateTime.UtcNow,
                    ClosingDateModifiedOn = model.InitialClosingDate == null ? model.InitialClosingDate : DateTime.UtcNow
                    //ClosingDateCreatedOn = ClosingDateCreatedOn//DateTime.Now TODO: only need to insert date if InitialClosingDate is filled??
                };
                this.InsertNew(newSharepointData);
                UnitOfWork.Save();
            }
            else{ //

                sharePointData.IsCloserContractor = model.IsCloserContractor;
                sharePointData.ContractorContactName = model.IsCloserContractor==true? model.ContractorContactName:null;
                sharePointData.ClosingProgramSpecialistId = model.ClosingProgramSpecialistId;
                sharePointData.IsCostCertRecieved = model.IsCostCertRecieved;
                sharePointData.CostCertRecievedDate = model.IsCostCertRecieved==true? model.CostCertRecievedDate:null;
                sharePointData.IsCostCertCompletedAndIssued = model.IsCostCertCompletedAndIssued;
                sharePointData.CostCertIssueDate =model.IsCostCertCompletedAndIssued==true ? model.CostCertIssueDate:null;
                sharePointData.Is290Completed = model.Is290Completed;
                sharePointData.Two90CompletedDate = model.Is290Completed==true ? model.Two90CompletedDate:null;
                sharePointData.IsActiveNCRE = model.IsActiveNCRE;
                sharePointData.NCREDueDate = model.IsActiveNCRE==true?model.NCREDueDate:null;
                sharePointData.NCREInitalBalance = model.NCREInitalBalance;
                sharePointData.ClosingAEId = model.ClosingAEId;
                sharePointData.ClosingCloserId = model.ClosingCloserId;
                sharePointData.InitialClosingDate = model.InitialClosingDate;
                if (sharePointData.ClosingDateCreatedOn == null && model.InitialClosingDate != null)
                {
                    //sharePointData.ClosingDateCreatedOn = DateTime.Now;
                    sharePointData.ClosingDateCreatedOn = DateTime.UtcNow;
                    firstTimeClosingDate = true;
                }
                if (sharePointData.ClosingDateCreatedOn != null && model.InitialClosingDate != null)
                    //sharePointData.ClosingDateModifiedOn = DateTime.Now;
                    sharePointData.ClosingDateModifiedOn = DateTime.UtcNow;
                sharePointData.ClosingPackageRecievedDate = model.ClosingPackageRecievedDate;
                //sharePointData.ModifiedOn = DateTime.Now;
                sharePointData.ModifiedOn = DateTime.UtcNow;
                sharePointData.ModifiedBy = UserPrincipal.Current.UserId;
                //sharePointData.ClosingDateCreatedOn = ClosingDateCreatedOn; //TODO: only need to insert date if InitialClosingDate is filled??
                this.Update(sharePointData);
                UnitOfWork.Save();

            }
            return firstTimeClosingDate;
        }

        //karri#672
        public bool SaveAuditTrialNCREInitBalanceInfo(string fhaNumber_ref, int userid, int attribid, string oldvalue, string newvalue)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_Set_AuditTrialAttribugteInfo, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<FHANumberRequestViewModel>("usp_Set_AuditTrialAttribugteInfo",
                 new
                 {
                     FHANumber_Ref = fhaNumber_ref,
                     UserID = userid,
                     AttribID = attribid,
                     OldValue = oldvalue,
                     NewValue = newvalue

                 }).FirstOrDefault();

            return true;

        }

        public AllSharepointData GetSharepointDataById(Guid taskInstanceId)
        {
            try
            {
                var allSharepointData = this.Find(m => m.TaskinstanceId == taskInstanceId).FirstOrDefault();
                return Mapper.Map<Prod_SharepointScreen, AllSharepointData>(allSharepointData);
            }
            catch (Exception ex)
            { }
            return null;
        }



        public void SaveOrUpdateOgcAddress(string fhaNumber, Guid taskInstanceId, int addressId, int reviewerTypeId)
        {
            var sharePointData = this.Find(m => m.TaskinstanceId == taskInstanceId).FirstOrDefault();
            if (sharePointData != null)
            {
                if (reviewerTypeId == (int)ProductionView.BackupOGC) sharePointData.BackupOgcAddressId = addressId;
                else sharePointData.OgcAddressId = addressId;
                //sharePointData.ModifiedOn = DateTime.Now;
                sharePointData.ModifiedOn = DateTime.UtcNow;
                sharePointData.ModifiedBy = UserPrincipal.Current.UserId;
                this.Update(sharePointData);
            }
            else
            {
                var newSharepointData = new Prod_SharepointScreen();
                
                    newSharepointData.TaskinstanceId = taskInstanceId;
                    newSharepointData.FHANumber = fhaNumber;
                    if (reviewerTypeId == (int)ProductionView.BackupOGC) newSharepointData.BackupOgcAddressId = addressId;
                    else newSharepointData.OgcAddressId = addressId;
                //newSharepointData.CreatedON = DateTime.Now;
                newSharepointData.CreatedON = DateTime.UtcNow;
                newSharepointData.CreatedBy = UserPrincipal.Current.UserId;
                //newSharepointData.ModifiedOn = DateTime.Now;
                newSharepointData.ModifiedOn = DateTime.UtcNow;
                newSharepointData.ModifiedBy = UserPrincipal.Current.UserId;
               
                this.InsertNew(newSharepointData);
            }
            UnitOfWork.Save();
        }

        /// <summary>
        /// Get Account Executive id from Prod_SharepointScreen
        /// AE information get stored from Production App type: select Application from dropdown . under miscelleneous info tab
        /// and also by Production App type: Select Final Closing. Under Closing tab
        /// </summary>
        /// <param name="pTaskinstanceId"></param>
        /// <returns></returns>
        public int GetAccountExecutiveById(Guid pTaskinstanceId)
        {
            var allSharepointData = this.Find(m => m.TaskinstanceId == pTaskinstanceId).FirstOrDefault();
            if (allSharepointData != null && allSharepointData.ClosingAEId > 0)
                return allSharepointData.ClosingAEId;
            if (allSharepointData != null && allSharepointData.SelectedAEId > 0)
                return allSharepointData.SelectedAEId;
            return 0;
        }
    }
}
