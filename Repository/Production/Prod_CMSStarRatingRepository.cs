﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class Prod_CMSStarRatingRepository:BaseRepository<Prod_CMSStarRating>,IProd_CMSStarRatingRepository
    {
        public Prod_CMSStarRatingRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Prod_CMSStarRatingRepository() : base(new UnitOfWork(DBSource.Live))
        {
            
        }
        public IList<CMSStarRatingModel> GetCMSStarRatings()
        {
            var results = this.GetAll().ToList();
            return Mapper.Map<IList<CMSStarRatingModel>>(results);
        }
    }
}
