﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository.Production
{
    public class AppProcessRepository : BaseRepository<OpaForm>, IAppProcessRepository
    {
        public AppProcessRepository()
            : base(new UnitOfWork(DBSource.Live))
        {

        }
        public AppProcessRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }



        public IEnumerable<ProductionQueueLenderInfo> GetAppRequests()
        {
            try
            {
                var context = this.Context as HCP_live;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in usp_HCP_Prod_GetSubmittedFhaRequests, please pass in correct context in unit of work.");

                var results = context.Database.SqlQuerySimple<ProductionQueueLenderInfo>("usp_HCP_Prod_GetSubmittedApplicationRequests").ToList();

                var AppRequest = (from result in results
                                  select new ProductionQueueLenderInfo()
                                  {
                                      Portfolio_Name = result.Portfolio_Name,
                                      opportunity_zone = result.opportunity_zone,//naresh prod queue phase 2 19/12/2019
                                      LenderName = result.LenderName,
                                      projectName = result.projectName,
                                      CreatedBy = result.CreatedBy,
                                      ModifiedOn = result.ModifiedOn,
                                      TaskinstanceId = result.TaskinstanceId,
                                      IsLIHTC = result.IsLIHTC,
                                      LoanType = result.LoanType,
                                      LoanAmount = result.LoanAmount

                                  }).OrderByDescending(n => n.ModifiedOn).OrderByDescending(o => o.IsLIHTC);
                return AppRequest;
            }

            catch (Exception ex)
            {

            }
            return null;
        }

        public GeneralInformationViewModel GetGeneralInfoDetailsForSharepoint(Guid taskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in usp_HCP_Prod_GetGeneralInfoDetailsForSharePointScreen, please pass in correct context in unit of work.");

                var generalInfo = context.Database.SqlQuerySimple<GeneralInformationViewModel>("usp_HCP_Prod_GetGeneralInfoDetailsForSharePointScreen", new
                {
                    TaskinstanceId = taskInstanceId
                }).FirstOrDefault();

                return generalInfo;
            }

            catch (Exception ex)
            {

            }
            return null;

        }

        public MiscellaneousInformationViewModel GetContractUWDetails(Guid taskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in usp_HCP_Prod_GetContractUWDetailsForSharePointScreen, please pass in correct context in unit of work.");

                var miscellaneousInfo = context.Database.SqlQuerySimple<MiscellaneousInformationViewModel>("usp_HCP_Prod_GetContractUWDetailsForSharePointScreen", new
                {
                    TaskinstanceId = taskInstanceId
                }).FirstOrDefault();

                return miscellaneousInfo?? new MiscellaneousInformationViewModel();
            }

            catch (Exception ex)
            {

            }
            return new MiscellaneousInformationViewModel();
        }
    }
}