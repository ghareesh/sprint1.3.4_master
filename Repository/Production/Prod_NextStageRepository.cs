﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Repository.Production
{
    public class Prod_NextStageRepository : BaseRepository<Prod_NextStage>, IProd_NextStageRepository
    {
        public Prod_NextStageRepository()
           : base(new UnitOfWork(DBSource.Live))
        {
        }

        public Prod_NextStageRepository(UnitOfWork unitOfWork)
           : base(unitOfWork)
        {
        }

        public Guid AddNextStage(Prod_NextStageModel model)
        {
            try
            {
                var context = this.Context as HCP_live;
                var nextstage = Mapper.Map<Prod_NextStage>(model);
                this.InsertNew(nextstage);
                context.SaveChanges();
                return nextstage.NextStgId;
            }
            catch (Exception ex)
            {

            }
            return new Guid();
        }
        //Check the logic
        //public List<string> GetFhasforNxtStage(int pagetypeid)
        //{
        //    var context = this.Context as HCP_live;
        //    var results = (from a in context.Prod_NextStages
        //                   join ul in context.User_Lender
        //                   on a.FhanUmber equals ul.FHANumber
        //                   where ul.User_ID == UserPrincipal.Current.UserId && a.NextPgTypeId == pagetypeid
        //                   select a.FhanUmber).Distinct().ToList();
        //    return results;

        //}
        public List<string> GetFhasforNxtStage(int pagetypeid, int lenderId)
        {
            var context = this.Context as HCP_live;
            if (pagetypeid == 18)
            {
                //Naveen 30-10-2019 added if cond
                if (!UserPrincipal.Current.UserRole.Contains("Servicer"))
                {
                    var results = (from a in context.Prod_NextStages
                                   join b in context.Prod_FHANumberRequest
                                   on a.FhaNumber equals b.FHANumber
                                   where b.LenderId == lenderId && a.CompletedPgTypeId == 5
                                   select a.FhaNumber).Distinct().ToList();
                    return results;
                }
                else
                {
                    var results = (from a in context.Prod_NextStages
                                   join b in context.Prod_FHANumberRequest on a.FhaNumber equals b.FHANumber
                                   join c in context.User_Lender on a.FhaNumber equals c.FHANumber
                                   where c.User_ID == UserPrincipal.Current.UserId && a.CompletedPgTypeId == 5
                                   select a.FhaNumber).Distinct().ToList();
                    return results;
                }

            }
            else if (pagetypeid == 17)
            {
                if (!UserPrincipal.Current.UserRole.Contains("Servicer"))
                {
                    var results = context.Database.SqlQuerySimple<string>("usp_HCP_Prod_GetFHANumbersForClosingProcess",
                   new
                   {
                       lenderId = lenderId,
                       PageTypeId = pagetypeid
                   }).Distinct().ToList();

                    //karri,hareesh,amar//8mar2020,os-#783
                    if (results.Count()>=0)
                    {
                        results = (from a in context.Prod_NextStages
                                   join b in context.Prod_FHANumberRequest
                                   on a.FhaNumber equals b.FHANumber
                                   where a.NextPgTypeId == pagetypeid && a.NextPgTaskInstanceId == null && b.LenderId == lenderId
                                   select a.FhaNumber).Distinct().ToList();
                    }

                    return results;

                }//Naveen 02-10-2019 (added else condition)
                else
                {
                    var results = context.Database.SqlQuerySimple<string>("usp_HCP_Prod_GetFHANumbersForClosingProcess",
                   new
                   {
                       lenderId = 0,
                       PageTypeId = pagetypeid,
                       CurrentUserrole = UserPrincipal.Current.UserRole,
                       CurrentUserid = UserPrincipal.Current.UserId
                   }).Distinct().ToList();

                    return results;
                }
            }
            else
            {
                if (!UserPrincipal.Current.UserRole.Contains("Servicer"))
                {
                    var results = (from a in context.Prod_NextStages
                                   join b in context.Prod_FHANumberRequest
                                   on a.FhaNumber equals b.FHANumber
                                   where a.NextPgTypeId == pagetypeid && a.NextPgTaskInstanceId == null && b.LenderId == lenderId
                                   select a.FhaNumber).Distinct().ToList();
                    return results;
                }//Naveen 02-10-2019 added else
                else
                {
                    var results = (from a in context.Prod_NextStages
                                   join b in context.Prod_FHANumberRequest on a.FhaNumber equals b.FHANumber
                                   where a.NextPgTypeId == pagetypeid && a.NextPgTaskInstanceId == null /*&& b.LenderId == lenderId*/
                                   join p in context.User_Lender on b.FHANumber equals p.FHANumber
                                   where p.User_ID == UserPrincipal.Current.UserId
                                   select a.FhaNumber).Distinct().ToList();
                    return results;
                }
            }

        }

        public bool UpdateTaskInstanceIdForNxtStage(Prod_NextStageModel model)
        {
            try
            {
                var context = this.Context as HCP_live;

                var results = (from n in context.Prod_NextStages
                               where n.FhaNumber == model.FhaNumber && n.NextPgTypeId == model.NextPgTypeId
                               select n).FirstOrDefault();

                results.NextPgTaskInstanceId = model.NextPgTaskInstanceId;
                results.ModifiedOn = model.ModifiedOn;
                results.ModifiedBy = model.ModifiedBy;
                results.IsNew = model.IsNew;
                this.Update(results);
                UnitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// get next task instance id by completed taskinstance id
        /// </summary>
        /// <param name="guidCurrentTaskinstanceid"></param>
        /// <returns></returns>
        public Guid? GetNxtStageTaskid(Guid guidCurrentTaskinstanceid)
        {
            var context = this.Context as HCP_live;

            var d = context.Prod_NextStages.Where(a => a.CompletedPgTaskInstanceId == guidCurrentTaskinstanceid)
                                            .OrderByDescending(a => a.ModifiedOn).ToList();

            if (d.Count() > 0 && d.FirstOrDefault() != null)
                return d.FirstOrDefault().NextPgTaskInstanceId;

            return null;
        }

        /// <summary>
        /// Get completed taskinstance id from nextpage taskinstanceid
        /// </summary>
        /// <param name="guidCurrentTaskinstanceid"></param>
        /// <returns></returns>
        public Guid? GetCompletedStageTaskid(Guid guidCurrentTaskinstanceid)
        {
            var context = this.Context as HCP_live;

            var d = context.Prod_NextStages.Where(a => a.NextPgTaskInstanceId == guidCurrentTaskinstanceid)
                                            .OrderByDescending(a => a.ModifiedOn).ToList();

            if (d.Count() > 0 && d.FirstOrDefault() != null)
                return d.FirstOrDefault().CompletedPgTaskInstanceId;

            return null;
        }
    }
}
