﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using AutoMapper;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
   public class LenderFhaRepository:BaseRepository<Lender_FHANumber>,ILenderFhaRepository
    {
         public LenderFhaRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
         public LenderFhaRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

         public IQueryable<Lender_FHANumber> DataToJoin 
        {
            get { return DbQueryRoot; }
        }

         public DateTime GetLatestQuarter()
         {
             var data = this.GetAll().OrderByDescending(p => p.ModifiedOn).First();
             var results = Mapper.Map<LenderFhaModel>(data);
             var latestQtr = Convert.ToDateTime(results.ModifiedOn);

             return latestQtr;
         }

        //Naveen 06-11-2019 added if cond
        public IList<string> GetFhaNumbersForLender(int lenderId)
        {
            var context = (HCP_live) this.Context;

            if (UserPrincipal.Current.UserRole != "Servicer")
            {
                //var results = (from n in context.Lender_FHANumber
                //               where n.LenderID == lenderId && n.FHA_EndDate == null
                //               select new { n.FHANumber }).ToList();

                //var results1 = (from n in context.User_Lender
                //                where n.Lender_ID == lenderId && n.User_ID == 3335
                //                select new { n.FHANumber }).ToList();

                List<string> a = context.Lender_FHANumber.Where(x => x.LenderID == lenderId && x.FHA_EndDate == null).Select(x => x.FHANumber).Concat(context.User_Lender.Where(x => x.Lender_ID == lenderId).Select(x => x.FHANumber)).ToList();


                return a;
            }
            else
            {
                var results = (from n in context.User_Lender
                               where n.User_ID == UserPrincipal.Current.UserId 
                               select n.FHANumber).ToList();
                return results;
            }

            ////return (from n in context.Lender_FHANumber
            ////        where (n.LenderID == lenderId)
            ////        select n.FHANumber).ToList();
        }
    }
}
