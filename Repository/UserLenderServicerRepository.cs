﻿using System;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System.Collections.Generic;
using System.Linq;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class UserLenderServicerRepository : BaseRepository<User_Lender>, IUserLenderServicerRepository
    {

        public UserLenderServicerRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public UserLenderServicerRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<User_Lender> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public List<string> GetAllowedFhaByLenderUserId(int lenderUserId, ILenderFhaRepository lenderFhaRepository)
        {
            // string strlenderUserId = lenderUserId.ToString();
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_GetFhasByLenderUserIds, please pass in correct context in unit of work.");

            //Naveen 30-10-2019 added if cond
            if (!(UserPrincipal.Current.UserRole == "Servicer"))
            {
                var query = (from lf in lenderFhaRepository.DataToJoin
                             join ul in GetAll() on lf.LenderID equals ul.Lender_ID
                             where ul.User_ID == lenderUserId && lf.FHA_EndDate == null
                             select lf.FHANumber).ToList();

                return query;
            }
            else
            {
                var query = (from lf in lenderFhaRepository.DataToJoin
                             join ul in GetAll() on lf.FHANumber equals ul.FHANumber
                             where ul.User_ID == UserPrincipal.Current.UserId
                             select lf.FHANumber).Distinct().ToList();

                return query;
            }

        }


        public IEnumerable<int?> GetLenderIdsByUserId(int userId)
        {
            var context = (HCP_live)this.Context;
            var results = (from a in GetAll()
                           join c in context.Lender_FHANumber on a.Lender_ID equals c.LenderID
                           where a.User_ID == userId && c.FHA_EndDate == null
                           select a.Lender_ID).Distinct().ToList();

            return results.Where(p => p.HasValue);
        }

        public List<int?> GetLenderIdsByUser(int userId, ILenderFhaRepository lenderFhaRepository)
        {

            var results = (from u in this.DataToJoin
                           join l in lenderFhaRepository.DataToJoin on u.FHANumber equals l.FHANumber into ul
                           from x in ul.DefaultIfEmpty()
                           where u.User_ID == userId && x.FHA_EndDate == null
                           select (int?)x.LenderID).Distinct().ToList();

            return results;
        }
        public IEnumerable<string> GetAllowedFhasForUser(int userId)
        {
            var context = (HCP_live)this.Context;
            var results = (from a in GetAll()
                           join c in context.Lender_FHANumber on a.FHANumber equals c.FHANumber
                           where a.User_ID == userId && c.FHA_EndDate == null
                           select a.FHANumber);
            return results;
        }

        public void AddNewUserLenderRecord(User_Lender userLender)
        {
            this.AttachNew(userLender);
        }

        public List<int> GetUserLenderRow(User_Lender userLender)
        {
            var results = (from a in GetAll()
                           where a.User_ID == userLender.User_ID && a.FHANumber == userLender.FHANumber
                           select a.User_Lender_ID).ToList();
            return results;
        }

        public void RemoveUserLenderRow(int userLenderId)
        {
            this.Delete(userLenderId);
        }

        public IEnumerable<int> GetLenderIdsByAeOrWlm(string userName, int monthsInPeriod, int year)
        {
            var context = this.Context as HCP_live;
            string user_role = UserPrincipal.Current.UserRole;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetLenderIdsByAeOrWlm, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetLendersByQuarterForAEorWLM_Result>(
                    "usp_HCP_GetLendersByQuarterForAEorWLM",
                    new { UserName = userName, MonthsInPeriod = monthsInPeriod, Year = year, User_role = user_role }).ToList();
            return results.Where(p => p.LenderID.HasValue).Select(p => p.LenderID.Value);
        }

        public List<string> GetAllowedNonCriticalFhaByLenderUserId(int lenderUserId, ILenderFhaRepository lenderFhaRepository, INonCriticalProjectInfoRepository nonCriticalProjectInfo)
        {
            string strlenderUserId = lenderUserId.ToString();
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetAllowedNonCriticalFhaByLenderUserId, please pass in correct context in unit of work.");

            //Naveen 25-10-2019 added if cond
            if (UserPrincipal.Current.UserRole == "Servicer")
            {
                var query = (from q in context.User_Lender
                             join p in lenderFhaRepository.DataToJoin on q.FHANumber equals p.FHANumber
                             join r in nonCriticalProjectInfo.DataToJoin on q.FHANumber equals r.FHANumber
                             where p.FHA_EndDate == null
                             where q.User_ID == lenderUserId
                             select q.FHANumber).Distinct();

                return query.ToList();
            }
            else
            {

                var query = (from lf in lenderFhaRepository.DataToJoin
                             join ul in context.User_Lender.Where(a => a.User_ID == lenderUserId) on lf.LenderID equals ul.Lender_ID
                             join np in nonCriticalProjectInfo.DataToJoin on ul.Lender_ID equals np.LenderID
                             where lf.FHA_EndDate == null
                             select lf.FHANumber).Distinct();

                return query.ToList();

            }
        }


        public IEnumerable<int> GetLenderOperatorsbyFHA(string fhanumber)
        {
            var context = (HCP_live)this.Context;

            var results = (from a in GetAll()
                           join c in context.Lender_FHANumber on a.FHANumber equals c.FHANumber
                           where a.FHANumber == fhanumber && c.FHA_EndDate == null
                           select a.User_ID);
            return results;
        }


        public IEnumerable<string> GetAllowedFhasForUsers(List<int> UserIds)
        {
            var context = (HCP_live)this.Context;
            var results = (from a in GetAll()
                           join c in context.Lender_FHANumber on a.FHANumber equals c.FHANumber
                           where c.FHA_EndDate == null && UserIds.Contains(a.User_ID)
                           select a.FHANumber).Distinct();

            return results;
        }
        public IEnumerable<FhaInfoModel> GetSelectedFhasForInspector(int userId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetReadyFhasForInspectionContractor, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_GetFhasByLenderIds_Result>("USP_GetSelectedFhasForInspectedContractor", new { userID = userId }).OrderBy(p => p.FHANumber).ToList();
            return Mapper.Map<IEnumerable<usp_HCP_GetFhasByLenderIds_Result>, IEnumerable<FhaInfoModel>>(results);
        }


        //#293 Hareesh
        public List<string> GetAllowedFhaByLender(ILenderFhaRepository lenderFhaRepository)
        {

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_GetFhasByLenderUserIds, please pass in correct context in unit of work.");

            //var query = (from lf in lenderFhaRepository.DataToJoin
            //             join ul in GetAll()
            //             on lf.LenderID equals ul.Lender_ID
            //             where lf.FHA_EndDate == null
            //             select lf.FHANumber).Distinct().ToList();

            //Naveen 30-10-2019 added if cond
            if (!(UserPrincipal.Current.UserRole == "Servicer"))
            {
                var query = (from lf in lenderFhaRepository.DataToJoin
                             join ul in GetAll()
                             on lf.LenderID equals ul.Lender_ID
                             where lf.FHA_EndDate == null
                             orderby lf.FHANumber
                             select lf.FHANumber).Distinct().ToList();
                query.Sort();
                return query;
            }
            else
            {
                var query = (from lf in lenderFhaRepository.DataToJoin
                             join ul in GetAll() on lf.FHANumber equals ul.FHANumber
                             where ul.User_ID == UserPrincipal.Current.UserId
                             orderby lf.FHANumber
                             select lf.FHANumber).Distinct().ToList();
                query.Sort();
                return query;
            }

        }
        // Hareesh 13092019 
        public List<string> GetAllowedFhaByLenderDetails(int lenderUserId, ILenderFhaRepository lenderFhaRepository)
        {
            string strlenderUserId = lenderUserId.ToString();
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in usp_HCP_GetFhasByLenderUserIds, please pass in correct context in unit of work.");

            //var query = (from lf in lenderFhaRepository.DataToJoin
            //             join ul in GetAll()
            //             on lf.LenderID equals ul.Lender_ID
            //             where lf.FHA_EndDate == null
            //             select lf.FHANumber).Distinct().ToList();
            var query = (from lf in lenderFhaRepository.DataToJoin
                         join ul in GetAll()
                         on lf.LenderID equals ul.Lender_ID
                         where ul.User_ID == lenderUserId && lf.FHA_EndDate == null
                         orderby lf.FHANumber
                         select lf.FHANumber).Distinct().ToList();
            query.Sort();
            return query;
        }
    }
}
