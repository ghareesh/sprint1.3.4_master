﻿using AutoMapper;
using EntityObject.Entities.HCP_intermediate;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.SqlServer;
using Repository;
using Repository.Interfaces;
using System.Globalization;

namespace HUDHealthcarePortal.Repository
{
    public class DataUploadIntermediateRepository : BaseRepository<Lender_DataUpload_Intermediate>, IDataUploadIntermediateRepository
    {
        public DataUploadIntermediateRepository()
            : base(new UnitOfWork(DBSource.Intermediate))
        {
        }

        public DataUploadIntermediateRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<Lender_DataUpload_Intermediate> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public ExcelUploadView_Model GetProjectDetail(int ldiID)
        {
            var uploadData = this.GetByIDFast(ldiID);
            return Mapper.Map<Lender_DataUpload_Intermediate, ExcelUploadView_Model>(uploadData);
        }

        public void UpdateData(IEnumerable<ExcelUploadView_Model> model)
        {
            // entity to update already attached to context, so map to this existing entity without creating new entity
            List<Lender_DataUpload_Intermediate> entityToUpdate = new List<Lender_DataUpload_Intermediate>();
            model.ToList().ForEach(p => entityToUpdate.Add(this.GetByIDFast(p.LDI_ID)));
            foreach (var item in model)
            {
                var toUpdate = entityToUpdate.FirstOrDefault(p => p.LDI_ID == item.LDI_ID);
                toUpdate.DebtCoverageRatio2 = item.DebtCoverageRatio2;
                toUpdate.AverageDailyRateRatio = item.AverageDailyRateRatio;
                toUpdate.NOIRatio = item.NOIRatio;
                toUpdate.FHAQuarter = item.FHAQuarter;
                toUpdate.HasCalculated = item.HasCalculated;
            }
            entityToUpdate.ForEach(p => this.Update(p));
        }

        public IEnumerable<DerivedUploadData> GetUploadedData(int? iLenderId)
        {
            var uploadData = this.Find(p => !p.IsUploadNotComplete.HasValue && (iLenderId.HasValue ?
                p.LenderID == iLenderId.Value : true)).ToList();
            IEnumerable<DerivedUploadData> results = Mapper.Map<IEnumerable<Lender_DataUpload_Intermediate>, IEnumerable<DerivedUploadData>>(uploadData);
            return results;
        }

        public PaginateSortModel<ExcelUploadView_Model> GetUploadedDataWithPageSort(int? iLenderId,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, List<string> limitToFhas)
        {
            bool needLimitToFhas = limitToFhas != null && limitToFhas.Count > 0;
            PaginateSortModel<Lender_DataUpload_Intermediate> uploadData;
            if(needLimitToFhas)
                uploadData = this.FindWithPageSort(p => !p.IsUploadNotComplete.HasValue && (iLenderId.HasValue ?
                    p.LenderID == iLenderId.Value : true) && (limitToFhas.Contains(p.FHANumber)),
                    strSortBy, sortOrder, pageSize, pageNum);
            else
            {
                uploadData = this.FindWithPageSort(p => !p.IsUploadNotComplete.HasValue && (iLenderId.HasValue ?
                    p.LenderID == iLenderId.Value : true),
                    strSortBy, sortOrder, pageSize, pageNum);
            }
            PaginateSortModel<ExcelUploadView_Model> results = Mapper.Map<PaginateSortModel<Lender_DataUpload_Intermediate>, PaginateSortModel<ExcelUploadView_Model>>(uploadData);
            return results;
        }

        public IEnumerable<DerivedUploadData> GetUploadedDataByTimeForSearch(DateTime dateInserted, ICommentViewRepository commentViewRepo)
        {
            var query = (from u in this.DataToJoin
                         join c in commentViewRepo.DataToJoin
                          on u.LDI_ID equals c.LDI_ID into uc
                         from x in uc.DefaultIfEmpty()
                         where (!u.IsUploadNotComplete.HasValue) && Math.Abs(SqlFunctions.DateDiff("second", u.DataInserted, dateInserted) ?? 2) < 1
                         select new DerivedUploadData
                         {
                             DataSource = DBSource.Intermediate,
                             LDI_ID = u.LDI_ID,
                             ProjectName = u.ProjectName,
                             ServiceName = u.ServiceName,
                             PeriodEnding = u.PeriodEnding,
                             UnitsInFacility = u.UnitsInFacility,
                             MonthsInPeriod = u.MonthsInPeriod,
                             FHANumber = u.FHANumber,
                             TotalRevenues = u.TotalRevenues,
                             TotalExpenses = u.TotalExpenses,
                             MortgageInsurancePremium = u.MortgageInsurancePremium,
                             FHAInsuredPrincipalInterestPayment = u.FHAInsuredPrincipalInterestPayment,
                             ActualNumberOfResidentDays = u.ActualNumberOfResidentDays,
                             NOIRatio = u.NOIRatio,
                             FHAQuarter = u.FHAQuarter,
                             AverageDailyRateRatio = u.AverageDailyRateRatio,
                             DebtCoverageRatio2 = u.DebtCoverageRatio2,
                             HasComment = x == null ? false : true,
                             DataInserted = u.DataInserted
                         }).Distinct();
            return query.ToList();
        }

        public IEnumerable<DerivedUploadData> GetUploadedDataByTime(DateTime dateInserted)
        {
            var uploadData = this.Find(p => (!p.IsUploadNotComplete.HasValue) && Math.Abs(SqlFunctions.DateDiff("second", p.DataInserted, dateInserted) ?? 2) < 1);
            return Mapper.Map<IEnumerable<Lender_DataUpload_Intermediate>, IEnumerable<DerivedUploadData>>(uploadData);
        }

        public IEnumerable<ExcelUploadView_Model> GetUploadedDataByScoreRange(decimal minVal, decimal maxVal)
        {
            var uploadData = this.Find(p => (!p.IsUploadNotComplete.HasValue) && p.ScoreTotal.HasValue && p.ScoreTotal.Value >= minVal && p.ScoreTotal.Value < maxVal);
            return Mapper.Map<IEnumerable<Lender_DataUpload_Intermediate>, IEnumerable<ExcelUploadView_Model>>(uploadData);
        }

        public IEnumerable<ExcelUploadView_Model> GetUnCalculated()
        {
            var unCalculated = this.Find(p => (!p.IsUploadNotComplete.HasValue) && p.HasCalculated != true);
            return Mapper.Map<IEnumerable<Lender_DataUpload_Intermediate>, IEnumerable<ExcelUploadView_Model>>(unCalculated);
        }

        public int SaveFormUploadToIntermediateTbl(FormUploadModel model)
        {
            var uploadData = Mapper.Map<Lender_DataUpload_Intermediate>(model);
            this.InsertNew(uploadData);
            return uploadData.LDI_ID;
        }

        public IEnumerable<DerivedUploadData> GetUploadedDataByRole(int? iLenderId, ICommentViewRepository commentViewRepo, List<string> limitToFhas)
        {
            bool needLimitToFhas = limitToFhas != null && limitToFhas.Count > 0;
            IQueryable<DerivedUploadData> query;
            if (needLimitToFhas)
                query = (from u in this.DataToJoin
                         join c in commentViewRepo.DataToJoin
                          on u.LDI_ID equals c.LDI_ID into uc
                         from x in uc.DefaultIfEmpty()
                         where (!u.IsUploadNotComplete.HasValue) && (u.LenderID == (iLenderId.HasValue ? iLenderId.Value : u.LenderID))
                         && (limitToFhas.Contains(u.FHANumber))
                         select new DerivedUploadData
                         {
                             DataSource = DBSource.Intermediate,
                             LDI_ID = u.LDI_ID,
                             ProjectName = u.ProjectName,
                             ServiceName = u.ServiceName,
                             PeriodEnding = u.PeriodEnding,
                             UnitsInFacility = u.UnitsInFacility,
                             MonthsInPeriod = u.MonthsInPeriod,
                             DataInserted = u.DataInserted,
                             FHANumber = u.FHANumber,
                             TotalRevenues = u.TotalRevenues,
                             TotalExpenses = u.TotalExpenses,
                             MortgageInsurancePremium = u.MortgageInsurancePremium,
                             ActualNumberOfResidentDays = u.ActualNumberOfResidentDays,
                             ScoreTotal = u.ScoreTotal,
                             NOIRatio = u.NOIRatio,
                             FHAQuarter = u.FHAQuarter,
                             AverageDailyRateRatio = u.AverageDailyRateRatio,
                             DebtCoverageRatio2 = u.DebtCoverageRatio2,
                             FHAInsuredPrincipalInterestPayment = u.FHAInsuredPrincipalInterestPayment,
                             HasComment = x == null ? false : true
                         }).Distinct();
            else
            {
                query = (from u in this.DataToJoin
                         join c in commentViewRepo.DataToJoin
                          on u.LDI_ID equals c.LDI_ID into uc
                         from x in uc.DefaultIfEmpty()
                         where (!u.IsUploadNotComplete.HasValue) && (u.LenderID == (iLenderId.HasValue ? iLenderId.Value : u.LenderID))
                         select new DerivedUploadData
                         {
                             DataSource = DBSource.Intermediate,
                             LDI_ID = u.LDI_ID,
                             ProjectName = u.ProjectName,
                             ServiceName = u.ServiceName,
                             PeriodEnding = u.PeriodEnding,
                             UnitsInFacility = u.UnitsInFacility,
                             MonthsInPeriod = u.MonthsInPeriod,
                             DataInserted = u.DataInserted,
                             FHANumber = u.FHANumber,
                             TotalRevenues = u.TotalRevenues,
                             TotalExpenses = u.TotalExpenses,
                             MortgageInsurancePremium = u.MortgageInsurancePremium,
                             ActualNumberOfResidentDays = u.ActualNumberOfResidentDays,
                             ScoreTotal = u.ScoreTotal,
                             NOIRatio = u.NOIRatio,
                             FHAQuarter = u.FHAQuarter,
                             AverageDailyRateRatio = u.AverageDailyRateRatio,
                             DebtCoverageRatio2 = u.DebtCoverageRatio2,
                             FHAInsuredPrincipalInterestPayment = u.FHAInsuredPrincipalInterestPayment,
                             HasComment = x == null ? false : true
                         }).Distinct();
            }
            return query.ToList();
        }

        #region Derived upload data with comment flag

        public PaginateSortModel<DerivedUploadData> GetUploadedDataWithPageSort(int? iLenderId,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, ICommentViewRepository commentViewRepo, List<string> limitToFhas)
        {
            bool needLimitToFhas = limitToFhas != null && limitToFhas.Count > 0;
            IQueryable<DerivedUploadData> query;
            
            if(needLimitToFhas)
                query = (from u in this.DataToJoin
                         join c in commentViewRepo.DataToJoin
                          on u.LDI_ID equals c.LDI_ID into uc
                         from x in uc.DefaultIfEmpty()
                          where (!u.IsUploadNotComplete.HasValue) && (u.LenderID == (iLenderId.HasValue ? iLenderId.Value : u.LenderID))
                          && (limitToFhas.Contains(u.FHANumber))
                          select new DerivedUploadData
                          {
                              DataSource = DBSource.Intermediate,
                              LDI_ID = u.LDI_ID,
                              ProjectName = u.ProjectName,
                              ServiceName = u.ServiceName,
                              PeriodEnding = u.PeriodEnding,
                              UnitsInFacility = u.UnitsInFacility,
                              MonthsInPeriod = u.MonthsInPeriod,
                              DataInserted = u.DataInserted,
                              FHANumber = u.FHANumber,
                              TotalRevenues = u.TotalRevenues,
                              TotalExpenses = u.TotalExpenses,
                              FHAInsuredPrincipalInterestPayment = u.FHAInsuredPrincipalInterestPayment,
                              MortgageInsurancePremium = u.MortgageInsurancePremium,
                              ActualNumberOfResidentDays = u.ActualNumberOfResidentDays,
                              ScoreTotal = u.ScoreTotal,
                              DebtCoverageRatio2 = u.DebtCoverageRatio2,
                              AverageDailyRateRatio = u.AverageDailyRateRatio,
                              NOIRatio = u.NOIRatio,
                              FHAQuarter = u.FHAQuarter,
                              HasComment = x == null ? false : true
                          }).Distinct();
            else
            {

                query = (from u in this.DataToJoin
                         join c in commentViewRepo.DataToJoin
                          on u.LDI_ID equals c.LDI_ID into uc
                         from x in uc.DefaultIfEmpty()
                         where (!u.IsUploadNotComplete.HasValue) && (u.LenderID == (iLenderId.HasValue ? iLenderId.Value : u.LenderID))
                         select new DerivedUploadData
                         {
                             DataSource = DBSource.Intermediate,
                             LDI_ID = u.LDI_ID,
                             DataInserted = u.DataInserted,
                             ProjectName = u.ProjectName,
                             ServiceName = u.ServiceName,
                             PeriodEnding = u.PeriodEnding,
                             UnitsInFacility = u.UnitsInFacility,
                             MonthsInPeriod = u.MonthsInPeriod,
                             FHANumber = u.FHANumber,
                             TotalRevenues = u.TotalRevenues,
                             TotalExpenses = u.TotalExpenses,
                             FHAInsuredPrincipalInterestPayment = u.FHAInsuredPrincipalInterestPayment,
                             MortgageInsurancePremium = u.MortgageInsurancePremium,
                             ActualNumberOfResidentDays = u.ActualNumberOfResidentDays,
                             ScoreTotal = u.ScoreTotal,
                             DebtCoverageRatio2 = u.DebtCoverageRatio2,
                             AverageDailyRateRatio = u.AverageDailyRateRatio,
                             NOIRatio = u.NOIRatio,
                             FHAQuarter = u.FHAQuarter,
                             HasComment = x == null ? false : true
                         }).Distinct();
            }
            var temp = query.ToList();
            temp.ForEach(i => i.MonthsInPeriod_Disp = Convert.ToInt32(i.MonthsInPeriod));
            temp.ForEach(i => i.PeriodEnding_Disp = Convert.ToDateTime(i.PeriodEnding));
            var Interm = temp.SortAndPaginateList(strSortBy, sortOrder, pageSize, pageNum);
            return Interm;
        }

        public PaginateSortModel<DerivedUploadData> GetUploadedDataByTimePageSort(DateTime dateInserted, int userId,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, ICommentViewRepository commentViewRepo)
        {
            var query = (from u in this.DataToJoin
                         join c in commentViewRepo.DataToJoin
                          on u.LDI_ID equals c.LDI_ID into uc
                         from x in uc.DefaultIfEmpty()
                         where (!u.IsUploadNotComplete.HasValue) && Math.Abs(SqlFunctions.DateDiff("second", u.DataInserted, dateInserted) ?? 2) < 1
                         && u.UserID == userId
                         select new DerivedUploadData
                         {
                             DataSource = DBSource.Intermediate,
                             LDI_ID = u.LDI_ID,
                             ProjectName = u.ProjectName,
                             ServiceName = u.ServiceName,
                             PeriodEnding = u.PeriodEnding,
                             UnitsInFacility = u.UnitsInFacility,
                             MonthsInPeriod = u.MonthsInPeriod,
                             FHANumber = u.FHANumber,
                             TotalRevenues = u.TotalRevenues,
                             TotalExpenses = u.TotalExpenses,
                             MortgageInsurancePremium = u.MortgageInsurancePremium,
                             ActualNumberOfResidentDays = u.ActualNumberOfResidentDays,
                             ScoreTotal = u.ScoreTotal,
                             HasComment = x == null ? false : true,
                             NOIRatio = u.NOIRatio,
                             FHAQuarter = u.FHAQuarter,
                             AverageDailyRateRatio = u.AverageDailyRateRatio,
                             DebtCoverageRatio2 = u.DebtCoverageRatio2,
                             FHAInsuredPrincipalInterestPayment = u.FHAInsuredPrincipalInterestPayment,
                             DataInserted = u.DataInserted
                         }).Distinct();

            //umesh
            var temp = query.ToList();
            temp.ForEach(i => i.MonthsInPeriod_Disp = Convert.ToInt32(i.MonthsInPeriod));
            temp.ForEach(i => i.PeriodEnding_Disp = Convert.ToDateTime(i.PeriodEnding));


            var Interm = temp.SortAndPaginateList(strSortBy, sortOrder, pageSize, pageNum);
            return Interm;
        }
        #endregion

        public IEnumerable<ReportViewModel> GetMissingProjectsForLender(string username, string usertype,int lenderId, int monthsInPeriod, int year)
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in GetUploadStatusByLenderId, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_Get_MissingProjectReportsForLender_Result>(
                    "usp_HCP_Get_MissingProjectReportsForLender",
                    new
                    {
                        Username = username,
                        UserType = usertype,
                        LenderId = lenderId,
                        MonthsInPeriod = monthsInPeriod,
                        Year = year
                    }).ToList();
            return
                Mapper.Map<IEnumerable<usp_HCP_Get_MissingProjectReportsForLender_Result>, IEnumerable<ReportViewModel>>(results);
        }

        public Tuple<DateTime, int> GetLatestPeriodEnding(int? iLenderId)
        {
            Lender_DataUpload_Intermediate data = null;
            var elements = this.GetAll().ToList();
            if (iLenderId.HasValue)
            {
                if (elements.Any())
                {
                    if (elements.Any(p => !p.IsUploadNotComplete.HasValue && (p.LenderID == iLenderId.Value)))
                    {
                        data =
                            elements.Where(p => !p.IsUploadNotComplete.HasValue && (p.LenderID == iLenderId.Value)
                                                &&
                                                (Convert.ToInt32(p.MonthsInPeriod) == 3 ||
                                                 Convert.ToInt32(p.MonthsInPeriod) == 6 ||
                                                 Convert.ToInt32(p.MonthsInPeriod) == 9 ||
                                                 Convert.ToInt32(p.MonthsInPeriod) == 12))
                                .OrderByDescending((p => Convert.ToDateTime(p.PeriodEnding).Year))
                                .ThenByDescending(p => Convert.ToInt32(p.MonthsInPeriod))
                                .FirstOrDefault();
                    }
                    else
                    {
                        data =
                            elements.Where(
                                p => Convert.ToInt32(p.MonthsInPeriod) == 3 || Convert.ToInt32(p.MonthsInPeriod) == 6 ||
                                     Convert.ToInt32(p.MonthsInPeriod) == 9 || Convert.ToInt32(p.MonthsInPeriod) == 12)
                                .OrderByDescending((p => Convert.ToDateTime(p.PeriodEnding).Year))
                                .ThenByDescending(p => Convert.ToInt32(p.MonthsInPeriod))
                                .FirstOrDefault();
                    }

                }
            }
            else
            {
                if (elements.Any())
                {
                    data =
                        elements.Where(
                            p => Convert.ToInt32(p.MonthsInPeriod) == 3 || Convert.ToInt32(p.MonthsInPeriod) == 6 ||
                                 Convert.ToInt32(p.MonthsInPeriod) == 9 || Convert.ToInt32(p.MonthsInPeriod) == 12)
                            .OrderByDescending((p => Convert.ToDateTime(p.PeriodEnding).Year))
                            .ThenByDescending(p => Convert.ToInt32(p.MonthsInPeriod))
                            .FirstOrDefault();
                }
            }

            var results = Mapper.Map<ExcelUploadView_Model>(data);
            if (results != null)
            {
                return new Tuple<DateTime, int>(Convert.ToDateTime(results.PeriodEnding),
                    int.Parse(results.MonthsInPeriod));
            }
            return null;
        }

        public IEnumerable<ProjectInfoViewModel> GetProjectInfoReport(string username)
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in ProjectInfoViewModel, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_Get_ProjectInfoReport_Result>("usp_HCP_Get_ProjectInfoReport",
                new { Username = username }).ToList();
            return Mapper.Map<IEnumerable<ProjectInfoViewModel>>(results);
        }

        public int DistributeQuarterly(string QryDate="4/21/2016")
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in usp_HCP_DistributeQuaterly, please pass in correct context in unit of work.");

            return context.Database.ExecuteSqlCommandSimple("usp_HCP_DistributeQuaterly", new { @QueryDate = QryDate });
        }

        public int ApplyCalculations(string QryDate = "4/21/2016")
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in usp_HCP_ApplyCalculations, please pass in correct context in unit of work.");

            return context.Database.ExecuteSqlCommandSimple("usp_HCP_ApplyCalculations", new { @QueryDate = QryDate });
        }

        public int CalculateErrors(string QryDate = "4/21/2016")
        {
            var context = this.Context as HCP_intermediate;
            if (context == null)
                throw new InvalidCastException("context is not from db intermediate in usp_HCP_CalculateErrorss, please pass in correct context in unit of work.");

            return context.Database.ExecuteSqlCommandSimple("usp_HCP_CalculateErrors", new { @QueryDate = QryDate });
        }

        public IEnumerable<KeyValuePair<int, string>> GetPeriodEnding(string QryDate = "4/21/2016")
        {
            var dt = DateTime.ParseExact(QryDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            return this.GetAll().Where(a => DateTime.ParseExact(a.PeriodEnding, "yyyy-MM-dd", CultureInfo.InvariantCulture) >= dt).Select(p => new KeyValuePair<int, string>(p.LDI_ID, p.PeriodEnding)).OrderBy(p => p.Value);

        }

        public List<string> GetExistingFHAs(List<string> uploadItems) //fha_lenderId_periodEnding
        {
            if (uploadItems == null || uploadItems.Count == 0) return null;
            List<string> fhas = new List<string>();
            foreach (string t in uploadItems)
            {
                string fha = t.Substring(0, 9);
                int lenderId = Convert.ToInt32(t.Substring(10, 5));
                string periodEnding = t.Substring(16);

                //HCP_intermediate db = new HCP_intermediate();

                //var a = (from q in db.Lender_DataUpload_Intermediate
                //         select new { q.FHANumber }).ToList();

                var fs = (from i in this.DataToJoin
                          where i.FHANumber == fha && i.LenderID == lenderId && i.PeriodEnding == periodEnding
                          select i.FHANumber).Count();
                if (fs > 0 && !fhas.Contains(fha)) fhas.Add(fha);
            }
            return fhas;
        }

        public Tuple<string, int> GetLatestPeriodEnding(int lenderId, string fha, string queryDate)
        {
            DateTime start = Convert.ToDateTime(queryDate);
            Tuple<string, int> t = null;
            var fs = (from i in this.DataToJoin
                      where i.FHANumber == fha && i.LenderID == lenderId && i.FHANumber == fha && i.DataInserted >= start
                      orderby i.DataInserted descending
                      select new { PeriodEnding = i.PeriodEnding, MonthsInPeriod = i.MonthsInPeriod }).FirstOrDefault();
            if (fs != null)
            {
                t = new Tuple<string, int>(fs.PeriodEnding, Convert.ToInt32(fs.MonthsInPeriod));
            }
            return t;
        }
    }
}
