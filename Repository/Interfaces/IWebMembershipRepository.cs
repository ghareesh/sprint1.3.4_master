﻿namespace Repository.Interfaces
{
    public interface IWebMembershipRepository
    {
        void UnlockUserAccount(int userId);
        int? GetUserIdByToken(string userToken);
        int? GetUserIdByResetPwdToken(string resetPwdToken);
        void InitForceChangePassword(int userId);
    }
}
