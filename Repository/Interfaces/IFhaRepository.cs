﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IFhaRepository
    {
        IEnumerable<FhaInfoModel> GetFhasByLenderIds(string lenderIds);
        IEnumerable<FhaInfoModel> GetReadyFhasForInspectionContracotr();
        IEnumerable<FhaInfoModel> GetFhasByWLMIds(int wlmid);
        IEnumerable<FhaInfoModel> GetServicersFhasByLenderIds(string lenderIds, string ServicerId);
    }
}
