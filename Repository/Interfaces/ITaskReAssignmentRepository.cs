﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Model;


namespace Repository.Interfaces
{
    public interface ITaskReAssignmentRepository
    {
        IEnumerable<KeyValuePair<string, string>> GetAllTaskAgeIntervals();

        PaginateSortModel<TaskDetailPerAeModel> GetTaskDetailsForAE(string aeIds, DateTime? fromDate, DateTime? toDate,
            int fromTaskAge, int toTaskAge, string projectAction, int? page, string sort, SqlOrderByDirecton sortDir);

        void InsertReassignedTasks(IList<TaskReAssignmentViewModel> taskList);

        IList<TaskDetailPerAeModel> GetReassignedTasksForAE(string reAssignedTo, List<Guid> selectedTaskInstanceIds);
        IEnumerable<TaskReAssignmentViewModel> GetReAssginedTasksByUserName(string userName);
        void UpdateTaskReassignmentWithDeleted_Ind(List<Guid> taskInstanceIdList);
        string GetReassignedAE(Guid TaskInstanceId);
    }
}
