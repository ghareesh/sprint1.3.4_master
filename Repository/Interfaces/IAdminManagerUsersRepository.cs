﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IAdminManagerUsersRepository
    {
        IEnumerable<UserViewModel> GetDataAdminManageUser(string searchText, int page, string sort, SqlOrderByDirecton sortdir);
        //Naveen#543
        IEnumerable<UserViewModel> GetServicersrepo(int? lenderid);
    }
}
