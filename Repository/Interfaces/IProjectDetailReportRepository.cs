﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Repository.Interfaces
{
   public  interface IProjectDetailReportRepository
    {

       ReportModel GetProjectDetailReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016");
         //ReportModel GetProjectDetailReportForSuperUserWithWLM(string userName, string userType);
       ReportModel GetProjectDetailReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016");
       ReportModel GetProjectDetailReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016");
         //High Level Report
       ReportModel GetProjectDetailReportForSuperUserHighLevel(string userName, string userType, string SortOrder = "ASC", string QryDate = "4/21/2016");
         //Error Report
       ReportModel GetErrorReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016");
       ReportModel GetErrorReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016");
       ReportModel GetErrorReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016");

         //PT Report
       ReportModel GetPTReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016");
       ReportModel GetPTReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016");
       ReportModel GetPTReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016");

         //AE Current Quarter Report
       ReportModel GetCurrentQuarterReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016");
    }
}
