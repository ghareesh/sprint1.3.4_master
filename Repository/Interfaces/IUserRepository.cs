﻿using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IUserRepository : IJoinable<HCP_Authentication>
    {
        IEnumerable<UserViewModel> GetAllUsers();
        IEnumerable<UserViewModel> SearchByFirstOrLastName(string searchText, int maxResults, IAddressRepository addressRepo);
        PaginateSortModel<UserViewModel>
            SearchByFirstOrLastNamePageSort(string searchText, string strSortBy, SqlOrderByDirecton sortOrder,
            int pageSize, int pageNum, IAddressRepository addressRepo);
        UserViewModel GetUserById(int userId);
        void SavePasswordHist(int userId, string passwordHashHist);
        void UpdateUser(UserViewModel model);
        UserViewModel GetUploadUserByLdiID(int ldiID);
        UserViewModel GetUserByUsername(string username);
        string GetUserNameById(int userId);
        IEnumerable<UserInfoModel> GetOperatorsByLenderId(int lenderId);
        IEnumerable<UserInfoModel> GetLenderUsersByLenderId(int lenderId);
        IEnumerable<UserInfoModel> GetLenderUsersByAeUserId(int aeUserId);
        void ActivateOrInactivateUser(int userId, bool isUserInactive);
        bool? IsUserInactive(int userId);
        string GetNameAndRoleForUser(int userId);
        string GetNameByUserName(string username);

        string GetFullNameById(int pUserId); //skumar-form290- get full name by userid
        string GetUserLastNameByFirstName(string firstname);
        bool IsEmailExists(string email);//naveen 03-10-2019


    }
}
