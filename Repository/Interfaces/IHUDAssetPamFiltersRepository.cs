﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;
using Model.Production;

namespace Repository.Interfaces
{
    //class IHUDAssetPamFiltersRepository
    //{
    //}
    public interface IHUDAssetPamFiltersRepository
    {
        string SaveAssetPamReportFilterDetails(HUDAssetpamreportfiltermodel Model);
        HUDAssetpamreportfiltermodel GetAssetSavedFilter();
    }
}
