﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository.Interfaces.ProjectAction;
using Model.ProjectAction;

namespace Repository.Interfaces.ProjectAction
{
    public interface IGroupTaskRepository
    {
        GroupTask AddNewGroupTask(GroupTaskModel groupTaskModel);
        void UpdateGroupTask(ProjectActionViewModel  projectActionViewModel);
        bool CheckIfProjectActionExistsForFhaNumber(int projectActionTypeId, string fhaNumber);

        PaginateSortModel<GroupTaskModel> GetGroupTasksForLender(int lenderId, ILenderFhaRepository lenderFhaRepository,
            IProjectActionRepository projectActionRepository, IUserRepository userRepository);

        GroupTaskModel GetGroupTaskById(int taskId);
        void UnlockGroupTask(int taskId);
        void UpdateGroupTaskForCheckout(GroupTaskModel groupTaskModel);
        void UpdateOPAGroupTask(OPAViewModel OPAActionViewModel);
        void LockGroupTask(int taskId);

        GroupTaskModel GroupTaskbyFHAProjectAction(int projectActionTypeId, string fhaNumber);
        bool CheckIfFileExistsByGroupTaskId(int GroupTaskId);
        //Added by harish for group task @20032020
        GroupTaskModel GetGrouptaskbyTaskinstanceid(Guid taskIInstanceId);
    }
}
