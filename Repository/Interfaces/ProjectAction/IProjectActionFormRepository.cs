﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces.ProjectAction
{
    public interface IProjectActionFormRepository
    {
        IEnumerable<KeyValuePair<int, string>> GetProjectActionTypes();
        string GetProjectActionName(int projectActionId);
        Guid SaveProjectActionRequestForm(ProjectActionViewModel projectActionViewModel);
        void UpdateTaskId(ProjectActionViewModel model);
        ProjectActionViewModel GetProjectActionRequestId(Guid formId);
        void UpdateProjectActionRequestForm(ProjectActionViewModel projectActionViewModel);
        void InsertAEChecklistStatus(ProjectActionViewModel projectActionViewModel);

        void UpdateAEChecklistStatus(ProjectActionViewModel projectActionViewModel,
            IProjectActionAEFormStatusRepository projectActionAeFormStatusRepository);

        bool IsProjectActionApprovedOrDenied(Guid projectActionFormId);
        // harish added new method to get assetmanagement project action type 11032020
        string GetProjectActionNameforAssetmanagement(int projectActionId);
    }
}
