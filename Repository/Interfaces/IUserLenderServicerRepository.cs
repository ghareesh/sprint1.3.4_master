﻿using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using Repository.Interfaces.ProjectAction;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IUserLenderServicerRepository
    {
        void AddNewUserLenderRecord(User_Lender userLender);
        List<int> GetUserLenderRow(User_Lender userLender);
        void RemoveUserLenderRow(int userLenderId);
        List<string> GetAllowedFhaByLenderUserId(int lenderUserId, ILenderFhaRepository lenderFhaRepository);
        IEnumerable<int> GetLenderIdsByAeOrWlm(string userName, int monthsInPeriod, int year);
        IEnumerable<int?> GetLenderIdsByUserId(int userId);
        IEnumerable<string> GetAllowedFhasForUser(int userId);
        List<int?> GetLenderIdsByUser(int userId, ILenderFhaRepository lenderFhaRepository);

        List<string> GetAllowedNonCriticalFhaByLenderUserId(int lenderUserId, ILenderFhaRepository lenderFhaRepository,
            INonCriticalProjectInfoRepository nonCriticalProjectInfo);
        //Umesh
        IEnumerable<int> GetLenderOperatorsbyFHA(string fhanumber);

        IEnumerable<string> GetAllowedFhasForUsers(List<int> UserIds);

        IEnumerable<FhaInfoModel> GetSelectedFhasForInspector(int userId);
        //#293hareesh
        List<string> GetAllowedFhaByLender( ILenderFhaRepository lenderFhaRepository);
        // helpdesk fha numbere hareesh
        List<string> GetAllowedFhaByLenderDetails(int lenderUserId, ILenderFhaRepository lenderFhaRepository);


    }
}
