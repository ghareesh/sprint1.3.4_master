﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Model.Production;
using Model;

namespace Repository.Interfaces
{
    public interface ITaskFileRepository
    {
        Guid AddTaskFile(TaskFileModel model);
        TaskFileModel GetTaskFileById(int taskId);
        void UpdateTaskFile(TaskFileModel task);
        IEnumerable<TaskFileModel> GetTaskFileByTaskInstanceId(Guid taskFileId);
        IEnumerable<TaskFileModel> GetTaskFileByTaskFileId(Guid taskFileId);
        IEnumerable<TaskFileModel> GetTaskFileByTaskFileType(string TaskFileType);
        IEnumerable<TaskFileModel> GetTaskFileByTaskFileName(string TaskFileType);
        IEnumerable<TaskFileModel> GetTaskFileByTaskInstanceAndFileTypeId(Guid taskFileId, FileType fileType);
        TaskFileModel GetTaskFileByTaskInstanceAndFileType(Guid TaskInstanceId, string FileType);
        Guid AddGroupTaskFile(TaskFileModel fileModel);
        int DeleteGroupTaskFile(Guid taskInstanceId, Guid fileType);
        TaskFileModel GetGroupTaskFileByTaskInstanceAndFileTypeId(Guid taskInstanceId, Guid  fileType);
        IList<TaskFileModel> GetGroupTaskFilesByTaskInstance(Guid taskInstanceId);

        //OPA Changes
        TaskFileModel GetGroupTaskFileByTaskInstanceAndTaskFileID(Guid taskFileId, Guid taskInstanceID);
        int DeleteGroupTaskFileByTaskInstanceAndTaskFileID(Guid taskFileId, Guid taskInstanceID);
        int DeleteTaskFileByFileID(int FileID);
        void RenameFiles(List<Tuple<Guid, string, string>> fileRenameList);

        void UpdateDocType(TaskFileModel taskFile);

        //Sharepoint
        IEnumerable<SharepointAttachmentsModel> GetTaskFileForSharepoint(Guid taskInstanceId, string fileType);

        // harish added new method to get all taskfiles based on taskinstanceid 30-01-2020
        TaskFileModel GetTaskFileByTaskInstance(Guid TaskInstanceId);
        // harish added new method to update folder structure in taskfile db 31-01-2020
        void UpdateFolderinTaskfile(TaskFileModel task);

        // harish added 06042020
        TaskFileModel GetTaskFileByIdToUpdateDocid(int taskFileId);
        //harish added 06042020
        void UpdateDoctyidinTaskfile(TaskFileModel task);
        // harish added 06042020
        IEnumerable<TaskFileModel> GetAllTaskFileByTaskinstanceidinsteadofTaskGuid(Guid? taskInstanceId);
        //harish added 15052020
        AM_R4RAndNCREDocumentModel GetTransactionId(string ActionName);
    }
}
