﻿using System;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface ILenderFhaRepository: IJoinable<Lender_FHANumber>
    {
        DateTime GetLatestQuarter();
        IList<string> GetFhaNumbersForLender(int lenderId);

    }
}
