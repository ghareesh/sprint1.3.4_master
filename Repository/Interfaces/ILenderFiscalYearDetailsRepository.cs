﻿using System;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface ILenderFiscalYearDetailsRepository : IJoinable<LenderFiscalYearDetails>
    {
        int? GetFiscalYearEndingMonth(int lenderId, string fha);
        // harish added #584
        // harish added 04-12-2019 #584
        //int? getlenderrowdata(int lenderId, string fha);
        LenderFiscalYearDetails getlenderrowdata(int lenderId, string fha);
        // harish added #584
        int save(LenderFiscalYearDetails dataInModel);
        // harish added #584
        int Updateyear(LenderFiscalYearDetails dataInModel);
    }
}
