﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using Model.Production;

namespace Repository.Interfaces
{
    public interface IPAMRepository
    {
        //PAMReportModel GetPAMReportSummary(string userName, string aeIds, string isouIds, DateTime? fromDate, DateTime? toDate, int wlmId, int aeId, int isouId, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds);
        PAMReportModel GetPAMReportSummary(string userName, string aeIds, string isouIds, DateTime? fromDate, DateTime? toDate, int wlmId, int aeId, int isouId, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds
            , string propertyName, string fhaNumber, string assignedTO, int? hasOpened, int? daysOpened);//karri:D#610

        int GetWlmId(int userId);
        int GetAeId(int userId);
        int GetIsouId(int userId);
        PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string aeIds, string isouIds, string wlmIds, int? status,
            string projectAction, int wlmId, int aeId, int isouId, DateTime? fromDate, DateTime? toDate);

        IEnumerable<KeyValuePair<int, string>> GetPamProjectActionTypes();
        //bool SaveHUDPamFilters(HUDPamReportFiltersModel Model);
    }

}
