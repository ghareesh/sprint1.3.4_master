﻿using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
	public interface IProd_FormAmendmentTaskRepository
	{

		Prod_FormAmendmentTaskModel GetFormAmendmentById(int pAmendmentTemplateID);
		IList<Prod_FormAmendmentTaskModel> GetFormAmendmentByTaskInstanceId(Guid pTaskInstanceId);

		bool SaveAmendment(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel);
		IList<Prod_FormAmendmentTaskModel> GetFormAmendmentByFHANumber(string pFHANumber);
		bool SaveDapFromAmendmentSP(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel);


	}
}
