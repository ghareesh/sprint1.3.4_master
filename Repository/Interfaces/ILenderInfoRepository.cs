﻿using System.Collections.Generic;
using System.Linq;
using HUDHealthcarePortal.Model;
namespace Repository.Interfaces
{
    public interface ILenderInfoRepository
    {
        IEnumerable<KeyValuePair<int, string>> GetAllLenders();
        IEnumerable<KeyValuePair<int, string>> GetLenderDetail(int lenderId);
        IList<MenuRestrictionsViewmodel> GetMenuRestrictedLenders();
        string GetLenderName(int lenderId);
    }
}
