﻿

using HUDHealthcarePortal.Model;
using Model;
using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.OPAForm
{
    public interface IOPARepository
    {
        void UpdateTaskId(OPAViewModel model);
        string GetOPAName(int opaId);
        Guid SaveOPAForm(OPAViewModel model);
        void UpdatOPARequestForm(OPAViewModel opaViewModel);
        OPAViewModel GetProjectActionRequestId(Guid formId);
        List<OPAHistoryViewModel> GetOpaHistory(Guid taskInstanceId);
        List<TaskModel> GetChildTasks(Guid taskInstanceId);

        List<OPAWorkProductModel> getAllOPAWorkProducts(Guid taskInstanceId, string fileType);

        //void UpdateAdditionalInfo(OPAViewModel opaViewModel);
        OPAViewModel OPATaskbyFHAProjectAction(int projectActionTypeId, string fhaNumber);
        OPAViewModel GetOPAByTaskId(int taskId);
        OPAViewModel GetOPAByChildTaskId(Guid childTaskInstanceId);
        List<OPAHistoryViewModel> GetOpaHistoryForRequestAdditionalInfo(Guid childTaskInstanceId);
        OPAViewModel GetOPAByTaskInstanceId(Guid taskIInstanceId);
        List<Guid> GeetChildTaskforReassignment(List<Guid> Joinlist, string assignedto);
        
        List<OPAHistoryViewModel> GetOpaFileCommentHistory(int taskFileId);
        List<OPAHistoryViewModel> GetOpaFileUploadHistory(int taskFileId);
        List<Prod_SubFolderStructureModel> GetSubFolder(int parentFolderkey, string propertyId, string fhaNumber);
        Prod_Assainedto AssignedTo(int Pageid, string FhaNumber);
        List<Prod_SubFolderStructureModel> GetFolderInfoByKey(int folderkey);
        List<int> GetFileIDByFolderKey(int folderkey);
        
        int SaveSubFolder(Prod_SubFolderStructureModel model);

        #region Production Application
        List<OPAHistoryViewModel> GetProdOpaHistoryReviewersList(int userId, int viewId, int fileId);
        List<OPAHistoryViewModel> GetProdOpaHistoryFileList(Guid taskInstanceId, int userId, int viewId, int folderKey);
        //List<OPAHistoryViewModel> GetProdOpaHistoryFileList(Guid taskInstanceId, int userId, int viewId, int folderKey, int notAssigned);
		List<Prod_OPAHistoryViewModel> GetProdOpaHistory(Guid taskInstanceId,string PropertyId,string FhaNumber, int userId, int viewId, bool isEdit);
        List<Prod_OPAHistoryViewModel> GetProdLenderUpload(Guid taskInstanceId, string PropertyId, string FhaNumber);
        
        List<Prod_OPAHistoryViewModel> GetProdLenderUploadForAmendments(Guid taskInstanceId, string PropertyId, string FhaNumber);

        OPAViewModel GetProdOPAByChildTaskId(Guid childTaskInstanceId);
        OPAViewModel GetProdOPAByTaskXrefId(Guid taskXrefId);
        List<TaskModel> GetProdChildTasksByXrefId(Guid taskXrefId, bool isXref);
        OPAViewModel GetProdOPAByFha(string FhaNumber);
        List<Prod_OPAHistoryViewModel> GetGetWPUpload(Guid taskInstanceId,int Status, int PageTypeId);
        string GetTaskId(int pageTypeId, string FHANumber, Guid TaskInstanceId);
        object FormNameBYPageTypeID(int pageTypeId);
        #endregion

        //Naveen Hareesh 19-11-2019
        void UpdateOPAGroupTask(OPAViewModel OPAActionViewModel);

        // #605 siddu
        List<OPAWorkProductModel> getAllOPAWorkInternalProducts(Guid taskInstanceId);
        //#664
        List<OPAHistoryViewModel> GetOpaHistoryRAI(Guid taskInstanceId);

        // harish added new method to get data from  07-01-2020
        List<OPAWorkProductModel> RISgetAllOPAWorkInternalProducts(Guid taskInstanceId);
        // harish added new line of code for updating projectactiontypeid in opaform table 29-01-2020
        int Updateprojectactiontypeid(OPAViewModel dataInModel, int Projectactiontypeid);

        //karri, venkat#832
        int GetForm290ClosingPreChecksValue(string FHANumber);
    }
}
