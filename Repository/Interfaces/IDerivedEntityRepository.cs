﻿using System.Collections.Generic;

namespace Repository.Interfaces
{
    public interface IDerivedEntityRepository<TDerivedEntity>
    {
        IEnumerable<TDerivedEntity> GetBySqlQuery(string sqlQuery, params object[] parameters);
        IEnumerable<TDerivedEntity> GetBySqlQueryWithPageSort(int pageNum, int pageSize, string sortBy, string sortDir, params object[] parameters);
    }
}
