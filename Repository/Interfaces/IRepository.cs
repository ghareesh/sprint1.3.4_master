﻿using System.Collections.Generic;

namespace Repository.Interfaces
{
    interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        //IQueryable<TEntity> Find(Func<TEntity, bool> predicate);
        //TEntity GetByID(int id);
        //void Insert(TEntity entity);
        //void Delete(int id);
        //void Delete(TEntity entityToDelete);
        //void Update(TEntity entityToUpdate);
    }
}
