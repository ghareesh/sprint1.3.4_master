﻿using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class Prod_FormAmendmentTaskRepository : BaseRepository<Prod_FormAmendmentTask>, IProd_FormAmendmentTaskRepository
	{
		public Prod_FormAmendmentTaskRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public Prod_FormAmendmentTaskRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<Prod_FormAmendmentTask> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public Prod_FormAmendmentTaskModel GetFormAmendmentById(int pAmendmentTemplateID)
		{
			var amendment = this.GetByIDFast(pAmendmentTemplateID);
			return Mapper.Map<Prod_FormAmendmentTaskModel>(amendment);

		}
	}
}
