﻿using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Model;
using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Production
{
   public  interface ITaskFile_FolderMappingRepository
    {
       int AddTaskFile_FolderMapping(TaskFile_FolderMappingModel model);
       IEnumerable<Prod_FolderStructureModel> GetFolderbyViewId(int ViewId);
       IEnumerable<TaskFileModel> GetMappedFilesbyFolder(int[] folderkeys, Guid InstanceId);
       IEnumerable<DocumentTypeModel> GetMappedDocTypesbyFolder(int folderkey);
       IEnumerable<Prod_FolderStructureModel> GetFolderListbyTaskInstanceId(Guid taskInstanceId);
       IEnumerable<OPAViewModel> GetProdReviewersTaskStatus(Guid taskInstanceId);
       IEnumerable<OPAViewModel> GetProdPendingRAI(Guid taskInstanceId);
        //harish added new interface
        IEnumerable<AM_Document> GetAM_Asstmanagement(int projectactiontypeid);

        // harihs added new method to get pending rai
        IEnumerable<OPAViewModel> GetOPAPendingRAI(Guid taskInstanceId);
    }
}
