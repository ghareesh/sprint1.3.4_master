﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;

namespace Repository.Interfaces.Production
{
    public interface IFHANumberRequestRepository
    {
        Guid AddFHARequest(FHANumberRequestViewModel model);
        bool UpdateFHARequest(FHANumberRequestViewModel model);
        IEnumerable<ProductionQueueLenderInfo> GetFhaRequests(List<Guid> taskInstances);
        FHANumberRequestViewModel GetFhaRequestById(Guid instanceId);
       List<string> GetReadyforAppFhas();
       List<string> GetReadyforConSingleStageFhas();
       List<string> GetReadyforConTwoStageFhas();
       
        FHANumberRequestViewModel GetFhaRequestByTaskInstanceId(Guid taskInstanceId);
        IEnumerable<FhaSubmittedLendersModel> GetFhaSubmittedLenders();
        IEnumerable<AssignedunderwriternamesModel> GetprodAssignedUnderwriternames();
        ApplicationDetailViewModel GetApplicationDetailsForSharePointScreen(Guid taskInstanceId);

        //bool UpdateLoanAmount(string fhaNumber, decimal loanAmount);//karri#672
        bool UpdateLoanAmount(string fhaNumber, decimal loanAmount, string projName, string origFHANumber, string origProjName, decimal origLoanAmount);

        //bool UpdateLoanAmount(string fhaNumber, decimal loanAmount);
        bool IsRequestExistsForFhaNumber(string fhaNumber);
        IList<Prod_FhaAgingReportViewModel> GetFhaAgingReport();
        FHANumberRequestViewModel GetFhaRequestByFhaNumber(string fhaNumber);
        IList<FHANumberRequestViewModel> GetAllFHARequests();
        bool FhataskAssignedfromQueDateUpdate(Guid TaskinstanceId);
    }
}
