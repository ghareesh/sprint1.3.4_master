﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;

namespace Repository.Interfaces.Production
{
    public interface IProd_ProjectTypeRepository
    {
        IList<ProjectTypeModel> GetAllProjectTypes();
        IList<ProjectTypeModel> GetAllProjectTypesForLC();
        IList<ProjectTypeModel> GetProjectTypesByAppType(string pageTypeIds);
        string GetProjectTypebyID(int projecttypeid);
        IList<ProjectTypeModel> GetAllotherloanTypes();
        int GetProjectTypebyName(string projectTypeName);
        string GetUserInfoByID(int UserID);
        //harish added 664
        string GetProjectActionTypebyID(int projecttypeid);
    }
}
