﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Production
{
   public interface IDocumentTypeRespository
    {
        DocumentTypeModel GetDocumentTypeById(int id);
        List<DocumentTypeModel> GetAllDocTypes();
        DocumentTypeModel GetDocumentTypeIDByShortname(string ShortName, int folderkey);
        DocumentTypeModel GetDocumentTypeId(string filename,int folderkey);
    }
}
