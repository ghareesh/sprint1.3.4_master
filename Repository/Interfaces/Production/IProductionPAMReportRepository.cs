﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using Model.Production;

namespace BusinessService.Interfaces.Production
{
    public interface IProductionPAMReportRepository
    {
        ProductionPAMReportModel GetProductionPAMReportSummary(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate, string status, string Level, Guid? grouptaskinstanceid,string Mode="Other");
        ProductionPAMReportModel GetProductionLenderPAMReportSummary(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes);
        ProductionPAMReportModel GetLenderPAMSubGridChildTasks(Guid taskInstanceId);
        ProductionPAMReportModel GetProductionLenderPAMExcel(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes);
        PAMReportTestGrid1ViewModel GetProdPAMGridOneResults(Guid taskInstanceId);
        PAMReportTestGrid2ViewModel GetProdPAMGridTwoResults(Guid taskInstanceId);
        IList<PAMReportV3ViewModel> GetProdHUDPAMReportResults();
		IList<PAMReportV3ViewModel> GetProdHUDPAMSubReportResults(Guid pTaskInstanceId, string pFHANumber);//skumar-2838

		ProdPAMReportStatusGridViewModel GetProdPAMReportStatusGridResults();
    }
}
