﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Model.Production;

namespace Repository.Interfaces.Production
{
    public  interface IProd_ViewRepository
    {
        List<Prod_ViewModel> GetViewsbyType(int TypeID);
        List<Prod_ViewModel> GetUnAssignedViewsbyTaskInstanceId(int TypeID,Guid taskinstanceid);
    }
}
