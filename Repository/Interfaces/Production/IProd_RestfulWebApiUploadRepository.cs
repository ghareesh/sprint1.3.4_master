﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;
using System.Web;

namespace Repository.Interfaces.Production
{
  public interface IProd_RestfulWebApiUploadRepository
    {
        
      RestfulWebApiResultModel AssetManagementUploadDocumentUsingWebApi(RestfulWebApiUploadModel file, string token, HttpPostedFileBase myFile, string requestType);
      RestfulWebApiResultModel UploadDocumentUsingWebApi(RestfulWebApiUploadModel file, string token, HttpPostedFileBase myFile, string requestType);
      RestfulWebApiResultModel uploadSharepointPdfFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary);
      RestfulWebApiResultModel uploadCopiedFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary, string filename);

        //#868 harish added 06052020 new api call for to save share point pdf files
        RestfulWebApiResultModel AssetManagementuploadSharepointPdfFile(RestfulWebApiUploadModel documentInfo, string token, Byte[] binary);
        //#868harish added 07052020 R4R and NCRE filesUPload
        RestfulWebApiResultModel AssetManagementR4RAndNCREUploadDocumentUsingWebApi(RestfulWebApiUploadModel DocumentInfo, string token, HttpPostedFileBase UplodFile, string requestType);
    }
}
