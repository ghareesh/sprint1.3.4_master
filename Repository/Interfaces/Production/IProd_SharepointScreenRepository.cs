﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;

namespace Repository.Interfaces.Production
{
    public interface IProd_SharepointScreenRepository
    {
        bool SaveOrUpdateGeneralInformationDetailsForSharepoint(GeneralInformationViewModel generalInfoModel);
        SharePointCommentsModel GetSharepointComments(Guid taskInstanceId);
        bool AddOrUpdateComments(Guid taskInstanceId, string fhaNumber, string comment, int reviewerType);
        void SaveOrUpdateContractorsPayment(Guid taskInstanceId, string fhaNumber, decimal contractorPrice, decimal secondaryAmntPaid, decimal amountPaid);
        SharePointClosingInfoModel GetSharePointClosingInfo(Guid taskInstanceId);
        bool SaveOrUpdateClosingInfo(ClosingInfo model, string fhaNumber);
        MiscellaneousInformationViewModel GetSharePointDetailsForMiscellaneousInfo(
            MiscellaneousInformationViewModel miscInfoModel);
        bool SaveOrUpdateMiscellaneousInfoForSharepoint(MiscellaneousInformationViewModel miscInfoModel);
        AllSharepointData GetSharepointDataById(Guid taskInstanceId);
        void SaveOrUpdateOgcAddress(string fhaNumber, Guid taskInstanceId, int addressId, int reviewerTypeId);
        int GetAccountExecutiveById(Guid pTaskinstanceId);

        //karri#672
        bool SaveAuditTrialNCREInitBalanceInfo(string fhaNumber_ref, int userid, int attribid, string oldvalue, string newvalue);

    }
}

