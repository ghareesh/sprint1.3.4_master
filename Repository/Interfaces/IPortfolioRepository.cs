﻿using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IPortfolioRepository
    {
        IEnumerable<DerivedUploadData> GetPortfolios(string quarterToken);
        IEnumerable<DerivedUploadData> GetPortfolios(int page, int pageSize, string sort, SqlOrderByDirecton sortOrder, string quarterToken);
        IEnumerable<DerivedUploadData> GetPortfolioDetail(int page, int pageSize, string sort, SqlOrderByDirecton sortOrder, string portfolioNumber, string quarterToken);
    }
}
