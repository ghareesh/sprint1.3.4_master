﻿using System.Linq;
using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using Model.Production;
using Repository.Interfaces;
using TaskDB = EntityObject.Entities.HCP_task;
using Model;

namespace HUDHealthcarePortal.Repository
{
    public class TaskFileRepository : BaseRepository<TaskDB.TaskFile>, ITaskFileRepository
    {
        public TaskFileRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }
        public TaskFileRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public Guid AddTaskFile(TaskFileModel fileModel)
        {
            var taskFile = Mapper.Map<TaskDB.TaskFile>(fileModel);
            if (!Find(p => p.TaskInstanceId == fileModel.TaskInstanceId).ToList().Any())
            {
                this.InsertNew(taskFile);
            }
            return taskFile.TaskFileId;
        }

        public Guid AddGroupTaskFile(TaskFileModel fileModel)
        {
            var taskFile = Mapper.Map<TaskDB.TaskFile>(fileModel);
            if (fileModel.FileType != FileType.WP)             
                taskFile.FileType = fileModel.GroupFileType;
            taskFile.RoleName = UserPrincipal.Current.UserRole;
            //TaskDB.TaskFile taskFile = new TaskDB.TaskFile();
            //taskFile.TaskFileId  = fileModel.TaskFileId;
            //taskFile.FileData = fileModel.FileData;
            //taskFile.FileName = fileModel.FileName;
            //taskFile.FileType = fileModel.GroupFileType;
            //taskFile.SystemFileName = fileModel.SystemFileName;
            //taskFile.TaskInstanceId = fileModel.TaskInstanceId;  
            //taskFile.UploadTime = fileModel.UploadTime;
            this.InsertNew(taskFile);
            return taskFile.TaskFileId;
        }

        public IList<TaskFileModel> GetGroupTaskFilesByTaskInstance(Guid taskInstanceId)
        {
            IList<TaskFileModel> taskFileList = new List<TaskFileModel>();

            var context =  this.Context as TaskDB.HCP_task; 
            var taskFiles = context.TaskFiles.Where(p => p.TaskInstanceId == taskInstanceId);
            // var taskFileModel = Mapper.Map< TaskFileModel>(taskFile);
            if (taskFiles != null)
            {
                foreach (var taskFile in taskFiles.ToList())
                {
                    TaskFileModel taskFileModel = new TaskFileModel();
                    taskFileModel.TaskFileId = taskFile.TaskFileId;
                    taskFileModel.FileData = taskFile.FileData;
                    taskFileModel.FileName = taskFile.FileName;
                    taskFileModel.GroupFileType = taskFile.FileType;
                    taskFileModel.SystemFileName = taskFile.SystemFileName;
                    taskFileModel.TaskInstanceId = taskFile.TaskInstanceId;
                    taskFileModel.UploadTime = taskFile.UploadTime;
                    taskFileModel.GroupFileType = taskFile.FileType;
                    taskFileList.Add(taskFileModel);
                }
            }
            return taskFileList;
        }

        public  TaskFileModel GetGroupTaskFileByTaskInstanceAndFileTypeId(Guid taskInstanceId, Guid fileType)
        {
            var strFileType = fileType.ToString();
            var taskFile = this.Find(p => p.TaskInstanceId == taskInstanceId && p.FileType == strFileType).FirstOrDefault();
           // var taskFileModel = Mapper.Map< TaskFileModel>(taskFile);
            if (taskFile != null)
            {
                TaskFileModel taskFileModel = new TaskFileModel();
                taskFileModel.TaskFileId = taskFile.TaskFileId;
                taskFileModel.FileData = taskFile.FileData;
                taskFileModel.FileName = taskFile.FileName;
                taskFileModel.GroupFileType = taskFile.FileType;
                taskFileModel.SystemFileName = taskFile.SystemFileName;
                taskFileModel.TaskInstanceId = taskFile.TaskInstanceId;
                taskFileModel.UploadTime = taskFile.UploadTime;

                taskFileModel.GroupFileType = taskFile.FileType;
                return taskFileModel;
            }
            return null;
        }


        public int DeleteGroupTaskFile(Guid taskInstanceId, Guid fileType)
        {
            var filetype = fileType.ToString();
            var entityToUpdate = this.Find(p => p.TaskInstanceId == taskInstanceId && p.FileType == filetype).FirstOrDefault();
            if (entityToUpdate != null)
            {
                this.Delete(entityToUpdate);
                return 1;
            }
            return 0;
        }

        public TaskFileModel GetTaskFileById(int taskFileId)
        {
            var taskFile = this.GetByIDFast(taskFileId);
            var TaskFileModel = Mapper.Map<TaskFileModel>(taskFile);
            return TaskFileModel;
        }
        public TaskFileModel GetTaskFileByTaskInstanceAndFileType(Guid TaskInstanceId, string FileType)
        {
            var taskFile = this.Find(p => p.TaskInstanceId == TaskInstanceId && p.FileType == FileType).FirstOrDefault();
            var TaskFileModel = Mapper.Map<TaskFileModel>(taskFile);
            return TaskFileModel;
        }
        public IEnumerable<TaskFileModel> GetTaskFileByTaskInstanceAndFileTypeId(Guid taskInstanceId, FileType fileType)
        {
            var strFileType = fileType.ToString();
            var taskFile = this.Find(p => p.TaskInstanceId == taskInstanceId && p.FileType == strFileType);
            return Mapper.Map<IEnumerable<TaskFileModel>>(taskFile);
        }

        public IEnumerable<TaskFileModel> GetTaskFileByTaskInstanceId(Guid taskInstanceId)
        {
            var taskFile = this.Find(p => p.TaskInstanceId == taskInstanceId);
            return Mapper.Map<IEnumerable<TaskFileModel>>(taskFile);
        }
        public IEnumerable<TaskFileModel> GetTaskFileByTaskFileId(Guid taskFileId)
        {
            var taskFile = this.Find(p => p.TaskFileId == taskFileId);
            return Mapper.Map<IEnumerable<TaskFileModel>>(taskFile);
        }
        public IEnumerable<TaskFileModel> GetTaskFileByTaskFileType(string taskFileType)
        {
            var taskFile = this.Find(p => p.FileType == taskFileType);
            return Mapper.Map<IEnumerable<TaskFileModel>>(taskFile);
        }

        public IEnumerable<TaskFileModel> GetTaskFileByTaskFileName(string fileName)
        {
            var taskFile = this.Find(p => p.FileName == fileName);
            return Mapper.Map<IEnumerable<TaskFileModel>>(taskFile);
        }

        public void UpdateTaskFile(TaskFileModel taskFile)
        {
            var entityToUpdate = this.Find(p => p.TaskInstanceId == taskFile.TaskInstanceId).FirstOrDefault();
            if (entityToUpdate != null)
            {
                entityToUpdate.FileName = taskFile.FileName;
                entityToUpdate.SystemFileName = taskFile.SystemFileName; 
                entityToUpdate.FileData = taskFile.FileData;
            }
            this.Update(entityToUpdate);
        }


        public TaskFileModel GetGroupTaskFileByTaskInstanceAndTaskFileID(Guid taskFileId, Guid taskInstanceID)
        {

            var taskFile = this.Find(p => p.TaskInstanceId == taskInstanceID && p.TaskFileId == taskFileId).FirstOrDefault();
            // var taskFileModel = Mapper.Map< TaskFileModel>(taskFile);
            if (taskFile != null)
            {
                TaskFileModel taskFileModel = new TaskFileModel();
                taskFileModel.TaskFileId = taskFile.TaskFileId;
                taskFileModel.FileData = taskFile.FileData;
                taskFileModel.FileName = taskFile.FileName;
                taskFileModel.GroupFileType = taskFile.FileType;
                taskFileModel.SystemFileName = taskFile.SystemFileName;
                taskFileModel.TaskInstanceId = taskFile.TaskInstanceId;
                taskFileModel.UploadTime = taskFile.UploadTime;

                taskFileModel.GroupFileType = taskFile.FileType;
                return taskFileModel;
            }
            return null;
        }

        public int DeleteGroupTaskFileByTaskInstanceAndTaskFileID(Guid taskFileId, Guid taskInstanceID)
        {

            var entityToUpdate = this.Find(p => p.TaskInstanceId == taskInstanceID && p.TaskFileId == taskFileId).FirstOrDefault();
            if (entityToUpdate != null)
            {
                this.Delete(entityToUpdate);
                return 1;
            }
            return 0;
        }


        public   int DeleteTaskFileByFileID(int FileID)
        {
            var entityToUpdate = this.Find(p => p.FileId == FileID).FirstOrDefault();
            if (entityToUpdate != null)
            {
                this.Delete(entityToUpdate);
                return 1;
            }
            return 0;
        }

        public void RenameFiles(List<Tuple<Guid, string,string>> fileRenameList)
        {
             var context = (TaskDB.HCP_task)this.Context;
            TaskDB.TaskFile entityToUpdate;
            Guid taskFileId;
            foreach (var item in fileRenameList)
            {
                taskFileId = item.Item1;
                entityToUpdate = this.Find(p => p.TaskFileId == taskFileId).FirstOrDefault();
                if (entityToUpdate != null) entityToUpdate.FileName = item.Item2 ;
                this.Update(entityToUpdate);
                context.SaveChanges();
                var childFileIdList = (from n in context.TaskFileHistory where n.ParentTaskFileId == item.Item1 select n.ChildTaskFileId)
                    .ToList();
                foreach (var fileId in childFileIdList)
                {
                    var id = fileId;
                    entityToUpdate = this.Find(p => p.TaskFileId == id).FirstOrDefault();
                    if (entityToUpdate != null) entityToUpdate.FileName = item.Item2 ;
                    this.Update(entityToUpdate);
                    context.SaveChanges();
                }
            }
        }

        public IEnumerable<SharepointAttachmentsModel> GetTaskFileForSharepoint(Guid taskInstanceId, string fileType)
        {
            IQueryable<TaskDB.TaskFile> taskFile = null;
            if ((SharepointSections)Enum.Parse(typeof(SharepointSections), fileType) == SharepointSections.all)
            {
                var sharepointSections =
                    Enum.GetValues(typeof (SharepointSections))
                        .Cast<SharepointSections>()
                        .Select(p => p.ToString())
                        .ToList();
                taskFile = Find(p => p.TaskInstanceId == taskInstanceId && sharepointSections.Contains(p.FileType));
            }
            else
            {
                taskFile = this.Find(p => p.TaskInstanceId == taskInstanceId && p.FileType == fileType);
            }

           
            return Mapper.Map<IEnumerable<SharepointAttachmentsModel>>(taskFile);
        }


        public void UpdateDocType(TaskFileModel task)
        {
            var entityToUpdate = this.GetByIDFast(task.FileId);
            if (entityToUpdate != null)
            {
                entityToUpdate.DocTypeID = task.DocTypeID;
                entityToUpdate.FileName = task.FileName;
                this.Update(entityToUpdate);
                UnitOfWork.Save();
            }
        }

        //harish added new method to get taskfiles based on taskinstance id 30-01-2020
        public TaskFileModel GetTaskFileByTaskInstance(Guid TaskInstanceId)
        {
            var taskFile = this.Find(p => p.TaskInstanceId == TaskInstanceId).FirstOrDefault();
            var TaskFileModel = Mapper.Map<TaskFileModel>(taskFile);
            return TaskFileModel;
        }

        // harish added new method to update folder structure in db
        public void UpdateFolderinTaskfile(TaskFileModel task)
        {
            var entityToUpdate = this.GetByIDFast(task.FileId);
            if (entityToUpdate != null)
            {
                entityToUpdate.FolderName = task.FolderName;
                this.Update(entityToUpdate);
                UnitOfWork.Save();
            }
        }

        //harish added 03042020 
        public TaskFileModel GetTaskFileByIdToUpdateDocid(int taskFileId)
        {
            var taskFile = this.GetByIDFast(taskFileId);
            var TaskFileModel = Mapper.Map<TaskFileModel>(taskFile);
            return TaskFileModel;
        }

        // harish added new method to update doctyepid 03042020
        public void UpdateDoctyidinTaskfile(TaskFileModel task)
        {
            var entityToUpdate = this.GetByIDFast(task.FileId);
            if (entityToUpdate != null)
            {
                entityToUpdate.DocTypeID = task.DocTypeID;
                entityToUpdate.FileName = task.FileName;
                this.Update(entityToUpdate);
                UnitOfWork.Save();
            }
        }
        // harish added new method to get Taskfile 03042020
        public IEnumerable<TaskFileModel> GetAllTaskFileByTaskinstanceidinsteadofTaskGuid(Guid? taskInstanceId)
        {
            var taskFile = this.Find(p => p.TaskInstanceId == taskInstanceId);
            return Mapper.Map<IEnumerable<TaskFileModel>>(taskFile);
        }

        // harish r4r & ncre 07052020
        public AM_R4RAndNCREDocumentModel GetTransactionId(string ActionName)
        {
            var context = (TaskDB.HCP_task)this.Context;
            context.Database.CommandTimeout = 600;
            var Documents = (from n in context.AMR4RAndNCREDocument
                             where n.FileName == ActionName
                             select n).FirstOrDefault();
            var result = Mapper.Map<AM_R4RAndNCREDocumentModel>(Documents);
            // var r = Mapper.Map<AM_R4RAndNCREDocumentModel>(Documents);
            return result;


        }
    }
}
