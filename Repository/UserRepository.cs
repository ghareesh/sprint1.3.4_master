﻿using System.Data;
using System.Data.SqlClient;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    /// <summary>
    /// This class interacts with appropriate data access objects to fetch data related to user accounts
    /// </summary>
    public class UserRepository : BaseRepository<HCP_Authentication>, IUserRepository
    {
        public UserRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public UserRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<HCP_Authentication> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public UserViewModel GetUserById(int userId)
        {
            var user = this.Find(p => p.UserID == userId, "Address").FirstOrDefault();
            var userModel = Mapper.Map<UserViewModel>(user);
            if (user != null) userModel.AddressModel = Mapper.Map<AddressModel>(user.Address);
            if (!string.IsNullOrEmpty(userModel.SelectedTimezoneId))
            {
                var enumTimezone = EnumType.Parse<HUDTimeZone>(userModel.SelectedTimezoneId);
                userModel.SelectedTimezone = EnumType.GetEnumDescription(enumTimezone);
            }
            return userModel;
        }

        public UserViewModel GetUserByUsername(string username)
        {
            var addressRepo = new AddressRepository(this.UnitOfWork);
            var user = this.Find(p => p.UserName == username).FirstOrDefault();
            if (user == null)
                return null;
            var userModel = Mapper.Map<UserViewModel>(user);
            if (userModel != null)
            {
                userModel.AddressModel = userModel.AddressID.HasValue ? addressRepo.GetAddressByID(userModel.AddressID.Value) : new AddressModel();
            }
            return userModel;
        }

        public string GetUserNameById(int userId)
        {
            var addressRepo = new AddressRepository(this.UnitOfWork);
            var user = this.Find(p => p.UserID == userId).FirstOrDefault();
            if (user == null) return null;
            else return user.UserName;
        }

        /// <summary>
        /// get full name by id 
        /// </summary>
        /// <param name="pUserId"></param>
        /// <returns></returns>
        public string GetFullNameById(int pUserId)
        {
            var objUser = this.Find(p => p.UserID == pUserId).FirstOrDefault();
            string fName = string.Empty;
            string mName = string.Empty;
            string lName = string.Empty;
            string name = string.Empty;

            if (pUserId <= 0)
                return string.Empty;
            if (objUser == null) return string.Empty;

            fName = objUser.FirstName;
            lName = objUser.LastName;
            mName = !string.IsNullOrEmpty(objUser.UserMInitial) ? objUser.UserMInitial : " ";
            name = fName + " " + mName + " " + lName;
            return name;
        }

        public UserViewModel GetUploadUserByLdiID(int ldiID)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetUploadUserByLdiID, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetUploadUserByLdiID_Result>("usp_HCP_GetUploadUserByLdiID",
                    new { LdiID = ldiID }).FirstOrDefault();
            return Mapper.Map<usp_HCP_GetUploadUserByLdiID_Result, UserViewModel>(results);
        }

        public IEnumerable<UserInfoModel> GetOperatorsByLenderId(int lenderId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetOperatorByLenderId, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetOperatorsByLenderId_Result>("usp_HCP_GetOperatorsByLenderId",
                    new { LenderId = lenderId });
            return Mapper.Map<IEnumerable<UserInfoModel>>(results);
        }

        public IEnumerable<UserInfoModel> GetLenderUsersByLenderId(int lenderId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetLenderUsersByLenderId, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetLenderUsersByLenderId_Result>(
                    "usp_HCP_GetLenderUsersByLenderId",
                    new { LenderId = lenderId });
            return Mapper.Map<IEnumerable<UserInfoModel>>(results);
        }

        public IEnumerable<UserInfoModel> GetLenderUsersByAeUserId(int aeUserId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetLenderUsersByAeUserId, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetLenderUsersByAeUserId_Result>(
                    "usp_HCP_GetLenderUsersByAeUserId",
                    new { UserId = aeUserId });
            return Mapper.Map<IEnumerable<UserInfoModel>>(results);
        }

        /// <summary>
        /// this method we are not using it, bec. there is a duplicate method GetDataAdminManageUser avaliable in AdminManageUsersRepository
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserViewModel> GetAllUsers()
        {
            var context = this.Context as HCP_live;
            var addressRepo = new AddressRepository(this.UnitOfWork);
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetDataUploadSummaryByUserId, please pass in correct context in unit of work.");
            var userAll =
                context.Database.SqlQuerySimple<usp_HCP_Get_Admin_ManageUsers_View_Result>(
                    "usp_HCP_Get_Admin_ManageUsers_View").ToList();
            // should just show active users, add deleted_ind to soft delete
            // var userAll = this.GetAll();
            var users = Mapper.Map<IEnumerable<UserViewModel>>(userAll);
            foreach (var item in users)
            {
                item.AddressModel = item.AddressID.HasValue ? addressRepo.GetAddressByID(item.AddressID.Value) : new AddressModel();
                if (!string.IsNullOrEmpty(item.SelectedTimezoneId))
                {
                    var enumTimezone = EnumType.Parse<HUDTimeZone>(item.SelectedTimezoneId);
                    item.SelectedTimezone = EnumType.GetEnumDescription(enumTimezone);
                }
            }
            return users;
        }

        public IEnumerable<UserViewModel> SearchByFirstOrLastName(string searchText, int maxResults, IAddressRepository addressRepo)
        {
            string searchTextLower = searchText.ToLower();
            var results = (from u in this.DataToJoin
                           join a in addressRepo.DataToJoin
                            on u.AddressID equals a.AddressID into ua
                           from x in ua.DefaultIfEmpty()
                           where u.FirstName.ToLower().StartsWith(searchTextLower) || u.LastName.ToLower().StartsWith(searchTextLower)
                           select new UserViewModel
                           {
                               AddressID = u.AddressID,
                               AddressLine1 = x.AddressLine1,
                               City = x.City,
                               Deleted_Ind = u.Deleted_Ind,
                               EmailAddress = x.Email,
                               FirstName = u.FirstName,
                               LastName = u.LastName,
                               MiddleName = u.UserMInitial,
                               Organization = x.Organization,
                               Title = x.Title,
                               UserID = u.UserID,
                               UserName = u.UserName,
                               ZipCode = x.ZIP
                           }).Take(maxResults);

            return results;
        }

        public PaginateSortModel<UserViewModel> SearchByFirstOrLastNamePageSort(string searchText, string strSortBy,
            SqlOrderByDirecton sortOrder, int pageSize, int pageNum, IAddressRepository addressRepo)
        {
            string searchTextLower = searchText.ToLower();
            var query = (from u in this.DataToJoin
                         join a in addressRepo.DataToJoin
                          on u.AddressID equals a.AddressID into ua
                         from x in ua.DefaultIfEmpty()
                         where u.FirstName.ToLower().StartsWith(searchTextLower) || u.LastName.ToLower().StartsWith(searchTextLower)
                         select new UserViewModel
                         {
                             AddressID = u.AddressID,
                             AddressLine1 = x.AddressLine1,
                             City = x.City,
                             Deleted_Ind = u.Deleted_Ind,
                             EmailAddress = x.Email,
                             FirstName = u.FirstName,
                             MiddleName = u.UserMInitial,
                             LastName = u.LastName,
                             Organization = x.Organization,
                             Title = x.Title,
                             UserID = u.UserID,
                             UserName = u.UserName,
                             ZipCode = x.ZIP
                         });

            return query.SortAndPaginate(strSortBy, sortOrder, pageSize, pageNum);
        }


        public void SavePasswordHist(int userId, string passwordHashHist)
        {
            var user = this.GetByIDFast(userId);
            user.PasswordHist = passwordHashHist;
        }

        public void UpdateUser(UserViewModel model)
        {
            var user = this.GetByIDFast(model.UserID);
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.UserMInitial = model.MiddleName;
            user.UserName = model.EmailAddress;
            //umesh
            user.TitleId = model.TitleId;
            // is register complete is from websecurity, don't set
            //user.IsRegisterComplete = model.IsRegisterComplete;
            if (!string.IsNullOrEmpty(model.PasswordHist))
                user.PasswordHist = model.PasswordHist;
            user.PreferredTimeZone = model.SelectedTimezoneId;
            this.Update(user);
            //var isouDep = new HUDWorkloadInternalSpecialOptionUserRepository(this.UnitOfWork);
        }

        /// <summary>
        ///  Method to make a user active or inactive
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isUserInactive"></param>
        public void ActivateOrInactivateUser(int userId, bool isUserInactive)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Soft-Delete-User, please pass in correct context in unit of work.");
            context.Database.ExecuteSqlCommand(
                "usp_HCP_Soft_Delete_User @UserId, @Inactive",
                new SqlParameter("@UserId", userId),
                new SqlParameter("@Inactive", isUserInactive)
                );
        }

        /// <summary>
        ///  Method to check if the user is inactivated or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool? IsUserInactive(int userId)
        {
            var user = this.GetByIDFast(userId);
            return user.Deleted_Ind ?? false;
        }

        public string GetNameAndRoleForUser(int userId)
        {
            var context = this.Context as HCP_live;
            var result = (from a in context.HCP_Authentication
                          join ur in context.webpages_UsersInRoles on a.UserID equals ur.UserId
                          join r in context.webpages_Roles on ur.RoleId equals r.RoleId
                          where ur.UserId == userId
                          select (a.FirstName + "," + a.LastName + "," + r.RoleName)
                 ).FirstOrDefault();
            return result;
        }

        public string GetNameByUserName(string username)
        {
            var context = this.Context as HCP_live;
            var result = (from a in context.HCP_Authentication
                          where a.UserName == username
                          select (a.FirstName + " " + a.LastName)
                 ).FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Get last name by first name
        /// </summary>
        /// <param name="pFirstname"></param>
        /// <returns></returns>
        public string GetUserLastNameByFirstName(string pFirstname)
        {
            var addressRepo = new AddressRepository(this.UnitOfWork);
            var user = this.Find(p => p.FirstName == pFirstname).FirstOrDefault();
            if (user == null)
                return null;
            return user.LastName;
        }

        //naveen
        public bool IsEmailExists(string email)
        {
            string emailId = email.ToLower();
            var context = this.Context as HCP_live;

            var result = (from a in context.HCP_Authentication
                          where a.UserName == emailId
                          select (a.FirstName + " " + a.LastName)
                  ).Any();


            return result;
        }

    }
}
