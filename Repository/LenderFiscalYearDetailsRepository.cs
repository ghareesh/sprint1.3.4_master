﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using AutoMapper;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class LenderFiscalYearDetailsRepository : BaseRepository<LenderFiscalYearDetails>, ILenderFiscalYearDetailsRepository
    {
        public LenderFiscalYearDetailsRepository(): base(new UnitOfWork(DBSource.Live)){}

        public LenderFiscalYearDetailsRepository(UnitOfWork unitOfWork): base(unitOfWork){}

        public IQueryable<LenderFiscalYearDetails> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public int? GetFiscalYearEndingMonth(int lenderId, string fha)
        {
            var context = this.Context as HCP_live;
            var result = (from l in context.LenderFiscalYearDetails
                          where l.LenderId == lenderId && l.FHANumber == fha
                          orderby l.Id descending
                          select l.FiscalYearEndMonth
                          ).FirstOrDefault();
            return result;
        }
        // harish added new line of code for batch file upload save ,update and get data
        public LenderFiscalYearDetails getlenderrowdata(int lenderId, string fha)
        {
            var context = this.Context as HCP_live;
            var result = (from l in context.LenderFiscalYearDetails
                          where l.LenderId == lenderId && l.FHANumber == fha
                          orderby l.Id descending
                          select l
                          ).FirstOrDefault();
            //if(result ==null)
            // {
            //    var result1 = (from l in context.LenderFiscalYearDetails
            //               where l.LenderId == lenderId && l.FHANumber == fha
            //               orderby l.Id descending
            //               select l.FiscalYearEndMonth
            //               ).FirstOrDefault();
            //     return result1;
            // }
            return result;
        }
        //save#584

        public int save(LenderFiscalYearDetails dataInModel)
        {


            var context = (HCP_live)this.Context;


            LenderFiscalYearDetails tobj = new LenderFiscalYearDetails();
            tobj.LenderId = dataInModel.LenderId;
            tobj.FHANumber = dataInModel.FHANumber;
            tobj.FiscalYearEndMonth = dataInModel.FiscalYearEndMonth;
            tobj.Q1 = dataInModel.Q1;
            tobj.Q2 = dataInModel.Q2;
            tobj.Q3 = dataInModel.Q3;
            tobj.Q4 = dataInModel.Q4;




            this.InsertNew(tobj);
            Context.SaveChanges();

            return tobj.Id;

        }

        //update #584
        public int Updateyear(LenderFiscalYearDetails dataInModel)
        {


            var context = (HCP_live)this.Context;
            //var context = (HCP_live)this.Context;

            var lenderfisicalyearupdate = (from n in context.LenderFiscalYearDetails
                                           where n.Id == dataInModel.Id
                                           select n).FirstOrDefault();
           
            lenderfisicalyearupdate.FiscalYearEndMonth = dataInModel.FiscalYearEndMonth;
            lenderfisicalyearupdate.Q1 = dataInModel.Q1;
            lenderfisicalyearupdate.Q2 = dataInModel.Q2;
            lenderfisicalyearupdate.Q3 = dataInModel.Q3;
            lenderfisicalyearupdate.Q4 = dataInModel.Q4;

            this.Update(lenderfisicalyearupdate);
           
            Context.SaveChanges();

            return lenderfisicalyearupdate.Id;

        }
    }
}
