﻿using System;
using System.Collections;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using TaskDB = EntityObject.Entities.HCP_task;

namespace Repository
{
    public class TaskConcurrencyRepository : BaseRepository<TaskDB.TaskConcurrency>, ITaskConcurrencyRepository
    {
       public TaskConcurrencyRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }
       public TaskConcurrencyRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public byte[] GetConcurrencyTimeStamp(Guid taskInstanceId)
        {
            var result = Find(p => p.TaskInstanceId == taskInstanceId).FirstOrDefault();
            if (result != null) return result.Concurrency;
            return null;
        }

        public void UpdateTaskInstance(TaskModel taskModel)
        {
            var entityToUpdate = Find(p => p.TaskInstanceId == taskModel.TaskInstanceId).FirstOrDefault();
            if (entityToUpdate != null)
            {
                if (taskModel.Concurrency == null || StructuralComparisons.StructuralEqualityComparer.Equals(entityToUpdate.Concurrency, taskModel.Concurrency))
                {
                    entityToUpdate.ModifiedBy = UserPrincipal.Current.UserId;
                    entityToUpdate.ModifiedOn = DateTime.UtcNow;
                    Update(entityToUpdate);
                }
                else
                {
                    throw new DbUpdateConcurrencyException();
                }
            }
        }

        public byte[] AddTaskInstance(TaskModel taskModel)
        {
            var task = Mapper.Map<TaskDB.TaskConcurrency>(taskModel);
            task.ModifiedOn = DateTime.UtcNow;
            task.ModifiedBy = UserPrincipal.Current.UserId;
            if (!Find(p => p.TaskInstanceId == taskModel.TaskInstanceId).ToList().Any())
            {
                InsertNew(task);
            }
            return task.Concurrency;
        }
    }
}
