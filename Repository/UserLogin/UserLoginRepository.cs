﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces.UserLogin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.UserLogin
{
    public class UserLoginRepository : BaseRepository<HCP_User_LoginTime>, IUserLoginRepository 
    {
        public UserLoginRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public UserLoginRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public Guid SaveUserLogin(UserLoginModel model)
        {
            var context = (HCP_live)this.Context;

            var UL = Mapper.Map<HCP_User_LoginTime>(model);
            this.InsertNew(UL);
            return UL.UserLoginID;
        }

        public void UpdatUserLogin(UserLoginModel ulModel)
        {
            var context = (HCP_live)this.Context;

            var UL = (from n in context.HCP_User_LoginTime
                      where n.Session_Id == ulModel.Session_Id && n.User_id== ulModel.User_id
                           select n).FirstOrDefault();
            if (UL!=null)
            {
  
            UL.Logout_Time = ulModel.Logout_Time.Value;
            UL.ModifiedBy = ulModel.ModifiedBy;
            UL.ModifiedOn = DateTime.UtcNow;
            try
            {
                this.Update(UL);
            }
            catch (Exception ex)
            {

            }
            }
        }


        public List<UserLoginModel> GetLoginHistory(int userid)
        {
            var context = (HCP_live)this.Context;

            var UL = (from n in context.HCP_User_LoginTime
                      where n.User_id == userid
                      orderby n.Login_Time descending
                      select n).Take(10).ToList();
          return Mapper.Map<List<HCP_User_LoginTime>, List<UserLoginModel>>(UL);

        }
    }
       
    }

