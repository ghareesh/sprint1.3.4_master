﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class HUDWorkloadPortfolioManagerRepository : BaseRepository<HUD_WorkLoad_Manager_Portfolio_Manager>, IHUDWorkloadPortfolioManagerRepository
    {

        public HUDWorkloadPortfolioManagerRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public HUDWorkloadPortfolioManagerRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public int GetWorkloadManagerIdFromAeId(int aeId)
        {
            var wlm = this.Find(p => p.HUD_Project_Manager_ID == aeId).FirstOrDefault();
            if (wlm != null) return wlm.HUD_WorkLoad_Manager_ID;
            return 0;
        }

        public string GetWorkloadManagerEmailFromAeId(int aeId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetWorkloadManagerEmailFromAeId, please pass in correct context in unit of work.");

            var results = (from h in context.HUD_WorkLoad_Manager_Portfolio_Manager
                           join w in context.HUD_WorkLoad_Manager on h.HUD_WorkLoad_Manager_ID equals w.HUD_WorkLoad_Manager_ID
                           join a in context.Addresses on w.AddressID equals a.AddressID
                           where h.HUD_Project_Manager_ID == aeId
                           select a.Email).First();

            return results;
        }
    }
}
