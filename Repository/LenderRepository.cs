﻿using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//#189
namespace HUDHealthcarePortal.Lenderprocess
{
    public class LenderRepository : BaseRepository<Lenderdata>
    {
        public LenderRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public LenderRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public void Addlender(Lenderdata obj)
        {
            var context = (HCP_live)this.Context;

            LenderInfo lenobj = new LenderInfo();

            lenobj.LenderID = obj.LenderID;
            lenobj.Lender_Name = obj.Lender_Name.ToUpper();
            lenobj.ModifiedBy = UserPrincipal.Current.UserId;
            lenobj.ModifiedOn = DateTime.UtcNow;
            lenobj.Deleted_Ind = false;

            context.LenderInfoes.Add(lenobj);
            context.SaveChanges();
        }
    }

}
