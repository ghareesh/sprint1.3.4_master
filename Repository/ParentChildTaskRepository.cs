﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AutoMapper;
using TaskDB = EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class ParentChildTaskRepository:BaseRepository<TaskDB.ParentChildTask>,IParentChildTaskRepository
    {
        public ParentChildTaskRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public ParentChildTaskRepository() : base(new UnitOfWork(DBSource.Task))
        {
        }

        public void AddParentChildTask(ParentChildTaskModel model)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var task = Mapper.Map<TaskDB.ParentChildTask>(model);
            this.InsertNew(task);
            context.SaveChanges();
        }

        public bool IsParentTaskAvailable(Guid childTaskInstanceId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var results =
                (from n in context.ParentChildTask select n)
                    .FirstOrDefault(p => p.ChildTaskInstanceId == childTaskInstanceId);
            return results != null;
        }


        public ParentChildTaskModel GetParentTask(Guid childTaskInstanceId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var result =
                (from n in context.ParentChildTask select n)
                    .FirstOrDefault(p => p.ChildTaskInstanceId == childTaskInstanceId);
            var task = Mapper.Map<ParentChildTaskModel>(result);
            return task;
        }


        public ParentChildTaskModel GetRAIParentTask(Guid childTaskInstanceId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var result = (from n in context.ParentChildTask
                          where n.ParentTaskInstanceId == childTaskInstanceId
                          select n).FirstOrDefault();
            //var result =
            //    (from n in context.ParentChildTask select n)
            //        .FirstOrDefault(p => p.ParentTaskInstanceId == childTaskInstanceId);
            var task = Mapper.Map<ParentChildTaskModel>(result);
            return task;
        }
        // harish added new method for rai 
        public ParentChildTaskModel GetRAIChildTaskFromParentchildtask(Guid childTaskInstanceId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var result =
                (from n in context.ParentChildTask select n)
                    .FirstOrDefault(p => p.ParentTaskInstanceId == childTaskInstanceId);
            var task = Mapper.Map<ParentChildTaskModel>(result);
            return task;
        }
    }
}
