﻿using HUDHealthcarePortal.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class RepairCostEstimateTypeRepository : BaseRepository<RepairCostEstimateType>, IRepairCostEstimateTypeRepository
	{
		public RepairCostEstimateTypeRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public RepairCostEstimateTypeRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<RepairCostEstimateType> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public List<RepairCostEstimateTypeModel> GetRepairCostEstimateTypes()
		{
			var repairCostEstimateType = this.GetAll().ToList();			
			return Mapper.Map<List<RepairCostEstimateTypeModel>>(repairCostEstimateType);
		}
		public string GetRepairCostEstimateTypeName(int pId)
		{
			RepairCostEstimateType objRepairCostEstimateType = this.GetByID(pId);
			if (objRepairCostEstimateType != null)
				return objRepairCostEstimateType.RepairCostEstimate;
			return null;
		}


	}
}




