﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core;
using Elmah.Assertions;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_task;
 
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Repository.Interfaces.ProjectAction;
using Model.ProjectAction;
using Repository.Interfaces;
using Repository.Interfaces.ProjectAction;


namespace Repository.ProjectAction
{
    public class GroupTaskRepository:BaseRepository<GroupTask>,IGroupTaskRepository
    {
        public GroupTaskRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }
        public GroupTaskRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public GroupTask AddNewGroupTask(GroupTaskModel groupTaskModel)
        {
            var groupTask = Mapper.Map<GroupTask>(groupTaskModel);
            this.InsertNew(groupTask);
            return groupTask;
        }

        public void UpdateGroupTask(ProjectActionViewModel projectActionViewModel)
        {
            var context = (HCP_task)this.Context;
            var groupTask = (from g in context.GroupTask
                where g.TaskInstanceId == projectActionViewModel.GroupTaskInstanceId
                select g).FirstOrDefault();
            if (groupTask != null)
            {
                groupTask.ServicerSubmissionDate = projectActionViewModel.ServicerSubmissionDate;
                groupTask.RequestStatus = projectActionViewModel.RequestStatus;
                this.Update(groupTask);
            }
        }

        public bool CheckIfProjectActionExistsForFhaNumber(int projectActionTypeId, string fhaNumber)
        {
            var context = (HCP_task)this.Context;

            var result = (from n in context.GroupTask
                select n).FirstOrDefault(
                    p =>
                        p.ProjectActionTypeId == projectActionTypeId && p.FhaNumber == fhaNumber &&
                        (p.RequestStatus == (int) RequestStatus.Submit || p.RequestStatus == (int) RequestStatus.Draft));
            return result != null;
        }

        public PaginateSortModel<GroupTaskModel> GetGroupTasksForLender(int lenderId, ILenderFhaRepository lenderFhaRepository, IProjectActionRepository projectActionRepository, IUserRepository userRepository)
        {
            var context = (HCP_task)this.Context;
            var fhaList = lenderFhaRepository.GetFhaNumbersForLender(lenderId);
           
            var results = (from n in context.GroupTask
                          where fhaList.Contains(n.FhaNumber)
                          select n).OrderByDescending(p=>p.ModifiedOn);

           
            var resultModel = Mapper.Map<IEnumerable<GroupTaskModel>>(results);
            //change the UTC date format to User preffered format
            //resultModel.ToList().ForEach(r =>
            //{
            //    r.CreatedOn = TimezoneManager.GetPreferredTimeFromUtc(r.CreatedOn);
            //    r.ModifiedOn = TimezoneManager.GetPreferredTimeFromUtc(r.ModifiedOn);
            //    r.ProjectActionStartDate = TimezoneManager.GetPreferredTimeFromUtc(r.ProjectActionStartDate.Value);

            //}); 

            var resultList = new PaginateSortModel<GroupTaskModel>();

            if (resultModel != null)
            {
                var resultStr = "";
                string[] resultStrArr;
                foreach (var groupTaskModel in resultModel)
                {
                    groupTaskModel.ProjectActionName =
                        projectActionRepository.GetProjectActionName(groupTaskModel.ProjectActionTypeId);
                    if (groupTaskModel.InUse > 0)
                    {
                        resultStr = userRepository.GetNameAndRoleForUser(groupTaskModel.InUse);
                        resultStrArr = resultStr.Split(',');
                        if (resultStrArr != null && resultStrArr.Length > 0)
                        {
                            groupTaskModel.UserNameInUse = resultStrArr[0] + " " + resultStrArr[1];
                            groupTaskModel.UserRole = resultStrArr[2];
                        }
                    }
                    else
                    {
                        groupTaskModel.UserNameInUse = "";
                        groupTaskModel.UserRole = "";
                    }

                    if (groupTaskModel.RequestStatus == 1 || groupTaskModel.RequestStatus == 2 || groupTaskModel.RequestStatus == 5 || groupTaskModel.RequestStatus == 7)
                    {
                        resultStr = userRepository.GetNameAndRoleForUser(groupTaskModel.ModifiedBy);
                        resultStrArr = resultStr.Split(',');
                        if (resultStrArr != null && resultStrArr.Length > 0)
                        {
                            groupTaskModel.UserNameInUse = resultStrArr[0] + " " + resultStrArr[1];
                            groupTaskModel.UserRole = resultStrArr[2];
                        }
                    }
                    


                    groupTaskModel.Status =
                        EnumType.GetEnumDescription(
                            EnumType.Parse<RequestStatus>(groupTaskModel.RequestStatus.ToString()));
                }
                resultList.Entities = resultModel.ToList();
                resultList.PageSize = 10;
                resultList.TotalRows = resultModel.Count();
            }

            return resultList;
        }

        public GroupTaskModel GetGroupTaskById(int taskId)
        {
            var results = this.GetByIDFast(taskId);
            return Mapper.Map<GroupTaskModel>(results);
        }

        public void UnlockGroupTask(int taskId)
        {
            var results = this.GetByIDFast(taskId);
            results.InUse = 0;
            results.ModifiedBy = UserPrincipal.Current.UserId;
            results.ModifiedOn = DateTime.UtcNow;
            this.Update(results);
        }

        public void UpdateGroupTaskForCheckout(GroupTaskModel groupTaskModel)
        {
            var results = this.GetByIDFast(groupTaskModel.TaskId);
            results.InUse = groupTaskModel.InUse;
            results.ModifiedBy = UserPrincipal.Current.UserId;
            results.ModifiedOn = DateTime.UtcNow;
            this.Update(results);
        }


        public void UpdateOPAGroupTask(OPAViewModel OPAActionViewModel)
        {
            var context = (HCP_task)this.Context;
            
                var groupTask = (from g in context.GroupTask
                                 where g.TaskInstanceId == OPAActionViewModel.GroupTaskInstanceId
                                 select g).FirstOrDefault();
                if (groupTask != null)
                {
                    groupTask.ServicerSubmissionDate = OPAActionViewModel.ServicerSubmissionDate;
                    groupTask.RequestStatus = OPAActionViewModel.RequestStatus;
                    groupTask.ServicerComments = OPAActionViewModel.ServicerComments;
                    groupTask.ModifiedBy = UserPrincipal.Current.UserId;
                    groupTask.IsAddressChanged = OPAActionViewModel.IsAddressChange;
                    this.Update(groupTask);
                }
            
        }


        public GroupTaskModel GroupTaskbyFHAProjectAction(int projectActionTypeId, string fhaNumber)
        {
            var context = (HCP_task)this.Context;

            var result = (from n in context.GroupTask
                          select n).FirstOrDefault(
                    p =>
                        p.ProjectActionTypeId == projectActionTypeId && p.FhaNumber == fhaNumber &&
                        (p.RequestStatus == (int)RequestStatus.Submit || p.RequestStatus == (int)RequestStatus.Draft || p.RequestStatus == (int)RequestStatus.RequestAdditionalInfo));
          
            return Mapper.Map<GroupTaskModel>(result);
         


        }


        public void LockGroupTask(int taskId)
        {
            var results = this.GetByIDFast(taskId);
            results.InUse = UserPrincipal.Current.UserId;
            results.ModifiedBy = UserPrincipal.Current.UserId;
            results.ModifiedOn = DateTime.UtcNow;
            this.Update(results);
        }

        public bool CheckIfFileExistsByGroupTaskId(int GroupTaskId)
        {
            var context = (HCP_task)this.Context;

            var result = (from g in context.GroupTask
                join t in context.TaskFiles
                    on g.TaskInstanceId equals t.TaskInstanceId
                select g).FirstOrDefault(p => p.TaskId == GroupTaskId);

            return result != null;
        }
        // harish added 19032020
        public GroupTaskModel GetGrouptaskbyTaskinstanceid(Guid taskIInstanceId)
        {
            var context = (HCP_task)this.Context;
            var group = (from n in context.GroupTask where n.TaskInstanceId == taskIInstanceId select n).FirstOrDefault();
            return Mapper.Map<GroupTaskModel>(group);
        }
    }
}
