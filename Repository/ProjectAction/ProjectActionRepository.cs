﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Repository.Interfaces.ProjectAction;

namespace HUDHealthcarePortal.Repository.ProjectAction
{
    public class ProjectActionRepository : BaseRepository<HCP_Project_Action>, IProjectActionRepository
    {
        public ProjectActionRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public ProjectActionRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public IList<ProjectActionTypeViewModel> GetAllProjectActions()
        {
            var context = (HCP_live)this.Context;

            var result = (from n in context.HCP_Project_Action select n).Where(p=>p.Deleted_Ind == false).ToList();
            return Mapper.Map<List<HCP_Project_Action>, List<ProjectActionTypeViewModel>>(result); 
        }

        public IList<ProjectActionTypeViewModel> GetAllProjectActionsByPageId(int pageTypeId)
        {
            var context = (HCP_live)this.Context;

            var result = (from n in context.HCP_Project_Action select n).Where(p => p.Deleted_Ind == false && p.PageTypeId == pageTypeId).ToList();
            return Mapper.Map<List<HCP_Project_Action>, List<ProjectActionTypeViewModel>>(result);
        }

        public string GetProjectActionName(int projectActionTypeId)
        {
            var context = (HCP_live)this.Context;

            var results = (from n in context.HCP_Project_Action
                where n.ProjectActionID == projectActionTypeId
                select n.ProjectActionName).FirstOrDefault();
            return results;
        }

        // harish

        public IList<ProjectActionTypeViewModel> GetAllProjectActionstype(int projectActionTypeId)
        {
            var context = (HCP_live)this.Context;

            var result = (from n in context.HCP_Project_Action
                          where n.ProjectActionID == projectActionTypeId
                          select n).Where(p => p.Deleted_Ind == false).ToList();
            return Mapper.Map<List<HCP_Project_Action>, List<ProjectActionTypeViewModel>>(result);
        }
    }
}
