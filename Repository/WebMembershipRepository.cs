﻿using HUDHealthcarePortal.Core;
using System;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class WebMembershipRepository : BaseRepository<webpages_Membership>, IWebMembershipRepository
    {
        public WebMembershipRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public WebMembershipRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public int? GetUserIdByToken(string userToken)
        {
            var webmember = this.Find(p => p.ConfirmationToken == userToken).FirstOrDefault();
            if (webmember == null)
                return null;
            else
                return webmember.UserId;
        }

        public int? GetUserIdByResetPwdToken(string resetPwdToken)
        {
            var webmember = this.Find(p => p.PasswordVerificationToken == resetPwdToken).FirstOrDefault();
            if (webmember == null)
                return null;
            else
                return webmember.UserId;
        }

        public void UnlockUserAccount(int userId)
        {
            try
            {
                var membershipAccount = this.GetByIDFresh(userId);
                membershipAccount.PasswordFailuresSinceLastSuccess = 0;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format("Unlock user id: {0} failed.", userId), ex.InnerException);
            }
        }

        public void InitForceChangePassword(int userId)
        {
            try
            {
                var membershipAccount = this.GetByIDFresh(userId);
                membershipAccount.PasswordChangedDate = new DateTime(2000, 1, 1);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format("InitForceChangePassword user id: {0} failed.", userId), ex.InnerException);
            }
        }
    }
}