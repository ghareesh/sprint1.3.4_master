﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Runtime.ConstrainedExecution;
using System.Security.Policy;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using System.Linq;
using HUDHealthcarePortal.Model;
using Model;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class NonCriticalProjectInfoRepository : BaseRepository<NonCritical_ProjectInfo>, INonCriticalProjectInfoRepository
    {
        public NonCriticalProjectInfoRepository() : base(new UnitOfWork(DBSource.Live))
        {
        }

        public NonCriticalProjectInfoRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<NonCritical_ProjectInfo> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public NonCriticalPropertyModel GetNonCriticalPropertyInfo(string fhaNumber)
        {
            var context = (HCP_live)this.Context;
            var results = new NonCriticalPropertyModel();
            //var results = (from p in GetAll()
            //               join lf in context.Lender_FHANumber on p.FHANumber equals lf.FHANumber
            //               join c in context.NonCritical_ProjectInfo on new { p.PropertyID, p.LenderID, p.FHANumber } equals new { c.PropertyID, c.LenderID, c.FHANumber }
            //               join a in context.ProjectInfoes on new { p.PropertyID, p.LenderID, p.FHANumber } equals new { a.PropertyID, a.LenderID, a.FHANumber } into pa
            //               from x in pa.DefaultIfEmpty()
            //               join b in context.NonCriticalRequestExtension on new { p.PropertyID, p.LenderID, p.FHANumber } equals new { b.PropertyID, b.LenderID, b.FHANumber } into pb
            //               from y in pb.DefaultIfEmpty()
            //               where p.LenderID == lf.LenderID
            //               && lf.FHA_EndDate == null && p.FHANumber == fhaNumber
            //               orderby p.ModifiedOn descending
            //               select new NonCriticalPropertyModel()
            //               {
            //                   PropertyId = p.PropertyID,
            //                   PropertyName = x != null ? x.ProjectName : string.Empty,
            //                   ClosingDate = c.ClosingDate.HasValue ? c.ClosingDate.Value.ToShortDateString() : string.Empty,
            //                   EndingDate = c.EndingDate.HasValue ? c.EndingDate.Value.ToShortDateString() : string.Empty,
            //                   ExtensionApprovalDate = c.ExtensionApprovalDate.HasValue ? c.ExtensionApprovalDate.Value.ToShortDateString() : string.Empty,
            //                   IsAnyPendingExtensionRequestExist = pb != null && pb.Any(a => a.ExtensionRequestStatus == 1) ? true : false,
            //                   NonCriticalAccountBalance = c.InitialNCREBalance,
            //                   NCRECurrentBalance = c.CurrentBalance,
            //                   TroubledCode = x != null ? x.Troubled_Code : string.Empty,
            //                   IsNCREValidForFHANumber = c.IsAmountConfirmed != null && (bool)c.IsAmountConfirmed
            //               }).Distinct().FirstOrDefault();
            results = context.Database.SqlQuerySimple<NonCriticalPropertyModel>("usp_HCP_GetNonCriticalPropertyInfo",
                    new
                    {
                        fhaNumber = fhaNumber
                    }).Distinct().FirstOrDefault();
            int x = 2;
            return results;
        }

        public NonCritical_ProjectInfo GetNonCriticalProjectInfoByNCR(NonCriticalRepairsRequest nonCriticalRepairsRequest)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetNonCriticalProjectInfoByNCR, please pass in correct context in unit of work.");

            var nonProjectInfo = (from n in context.NonCritical_ProjectInfo
                                  where
                                      n.PropertyID == nonCriticalRepairsRequest.PropertyID &&
                                      n.LenderID == nonCriticalRepairsRequest.LenderID &&
                                      n.FHANumber == nonCriticalRepairsRequest.FHANumber
                                  select n).FirstOrDefault();

            return nonProjectInfo;
        }

        // Add methods
        public List<NonCriticalLateAlertsModel> GetNonCriticalLateAlertsToSend(string date)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetDataUploadSummaryByUserId, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<NonCriticalLateAlertsModel>("usp_HCP_Get_NonCriticalLateAlertInfo",
                new
                {
                    Date = date
                });

            return results.ToList();
        }

        public NonCritical_ProjectInfo GetNonCriticalProjectInfo(int propertyId, int lenderId, string fhaNumber)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetNonCriticalProjectInfo, please pass in correct context in unit of work.");

            //Naveen 28-10-2019 (added if cond)
            if (!(UserPrincipal.Current.UserRole == "Servicer"))
            {
                var nonProjectInfo = (from n in context.NonCritical_ProjectInfo
                                      where n.PropertyID == propertyId &&
                                          n.LenderID == lenderId &&
                                          n.FHANumber == fhaNumber
                                      select n).FirstOrDefault();

                return nonProjectInfo;
            }
            else
            {
                var nonProjectInfo = (from n in context.NonCritical_ProjectInfo
                                      join p in context.User_Lender.Where(x => x.User_ID == UserPrincipal.Current.UserId) on n.FHANumber equals p.FHANumber
                                      where n.PropertyID == propertyId &&
                                          n.LenderID == p.Lender_ID &&
                                          n.FHANumber == fhaNumber
                                      select n).FirstOrDefault();

                return nonProjectInfo;
            }



        }

        public void UpdateNonCriticalProjectInfo(NonCritical_ProjectInfo nonCriticalProjectInfo)
        {
            if (nonCriticalProjectInfo != null)
            {
                nonCriticalProjectInfo.ModifiedOn = DateTime.UtcNow;
                nonCriticalProjectInfo.ModifiedBy = UserPrincipal.Current.UserId;
            }
        }

    }
}