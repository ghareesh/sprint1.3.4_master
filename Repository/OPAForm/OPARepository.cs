﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces.OPAForm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_live.Programmability;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Repository.Interfaces;
using Repository.Interfaces;
using Model.Production;


namespace Repository.OPAForm
{
    public class OPARepository : BaseRepository<OpaForm>, IOPARepository
    {

        private IParentChildTaskRepository parentChildTaskRepository;
        private INewFileRequestRepository newFileRequestRepository;
        private IUserRepository userRepository;

        public OPARepository()
            : base(new UnitOfWork(DBSource.Live))
        {

        }

        public OPARepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }


        public void UpdateTaskId(OPAViewModel model)
        {
            var context = (HCP_live)this.Context;

            var opaForm = (from n in context.OpaForm
                           where n.ProjectActionFormId == model.ProjectActionFormId
                           select n).FirstOrDefault();

            opaForm.MyTaskId = model.MytaskId;
            this.Update(opaForm);
        }

        public string GetOPAName(int opaId)
        {
            var context = (HCP_live)this.Context;
            return (from p in context.HCP_Project_Action
                    where p.ProjectActionID == opaId
                    select p).FirstOrDefault().ProjectActionName;
        }

        public Guid SaveOPAForm(OPAViewModel model)
        {
            var context = (HCP_live)this.Context;

            var OPA = Mapper.Map<OpaForm>(model);
            this.InsertNew(OPA);
            return OPA.ProjectActionFormId;
        }

        public void UpdatOPARequestForm(OPAViewModel opaViewModel)
        {
            var context = (HCP_live)this.Context;

            var opaForm = (from n in context.OpaForm
                           where n.ProjectActionFormId == opaViewModel.ProjectActionFormId
                           select n).FirstOrDefault();

            opaForm.RequestStatus = opaViewModel.RequestStatus;
            opaForm.AEComments = opaViewModel.AEComments;
            opaForm.ModifiedOn = DateTime.UtcNow;
            opaForm.ModifiedBy = UserPrincipal.Current.UserId;
            opaForm.TaskInstanceId = opaViewModel.TaskInstanceId;
            opaForm.IsAddressChange = opaViewModel.IsAddressChange;
            opaForm.ServicerSubmissionDate = opaViewModel.ServicerSubmissionDate;
            if (opaViewModel.FhaNumber.Length > 9 && opaViewModel.FhaNumber.Contains(','))
            {
                opaForm.FhaNumber = opaViewModel.FhaNumber.Substring(0, 9);
            }
            else
            {
                opaForm.FhaNumber = opaViewModel.FhaNumber;
            }

            this.Update(opaForm);


        }

        public List<OPAHistoryViewModel> GetOpaHistory(Guid taskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<OPAHistoryViewModel>();
                parentChildTaskRepository = new ParentChildTaskRepository();
                newFileRequestRepository = new NewFileRequestRepository();
                if (parentChildTaskRepository.IsParentTaskAvailable(taskInstanceId))
                {
                    var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>("usp_HCP_GetOpaHistoryForRequestAdditionalInfo",
                    new
                    {
                        TaskInstanceId = taskInstanceId
                    }).OrderByDescending(p => p.actionTaken).ToList();
                    opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                    if (newFileRequestRepository.IsNewFileRequest(taskInstanceId))
                    {
                        var newFileRequestModel = newFileRequestRepository.GetNewFileRequest(taskInstanceId);
                        if (newFileRequestModel != null)
                        {
                            userRepository = new UserRepository();
                            var userNameAndRole = userRepository.GetNameAndRoleForUser(newFileRequestModel.RequestedBy);
                            var userArray = userNameAndRole.Split(',');
                            var opaHistoryViewModel = new OPAHistoryViewModel()
                            {
                                actionTaken = "New File Request",
                                name = userArray[0] + " " + userArray[1],
                                userRole = "AE",
                                comment = newFileRequestModel.Comments,
                                submitDate = newFileRequestModel.RequestedOn

                            };
                            opaHistory.Add(opaHistoryViewModel);


                        }
                    }
                }
                else
                {
                    //var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_GetOpaHistory",
                    //new
                    //{
                    //    TaskInstanceId = taskInstanceId
                    //}).ToList();

                    //var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_GetOpaHistory_test",
                    //new
                    //{
                    //    TaskInstanceId = taskInstanceId,
                    //    rolename = UserPrincipal.Current.UserRole
                    //}).ToList();
                    //var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_GetOpaHistory_testsiddu"usp_HCP_GetOpaHistory_testHari,
                    var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_GetOpaHistory_AssetMgmt",
                 new
                   {
                       TaskInstanceId = taskInstanceId,
                       rolename = UserPrincipal.Current.UserRole
                   }).ToList();
                    opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistory_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                    opaHistory = opaHistory.Any() ? opaHistory.Where(x => x.FolderKey != 40 && x.FolderKey != 41).ToList() : opaHistory;

                    //opaHistory = 
                }


                return opaHistory.OrderByDescending(p => p.actionDate).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public List<TaskModel> GetChildTasks(Guid taskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaChildTasks = new List<TaskModel>();
                var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaChildTasksByParentTask_Result>("usp_HCP_GetOpaChildTasksByParentTask",
                new
                {
                    TaskInstanceId = taskInstanceId
                }).ToList();

                opaChildTasks = Mapper.Map<IEnumerable<usp_HCP_GetOpaChildTasksByParentTask_Result>, IEnumerable<TaskModel>>(results).ToList();
                return opaChildTasks;
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        public List<OPAWorkProductModel> getAllOPAWorkProducts(Guid taskInstanceId, string fileType)
        {

            try
            {
                var context = this.Context as HCP_task;
                var liveContext = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in getAllOPAWorkProducts, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaWorkProduct = new List<OPAWorkProductModel>();
                //var results = from a in context.TaskFiles 
                //              where a.TaskInstanceId.Equals(taskInstanceId) select a;

                var results = context.Database.SqlQuerySimple<USP_GetOPAFiles_Result>(
                   "USP_GetOPAFilesByTaskId",
                   new
                   {
                       TaskInstanceId = taskInstanceId,
                       FileType = fileType
                   }).ToList();

                opaWorkProduct = Mapper.Map<IEnumerable<USP_GetOPAFiles_Result>, IEnumerable<OPAWorkProductModel>>(results).ToList();
                return opaWorkProduct;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;

        }

        public List<OPAHistoryViewModel> GetOpaFileCommentHistory(int taskFileId)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<OPAHistoryViewModel>();
                var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_GetTaskFileCommentsHistory",
                new
                {
                    TaskFileId = taskFileId
                }).ToList();

                opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistory_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                return opaHistory;
            }
            catch (Exception ex)
            {
            }
            return null;

        }

        public List<OPAHistoryViewModel> GetOpaFileUploadHistory(int taskFileId)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<OPAHistoryViewModel>();
                var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_GetTaskFileUploadHistory",
                new
                {
                    TaskFileId = taskFileId
                }).ToList();

                opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistory_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                return opaHistory;
            }
            catch (Exception ex)
            {
            }
            return null;

        }


        //public void uploadOpaWorkProduct(OPAWorkProductModel oPAWorkProductModel)
        //{

        //    try
        //    {
        //        var context = this.Context as HCP_task;

        //        if (context == null)
        //            throw new InvalidCastException("context is not from db Live in getAllOPAWorkProducts, please pass in correct context.");
        //        context.Database.CommandTimeout = 600;

        //        var OPA = Mapper.Map<OpaForm>(oPAWorkProductModel);
        //        this.InsertNew(OPA);

        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //}

        //public void UpdateAdditionalInfo(OPAViewModel opaViewModel)
        //{
        //    var context = (HCP_live)this.Context;

        //    try
        //    {

        //        var AddInfo = (from p in context.AdditionalInformation
        //                       where p.AdditionalInformationId == opaViewModel.AdditionalInformation.AdditionalInformationId
        //                       select p).FirstOrDefault();

        //        AddInfo.LenderComment = opaViewModel.AdditionalInformation.LenderComment;
        //        AddInfo.ModifiedBy = UserPrincipal.Current.UserId;
        //        AddInfo.ModifiedDate = DateTime.UtcNow;
        //        context.AdditionalInformation.Attach(AddInfo);
        //        context.Entry(AddInfo).State = System.Data.Entity.EntityState.Modified;
        //        context.SaveChanges();


        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        public OPAViewModel GetProjectActionRequestId(Guid formId)
        {
            var context = (HCP_live)this.Context;
            var projectActionForm = (from n in context.OpaForm
                                     where n.ProjectActionFormId == formId
                                     select n).FirstOrDefault();

            return Mapper.Map<OPAViewModel>(projectActionForm);

        }



        public OPAViewModel OPATaskbyFHAProjectAction(int projectActionTypeId, string fhaNumber)
        {
            var context = (HCP_live)this.Context;

            var projectActionForm = (from n in context.OpaForm
                                     select n).FirstOrDefault(
                    p =>
                        p.ProjectActionTypeId == projectActionTypeId && p.FhaNumber == fhaNumber &&
                        (p.RequestStatus == (int)RequestStatus.Submit || p.RequestStatus == (int)RequestStatus.Draft || p.RequestStatus == (int)RequestStatus.RequestAdditionalInfo));

            return Mapper.Map<OPAViewModel>(projectActionForm);
        }

        public OPAViewModel GetOPAByTaskId(int taskId)
        {
            var context = (HCP_live)this.Context;
            var opaForm = (from n in context.OpaForm select n).FirstOrDefault(p => p.MyTaskId == taskId);
            return Mapper.Map<OPAViewModel>(opaForm);
        }

        public List<OPAHistoryViewModel> GetOpaHistoryForRequestAdditionalInfo(Guid childTaskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetOpaHistoryForRequestAdditionalInfo, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<OPAHistoryViewModel>();
                var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>("usp_HCP_GetOpaHistoryForRequestAdditionalInfo",
                new
                {
                    TaskInstanceId = childTaskInstanceId
                }).ToList();

                opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                return opaHistory;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public OPAViewModel GetOPAByTaskInstanceId(Guid taskIInstanceId)
        {
            var context = (HCP_live)this.Context;
            var opaForm = (from n in context.OpaForm where n.TaskInstanceId == taskIInstanceId select n).FirstOrDefault();
            return Mapper.Map<OPAViewModel>(opaForm);
        }

        public OPAViewModel GetOPAByChildTaskId(Guid childTaskInstanceId)
        {
            var context = (HCP_live)this.Context;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetOPAByChildTaskId, please pass in correct context in unit of work.");
            context.Database.CommandTimeout = 600;
            var results = context.Database.SqlQuerySimple<usp_HCP_GetOPAByChildTaskId_Result>("usp_HCP_GetOPAByChildTaskId",
            new
            {
                TaskInstanceId = childTaskInstanceId
            }).FirstOrDefault();

            return Mapper.Map<OPAViewModel>(results);
        }
        public List<Guid> GeetChildTaskforReassignment(List<Guid> Joinlist, string assignedto)
        {
            try
            {
                var context = (HCP_live)this.Context;

                var taskIdList = new List<Guid>();
                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaChildTasks = new List<ParentChildTaskModel>();
                var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaChildTasksByParentTask_Result>("usp_HCP_GetOpenChildTasksByParentTask",
                new
                {
                    AEEmail = assignedto
                }).ToList();

                var finalresult = results.Where(x => Joinlist.Contains(x.ParentTaskInstanceId));
                foreach (var childtask in finalresult)
                {
                    taskIdList.Add(childtask.TaskInstanceId);
                }

                //var result = taskIdList.Where(i => Joinlist.Contains(i)).ToList();

                //opaChildTasks = Mapper.Map<IEnumerable<usp_HCP_GetOpaChildTasksByParentTask_Result>, IEnumerable<ParentChildTaskModel>>(results).ToList();
                return taskIdList;
            }
            catch (Exception ex)
            {
            }
            return null;

        }

        #region Production Application

        //public List<OPAHistoryViewModel> GetProdOpaHistoryFileList(Guid taskInstanceId, int userId, int viewId, int folderKey, int notAssigned)

        public List<OPAHistoryViewModel> GetProdOpaHistoryFileList(Guid taskInstanceId, int userId, int viewId, int folderKey)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<OPAHistoryViewModel>();
                parentChildTaskRepository = new ParentChildTaskRepository();
                newFileRequestRepository = new NewFileRequestRepository();
                if (parentChildTaskRepository.IsParentTaskAvailable(taskInstanceId))
                {
                    var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>("usp_HCP_GetOpaHistoryForRequestAdditionalInfo",
                    new
                    {
                        TaskInstanceId = taskInstanceId
                    }).OrderByDescending(p => p.actionTaken).ToList();
                    opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                    //if (newFileRequestRepository.IsNewFileRequest(taskInstanceId))
                    //{
                    //    var newFileRequestModel = newFileRequestRepository.GetNewFileRequest(taskInstanceId);
                    //    if (newFileRequestModel != null)
                    //    {
                    //        userRepository = new UserRepository();
                    //        var userNameAndRole = userRepository.GetNameAndRoleForUser(newFileRequestModel.RequestedBy);
                    //        var userArray = userNameAndRole.Split(',');
                    //        var opaHistoryViewModel = new OPAHistoryViewModel()
                    //        {
                    //            actionTaken = "New File Request",
                    //            name = userArray[0] + " " + userArray[1],
                    //            userRole = "AE",
                    //            comment = newFileRequestModel.Comments,
                    //            submitDate = newFileRequestModel.RequestedOn

                    //        };
                    //        opaHistory.Add(opaHistoryViewModel);


                    //    }
                    //}
                }
                else
                {
                    var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_Prod_GetOpaHistory_FileList",
                    new
                    {
                        TaskInstanceId = taskInstanceId,
                        UserId = userId,
                        ViewId = viewId,
                        FolderKey = folderKey//,
                                             //NotAssigned = notAssigned
                    }).ToList();
                    opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistory_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                }


                return opaHistory.OrderByDescending(p => p.actionDate).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<OPAHistoryViewModel> GetProdOpaHistoryReviewersList(int userId, int viewId, int fileId)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<OPAHistoryViewModel>();
                parentChildTaskRepository = new ParentChildTaskRepository();
                newFileRequestRepository = new NewFileRequestRepository();

                var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_Prod_GetOpaHistory_ReviewersList",
                    new
                    {
                        FileId = fileId,
                        ViewId = viewId,
                        UserId = userId

                    }).ToList();
                opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistory_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();



                return opaHistory.ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public List<Prod_OPAHistoryViewModel> GetProdOpaHistory(Guid taskInstanceId, string PropertyId, string FhaNumber, int userId, int viewId, bool isEdit)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<Prod_OPAHistoryViewModel>();
                parentChildTaskRepository = new ParentChildTaskRepository();
                newFileRequestRepository = new NewFileRequestRepository();
                if (parentChildTaskRepository.IsParentTaskAvailable(taskInstanceId))
                {
                    var storedProc = string.Empty;
                    if (isEdit)
                    {
                        storedProc = "usp_HCP_Prod_GetRAIEdit";
                    }
                    else
                    {
                        storedProc = "usp_HCP_Prod_GetRAIReadOnly";
                    }
                    var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetOpaHistoryForRequestAdditionalInfo_Result>(storedProc,
                    new
                    {
                        TaskInstanceId = taskInstanceId,
                        PropertyId = PropertyId,
                        FHANumber = FhaNumber
                    }).ToList();
                    opaHistory = Mapper.Map<IEnumerable<usp_HCP_Prod_GetOpaHistoryForRequestAdditionalInfo_Result>, IEnumerable<Prod_OPAHistoryViewModel>>(results).ToList();
                    if (storedProc == "usp_HCP_Prod_GetRAIReadOnly")
                    {
                        opaHistory = opaHistory.OrderBy(x => x.id).ToList();
                    }
                    else
                    {
                        opaHistory = opaHistory.OrderBy(x => x.FolderSortingNumber).ToList();
                    }
                    //opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>, IEnumerable<Prod_OPAHistoryViewModel>>(results).ToList();
                    if (newFileRequestRepository.IsNewFileRequest(taskInstanceId))
                    {
                        //var newFileRequestModel = newFileRequestRepository.GetNewFileRequest(taskInstanceId);
                        //if (newFileRequestModel != null)
                        //{
                        //    userRepository = new UserRepository();
                        //    var userNameAndRole = userRepository.GetNameAndRoleForUser(newFileRequestModel.RequestedBy);
                        //    var userArray = userNameAndRole.Split(',');
                        //    var opaHistoryViewModel = new Prod_OPAHistoryViewModel()
                        //    {
                        //        actionTaken = "New File Request",
                        //        name = userArray[0] + " " + userArray[1],
                        //        userRole = "AE",
                        //        comment = newFileRequestModel.Comments,
                        //        submitDate = newFileRequestModel.RequestedOn

                        //    };
                        //    opaHistory.Add(opaHistoryViewModel);


                        //}
                    }
                }
                else
                {
                    var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetOpaHistory_Result>("usp_HCP_Prod_GetOpaHistory",
                    new
                    {
                        TaskInstanceId = taskInstanceId,
                        UserId = userId
                    }).ToList();
                    opaHistory = Mapper.Map<IEnumerable<usp_HCP_Prod_GetOpaHistory_Result>, IEnumerable<Prod_OPAHistoryViewModel>>(results).ToList();
                }


                return opaHistory.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public List<Prod_OPAHistoryViewModel> GetProdLenderUpload(Guid taskInstanceId, string PropertyId, string FhaNumber)
        {
            try
            {
                int userId = UserPrincipal.Current.UserId;
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<Prod_OPAHistoryViewModel>();
                parentChildTaskRepository = new ParentChildTaskRepository();
                newFileRequestRepository = new NewFileRequestRepository();

                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetOpaHistory_Result>("usp_HCP_Prod_GetLenderUpload",
                new
                {
                    TaskInstanceId = taskInstanceId,
                    UserId = userId,
                    PropertyId = PropertyId,
                    FHANumber = FhaNumber

                }).ToList();
                opaHistory = Mapper.Map<IEnumerable<usp_HCP_Prod_GetOpaHistory_Result>, IEnumerable<Prod_OPAHistoryViewModel>>(results).ToList();


                return opaHistory.OrderByDescending(p => p.actionDate).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        public List<Prod_OPAHistoryViewModel> GetProdLenderUploadForAmendments(Guid taskInstanceId, string PropertyId, string FhaNumber)
        {
            try
            {
                int userId = UserPrincipal.Current.UserId;
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<Prod_OPAHistoryViewModel>();
                parentChildTaskRepository = new ParentChildTaskRepository();
                newFileRequestRepository = new NewFileRequestRepository();

                // var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetOpaHistory_Result>("usp_HCP_Prod_GetProdLenderUploadForAmendments"usp_HCP_Prod_GetProdLenderUploadForAmendments_Test19022020,
                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetOpaHistory_Result>("usp_HCP_Prod_GetProdLenderUploadForAmendments_AssetMgmt",
              new
                {
                    TaskInstanceId = taskInstanceId,
                    UserId = userId,
                    PropertyId = PropertyId,
                    FHANumber = FhaNumber

                }).ToList();
                //results = results.Where(x => x.CreatedBy == 6583).ToList();
                opaHistory = Mapper.Map<IEnumerable<usp_HCP_Prod_GetOpaHistory_Result>, IEnumerable<Prod_OPAHistoryViewModel>>(results).ToList();


                return opaHistory.OrderByDescending(p => p.actionDate).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public List<Prod_SubFolderStructureModel> GetSubFolder(int parentKey, string PropertyId, string FhaNumber)
        {
            try
            {

                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from  Live db, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var subFolder = new List<Prod_SubFolderStructureModel>();

                var results = context.Database.SqlQuerySimple<Prod_SubFolderStructureModel>("usp_HCP_Prod_GetSubFolder",
                new
                {
                    ParentKey = parentKey,
                    ProjectId = PropertyId,
                    FhaNumber = FhaNumber,

                }).ToList();
                //subFolder = Mapper.DynamicMap<IEnumerable<usp_HCP_Prod_GetSubFolder>, IEnumerable<Prod_SubFolderStructureModel>>(results).ToList();


                return results.OrderByDescending(p => p.SubfolderSequence).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public string GetTaskId(int pageTypeId, string fHANumber, Guid taskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from  Live db, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var results = context.Database.SqlQuerySimple<Int32>("usp_HCP_Prod_taskidForExecutedClosing",
                new
                {
                    PagetypeId = pageTypeId,
                    FHANumber = fHANumber,
                    TaskInstanceId = taskInstanceId
                }).ToList();
                List<Int32> taskid = new List<Int32>();
                taskid.AddRange(results);

                return taskid[0].ToString();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public Prod_Assainedto AssignedTo(int Pageid, string FhaNumber)
        {
            try
            {

                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from  Live db, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;


                var results = context.Database.SqlQuerySimple<Prod_Assainedto>("usp_HCP_Prod_AssignedTo",
                new
                {
                    PagetypeId = Pageid,
                    FHANumber = FhaNumber
                }).ToList();
                //subFolder = Mapper.DynamicMap<IEnumerable<usp_HCP_Prod_GetSubFolder>, IEnumerable<Prod_SubFolderStructureModel>>(results).ToList();

                return results[0];
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public object FormNameBYPageTypeID(int pageTypeId)
        {
            try
            {

                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from  Live db, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var data = (from v in context.HCP_PageType where v.PageTypeId == pageTypeId select v.PageTypeDescription).FirstOrDefault();

                //var results = context.Database.SqlQuerySimple<string>("usp_HCP_Prod_GetPageNameBYPageTypeID",
                //new
                //{
                //    PageTypeId = pageTypeId

                //}).ToList();
                return data;

            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public List<Prod_SubFolderStructureModel> GetFolderInfoByKey(int folderkey)
        {
            try
            {

                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from  Live db, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;

                var results = context.Database.SqlQuerySimple<Prod_SubFolderStructureModel>("usp_HCP_Prod_GetFolderInfoByKey",
                new
                {
                    FolderKey = folderkey


                }).ToList();
                return results.ToList();

            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public List<int> GetFileIDByFolderKey(int folderkey)
        {
            try
            {

                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from  Live db, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;

                var results = context.Database.SqlQuerySimple<int>("usp_HCP_Prod_GetFileIDByFolderKey",
                new
                {
                    FolderKey = folderkey


                }).ToList();
                return results.ToList();

            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public int SaveSubFolder(Prod_SubFolderStructureModel model)
        {
            try
            {

                var context = this.Context as HCP_live;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in InsertAEChecklistStaus, please pass in correct context in unit of work.");

                return context.Database.ExecuteSqlCommandSimple("usp_HCP_Prod_InsertSubFolder", new
                {
                    ParentKey = model.FolderKey,
                    FhaNo = model.FhaNo,
                    SubfolderSequence = model.SubfolderSequence,
                    ProjectNo = model.ProjectNo,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn,
                    PageTypeId = model.PageTypeId,
                    GroupTaskInstanceId = model.GroupTaskInstanceId
                });

            }
            catch (Exception ex)
            {
            }
            return -1;
        }
        public OPAViewModel GetProdOPAByChildTaskId(Guid childTaskInstanceId)
        {
            var context = (HCP_live)this.Context;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetOPAByChildTaskId, please pass in correct context in unit of work.");
            context.Database.CommandTimeout = 600;
            var results = context.Database.SqlQuerySimple<usp_HCP_GetOPAByChildTaskId_Result>("usp_HCP_Prod_GetOPAByChildTaskId",
            new
            {
                TaskInstanceId = childTaskInstanceId
            }).FirstOrDefault();

            return Mapper.Map<OPAViewModel>(results);
        }

        public OPAViewModel GetProdOPAByTaskXrefId(Guid taskXrefId)
        {
            var context = (HCP_live)this.Context;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetOPAByChildTaskId, please pass in correct context in unit of work.");
            context.Database.CommandTimeout = 600;
            var results = context.Database.SqlQuerySimple<usp_HCP_GetOPAByChildTaskId_Result>("usp_HCP_Prod_GetOPAByTaskXrefId",
            new
            {
                TaskInstanceId = taskXrefId
            }).FirstOrDefault();

            return Mapper.Map<OPAViewModel>(results);
        }

        public List<TaskModel> GetProdChildTasksByXrefId(Guid taskXrefId, bool isXref)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaChildTasks = new List<TaskModel>();
                var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaChildTasksByParentTask_Result>("usp_HCP_GetProdChildTasksByXrefId",
                new
                {
                    TaskInstanceId = taskXrefId,
                    IsTaskXref = isXref
                }).ToList();

                opaChildTasks = Mapper.Map<IEnumerable<usp_HCP_GetOpaChildTasksByParentTask_Result>, IEnumerable<TaskModel>>(results).ToList();
                return opaChildTasks;
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        #endregion




        public OPAViewModel GetProdOPAByFha(string FhaNumber)
        {
            var context = (HCP_live)this.Context;
            //var result = (from n in context.OpaForm
            //              select n).FirstOrDefault(
            //        p =>
            //            p.FhaNumber == FhaNumber &&
            //            (p.RequestStatus == (int)RequestStatus.Submit || p.RequestStatus == (int)RequestStatus.Draft || p.RequestStatus == (int)RequestStatus.RequestAdditionalInfo));

            //return Mapper.Map<OPAViewModel>(result);

            var results = context.Database.SqlQuerySimple<OpaForm>("usp_HCP_Prod_GetProdOPAByFHA",
           new
           {
               FhaNumber = FhaNumber
           }).FirstOrDefault();

            return Mapper.Map<OPAViewModel>(results);

        }


        public List<Prod_OPAHistoryViewModel> GetGetWPUpload(Guid taskInstanceId, int Status, int PageTypeId)
        {
            try
            {
                int userId = UserPrincipal.Current.UserId;
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<Prod_OPAHistoryViewModel>();
                parentChildTaskRepository = new ParentChildTaskRepository();
                newFileRequestRepository = new NewFileRequestRepository();

                var results = context.Database.SqlQuerySimple<usp_HCP_Prod_GetOpaHistory_Result>("usp_HCP_Prod_GetWPUpload",
                new
                {
                    TaskInstanceId = taskInstanceId,
                    UserId = userId,
                    Status = Status,
                    PageTypeId = PageTypeId

                }).ToList();
                opaHistory = Mapper.Map<IEnumerable<usp_HCP_Prod_GetOpaHistory_Result>, IEnumerable<Prod_OPAHistoryViewModel>>(results).ToList();


                return opaHistory.OrderByDescending(p => p.actionDate).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public void UpdateOPAGroupTask(OPAViewModel OPAActionViewModel)
        {
            var context = this.Context as HCP_live;

            var OpaStat = (from g in context.OpaForm
                           where g.TaskInstanceId == OPAActionViewModel.TaskInstanceId
                           select g).FirstOrDefault();
            if (OpaStat != null)
            {
                OpaStat.ServicerSubmissionDate = OPAActionViewModel.ServicerSubmissionDate;
                OpaStat.RequestStatus = OPAActionViewModel.RequestStatus;
                OpaStat.ServicerComments = OPAActionViewModel.ServicerComments;
                OpaStat.ModifiedBy = UserPrincipal.Current.UserId;
                //OpaStat.IsAddressChanged = OPAActionViewModel.IsAddressChange;
                this.Update(OpaStat);
                context.SaveChanges();
            }
        }

        // #605
        //Added by siddu Internalwork product//
        public List<OPAWorkProductModel> getAllOPAWorkInternalProducts(Guid taskInstanceId)
        {

            try
            {
                var context = this.Context as HCP_task;
                var liveContext = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in getAllOPAWorkProducts, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaWorkProduct = new List<OPAWorkProductModel>();
                //var results = from a in context.TaskFiles
                //              where a.TaskInstanceId.Equals(taskInstanceId) select a;

                var results = context.Database.SqlQuerySimple<USP_GetOPAFiles_Result>(
                   "USP_GetOPAFilesByTaskIdInternalwork",
                   new
                   {
                       TaskInstanceId = taskInstanceId,
                       // FileType = FileId
                   }).ToList();

                opaWorkProduct = Mapper.Map<IEnumerable<USP_GetOPAFiles_Result>, IEnumerable<OPAWorkProductModel>>(results).ToList();
                foreach (var internalprod in opaWorkProduct)
                {
                    if (internalprod.FolderKey == null)
                    {
                        internalprod.FolderKey = 40;
                    }
                }
                opaWorkProduct = opaWorkProduct.Any() ? opaWorkProduct.Where(x => x.FolderKey == 40).ToList() : opaWorkProduct;
                return opaWorkProduct;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;

        }

        //#664 harish added 
        public List<OPAHistoryViewModel> GetOpaHistoryRAI(Guid taskInstanceId)
        {
            try
            {
                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaHistory = new List<OPAHistoryViewModel>();
                parentChildTaskRepository = new ParentChildTaskRepository();
                newFileRequestRepository = new NewFileRequestRepository();
                if (parentChildTaskRepository.IsParentTaskAvailable(taskInstanceId))
                {
                    var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>("usp_HCP_GetOpaHistoryForRequestAdditionalInfo",
                    new
                    {
                        TaskInstanceId = taskInstanceId
                    }).OrderByDescending(p => p.actionTaken).ToList();
                    opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                    if (newFileRequestRepository.IsNewFileRequest(taskInstanceId))
                    {
                        var newFileRequestModel = newFileRequestRepository.GetNewFileRequest(taskInstanceId);
                        if (newFileRequestModel != null)
                        {
                            userRepository = new UserRepository();
                            var userNameAndRole = userRepository.GetNameAndRoleForUser(newFileRequestModel.RequestedBy);
                            var userArray = userNameAndRole.Split(',');
                            var opaHistoryViewModel = new OPAHistoryViewModel()
                            {
                                actionTaken = "New File Request",
                                name = userArray[0] + " " + userArray[1],
                                userRole = "AE",
                                comment = newFileRequestModel.Comments,
                                submitDate = newFileRequestModel.RequestedOn

                            };
                            opaHistory.Add(opaHistoryViewModel);


                        }
                    }
                }
                else
                {
                    //var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_GetOpaHistory",
                    //new
                    //{
                    //    TaskInstanceId = taskInstanceId
                    //}).ToList();

                    var results = context.Database.SqlQuerySimple<usp_HCP_GetOpaHistory_Result>("usp_HCP_GetOpaHistory_test",
                    new
                    {
                        TaskInstanceId = taskInstanceId,
                        rolename = UserPrincipal.Current.UserRole
                    }).ToList();
                    opaHistory = Mapper.Map<IEnumerable<usp_HCP_GetOpaHistory_Result>, IEnumerable<OPAHistoryViewModel>>(results).ToList();
                }


                return opaHistory.OrderByDescending(p => p.actionDate).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        // harish added new Store Procedure for RIS 07-01-2020
        public List<OPAWorkProductModel> RISgetAllOPAWorkInternalProducts(Guid taskInstanceId)
        {

            try
            {
                var context = this.Context as HCP_task;
                var liveContext = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from db Live in getAllOPAWorkProducts, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var opaWorkProduct = new List<OPAWorkProductModel>();
                //var results = from a in context.TaskFiles
                //              where a.TaskInstanceId.Equals(taskInstanceId) select a;

                var results = context.Database.SqlQuerySimple<USP_GetOPAFiles_Result>(
                   "USP_GetRISOPAFilesByTaskIdInternalworkToAE",
                   new
                   {
                       TaskInstanceId = taskInstanceId,
                       // FileType = FileId
                   }).ToList();

                opaWorkProduct = Mapper.Map<IEnumerable<USP_GetOPAFiles_Result>, IEnumerable<OPAWorkProductModel>>(results).ToList();
                opaWorkProduct = opaWorkProduct.Any() ? opaWorkProduct.Where(x => x.FolderKey == 40).ToList() : opaWorkProduct;
                return opaWorkProduct;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;

        }

        // harish added new line of code for updating 
        public int Updateprojectactiontypeid(OPAViewModel dataInModel, int Projectactiontypeid)
        {


            var context = (HCP_live)this.Context;
            //var context = (HCP_live)this.Context;

            var projectionactiontypeidupdate = (from n in context.OpaForm
                                                where n.ProjectActionFormId == dataInModel.ProjectActionFormId
                                                select n).FirstOrDefault();
            projectionactiontypeidupdate.ProjectActionTypeId = Projectactiontypeid;

            this.Update(projectionactiontypeidupdate);

            Context.SaveChanges();

            return projectionactiontypeidupdate.ProjectActionTypeId;

        }

        //karri, venkat#832; karri venkat #848
        public int GetForm290ClosingPreChecksValue(string FHANumber)
        {
            try
            {

                var context = this.Context as HCP_live;

                if (context == null)
                    throw new InvalidCastException("context is not from  Live db, please pass in correct context in unit of work.");
                context.Database.CommandTimeout = 600;
                var results = context.Database.SqlQuerySimple<Int32>("usp_HCP_Form290ClosingPreCheckConditions",
                new
                {

                    FHANumber = FHANumber

                }).ToList();
                List<Int32> retValues = new List<Int32>();
                retValues.AddRange(results);

                return retValues[0];

            }
            catch (Exception ex)
            {
            }
            return -1;
        }

    }
}
