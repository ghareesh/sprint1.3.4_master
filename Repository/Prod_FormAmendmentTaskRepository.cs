﻿using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class Prod_FormAmendmentTaskRepository : BaseRepository<Prod_FormAmendmentTask>, IProd_FormAmendmentTaskRepository
	{
		public Prod_FormAmendmentTaskRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public Prod_FormAmendmentTaskRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<Prod_FormAmendmentTask> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public Prod_FormAmendmentTaskModel GetFormAmendmentById(int pAmendmentTemplateID)
		{
			var amendment = this.GetByIDFast(pAmendmentTemplateID);
			return Mapper.Map<Prod_FormAmendmentTaskModel>(amendment);
		}

		public IList<Prod_FormAmendmentTaskModel> GetFormAmendmentByFHANumber(string pFHANumber)
		{
			var amendment = this.Find(o => o.FHANumber == pFHANumber).ToList();
			List<Prod_FormAmendmentTask> olistProd_FormAmendmentTask = this.Find(o => o.FHANumber == pFHANumber).ToList();
			return Mapper.Map<List<Prod_FormAmendmentTaskModel>>(olistProd_FormAmendmentTask);
		}
		public IList<Prod_FormAmendmentTaskModel> GetFormAmendmentByTaskInstanceId(Guid pTaskInstanceId)
		{
			var amendment = this.GetAll().Where(o => o.TaskInstanceId == pTaskInstanceId).OrderByDescending(o => o.ModifiedOn).ToList();

			return Mapper.Map<IList<Prod_FormAmendmentTaskModel>>(amendment);
		}

		public bool SaveAmendment(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel)
		{
			var context = this.Context as HCP_task;
			if (pProd_FormAmendmentTaskModel.AmendmentTemplateID > 0)
			{
				if (pProd_FormAmendmentTaskModel.RoleType.Equals(HUDRole.ProductionWlm.ToString()))
				{
					List<Prod_FormAmendmentTaskModel> olistProd_FormAmendmentTaskModel = GetFormAmendmentByTaskInstanceId(pProd_FormAmendmentTaskModel.TaskInstanceId.Value).ToList();
					Prod_FormAmendmentTaskModel objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();


					if (olistProd_FormAmendmentTaskModel != null && olistProd_FormAmendmentTaskModel.Count > 0)
						objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.Where(o => o.CreatedBy == pProd_FormAmendmentTaskModel.CreatedBy).FirstOrDefault();
					if (objProd_FormAmendmentTaskModel == null)
					{
						pProd_FormAmendmentTaskModel.CreatedOn = DateTime.UtcNow;
						pProd_FormAmendmentTaskModel.ModifiedOn = DateTime.UtcNow;
						pProd_FormAmendmentTaskModel.AmendmentTemplateID = 0;
						var tblEnt = Mapper.Map<Prod_FormAmendmentTask>(pProd_FormAmendmentTaskModel);
						context.Prod_FormAmendmentTask.Add(tblEnt);
						context.SaveChanges();
					}
					else
					{
						return true;
						
					}
				}
				if (pProd_FormAmendmentTaskModel.RoleType.Equals(HUDRole.ProductionUser.ToString()))
				{
					pProd_FormAmendmentTaskModel.ModifiedOn = DateTime.UtcNow;
					var tblEnt = Mapper.Map<Prod_FormAmendmentTask>(pProd_FormAmendmentTaskModel);
					context.Prod_FormAmendmentTask.Attach(tblEnt);
					context.Entry(tblEnt).State = System.Data.Entity.EntityState.Modified;
					context.SaveChanges();
				}
			}
			else
			{
				pProd_FormAmendmentTaskModel.CreatedOn = DateTime.UtcNow;
				pProd_FormAmendmentTaskModel.ModifiedOn = DateTime.UtcNow;
				var tblEnt = Mapper.Map<Prod_FormAmendmentTask>(pProd_FormAmendmentTaskModel);
				context.Prod_FormAmendmentTask.Add(tblEnt);
				context.SaveChanges();
			}
			
			return true;
		}

		public bool SaveDapFromAmendmentSP(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel)
		{
			var context = this.Context as HCP_task;
			IList<Prod_FormAmendmentTaskModel> objListProd_FormAmendmentTaskModel = new List<Prod_FormAmendmentTaskModel>();
			objListProd_FormAmendmentTaskModel = GetFormAmendmentByFHANumber(pProd_FormAmendmentTaskModel.FHANumber);
			foreach (var obj in objListProd_FormAmendmentTaskModel)
			{
				if (obj.FirmAmendmentNum == pProd_FormAmendmentTaskModel.FirmAmendmentNum && obj.TaskInstanceId == pProd_FormAmendmentTaskModel.TaskInstanceId)
				{
					var amendment = this.GetByIDFast(obj.AmendmentTemplateID);
					amendment.ModifiedOn = DateTime.UtcNow;
					amendment.DAPCompletedDate = pProd_FormAmendmentTaskModel.DAPCompletedDate;
					amendment.LoanAmount = pProd_FormAmendmentTaskModel.LoanAmount;
					var tblEnt = Mapper.Map<Prod_FormAmendmentTask>(amendment);
					context.Prod_FormAmendmentTask.Attach(tblEnt);
					context.Entry(tblEnt).State = System.Data.Entity.EntityState.Modified;
					context.SaveChanges();
				}
			}

			return true;
		}
	}
}
