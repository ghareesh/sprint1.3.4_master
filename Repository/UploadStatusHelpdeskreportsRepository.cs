﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_live.Programmability;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class UploadStatusHelpdeskreportsRepository : BaseRepository<usp_HCP_GetTicketDetails_Latest>, IUploadStatusHelpdeskreportsRepository
    {
        public UploadStatusHelpdeskreportsRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public UploadStatusHelpdeskreportsRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        public IEnumerable<usp_HCP_GetTicketDetails_Latest> GetUploadStatusDetailByhelpdeskreport(string Status, string FromDate, string ToDate, string HelpDeskName)
        {
            //var context = this.Context as HCP_live;
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in  usp_HCP_GetTicketDetails_Latest, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_GetTicketDetails_Latest>(" usp_HCP_GetTicketDetails_Latest", new { }).ToList();
            // return Mapper.Map<IEnumerable<HD_TICKET_ViewModel>>(results);
            //  List<HD_TICKET_ViewModel> htvm = results.Select(a => new HD_TICKET_ViewModel
            //  {
            //      TICKET_ID = a.TICKET_ID,
            //      Numberofdays = a.Numberofdays,
            //      Numberofweeks = a.Numberofweeks,
            //      STATUS = a.STATUS
            //,
            //      CREATED_ON = a.CREATED_ON
            //,
            //      LAST_MODIFIED_ON = a.LAST_MODIFIED_ON,
            //  }).ToList();


            return results;

        }
    }
}
