﻿using System;
using System.Collections.Generic;
using AutoMapper;
using HUDHealthcarePortal.Model;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_intermediate;
using EntityObject.Entities.HCP_live.Programmability;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Model.ProjectAction;
using Model;
using Model.ProjectAction;
using TaskDB = EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model.AssetManagement;
using usp_HCP_Get_MissingProject_ManagementReport_Result = EntityObject.Entities.HCP_live.usp_HCP_Get_MissingProject_ManagementReport_Result;
using usp_HCP_GetProjectDetailReport_Result = EntityObject.Entities.HCP_live.usp_HCP_GetProjectDetailReport_Result;
using usp_HCP_GetProjectDetailReport_HighLevel_Result = EntityObject.Entities.HCP_live.usp_HCP_GetProjectDetailReport_HighLevel_Result;
using usp_HCP_GetProjectDetailReport_Error_Result = EntityObject.Entities.HCP_live.usp_HCP_GetProjectDetailReport_Error_Result;
using usp_HCP_GetProjectDetailReport_PT_Result = EntityObject.Entities.HCP_live.usp_HCP_GetProjectDetailReport_PT_Result;
using usp_HCP_GetProjectDetailReport_CurrentQuarter_Result = EntityObject.Entities.HCP_live.usp_HCP_GetProjectDetailReport_CurrentQuarter_Result;
using EntityObject.Entities.HCP_task;
using EntityObject.Entities.HCP_task.Programmability;
using Model.Production;
using Model.InternalExternalTask;
using Model.AssetManagement;

namespace HUDHealthcarePortal.Repository
{
    public static class AutoMapperRepositoryConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<usp_HCP_Get_ExcelUpload_View_Result, ReportExcelUpload_Model>();
                cfg.CreateMap<usp_HCP_GetUploadUserByLdiID_Result, UserViewModel>();
                cfg.CreateMap<Lender_DataUpload_Intermediate, ExcelUploadView_Model>()
                    .ForMember(dest => dest.DataSource, map => map.MapFrom(src => DBSource.Intermediate));
                cfg.CreateMap<ExcelUploadView_Model, Lender_DataUpload_Intermediate>();
                cfg.CreateMap<Lender_DataUpload_Live, ExcelUploadView_Model>()
                    .ForMember(dest => dest.DataSource, map => map.MapFrom(src => DBSource.Live));
                cfg.CreateMap<Lender_DataUpload_Live, ExcelUploadView_Model>();
                cfg.CreateMap<usp_HCP_AuthenticateInitial_LogIn_Result, UserInfoModel>();
                cfg.CreateMap<usp_HCP_GetOperatorsByLenderId_Result, UserInfoModel>();
                cfg.CreateMap<usp_HCP_GetLenderUsersByLenderId_Result, UserInfoModel>();
                cfg.CreateMap<usp_HCP_GetLenderUsersByAeUserId_Result, UserInfoModel>();
                cfg.CreateMap<Address, AddressModel>();
                cfg.CreateMap<AddressModel, Address>();
                cfg.CreateMap<HCP_Authentication, UserViewModel>()
                    .ForMember(dest => dest.SelectedTimezoneId, map => map.MapFrom(src => src.PreferredTimeZone))
                    .ForMember(dest => dest.MiddleName, map => map.MapFrom(src => src.UserMInitial));
                cfg.CreateMap<UserViewModel, HCP_Authentication>()
                    .ForMember(dest => dest.PreferredTimeZone, map => map.MapFrom(src => src.SelectedTimezoneId));
                cfg.CreateMap<CommentModel, Comment>();
                cfg.CreateMap<ProjectInfo, ProjectInfoModel>();
                cfg.CreateMap<ProjectInfoModel, ProjectInfo>();
                cfg.CreateMap<Comment, CommentModel>();
                cfg.CreateMap<HCP_Email, EmailModel>();
                cfg.CreateMap<EmailModel, HCP_Email>();
                cfg.CreateMap<SecurityQuestion, SecurityQuestionLookup>();
                cfg.CreateMap<SecurityQuestionLookup, SecurityQuestion>();
                cfg.CreateMap<EmailTypeLookup, HCP_EmailType>();
                cfg.CreateMap<HCP_EmailType, EmailTypeLookup>();
                // UploadStatusViewModel is one way mapping (read only)
                cfg.CreateMap<usp_HCP_Get_QuarterlyUploadReport_View_Result, UploadStatusViewModel>();
                cfg.CreateMap<usp_HCP_Get_QuarterlyUploadReportByQuarter_Result, UploadStatusViewModel>();

                cfg.CreateMap<usp_HCP_Get_ProjectInfoReport_Result, ProjectInfoViewModel>();

                cfg.CreateMap<usp_HCP_Get_Admin_ManageUsers_View_Result, UserViewModel>()
                    .ForMember(dest => dest.SelectedTimezone, map => map.MapFrom(src => src.PreferredTimeZone))
                    .ForMember(dest => dest.MiddleName, map => map.MapFrom(src => src.UserMInitial));
                cfg.CreateMap<usp_HCP_Get_QuarterlyUploadReportDetail_Result, UploadStatusDetailModel>();
                cfg.CreateMap<usp_HCP_Get_Multiple_Loans_PropertyID_Result, ReportsModel>()
                    .ForMember(dest => dest.LenderName, map => map.MapFrom(src => src.Lender_Name));
                cfg.CreateMap<usp_HCP_Get_Multiple_Loans_PropertyID_View_Result, MultiLoanModel>()
                    .ForMember(dest => dest.FhaCount, map => map.MapFrom(src => src.PropertyID_Count));
                cfg.CreateMap<PaginateSortModel<Lender_DataUpload_Intermediate>, PaginateSortModel<ExcelUploadView_Model>>();
                cfg.CreateMap<PaginateSortModel<Lender_DataUpload_Live>, PaginateSortModel<ExcelUploadView_Model>>();
                cfg.CreateMap<FormUploadModel, Lender_DataUpload_Intermediate>();
                cfg.CreateMap<usp_HCP_Get_Multiple_Loans_By_PropertyID_Result, MultiLoanDetailModel>();
                cfg.CreateMap<usp_HCP_GetFhasByLenderIds_Result, FhaInfoModel>()
                    .ForMember(dest => dest.LenderId, map => map.MapFrom(src => src.id));
                cfg.CreateMap<TaskModel, TaskDB.Task>();
                cfg.CreateMap<TaskModel, TaskDB.TaskConcurrency>();
                cfg.CreateMap<TaskDB.Task, TaskModel>();
                cfg.CreateMap<TaskFileModel, TaskDB.TaskFile>();
                cfg.CreateMap<AM_R4RAndNCREDocument, AM_R4RAndNCREDocumentModel>();//harish added for R4R and NCRE
                cfg.CreateMap<AM_R4RAndNCREDocumentModel, AM_R4RAndNCREDocument>();//harish added for R4R and NCRE
                cfg.CreateMap<TaskDB.TaskFile, TaskFileModel>();
                cfg.CreateMap<Lender_FHANumber, LenderFhaModel>();
                cfg.CreateMap<TaskDB.usp_HCP_GetTasksByUserName_Result, TaskModel>()
                    .ForMember(dest=> dest.FHANumber, map=>map.MapFrom(src=>src.FhaNumber));
                cfg.CreateMap<TaskDB.usp_HCP_GetCompletedAndWaitingStartTime_Result, TaskModel>();
                cfg.CreateMap<TaskDB.usp_HCP_GetCompletedAndWaitingStartTime_Result, TaskReAssignmentViewModel>();
                cfg.CreateMap<HUD_Project_Manager, HUDManagerModel>()
                    .ForMember(dest =>dest.Manager_Name, map =>map.MapFrom(src=>src.HUD_Project_Manager_Name))
                    .ForMember(dest => dest.Manager_ID, map => map.MapFrom(src => src.HUD_Project_Manager_ID));
                cfg.CreateMap<HUD_WorkLoad_Manager, HUDManagerModel>();
                cfg.CreateMap<usp_HCP_Get_MissingProjectReportsForLender_Result, ReportViewModel>();
                cfg.CreateMap<Lender_DataUpload_Intermediate, DerivedUploadData>();
                cfg.CreateMap<Lender_DataUpload_Live, DerivedUploadData>();
                cfg.CreateMap<PaginateSortModel<Lender_DataUpload_Live>, PaginateSortModel<DerivedUploadData>>();
                cfg.CreateMap<PaginateSortModel<Lender_DataUpload_Intermediate>, PaginateSortModel<DerivedUploadData>>();
                cfg.CreateMap<usp_HCP_Get_Portfolio_Aggregate_Result, DerivedUploadData>();
                cfg.CreateMap<usp_HCP_Get_Portfolio_Detail_Result, DerivedUploadData>()
                    .ForMember(dest => dest.DataInserted, map => map.MapFrom(src => src.DateInserted));
                cfg.CreateMap<HUD_Project_Manager, UserViewModel>()
                    .ForMember(dest => dest.AddressID, map => map.MapFrom(src => src.AddressID))
                    .ForMember(dest => dest.FirstName, map => map.MapFrom(src => src.HUD_Project_Manager_Name))
                    .ForMember(dest => dest.Title, map => map.MapFrom(src => src.Address.Title))
                    .ForMember(dest => dest.Organization, map => map.MapFrom(src => src.Address.Organization))
                    .ForMember(dest => dest.PhoneNumber, map => map.MapFrom(src => src.Address.PhonePrimary))
                    .ForMember(dest => dest.AddressLine1, map => map.MapFrom(src => src.Address.AddressLine1 + src.Address.AddressLine2))
                    .ForMember(dest => dest.City, map => map.MapFrom(src => src.Address.City))
                    .ForMember(dest => dest.SelectedStateCd, map => map.MapFrom(src => src.Address.StateCode))
                    .ForMember(dest => dest.ZipCode, map => map.MapFrom(src => src.Address.ZIP + src.Address.ZIP4_Code))
                    .ForMember(dest => dest.EmailAddress, map => map.MapFrom(src => src.Address.Email.Trim()))
                    .ForMember(dest => dest.UserName, map => map.MapFrom(src => src.Address.Email.Trim()));
                cfg.CreateMap<HUD_WorkLoad_Manager, UserViewModel>()
                    .ForMember(dest => dest.AddressID, map => map.MapFrom(src => src.AddressID))
                    .ForMember(dest => dest.FirstName, map => map.MapFrom(src => src.HUD_WorkLoad_Manager_Name))
                    .ForMember(dest => dest.Title, map => map.MapFrom(src => src.Address.Title))
                    .ForMember(dest => dest.Organization, map => map.MapFrom(src => src.Address.Organization))
                    .ForMember(dest => dest.PhoneNumber, map => map.MapFrom(src => src.Address.PhonePrimary))
                    .ForMember(dest => dest.AddressLine1, map => map.MapFrom(src => src.Address.AddressLine1 + src.Address.AddressLine2))
                    .ForMember(dest => dest.City, map => map.MapFrom(src => src.Address.City))
                    .ForMember(dest => dest.SelectedStateCd, map => map.MapFrom(src => src.Address.StateCode))
                    .ForMember(dest => dest.ZipCode, map => map.MapFrom(src => src.Address.ZIP + src.Address.ZIP4_Code))
                    .ForMember(dest => dest.EmailAddress, map => map.MapFrom(src => src.Address.Email.Trim()))
                    .ForMember(dest => dest.UserName, map => map.MapFrom(src => src.Address.Email.Trim()));
                cfg.CreateMap<UserViewModel, User_WLM_PM>()
                    .ForMember(dest => dest.HUD_WorkLoad_Manager_ID,
                        map => map.MapFrom(src => src.SelectedWorkloadManagerId))
                    .ForMember(dest => dest.HUD_Project_Manager_ID,
                        map => map.MapFrom(src => src.SelectedAccountExecutiveId));
                cfg.CreateMap<usp_HCP_Get_MissingProject_ManagementReport_Result, ReportGridModel>();
                cfg.CreateMap<usp_HCP_GetProjectDetailReport_Result, ReportGridModel>();
                cfg.CreateMap<usp_HCP_GetProjectDetailReport_PT_Result, ReportGridModel>();
                cfg.CreateMap<usp_HCP_GetProjectDetailReport_CurrentQuarter_Result, ReportGridModel>();

                cfg.CreateMap<usp_HCP_GetProjectDetailReport_Error_Result, ReportGridModel>();
                cfg.CreateMap<usp_HCP_GetProjectDetailReport_HighLevel_Result, ReportGridModel>();
                cfg.CreateMap<usp_HCP_Get_MissingProject_ManagementReport_Result, AddressModel>()
                .ForMember(dest => dest.Email, map => map.MapFrom(src => src.HudWorkLoadManagerEmailAddress));
                cfg.CreateMap<usp_HCP_Get_MissingProject_ManagementReport_HighLevel_Result, ReportGridModel>();
                cfg.CreateMap<AddressModel, ReportGridModel>();
                cfg.CreateMap<ReportGridModel, ReportHeaderModel>();
                cfg.CreateMap<usp_HCP_Get_WLM_AE_Details_Result, ReportHeaderModel>();
                cfg.CreateMap<usp_HCP_GetRiskSummaryReportByQuarter_Result, ExcelUploadView_Model>();
                cfg.CreateMap<usp_HCP_GetRiskSummaryReportDetail_Result, DerivedUploadData>();

                cfg.CreateMap<NonCriticalRepairsViewModel, NonCriticalRepairsRequest>()
                    .ForMember(dest => dest.FHANumber, map => map.MapFrom(src => src.FHANumber))
                    .ForMember(dest => dest.NonCriticalRepairsRequestID, map => map.MapFrom(src => src.NonCriticalRequestId))
                    .ForMember(dest => dest.PaymentRequested, map => map.MapFrom(src => src.PaymentAmountRequested))
                    .ForMember(dest => dest.FileName92117, map => map.MapFrom(src => src.FileName92117))
                    .ForMember(dest => dest.FileName92464, map => map.MapFrom(src => src.FileName92464))
                    .ForMember(dest => dest.DefaultRequestExtensionFileName, map => map.MapFrom(src => src.FileDefaultRequestExtensionFileName))
                    .ForMember(dest => dest.FinalInspectionReport, map => map.MapFrom(src => src.FinalInspectionReport))
                    .ForMember(dest => dest.ScopeCertificationFileName, map => map.MapFrom(src => src.FileScopeCertificationFileName))
                    .ForMember(dest => dest.ContractFileName, map => map.MapFrom(src => src.FileContractFileName))
                    .ForMember(dest => dest.SubmittedDate, map => map.MapFrom(src => src.SubmittedDate))
                    .ForMember(dest => dest.IsFinalDraw, map => map.MapFrom(src => src.IsFinalDraw))
                    .ForMember(dest => dest.ApprovedDate, map => map.MapFrom(src => src.ApprovedDate))
                    .ForMember(dest => dest.CanProvidePhotosInvoices, map => map.MapFrom(src => src.CanProvideAllPhotosInvoices))
                    .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                    .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy))
                    .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
                    .ForMember(dest => dest.IsAdvance, map => map.MapFrom(src => src.IsThisAnAdvance));
                cfg.CreateMap<ReserveForReplacementFormModel, Reserve_For_Replacement_Form_Data>();
                cfg.CreateMap<Reserve_For_Replacement_Form_Data, ReserveForReplacementFormModel>()
                    .ForMember(dest => dest.AddnRemarks, map => map.MapFrom(src => src.AdditionalRemarks));
                cfg.CreateMap<usp_HCP_GetPAMReportDetail_Result, PAMReportViewModel>();
                cfg.CreateMap<usp_HCP_GetProdPAMReportDetail_Result, PAMReportProductionViewModel>();
                cfg.CreateMap<usp_HCP_GetProdPAMReportDetail_Result, ProductionReassignViewModel>();
                cfg.CreateMap<usp_HCP_GetProdPAMReportDetail_Result, LenderProductionReassignViewModel>();// harish added
                cfg.CreateMap<usp_HCP_GetLenderUsersByLenderId_Result, UserInfoModel>();//1904
                cfg.CreateMap<NonCriticalRepairsRequest, NonCriticalRepairsViewModel>()
                    .ForMember(dest => dest.FHANumber, map => map.MapFrom(src => src.FHANumber))
                    .ForMember(dest => dest.NonCriticalRequestId, map => map.MapFrom(src => src.NonCriticalRepairsRequestID))
                    .ForMember(dest => dest.PaymentAmountRequested, map => map.MapFrom(src => src.PaymentRequested))
                    .ForMember(dest => dest.FileName92117, map => map.MapFrom(src => src.FileName92117))
                    .ForMember(dest => dest.FileName92464, map => map.MapFrom(src => src.FileName92464))
                    .ForMember(dest => dest.FileDefaultRequestExtensionFileName, map => map.MapFrom(src => src.DefaultRequestExtensionFileName))
                    .ForMember(dest => dest.FinalInspectionReport, map => map.MapFrom(src => src.FinalInspectionReport))
                    .ForMember(dest => dest.FileScopeCertificationFileName, map => map.MapFrom(src => src.ScopeCertificationFileName))
                    .ForMember(dest => dest.FileContractFileName, map => map.MapFrom(src => src.ContractFileName))
                    .ForMember(dest => dest.SubmittedDate, map => map.MapFrom(src => src.SubmittedDate))
                    .ForMember(dest => dest.IsFinalDraw, map => map.MapFrom(src => src.IsFinalDraw))
                    .ForMember(dest => dest.ApprovedDate, map => map.MapFrom(src => src.ApprovedDate))
                    .ForMember(dest => dest.CanProvideAllPhotosInvoices, map => map.MapFrom(src => src.CanProvidePhotosInvoices))
                    .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                    .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy))
                    .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
                    .ForMember(dest => dest.IsThisAnAdvance, map => map.MapFrom(src => src.IsAdvance));
                cfg.CreateMap<NonCriticalRequestExtensionModel, NonCriticalRequestExtension>()
                    .ForMember(dest => dest.FHANumber, map => map.MapFrom(src => src.FHANumber))
                    .ForMember(dest => dest.NonCriticalRequestExtensionId,
                        map => map.MapFrom(src => src.NonCriticalRequestExtensionId))
                    .ForMember(dest => dest.ExtensionPeriod, map => map.MapFrom(src => src.ExtensionPeriod))
                    .ForMember(dest => dest.ExtensionRequestStatus,
                        map => map.MapFrom(src => src.ExtensionRequestStatus))
                    .ForMember(dest => dest.ClosingDate, map => map.MapFrom(src => src.ClosingDate))
                    .ForMember(dest => dest.Comments, map => map.MapFrom(src => src.Comments))
                    .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy))
                    .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                    .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
                    .ForMember(dest => dest.LenderID, map => map.MapFrom(src => src.LenderID))
                    .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn));
                cfg.CreateMap<NonCriticalRequestExtension, NonCriticalRequestExtensionModel>()
                    .ForMember(dest => dest.FHANumber, map => map.MapFrom(src => src.FHANumber))
                    .ForMember(dest => dest.NonCriticalRequestExtensionId,
                        map => map.MapFrom(src => src.NonCriticalRequestExtensionId))
                    .ForMember(dest => dest.ExtensionPeriod, map => map.MapFrom(src => src.ExtensionPeriod))
                    .ForMember(dest => dest.ExtensionRequestStatus,
                        map => map.MapFrom(src => src.ExtensionRequestStatus))
                    .ForMember(dest => dest.ClosingDate, map => map.MapFrom(src => src.ClosingDate))
                    .ForMember(dest => dest.Comments, map => map.MapFrom(src => src.Comments))
                    .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy))
                    .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                    .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
                    .ForMember(dest => dest.LenderID, map => map.MapFrom(src => src.LenderID))
                    .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn));
                cfg.CreateMap<HCP_EmailTemplateMessages, EmailTemplateMessagesModel>();
                cfg.CreateMap<NonCriticalLateAlertsModel, NonCriticalLateAlerts>()
                    .ForMember(dest => dest.PropertyID, map => map.MapFrom(src => src.PropertyID))
                    .ForMember(dest => dest.LenderID, map => map.MapFrom(src => src.LenderID))
                    .ForMember(dest => dest.FHANumber, map => map.MapFrom(src => src.FHANumber))
                    .ForMember(dest => dest.TimeSpanId, map => map.MapFrom(src => src.TimeSpanId))
                    .ForMember(dest => dest.HCP_EmailId, map => map.MapFrom(src => src.HCP_EmailId))
                    .ForMember(dest => dest.RecipientIds, map => map.MapFrom(src => src.RecipientIds));
                cfg.CreateMap<TransactionModel, Transaction>()
                    .ForMember(dest => dest.TransactionId, map => map.MapFrom(src => src.TransactionId))
                    .ForMember(dest => dest.TransactionType, map => map.MapFrom(src => src.TransactionType))
                    .ForMember(dest => dest.Amount, map => map.MapFrom(src => src.Amount))
                    .ForMember(dest => dest.CurrentBalance, map => map.MapFrom(src => src.CurrentBalance))
                    .ForMember(dest => dest.FormId, map => map.MapFrom(src => src.FormId))
                    .ForMember(dest => dest.FormType, map => map.MapFrom(src => src.FormType))
                    .ForMember(dest => dest.Status, map => map.MapFrom(src => src.Status))
                    .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                    .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn));

                cfg.CreateMap<NonCriticalRepairsViewModel, TransactionModel>()
                   .ForMember(dest => dest.Amount, map => map.MapFrom(src => src.PaymentAmountRequested))
                   .ForMember(dest => dest.CurrentBalance, map => map.MapFrom(src => src.NonCriticalCurrentBalance))
                   .ForMember(dest => dest.FormId, map => map.MapFrom(src => src.NonCriticalRequestId))
                   .ForMember(dest => dest.Status, map => map.MapFrom(src => src.RequestStatus))
                   .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                   .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn));
                cfg.CreateMap<NonCriticalRulesChecklist, NonCriticalRulesChecklistModel>();
                cfg.CreateMap<NonCriticalReferReason , NonCriticalReferReasonModel>();
                cfg.CreateMap<NonCriticalReferReasonModel, NonCriticalReferReason>();
                cfg.CreateMap<Transaction, TransactionModel>();
                cfg.CreateMap<QuestionViewModel, CheckListQuestion>();
                cfg.CreateMap<CheckListQuestion, QuestionViewModel>();

                cfg.CreateMap<ProjectActionViewModel, ProjectActionForm>()
                    .ForMember(dest => dest.ProjectActionFormId, map => map.MapFrom(src => src.ProjectActionFormId))
                    .ForMember(dest => dest.FhaNumber, map => map.MapFrom(src => src.FhaNumber))
                    .ForMember(dest => dest.MyTaskId, map => map.MapFrom(src => src.MytaskId))
                    .ForMember(dest => dest.ProjectActionStartDate, map => map.MapFrom(src => src.ProjectActionDate))
                    .ForMember(dest => dest.ProjectActionTypeId, map => map.MapFrom(src => src.ProjectActionTypeId))
                    .ForMember(dest => dest.PropertyName, map => map.MapFrom(src => src.PropertyName))
                    .ForMember(dest => dest.ServicerSubmissionDate, map => map.MapFrom(src => src.ServicerSubmissionDate))
                    .ForMember(dest => dest.RequestStatus, map => map.MapFrom(src => src.RequestStatus))
                    .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                    .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
                    .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
                    .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy));

                cfg.CreateMap<OPAViewModel, OpaForm>()
           .ForMember(dest => dest.ProjectActionFormId, map => map.MapFrom(src => src.ProjectActionFormId))
           .ForMember(dest => dest.FhaNumber, map => map.MapFrom(src => src.FhaNumber))
           .ForMember(dest => dest.MyTaskId, map => map.MapFrom(src => src.MytaskId))
           .ForMember(dest => dest.ProjectActionStartDate, map => map.MapFrom(src => src.ProjectActionDate))
           .ForMember(dest => dest.ProjectActionTypeId, map => map.MapFrom(src => src.ProjectActionTypeId))
           .ForMember(dest => dest.PropertyName, map => map.MapFrom(src => src.PropertyName))
           .ForMember(dest => dest.ServicerSubmissionDate, map => map.MapFrom(src => src.ServicerSubmissionDate))
           .ForMember(dest => dest.RequestStatus, map => map.MapFrom(src => src.RequestStatus))
           .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
           .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
           .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
           .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy))
           .ForMember(dest => dest.TaskInstanceId, map => map.MapFrom(src => src.TaskInstanceId))
           .ForMember(dest => dest.RequesterName, map => map.MapFrom(src => src.RequesterName));


                cfg.CreateMap<HCP_Project_Action, ProjectActionTypeViewModel>();
                cfg.CreateMap<GroupTaskModel, TaskDB.GroupTask>();
                //Production
                cfg.CreateMap<ProdGroupTaskModel, TaskDB.Prod_GroupTasks>();
                cfg.CreateMap<TaskDB.Prod_GroupTasks, ProdGroupTaskModel>();
                cfg.CreateMap<ProjectActionForm, ProjectActionViewModel>()
                   .ForMember(dest => dest.ProjectActionFormId, map => map.MapFrom(src => src.ProjectActionFormId))
                   .ForMember(dest => dest.FhaNumber, map => map.MapFrom(src => src.FhaNumber))
                   .ForMember(dest => dest.MytaskId , map => map.MapFrom(src => src.MyTaskId))
                   .ForMember(dest => dest.ProjectActionDate, map => map.MapFrom(src => src.ProjectActionStartDate))
                   .ForMember(dest => dest.ProjectActionTypeId, map => map.MapFrom(src => src.ProjectActionTypeId))
                   .ForMember(dest => dest.PropertyName, map => map.MapFrom(src => src.PropertyName))
                   .ForMember(dest => dest.ServicerSubmissionDate, map => map.MapFrom(src => src.ServicerSubmissionDate))
                   .ForMember(dest => dest.RequestStatus, map => map.MapFrom(src => src.RequestStatus))
                   .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                   .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
                   .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))


                   .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy));


                cfg.CreateMap<OpaForm, OPAViewModel>()
                  .ForMember(dest => dest.ProjectActionFormId, map => map.MapFrom(src => src.ProjectActionFormId))
                  .ForMember(dest => dest.FhaNumber, map => map.MapFrom(src => src.FhaNumber))
                  .ForMember(dest => dest.MytaskId, map => map.MapFrom(src => src.MyTaskId))
                  .ForMember(dest => dest.ProjectActionDate, map => map.MapFrom(src => src.ProjectActionStartDate))
                  .ForMember(dest => dest.ProjectActionTypeId, map => map.MapFrom(src => src.ProjectActionTypeId))
                  .ForMember(dest => dest.PropertyName, map => map.MapFrom(src => src.PropertyName))
                  .ForMember(dest => dest.ServicerSubmissionDate, map => map.MapFrom(src => src.ServicerSubmissionDate))
                  .ForMember(dest => dest.RequestStatus, map => map.MapFrom(src => src.RequestStatus))
                  .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                  .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
                  .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
                  .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy))
                  .ForMember(dest => dest.TaskInstanceId, map => map.MapFrom(src => src.TaskInstanceId));


                cfg.CreateMap<HCP_User_LoginTime, UserLoginModel>()
                  .ForMember(dest => dest.UserLoginID, map => map.MapFrom(src => src.UserLoginID))
                  .ForMember(dest => dest.Login_Time, map => map.MapFrom(src => src.Login_Time))
                  .ForMember(dest => dest.Logout_Time, map => map.MapFrom(src => src.Logout_Time))
                  .ForMember(dest => dest.RoleName, map => map.MapFrom(src => src.RoleName))
                  .ForMember(dest => dest.Session_Id, map => map.MapFrom(src => src.Session_Id))
                  .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
                  .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
                  .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                   .ForMember(dest => dest.User_id, map => map.MapFrom(src => src.User_id))
                  .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy));


                cfg.CreateMap<UserLoginModel, HCP_User_LoginTime>()
                  .ForMember(dest => dest.UserLoginID, map => map.MapFrom(src => src.UserLoginID))
                  .ForMember(dest => dest.Login_Time, map => map.MapFrom(src => src.Login_Time))
                  .ForMember(dest => dest.Logout_Time, map => map.MapFrom(src => src.Logout_Time))
                  .ForMember(dest => dest.RoleName, map => map.MapFrom(src => src.RoleName))
                  .ForMember(dest => dest.Session_Id, map => map.MapFrom(src => src.Session_Id))
                  .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
                  .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
                  .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                  .ForMember(dest => dest.User_id, map => map.MapFrom(src => src.User_id))
                  .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy));

                cfg.CreateMap<Menu_Restrictions, MenuRestrictionsViewmodel>()
                  .ForMember(dest => dest.RestrictionId, map => map.MapFrom(src => src.Restriction_Id))
                  .ForMember(dest => dest.LenderId, map => map.MapFrom(src => src.Lender_Id))
                  .ForMember(dest => dest.MenuName, map => map.MapFrom(src => src.Menu_Name));


                cfg.CreateMap<usp_HCP_GetOpaChildTasksByParentTask_Result, ParentChildTaskModel>();

                cfg.CreateMap<ParentChildTask, ParentChildTaskModel>();
                cfg.CreateMap<ParentChildTaskModel, ParentChildTask>();
                //.ForMember(dest => dest.ParentTaskInstanceId, map => map.MapFrom(src => src.ParentTaskInstanceId))
                // .ForMember(dest => dest.ParentTaskInstanceId, map => map.MapFrom(src => src.TaskInstanceId)
                //  .ForMember(dest => dest.CreatedOn, map => map.MapFrom(src => src.CreatedOn))
                //  .ForMember(dest => dest.CreatedBy, map => map.MapFrom(src => src.CreatedBy))
                // .ForMember(dest => dest.ParentTaskInstanceId, map => map.MapFrom(src => src.ParentTaskInstanceId));


                cfg.CreateMap<TaskDB.GroupTask, GroupTaskModel>();
                cfg.CreateMap<TaskDB.usp_HCP_GetTaskDetailsForAE_Result,TaskDetailPerAeModel>();
                cfg.CreateMap<TaskReAssignmentViewModel, TaskDB.TaskReAssignment>();
                cfg.CreateMap<TaskDB.TaskReAssignment, TaskDetailPerAeModel>();
                cfg.CreateMap<TaskDB.usp_HCP_GetReAssignedTasksByUserName_Result, TaskReAssignmentViewModel>();
                cfg.CreateMap<usp_HCP_GetOpaHistory_Result, OPAHistoryViewModel>();
                cfg.CreateMap<usp_HCP_Prod_GetOpaHistory_Result, Prod_OPAHistoryViewModel>();

                cfg.CreateMap<USP_GetOPAFiles_Result, OPAWorkProductModel>();

                cfg.CreateMap<usp_HCP_GetOpaChildTasksByParentTask_Result, TaskModel>();
                cfg.CreateMap<AdditionalInformation, AdditionalInformationModel>();
                cfg.CreateMap<AdditionalInformationModel, AdditionalInformation>();
                cfg.CreateMap<ParentChildTaskModel, TaskDB.ParentChildTask>();
                cfg.CreateMap<ReviewerTitles, ReviewerTitlesModel>();

                  cfg.CreateMap<ReviewerTaskAssignment, ReviewerTaskAssigmentViewModel>()

                   .ForMember(dest => dest.TaskInstanceId, map => map.MapFrom(src => src.TaskInstanceId))
                   .ForMember(dest => dest.FHANumber, map => map.MapFrom(src => src.FHANumber))
                   .ForMember(dest => dest.ReviwerUserId, map => map.MapFrom(src => src.ReviwerUserId))
                    .ForMember(dest => dest.Status, map => map.MapFrom(src => src.Status))
                    .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
                    .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy));



                cfg.CreateMap<ReviewerTaskAssigmentViewModel, ReviewerTaskAssignment>()

                    .ForMember(dest => dest.TaskInstanceId, map => map.MapFrom(src => src.TaskInstanceId))
                    .ForMember(dest => dest.FHANumber, map => map.MapFrom(src => src.FHANumber))
                    .ForMember(dest => dest.ReviwerUserId, map => map.MapFrom(src => src.ReviwerUserId))
                     .ForMember(dest => dest.Status, map => map.MapFrom(src => src.Status))
                     .ForMember(dest => dest.ModifiedOn, map => map.MapFrom(src => src.ModifiedOn))
                     .ForMember(dest => dest.ModifiedBy, map => map.MapFrom(src => src.ModifiedBy));

                cfg.CreateMap<PDRColumn, PDRColumnViewModel>()
                    .ForMember(des => des.ColumnID, map => map.MapFrom(src => src.PDRColumnID))
                    .ForMember(des => des.ColumnName, map => map.MapFrom(src => src.PDRColumnName));

                cfg.CreateMap<PDRUserPreference, PDRUserPreferenceViewModel>()
                     .ForMember(des => des.PDRUserPreferenceID, map => map.MapFrom(src => src.PDRUserPreferenceID))
                    .ForMember(des => des.PDRColumnID, map => map.MapFrom(src => src.PDRColumnID))
                    .ForMember(des => des.UserID, map => map.MapFrom(src => src.UserID))
                    .ForMember(des => des.Visibility, map => map.MapFrom(src => src.Visibility));

                cfg.CreateMap<PDRUserPreferenceViewModel, PDRUserPreference>();
                cfg.CreateMap<usp_HCP_GetOPAByChildTaskId_Result, OPAViewModel>();
                cfg.CreateMap<usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result, OPAHistoryViewModel>();
                cfg.CreateMap<ReviewFileStatusModel, ReviewFileStatus>();
                cfg.CreateMap<RequestAdditionalInfoFileModel, RequestAdditionalInfoFiles>();
                cfg.CreateMap<RequestAdditionalInfoFiles, RequestAdditionalInfoFileModel>();
                cfg.CreateMap<TaskFileHistoryModel, TaskFileHistory>();
                cfg.CreateMap<ReviewFileStatus, ReviewFileStatusModel>();
                cfg.CreateMap<NewFileRequestModel, NewFileRequest>();
                cfg.CreateMap<ReviewFileCommentModel, ReviewFileComment>();
                cfg.CreateMap<NewFileRequest, NewFileRequestModel>();
                cfg.CreateMap<ChildTaskNewFileModel, ChildTaskNewFiles>();



                //Production
                cfg.CreateMap<usp_HCP_Prod_GetAllReviewersStatusByTaskInstanceId_Result, OPAViewModel>();
                cfg.CreateMap<usp_HCP_Prod_GetPendingRAIByTaskInstanceId_Result, OPAViewModel>();
                cfg.CreateMap<usp_HCP_Prod_GetOpaHistory_FolderList_Result, Prod_FolderStructureModel>();
                cfg.CreateMap<usp_HCP_Prod_GetOpaHistory_Result, Prod_OPAHistoryViewModel>();
                cfg.CreateMap<usp_HCP_Prod_GetOpaHistoryForRequestAdditionalInfo_Result, Prod_OPAHistoryViewModel>();
                cfg.CreateMap<Prod_ActivityType, ActivityTypeModel>();
                cfg.CreateMap<Prod_BorrowerType, BorrowerTypeModel>();
                cfg.CreateMap<Prod_ProjectType, ProjectTypeModel>();
                cfg.CreateMap<Prod_LoanType, LoanTypeModel>();
                cfg.CreateMap<FHANumberRequestViewModel, Prod_FHANumberRequest>();
                cfg.CreateMap<Prod_FHANumberRequest, FHANumberRequestViewModel>();
                cfg.CreateMap<Prod_CMSStarRating, CMSStarRatingModel>();
                cfg.CreateMap<SelectedAdditionalFinancingModel, Prod_SelectedAdditionalFinancingResources>();
                cfg.CreateMap<Prod_TaskXrefModel, Prod_TaskXref>();
                cfg.CreateMap<Prod_TaskXref, Prod_TaskXrefModel>();
                cfg.CreateMap<Prod_GroupTasksModel, Prod_GroupTasks>();
                cfg.CreateMap<USP_GetProductionTasksByUserName, ProductionMyTaskModel>();
                cfg.CreateMap<Prod_GroupTasks, Prod_GroupTasksModel>();
                cfg.CreateMap<TaskFile_FolderMappingModel, TaskFile_FolderMapping>();
                cfg.CreateMap<TaskFile_FolderMapping, TaskFile_FolderMappingModel>();
                cfg.CreateMap<USP_GetGroupTasksbyUser_Result, Prod_GroupTasksModel>();
                cfg.CreateMap<Prod_GroupTasksModel, USP_GetGroupTasksbyUser_Result>();
                cfg.CreateMap<Prod_ViewModel, Prod_View>();
                cfg.CreateMap<Prod_View, Prod_ViewModel>();
                cfg.CreateMap<ProductionTaskAssignmentModel, Prod_TaskXref>();

                cfg.CreateMap<EmailRecipientModel, EmailRecipient_Result>();


                cfg.CreateMap<EmailRecipient_Result, EmailRecipientModel>();

                cfg.CreateMap<Prod_NextStageModel, Prod_NextStage>();


                cfg.CreateMap<Prod_NextStage, Prod_NextStageModel>();

                cfg.CreateMap<ProductionTaskAssignmentModel, Prod_TaskXrefModel>();
                cfg.CreateMap<Prod_TaskXrefModel, ProductionTaskAssignmentModel>();
                cfg.CreateMap<Prod_TaskXref, ProductionTaskAssignmentModel>();
                cfg.CreateMap<Prod_SelectedAdditionalFinancingResources, SelectedAdditionalFinancingModel>();
                cfg.CreateMap<Prod_FolderStructure, Prod_FolderStructureModel>();
                cfg.CreateMap<Prod_FolderStructureModel, Prod_FolderStructure>();
                cfg.CreateMap<Prod_Note, Prod_NoteModel>();
                cfg.CreateMap<Prod_NoteModel, Prod_Note>();
                cfg.CreateMap<USP_GetFilteredProductionTasks, FilteredProductionTasksModel>();
                cfg.CreateMap<usp_HCP_Prod_GetFhaSubmittedLenders, FhaSubmittedLendersModel>();
                cfg.CreateMap<FHANumberRequestViewModel, ApplicationDetailViewModel>();
                cfg.CreateMap<USP_HCP_Prod_GetApplicationAndClosingType, ApplicationAndClosingTypeModel>();
                cfg.CreateMap<Prod_SharePointAccountExecutives, Prod_SharePointAccountExecutivesViewModel>();
                cfg.CreateMap<GeneralInformationViewModel, Prod_SharepointScreen>();
                cfg.CreateMap<MiscellaneousInformationViewModel, Prod_SharepointScreen>();
                cfg.CreateMap<TaskFile, SharepointAttachmentsModel>()
                    .ForMember(des => des.UploadedOn, map => map.MapFrom(src => src.UploadTime))
                    .ForMember(des => des.SectionName, map=>map.MapFrom(src=>src.FileType))
                    .ForMember(des => des.UploadedBy, map=>map.MapFrom(src=>src.CreatedBy.ToString()));
                cfg.CreateMap<DocumentTypes, DocumentTypeModel>();
                cfg.CreateMap<Prod_SharepointScreen, AllSharepointData>();
                cfg.CreateMap<InternalExternalTask, InternalExternalTaskModel>();
                cfg.CreateMap<InternalExternalTaskModel, InternalExternalTask>();
                cfg.CreateMap<Prod_Form290Task, Prod_Form290TaskModel>();
                cfg.CreateMap<Prod_Form290TaskModel, Prod_Form290Task>();
                cfg.CreateMap<usp_HCP_Prod_GetProdHUDPAMReportResult, PAMReportV3ViewModel>();
                cfg.CreateMap<HUDPamReportFiltersModel, HUD_PamReportFilters>()
                    .ForMember(des => des.FhaNumber, map => map.MapFrom(src => src.FHANumber))
                    .ForMember(des => des.DateFrom, map => map.MapFrom(src => src.FromDate))
                    .ForMember(des => des.Dateto, map => map.MapFrom(src => src.ToDate));
           

            cfg.CreateMap< HUD_PamReportFilters, HUDPamReportFiltersModel> ()
                   .ForMember(des => des.FHANumber, map => map.MapFrom(src => src.FhaNumber))
                   .ForMember(des => des.FromDate, map => map.MapFrom(src => src.DateFrom))
                   .ForMember(des => des.ToDate, map => map.MapFrom(src => src.Dateto));

				cfg.CreateMap<HUDSignatureModel, HUDSignature>();
                cfg.CreateMap<usp_HCP_Prod_GetAllProdDataResult, Prod_AdhocDataModel>();
            
				cfg.CreateMap<FirmCommitmentAmendmentType, FirmCommitmentAmendmentTypeModel>();
				cfg.CreateMap<SaluatationType, SaluatationTypeModel>();
				cfg.CreateMap<PartyType, PartyTypeModel>();
				cfg.CreateMap<DepositType, DepositTypeModel>();
				cfg.CreateMap<EscrowEstimateType, EscrowEstimateTypeModel>();
				cfg.CreateMap<RepairCostEstimateType, RepairCostEstimateTypeModel>();
				cfg.CreateMap<SpecialConditionType, SpecialConditionTypeModel>();
				cfg.CreateMap<ExhibitLetterType, ExhibitLetterTypeModel>();
				cfg.CreateMap<Prod_FormAmendmentTask, Prod_FormAmendmentTaskModel>();
				cfg.CreateMap<Prod_FormAmendmentTaskModel, Prod_FormAmendmentTask>();
				cfg.CreateMap<usp_HCP_Prod_GetAllProdDataResult, Prod_AdhocDataModel>();

                //LC
                cfg.CreateMap<Prod_LoanCommittee, Prod_LoanCommitteeViewModel>()
                .ForMember(des => des.WLM, map => map.MapFrom(src => src.Wlm));
                cfg.CreateMap<Prod_LoanCommitteeViewModel, Prod_LoanCommittee>()
                .ForMember(des => des.Wlm, map => map.MapFrom(src => src.WLM));
                cfg.CreateMap< RFORRANDNCR_GroupTasks, RFORRANDNCR_GroupTaskModel>();
                cfg.CreateMap<RFORRANDNCR_GroupTaskModel,RFORRANDNCR_GroupTasks>();
            });
        }
    }
}
