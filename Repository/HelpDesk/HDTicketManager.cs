﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Repository;
using System.Collections;
using System.Data;
using EntityObject.Entities.HCP_live.Programmability;
using Repository;

//karri:#149,#214
namespace HelpDeskController.Repository
{
    public class GroupUsers
    {
        private int userid;
        public GroupUsers(int userid)
        {
            this.userid = userid;
        }
        public int USERID
        {
            get { return userid; }
            set { userid = value; }
        }
    }

    public class HDTicketManager : BaseRepository<HD_TICKET>, iHDTicketManager
    {
        DataSet _ds = new DataSet("MetaData");

        public HDTicketManager(string filenamepath)
            : base(new UnitOfWork(DBSource.Live))
        {
            //D:\_KARRI\HUD PROJECTS\CurrentProj1\HUDHealthcarePortal\Content\HelpDesk\HelpDeskMetadata.xml
            //_ds.ReadXml(@"D:\_KARRI\HUD PROJECTS\CurrentProj1\HUDHealthcarePortal\Content\HelpDesk\HelpDeskMetadata.xml");
            _ds.ReadXml(filenamepath);
        }

        public DataTable getPriorityTbl()
        {
            return _ds.Tables["Priority"];
        }
        public DataTable getStatusTbl()
        {
            return _ds.Tables["Status"];
        }
        public DataTable getCateogryTbl()
        {
            return _ds.Tables["Category"];
        }
        public DataTable getSubCategoryTbl()
        {
            return _ds.Tables["SubCategory"];
        }
        public DataTable getOnActionTbl()
        {
            return _ds.Tables["OnAction"];
        }
        public string getStatusById(int id)
        {
            //return _ds.Tables["Status"].Select("Value =" + id)[0][0].ToString();
            return _ds.Tables["Status"].Select("Value =" + id).Count() == 0 ? string.Empty : _ds.Tables["Status"].Select("Value =" + id)[0][0].ToString();
        }
        public string getPriorityById(int id)
        {
            //return _ds.Tables["Priority"].Select("Value =" + id)[0][0].ToString();
            return _ds.Tables["Priority"].Select("Value =" + id).Count() == 0 ? string.Empty : _ds.Tables["Priority"].Select("Value =" + id)[0][0].ToString();
        }
        public string getCategoryById(int id)
        {
            //return _ds.Tables["Category"].Select("Value =" + id)[0][0].ToString();
            return _ds.Tables["Category"].Select("Value ='" + id + "'").Count() == 0 ? string.Empty : _ds.Tables["Category"].Select("Value ='" + id + "'")[0][0].ToString();
        }
        public string getSubCategoryById(int id)
        {
            //return _ds.Tables["SubCategory"].Select("Value =" + id)[0][0].ToString();
            return _ds.Tables["SubCategory"].Select("Value =" + id).Count() == 0 ? string.Empty : _ds.Tables["SubCategory"].Select("Value =" + id)[0][0].ToString();
        }
        public string getOnActionById(int id)
        {
            //return _ds.Tables["OnAction"].Select("Value =" + id)[0][0].ToString();
            return _ds.Tables["OnAction"].Select("Value =" + id).Count() == 0 ? string.Empty : _ds.Tables["OnAction"].Select("Value =" + id)[0][0].ToString();
        }
        public string getHelpDeskUserNameByID(int userid)
        {
            return _ds.Tables["HelpDesk"].Select("Value =" + userid).Count() == 0 ? string.Empty : _ds.Tables["HelpDesk"].Select("Value =" + userid)[0][0].ToString();
        }
        public string getHelpDeskUserIDByUserName(string username)
        {
            //string sortString = string.IsNullOrEmpty(sort) ? "UserName" : sort;
            return _ds.Tables["HelpDesk"].Select("Text =" + username).Count() == 0 ? string.Empty : _ds.Tables["HelpDesk"].Select("Text =" + username)[0][0].ToString();
        }
        public DataTable getHelpDesk1Users()
        {
            return _ds.Tables["HelpDesk"];
        }
        public DataTable getTechDeskUsers()
        {
            return _ds.Tables["TechDesk"];
        }
        //karri-hareesh-13092019
        public DataTable getInternalUsersRoles()
        {
            return _ds.Tables["InternalUserRole"];
        }
        //karri-hareesh-13092019
        public string getInternalUserRolenameById(int id)
        {
            //return _ds.Tables["Category"].Select("Value =" + id)[0][0].ToString();
            return _ds.Tables["InternalUserRole"].Select("Value ='" + id + "'").Count() == 0 ? string.Empty : _ds.Tables["InternalUserRole"].Select("Value ='" + id + "'")[0][0].ToString();
        }
        //karri-hareesh-13092019
        public int getInternalUserIDByRoleName(string id)
        {
            //return _ds.Tables["Category"].Select("Value =" + id)[0][0].ToString();
            return _ds.Tables["InternalUserRole"].Select("Text ='" + id + "'").Count() == 0 ? 0 : Convert.ToInt32(_ds.Tables["InternalUserRole"].Select("Text ='" + id + "'")[0][1].ToString());
        }




        public HDTicketManager(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        //insert ticket details
        //get ticket details on ticket id
        //get user details on userid
        //update ticket on status, assignment, ticket type, file uploads, messaging
        //ticket grid based on user
        //ticket grid based on user for help desk team
        public int saveTicket(HD_TICKET_ViewModel dataInModel)
        {
            //try
            //{
            //Searching from live DB Table with filter from View Model
            //var context = (HCP_live)this.Context;
            //var nonCriticalExtensionRequest = (from n in context.NonCriticalRequestExtension
            //                                   where n.NonCriticalRequestExtensionId == model.NonCriticalRequestExtensionId
            //                                   select n).FirstOrDefault();
            //nonCriticalExtensionRequest.TaskId = model.TaskId;
            //this.Update(nonCriticalExtensionRequest);

            var context = (HCP_live)this.Context;


            HD_TICKET tobj = new HD_TICKET();
            //tobj.TICKET_ID = dataInModel.TICKET_ID;
            tobj.STATUS = dataInModel.STATUS;
            tobj.ISSUE = dataInModel.ISSUE;
            tobj.DESCRIPTION = dataInModel.DESCRIPTION;
            tobj.CATEGORY = dataInModel.CATEGORY;
            tobj.SUBCATEGORY = dataInModel.SUBCATEGORY;
            tobj.PRIORITY = dataInModel.PRIORITY;
            tobj.PRIORITY = dataInModel.PRIORITY;
            tobj.CONVERSATION = dataInModel.USERCOMMENTS;
            tobj.CREATED_BY = dataInModel.CREATED_BY;
            tobj.CREATED_ON = dataInModel.CREATED_ON;
            tobj.ASSIGNED_TO = dataInModel.ASSIGNED_TO;
            tobj.ASSIGNED_DATE = dataInModel.ASSIGNED_DATE;
            tobj.LAST_MODIFIED_BY = dataInModel.LAST_MODIFIED_BY;
            tobj.LAST_MODIFIED_ON = dataInModel.LAST_MODIFIED_ON;
            tobj.RECREATION_STEPS = dataInModel.RECREATION_STEPS;
            tobj.PRIORITY = dataInModel.PRIORITY;
            tobj.ONACTION = dataInModel.ONACTION;
            tobj.ROLENAME = dataInModel.ROLENAME;
            tobj.FHANumber = dataInModel.FHANumber;
            tobj.PropertyId = dataInModel.PropertyId;
            tobj.PropertyName = dataInModel.PropertyName;

            ////for mapping values , for some reason not working, says
            ////Missing type map configuration or unsupported mapping.\r\n\r\nMapping types:\r\n HD_TICKET_ViewModel->HD_TICKET\r\nHUDHealthcarePortal.Model.HD_TICKET_ViewModel->EntityObject.Entities.HCP_live.HD_TICKET\r\n\r\nDestination path:\r\nHD_TICKET\r\n\r\nSource value:\r\nHUDHealthcarePortal.Model.HD_TICKET_ViewModel
            //var tobj = Mapper.Map<HD_TICKET>(dataInModel);

            this.InsertNew(tobj);
            Context.SaveChanges();

            return tobj.TICKET_ID;
            //}
            //catch (Exception ex)
            //{
            //    string a = ex.Message;
            //}
            //return tobj.TICKET_ID;
        }

        public void updateTicket(HD_TICKET_ViewModel dataInModel, bool isStatusChanged, bool isAssigned
                                , bool isReassigned, bool isPriorityChanged, bool isDataChanged, bool isReqFromHelpDesk1 = true)
        {
            var context = (HCP_live)this.Context;

            //main update proces om HUD HelpDesk1
            if (isReqFromHelpDesk1 || !isReqFromHelpDesk1)//there is a purpose
            {
                var updatedata = (from q in context.HD_TICKET
                                  where q.TICKET_ID == dataInModel.TICKET_ID
                                  select new { q }).SingleOrDefault();

                if (isStatusChanged)
                    updatedata.q.STATUS = dataInModel.STATUS;
                if (isAssigned)
                {
                    updatedata.q.ASSIGNED_TO = dataInModel.ASSIGNED_TO;
                    updatedata.q.ASSIGNED_DATE = dataInModel.ASSIGNED_DATE;
                }
                if (isReassigned)
                    updatedata.q.ASSIGNED_TO = dataInModel.ASSIGNED_TO;
                if (isPriorityChanged)
                    updatedata.q.PRIORITY = dataInModel.PRIORITY;
                if (isDataChanged)
                    updatedata.q.LAST_MODIFIED_ON = dataInModel.LAST_MODIFIED_ON;
                if (dataInModel.CONVERSATION != null && !string.IsNullOrEmpty(dataInModel.CONVERSATION))
                    updatedata.q.CONVERSATION = dataInModel.CONVERSATION;

                Context.SaveChanges();

            }
            else
            {
                var updatedata = (from q in context.HD_TICKET
                                  where q.TICKET_ID == dataInModel.TICKET_ID
                                  select new { q }).SingleOrDefault();

                updatedata.q.STATUS = dataInModel.STATUS;
                //updatedata.q.LAST_MODIFIED_ON = DateTime.Now;
                updatedata.q.LAST_MODIFIED_ON = DateTime.UtcNow;

                Context.SaveChanges();
            }

        }

        public static IEnumerable<HD_TICKET_ViewModel> GetHdticket(IEnumerable<EntityObject.Entities.HCP_live.HD_TICKET> hdticket)
        {
            List<HD_TICKET_ViewModel> Hdticketlist = new List<HD_TICKET_ViewModel>();
            foreach (EntityObject.Entities.HCP_live.HD_TICKET b in hdticket)
            {
                HD_TICKET_ViewModel hdTicket = new HD_TICKET_ViewModel();
                hdTicket.TICKET_ID = b.TICKET_ID;
                hdTicket.STATUS = b.STATUS;
                hdTicket.ISSUE = b.ISSUE;
                hdTicket.DESCRIPTION = b.DESCRIPTION;
                hdTicket.CATEGORY = b.CATEGORY;
                hdTicket.SUBCATEGORY = b.SUBCATEGORY;
                hdTicket.PRIORITY = b.PRIORITY;
                hdTicket.RECREATION_STEPS = b.RECREATION_STEPS;
                hdTicket.CONVERSATION = b.CONVERSATION;
                hdTicket.CREATED_BY = b.CREATED_BY;
                hdTicket.CREATED_ON = b.CREATED_ON;
                hdTicket.ASSIGNED_TO = b.ASSIGNED_TO;
                hdTicket.ASSIGNED_DATE = b.ASSIGNED_DATE;
                hdTicket.LAST_MODIFIED_BY = b.LAST_MODIFIED_BY;
                hdTicket.LAST_MODIFIED_ON = b.LAST_MODIFIED_ON;



                Hdticketlist.Add(hdTicket);
            }
            return Hdticketlist.AsEnumerable();
        }


        //to get all ticket details for a single user
        //IEnumerable<HD_TICKET> GetAllTickets();
        public List<HD_TICKET_ViewModel> GetMyTickets(int userid, bool isHudHelpDesk1User = false, bool isHudHelpDesk2User = false, int paramvalue = 0)
        {
            var context = (HCP_live)this.Context;

            var hdtickets = GetAllTickets();

            if (isHudHelpDesk2User)
                hdtickets = hdtickets.Where(q => q.ASSIGNED_TO == paramvalue).ToList();

            //for HelpDesk1, HelpDesk2
            if (userid.Equals(0))
            {
                var unassignedtickets = (from q in hdtickets
                                         join p in context.HCP_Authentication on q.CREATED_BY equals p.UserID
                                         where q.ASSIGNED_TO == null
                                         orderby q.TICKET_ID descending
                                         select new
                                         {

                                             CREATED_BY_USERNAME = p.UserName,
                                             q.TICKET_ID,
                                             q.STATUS,
                                             q.ISSUE,
                                             q.DESCRIPTION,
                                             q.CATEGORY,
                                             q.SUBCATEGORY,
                                             q.PRIORITY,
                                             q.RECREATION_STEPS,
                                             q.CONVERSATION,
                                             q.CREATED_BY,
                                             q.CREATED_ON,
                                             q.ASSIGNED_TO,
                                             q.ASSIGNED_DATE,
                                             q.LAST_MODIFIED_BY,
                                             q.LAST_MODIFIED_ON,
                                             p.FirstName,
                                             p.LastName,
                                             ASSIGNED_TO_USERNAME = string.Empty,
                                             q.ONACTION,
                                             q.ROLENAME,
                                             q.FHANumber,
                                             q.PropertyId,
                                             q.PropertyName
                                         }).ToList();

                var usertickets = (from q in hdtickets
                                   join p in context.HCP_Authentication on q.CREATED_BY equals p.UserID
                                   join r in context.HCP_Authentication on q.ASSIGNED_TO equals r.UserID
                                   orderby q.TICKET_ID descending
                                   select new
                                   {

                                       CREATED_BY_USERNAME = p.UserName,
                                       q.TICKET_ID,
                                       q.STATUS,
                                       q.ISSUE,
                                       q.DESCRIPTION,
                                       q.CATEGORY,
                                       q.SUBCATEGORY,
                                       q.PRIORITY,
                                       q.RECREATION_STEPS,
                                       q.CONVERSATION,
                                       q.CREATED_BY,
                                       q.CREATED_ON,
                                       q.ASSIGNED_TO,
                                       q.ASSIGNED_DATE,
                                       q.LAST_MODIFIED_BY,
                                       q.LAST_MODIFIED_ON,
                                       p.FirstName,
                                       p.LastName,
                                       ASSIGNED_TO_USERNAME = r.UserName,
                                       q.ONACTION,
                                       q.ROLENAME,
                                       q.FHANumber,
                                       q.PropertyId,
                                       q.PropertyName
                                   }).ToList();

                usertickets = usertickets.Union(unassignedtickets).ToList();

                List<HD_TICKET_ViewModel> htvm = usertickets.Select(a => new HD_TICKET_ViewModel
                {
                    TICKET_ID = a.TICKET_ID
                ,
                    STATUS = a.STATUS
                ,
                    ISSUE = a.ISSUE
                ,
                    DESCRIPTION = a.DESCRIPTION
                ,
                    CATEGORY = a.CATEGORY
                ,
                    SUBCATEGORY = a.SUBCATEGORY
                ,
                    PRIORITY = a.PRIORITY
                ,
                    RECREATION_STEPS = a.RECREATION_STEPS
                ,
                    CONVERSATION = a.CONVERSATION
                ,
                    CREATED_BY = a.CREATED_BY
                ,
                    CREATED_ON = a.CREATED_ON
                ,
                    ASSIGNED_TO = a.ASSIGNED_TO
                ,
                    ASSIGNED_DATE = a.ASSIGNED_DATE
                ,
                    LAST_MODIFIED_BY = a.LAST_MODIFIED_BY
                ,
                    LAST_MODIFIED_ON = a.LAST_MODIFIED_ON
                ,
                    CREATED_BY_USERNAME = a.CREATED_BY_USERNAME

                ,
                    CREATED_BY_USERFIRSTNAME = a.FirstName

                ,
                    CREATED_BY_USERLASTNAME = a.LastName

                ,
                    ASSIGNED_TO_USERNAME = a.ASSIGNED_TO_USERNAME
                ,
                    ONACTION = a.ONACTION
                ,
                    ROLENAME = a.ROLENAME
                ,
                    FHANumber = a.FHANumber
                ,
                    PropertyId = a.PropertyId
                ,
                    PropertyName = a.PropertyName

                }).OrderByDescending(m => m.TICKET_ID).ToList();

                //return Mapper.Map<HD_TICKET_ViewModel>(usertickets);
                return htvm;
            }

            //default for a given user
            var useradmintickets = (from q in hdtickets
                                    join p in context.HCP_Authentication on q.CREATED_BY equals p.UserID
                                    where p.UserID == userid//conditon on created by only
                                    orderby q.TICKET_ID descending
                                    select new
                                    {
                                        CREATED_BY_USERNAME = p.UserName,
                                        q.TICKET_ID,
                                        q.STATUS,
                                        q.ISSUE,
                                        q.DESCRIPTION,
                                        q.CATEGORY,
                                        q.SUBCATEGORY,
                                        q.PRIORITY,
                                        q.RECREATION_STEPS,
                                        q.CONVERSATION,
                                        q.CREATED_BY,
                                        q.CREATED_ON,
                                        q.ASSIGNED_TO,
                                        q.ASSIGNED_DATE,
                                        q.LAST_MODIFIED_BY,
                                        q.LAST_MODIFIED_ON,
                                        p.UserName,
                                        p.FirstName,
                                        p.LastName
                                    }).ToList();


            List<HD_TICKET_ViewModel> adminHtvm = useradmintickets.Select(a => new HD_TICKET_ViewModel
            {
                TICKET_ID = a.TICKET_ID
            ,
                STATUS = a.STATUS
            ,
                ISSUE = a.ISSUE
            ,
                DESCRIPTION = a.DESCRIPTION
            ,
                CATEGORY = a.CATEGORY
            ,
                SUBCATEGORY = a.SUBCATEGORY
            ,
                PRIORITY = a.PRIORITY
            ,
                RECREATION_STEPS = a.RECREATION_STEPS
            ,
                CONVERSATION = a.CONVERSATION
            ,
                CREATED_BY = a.CREATED_BY
            ,
                CREATED_ON = a.CREATED_ON
            ,
                ASSIGNED_TO = a.ASSIGNED_TO
            ,
                ASSIGNED_DATE = a.ASSIGNED_DATE
            ,
                LAST_MODIFIED_BY = a.LAST_MODIFIED_BY
            ,
                LAST_MODIFIED_ON = a.LAST_MODIFIED_ON
            ,
                CREATED_BY_USERNAME = a.CREATED_BY_USERNAME
            ,
                CREATED_BY_USERFIRSTNAME = a.FirstName
            ,
                CREATED_BY_USERLASTNAME = a.LastName
            }).ToList();

            //return Mapper.Map<HD_TICKET_ViewModel>(usertickets);
            return adminHtvm;

        }

        //HDUpdates//THIS WILL CALLED ONLY FOR NON-ADMIN, NON-HELP-DESK usrs
        public List<HD_TICKET_ViewModel> GetMyTickets(ArrayList alGroupUsers)
        {
            List<GroupUsers> GroupUsersList = new List<GroupUsers>();
            //GroupUsersList.Add(new GroupUsers(35));
            foreach (int guserid in alGroupUsers)
            {
                GroupUsersList.Add(new GroupUsers(guserid));
            }

            //has no significance excpet to force the filter
            int userid = 0;

            var context = (HCP_live)this.Context;

            var hdtickets = GetAllTickets();

            //for HD Users
            if (userid.Equals(0))
            {
                var usertickets = (from q in hdtickets
                                   join p in context.HCP_Authentication on q.CREATED_BY equals p.UserID
                                   join r in GroupUsersList on q.CREATED_BY equals r.USERID//HUDUpdates
                                   orderby q.TICKET_ID descending
                                   select new
                                   {
                                       CREATED_BY_USERNAME = p.UserName,
                                       q.TICKET_ID,
                                       q.STATUS,
                                       q.ISSUE,
                                       q.DESCRIPTION,
                                       q.CATEGORY,
                                       q.SUBCATEGORY,
                                       q.PRIORITY,
                                       q.RECREATION_STEPS,
                                       q.CONVERSATION,
                                       q.CREATED_BY,
                                       q.CREATED_ON,
                                       q.ASSIGNED_TO,
                                       q.ASSIGNED_DATE,
                                       q.LAST_MODIFIED_BY,
                                       q.LAST_MODIFIED_ON,
                                       p.FirstName,
                                       p.LastName
                                   }).ToList();


                List<HD_TICKET_ViewModel> htvm = usertickets.Select(a => new HD_TICKET_ViewModel
                {
                    TICKET_ID = a.TICKET_ID
                ,
                    STATUS = a.STATUS
                ,
                    ISSUE = a.ISSUE
                ,
                    DESCRIPTION = a.DESCRIPTION
                ,
                    CATEGORY = a.CATEGORY
                ,
                    SUBCATEGORY = a.SUBCATEGORY
                ,
                    PRIORITY = a.PRIORITY
                ,
                    RECREATION_STEPS = a.RECREATION_STEPS
                ,
                    CONVERSATION = a.CONVERSATION
                ,
                    CREATED_BY = a.CREATED_BY
                ,
                    CREATED_ON = a.CREATED_ON
                ,
                    ASSIGNED_TO = a.ASSIGNED_TO
                ,
                    ASSIGNED_DATE = a.ASSIGNED_DATE
                ,
                    LAST_MODIFIED_BY = a.LAST_MODIFIED_BY
                ,
                    LAST_MODIFIED_ON = a.LAST_MODIFIED_ON
                ,
                    CREATED_BY_USERNAME = a.CREATED_BY_USERNAME

                ,
                    CREATED_BY_USERFIRSTNAME = a.FirstName

                ,
                    CREATED_BY_USERLASTNAME = a.LastName
                }).ToList();

                //return Mapper.Map<HD_TICKET_ViewModel>(usertickets);
                return htvm;
            }

            //default for a given user
            var useradmintickets = (from q in hdtickets
                                    join p in context.HCP_Authentication on q.CREATED_BY equals p.UserID
                                    where p.UserID == userid//conditon on created by only
                                    orderby q.TICKET_ID descending
                                    select new
                                    {
                                        CREATED_BY_USERNAME = p.UserName,
                                        q.TICKET_ID,
                                        q.STATUS,
                                        q.ISSUE,
                                        q.DESCRIPTION,
                                        q.CATEGORY,
                                        q.SUBCATEGORY,
                                        q.PRIORITY,
                                        q.RECREATION_STEPS,
                                        q.CONVERSATION,
                                        q.CREATED_BY,
                                        q.CREATED_ON,
                                        q.ASSIGNED_TO,
                                        q.ASSIGNED_DATE,
                                        q.LAST_MODIFIED_BY,
                                        q.LAST_MODIFIED_ON,
                                        p.UserName,
                                        p.FirstName,
                                        p.LastName
                                    }).ToList();


            List<HD_TICKET_ViewModel> adminHtvm = useradmintickets.Select(a => new HD_TICKET_ViewModel
            {
                TICKET_ID = a.TICKET_ID
            ,
                STATUS = a.STATUS
            ,
                ISSUE = a.ISSUE
            ,
                DESCRIPTION = a.DESCRIPTION
            ,
                CATEGORY = a.CATEGORY
            ,
                SUBCATEGORY = a.SUBCATEGORY
            ,
                PRIORITY = a.PRIORITY
            ,
                RECREATION_STEPS = a.RECREATION_STEPS
            ,
                CONVERSATION = a.CONVERSATION
            ,
                CREATED_BY = a.CREATED_BY
            ,
                CREATED_ON = a.CREATED_ON
            ,
                ASSIGNED_TO = a.ASSIGNED_TO
            ,
                ASSIGNED_DATE = a.ASSIGNED_DATE
            ,
                LAST_MODIFIED_BY = a.LAST_MODIFIED_BY
            ,
                LAST_MODIFIED_ON = a.LAST_MODIFIED_ON
            ,
                CREATED_BY_USERNAME = a.CREATED_BY_USERNAME
            ,
                CREATED_BY_USERFIRSTNAME = a.FirstName
            ,
                CREATED_BY_USERLASTNAME = a.LastName
            }).ToList();

            //return Mapper.Map<HD_TICKET_ViewModel>(usertickets);
            return adminHtvm;

        }

        //to get single ticket details for a given user
        public List<HD_TICKET_ViewModel> GetMyTickets(int userid, int ticketid)
        {
            var hdtickets = GetMyTickets(userid);

            var ticketDetail = (from q in hdtickets
                                where q.TICKET_ID == ticketid
                                orderby q.TICKET_ID descending
                                select new { q }).ToList();

            List<HD_TICKET_ViewModel> htvm = ticketDetail.Select(a => new HD_TICKET_ViewModel
            {
                TICKET_ID = a.q.TICKET_ID
            ,
                STATUS = a.q.STATUS
            ,
                ISSUE = a.q.ISSUE
            ,
                DESCRIPTION = a.q.DESCRIPTION
            ,
                CATEGORY = a.q.CATEGORY
            ,
                SUBCATEGORY = a.q.SUBCATEGORY
            ,
                PRIORITY = a.q.PRIORITY
            ,
                RECREATION_STEPS = a.q.RECREATION_STEPS
            ,
                CONVERSATION = a.q.CONVERSATION
            ,
                CREATED_BY = a.q.CREATED_BY
            ,
                CREATED_ON = a.q.CREATED_ON
            ,
                ASSIGNED_TO = a.q.ASSIGNED_TO
            ,
                ASSIGNED_DATE = a.q.ASSIGNED_DATE
            ,
                LAST_MODIFIED_BY = a.q.LAST_MODIFIED_BY
            ,
                LAST_MODIFIED_ON = a.q.LAST_MODIFIED_ON
            ,
                CREATED_BY_USERNAME = a.q.CREATED_BY_USERNAME
            ,
                CREATED_BY_USERFIRSTNAME = a.q.CREATED_BY_USERFIRSTNAME
            ,
                CREATED_BY_USERLASTNAME = a.q.CREATED_BY_USERLASTNAME
            ,
                ASSIGNED_TO_USERNAME = a.q.ASSIGNED_TO_USERNAME
            ,
                ONACTION = a.q.ONACTION
            ,
                ROLENAME = a.q.ROLENAME
            ,
                FHANumber = a.q.FHANumber
            ,
                PropertyId = a.q.PropertyId
            ,
                PropertyName = a.q.PropertyName

            }).ToList();


            //return Mapper.Map<HD_TICKET_ViewModel>(usertickets);
            return htvm;

        }

        //gets all ticket details
        public List<HD_TICKET> GetAllTickets()
        {
            var context = (HCP_live)this.Context;
            //var hdtickets = (from r in context.HD_TICKET
            //                 select new { r}).ToList();

            var hdtickets = context.HD_TICKET.ToList();


            return hdtickets;

        }

        //HELP: https://www.codeproject.com/Articles/1043977/Mapping-ViewModel-to-Model-in-ASP-NET-MVC-using-Im

        //to get single ticket details for a given user
        //if userid=status=0=category=0 than all tickets(for HD users)
        /* commented by hareesh
          public List<HD_TICKET_ViewModel> GetMyTickets(int userid = 0, int status = -1, int category = -1)
          {
              if(userid==0 )
              { 
                 var hdtickets = GetMyTickets(userid);
                  return hdtickets;
              }
              else
              {
                  //if users searches for only status type but not any category
                  if(status != -1 && category == -1)
                  {
                      //this is a strongly typed model list
                      var hdtickets = GetAllTickets();

      //this is ananymous type inside with ...
      var ticketDetail = (from q in hdtickets
                          where q.STATUS == status
                          select new { q }).ToList();

      //MapLocalListToHDTVModel
      List<HD_TICKET_ViewModel> htvm = MapLocalListToHDTVModel(hdtickets);

      var ticketDetailvm = (from q in htvm
                            where q.STATUS == status
                            where q.ASSIGNED_TO == userid
                            select new { q }).ToList();

                      return ticketDetailvm;
                  }
              }


              //List<HD_TICKET_ViewModel> htvm = ticketDetail.Select(a => new HD_TICKET_ViewModel
              //{
              //    TICKET_ID = a.q.TICKET_ID
              //,
              //    STATUS = a.q.STATUS
              //,
              //    ISSUE = a.q.ISSUE
              //,
              //    DESCRIPTION = a.q.DESCRIPTION
              //,
              //    CATEGORY = a.q.CATEGORY
              //,
              //    PRIORITY = a.q.PRIORITY
              //,
              //    RECREATION_STEPS = a.q.RECREATION_STEPS
              //,
              //    CONVERSATION = a.q.CONVERSATION
              //,
              //    CREATED_BY = a.q.CREATED_BY
              //,
              //    CREATED_ON = a.q.CREATED_ON
              //,
              //    ASSIGNED_TO = a.q.ASSIGNED_TO
              //,
              //    ASSIGNED_DATE = a.q.ASSIGNED_DATE
              //,
              //    LAST_MODIFIED_BY = a.q.LAST_MODIFIED_BY
              //,
              //    LAST_MODIFIED_ON = a.q.LAST_MODIFIED_ON
              //,
              //    CREATED_BY_USERNAME = a.q.CREATED_BY_USERNAME  
              //}).ToList();


              ////return Mapper.Map<HD_TICKET_ViewModel>(usertickets);
              //return htvm;

          } */

        private List<HD_TICKET_ViewModel> MapLocalListToHDTVModel(IEnumerable<HD_TICKET> mappingdata)
        {
            List<HD_TICKET_ViewModel> htvm = mappingdata.Select(a => new HD_TICKET_ViewModel
            {
                TICKET_ID = a.TICKET_ID
           ,
                STATUS = a.STATUS
           ,
                ISSUE = a.ISSUE
           ,
                DESCRIPTION = a.DESCRIPTION
           ,
                CATEGORY = a.CATEGORY
           ,
                SUBCATEGORY = a.SUBCATEGORY
           ,
                PRIORITY = a.PRIORITY
           ,
                RECREATION_STEPS = a.RECREATION_STEPS
           ,
                CONVERSATION = a.CONVERSATION
           ,
                CREATED_BY = a.CREATED_BY
           ,
                CREATED_ON = a.CREATED_ON
           ,
                ASSIGNED_TO = a.ASSIGNED_TO
           ,
                ASSIGNED_DATE = a.ASSIGNED_DATE
           ,
                LAST_MODIFIED_BY = a.LAST_MODIFIED_BY
           ,
                LAST_MODIFIED_ON = a.LAST_MODIFIED_ON,

                //CREATED_BY_USERNAME = a.CREATED_BY_USERNAME
            }).ToList();


            //return Mapper.Map<HD_TICKET_ViewModel>(usertickets);
            return htvm;
        }
        //public List<HD_TICKET> GetAllReportsTickets()
        //{
        //    //var context = this.Context as HCP_live;
        //    var context = this.Context as HCP_live;
        //    if (context == null)
        //        throw new InvalidCastException("context is not from db live in GetHelpdeskreporttickets, please pass in correct context in unit of work.");
        //    var results = context.Database.SqlQuerySimple<usp_HCP_GetTicketDetails>("usp_HCP_GetTicketDetails").ToList();
        //    List<HD_TICKET> htvm = results.Select(a => new HD_TICKET
        //    {
        //        TICKET_ID = a.TICKET_ID
        //  ,
        //        STATUS = a.STATUS
        //  ,
        //        ISSUE = a.ISSUE
        //  ,
        //        DESCRIPTION = a.DESCRIPTION
        //  ,
        //        CATEGORY = a.CATEGORY
        //  //,
        //  //     SUBCATEGORY = a.SUBCATEGORY
        //  ,
        //        PRIORITY = a.PRIORITY
        //  ,
        //        RECREATION_STEPS = a.RECREATION_STEPS
        //  ,
        //        CONVERSATION = a.CONVERSATION
        //  ,
        //        CREATED_BY = a.CREATED_BY
        //  ,
        //        CREATED_ON = a.CREATED_ON
        //  ,
        //        ASSIGNED_TO = a.ASSIGNED_TO
        //  ,
        //        ASSIGNED_DATE = a.ASSIGNED_DATE
        //  ,
        //        LAST_MODIFIED_BY = a.LAST_MODIFIED_BY
        //  ,
        //        LAST_MODIFIED_ON = a.LAST_MODIFIED_ON,

        //        //CREATED_BY_USERNAME = a.CREATED_BY_USERNAME
        //    }).ToList();


        //    return htvm;

        //}


        //written by siddesh  29082019 #344//

        public List<usp_HCP_GetTicketDetails_Latest> GetAllReportsTickets(string Status = null)
        {
            //var context = this.Context as HCP_live;
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in  usp_HCP_GetTicketDetails_Latest, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_GetTicketDetails_Latest>(" usp_HCP_GetTicketDetails_Latest", new { }).ToList();
            //  List<HD_TICKET_ViewModel> htvm = results.Select(a => new HD_TICKET_ViewModel
            //  {
            //      TICKET_ID = a.TICKET_ID,
            //      Numberofdays = a.Numberofdays,
            //   Numberofweeks = a.Numberofweeks,
            //        STATUS = a.STATUS
            //,
            //      CREATED_ON = a.CREATED_ON
            //,
            //      LAST_MODIFIED_ON = a.LAST_MODIFIED_ON,
            //  }).ToList();


            return results;

        }
        //siddu #825 @10042020
        public List<DateTime> GetHolidays()
        {
            var context = this.Context as HCP_live;
            List<DateTime> holidays = new List<DateTime>();


            var dateList = (from d in context.Fedralholidaylist

                            select new
                            {
                                d.Date
                            }).ToList();

            if (dateList != null)
            {
                foreach (var date2 in dateList)
                {
                    holidays.Add((date2.Date));
                }
            }
            return holidays;
        }
    }
}