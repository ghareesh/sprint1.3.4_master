﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Repository;
using System.Collections;
using System.Data;
using System.Threading.Tasks;

////HDNOTIFICAITONS
namespace HelpDeskController.Repository
{
    public class HDTicketLoggerMngr : BaseRepository<HD_TICKET_NOTIFICATIONS>
    {
        public HDTicketLoggerMngr()
            : base(new UnitOfWork(DBSource.Live))
        {
           
        }

        public HDTicketLoggerMngr(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        
        public int saveNotifiation(HD_TICKET_NOTIFICATIONS dataInModel)
        {
            var context = (HCP_live)this.Context;

            this.InsertNew(dataInModel);
            Context.SaveChanges();
            
            return dataInModel.NOTIFICATIONID;
          
        }

        // https://expertcodeblog.wordpress.com/2018/01/18/how-to-resolve-error-ienumerable-does-not-contain-a-definition-for-tolistasync/ 
        ////HDNOTIFICATIONS
        public async Task<List<HD_TICKET_NOTIFICATIONS>> getMyUnReadNotifications(int userid)
        {
            var query = await GetAllTickets(userid);

            //List<HD_TICKET_NOTIFICATIONS> data = query.ToList();
            //return data;
            return await Task.FromResult(query.ToList());
        }

        //gets all ticket details
        public async Task<List<HD_TICKET_NOTIFICATIONS>> GetAllTickets(int userid)
        {
            var context = (HCP_live)this.Context;
            //var hdtickets = (from r in context.HD_TICKET
            //                 select new { r}).ToList();

            var hdtickets = context.HD_TICKET_NOTIFICATIONS.Where(m => m.ISREAD == false && m.TARGETUSERID == userid).OrderBy(m => m.LOGDATE).ToList();


            //List<HD_TICKET_NOTIFICATIONS> data = hdtickets.ToList();
            return await Task.FromResult(hdtickets.ToList());

        }
        
        public void updateNotificationStatus(ArrayList notids)
        {
            //List<string> mints = new List<string> { "C", "CC", "D", "M", "O", "P", "S", "W" };
            //var locations = StateMints.Where(p => mints.Contains(p.Mint.ToUpper())).Select(p => p.Location).ToList();
            List<int> notifIDS = new List<int>();// { "C", "CC", "D", "M", "O", "P", "S", "W" };
            foreach(var item in notids)
            {
                notifIDS.Add((int)item);
            }

            var context = (HCP_live)this.Context;
            //context.HD_TICKET_NOTIFICATIONS.Where(p => notifIDS.Contains(p.NOTIFICATIONID))
            //var updatedata = context.HD_TICKET_NOTIFICATIONS.Where(p => notifIDS.Contains(p.NOTIFICATIONID)).Select(new { })

            //var updatedate = (from s in context.HD_TICKET_NOTIFICATIONS
            //                  where notifIDS.Contains(s.NOTIFICATIONID)
            //                  select new { s }
            //                  ).FirstOrDefault();

            var updatedate = context.HD_TICKET_NOTIFICATIONS.Where(m => notifIDS.Contains(m.NOTIFICATIONID)).ToList();
            updatedate.ForEach(a => a.ISREAD = true);            
            Context.SaveChanges();

        }

        public List<HD_TICKET_NOTIFICATIONS> getMyNotifications(int userid ,bool isHelpDesk1)
        {
            var context = (HCP_live)this.Context;

            if (isHelpDesk1 == true)
            {
                var hdtickets = context.HD_TICKET_NOTIFICATIONS.OrderByDescending(m => m.LOGDATE).ToList();
                return hdtickets.ToList();
            }
            else
            {
                var hdtickets = context.HD_TICKET_NOTIFICATIONS.Where(m => m.TARGETUSERID == userid).OrderByDescending(m => m.LOGDATE).ToList();
                return hdtickets.ToList();
            }
        }

    }
        
}