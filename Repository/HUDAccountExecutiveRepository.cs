﻿using System;
using System.Collections.Generic;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using System.Linq;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    /// <summary>
    /// this class is never meant to be called by upper level layers, so don't need to have an interface
    /// </summary>
    public class HUDAccountExecutiveRepository : BaseRepository<HUD_Project_Manager>, IHUDAccountExecutiveRepository
    {
        public HUDAccountExecutiveRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public HUDAccountExecutiveRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public HUD_Project_Manager GetProjectManagerById(int id)
        {
            var accountExec = this.Find(p => p.HUD_Project_Manager_ID == id, "Address").FirstOrDefault();
            return accountExec;
        }

        public IEnumerable<KeyValuePair<int, string>> GetAccountExecutives()
        {
            return this.GetAll().Select(p => new KeyValuePair<int, string>(p.HUD_Project_Manager_ID, p.HUD_Project_Manager_Name)).OrderBy(p => p.Value);
        }

        public UserViewModel GetAccountExecutiveDetail(int id)
        {
            var accountExecutiveDetail = GetProjectManagerById(id);
            return Mapper.Map<UserViewModel>(accountExecutiveDetail);
        }

        /// <summary>
        /// pass the ae ids as a string and get the ae names as a list
        /// </summary>
        /// <param name="aeids"></param>
        /// <returns></returns>
        public IList<string> GetAENamesListByIds(string aeids)
        {
            if (aeids == null) { aeids = string.Empty; }
            List<string> aeTempArray = new List<string>(aeids.Split(",".ToCharArray()));

            int[] aeIntArray = new int[aeTempArray.Count];

            for (int i = 0; i < aeTempArray.Count; i++)
            {
                if (aeTempArray[i] != "")
                {
                    aeIntArray[i] = Convert.ToInt32(aeTempArray[i]);
                }
            }

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetAENamesListByIds, please pass in correct context in unit of work.");

            return (from ae in context.HUD_Project_Manager
                where aeIntArray.Contains(ae.HUD_Project_Manager_ID)
                select ae.HUD_Project_Manager_Name).ToList();
        }
               
       /// <summary>
        ///  get all Account Executives names List
       /// </summary>
       /// <returns></returns>
        public IList<string> GetALLAENamesList()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetALLAENamesList, please pass in correct context in unit of work.");

            return (from ae in context.HUD_Project_Manager
                    select ae.HUD_Project_Manager_Name).ToList();
        }

        public IList<string> GetALLISOUNamesList()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetALLAENamesList, please pass in correct context in unit of work.");

            return (from ae in context.HUD_Project_Manager
                    select ae.HUD_Project_Manager_Name).ToList();
        }
        
    }
}

