﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class UploadDataSummaryRepository : BaseRepository<usp_HCP_Get_ExcelUpload_View_Result>, IUploadDataSummaryRepository
    {
        public UploadDataSummaryRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public UploadDataSummaryRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<ReportExcelUpload_Model> GetDataUploadSummaryByUserId(int iUserId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetDataUploadSummaryByUserId, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_Get_ExcelUpload_View_Result>("usp_HCP_Get_ExcelUpload_View",
                    new {UserID = iUserId}).ToList();
            var mappedResults = Mapper.Map<IEnumerable<usp_HCP_Get_ExcelUpload_View_Result>, IEnumerable<ReportExcelUpload_Model>>(results).ToList();
            mappedResults.ToList().ForEach(p => p.UserId = iUserId);
            return mappedResults;
        }
    }
}

