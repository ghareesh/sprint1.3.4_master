﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.Utilities;
using EntityObject.Entities.HCP_live;
using EntityObject.Entities.HCP_intermediate;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;
using Model;

namespace HUDHealthcarePortal.Repository
{
    public class ManagementReportRepository:BaseRepository<Lender_DataUpload_Live>,IManagementReportRepository
    {
        private ReportModel _reportModel;
        private ILenderFhaRepository _lenderFhaRepository;
        private IProjectInfoRepository _projectInfoRepository;

        public ManagementReportRepository(UnitOfWork unitOfWork, ReportType managementReportType, HUDRole hudRole)
            : base(unitOfWork)
        {
            _lenderFhaRepository = new LenderFhaRepository();
            _projectInfoRepository = new ProjectInfoRepository();
            _reportModel = new ReportModel(managementReportType);
            LoadDataModel(managementReportType, hudRole);
        }

        public IQueryable<Lender_DataUpload_Live> DataToJoin 
        {
            get { return DbQueryRoot; }
        }

        protected void LoadDataModel(ReportType managementReportType, HUDRole hudRole)
        {
            switch (managementReportType)
            {
                case ReportType.HighRiskProjectReport:
                    if (hudRole == HUDRole.SuperUser)
                    {
                        
                    }
                    else if (hudRole == HUDRole.WorkflowManager)
                    {
                        
                    }
                    else if (hudRole == HUDRole.AccountExecutive)
                    {
                       
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public ReportModel ManagementReportDataModel
        {
            get { return _reportModel; }
        }
    }
}
