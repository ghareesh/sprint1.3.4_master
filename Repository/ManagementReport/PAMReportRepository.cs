﻿using System;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using System.Configuration;
using HUDHealthcarePortal.Repository.Interfaces;
using Model.Production;

namespace Repository.ManagementReport
{
    public class PAMReportRepository : BaseRepository<PAMReportViewModel>, IPAMRepository
    {
        private PAMReportModel _plmReportModel;
        private IProjectInfoRepository _projectInfoRepository;
        private UnitOfWork unitOfWorkLive;
        const int TrueInt = 1;
        private IFhaRepository fhaRepository;
        const int FalseInt = 0;

        public PAMReportRepository() : base(new UnitOfWork(DBSource.Live))
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            _projectInfoRepository = new ProjectInfoRepository();
            _plmReportModel = new PAMReportModel(ReportType.ProjectReport);
            fhaRepository = new FhaRepository(unitOfWorkLive);
        }

        public PAMReportRepository(UnitOfWork unitOfWork) : base(unitOfWork) { }

        public PAMReportModel GetPAMReportSummary(string userName, string isouIds, string aeIds, DateTime? fromDate, DateTime? toDate, int wlmId, int aeId, int isouId,
            int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir, string wlmIds
            , string propertyName, string fhaNumber, string assignedTO, int? hasOpened, int? daysOpened)//karri:D#610
        {
           // projectAction = null;
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
            context.Database.CommandTimeout = 600;
            var reportModel = new PAMReportModel(ReportType.PAMReport);
            var results = context.Database.SqlQuerySimple<usp_HCP_GetPAMReportDetail_Result>("usp_HCP_GetPAMReportDetail",
                new
                {

                    AEIdList = aeIds,
                    StartDate = fromDate,
                    EndDate = toDate,
                    Status = status,
                    ProjectAction = projectAction,
                    UserId = UserPrincipal.Current.UserId,
                    UserRole = UserPrincipal.Current.UserRole,
                    ISOUIdList = isouIds
                }).ToList();
            //var e = results.Where(x => x.FHANumber == "075-22128").ToList();
            var res = (dynamic)null;
            string HideFakeFHAs = ConfigurationManager.AppSettings["HideFakeFHAs"];
            var isHideFake = !string.IsNullOrEmpty(HideFakeFHAs) && HideFakeFHAs.ToUpper().Equals("TRUE");
            if (isHideFake)
            {
                string lenderIds = ConfigurationManager.AppSettings["BlockUser"];
                var fhaLenderPairs = context.Database.SqlQuerySimple<usp_HCP_GetFhasByLenderIds_Result>("usp_HCP_GetFhasByLenderIds",
                new { LenderIds = lenderIds }).OrderBy(p => p.FHANumber).ToList();
                var distinctFHANumbers = fhaLenderPairs.Select(m => m.FHANumber).Distinct();
                res = results.Where(m => !distinctFHANumbers.Contains(m.FHANumber)).ToList();
                results = res;
            }
            reportModel.PAMReportGridModel = new PaginateSortModel<PAMReportViewModel>();
            reportModel.PAMReportGridModel.Entities = Mapper.Map<IEnumerable<usp_HCP_GetPAMReportDetail_Result>,
                IEnumerable<PAMReportViewModel>>(results).ToList();
            reportModel.PAMReportGridModel.TotalRows = reportModel.PAMReportGridModel.Entities.Count();
            reportModel.PAMReportGridModel.PageSize = 10;
            reportModel.PAMReportGridlList = reportModel.PAMReportGridModel.Entities.ToList();

            //karri:D#610
            //propertyName = "Greenridge Estates at Mountain Park";
            //fhaNumber = "126-22162";
            //assignedTO = "Tracy Grant";

            bool hasOpenedType = true;
            if (hasOpened != null && hasOpened.Equals(1))
                hasOpenedType = true;
            else
                hasOpenedType = false;

            //daysOpened = 40;
            var custmFilRecs = reportModel.PAMReportGridlList.ToList();

            if (propertyName != null && propertyName.Length > 3 && custmFilRecs.Count > 0)
                custmFilRecs = custmFilRecs.Where(x => x.PropertyName.ToUpper().Contains(propertyName.ToUpper())).ToList();

            if (fhaNumber != null && fhaNumber.Length > 3 && custmFilRecs.Count > 0)
                custmFilRecs = custmFilRecs.Where(x => x.FHANumber.ToUpper() == fhaNumber.ToUpper()).ToList();

            if (assignedTO != null && assignedTO.Length > 3 && custmFilRecs.Count > 0)
                custmFilRecs = custmFilRecs.Where(x => x.ReAssignedTo != null && x.ReAssignedTo.ToUpper().Contains(assignedTO.ToUpper())).ToList();

            if (hasOpened != null && hasOpened >= 0 && custmFilRecs.Count > 0)
                custmFilRecs = custmFilRecs.Where(x => x.IsProjectActionOpen == hasOpenedType).ToList();

            if (daysOpened != null && daysOpened >= 0 && custmFilRecs.Count > 0)
                custmFilRecs = custmFilRecs.Where(x => x.ProjectActionDaysToComplete >= daysOpened).ToList();

            reportModel.PAMReportGridlList = custmFilRecs;

            if (page == 0)
            {
                reportModel.PAMReportGridModel.PageSize = reportModel.PAMReportGridModel.TotalRows;
            }
            reportModel.PAMReportGridModel = PaginateSort.PAMSortAndPaginate(reportModel.PAMReportGridModel.Entities.AsQueryable(), sort,
                sortdir, reportModel.PAMReportGridModel.PageSize, page);
            var wlmRepository = new HUDWorkLoadManagerRepository();
            var aeRepository = new HUDAccountExecutiveRepository();
            var wlmAeRepository = new HUDWorkloadPortfolioManagerRepository();
            if (wlmId > 0)
            {
                reportModel.ReportLevel = ReportLevel.WLMUserLevel;
                reportModel.ReportHeaderModel.HudWorkloadManagerId = wlmId;
                reportModel.ReportHeaderModel.HudWorkloadManagerName =
                    wlmRepository.GetWorkloadMgrById(wlmId).HUD_WorkLoad_Manager_Name;
            }
            if (aeId > 0)
            {
                reportModel.ReportLevel = ReportLevel.AEUserLevel;
                reportModel.ReportHeaderModel.HudProjectManagerId = aeId;
                reportModel.ReportHeaderModel.HudProjectManagerName =
                    aeRepository.GetProjectManagerById(aeId).HUD_Project_Manager_Name;
                wlmId = wlmAeRepository.GetWorkloadManagerIdFromAeId(aeId);
                reportModel.ReportHeaderModel.HudWorkloadManagerId = wlmId;
                if (wlmId > 0) reportModel.ReportHeaderModel.HudWorkloadManagerName =
                    wlmRepository.GetWorkloadMgrById(wlmId).HUD_WorkLoad_Manager_Name;
            }

            if (isouId > 0)
            {
                reportModel.ReportLevel = ReportLevel.ISOUUserLevel;
                reportModel.ReportHeaderModel.HudProjectManagerId = isouId;
                reportModel.ReportHeaderModel.HudProjectManagerName = GetISOUName(isouId);
                // aeRepository.GetProjectManagerById(aeId).HUD_Project_Manager_Name;
                wlmId = GetWLMBYISOU(isouId);

                reportModel.ReportHeaderModel.HudWorkloadManagerId = wlmId;
                if (wlmId > 0) reportModel.ReportHeaderModel.HudWorkloadManagerName =
                    wlmRepository.GetWorkloadMgrById(wlmId).HUD_WorkLoad_Manager_Name;
            }

            /*
            if (UserPrincipal.Current.UserRole == "InternalAccountExecutive")
            {
                reportModel.ReportLevel = ReportLevel.IAEUserLevel;
                LenderPAMReportRepository lr = new LenderPAMReportRepository();
                var userList = (Dictionary<int, string>)lr.GetUserNameByIDs(UserPrincipal.Current.UserId.ToString()).
                    ToDictionary(g => g.Key, g => g.Value);
                reportModel.ReportHeaderModel.HudProjectManagerName = userList[UserPrincipal.Current.UserId];
            }*/
            return reportModel;
        }

        /// get input search criteria details
        public PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string aeIds, string isouIds, string wlmIds, int? status,
            string projectAction, int wlmId, int aeId, int isouId, DateTime? fromDate, DateTime? toDate)
        {
            var wlmRepository = new HUDWorkLoadManagerRepository();
            var aeRepository = new HUDAccountExecutiveRepository();
            var wlmAeRepository = new HUDWorkloadPortfolioManagerRepository();
            reportModel.FromDate = fromDate.HasValue ? String.Format("{0:MM/dd/yyyy}", fromDate.Value) : string.Empty;
            reportModel.ToDate = toDate.HasValue ? String.Format("{0:MM/dd/yyyy}", toDate.Value) : string.Empty;

            // below data used in displaying the search criteria worksheet, in excel export
            if (string.IsNullOrEmpty(aeIds) || (!string.IsNullOrEmpty(aeIds) && !string.IsNullOrEmpty(wlmIds)))
            {
                reportModel.ReportViewName = "HUD ADMINSTRATOR VIEW";
                if (string.IsNullOrEmpty(aeIds))
                {
                    reportModel.SelectedAENameList = aeRepository.GetALLAENamesList().ToList();
                    reportModel.SelectedWLMNameList = wlmRepository.GetALLWLMNamesList().ToList();
                }
                else
                {
                    reportModel.SelectedAENameList = aeRepository.GetAENamesListByIds(aeIds).ToList();
                    reportModel.SelectedWLMNameList = wlmRepository.GetWLMNamesListByIds(wlmIds).ToList();
                }
            }
            // isou display
            if (string.IsNullOrEmpty(isouIds) || (!string.IsNullOrEmpty(isouIds) && !string.IsNullOrEmpty(wlmIds)))
            {
                reportModel.ReportViewName = "HUD ADMINSTRATOR VIEW";
                if (string.IsNullOrEmpty(isouIds))
                {
                    reportModel.SelectedAENameList = aeRepository.GetALLAENamesList().ToList();
                    reportModel.SelectedWLMNameList = wlmRepository.GetALLWLMNamesList().ToList();
                }
                else
                {
                    reportModel.SelectedAENameList = aeRepository.GetAENamesListByIds(aeIds).ToList();
                    reportModel.SelectedWLMNameList = wlmRepository.GetWLMNamesListByIds(wlmIds).ToList();
                }
            }

            if (wlmId > 0)
            {
                reportModel.ReportViewName = "HUD WORKLOAD MANAGER VIEW";
                reportModel.SelectedAENameList = aeRepository.GetAENamesListByIds(aeIds).ToList();
            }
            else if (aeId > 0)
            {
                reportModel.ReportViewName = "HUD ACCOUNT EXECUTIVE VIEW";
            }
            else if (isouId > 0)
            {
                reportModel.ReportViewName = "HUD INTERNAL SPECIAL OPTION USER VIEW";
            }
            reportModel.StatusName = !status.HasValue ? "Open and Closed" : status.Value == 1 ? "Open" : "Closed";
            reportModel.ProjectActionName = projectAction == null ? "R4R and Non-Critical" : projectAction == "1" ? "R4R" : "Non-Critical";
            return reportModel;
        }

        public int GetWlmId(int userId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
            var wlmID = (from u in context.User_WLM_PM
                         where u.UserID == userId
                         select u).FirstOrDefault();
            return wlmID.HUD_WorkLoad_Manager_ID.Value;
        }

        public int GetAeId(int userId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
            var wlmID = (from u in context.User_WLM_PM
                         where u.UserID == userId
                         select u).FirstOrDefault();
            return wlmID.HUD_Project_Manager_ID.Value;
        }

        public int GetIsouId(int userId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
            var wlmID = (from u in context.User_WLM_PM
                         where u.UserID == userId
                         select u).FirstOrDefault();
            return wlmID.HUD_WorkLoad_Manager_ID.Value;
        }

        public int GetWLMBYISOU(int isou)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
            var wlmID = (from u in context.HUD_WorkloadManager_InternalSpecialOptionUser
                         where u.InternalSpecialOptionUserId == isou
                         select u).FirstOrDefault();
            return wlmID.HUD_WorkloadManagerId;

        }

        public string GetISOUName(int userId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");

            var wlmID = (from u in context.HCP_Authentication
                         where u.UserID == userId
                         select u).FirstOrDefault();
            return wlmID.FirstName + " " + wlmID.LastName;
        }

        public IEnumerable<KeyValuePair<int, string>> GetPamProjectActionTypes()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPamProjectActionTypes, please pass in correct context in unit of work.");
            var query = (from p in context.PamProjectActionType
                         select p).ToList();
            IEnumerable<KeyValuePair<int, string>> result = query.Select(p => new KeyValuePair<int, string>(p.PamProjectActionId, p.Name)).OrderBy(p => p.Key);
            return result;
        }
    }
}
