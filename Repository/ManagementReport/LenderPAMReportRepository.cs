﻿using System;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System.Collections.Generic;
using AutoMapper;

// User Story 1904
namespace Repository.ManagementReport
{
    public class LenderPAMReportRepository : BaseRepository<PAMReportViewModel>, ILenderPAMRepository
    {
        private PAMReportModel _plmReportModel;
        private IProjectInfoRepository _projectInfoRepository;
        private IPAMRepository _iPamRepository;
        const int TrueInt = 1;
        const int FalseInt = 0;

        public LenderPAMReportRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
            _iPamRepository = new PAMReportRepository();
            _projectInfoRepository = new ProjectInfoRepository();
            _plmReportModel = new PAMReportModel(ReportType.ProjectReport);
        }

        public LenderPAMReportRepository(UnitOfWork unitOfWork) : base(unitOfWork) { }

        public PAMReportModel GetLenderPAMReportSummary(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate,
            DateTime? toDate, int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir)
        {
            var context = this.Context as HCP_live;

            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPLMReportSummary, please pass in correct context in unit of work.");
            context.Database.CommandTimeout = 600;
            var reportModel = new PAMReportModel(ReportType.PAMReport);

            //Naveen 24-10-2019 added if cond
            //if (!UserPrincipal.Current.UserRole.Contains("Servicer")) 
            //{
            //    var results = context.Database.SqlQuerySimple<usp_HCP_GetPAMReportDetail_Result>("usp_HCP_GetLenderPAMReportDetail",
            //   new
            //   {
            //       RoleList = roleIds,
            //       LAMList = lamIds,
            //       BAMList = bamIds,
            //       LARList = larIds,
            //       StartDate = fromDate,
            //       EndDate = toDate,
            //       Status = status,
            //       ProjectAction = projectAction,
            //       UserId = UserPrincipal.Current.UserId,
            //       UserLenderId = UserPrincipal.Current.LenderId
            //   }).ToList();

            //    reportModel.PAMReportGridModel = new PaginateSortModel<PAMReportViewModel>();
            //    reportModel.PAMReportGridModel.Entities = Mapper.Map<IEnumerable<usp_HCP_GetPAMReportDetail_Result>, IEnumerable<PAMReportViewModel>>(results).ToList();
            //    reportModel.PAMReportGridModel.TotalRows = reportModel.PAMReportGridModel.Entities.Count();
            //    reportModel.PAMReportGridModel.PageSize = 10;
            //    reportModel.PAMReportGridlList = reportModel.PAMReportGridModel.Entities.ToList();
            //}
            //else
            //{

            //    var results = context.Database.SqlQuerySimple<usp_HCP_GetPAMReportDetail_Result>("usp_HCP_GetLenderPAMReportDetail_1",
            //                new
            //                {
            //                    RoleList = roleIds,
            //                    LAMList = lamIds,
            //                    BAMList = bamIds,
            //                    LARList = larIds,
            //                    StartDate = fromDate,
            //                    EndDate = toDate,
            //                    Status = status,
            //                    ProjectAction = projectAction,
            //                    UserId = UserPrincipal.Current.UserId,
            //                    UserLenderId = UserPrincipal.Current.LenderId,
            //                    LoggedRoleName = "Servicer",
            //                }).ToList();

            //    reportModel.PAMReportGridModel = new PaginateSortModel<PAMReportViewModel>();
            //    reportModel.PAMReportGridModel.Entities = Mapper.Map<IEnumerable<usp_HCP_GetPAMReportDetail_Result>, IEnumerable<PAMReportViewModel>>(results).ToList();
            //    reportModel.PAMReportGridModel.TotalRows = reportModel.PAMReportGridModel.Entities.Count();
            //    reportModel.PAMReportGridModel.PageSize = 10;
            //    reportModel.PAMReportGridlList = reportModel.PAMReportGridModel.Entities.ToList();
            //}

            var results = context.Database.SqlQuerySimple<usp_HCP_GetPAMReportDetail_Result>("usp_HCP_GetLenderPAMReportDetail_new",
               new
               {
                   RoleList = roleIds,
                   LAMList = lamIds,
                   BAMList = bamIds,
                   LARList = larIds,
                   StartDate = fromDate,
                   EndDate = toDate,
                   Status = status,
                   ProjectAction = projectAction,
                   UserId = UserPrincipal.Current.UserId,
                   UserLenderId = UserPrincipal.Current.LenderId,
                   rolename = UserPrincipal.Current.UserRole
               }).ToList();

            reportModel.PAMReportGridModel = new PaginateSortModel<PAMReportViewModel>();
            reportModel.PAMReportGridModel.Entities = Mapper.Map<IEnumerable<usp_HCP_GetPAMReportDetail_Result>, IEnumerable<PAMReportViewModel>>(results).ToList();
            reportModel.PAMReportGridModel.TotalRows = reportModel.PAMReportGridModel.Entities.Count();
            reportModel.PAMReportGridModel.PageSize = 10;
            reportModel.PAMReportGridlList = reportModel.PAMReportGridModel.Entities.ToList();



            if (page == 0)
            {
                reportModel.PAMReportGridModel.PageSize = reportModel.PAMReportGridModel.TotalRows;
            }
            reportModel.PAMReportGridModel = PaginateSort.PAMSortAndPaginate(reportModel.PAMReportGridModel.Entities.AsQueryable(), sort, sortdir, reportModel.PAMReportGridModel.PageSize, page);

            var wlmRepository = new HUDWorkLoadManagerRepository();
            var aeRepository = new HUDAccountExecutiveRepository();
            var wlmAeRepository = new HUDWorkloadPortfolioManagerRepository();

            return reportModel;
        }


        public IEnumerable<UserInfoModel> GetLenderUsersByRoleLenderId(int lenderId, string roleName)
        {
            var currentUserID = UserPrincipal.Current.UserId;
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetLenderUsersByLenderId, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetLenderUsersByLenderId_Result>(
                    "usp_HCP_GetLenderUsersByRoleLenderId",
                    new
                    {
                        LenderId = lenderId,
                        RoleName = roleName
                    });

            results = results.Where(u => u.UserID != currentUserID).ToList();

            return Mapper.Map<IEnumerable<UserInfoModel>>(results);
        }

        public PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string lamIds, string bamIds, string larIds, string roleIds, int? status,
          string projectAction, DateTime? fromDate, DateTime? toDate, string userRole, int lenderId)
        {
            reportModel.FromDate = fromDate.HasValue ? String.Format("{0:MM/dd/yyyy}", fromDate.Value) : string.Empty;
            reportModel.ToDate = toDate.HasValue ? String.Format("{0:MM/dd/yyyy}", toDate.Value) : string.Empty;
            reportModel.StatusName = !status.HasValue ? "Open and Closed" : status.Value == 1 ? "Open" : "Closed";

            if (string.IsNullOrEmpty(projectAction))
            {
                var pamprojectActionTypeDict = (Dictionary<int, string>)_iPamRepository.GetPamProjectActionTypes().ToDictionary(g => g.Key, g => g.Value);
                reportModel.SelectedProjectActionList = pamprojectActionTypeDict.Values.ToList();
            }
            else
            {
                var pamprojectActionTypeDict = (Dictionary<int, string>)GetPamProjectActionTypesByIDs(projectAction).ToDictionary(g => g.Key, g => g.Value);
                reportModel.SelectedProjectActionList = pamprojectActionTypeDict.Values.ToList();
            }

            if (userRole.Equals(HUDRole.LenderAccountRepresentative.ToString("g")))
            {
                reportModel.ReportViewName = "Lender Account Representative View";
            }
            else
            {
                if (userRole.Equals(HUDRole.LenderAccountManager.ToString("g")))
                {
                    reportModel.ReportViewName = "Lender Account Manager View";
                }
                else if (userRole.Equals(HUDRole.BackupAccountManager.ToString("g")))
                {
                    reportModel.ReportViewName = "Backup Account Manager View";
                }

                if (string.IsNullOrEmpty(lamIds))
                {

                    var lamList = (Dictionary<int, string>)GetLenderUsersByRoleLenderId(lenderId, HUDRole.LenderAccountManager.ToString("g")).ToDictionary(g => g.UserID, g => g.FirstName + g.LastName);
                    reportModel.SelectedLAMNameList = lamList.Values.ToList();
                }
                else
                {
                    var userList = (Dictionary<int, string>)GetUserNameByIDs(lamIds).ToDictionary(g => g.Key, g => g.Value);
                    reportModel.SelectedLAMNameList = userList.Values.ToList();

                }
                if (string.IsNullOrEmpty(bamIds))
                {
                    var bamList = (Dictionary<int, string>)GetLenderUsersByRoleLenderId(lenderId, HUDRole.BackupAccountManager.ToString("g")).ToDictionary(g => g.UserID, g => g.FirstName + g.LastName);
                    reportModel.SelectedBAMNameList = bamList.Values.ToList();
                }
                else
                {
                    var userList = (Dictionary<int, string>)GetUserNameByIDs(bamIds).ToDictionary(g => g.Key, g => g.Value);
                    reportModel.SelectedBAMNameList = userList.Values.ToList();
                }
                if (string.IsNullOrEmpty(larIds))
                {
                    var larList = (Dictionary<int, string>)GetLenderUsersByRoleLenderId(lenderId, HUDRole.LenderAccountRepresentative.ToString("g")).ToDictionary(g => g.UserID, g => g.FirstName + g.LastName);
                    reportModel.SelectedLARNameList = larList.Values.ToList();
                }
                else
                {
                    var userList = (Dictionary<int, string>)GetUserNameByIDs(larIds).ToDictionary(g => g.Key, g => g.Value);
                    reportModel.SelectedLARNameList = userList.Values.ToList();

                }
                if (string.IsNullOrEmpty(roleIds))
                {
                    var roleList = new List<string>();

                    roleList.Add("Self");
                    roleList.Add("LAM");
                    roleList.Add("BAM");
                    roleList.Add("LAR");
                    reportModel.SelectedRolesList = roleList;
                }
                else
                {
                    var roleList = new List<string>();
                    if (roleIds.Contains("0"))
                    {
                        roleList.Add("Self");
                    }
                    if (roleIds.Contains("1"))
                    {
                        roleList.Add("LAM");
                    }
                    if (roleIds.Contains("2"))
                    {
                        roleList.Add("BAM");
                    }
                    if (roleIds.Contains("3"))
                    {
                        roleList.Add("LAR");
                    }
                    reportModel.SelectedRolesList = roleList;
                }
            }
            return reportModel;
        }

        public IEnumerable<KeyValuePair<int, string>> GetPamProjectActionTypesByIDs(string projectActionList)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPamProjectActionTypes, please pass in correct context in unit of work.");
            List<int> projectActionIds = null;
            projectActionIds = projectActionList.Split(',').Select(int.Parse).ToList();
            var query = (from p in context.PamProjectActionType
                         where projectActionIds.Contains(p.PamProjectActionId)
                         select p).ToList();
            IEnumerable<KeyValuePair<int, string>> result = query.Select(p => new KeyValuePair<int, string>
                (p.PamProjectActionId, p.Name)).OrderBy(p => p.Value);
            return result;
        }

        public IEnumerable<KeyValuePair<int, string>> GetUserNameByIDs(string userIDList)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetPamProjectActionTypes, please pass in correct context in unit of work.");
            List<int> userIds = null;
            userIds = userIDList.Split(',').Select(int.Parse).ToList();
            var query = (from p in context.HCP_Authentication
                         where userIds.Contains(p.UserID)
                         select p).ToList();
            IEnumerable<KeyValuePair<int, string>> result = query.Select(p => new KeyValuePair<int, string>
                (p.UserID, p.FirstName + " " + p.LastName)).OrderBy(p => p.Value);
            return result;
        }
    }
}
