﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces;

namespace Repository
{
    public class NonCriticalLateAlertsRepository : BaseRepository<NonCriticalLateAlerts>, INonCriticalLateAlertsRepository
    {
        public NonCriticalLateAlertsRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public NonCriticalLateAlertsRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<NonCriticalLateAlerts> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public void SaveNonCriticalLateAlerts(IList<NonCriticalLateAlertsModel> models)
        {
            foreach (var model in models)
            {
                var nonCriticalLateAlerts = Mapper.Map<NonCriticalLateAlerts>(model);
                nonCriticalLateAlerts.CreatedOn = DateTime.UtcNow;
                this.InsertNew(nonCriticalLateAlerts);
                UnitOfWork.Save();
            }
        }
    }
}