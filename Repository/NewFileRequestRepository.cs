﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class NewFileRequestRepository : BaseRepository<NewFileRequest>, INewFileRequestRepository
    {
        public NewFileRequestRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public NewFileRequestRepository() : base(new UnitOfWork(DBSource.Task)) { }

        public void SaveNewFileRequest(NewFileRequestModel model)
        {
           
            var newFileRequest = Mapper.Map<NewFileRequest>(model);
            if (newFileRequest != null)
            {
                this.InsertNew(newFileRequest);
                this.Context.SaveChanges();
            }
        }

        public bool IsNewFileRequest(Guid childTaskInstanceId)
        {
            var result = this.Find(p => p.ChildTaskInstanceId == childTaskInstanceId).FirstOrDefault();
            return result != null;
        }

        public NewFileRequestModel GetNewFileRequest(Guid childTaskInstanceId)
        {
            var context = Context as HCP_task;
            if (context != null)
            {
                var result =
                    (from n in context.NewFileRequest where n.ChildTaskInstanceId == childTaskInstanceId select n)
                        .FirstOrDefault();
                return Mapper.Map<NewFileRequestModel>(result);
            }
            return null;
        }
    }
}
