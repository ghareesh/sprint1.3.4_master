﻿using System.Data.Entity.Core.Objects;
using System.Linq;
using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using Model.Production;
using Repository;
using Repository.Interfaces;
using TaskDB = EntityObject.Entities.HCP_task;

namespace HUDHealthcarePortal.Repository
{
    public class TaskRepository : BaseRepository<TaskDB.Task>, ITaskRepository
    {
        public TaskRepository() : base(new UnitOfWork(DBSource.Task)) { }

        public TaskRepository(UnitOfWork unitOfWork) : base(unitOfWork) { }

        public IEnumerable<TaskModel> GetTasksByUserName(string userName)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetTasksByUserName, please pass in correct context in unit of work.");
            var enumeratedList = Mapper.Map<IEnumerable<TaskModel>>(context.Database.SqlQuerySimple<TaskDB.usp_HCP_GetTasksByUserName_Result>("usp_HCP_GetTasksByUserName",
                new { UserName = userName })).ToList();

            foreach (var taskModel in enumeratedList)
            {
                var mappedResult =
                    Mapper.Map<IEnumerable<TaskModel>>(context.Database.SqlQuerySimple<TaskDB.usp_HCP_GetCompletedAndWaitingStartTime_Result>("usp_HCP_GetCompletedAndWaitingStartTime",
                    new { taskModel.TaskInstanceId })).ToList();
                if (mappedResult.Count > 0)
                {
                    taskModel.CompletedStartTime = mappedResult[0].CompletedStartTime;
                    taskModel.WaitingStartTime = mappedResult[0].WaitingStartTime;
                }
            }

            return enumeratedList;
        }

        public Guid AddTask(TaskModel model)
        {
            var task = Mapper.Map<TaskDB.Task>(model);
            this.InsertNew(task);
            return task.TaskInstanceId;
        }

        public TaskModel GetTaskById(int taskId)
        {
            var task = this.GetByIDFast(taskId);
            var taskModel = Mapper.Map<TaskModel>(task);
            return taskModel;
        }

        /// <summary>
        /// returns the lastest task id, with latest status
        /// </summary>
        /// <param name="taskInstanceId"></param>
        /// <returns></returns>
        public TaskModel GetLatestTaskByTaskInstanceId(Guid taskInstanceId)
        {
            var task = this.Find(p => p.TaskInstanceId == taskInstanceId).OrderByDescending(p => p.StartTime);
            if (task.Any())
            {
                return Mapper.Map<TaskModel>(task.First());
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<TaskModel> GetTasksByTaskInstanceId(Guid taskInstanceId)
        {
            var tasks = this.Find(p => p.TaskInstanceId == taskInstanceId).OrderByDescending(p => p.SequenceId);
            return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        }

        public List<int> GetTaskIds(List<string> instanceIds)
        {
            List<Guid> list = new List<Guid>();
            foreach (string s in instanceIds)
            {
                Guid g = new Guid(s);
                list.Add(g);
            }
            Guid[] gs = list.ToArray<Guid>();
            var context = this.Context as TaskDB.HCP_task;
            var allTaskIDs = (from r in context.Tasks
                              where gs.Contains(r.TaskInstanceId)
                              select r.TaskId).ToList<int>();
            return allTaskIDs;
        }

        public List<string> GetFHANumbersByInstanceIds(List<string> instanceIds)
        {
            List<Guid> list = new List<Guid>();
            foreach (string s in instanceIds)
            {
                Guid g = new Guid(s);
                list.Add(g);
            }
            Guid[] gs = list.ToArray<Guid>();
            var context = this.Context as TaskDB.HCP_task;
            var fhas = (from r in context.Tasks
                        where gs.Contains(r.TaskInstanceId)
                        select r.FhaNumber).ToList<string>();
            return fhas;
        }

        public void UpdateTaskNotes(int taskid, string HudRemarks)
        {
            var context = this.Context as TaskDB.HCP_task;
            var task = (from r in context.Tasks
                        where r.TaskId == taskid
                        select r).FirstOrDefault();
            if (task != null)
            {
                task.Notes = HudRemarks;
                this.Update(task);
                UnitOfWork.Save();
            }
        }

        public void UpdateDataStore1(int taskid, string dataStoreXML)
        {
            var context = this.Context as TaskDB.HCP_task;
            var task = (from r in context.Tasks
                        where r.TaskId == taskid
                        select r).FirstOrDefault();
            if (task != null)
            {
                task.DataStore1 = dataStoreXML;
                this.Update(task);
                UnitOfWork.Save();
            }
        }

        //public IEnumerable<TaskModel> GetTasksByTaskInstanceId(Guid taskInstanceId)
        //{
        //    var tasks = this.Find(p => p.TaskInstanceId == taskInstanceId).OrderByDescending(p => p.SequenceId);
        //    return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        //}

        public void UpdateTask(TaskModel task)
        {
            var context = this.Context as TaskDB.HCP_task;

            var entityToUpdate = this.Find(p => p.TaskId == task.TaskId).FirstOrDefault();
            entityToUpdate.TaskStepId = task.TaskStepId;
            if (entityToUpdate != null)
                entityToUpdate.TaskOpenStatus = task.TaskOpenStatus;
            this.Update(entityToUpdate);
            context.SaveChanges();
        }

        public void UpdateTaskReassigned(IList<Guid> taskIdList)
        {
            if (taskIdList != null)
            {
                foreach (var taskId in taskIdList)
                {
                    Guid id = taskId;
                    var task = this.Find(p => p.TaskInstanceId == id).OrderByDescending(x => x.SequenceId).First();
                    if (task != null) task.IsReAssigned = true;
                }
            }
        }
        public void SaveReviewFileStatus(Guid taskInstanceID, int reviewerUserId, int userId, int? ReviewerProdViewId = null)
        {


            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in InsertReviewerTaskFileData, please pass in correct context in unit of work.");
            var results =
                context.Database.ExecuteSqlCommandSimple(
                    "usp_HCP_InsertReviewerTaskFileData",
                    new
                    {
                        TaskInstanceId = taskInstanceID,
                        ReviewerUserId = reviewerUserId,
                        UserId = userId,
                        ReviewerProdViewId = ReviewerProdViewId
                    });

        }

        public int GetReviewerUserIdByTaskInstanceId(Guid taskInstanceId)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetReviewerUserIdByTaskInstanceId, please pass in correct context in unit of work.");

            var results =
            context.Database.SqlQuerySimple<TaskDB.usp_HCP_GetReviewerUserIdByTaskInstanceIdResult>(
                " usp_HCP_GetReviewerUserIdByTaskInstanceId",
                new
                {
                    TaskInstanceId = taskInstanceId
                }).FirstOrDefault();
            return results.UserId;
        }

        //karri#434,hareesh
        public IEnumerable<TaskModel> GetCompletedTasks()
        {
            //// 435hareesh added m.TaskStepId >= (int)ProductionFhaRequestStatus.InQueue instead of m.TaskStepId > (int)ProductionFhaRequestStatus.InQueue 
            //var tasks = this.Find(m => m.PageTypeId >= (int)PageType.FhaRequest &&
            //                     (m.TaskStepId == (int)ProductionAppProcessStatus.ProjectActionRequestComplete || m.TaskStepId > (int)ProductionFhaRequestStatus.InQueue)).OrderByDescending(m => m.TaskId);

            var context = this.Context as TaskDB.HCP_task;
            var tasks =
            context.Database.SqlQuerySimple<TaskModel>(
                "USP_GetProdQue_Completed");

            return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        }

        //karri#434,hareesh
        public IEnumerable<TaskModel> GetCompletedTasksByType(int type)
        {
            //// 435hareesh added m.TaskStepId >= (int)ProductionFhaRequestStatus.InQueue instead of m.TaskStepId > (int)ProductionFhaRequestStatus.InQueue 
            //var tasks = this.Find(m => m.PageTypeId >= (int)PageType.FhaRequest &&
            //                     (m.TaskStepId == (int)ProductionAppProcessStatus.ProjectActionRequestComplete || m.TaskStepId > (int)ProductionFhaRequestStatus.InQueue)).OrderByDescending(m => m.TaskId);

            var context = this.Context as TaskDB.HCP_task;
            var tasks =
            context.Database.SqlQuerySimple<TaskModel>("USP_GetProdQue_Completed_By_Type", new
            {
                pagetypeid = type
            }).ToList();

            return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        }

        public IEnumerable<TaskModel> GetProductionTasks()
        {
            //// 435hareesh added m.TaskStepId >= (int)ProductionFhaRequestStatus.InQueue instead of m.TaskStepId > (int)ProductionFhaRequestStatus.InQueue 
            //var tasks = this.Find(m => m.PageTypeId >= (int)PageType.FhaRequest &&
            //                     (m.TaskStepId == (int)ProductionAppProcessStatus.ProjectActionRequestComplete || m.TaskStepId > (int)ProductionFhaRequestStatus.InQueue)).OrderByDescending(m => m.TaskId);

            var context = this.Context as TaskDB.HCP_task;
            var tasks =
            context.Database.SqlQuerySimple<TaskModel>(
                "USP_GetProdQue_Assigned");

            return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        }

        public IEnumerable<TaskModel> GetProductionQueues()
        {
            //TODO:this will be modified once FHA Request Completed 
            //var tasks = this.Find(m => m.TaskStepId == (int)ProductionFhaRequestStatus.InQueue
            //                  && m.PageTypeId >= (int)PageType.FhaRequest
            //                   ).OrderByDescending(m => m.TaskId);

            // hareesh added below code Sp[GetAlltasks]

            var context = this.Context as TaskDB.HCP_task;
            var tasks =
            context.Database.SqlQuerySimple<TaskModel>(
                "GetAlltasks");

            //Naveen Hareesh Added below code
            //var context = this.Context as TaskDB.HCP_task;
            //var tasks =
            //context.Database.SqlQuerySimple<TaskModel>(
            //    "Gettasks");
            return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        }

        public IEnumerable<TaskModel> GetProductionTaskByType(int type)
        {
            // 435hareesh added m.TaskStepId >= (int)ProductionFhaRequestStatus.InQueue instead of m.TaskStepId > (int)ProductionFhaRequestStatus.InQueue 
            //  var tasks = this.Find(m => m.PageTypeId == type && (m.TaskStepId >= (int)ProductionFhaRequestStatus.InQueue
            //  || m.TaskStepId == (int)ProductionAppProcessStatus.ProjectActionRequestComplete)).OrderByDescending(m => m.TaskId);
            var context = this.Context as TaskDB.HCP_task;
            var tasks =
             context.Database.SqlQuerySimple<TaskModel>("USP_GetProdQue_Assigned_By_Type", new
             {
                 pagetypeid = type
             }).ToList();
            return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        }
        public IEnumerable<TaskModel> GetProductionQueueByType(int type)
        {
            //var tasks = this.Find(m => m.PageTypeId == type && m.TaskStepId == (int)ProductionFhaRequestStatus.InQueue).OrderByDescending(m => m.TaskId);

            // harish added new sp for getting all recoreds based on type
            var context = this.Context as TaskDB.HCP_task;
            var tasks =
             context.Database.SqlQuerySimple<TaskModel>("GetAlltasks_By_Type", new
             {
                 pagetypeid = type
             }).ToList();
            //Gettasksbypagetypeid
            // commented by hareesh below line 24-11-2019
            //var context = this.Context as TaskDB.HCP_task;
            //var tasks =
            //context.Database.SqlQuerySimple<TaskModel>("Gettasksbypagetype", new
            //{
            //    pagetype = type
            //}).ToList();
            return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        }
        public IEnumerable<TaskModel> GetProductionTaskByStatus(int status)
        {
            var tasks = this.Find(m => m.TaskStepId == status && m.TaskStepId != (int)ProductionFhaRequestStatus.InQueue
                && (m.PageTypeId >= (int)PageType.FhaRequest)).OrderByDescending(m => m.TaskId);
            return Mapper.Map<IEnumerable<TaskModel>>(tasks);
        }

        public IEnumerable<TaskModel> GetProductionTaskByTypeAndStatus(int type, int status)
        {
            //TODO:production application status must included here
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db task in GetProductionTaskByTypeAndStatus, please pass in correct context in unit of work.");

            if (type == (int)PageType.Form290)
            {
                var tasks = from t in context.Tasks
                            where t.PageTypeId == type
                            group t by new
                            {
                                t.TaskInstanceId
                            } into gt
                            select gt.FirstOrDefault(result => result.TaskStepId == gt.Max(sub => sub.TaskStepId));               
                            return Mapper.Map<IEnumerable<TaskModel>>(tasks.Where(p => p.TaskStepId == status));
            }
            else
            {
                var tasks = this.Find(m => m.PageTypeId == type && m.TaskStepId == status &&
                                       m.TaskStepId != (int)ProductionFhaRequestStatus.InQueue).OrderByDescending(m => m.TaskId);
                return Mapper.Map<IEnumerable<TaskModel>>(tasks);
            }

        }

        /// <summary>
        /// This proc is used to create FHA request tasks, Insert(parent), portfolio and creditreview
        /// </summary>
        /// <param name="assignedBy"></param>
        /// <param name="taskInstanceID"></param>
        /// <param name="userId"></param>
        /// <param name="userRoleName"></param>
        /// <param name="isPortfolioRequired"></param>
        /// <param name="isCreditReviewRequired"></param>
        /// <returns></returns>
        public int CreateFHARequestTasks(string assignedBy, Guid? taskInstanceID, int userId, string userRoleName, bool isPortfolioRequired, bool isCreditReviewRequired)
        {

            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in USP_Prod_CreateFHARequestTaskAndXref, please pass in correct context in unit of work.");
            var returnCode =
                context.Database.ExecuteSqlCommandSimple(
                    "USP_Prod_CreateFHARequestTaskAndXref",
                    new
                    {
                        Assignedby = assignedBy,
                        TaskinstanceId = taskInstanceID,
                        UserId = userId,
                        UserRole = userRoleName,
                        IsPortfolioRequired = isPortfolioRequired,
                        IsCreditReviewRequired = isCreditReviewRequired,
                    });
            if (returnCode != -1)
            {
                throw new Exception("Error occured on executing the proc USP_Prod_CreateFHARequestTaskAndXref");
            }
            else
            {
                return returnCode;
            }
        }

        public bool AssignProductionFhaInsert(ProductionTaskAssignmentModel model)
        {
            
            var taskFhaInsert = this.Find(m => m.TaskInstanceId == model.TaskInstanceId).FirstOrDefault();
            if (taskFhaInsert != null)
            {
                if (taskFhaInsert.AssignedTo == "Queue" || taskFhaInsert.AssignedTo == null)
                {
                    taskFhaInsert.AssignedTo = model.UserName;
                    //karri,hareesh, #4345
                    //if (model.IsChildAssigned)
                    {
                        taskFhaInsert.TaskStepId = (int)ProductionFhaRequestStatus.InProcess;
                    }
                    this.Update(taskFhaInsert);
                    UnitOfWork.Save();
                   
                    
                    
                    return true;
                }

            }
            return false;
        }
        
        public void UpdateTaskStatus(TaskModel task)
        {
            var entityToUpdate = this.Find(p => p.TaskId == task.TaskId).FirstOrDefault();
            if (entityToUpdate != null)
                entityToUpdate.TaskStepId = task.TaskStepId;
            this.Update(entityToUpdate);
            UnitOfWork.Save();
        }

        #region Production Application
        public Prod_TaskXrefModel GetReviewerViewIdByXrefTaskInstanceId(Guid xrefTaskInstanceId)
        {
            try
            {
                var context = this.Context as TaskDB.HCP_task;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in GetReviewerUserIdByTaskInstanceId, please pass in correct context in unit of work.");
                var result = (from r in context.ProdTaskXrefs
                              where r.TaskXrefid == xrefTaskInstanceId
                              select r).ToList();


                if (result.Any())
                {
                    return Mapper.Map<Prod_TaskXrefModel>(result.First());
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exp)
            {
                return null;
            }


        }
        public int CheckAmendmentExist(string selectedFhaNumber, int PageTypeId, int ViewId)
        {
            int result = 0;
            try
            {
                var context = this.Context as TaskDB.HCP_task;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in GetReviewerUserIdByTaskInstanceId, please pass in correct context in unit of work.");
                 result = (from r in context.Tasks
                              where r.FhaNumber == selectedFhaNumber 
                              && r.PageTypeId == PageTypeId
                              && r.AssignedTo == null
                              select r).Count();                
            }
            catch (Exception exp)
            {
                
            }
            return result;
        }
        public TaskModel GetLatestTaskByTaskXrefid(Guid taskXrefId)
        {

            var context = this.Context as TaskDB.HCP_task;
            var task = (from tsk in context.Tasks

                        join xref in context.ProdTaskXrefs on tsk.TaskInstanceId equals xref.TaskInstanceId
                        where xref.TaskXrefid == taskXrefId
                        select tsk).ToList().OrderByDescending(p => p.StartTime);

            //var task = temp.Find(p => p.TaskInstanceId == taskInstanceId).OrderByDescending(p => p.StartTime);
            if (task.Any())
            {
                return Mapper.Map<TaskModel>(task.First());
            }
            else
            {
                return null;
            }

        }

        public int GetFolderKeyForChildFileId(Guid ParentTaskFileId)
        {
            var context = this.Context as TaskDB.HCP_task;

            var folderKey = (from tfm in context.TaskFile_FolderMapping
                             where tfm.TaskFileId == ParentTaskFileId
                             select tfm.FolderKey).First();

            return folderKey;

        }


        #endregion

        public IEnumerable<ProductionMyTaskModel> GetProductionTaskByUserName(string userName, string role)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetTasksByUserName, please pass in correct context in unit of work.");
            var assignedProductionTasks = Mapper.Map<IEnumerable<ProductionMyTaskModel>>(context.Database.SqlQuerySimple<TaskDB.Programmability.USP_GetProductionTasksByUserName>("USP_GetProductionTasksByUserName",
                new { UserName = userName, Role = role })).Where(a=>a.ProductionTaskType!="OPA").ToList();

            return assignedProductionTasks;
        }


        public void UpdateTaskAssignment(TaskModel task)
        {
            var context = this.Context as TaskDB.HCP_task;
            var entityToUpdate = this.Find(p => p.TaskId == task.TaskId).FirstOrDefault();
            if (entityToUpdate != null)
            {
                entityToUpdate.AssignedTo = task.AssignedTo;
                if (task.PageTypeId == (int)PageType.Amendments)
                {
                    entityToUpdate.TaskStepId = task.TaskStepId;
                }
                //SP293 naresh add below lines for production queue start
                if (task.PageTypeId == (int)PageType.ProductionApplication || task.PageTypeId == (int)PageType.ConstructionSingleStage || task.PageTypeId == (int)PageType.ConstructionTwoStageInitial || task.PageTypeId == (int)PageType.ConstructionTwoStageFinal || task.PageTypeId == (int)PageType.ExecutedClosing || task.PageTypeId == (int)PageType.ClosingAllExceptConstruction || task.PageTypeId == (int)PageType.ClosingConstructionInsuredAdvances || task.PageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion)
                {
                    entityToUpdate.TaskStepId = (int)TaskStep.InProcess;
                }
                //SP293 naresh add below lines for production queue ending
            }

            this.Update(entityToUpdate);
            context.SaveChanges();
        }


        public IEnumerable<FilteredProductionTasksModel> GetFilteredProductionTasks(int productionType, DateTime dateFrom, DateTime dateTo, int lenderId, int loanType)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetFilteredProductionTasks, please pass in correct context in unit of work.");
            var filteredProductionTasks = Mapper.Map<IEnumerable<FilteredProductionTasksModel>>(context.Database.SqlQuerySimple<TaskDB.Programmability.USP_GetFilteredProductionTasks>("USP_GetFilteredProductionTasks",
                new
                {
                    productionType = productionType,
                    dateFrom = dateFrom,
                    dateTo = dateTo,
                    lenderId = lenderId,
                    loanType = loanType
                })).ToList();

            return filteredProductionTasks;
        }

        public IEnumerable<ApplicationAndClosingTypeModel> GetApplicationAndClosingType(Guid taskInstanceId)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetApplicationAndClosingType, please pass in correct context in unit of work.");

            var applicationclosingtypes = Mapper.Map<IEnumerable<ApplicationAndClosingTypeModel>>(context.Database.SqlQuerySimple<TaskDB.Programmability.USP_HCP_Prod_GetApplicationAndClosingType>("USP_HCP_Prod_GetApplicationAndClosingType",
                new
                {
                    TaskinstanceId = taskInstanceId
                })).ToList();

            return applicationclosingtypes;
        }



        public void UpdateTaskStatus(Guid taskInstanceId)
        {
            var context = this.Context as TaskDB.HCP_task;
            var taskToUpdate = this.Find(m => m.TaskInstanceId == taskInstanceId).FirstOrDefault();
            if (taskToUpdate != null)
            {
                //var result = (from r in context.ProdTaskXrefs
                //              where r.TaskInstanceId == taskInstanceId && r.Status != (int)TaskStep.ProjectActionRequestComplete
                //              select r).ToList();
                //foreach (var taskXref in result)
                //{
                //    taskXref.Status = (int)TaskStep.ProjectActionRequestComplete;
                //    taskXref.CompletedOn = DateTime.Now;
                //    taskXref.Comments = "Completed By Underwriter";
                //    context.SaveChanges();
                //}

                taskToUpdate.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                this.Update(taskToUpdate);
                UnitOfWork.Save();
            }
        }
        public string GetAssignedCloserByFHANumber(string FHANumber)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db task in GetReviewerUserIdByTaskInstanceId, please pass in correct context in unit of work.");

            var results =
            context.Database.SqlQuerySimple<TaskDB.usp_HCP_Get_AssignedCloser>(
                "usp_HCP_Get_AssignedCloser",
                new
                {
                    FHANumber = FHANumber
                }).FirstOrDefault();
            if (results != null)
            {
                return results.FirstName + " " + results.LastName;
            }

            return null;
        }


        public IEnumerable<TaskModel> GetCompletedFHAsAndNotAssignedChild()
        {
            var context = this.Context as TaskDB.HCP_task;
            var fhaTasks = (from task in context.Tasks
                            where task.TaskStepId == (int)ProductionFhaRequestStatus.Completed
                            join xref in context.ProdTaskXrefs on task.TaskInstanceId equals xref.TaskInstanceId
                            where xref.AssignedTo == null
                            select task).ToList().OrderByDescending(p => p.TaskId);
            var distnictTask = fhaTasks.GroupBy(x => x.TaskInstanceId).Select(y => y.Last());
            return Mapper.Map<List<TaskModel>>(distnictTask);

        }

        public bool IsFirmCommitmentExists(Guid taskInstanceId)
        {
            var context = this.Context as TaskDB.HCP_task;

            var result = (from tk in context.Tasks
                          join pc in context.ParentChildTask on tk.TaskInstanceId equals pc.ChildTaskInstanceId
                          join tx in context.ProdTaskXrefs on pc.ParentTaskInstanceId equals tx.TaskXrefid
                          where tx.TaskInstanceId == taskInstanceId & tk.TaskStepId == (int)TaskStep.Request
                          select tk).FirstOrDefault();
            //var result = (from tk in context.Tasks
            //              join pc in context.ParentChildTask on tk.TaskInstanceId equals pc.ChildTaskInstanceId
            //              join tx in context.ProdTaskXrefs on pc.ParentTaskInstanceId equals tx.TaskXrefid
            //              where tx.TaskInstanceId == taskInstanceId & tk.TaskStepId == (int)TaskStep.InProcess
            //              select tk).FirstOrDefault();
            return result != null;
        }

        public bool IsFirmCommitmentCompleted(Guid taskInstanceId)
        {
            var context = this.Context as TaskDB.HCP_task;

            var result = (from tk in context.Tasks
                          join pc in context.ParentChildTask on tk.TaskInstanceId equals pc.ChildTaskInstanceId
                          join tx in context.ProdTaskXrefs on pc.ParentTaskInstanceId equals tx.TaskXrefid
                          where tx.TaskInstanceId == taskInstanceId & tk.TaskStepId == (int)TaskStep.Response
                          select tk).FirstOrDefault();
            //var result = (from tk in context.Tasks
            //              join pc in context.ParentChildTask on tk.TaskInstanceId equals pc.ChildTaskInstanceId
            //              join tx in context.ProdTaskXrefs on pc.ParentTaskInstanceId equals tx.TaskXrefid
            //              where tx.TaskInstanceId == taskInstanceId & tk.TaskStepId == (int)TaskStep.Complete
            //              select tk).FirstOrDefault();
            return result != null;
        }

        public Guid AddForm290Task(TaskModel model)
        {
            var task = Mapper.Map<TaskDB.Task>(model);
            this.InsertNew(task);
            UnitOfWork.Save();
            return task.TaskInstanceId;
        }
        public bool AssignForm290Task(ProductionTaskAssignmentModel model)
        {
            var taskForm290 = this.Find(m => m.TaskInstanceId == model.TaskInstanceId).FirstOrDefault();
            if (taskForm290 != null)
            {
                if (taskForm290.AssignedTo == "Queue" || taskForm290.AssignedTo == null)
                {
                    taskForm290.AssignedTo = model.UserName;
                    taskForm290.AssignedBy = UserPrincipal.Current.UserName;
                    taskForm290.TaskStepId = (int)TaskStep.Form290Request;
                    //taskForm290.TaskStepId = (int)ProductionFhaRequestStatus.InProcess;
                    this.Update(taskForm290);
                    UnitOfWork.Save();
                    return true;
                }
            }
            return false;
        }

        public string GetFHANumberByTaskInstanceId(Guid taskInstanceId)
        {
            return this.Find(m => m.TaskInstanceId == taskInstanceId).First().FhaNumber;
        }

		public IEnumerable<TaskModel> GetTasksByFHANumber(string pFHANumber)
		{
			var tasks = this.Find(p => p.FhaNumber == pFHANumber).OrderByDescending(p => p.SequenceId);
			return Mapper.Map<IEnumerable<TaskModel>>(tasks);
		}

        public IEnumerable<Prod_TaskXrefModel> UpdateTaskStatus1(Guid taskInstanceId)
        {
            var context = this.Context as TaskDB.HCP_task;

            var subtask = context.ProdTaskXrefs.Where(x => x.TaskInstanceId == taskInstanceId).ToList();

            return Mapper.Map<IEnumerable<Prod_TaskXrefModel>>(subtask); 
        }

        // harish added new method to save opa files 17-02-2020
        // harish aded new parameter for upload int? IsOPAUpload, int? ISRAIUpload, 24-01-2020
        public void SaveOPAReviewFileStatus(Guid taskInstanceID, int reviewerUserId, int userId, int? ReviewerProdViewId = null)
        {


            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db live in InsertReviewerTaskFileData, please pass in correct context in unit of work.");
            var results =
                context.Database.ExecuteSqlCommandSimple(
                    "usp_HCP_InsertReviewerTaskFileData_AsstMgmt",
                    new
                    {
                        TaskInstanceId = taskInstanceID,
                        ReviewerUserId = reviewerUserId,
                        UserId = userId,
                        ReviewerProdViewId = ReviewerProdViewId

                    });

        }


    }
}