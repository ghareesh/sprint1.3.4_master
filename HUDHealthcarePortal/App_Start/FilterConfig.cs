﻿using System.Web.Mvc;
using HUDHealthcarePortal.Filters;

namespace HUDHealthCarePortal
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AccessDeniedAuthorizeAttribute());
        }
    }
}