namespace HUDHealthcarePortal.CustomModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Loans_82817_New
    {
        public int Id { get; set; }

        [Column("Portfolio Number")]
        [StringLength(255)]
        public string Portfolio_Number { get; set; }

        [Column("Portfolio Name")]
        [StringLength(255)]
        public string Portfolio_Name { get; set; }

        [Column("Master Lease Number")]
        [StringLength(255)]
        public string Master_Lease_Number { get; set; }

        [Column("Credit Review")]
        [StringLength(255)]
        public string Credit_Review { get; set; }

        public double? property_id { get; set; }

        [StringLength(255)]
        public string fha_number_with_suffix { get; set; }

        [Column("Medicare Provider Number")]
        [StringLength(255)]
        public string Medicare_Provider_Number { get; set; }

        [StringLength(255)]
        public string soa_code { get; set; }

        [StringLength(255)]
        public string primary_loan_code { get; set; }

        [StringLength(255)]
        public string soa_numeric_name { get; set; }

        public DateTime? initial_endorsement_date { get; set; }

        [Column(TypeName = "money")]
        public decimal? amortized_unpaid_principal_bal { get; set; }

        [StringLength(255)]
        public string property_name_text { get; set; }

        [StringLength(255)]
        public string address_line1_text { get; set; }

        [StringLength(255)]
        public string address_line2_text { get; set; }

        [StringLength(255)]
        public string city_name_text { get; set; }

        [StringLength(255)]
        public string state_code { get; set; }

        [StringLength(255)]
        public string zip_code { get; set; }

        [StringLength(255)]
        public string troubled_code { get; set; }

        [StringLength(255)]
        public string is_active_dec_case_ind { get; set; }

        [StringLength(255)]
        public string reac_last_inspection_score { get; set; }

        [Column("NCRE Balance")]
        public double? NCRE_Balance { get; set; }

        [Column("Account Executive")]
        [StringLength(255)]
        public string Account_Executive { get; set; }

        [Column("HUD Account Executive Telephone Number")]
        [StringLength(255)]
        public string HUD_Account_Executive_Telephone_Number { get; set; }

        [Column("HUD Account Executive Email Address")]
        [StringLength(255)]
        public string HUD_Account_Executive_Email_Address { get; set; }

        [Column("Workload Manager")]
        [StringLength(255)]
        public string Workload_Manager { get; set; }

        [Column("HUD Workload Manager Telephone Number")]
        [StringLength(255)]
        public string HUD_Workload_Manager_Telephone_Number { get; set; }

        [Column("HUD Workload Manager Email Address")]
        [StringLength(255)]
        public string HUD_Workload_Manager_Email_Address { get; set; }

        [Column("Lender Mortgagee ID")]
        [StringLength(255)]
        public string Lender_Mortgagee_ID { get; set; }

        [StringLength(255)]
        public string Lender { get; set; }

        [Column("Lender Contact Name")]
        [StringLength(255)]
        public string Lender_Contact_Name { get; set; }

        [Column("Lender Contact Title")]
        public double? Lender_Contact_Title { get; set; }

        [Column("Lender Contact Telephone Number")]
        [StringLength(255)]
        public string Lender_Contact_Telephone_Number { get; set; }

        [Column("Lender Contact Email Address")]
        [StringLength(255)]
        public string Lender_Contact_Email_Address { get; set; }

        [Column("Lender Contact Street Address 1")]
        [StringLength(255)]
        public string Lender_Contact_Street_Address_1 { get; set; }

        [Column("Lender Contact Street Address 2")]
        [StringLength(255)]
        public string Lender_Contact_Street_Address_2 { get; set; }

        [Column("Lender Contact City")]
        [StringLength(255)]
        public string Lender_Contact_City { get; set; }

        [Column("Lender Contact State")]
        [StringLength(255)]
        public string Lender_Contact_State { get; set; }

        [Column("Lender Contact Zip Code")]
        [StringLength(255)]
        public string Lender_Contact_Zip_Code { get; set; }

        [Column("Lender Contact Zip Code + 4")]
        [StringLength(255)]
        public string Lender_Contact_Zip_Code___4 { get; set; }

        [Column("Servicer Mortgagee ID")]
        [StringLength(255)]
        public string Servicer_Mortgagee_ID { get; set; }

        [StringLength(255)]
        public string Servicer { get; set; }

        [Column("Servicer Contact Name")]
        [StringLength(255)]
        public string Servicer_Contact_Name { get; set; }

        [Column("Servicer Contact Title")]
        public double? Servicer_Contact_Title { get; set; }

        [Column("Servicer Contact Telephone Number")]
        [StringLength(255)]
        public string Servicer_Contact_Telephone_Number { get; set; }

        [Column("Servicer Contact Email Address")]
        [StringLength(255)]
        public string Servicer_Contact_Email_Address { get; set; }

        [Column("Servicer Contact Street Address 1")]
        [StringLength(255)]
        public string Servicer_Contact_Street_Address_1 { get; set; }

        [Column("Servicer Contact Street Address 2")]
        [StringLength(255)]
        public string Servicer_Contact_Street_Address_2 { get; set; }

        [Column("Servicer Contact City")]
        [StringLength(255)]
        public string Servicer_Contact_City { get; set; }

        [Column("Servicer Contact State")]
        [StringLength(255)]
        public string Servicer_Contact_State { get; set; }

        [Column("Servicer Contact Zip Code")]
        [StringLength(255)]
        public string Servicer_Contact_Zip_Code { get; set; }

        [Column("Servicer Contact Zip + 4")]
        [StringLength(255)]
        public string Servicer_Contact_Zip___4 { get; set; }

        [StringLength(255)]
        public string owner_organization_name { get; set; }

        [StringLength(255)]
        public string owner_address_line1 { get; set; }

        [StringLength(255)]
        public string owner_address_line2 { get; set; }

        [StringLength(255)]
        public string owner_city_name { get; set; }

        [StringLength(255)]
        public string owner_state_code { get; set; }

        [StringLength(255)]
        public string owner_zip_code { get; set; }

        [StringLength(255)]
        public string owner_main_phone_number_text { get; set; }

        [StringLength(255)]
        public string owner_email_text { get; set; }

        [StringLength(255)]
        public string owner_contact_indv_full_name { get; set; }

        [StringLength(255)]
        public string owner_contact_indv_title_text { get; set; }

        [StringLength(255)]
        public string owner_contact_address_line1 { get; set; }

        [StringLength(255)]
        public string owner_contact_address_line2 { get; set; }

        [StringLength(255)]
        public string owner_contact_city_name { get; set; }

        [StringLength(255)]
        public string owner_contact_state_code { get; set; }

        [StringLength(255)]
        public string owner_contact_zip_code { get; set; }

        [StringLength(255)]
        public string owner_contact_main_phone_num { get; set; }

        [StringLength(255)]
        public string owner_contact_email_text { get; set; }

        [StringLength(255)]
        public string mgmt_agent_org_name { get; set; }

        [StringLength(255)]
        public string mgmt_agent_address_line1 { get; set; }

        [StringLength(255)]
        public string mgmt_agent_address_line2 { get; set; }

        [StringLength(255)]
        public string mgmt_agent_city_name { get; set; }

        [StringLength(255)]
        public string mgmt_agent_state_code { get; set; }

        [StringLength(255)]
        public string mgmt_agent_zip_code { get; set; }

        [StringLength(255)]
        public string mgmt_agent_main_phone_number { get; set; }

        [StringLength(255)]
        public string mgmt_agent_email_text { get; set; }

        [StringLength(255)]
        public string mgmt_contact_full_name { get; set; }

        [StringLength(255)]
        public string mgmt_contact_indv_title_text { get; set; }

        [StringLength(255)]
        public string mgmt_contact_address_line1 { get; set; }

        [StringLength(255)]
        public string mgmt_contact_address_line2 { get; set; }

        [StringLength(255)]
        public string mgmt_contact_city_name { get; set; }

        [StringLength(255)]
        public string mgmt_contact_state_code { get; set; }

        [StringLength(255)]
        public string mgmt_contact_zip_code { get; set; }

        [StringLength(255)]
        public string mgmt_contact_main_phn_nbr { get; set; }

        [StringLength(255)]
        public string mgmt_contact_email_text { get; set; }

        [StringLength(255)]
        public string is_nursing_home_ind { get; set; }

        [StringLength(255)]
        public string mddr_in_default_status_name { get; set; }
    }
}
