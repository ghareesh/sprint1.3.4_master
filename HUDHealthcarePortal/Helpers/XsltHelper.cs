﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Xsl;

namespace HUDHealthcarePortal.Helpers
{
    public static class XsltHelper
    {
        public static IHtmlString RenderXml(this ViewPage page, XmlDocument xmlDocument, string xsltPath, Dictionary<string, string> xslArgParams) 
        {
            XsltArgumentList xslArgs  = new  XsltArgumentList();
            if(xslArgParams != null ) {
                 foreach(string key in xslArgParams.Keys) 
                 {
                    xslArgs.AddParam(key, null, xslArgParams[key]);
                 }
            }
            XslCompiledTransform t = new XslCompiledTransform();
            t.Load(xsltPath) ;
            using (var sw = new StringWriter())
            {
                t.Transform(xmlDocument, xslArgs, sw);
                return new HtmlString(sw.ToString());
            }
       }

        public static IHtmlString RenderXml(XmlDocument xmlDocument, string xsltPath, Dictionary<string, string> xslArgParams)
        {
            XsltArgumentList xslArgs = new XsltArgumentList();
            if (xslArgParams != null)
            {
                foreach (string key in xslArgParams.Keys)
                {
                    xslArgs.AddParam(key, null, xslArgParams[key]);
                }
            }
            XslCompiledTransform t = new XslCompiledTransform();
            t.Load(xsltPath);
            using (var sw = new StringWriter())
            {
                t.Transform(xmlDocument, xslArgs, sw);
                return new HtmlString(sw.ToString());
            }
        }
    }
}