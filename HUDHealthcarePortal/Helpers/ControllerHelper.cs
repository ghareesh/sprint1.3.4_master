﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Model;
using MvcSiteMapProvider;
using System.Web.Mvc;
using Model.Production;
using BusinessService.Interfaces;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using Core;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;
using System.ComponentModel;
using BusinessService.ProjectAction;


namespace HUDHealthcarePortal.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class ControllerHelper
    {   
        /// <summary>
        /// Populates the parent node route values.
        /// </summary>
        /// <param name="contextParam">The context parameter.</param>
        public static void PopulateParentNodeRouteValues(string contextParam)
        {
            if (SiteMaps.Current.CurrentNode == null) return;
            var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            if (!string.IsNullOrEmpty(contextParam))
            {
                var strValue = contextParam.Substring(1, contextParam.Length - 2);
                if (strValue.Contains(","))
                {
                    var strParams = strValue.Split(new[] { ',' });
                    foreach (var pairs in strParams.Select(param => param.Split(new[] { '=' })))
                    {
                        parentNode.RouteValues[pairs[0].Trim()] = pairs[1].Trim();
                    }
                }
                else
                {
                    var pairs = strValue.Split(new[] { '=' });
                    parentNode.RouteValues[pairs[0].Trim()] = pairs[1].Trim();
                }
            }
        }

        public static void PopulateParentNodeRouteValuesForPageSort(string routeValueSessionKey)
        {
            if (SiteMaps.Current.CurrentNode == null) return;
            var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(routeValueSessionKey);
            if(routeValues == null)
                return;
            if (!parentNode.RouteValues.Keys.Contains("page"))
                parentNode.RouteValues["page"] = routeValues["page"];
            if (!parentNode.RouteValues.Keys.Contains("sort"))
                parentNode.RouteValues["sort"] = routeValues["sort"];
            if (!parentNode.RouteValues.Keys.Contains("sortdir"))
                parentNode.RouteValues["sortdir"] = routeValues["sortdir"];
        }

        public static void PopulateParentNodeRouteValuesForUploadStatus()
        {
            if (SiteMaps.Current.CurrentNode == null) return;
            var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_UPLOAD_STATUS_LINK_DICT);
            if (routeValues == null)
                return;
            if (!parentNode.RouteValues.Keys.Contains("page"))
                parentNode.RouteValues["page"] = routeValues["page"];
            if (!parentNode.RouteValues.Keys.Contains("sort"))
                parentNode.RouteValues["sort"] = routeValues["sort"];
            if (!parentNode.RouteValues.Keys.Contains("sortdir"))
                parentNode.RouteValues["sortdir"] = routeValues["sortdir"];
            if (!parentNode.RouteValues.Keys.Contains("strLenderId"))
                parentNode.RouteValues["strLenderId"] = routeValues["strLenderId"];
            if (!parentNode.RouteValues.Keys.Contains("strSelectedQtr"))
                parentNode.RouteValues["strSelectedQtr"] = routeValues["strSelectedQtr"];
        }

        public static void PopulateParentNodeRouteValuesForMissingProject()
        {
            if (SiteMaps.Current.CurrentNode == null) return;
            var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_MISSING_PROJECT_LINK_DICT);
            if (routeValues == null)
                return;
            if (!parentNode.RouteValues.Keys.Contains("wlmId"))
                parentNode.RouteValues["wlmId"] = routeValues["wlmId"];
            if (!parentNode.RouteValues.Keys.Contains("aeId"))
                parentNode.RouteValues["aeId"] = routeValues["aeId"];
            if(!parentNode.RouteValues.Keys.Contains("reportYear"))
                parentNode.RouteValues["reportYear"] = routeValues["reportYear"];
        }

        public static void PopulateParentNodeRouteValuesForProjectReports(ReportType reportType)
        {
            if (SiteMaps.Current.CurrentNode == null) return;
            var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_PROJECTS_REPORT_LINK_DICT);

            if(reportType == ReportType.LargeLoanReport)
                routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT);
            else if(reportType == ReportType.RatioExceptionsReport)
                routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT);
            
            if (routeValues == null)
                return;
            if (!parentNode.RouteValues.Keys.Contains("wlmId"))
                parentNode.RouteValues["wlmId"] = routeValues["wlmId"];
            if (!parentNode.RouteValues.Keys.Contains("aeId"))
                parentNode.RouteValues["aeId"] = routeValues["aeId"];
            if (!parentNode.RouteValues.Keys.Contains("reportYear"))
                parentNode.RouteValues["reportYear"] = routeValues["reportYear"];
        }

        public static Dictionary<string, string> CreateDictionaryFromActionParameters(string callingParam)
        {
            var strValue = callingParam.Substring(1, callingParam.Length - 2);
            var dictionary = strValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(part => part.Split('='))
                .ToDictionary(split => split[0].Trim(), split => split[1].Trim());
            return dictionary;
        }

        public static String CheckFileType(string fileName, string fileTypes)
        {
            var extension = Path.GetExtension(fileName);
            if (extension != null && !fileTypes.Split(',').ToList().Contains(extension.ToLower()))
            {
                return "The file is not in the expected format. Please make sure the file you are attaching is in accepted format (" + fileTypes + ").";
            }
            return null;
        }

        public static void PopulateTaskFileModel(HttpPostedFileBase fileStream, FileType fileType, TaskModel task, IList<TaskFileModel> listTaskModel)
        {
            var taskFile = new TaskFileModel();
            taskFile.FileName = Path.GetFileName(fileStream.FileName);
            taskFile.FileType = fileType;
            taskFile.UploadTime = task.MyStartTime;
            BinaryReader rdr = null;
            try
            {
                rdr = new BinaryReader(fileStream.InputStream);
                taskFile.FileData = rdr.ReadBytes(fileStream.ContentLength);
            }
            finally
            {
                // Ensure Streams do not get left to garbage collection, the data can persist.
                if (rdr != null)
                {
                    rdr.Close();
                    rdr.Dispose();
                    fileStream.InputStream.Flush();
                }
                fileStream.InputStream.Close();
                fileStream.InputStream.Dispose();
            }
            // Get the taskGuid for the associated task.
            taskFile.TaskInstanceId = task.TaskInstanceId;
            listTaskModel.Add(taskFile);
        }

        public static TaskFileModel PoupuLateGroupTaskFile(HttpPostedFileBase fileStream ,double fileSize, string checkListId, string startTime, string systemFileName, Guid taskInstanceId)
        {
           var taskFile = new TaskFileModel();
           taskFile.FileName = Path.GetFileName(fileStream.FileName);
            taskFile.GroupFileType = checkListId;
            taskFile.UploadTime = DateTime.UtcNow;
            taskFile.FileSize = fileSize;
            taskFile.SystemFileName  = systemFileName;
            taskFile.TaskInstanceId = taskInstanceId;
            taskFile.CreatedBy = UserPrincipal.Current.UserId;
            //naveen
            BinaryReader rdr = null;
            try
            {
                //naveen
                rdr = new BinaryReader(fileStream.InputStream);
                //taskFile.FileData = null;//rdr.ReadBytes(fileStream.ContentLength);
                taskFile.FileData = rdr.ReadBytes(fileStream.ContentLength);
            }
            finally
            {
                //naveen
                //Ensure Streams do not get left to garbage collection, the data can persist.
                if (rdr != null)
                {
                    rdr.Close();
                    rdr.Dispose();
                    fileStream.InputStream.Flush();
                }
                fileStream.InputStream.Close();
                fileStream.InputStream.Dispose();
            }

            return taskFile;

        }

        //karri;naveen
        public static TaskFileModel PoupuLateGroupTaskFile(string filePhyPath, string startTime, string systemFileName, Guid taskInstanceId)
        {
            string filename = string.Empty;
            string fileType = string.Empty;

            int iPos = filePhyPath.LastIndexOf('\\');//c:\\1234_345678_112222\\9250_myfile.pdf
            if (iPos > 0)
            {
                string combFileName = filePhyPath.Substring(iPos + 1);

                int jPos = combFileName.IndexOf('_');

                if (jPos > 0)
                {
                    fileType = combFileName.Substring(0, jPos);
                    filename = combFileName.Substring(jPos + 1);
                }
            }

            var taskFile = new TaskFileModel();
            taskFile.FileName = Path.GetFileName(filename);
            //taskFile.GroupFileType = checkListId;//?check if rquired or not
            taskFile.UploadTime = DateTime.UtcNow;
            
            taskFile.SystemFileName = systemFileName;
            taskFile.TaskInstanceId = taskInstanceId;
            taskFile.CreatedBy = UserPrincipal.Current.UserId;
            //naveen
            System.IO.FileStream fs = null;
            System.IO.BinaryReader binaryReader = null;
            try
            {
                fs = new System.IO.FileStream(filePhyPath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                binaryReader = new System.IO.BinaryReader(fs);

                long byteLength = new System.IO.FileInfo(filePhyPath).Length;
                taskFile.FileData = binaryReader.ReadBytes((Int32)byteLength);
                taskFile.FileSize = byteLength;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
                if (binaryReader != null)
                    binaryReader.Close();
            }

            return taskFile;

        }

        public static TaskFileModel PopulateGroupTaskFileForSharepoint(string fileName,double fileSize, string checkListId, string startTime, string systemFileName, Guid taskInstanceId)
        {
            var taskFile = new TaskFileModel();
            taskFile.FileName = fileName;
            taskFile.GroupFileType = checkListId;
            taskFile.UploadTime = DateTime.UtcNow;
            taskFile.FileSize = fileSize;
            taskFile.SystemFileName = systemFileName;
            taskFile.TaskInstanceId = taskInstanceId;
            taskFile.CreatedBy = UserPrincipal.Current.UserId;

            return taskFile;
        }

        public static void SetMyTaskSessionRouteValuesToTempData(TempDataDictionary dict)
        {
            var routeValues =
                SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            if (routeValues != null)
            {
                dict["page"] = routeValues["page"];
                dict["sort"] = routeValues["sort"];
                dict["sortdir"] = routeValues["sortdir"];
            }
        }

        public static void SetGroupTaskSessionRouteValuesToTempData(TempDataDictionary dict)
        {
            var routeValues =
                SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_GROUP_TASKS_LINK_DICT);
            if (routeValues != null)
            {
                dict["page"] = routeValues["page"];
                dict["sort"] = routeValues["sort"];
                dict["sortdir"] = routeValues["sortdir"];
            }
        }
        public static void SetMangeUserSessionRouteValuesToTempData(TempDataDictionary dict)
        {
            var routeValues =
                SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_MANGE_USER_REPORT_LINK_DICT);
            if (routeValues != null)
            {
                dict["searchText"] = routeValues["searchText"]; 
                dict["page"] = routeValues["page"];
                dict["sort"] = routeValues["sort"];
                dict["sortdir"] = routeValues["sortdir"];
            }
        }
        public static Prod_MessageModel CheckTransAccess()
        {
            IProd_RestfulWebApiTokenRequest WebApiTokenRequest = new  Prod_RestfulWebApiTokenRequest();
            var model = new Prod_MessageModel();
            var request = new RestfulWebApiTokenResultModel();
            request = WebApiTokenRequest.RequestToken();
            if (string.IsNullOrEmpty(request.access_token))
            {
                //model.Message = "TransAccess is down.";
                model.HtmlRaw = "The file upload functionality is not available at this time. Please try it later.";
                model.status = true;

        }
            else
            {
                model.status = false;
            }
            return model;
        }
        public static TaskFileModel PopulateTaskFileForInternalExternal(string fileName, double fileSize, string fileType, string startTime, string systemFileName, Guid taskInstanceId)
        {
            var taskFile = new TaskFileModel();
            taskFile.FileName = fileName;
            taskFile.GroupFileType = fileType;
            taskFile.UploadTime = DateTime.UtcNow;
            taskFile.FileSize = fileSize;
            taskFile.SystemFileName = systemFileName;
            taskFile.TaskInstanceId = taskInstanceId;
            taskFile.CreatedBy = UserPrincipal.Current.UserId;

            return taskFile;
        }

        public static IList<PAMReportV3ViewModel> FilterProdPAMResults(HUDPamReportFiltersModel model, IList<PAMReportV3ViewModel> outresults,List<string> ProjTypes = null)
        {
            //@*# 701 karri:prodpamfil*@//added extra 2 params for project stage, first one when single, or multiple values selected, last one for ALL type 
            if (model != null)
            {
                if (model.FromDate != null)
                {
                    //outresults = outresults.Where(x => x.ProjectStartDate.ToString("MM/dd/yyyy") == ((DateTime) model.FromDate).ToString("MM/dd/yyyy")).ToList();
                    outresults = outresults.Where(x => x.ProjectStartDate.Date >= ((DateTime)model.FromDate).Date).ToList();
                }
                if (model.ToDate != null)
                {
                    outresults = outresults.Where(x => x.ProjectStartDate.Date <= ((DateTime)model.ToDate).Date).ToList();
                }

                if (model.FHANumber != null && model.ProjectName != null)
                {
                    outresults = outresults.Where(x => x.FhaNumber == model.FHANumber || x.ProjectName == model.ProjectName).ToList();
                }
                else if (model.FHANumber != null)
                {
                    outresults = outresults.Where(x => x.FhaNumber == model.FHANumber).ToList();
                }
                else if (model.ProjectName != null)
                {
                    outresults = outresults.Where(x => x.ProjectName == model.ProjectName).ToList();
                }

                if (model.LoanType != null)
                {
                    outresults = outresults.Where(x => x.LoanType == model.LoanType).ToList();
                }
                if (model.Status != null)
                {
                    outresults = outresults.Where(x => x.Status == model.Status).ToList();
                }
                if (model.FHANumberRequest != null)
                {
                    outresults = outresults.Where(x => x.ProjectStatus == model.FHANumberRequest && x.ProjectStage == "FHA Number Request").ToList();
                }
                if (model.PortfolioNumberAssignment != null)
                {
                    outresults = outresults.Where(x => x.FhaNumber == model.FHANumber).ToList();
                }
                if (model.Title_Survey != null)
                {
                    outresults = outresults.Where(x => x.TitleSurveyStatus == model.Title_Survey).ToList();
                }
                if (model.Environmentalist != null)
                {
                    outresults = outresults.Where(x => x.EnvironmentalistStatus == model.Environmentalist).ToList();
                }
                if (model.Underwriter != null)
                {
                    if (model.Underwriter == "Complete")
                    {
                        outresults = outresults.Where(x => x.ProjectStatus == model.Underwriter && x.IsUnderWriterAssigned == true).ToList();
                    }
                    else if (model.Underwriter == "Unassigned")
                    {
                        outresults = outresults.Where(x => x.ProjectStatus == model.Underwriter && x.IsUnderWriterAssigned == false).ToList();
                    }
                    else
                    {
                        outresults = outresults.Where(x => x.ProjectStatus == model.Underwriter).ToList();
                    }
                }
                if (model.Appraiser != null)
                {
                    outresults = outresults.Where(x => x.AppraiserStatus == model.Appraiser).ToList();
                }
                if (model.ExecutedDocuments != null)
                {
                    outresults = outresults.Where(x => x.ProjectStatus == model.ExecutedDocuments && x.ProjectStage== "Executed Closing").ToList();
                }
				if (model.ProjectStage != null)
				{
					outresults = outresults.Where(x => x.ProjectStage == model.ProjectStage).ToList();
				}
                // #701 Added by siddu//
                if (model.closer != null)
                {
                    outresults = outresults.Where(x => x.closer == model.closer).ToList();
                }
                // #701 Added by siddu//
                if (model.WLMGTM != null)
                {
                    outresults = outresults.Where(x => x.WLMGTM == model.WLMGTM).ToList();

                }

                //@*# 701 karri:prodpamfil*@
                if (ProjTypes != null && ProjTypes.Count > 0)
                {
                    //List<string> projTypes = new List<string>();
                    //projTypes.Add("Application");

                    outresults = outresults.Where(x => ProjTypes.Contains(x.ProjectStage)).ToList();
                }
                // if (model.MasterLeaseNumberAssignment != null)
                //{
                //    outresults = outresults.Where(x => x.MasterLeaseNumberAssignment == model.MasterLeaseNumberAssignment).ToList();
                //}
                // if (model.PortfolioNumberAssignment != null)
                //{
                //    outresults = outresults.Where(x => x.PortfolioNumberAssignment == model.PortfolioNumberAssignment).ToList();
                //}
                // if (model.CorporateCreditReview != null)
                //{
                //    outresults = outresults.Where(x => x.CorporateCreditReview == model.CorporateCreditReview).ToList();
                //}


            }
            return outresults.OrderByDescending(X => X.ProjectStartDate).ToList();
        }



		/// <summary>
		/// Get prodpam summary
		/// </summary>
		/// <returns></returns>
		public static ProdPAMReportStatusGridViewModel GetPamSummary()
		{
			IList<PAMReportV3ViewModel> olistAssignedFHAUnassigned = new List<PAMReportV3ViewModel>();
			IList<PAMReportV3ViewModel> olistAssignedFHACompleted = new List<PAMReportV3ViewModel>();
			List<PAMReportV3ViewModel> olistAssignedFHAInProcess = new List<PAMReportV3ViewModel>();

			List<PAMReportV3ViewModel> olistAssignedAPTasksCompleted = new List<PAMReportV3ViewModel>();
			List<PAMReportV3ViewModel> olistAssignedAPInProcess = new List<PAMReportV3ViewModel>();
			IList<PAMReportV3ViewModel> olistAssignedAPUnassigned = new List<PAMReportV3ViewModel>();

			List<PAMReportV3ViewModel> olistAssignedNCInProcess = new List<PAMReportV3ViewModel>();
			IList<PAMReportV3ViewModel> olistAssignedNCUnassigned = new List<PAMReportV3ViewModel>();

			List<PAMReportV3ViewModel> olistAssignedForm290Completed = new List<PAMReportV3ViewModel>();
			List<PAMReportV3ViewModel> olistAssignedForm290Assigned = new List<PAMReportV3ViewModel>();

			List<PAMReportV3ViewModel> olistAssignedAmendmentTasksCompleted = new List<PAMReportV3ViewModel>();
			List<PAMReportV3ViewModel> olistAssignedAmendmentInProcess = new List<PAMReportV3ViewModel>();
			IList<PAMReportV3ViewModel> olistUnAssignedAmendment = new List<PAMReportV3ViewModel>();


			ProdPAMReportStatusGridViewModel statusGridViewModel = new ProdPAMReportStatusGridViewModel();
			var dates = ControllerHelper.GetFYDuration();

			DateTime fisStart = dates[0];
			DateTime fisEnd = dates[1];


			olistAssignedFHACompleted = ProdPamGetAssignedFHARequests((int)TaskStep.Complete).ToList();
			olistAssignedFHACompleted = olistAssignedFHACompleted.Where(o => o.FhaNumber != null).ToList();
			olistAssignedFHAInProcess = ProdPamGetAssignedFHARequests((int)TaskStep.InProcess).ToList();
			olistAssignedFHAUnassigned = ProdPamGetUnAssignedFHARequests().ToList();

			olistAssignedAPTasksCompleted = ProdPamGetAssignedAPTasks((int)TaskStep.Complete).ToList();
			olistAssignedAPInProcess = ProdPamGetAssignedAPTasks((int)TaskStep.InProcess).ToList();
			olistAssignedAPUnassigned = ProdPamGetUnAssignedAPTasks().ToList();

			olistAssignedNCInProcess = ProdPamGetAssignedNCDraftClosingTasks((int)TaskStep.InProcess).ToList();
			olistAssignedNCUnassigned = ProdPamGetUnAssignedNCDraftClosingTasks().ToList();

			//if underwriter in process from AP Q list, remove and add to AP inprocess list
			IList<PAMReportV3ViewModel> olistUwInProcessFromUnassignedAP = new List<PAMReportV3ViewModel>();
			olistUwInProcessFromUnassignedAP = GetUwInProcessFromUnassignedAP(olistAssignedAPUnassigned);

			foreach (var obj in olistUwInProcessFromUnassignedAP)
			{
				olistAssignedAPUnassigned.Remove(obj);
				olistAssignedAPInProcess.Add(obj);
			}
			//if underwriter in process from Non cons initial draft  Q list, remove and add to  inprocess list
			IList<PAMReportV3ViewModel> olistUwInProcessFromUnassignedNC = new List<PAMReportV3ViewModel>();
			olistUwInProcessFromUnassignedNC = GetUwInProcessFromUnassignedNC(olistAssignedNCUnassigned);

			foreach (var obj in olistUwInProcessFromUnassignedNC)
			{
				olistAssignedNCUnassigned.Remove(obj);
				olistAssignedNCInProcess.Add(obj);
			}

			olistAssignedForm290Completed = ProdPamGetAssignedForm290((int)TaskStep.Complete).GroupBy(item => item.FhaNumber)
																			   .Select(grouping => grouping.FirstOrDefault()).ToList();
			olistAssignedForm290Completed = olistAssignedForm290Completed.Where(i => i.Status == "Form290 Request Completed").ToList();

			olistAssignedForm290Assigned = ProdPamGetAssignedForm290((int)TaskStep.InProcess).GroupBy(item => item.FhaNumber)
																			   .Select(grouping => grouping.FirstOrDefault()).ToList();
			olistAssignedForm290Assigned = olistAssignedForm290Assigned.Where(i => i.Status == "In Process" || i.Status == "Form290 In Process").ToList();

			//fha number process
			statusGridViewModel.TotalFHAInqueue = olistAssignedFHAUnassigned.Count();
			statusGridViewModel.TotalFHAComplete = olistAssignedFHACompleted.Count();
			statusGridViewModel.TotalFHAInprocess = olistAssignedFHAInProcess.Count();

			//application tasks
			statusGridViewModel.TotalApplicationInqueue = olistAssignedAPUnassigned.Count();
			statusGridViewModel.TotalApplicationInprocess = olistAssignedAPInProcess.Count();
			statusGridViewModel.TotalApplicationComplete = olistAssignedAPTasksCompleted.Count();
			statusGridViewModel.TotalWaitingApplication = statusGridViewModel.TotalFHAComplete - statusGridViewModel.TotalApplicationComplete - statusGridViewModel.TotalApplicationInqueue - statusGridViewModel.TotalApplicationInprocess;

			//Form290
			statusGridViewModel.Total290Assigned = olistAssignedForm290Assigned.GroupBy(item => item.FhaNumber)
																			   .Select(grouping => grouping.FirstOrDefault()).Count();
			statusGridViewModel.Total290Complete = olistAssignedForm290Completed.GroupBy(item => item.FhaNumber)
																			   .Select(grouping => grouping.FirstOrDefault()).Count();

			//Executed docs
			statusGridViewModel.TotalExecutedDocUploaded = ProdPamGetExecutedDocs().Count();
			//non construction draft closing
			statusGridViewModel.TotalClosingInprocess = olistAssignedNCInProcess.Count();
			statusGridViewModel.TotalClosingComplete = ProdPamGetAssignedNCDraftClosingTasks((int)TaskStep.Complete).Count();
			statusGridViewModel.TotalClosingInqueue = olistAssignedNCUnassigned.Count();
			statusGridViewModel.TotalWaitingClosing = statusGridViewModel.TotalApplicationComplete - statusGridViewModel.TotalClosingInprocess - statusGridViewModel.TotalClosingComplete - statusGridViewModel.TotalClosingInqueue;

            //amendments
            //amendments
            olistUnAssignedAmendment = ProdPamGetUnAssignedAmendmentTasks().GroupBy(item => new { item.FhaNumber, item.TaskId }).Select(grouping => grouping.FirstOrDefault()).ToList();
            statusGridViewModel.AmendmentInqueue = olistUnAssignedAmendment.Count();// ProdPamGetUnAssignedAmendmentTasks().Count();
                                                                                    //olistAssignedAmendmentInProcess = ProdPamGetAssignedAmendmentClosingTasks((int)TaskStep.InProcess).ToList();
            olistAssignedAmendmentInProcess = ProdPamGetAssignedAmendmentClosingTasks((int)TaskStep.InProcess).GroupBy(item => new { item.FhaNumber, item.TaskId })
                                                                               .Select(grouping => grouping.FirstOrDefault()).ToList();

            statusGridViewModel.AmendmentInProcess = olistAssignedAmendmentInProcess.Count();
            olistAssignedAmendmentTasksCompleted = ProdPamGetAssignedAmendmentClosingTasks((int)TaskStep.Complete).GroupBy(item => new { item.FhaNumber, item.TaskId })
                                                    .Select(grouping => grouping.FirstOrDefault()).ToList();


            statusGridViewModel.AmendmentComplete = olistAssignedAmendmentTasksCompleted.Count();
			return statusGridViewModel;

		}


		public static IList<PAMReportV3ViewModel> ProdPamGetAssignedAmendmentClosingTasks(int pStatus)
		{
			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IEnumerable<TaskModel> productionTasks;
			IAppProcessManager appProcessManager = new AppProcessManager();
			IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			int reqType = (int)PageType.Amendments;
			//productionTasks = _productionQueue.GetProductionTaskByType(reqType).ToList();
			if (pStatus == (int)ProductionAppProcessStatus.Completed)
				pStatus = (int)ProductionAppProcessStatus.ProjectActionRequestComplete;
			productionTasks = _productionQueue.GetProductionTaskByTypeAndStatus(reqType, pStatus).ToList();

			var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
			var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

			var applicationRequests = appProcessManager.GetAppRequests().ToList();
			var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
			List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
			if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
			{
				submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
			}

			var fhaRequestTasks = (from task in taskModels.Where(m => m.PageTypeId == (int)PageType.FhaRequest)
								   join fhaRequest in _productionQueue.GetFhaRequests(distnictInstanceId).ToList()
								   on task.TaskInstanceId equals fhaRequest.TaskinstanceId
								   select new
								   {
									   task.TaskInstanceId,
									   task.TaskId,
									   TaskName = fhaRequest.projectName,
									   task.SequenceId,
									   task.PageTypeId,
									   task.StartTime,
									   task.AssignedBy,
									   task.AssignedTo,
									   ModofiedOn = fhaRequest.ModifiedOn,
									   Lender = fhaRequest.LenderName,
									   Status =
									   EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(task.TaskStepId.ToString())),
									   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                       //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                       DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                       LoanType = fhaRequest.LoanType,
									   task.FHANumber
								   });
			var productionApTasks = (from appRequest in applicationRequests
									 join task in productionTasks
			 on appRequest.TaskinstanceId equals task.TaskInstanceId
									 select new
									 {
										 task.TaskInstanceId,
										 task.TaskId,
										 TaskName = appRequest.projectName,
										 task.SequenceId,
										 task.PageTypeId,
										 task.StartTime,
										 task.AssignedBy,
										 task.AssignedTo,
										 ModofiedOn = DateTime.UtcNow,
										 Lender = appRequest.LenderName,
										 Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
										 Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                         //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
										 task.FHANumber
									 }).ToList();

			var form290AssignedTasks = (from closeRequest in submittedForm290
										join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
										   on closeRequest.TaskinstanceId equals task.TaskInstanceId
										select new
										{
											task.TaskInstanceId,
											task.TaskId,
											TaskName = closeRequest.projectName,
											task.SequenceId,
											task.PageTypeId,
											task.StartTime,
											task.AssignedBy,
											task.AssignedTo,
											ModofiedOn = DateTime.UtcNow,
											Lender = closeRequest.LenderName,
											Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
											Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                            //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                            DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                            LoanType = closeRequest.LoanType,
											task.FHANumber
										}).ToList();
			var allProductionTasks = fhaRequestTasks.Union(productionApTasks).Union(form290AssignedTasks).ToList();
			var groupedTasks = allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First());
			foreach (var item in groupedTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;// item.TaskName;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.Status = item.Status;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
                objPAMReportV3ViewModel.TaskId = item.TaskId;

                olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			olistPAMReportV3ViewModel = olistPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// Prodpamreport-  FHA inqueue
		/// </summary>	
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetUnAssignedFHARequests()
		{
			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			//IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			IAppProcessManager appProcessManager = new AppProcessManager();
			int reqType = (int)PageType.FhaRequest;


			var productionTasks = _productionQueue.GetProductionQueueByType(reqType).ToList();
			var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
			var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

			var notAssignedTasks = _productionQueue.GetCompletedFHAsAndNotAssignedChild();
			productionTasks = productionTasks.Union(notAssignedTasks).ToList();

			//var applicationRequests = appProcessManager.GetAppRequests().ToList();
			//var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
			//List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
			//if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
			//{
			//	submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
			//}			

			var fhaRequestTasks = (from fhaRequest in _productionQueue.GetFhaRequests(distnictInstanceId).ToList()
								   join task in productionTasks
									on fhaRequest.TaskinstanceId equals task.TaskInstanceId
								   select new
								   {
									   task.TaskInstanceId,
									   task.TaskId,
									   TaskName = fhaRequest.projectName,
									   task.SequenceId,
									   task.PageTypeId,
									   task.StartTime,
									   task.AssignedBy,
									   task.AssignedTo,
									   Lender = fhaRequest.LenderName,
									   ModofiedOn = fhaRequest.ModifiedOn,
									   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                       //DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                                       DaysInQueue = (DateTime.UtcNow.Date - task.StartTime.Date).Days + " Day(s)",
                                       IsLIHTC = fhaRequest.IsLIHTC == true ? "Yes" : "",
									   LoanType = fhaRequest.LoanType,
									   LoanAmount = fhaRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
									   task.FHANumber
								   }).ToList();

			foreach (var item in fhaRequestTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = item.TaskName + "-" + item.LoanType;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// Prodpamreport report - get FHA (inprocess and completed) based on status
		/// </summary>
		/// <param name="pStatus"></param>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetAssignedFHARequests(int pStatus)
		{
			List<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IEnumerable<TaskModel> productionTasks;
			IAppProcessManager appProcessManager = new AppProcessManager();
			int fhareqType = (int)PageType.FhaRequest;


			productionTasks = _productionQueue.GetProductionTaskByTypeAndStatus(fhareqType, pStatus).ToList();
			var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
			var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();
			var applicationRequests = appProcessManager.GetAppRequests().ToList();
			var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
			List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
			if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
			{
				submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
			}
			var fhaRequestTasks = (from task in taskModels.Where(m => m.PageTypeId == (int)PageType.FhaRequest)
								   join fhaRequest in _productionQueue.GetFhaRequests(distnictInstanceId).ToList()
								   on task.TaskInstanceId equals fhaRequest.TaskinstanceId
								   select new
								   {
									   task.TaskInstanceId,
									   task.TaskId,
									   TaskName = fhaRequest.projectName,
									   task.SequenceId,
									   task.PageTypeId,
									   task.StartTime,
									   task.AssignedBy,
									   task.AssignedTo,
									   ModofiedOn = fhaRequest.ModifiedOn,
									   Lender = fhaRequest.LenderName,
									   Status =
									   EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(task.TaskStepId.ToString())),
									   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                       //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                       DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                       LoanType = fhaRequest.LoanType,
									   task.FHANumber
								   }).ToList();

			var productionApTasks = (from appRequest in applicationRequests
									 join task in productionTasks
			 on appRequest.TaskinstanceId equals task.TaskInstanceId
									 select new
									 {
										 task.TaskInstanceId,
										 task.TaskId,
										 TaskName = appRequest.projectName,
										 task.SequenceId,
										 task.PageTypeId,
										 task.StartTime,
										 task.AssignedBy,
										 task.AssignedTo,
										 ModofiedOn = DateTime.UtcNow,
										 Lender = appRequest.LenderName,
										 Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
										 Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                         //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
										 task.FHANumber
									 }).ToList();

			var form290AssignedTasks = (from closeRequest in submittedForm290
										join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
										   on closeRequest.TaskinstanceId equals task.TaskInstanceId
										select new
										{
											task.TaskInstanceId,
											task.TaskId,
											TaskName = closeRequest.projectName,
											task.SequenceId,
											task.PageTypeId,
											task.StartTime,
											task.AssignedBy,
											task.AssignedTo,
											ModofiedOn = DateTime.UtcNow,
											Lender = closeRequest.LenderName,
											Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
											Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
											//DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                            DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                            LoanType = closeRequest.LoanType,
											task.FHANumber
										}).ToList();

			var allProductionTasks = fhaRequestTasks.Union(productionApTasks).Union(form290AssignedTasks).ToList();
			var groupedTasks = allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First());



			foreach (var item in groupedTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = item.TaskName + "-" + item.LoanType;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.Status = item.Status;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				//objPAMReportV3ViewModel.PageTypeId = item.PageTypeId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// Prodpamreport-  application tasks inqueue
		/// </summary>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetUnAssignedAPTasks()
		{
			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IAppProcessManager appProcessManager = new AppProcessManager();
			IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			int reqType = (int)PageType.ProductionApplication;

			var productionTasks = _productionQueue.GetProductionQueueByType(reqType).ToList();
			var notAssignedTasks = _productionQueue.GetCompletedFHAsAndNotAssignedChild();
			productionTasks = productionTasks.Union(notAssignedTasks).ToList();

			var applicationRequests = appProcessManager.GetAppRequests().ToList();
			var apTasks = (from appRequest in applicationRequests
						   join task in productionTasks
						 on appRequest.TaskinstanceId equals task.TaskInstanceId
						   select new
						   {
							   task.TaskInstanceId,
							   task.TaskId,
							   TaskName = appRequest.projectName,
							   task.SequenceId,
							   task.PageTypeId,
							   task.StartTime,
							   task.AssignedBy,
							   task.AssignedTo,
							   Lender = appRequest.LenderName,
							   ModofiedOn = appRequest.ModifiedOn,
							   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                               //DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                               DaysInQueue = (DateTime.UtcNow.Date - task.StartTime.Date).Days + " Day(s)",
                               IsLIHTC = appRequest.IsLIHTC == true ? "Yes" : "",
							   LoanType = appRequest.LoanType,
							   LoanAmount = appRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
							   task.FHANumber
						   }).ToList();

			foreach (var item in apTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;//item.TaskName;				
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			olistPAMReportV3ViewModel = olistPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// Get Unassigned Amendmments
		/// </summary>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetUnAssignedAmendmentTasks()
		{
			//IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			//IProductionQueueManager _productionQueue = new ProductionQueueManager();
			//IAppProcessManager appProcessManager = new AppProcessManager();
			//IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			//int reqType = (int)PageType.Amendments;

			//var productionTasks = _productionQueue.GetProductionQueueByType(reqType).ToList();
			//var notAssignedTasks = _productionQueue.GetCompletedFHAsAndNotAssignedChild();
			//productionTasks = productionTasks.Union(notAssignedTasks).ToList();

			//var applicationRequests = appProcessManager.GetAppRequests().ToList();
			//var apTasks = (from appRequest in applicationRequests
			//			   join task in productionTasks
			//			 on appRequest.TaskinstanceId equals task.TaskInstanceId
			//			   select new
			//			   {
			//				   task.TaskInstanceId,
			//				   task.TaskId,
			//				   TaskName = appRequest.projectName,
			//				   task.SequenceId,
			//				   task.PageTypeId,
			//				   task.StartTime,
			//				   task.AssignedBy,
			//				   task.AssignedTo,
			//				   Lender = appRequest.LenderName,
			//				   ModofiedOn = appRequest.ModifiedOn,
			//				   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
			//				   DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
			//				   IsLIHTC = appRequest.IsLIHTC == true ? "Yes" : "",
			//				   LoanType = appRequest.LoanType,
			//				   LoanAmount = appRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
			//				   task.FHANumber
			//			   }).ToList();

			//foreach (var item in apTasks)
			//{
			//	PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
			//	objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;//item.TaskName;				
			//	objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
			//	objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
			//	olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			//}
			//return olistPAMReportV3ViewModel;


			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IAppProcessManager appProcessManager = new AppProcessManager();
			IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			int reqType = (int)PageType.Amendments;

			var productionTasks = _productionQueue.GetProductionQueueByType(reqType).ToList();
			var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
			var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

			var notAssignedTasks = _productionQueue.GetCompletedFHAsAndNotAssignedChild();
			productionTasks = productionTasks.Union(notAssignedTasks).ToList();

			var applicationRequests = appProcessManager.GetAppRequests().ToList();
			var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
			List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
			if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
			{
				submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
			}
			var apTasks = (from fhaRequest in _productionQueue.GetFhaRequests(distnictInstanceId).ToList()
						   join task in productionTasks
						   //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
						   on fhaRequest.TaskinstanceId equals task.TaskInstanceId
						   select new
						   {
							   task.TaskInstanceId,
							   task.TaskId,
							   TaskName = fhaRequest.projectName,
							   task.SequenceId,
							   task.PageTypeId,
							   task.StartTime,
							   task.AssignedBy,
							   task.AssignedTo,
							   Lender = fhaRequest.LenderName,
							   ModofiedOn = fhaRequest.ModifiedOn,
							   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                               //DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                               DaysInQueue = (DateTime.UtcNow.Date - task.StartTime.Date).Days + " Day(s)",
                               IsLIHTC = fhaRequest.IsLIHTC == true ? "Yes" : "",
							   LoanType = fhaRequest.LoanType,
							   LoanAmount = fhaRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
							   task.FHANumber

						   })
						   .Union(from appRequest in applicationRequests
								  join task in productionTasks
								on appRequest.TaskinstanceId equals task.TaskInstanceId
								  select new
								  {
									  task.TaskInstanceId,
									  task.TaskId,
									  TaskName = appRequest.projectName,
									  task.SequenceId,
									  task.PageTypeId,
									  task.StartTime,
									  task.AssignedBy,
									  task.AssignedTo,
									  Lender = appRequest.LenderName,
									  ModofiedOn = appRequest.ModifiedOn,
									  Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                      //DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                                      DaysInQueue = (DateTime.UtcNow.Date - task.StartTime.Date).Days + " Day(s)",
                                      IsLIHTC = appRequest.IsLIHTC == true ? "Yes" : "",
									  LoanType = appRequest.LoanType,
									  LoanAmount = appRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
									  task.FHANumber
								  })
									.Union(from closeRequest in submittedForm290
										   join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
											  on closeRequest.TaskinstanceId equals task.TaskInstanceId
										   select new
										   {
											   task.TaskInstanceId,
											   task.TaskId,
											   TaskName = closeRequest.projectName,
											   task.SequenceId,
											   task.PageTypeId,
											   task.StartTime,
											   task.AssignedBy,
											   task.AssignedTo,
											   Lender = closeRequest.LenderName,
											   ModofiedOn = closeRequest.ModifiedOn,
											   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                               //DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                                               DaysInQueue = (DateTime.UtcNow.Date - task.StartTime.Date).Days + " Day(s)",
                                               IsLIHTC = closeRequest.IsLIHTC == true ? "Yes" : "",
											   LoanType = closeRequest.LoanType,
											   LoanAmount = closeRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
											   task.FHANumber
										   }).ToList();

			foreach (var item in apTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;// item.TaskName;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			olistPAMReportV3ViewModel = olistPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// Prodpamreport -  get Application (inprocess and completed) based on status 
		/// </summary>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetAssignedAPTasks(int pStatus)
		{
			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IEnumerable<TaskModel> productionTasks;
			IAppProcessManager appProcessManager = new AppProcessManager();
			IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();

			int reqType = (int)PageType.ProductionApplication;

			if (pStatus == (int)ProductionAppProcessStatus.Completed)
				pStatus = (int)ProductionAppProcessStatus.ProjectActionRequestComplete;
			productionTasks = _productionQueue.GetProductionTaskByTypeAndStatus(reqType, pStatus).ToList();
			var applicationRequests = appProcessManager.GetAppRequests().ToList();

			var productionApTasks = (from appRequest in applicationRequests
									 join task in productionTasks on appRequest.TaskinstanceId equals task.TaskInstanceId
									 select new
									 {
										 task.TaskInstanceId,
										 task.TaskId,
										 TaskName = appRequest.projectName,
										 task.SequenceId,
										 task.PageTypeId,
										 task.StartTime,
										 task.AssignedBy,
										 task.AssignedTo,
										 ModofiedOn = DateTime.UtcNow,
										 Lender = appRequest.LenderName,
										 Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
										 Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                         //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
										 task.FHANumber
									 }).ToList();

			var groupedTasks = productionApTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First());

			foreach (var item in groupedTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;//item.TaskName;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.Status = item.Status;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				objPAMReportV3ViewModel.PageTypeId = item.PageTypeId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			olistPAMReportV3ViewModel = olistPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAssignedAPUnassignedList"></param>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> GetUwInProcessFromUnassignedAP(IList<PAMReportV3ViewModel> pAssignedAPUnassignedList)
		{
			//IProductionQueueManager _productionQueue = new ProductionQueueManager();
			//IEnumerable<TaskModel> productionTasks ;
			IList<PAMReportV3ViewModel> oList = new List<PAMReportV3ViewModel>();
			IProd_TaskXrefManager prod_TaskXrefManager = new Prod_TaskXrefManager();
			foreach (var obj in pAssignedAPUnassignedList)
			{   //GetFHARequestDetail
				var productionSubTasks = prod_TaskXrefManager.GetProductionSubTasks(obj.CurrentTaskInstanceId).ToList();
				//EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(data.ViewId.ToString()))
				//var ob = productionSubTasks.Where(o => o. == 4).ToList();
				if (productionSubTasks != null && productionSubTasks.Count > 0)
				{
					foreach (var data in productionSubTasks)
					{
						var status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(data.Status.ToString()));
						var fhaType = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(data.ViewId.ToString()));
						if (fhaType == "UnderWriter" && status == "In Proccess")
							oList.Add(obj);
					}

				}
			}
			oList = oList.Where(o => o.FhaNumber != null).ToList();
			return oList;
		}

		/// <summary>
		/// non construction closing (executed docs up;oaded by lender)
		/// </summary>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetExecutedDocs()
		{
			List<TaskModel> olstProductionTasks = new List<TaskModel>();
			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IEnumerable<TaskModel> productionTasks;
			IAppProcessManager appProcessManager = new AppProcessManager();
			IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			int reqType = (int)PageType.ExecutedClosing;
			productionTasks = _productionQueue.GetProductionTaskByType(reqType).ToList();
			//productionTasks = _productionQueue.GetProductionTaskByTypeAndStatus(reqType, pStatus).ToList();		
			olstProductionTasks = RemoveproductionTasks(productionTasks.ToList());
			var applicationRequests = appProcessManager.GetAppRequests().ToList();
			List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();

			var productionApTasks = (from appRequest in applicationRequests
									 join task in olstProductionTasks//productionTasks
									on appRequest.TaskinstanceId equals task.TaskInstanceId
									 select new
									 {
										 task.TaskInstanceId,
										 task.TaskId,
										 TaskName = appRequest.projectName,
										 task.SequenceId,
										 task.PageTypeId,
										 task.StartTime,
										 task.AssignedBy,
										 task.AssignedTo,
										 ModofiedOn = DateTime.UtcNow,
										 Lender = appRequest.LenderName,
										 Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
										 Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                         //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
										 task.FHANumber
									 }).ToList();

			//olstTaskModel = RemoveproductionTaskList(productionApTasks.ToList());

			foreach (var item in productionApTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;// item.TaskName;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.Status = item.Status;
				objPAMReportV3ViewModel.PageTypeId = item.PageTypeId;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			olistPAMReportV3ViewModel = RemoveproductionTaskList(olistPAMReportV3ViewModel);
			olistPAMReportV3ViewModel = olistPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// Remove duplicate from  list
		/// </summary>
		/// <param name="plistPAMReportV3ViewModel"></param>
		/// <returns></returns>
		private static IList<PAMReportV3ViewModel> RemoveproductionTaskList(IList<PAMReportV3ViewModel> plistPAMReportV3ViewModel)
		{
			var query = plistPAMReportV3ViewModel.GroupBy(item => item.FhaNumber)
				.Select(grouping => grouping.FirstOrDefault());
			return query.ToList();
		}

		/// <summary>
		/// Remove duplicate from anonymous list
		/// </summary>
		/// <param name="plist"></param>
		/// <returns></returns>
		private static List<TaskModel> RemoveproductionTasks(List<TaskModel> plist)
		{
			List<TaskModel> lstTaskModel = new List<TaskModel>();

			plist.ForEach(s =>
			{
				if (lstTaskModel.FindAll(f => { return f.FHANumber == s.FHANumber; }).Count == 0)
				{
					lstTaskModel.Add(s);
				}
			});

			//var query = plist.GroupBy(item => item.FHANumber)
			//	.Select(grouping => grouping.FirstOrDefault());

			return lstTaskModel;
		}

		/// <summary>
		/// Initial draft closing in queue
		/// </summary>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetUnAssignedNCDraftClosingTasks()
		{
			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IAppProcessManager appProcessManager = new AppProcessManager();
			IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			int reqType = (int)PageType.ClosingAllExceptConstruction;

			var productionTasks = _productionQueue.GetProductionQueueByType(reqType).ToList();
			var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
			var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

			var notAssignedTasks = _productionQueue.GetCompletedFHAsAndNotAssignedChild();
			productionTasks = productionTasks.Union(notAssignedTasks).ToList();

			var applicationRequests = appProcessManager.GetAppRequests().ToList();
			var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
			List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
			if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
			{
				submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
			}
			var apTasks = (from fhaRequest in _productionQueue.GetFhaRequests(distnictInstanceId).ToList()
						   join task in productionTasks
	//fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
	on fhaRequest.TaskinstanceId equals task.TaskInstanceId
						   select new
						   {
							   task.TaskInstanceId,
							   task.TaskId,
							   TaskName = fhaRequest.projectName,
							   task.SequenceId,
							   task.PageTypeId,
							   task.StartTime,
							   task.AssignedBy,
							   task.AssignedTo,
							   Lender = fhaRequest.LenderName,
							   ModofiedOn = fhaRequest.ModifiedOn,
							   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                               //DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                               DaysInQueue = (DateTime.UtcNow.Date - task.StartTime.Date).Days + " Day(s)",
                               IsLIHTC = fhaRequest.IsLIHTC == true ? "Yes" : "",
							   LoanType = fhaRequest.LoanType,
							   LoanAmount = fhaRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
							   task.FHANumber

						   }).Union(from appRequest in applicationRequests
									join task in productionTasks
								  on appRequest.TaskinstanceId equals task.TaskInstanceId
									select new
									{
										task.TaskInstanceId,
										task.TaskId,
										TaskName = appRequest.projectName,
										task.SequenceId,
										task.PageTypeId,
										task.StartTime,
										task.AssignedBy,
										task.AssignedTo,
										Lender = appRequest.LenderName,
										ModofiedOn = appRequest.ModifiedOn,
										Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                        //DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                                        DaysInQueue = (DateTime.UtcNow.Date - task.StartTime.Date).Days + " Day(s)",
                                        IsLIHTC = appRequest.IsLIHTC == true ? "Yes" : "",
										LoanType = appRequest.LoanType,
										LoanAmount = appRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
										task.FHANumber
									})
												   .Union(from closeRequest in submittedForm290
														  join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
															 on closeRequest.TaskinstanceId equals task.TaskInstanceId
														  select new
														  {
															  task.TaskInstanceId,
															  task.TaskId,
															  TaskName = closeRequest.projectName,
															  task.SequenceId,
															  task.PageTypeId,
															  task.StartTime,
															  task.AssignedBy,
															  task.AssignedTo,
															  Lender = closeRequest.LenderName,
															  ModofiedOn = closeRequest.ModifiedOn,
															  Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                                              //DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                                                              DaysInQueue = (DateTime.UtcNow.Date - task.StartTime.Date).Days + " Day(s)",
                                                              IsLIHTC = closeRequest.IsLIHTC == true ? "Yes" : "",
															  LoanType = closeRequest.LoanType,
															  LoanAmount = closeRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),
															  task.FHANumber
														  }).ToList();

			foreach (var item in apTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;// item.TaskName;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			olistPAMReportV3ViewModel = olistPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// if underwriter in process from Non cons initial draft  Q list, remove and add to  inprocess list
		/// </summary>
		/// <param name="pAssignedNCUnassignedList"></param>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> GetUwInProcessFromUnassignedNC(IList<PAMReportV3ViewModel> pAssignedNCUnassignedList)
		{
			IList<PAMReportV3ViewModel> oList = new List<PAMReportV3ViewModel>();
			IProd_TaskXrefManager prod_TaskXrefManager = new Prod_TaskXrefManager();
			foreach (var obj in pAssignedNCUnassignedList)
			{   //GetFHARequestDetail
				var productionSubTasks = prod_TaskXrefManager.GetProductionSubTasks(obj.CurrentTaskInstanceId).ToList();
				if (productionSubTasks != null && productionSubTasks.Count > 0)
				{
					foreach (var data in productionSubTasks)
					{
						var status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(data.Status.ToString()));
						var fhaType = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(data.ViewId.ToString()));
						if (fhaType == "UnderWriter" && status == "In Proccess")
							oList.Add(obj);
					}

				}
			}
			oList = oList.Where(o => o.FhaNumber != null).ToList();
			return oList;
		}





		/// <summary>
		/// Draft closing (inprocess and completed)
		/// </summary>
		/// <param name="pStatus"></param>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetAssignedNCDraftClosingTasks(int pStatus)
		{
			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IEnumerable<TaskModel> productionTasks;
			IAppProcessManager appProcessManager = new AppProcessManager();
			IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			int reqType = (int)PageType.ClosingAllExceptConstruction;
			//productionTasks = _productionQueue.GetProductionTaskByType(reqType).ToList();
			if (pStatus == (int)ProductionAppProcessStatus.Completed)
				pStatus = (int)ProductionAppProcessStatus.ProjectActionRequestComplete;
			productionTasks = _productionQueue.GetProductionTaskByTypeAndStatus(reqType, pStatus).ToList();

			var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
			var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

			var applicationRequests = appProcessManager.GetAppRequests().ToList();
			var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
			List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
			if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
			{
				submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
			}

			var fhaRequestTasks = (from task in taskModels.Where(m => m.PageTypeId == (int)PageType.FhaRequest)
								   join fhaRequest in _productionQueue.GetFhaRequests(distnictInstanceId).ToList()
								   on task.TaskInstanceId equals fhaRequest.TaskinstanceId
								   select new
								   {
									   task.TaskInstanceId,
									   task.TaskId,
									   TaskName = fhaRequest.projectName,
									   task.SequenceId,
									   task.PageTypeId,
									   task.StartTime,
									   task.AssignedBy,
									   task.AssignedTo,
									   ModofiedOn = fhaRequest.ModifiedOn,
									   Lender = fhaRequest.LenderName,
									   Status =
									   EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(task.TaskStepId.ToString())),
									   Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                       //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                       DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                       LoanType = fhaRequest.LoanType,
									   task.FHANumber
								   });
			var productionApTasks = (from appRequest in applicationRequests
									 join task in productionTasks
			 on appRequest.TaskinstanceId equals task.TaskInstanceId
									 select new
									 {
										 task.TaskInstanceId,
										 task.TaskId,
										 TaskName = appRequest.projectName,
										 task.SequenceId,
										 task.PageTypeId,
										 task.StartTime,
										 task.AssignedBy,
										 task.AssignedTo,
										 ModofiedOn = DateTime.UtcNow,
										 Lender = appRequest.LenderName,
										 Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
										 Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
										 //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
										 task.FHANumber
									 }).ToList();

			var form290AssignedTasks = (from closeRequest in submittedForm290
										join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
										   on closeRequest.TaskinstanceId equals task.TaskInstanceId
										select new
										{
											task.TaskInstanceId,
											task.TaskId,
											TaskName = closeRequest.projectName,
											task.SequenceId,
											task.PageTypeId,
											task.StartTime,
											task.AssignedBy,
											task.AssignedTo,
											ModofiedOn = DateTime.UtcNow,
											Lender = closeRequest.LenderName,
											Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
											Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                            //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                            DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                            LoanType = closeRequest.LoanType,
											task.FHANumber
										}).ToList();
			var allProductionTasks = fhaRequestTasks.Union(productionApTasks).Union(form290AssignedTasks).ToList();
			var groupedTasks = allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First());
			foreach (var item in groupedTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;// item.TaskName;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.Status = item.Status;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			olistPAMReportV3ViewModel = olistPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// Get Assigned Form290
		/// </summary>
		/// <param name="pStatus"></param>
		/// <returns></returns>
		public static IList<PAMReportV3ViewModel> ProdPamGetAssignedForm290(int pStatus)
		{
			IList<PAMReportV3ViewModel> olistPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
			IProductionQueueManager _productionQueue = new ProductionQueueManager();
			IEnumerable<TaskModel> productionTasks;
			IAppProcessManager appProcessManager = new AppProcessManager();
			IFHANumberRequestManager fhaRequestManager = new FHANumberRequestManager();
			int reqType = (int)PageType.Form290;
			productionTasks = _productionQueue.GetProductionTaskByType(reqType).ToList();
			//productionTasks = _productionQueue.GetProductionTaskByTypeAndStatus(reqType, pStatus).ToList();
			var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
			var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

			var applicationRequests = appProcessManager.GetAppRequests().ToList();
			var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
			List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
			if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
			{
				submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
			}

			var form290AssignedTasks = (from closeRequest in submittedForm290
										join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
										   on closeRequest.TaskinstanceId equals task.TaskInstanceId
										select new
										{
											task.TaskInstanceId,
											task.TaskId,
											TaskName = closeRequest.projectName,
											task.SequenceId,
											task.PageTypeId,
											task.StartTime,
											task.AssignedBy,
											task.AssignedTo,
											ModofiedOn = DateTime.UtcNow,
											Lender = closeRequest.LenderName,
											Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
											Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                            //DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                            DaysInQueue = (DateTime.UtcNow - task.StartTime).Days + " Day(s)",
                                            LoanType = closeRequest.LoanType,
											task.FHANumber
										}).ToList();

			foreach (var item in form290AssignedTasks)
			{
				PAMReportV3ViewModel objPAMReportV3ViewModel = new PAMReportV3ViewModel();
				objPAMReportV3ViewModel.ProjectName = fhaRequestManager.GetFhaRequestByFhaNumber(item.FHANumber).ProjectName + "-" + item.LoanType;// item.TaskName;
				objPAMReportV3ViewModel.FhaNumber = item.FHANumber;
				objPAMReportV3ViewModel.Status = item.Status;
				objPAMReportV3ViewModel.CurrentTaskInstanceId = item.TaskInstanceId;
				olistPAMReportV3ViewModel.Add(objPAMReportV3ViewModel);
			}
			olistPAMReportV3ViewModel = olistPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
			return olistPAMReportV3ViewModel;
		}

		/// <summary>
		/// prodpam - get 290 tasks
		/// </summary>
		/// <param name="appRequests"></param>
		/// <param name="taskInstanceId"></param>
		/// <returns></returns>
		public static IEnumerable<ProductionQueueLenderInfo> GetForm290Task(List<ProductionQueueLenderInfo> appRequests, List<Guid> taskInstanceId)
		{
			IProductionQueueManager productionQueue = new ProductionQueueManager();
			var form290Requests = (from form290 in productionQueue.GetForm290TaskByTaskInstanceID(taskInstanceId)
								   select new
								   {
									   form290.TaskInstanceID,
									   form290.ClosingTaskInstanceID

								   }
								   ).ToList();

			var unAssignedForm290Task = (from closingRequest in appRequests
										 join task in form290Requests
										 on closingRequest.TaskinstanceId equals task.ClosingTaskInstanceID
										 select new ProductionQueueLenderInfo()
										 {
											 ClosingInstanceID = task.ClosingTaskInstanceID,
											 TaskinstanceId = task.TaskInstanceID,
											 projectName = closingRequest.projectName,
											 LenderName = closingRequest.LenderName,
											 LoanType = closingRequest.LoanType,
											 LoanAmount = closingRequest.LoanAmount,
											 IsLIHTC = closingRequest.IsLIHTC,
											 CreatedBy = closingRequest.CreatedBy,
											 ModifiedOn = closingRequest.ModifiedOn

										 }).ToList();



			return unAssignedForm290Task;
		}

		/// <summary>
		/// extract integers from string
		/// </summary>
		/// <param name="pStr"></param>
		/// <returns></returns>
		public static int ExtractIntFromString(string pStr)
		{
			int outVal = 0;
			string splitPattern = @"[^\d]";
			if (string.IsNullOrEmpty(pStr))
				pStr = string.Empty;
			string[] results = Regex.Split(pStr, splitPattern);
			StringBuilder sb = new StringBuilder();
			string outS = string.Empty;
			foreach (string s in results)
			{
				sb.Append(s);
			}
			outS = sb.ToString();
			if (!string.IsNullOrEmpty(outS))
				outVal = Convert.ToInt32(outS);
			return outVal;
		}

		/// <summary>
		/// Get max last updated from Taskxref 
		/// </summary>
		/// <param name="pFhanum"></param>
		/// <param name="pTaskStep"></param>
		/// <returns></returns>
		public static DateTime GetTaskXrefLastupdated(Guid pTaskInstanceId, int pTaskStep)
		{
			DateTime lastModified = new DateTime();
			IProd_TaskXrefManager prod_TaskXrefManager = new Prod_TaskXrefManager();
			var lstProd_TaskXrefModel= prod_TaskXrefManager.GetProductionSubTasks(pTaskInstanceId);
			if (lstProd_TaskXrefModel.Count() > 0)
			{
				if (pTaskStep == (int)TaskStep.InProcess)
				{
					lastModified = lstProd_TaskXrefModel.OrderByDescending(t => t.ModifiedOn).First().ModifiedOn;
				}
				if (pTaskStep == (int)TaskStep.Complete || pTaskStep == (int)TaskStep.ProjectActionRequestComplete)
				{
					lastModified = lstProd_TaskXrefModel.OrderByDescending(t => t.CompletedOn).First().CompletedOn.Value;
					//if(lastModified == new DateTime())
					//	lastModified = lstProd_TaskXrefModel.OrderByDescending(t => t.ModifiedOn).First().ModifiedOn;

				}
			}
			return lastModified;
		}

		public static DateTime  LastUpdatedForm290ProdTaskByInstanceId(Guid pTaskInstanceId, int pStatus)
		{
			ITaskManager taskManager  = new  TaskManager();
			DateTime lastModified = new DateTime();
			var lstTaskModel = taskManager.GetTasksByTaskInstanceId(pTaskInstanceId).Where(o=>o.PageTypeId == (int)PageType.Form290);
			if (pStatus == (int)TaskStep.Form290Request)
			{
				lastModified = lstTaskModel.Where(o=>o.SequenceId==0).OrderByDescending(t => t.StartTime).First().StartTime;
			}
			if (pStatus == (int)TaskStep.Form290Complete)
			{
				lastModified = lstTaskModel.Where(o => (o.SequenceId == 1 || o.SequenceId == 0)).OrderByDescending(t => t.StartTime).First().StartTime;
			}
			return lastModified;
		}

		/// <summary>
		/// get folderkeys for non constructional draft and executed closing
		/// </summary>
		/// <returns></returns>
		public static List<int> GetProd_FolderStructureNCFolderKey()
		{
			List<int> lstNCFolderKeys = new List<int>();
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section00);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section1);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section2);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section3);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section4);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section5);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section6);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section7);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section8);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section9);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section10);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section11);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section12);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section13);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section14);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section15);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section17);
			lstNCFolderKeys.Add((int)Prod_FolderStructureNCFolderKey.Section18);
			return lstNCFolderKeys;
		}

		/// <summary>
		/// get folderkeys for app tasks
		/// </summary>
		/// <returns></returns>
		public static List<int> GetProd_FolderStructureAppFolderKey()
		{
			List<int> lstAppFolderKeys = new List<int>();
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section00);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section1);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section2);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section3);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section4);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section5);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section6);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section7);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section8);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section9);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section10);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section11);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section12);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section13);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section14);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section15);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section17);
			lstAppFolderKeys.Add((int)Prod_FolderStructureApplicationFolderKey.Section18);
			return lstAppFolderKeys;
		}

		/// <summary>
		/// Get start date in grid view under production q
		/// </summary>
		/// <param name="pTaskInstanceId"></param>
		/// <returns></returns>
		public static DateTime GetProdQStartDate(Guid pTaskInstanceId)
		{
			DateTime nextStartDate = new DateTime();
			ITaskManager taskManager = new TaskManager();
			IProd_NextStageManager prod_NextStageManager = new Prod_NextStageManager();
            //Commented as per Bug:4507
            //Guid? completedTaskInstanceId = prod_NextStageManager.GetCompletedStageTaskid(pTaskInstanceId);
			//if (completedTaskInstanceId.HasValue)
			//{
			//	var lstTaskModel = taskManager.GetTasksByTaskInstanceId(completedTaskInstanceId.Value).ToList();
			//	if (lstTaskModel != null && lstTaskModel.FirstOrDefault() != null && lstTaskModel.FirstOrDefault().StartTime != null)
			//		nextStartDate = lstTaskModel.FirstOrDefault().StartTime;
			//}

            var taskModel = taskManager.GetTasksByTaskInstanceId(pTaskInstanceId).ToList().Where(p=>p.SequenceId == 0).FirstOrDefault();
            if (taskModel != null && taskModel.StartTime != null)
                nextStartDate = taskModel.StartTime;
                return nextStartDate;
		}

		/// <summary>
		/// Create Signature
		/// </summary>
		/// <param name="pPath"></param>
		/// <param name="pSignature"></param>
		public static  void WriterSignatureFile(string pPath, byte[] pSignature)
		{
			using (FileStream fs = new FileStream(pPath, FileMode.Create))
			{
				using (BinaryWriter bw = new BinaryWriter(fs))
				{
					bw.Write(pSignature);
					bw.Close();
				}
			}
		}

		/// <summary>
		/// create folder in need
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static bool CreateFolderIfNeeded(string path)
		{
			bool result = true;
			if (!Directory.Exists(path))
			{
				try
				{
					Directory.CreateDirectory(path);
				}
				catch (Exception)
				{
					/*TODO: You must process this exception.*/
					result = false;
				}
			}
			return true;
		}

		/// <summary>
		/// delete file
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static bool DeleteFile(string pPath, string pFile)
		{
			bool result = true;
			System.IO.DirectoryInfo di = new DirectoryInfo(pPath);

			foreach (FileInfo file in di.GetFiles())
			{
				if(file.Name == pFile)
					file.Delete();
			}
			return true;
		}

		/// <summary>
		/// get fisical year
		/// </summary>
		/// <returns></returns>
		public static List<DateTime> GetFYDuration()
		{
			List<DateTime> oDates = new List<DateTime>();
			int year = 0;
			if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings["FisicalYear"], out year))
			{
				throw new InvalidOperationException("Invalid FisicalYear in web.config");
			}
			DateTime startDate = new DateTime(year, 10, 1);
			DateTime endDate = new DateTime(year + 1, 10, 1).AddDays(-1);
			oDates.Add(startDate);
			oDates.Add(endDate);
			return oDates;
		}

		/// <summary>
		/// Get amendment issued date 
		/// </summary>
		/// <param name="pTaskInstanceId"></param>
		/// <returns></returns>
		public static DateTime GetAmendmentIssueDate(Guid pTaskInstanceId)
		{
			ITaskManager taskManager = new TaskManager();
			DateTime ammendmentIssuedDate = new DateTime();
			ammendmentIssuedDate = taskManager.GetTasksByTaskInstanceId(pTaskInstanceId).First().StartTime;
			return ammendmentIssuedDate;
		}
		
		/// <summary>
		/// Get Lender Company name
		/// </summary>
		/// <param name="pTaskInstanceId"></param>
		/// <returns></returns>
		public static string GetLenderCompanyName(int pLenderId)
		{
			IAccountManager accountManager = new AccountManager();
			List<UserInfoModel> userInfoModels = new List<UserInfoModel>();
			userInfoModels = accountManager.GetLenderUsersByLenderId(pLenderId).ToList();
			return userInfoModels.FirstOrDefault().Lender_Name;
		}

		/// <summary>
		/// get firm commitmemt issued date
		/// </summary>
		/// <param name="pTaskInstanceId"></param>
		/// <returns></returns>
		public static DateTime GetFirmCommitmentIssuedDate(Guid pTaskInstanceId)
		{
			DateTime dtFromCommitment = new DateTime();
			ITaskManager taskManager = new TaskManager();
			List<TaskModel> lstTaskModel = new List<TaskModel>();
			string fhanum = taskManager.GetFHANumberByTaskInstanceId(pTaskInstanceId);
			lstTaskModel = taskManager.GetTasksByFHANumber(fhanum).ToList();
			lstTaskModel = lstTaskModel.Where(o => ((o.PageTypeId == (int)PageType.ProductionApplication) || (o.PageTypeId == (int)PageType.ClosingAllExceptConstruction)) &&
														(o.TaskStepId == (int)TaskStep.ProjectActionRequestComplete) &&
														(o.SequenceId == 1) &&
														(o.TaskOpenStatus != null && !string.IsNullOrEmpty(o.TaskOpenStatus))).ToList();
			if (lstTaskModel.Count > 0)
				dtFromCommitment = lstTaskModel.OrderByDescending(o => o.PageTypeId).FirstOrDefault().StartTime;


			return dtFromCommitment;
		}

		/// <summary>
		/// Get enum descriptions 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="e"></param>
		/// <returns></returns>
		public static string GetDescription<T>(this T e) where T : IConvertible
		{
			if (e is Enum)
			{
				Type type = e.GetType();
				Array values = System.Enum.GetValues(type);

				foreach (int val in values)
				{
					if (val == e.ToInt32(CultureInfo.InvariantCulture))
					{
						var memInfo = type.GetMember(type.GetEnumName(val));
						var descriptionAttribute = memInfo[0]
							.GetCustomAttributes(typeof(DescriptionAttribute), false)
							.FirstOrDefault() as DescriptionAttribute;

						if (descriptionAttribute != null)
						{
							return descriptionAttribute.Description;
						}
					}
				}
			}

			return null; // could also return string.Empty
		}


		/// <summary>
		/// get unused firm amendment num
		/// </summary>
		/// <param name="pFHANumber"></param>
		/// <returns></returns>
		public static int GetNewFirmAmendmentNum(string pFHANumber)
		{
			int firmNum = 1;
			IList<Prod_FormAmendmentTaskModel> olistProd_FormAmendmentTaskModel = new List<Prod_FormAmendmentTaskModel>();
			IProjectActionFormManager projectActionFormManager = new ProjectActionFormManager();
			olistProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentByFHANumber(pFHANumber);
			if (olistProd_FormAmendmentTaskModel.Count > 0)
			{
				firmNum = olistProd_FormAmendmentTaskModel.Max(o => o.FirmAmendmentNum);
				++firmNum;
			}
			return firmNum;
		}

		//testing
		//public static string GetLenderLastName(string  pLenderName)
		//{
		//	IAccountManager accountManager = new AccountManager();
		//	List<UserInfoModel> userInfoModels = new List<UserInfoModel>();
		//	var d = accountManager.GetUserByUsername(pLenderName);
		//	return userInfoModels.FirstOrDefault().Lender_Name;
		//}


		//public static void FindReplace(string documentLocation, string findText, string replaceText)
		//{
		//	var app = new Application();
		//	var doc = app.Documents.Open(documentLocation);

		//	var range = doc.Range();

		//	range.Find.Execute(FindText: findText, Replace: WdReplace.wdReplaceAll, ReplaceWith: replaceText);

		//	var shapes = doc.Shapes;

		//	foreach (Shape shape in shapes)
		//	{
		//		var initialText = shape.TextFrame.TextRange.Text;
		//		var resultingText = initialText.Replace(findText, replaceText);
		//		shape.TextFrame.TextRange.Text = resultingText;
		//	}

		//	doc.Save();
		//	doc.Close();

		//	Marshal.ReleaseComObject(app);
		//}


		public static IList<Prod_LoanCommitteeViewModel> FilterResultForLoanCommitteeScheduler(IList<Prod_LoanCommitteeViewModel> GridContent, LoanCommitteeFiltersModel Model)
        {
            if (Model.LoanCommitteeDate != null)
            {
                GridContent = GridContent.Where(x => x.LoanCommitteDate == Model.LoanCommitteeDate).ToList();
            }
            if (Model.Underwriter != null)
            {
                GridContent = GridContent.Where(x => x.Underwriter == Model.Underwriter).ToList();
            }
            if (Model.WorkloadManager != null)
            {
                GridContent = GridContent.Where(x => x.WLM == Model.WorkloadManager).ToList();
            }
            if (Model.SequenceNumber != null)
            {
                GridContent = GridContent.Where(x => x.LCDecision == Convert.ToInt32(Model.SequenceNumber)).ToList();
            }
            if (Model.Day != null)
            {
                GridContent = GridContent.Where(x => x.LoanCommitteDate.Value.ToString() == Model.Day).ToList();
            }
            if (Model.ProjectTypeName != null)
            {
                GridContent = GridContent.Where(x => x.ProjectType == Convert.ToUInt32(Model.ProjectTypeName)).ToList();
            }
            if (Model.OGCAttorney != null)
            {
                GridContent = GridContent.Where(x => x.Ogc == Model.OGCAttorney).ToList();
            }
            if (Model.Appraiser != null)
            {
                GridContent = GridContent.Where(x => x.Appraiser == Model.Appraiser).ToList();
            }
            if (Model.Lender != null)
            {
                GridContent = GridContent.Where(x => x.Lender == Model.Lender).ToList();
            }

            return GridContent;
        }
	}
}
