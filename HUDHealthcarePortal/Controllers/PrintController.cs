﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Routing;
using System.Windows.Forms.VisualStyles;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using Core.Utilities;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using System;
using System.Linq;
using System.Web.Mvc;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using Model;
using SpreadsheetGear;
using SpreadsheetGear.Drawing;

namespace HUDHealthcarePortal.Controllers
{
    [Authorize]
    public class PrintController : Controller
    {
        IUploadDataManager uploadDataMgr;
        private IAccountManager accountMgr;
        private IMissingProjectReportManager missingProjectMgr;
        private IPortfolioManager portfolioMgr;
        private IProjectReportsManager projectReportsMgr;
        private IPAMReportManager pamReportMgr;
        public PrintController()
            : this(new UploadDataManager(), new AccountManager(),
            new MissingProjectReportManager(), new PortfolioManager(),
            new ProjectReportsManager(), new PAMReportsManager())
        {
        }

        public PrintController(IUploadDataManager uploadDataManager, IAccountManager accountManager,
            IMissingProjectReportManager missingProjectReportManager, IPortfolioManager portfolioManager,
            IProjectReportsManager projectReportsManager, IPAMReportManager pamReportManager)
        {
            uploadDataMgr = uploadDataManager;
            accountMgr = accountManager;
            missingProjectMgr = missingProjectReportManager;
            portfolioMgr = portfolioManager;
            projectReportsMgr = projectReportsManager;
            pamReportMgr = pamReportManager;
        }

        public ActionResult PrintUploadAll()
        {
            ViewBag.PrintReportType = "PrintUploadAll";
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintProjectReport()
        {
            var userRoles = RoleManager.GetUserRoles(UserPrincipal.Current.UserName);
            var uploadedData = uploadDataMgr
                    .GetUploadDataBySearch(UserPrincipal.Current.UserId, userRoles).OrderBy(p => p.ProjectName);
            // if user is AE or WLM, filter out inaccessible fhas
            bool isUserAeOrWlm = RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName);
            // get all fha belong to the user
            List<string> fhasForAeOrWlm = null;
            if (isUserAeOrWlm)
            {
                var uploadMgr = new UploadDataManager();
                fhasForAeOrWlm = uploadMgr.GetAllowedFhasForUser(UserPrincipal.Current.UserId).ToList();
            }
            List<DerivedUploadData> uploadedDataFiltered = new List<DerivedUploadData>();

            foreach (var item in uploadedData)
            {
                if (!isUserAeOrWlm || (fhasForAeOrWlm != null && fhasForAeOrWlm.Contains(item.FHANumber)))
                    uploadedDataFiltered.Add(item);
            }

            GetPrintImageForProjectReport(uploadedDataFiltered);

            return View();
        }

        public ActionResult PrintUploadByTime(DateTime timestamp, int userId)
        {
            ViewBag.PrintReportType = "PrintUploadByTime";
            ViewBag.Timestamp = timestamp;
            ViewBag.UserId = userId;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintProjectReportByTime(DateTime timestamp, int userId)
        {
            var uploadedData = uploadDataMgr.GetUploadDataByTime(timestamp).Where(p => p.UserID == userId).OrderBy(p => p.ProjectName).ToList();

            GetPrintImageForProjectReport(uploadedData);

            return View();
        }
        public ActionResult PrintUploadByCategory(string category, string quarter)
        {
            ViewBag.PrintReportType = "PrintUploadByCategory";
            ViewBag.Category = category;
            ViewBag.Quarter = quarter;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintProjectReportByCategory(string category, string quarter)
        {
            var uploadedData = !string.IsNullOrEmpty(quarter) ? uploadDataMgr.GetUploadDataByCategoryAndQuarterBySearch(category, quarter).ToList()
                : uploadDataMgr.GetUploadDataByCategoryForProjectReport(category).Entities.ToList();
            uploadedData = uploadedData.OrderBy(p => p.ProjectName).ToList();

            GetPrintImageForProjectReport(uploadedData);

            return View();
        }

        public ActionResult PrintUploadByCategoryBySearch(string timestamp, string fhanumber, string category, string quarter)
        {
            ViewBag.PrintReportType = "PrintUploadByCategorySearch";
            ViewBag.Timestamp = timestamp;
            ViewBag.Fhanumber = fhanumber;
            ViewBag.Category = category;
            ViewBag.Quarter = quarter;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintProjectReportByCategorySearch(string timestamp, string fhanumber, string category,
            string quarter)
        {
            var uploadedData = new List<DerivedUploadData>();
            if (!string.IsNullOrEmpty(timestamp))
            {
                uploadedData = uploadDataMgr.GetUploadDataByTimeForSearch(Convert.ToDateTime(timestamp)).ToList();

            }
            else if (!string.IsNullOrEmpty(category))
            {
                uploadedData = !string.IsNullOrEmpty(quarter)
                     ? uploadDataMgr.GetUploadDataByCategoryAndQuarterBySearch(category,
                         quarter).ToList()
                     : uploadDataMgr.GetUploadDataByCategoryForProjectReport(category).Entities.ToList();
            }
            else
            {
                //var userRoles = Roles.GetRolesForUser(UserPrincipal.Current.UserName);
                var userRoles = RoleManager.GetUserRoles(UserPrincipal.Current.UserName);
                uploadedData = uploadDataMgr.GetUploadDataBySearch(UserPrincipal.Current.UserId, userRoles).ToList();
            }

            uploadedData = uploadedData.Where(p => p.FHANumber == fhanumber).OrderBy(p => p.ProjectName).ToList();

            GetPrintImageForProjectReport(uploadedData);
            return View();
        }

        public ActionResult PrintUploadStatusByLender(string strlenderId, string selectedQtr)
        {
            ViewBag.PrintReportType = "PrintUploadStatusByLender";
            ViewBag.LenderId = strlenderId;
            ViewBag.Quarter = selectedQtr;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintUploadStatusReportByLender(string strlenderId, string selectedQtr)
        {
            // if user is AE or WLM, filter out inaccessible lenders from all lenders
            bool isUserAeOrWlm = RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName);
            // get all lenders belong to the user
            List<int> lendersForAeOrWlm = null;
            if (isUserAeOrWlm)
            {
                lendersForAeOrWlm = accountMgr.GetLenderIdsByAeOrWlmForQtr(UserPrincipal.Current.UserName, selectedQtr).ToList();
            }

            int lenderId = int.Parse(strlenderId);
            var statuses = uploadDataMgr.GetUploadStatusDetailByQuarter(selectedQtr, lenderId).ToList();
            List<UploadStatusViewModel> statusesFiltered = new List<UploadStatusViewModel>();
            foreach (var item in statuses)
            {
                if (!isUserAeOrWlm || (lendersForAeOrWlm.Contains(item.LenderID)))
                    statusesFiltered.Add(item);
            }

            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForUploadStatus(cells);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForUploadStatus(statusesFiltered, cells);

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:D" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        public ActionResult PrintUploadStatusDetailByLender(string strlenderId, string selectedQtr)
        {
            ViewBag.PrintReportType = "PrintUploadStatusDetailByLender";
            ViewBag.LenderId = strlenderId;
            ViewBag.Quarter = selectedQtr;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintUploadStatusDetailReportByLender(string strlenderId, string selectedQtr)
        {
            int lenderId = int.Parse(strlenderId);
            var statuses = uploadDataMgr.GetUploadStatusDetailByLenderId(lenderId, selectedQtr).ToList();

            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForUploadStatusDetail(cells);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForUploadStatusDetail(statuses, cells);

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:F" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        public ActionResult PrintMissingProjectDetailByLender(string lenderId, string quarter)
        {
            ViewBag.PrintReportType = "PrintMissingProjectDetailByLender";
            ViewBag.LenderId = lenderId;
            ViewBag.Quarter = quarter;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintMissingProjectReportDetailByLender(string lenderId, string quarter)
        {
            int id = int.Parse(lenderId);
            var statuses = uploadDataMgr.GetMissingProjectsForLender(id, quarter).OrderBy(p => p.ProjectName).ToList();

            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForMissingProjectDetail(cells);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForMissingProjectDetail(statuses, cells);

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:B" + rowCount]);

            GetResponseImage(image);
            return View();
        }
        public ActionResult PrintUserUploadReport()
        {
            ViewBag.PrintReportType = "PrintMyUploadReport";
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintMyUploadReport()
        {
            var uploadedData = uploadDataMgr.GetDataUploadSummaryByUserId(UserPrincipal.Current.UserId).ToList();

            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForMyUploadReport(cells);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForMyUploadReport(uploadedData, cells);

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:E" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        public ActionResult PrintMissingProjectSuperUser(int year)
        {
            ViewBag.PrintReportType = "PrintMissingProjectSuperUser";
            ViewBag.Year = year;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintMissingProjectForSuperUser(int year)
        {
            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "SuperUser");

            var reportModel = missingProjectMgr.GetMissingProjectReport(HUDRole.SuperUser, UserPrincipal.Current.UserName, year);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "SuperUser");

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:D" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        public ActionResult PrintMissingProjectSuperUserWLM(int year)
        {
            ViewBag.PrintReportType = "PrintMissingProjectSuperUserWLM";
            ViewBag.Year = year;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintMissingProjectForSuperUserWLM(int year)
        {
            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "SuperUserWLM");

            var reportModel = missingProjectMgr.GetSuperUserMissingProjectReportWithWLM(UserPrincipal.Current.UserName, year);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "SuperUserWLM");

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:E" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        public ActionResult PrintMissingProjectWLM(int year, string userName)
        {
            ViewBag.PrintReportType = "PrintMissingProjectWLM";
            ViewBag.Year = year;
            ViewBag.Username = userName;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintMissingProjectForWLM(int year, string userName)
        {
            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "WLM");

            var reportModel = missingProjectMgr.GetWorkloadManagerMissingProjectReport(userName, year);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "WLM");

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:E" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        public ActionResult PrintMissingProjectAE(int year, string userName)
        {
            ViewBag.PrintReportType = "PrintMissingProjectAE";
            ViewBag.Year = year;
            ViewBag.Username = userName;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintMissingProjectForAE(int year, string userName)
        {
            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "AE");

            var reportModel = missingProjectMgr.GetAccountExecutiveMissingProjectReport(userName, year);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "AE");

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:D" + rowCount]);

            GetResponseImage(image);
            return View();
        }


        public ActionResult PrintPortfolioSummary(int? page, string sort, string sortDir, string quarter)
        {
            ViewBag.PrintReportType = "PrintPortfolioSummary";
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "page", page }, { "sort", sort }, { "sortdir", sortDir }, { "quarterToken", quarter } };

            routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_PORTFOLIO_SUMMARY_LINK_DICT];
            page = (int?)routeValuesDict["page"];
            sort = (string)routeValuesDict["sort"];
            sortDir = (string)routeValuesDict["sortdir"];
            quarter = (string)routeValuesDict["quarterToken"];

            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.SortDir = sortDir;
            ViewBag.Quarter = quarter;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintPortfolioSummaryReport(int? page, string sort, string sortDir, string quarter)
        {
            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "PortfolioSummary");

            var model = new PortfolioSummaryModel { YearQrtModel = new YearQuarterModel(4, quarter) };
            string yearQrtToken = string.IsNullOrEmpty(quarter)
                ? model.YearQrtModel.SelectedQrtToken
                : quarter;
            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            const int pageSize = 10;
            var portfolios = portfolioMgr.GetPortfolios(page ?? 1, pageSize, string.IsNullOrEmpty(sort) ? "Portfolio_Name" : sort,
                 sortOrder, yearQrtToken);

            int rowCount = ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(portfolios.Entities.ToList(), cells, "PortfolioSummary");

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:AE" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        public ActionResult PrintPortfolioDetail(int? page, string sort, string sortDir, string portfolioNum, string quarter)
        {
            ViewBag.PrintReportType = "PrintPortfolioDetail";
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.SortDir = sortDir;
            ViewBag.PortfolioNum = portfolioNum;
            ViewBag.Quarter = quarter;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintPortfolioDetailReport(int? page, string sort, string sortDir, string portfolioNum, string quarter)
        {
            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "PortfolioDetail");

            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            const int pageSize = 10;
            var portfolioDetail = portfolioMgr.GetPortfolioDetail(page ?? 1, pageSize, string.IsNullOrEmpty(sort) ? "ProjectName" : sort, sortOrder, portfolioNum, quarter);

            int rowCount = ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(portfolioDetail.Entities.ToList(), cells, "PortfolioDetail");

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:AK" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        public ActionResult PrintProjectReportSuperUser(DateTime minUploadDate, DateTime maxUploadDate)
        {
            ViewBag.PrintReportType = "PrintProjectReport";
            ViewBag.ReportLevel = "SuperUser";
            ViewBag.WlmId = "";
            ViewBag.AeId = "";
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintProjectReportWLM(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_PROJECTS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];

            ViewBag.PrintReportType = "PrintProjectReport";
            ViewBag.ReportLevel = "WLM";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = "";
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintProjectReportAE(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_PROJECTS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];

            ViewBag.PrintReportType = "PrintProjectReport";
            ViewBag.ReportLevel = "AE";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = aeId;
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintProjectReportDetail(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_PROJECTS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];
            var lenderId = (string)routeValuesDict["lenderId"];

            ViewBag.PrintReportType = "PrintProjectReport";
            ViewBag.ReportLevel = "Detail";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = aeId;
            ViewBag.LenderId = lenderId;
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintProjectReportReport(string reportLevel, string wlmId, string aeId, string lenderId, string minUploadDate, string maxUploadDate)
        {
            var userName = UserPrincipal.Current.UserName;
            ProjectReportModel reportModel;

            switch (reportLevel)
            {
                case "SuperUser":
                    reportModel = projectReportsMgr.GetSuperUserProjectSummary(userName, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate));
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, null, "Workload Manager");
                    break;
                case "WLM":
                    reportModel = projectReportsMgr.GetWLMProjectSummary(userName, wlmId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate));
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, null, "Account Executive");
                    break;
                case "AE":
                    reportModel = projectReportsMgr.GetAEProjectSummary(userName, wlmId, aeId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate));
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, null, "Lender");
                    break;
                case "Detail":
                    reportModel = projectReportsMgr.GetProjectLenderDetail(userName, wlmId, aeId, lenderId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate));
                    GetProjectDetailSpreadSheet(reportModel, minUploadDate, maxUploadDate, null);
                    break;
                default:
                    reportModel = new ProjectReportModel(ReportType.ProjectReport);
                    GetProjectDetailSpreadSheet(reportModel, minUploadDate, maxUploadDate, null);
                    break;
            }

            return View();
        }

        public ActionResult PrintLargeLoanReportSuperUser(DateTime minUploadDate, DateTime maxUploadDate)
        {
            ViewBag.PrintReportType = "PrintHighLoanProjectReport";
            ViewBag.ReportLevel = "SuperUser";
            ViewBag.WlmId = "";
            ViewBag.AeId = "";
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();

            return View("~/Views/Print/PrintReports.cshtml");
        }


        public ActionResult PrintLargeLoanReportWLM(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];

            ViewBag.PrintReportType = "PrintHighLoanProjectReport";
            ViewBag.ReportLevel = "WLM";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = "";
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintLargeLoanReportAE(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];

            ViewBag.PrintReportType = "PrintHighLoanProjectReport";
            ViewBag.ReportLevel = "AE";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = aeId;
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintLargeLoanReportDetail(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];
            var lenderId = (string)routeValuesDict["lenderId"];

            ViewBag.PrintReportType = "PrintHighLoanProjectReport";
            ViewBag.ReportLevel = "Detail";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = aeId;
            ViewBag.LenderId = lenderId;
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintHighLoanProjectReportReport(string reportLevel, string wlmId, string aeId, string lenderId, string minUploadDate, string maxUploadDate)
        {
            var userName = UserPrincipal.Current.UserName;
            ProjectReportModel reportModel;

            switch (reportLevel)
            {
                case "SuperUser":
                    reportModel = projectReportsMgr.GetSuperUserLargeLoanSummary(userName, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate));
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, null, "Workload Manager");
                    break;
                case "WLM":
                    reportModel = projectReportsMgr.GetWLMLargeLoanSummary(userName, wlmId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate));
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, null, "Account Executive");
                    break;
                case "AE":
                    reportModel = projectReportsMgr.GetAELargeLoanSummary(userName, wlmId, aeId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate));
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, null, "Lender");
                    break;
                case "Detail":
                    reportModel = projectReportsMgr.GetLargeLoanLenderDetail(userName, wlmId, aeId, lenderId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate));
                    GetProjectDetailSpreadSheet(reportModel, minUploadDate, maxUploadDate, null);
                    break;
                default:
                    reportModel = new ProjectReportModel(ReportType.LargeLoanReport);
                    GetProjectDetailSpreadSheet(reportModel, minUploadDate, maxUploadDate, null);
                    break;
            }

            return View();
        }

        public ActionResult PrintRatioExceptionsReportSuperUser(DateTime minUploadDate, DateTime maxUploadDate,
            bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable,
            bool isAvgPaymentPeriod)
        {
            ViewBag.PrintReportType = "PrintRatioExceptionProjectReport";
            ViewBag.ReportLevel = "SuperUser";
            ViewBag.WlmId = "";
            ViewBag.AeId = "";
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();
            ViewBag.IsDebtCoverageRatio = isDebtCoverageRatio;
            ViewBag.IsDaysCashOnHand = isDaysCashOnHand;
            ViewBag.IsAvgPaymentPeriod = isAvgPaymentPeriod;
            ViewBag.IsWorkingCapital = isWorkingCapital;
            ViewBag.IsDaysInAcctReceivable = isDaysInAcctReceivable;

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintRatioExceptionsReportWLM(DateTime minUploadDate, DateTime maxUploadDate,
            bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable,
            bool isAvgPaymentPeriod)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];

            ViewBag.PrintReportType = "PrintRatioExceptionProjectReport";
            ViewBag.ReportLevel = "WLM";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = "";
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();
            ViewBag.IsDebtCoverageRatio = isDebtCoverageRatio;
            ViewBag.IsDaysCashOnHand = isDaysCashOnHand;
            ViewBag.IsAvgPaymentPeriod = isAvgPaymentPeriod;
            ViewBag.IsWorkingCapital = isWorkingCapital;
            ViewBag.IsDaysInAcctReceivable = isDaysInAcctReceivable;

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintRatioExceptionsReportAE(DateTime minUploadDate, DateTime maxUploadDate,
            bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable,
            bool isAvgPaymentPeriod)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];

            ViewBag.PrintReportType = "PrintRatioExceptionProjectReport";
            ViewBag.ReportLevel = "AE";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = aeId;
            ViewBag.LenderId = "";
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();
            ViewBag.IsDebtCoverageRatio = isDebtCoverageRatio;
            ViewBag.IsDaysCashOnHand = isDaysCashOnHand;
            ViewBag.IsAvgPaymentPeriod = isAvgPaymentPeriod;
            ViewBag.IsWorkingCapital = isWorkingCapital;
            ViewBag.IsDaysInAcctReceivable = isDaysInAcctReceivable;

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintRatioExceptionsReportDetail(DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT];
            var lenderId = (string)routeValuesDict["lenderId"];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];

            ViewBag.PrintReportType = "PrintRatioExceptionProjectReport";
            ViewBag.ReportLevel = "Detail";
            ViewBag.WlmId = wlmId;
            ViewBag.AeId = aeId;
            ViewBag.LenderId = lenderId;
            ViewBag.MinUploadDate = minUploadDate.ToShortDateString();
            ViewBag.MaxUploadDate = maxUploadDate.ToShortDateString();
            ViewBag.IsDebtCoverageRatio = isDebtCoverageRatio;
            ViewBag.IsDaysCashOnHand = isDaysCashOnHand;
            ViewBag.IsAvgPaymentPeriod = isAvgPaymentPeriod;
            ViewBag.IsWorkingCapital = isWorkingCapital;
            ViewBag.IsDaysInAcctReceivable = isDaysInAcctReceivable;

            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintRatioExceptionProjectReportReport(string reportLevel, string wlmId, string aeId, string lenderId, string minUploadDate, string maxUploadDate, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            var userName = UserPrincipal.Current.UserName;
            ProjectReportModel reportModel;

            List<String> selectedRatioExceptions = ExcelAndPrintHelper.GetSelectedRatioExceptions(isDebtCoverageRatio, isWorkingCapital,
                isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            switch (reportLevel)
            {
                case "SuperUser":
                    reportModel = projectReportsMgr.GetSuperUserRatioExceptionSummary(userName, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, selectedRatioExceptions, "Workload Manager");
                    break;
                case "WLM":
                    reportModel = projectReportsMgr.GetWLMRatioExceptionSummary(userName, wlmId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, selectedRatioExceptions, "Account Executive");
                    break;
                case "AE":
                    reportModel = projectReportsMgr.GetAERatioExceptionSummary(userName, wlmId, aeId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
                    GetProjectSummarySpreadSheet(reportModel, minUploadDate, maxUploadDate, selectedRatioExceptions, "Lender");
                    break;
                case "Detail":
                    reportModel = projectReportsMgr.GetRatioExceptionLenderDetail(userName, wlmId, aeId, lenderId, DateTime.Parse(minUploadDate), DateTime.Parse(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
                    GetProjectDetailSpreadSheet(reportModel, minUploadDate, maxUploadDate, selectedRatioExceptions);
                    break;
                default:
                    reportModel = new ProjectReportModel(ReportType.LargeLoanReport);
                    GetProjectDetailSpreadSheet(reportModel, minUploadDate, maxUploadDate, selectedRatioExceptions);
                    break;
            }

            return View();
        }

        public void GetProjectDetailSpreadSheet(ProjectReportModel projectReportDetail, string minUploadDate, string maxUploadDate, List<String> selectedRatioExceptions)
        {
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectReportDetail");

            projectReportDetail.MinUploadDate = minUploadDate;
            projectReportDetail.MaxUploadDate = maxUploadDate;

            int headerRows = ExcelAndPrintHelper.GetHeaderValuesForProjectDetail(projectReportDetail, selectedRatioExceptions, cells);
            int rowCount = ExcelAndPrintHelper.SetDataValuestoExcelForProjectDetailReport(projectReportDetail, headerRows, cells);

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:N" + rowCount]);

            GetResponseImage(image);
        }

        public void GetProjectSummarySpreadSheet(ProjectReportModel projectReportSummary, string minUploadDate, string maxUploadDate, List<String> selectedRatioExceptions, string userLevel)
        {
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectReportSummary");

            projectReportSummary.MinUploadDate = minUploadDate;
            projectReportSummary.MaxUploadDate = maxUploadDate;

            int headerRows = ExcelAndPrintHelper.GetHeaderValuesForProjectSummary(projectReportSummary, userLevel, selectedRatioExceptions, cells);
            int rowCount = ExcelAndPrintHelper.SetDataValuestoExcelForProjectSummaryReport(projectReportSummary, userLevel, headerRows, cells);

            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:I" + rowCount]);

            GetResponseImage(image);
        }

        public ActionResult PrintPAMReport(string aeIds, DateTime? fromDate, DateTime? toDate, string status)
        {
            ViewBag.PrintReportType = "PrintPAMReport";
            ViewBag.AeIds = aeIds;
            ViewBag.FromDate = fromDate;
            ViewBag.ToDate = toDate;
            ViewBag.Status = status;
            return View("~/Views/Print/PrintReports.cshtml");
        }

        public ActionResult PrintPAMReports(string aeIds, string isouIds, DateTime? fromDate, DateTime? toDate, string status, string projectAction, int? page, string sort, string sortdir, string wlmIds)
        {
            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;

            ExcelAndPrintHelper.GetHeaderValuesForPAMReport(cells);
            int wlmId = 0, aeId = 0, isouId = 0;
            if (RoleManager.IsWorkloadManagerRole(UserPrincipal.Current.UserName))
            {
                wlmId = pamReportMgr.GetWlmId(UserPrincipal.Current.UserId);
            }
            else if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
            {
                aeId = pamReportMgr.GetAeId(UserPrincipal.Current.UserId);
                aeIds = aeId.ToString(CultureInfo.InvariantCulture);
            }
            else if (RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName))
            {
                isouId = pamReportMgr.GetIsouId(UserPrincipal.Current.UserId);
                isouIds = isouId.ToString(CultureInfo.InvariantCulture);
            }
            int? requestStatus = null;
            if (status == "0" || status == "1")
            {
                requestStatus = int.Parse(status);
            }

            //int? requestProjectAction = null;
            //if (projectAction == "0" || projectAction == "1")
            //{
            //    requestProjectAction = int.Parse(projectAction);
            //}

            string requestProjectAction = projectAction;

            var reportModel = pamReportMgr.
                GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds, fromDate, toDate, wlmId,
                aeId, isouId, requestStatus, requestProjectAction, 0, sort, string.IsNullOrEmpty(sortdir)
                    ? EnumUtils.Parse<SqlOrderByDirecton>("ASC")
                    : EnumUtils.Parse<SqlOrderByDirecton>(sortdir), wlmIds
                    , "", "", "", 1, 66);//karri:D#610

            //var reportModel = pamReportMgr.GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds, fromDate, toDate, wlmId,
            //    aeId,isouId, requestStatus, requestProjectAction, 0, sort, string.IsNullOrEmpty(sortdir)
            //        ? EnumUtils.Parse<SqlOrderByDirecton>("ASC")
            //        : EnumUtils.Parse<SqlOrderByDirecton>(sortdir),wlmIds);

            int rowCount = ExcelAndPrintHelper.SetDataValuesForPAMReport(reportModel.PAMReportGridModel.Entities.ToList(), cells);

            // Create Image class, passing in desired range to render 
            var image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:N" + rowCount]);

            GetResponseImage(image);
            return View();
        }

        private void GetResponseImage(Image image)
        {
            // Create Bitmap of range
            using (System.Drawing.Bitmap bitmap = image.GetBitmap())
            {
                // Stream the image to the client in PNG format.
                Response.Clear();
                Response.ContentType = "image/png";
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                memoryStream.WriteTo(Response.OutputStream);
                Response.End();
            }
        }

        private void GetPrintImageForProjectReport(List<DerivedUploadData> uploadedData)
        {
            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var worksheet = workbook.Worksheets["Sheet1"];
            var cells = worksheet.Cells;
            ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "ProjectReport");

            int rowCount = ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(uploadedData, cells, "ProjectReport");

            // Create Image class, passing in desired range to render 
            SpreadsheetGear.Drawing.Image image;
            if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName))
            {
                image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:AK" + rowCount]);
            }
            else
            {
                image = new SpreadsheetGear.Drawing.Image(worksheet.Cells["A1:AE" + rowCount]);
            }
            GetResponseImage(image);
        }

        public ActionResult PrintMultiLoanReport()
        {
            var acctMgr = new AccountManager();
            var model = acctMgr.GetMultipleLoanProperty();
            return View("PrintMultiLoansReport", model);
        }
    }
}
