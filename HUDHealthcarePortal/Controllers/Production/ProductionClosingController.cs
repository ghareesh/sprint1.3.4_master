﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;
using BusinessService.AssetManagement;
using BusinessService;
using Model;
using Model.Production;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using BusinessService.ManagementReport;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class ProductionClosingController : Controller
    {
        private IProjectActionFormManager projectActionFormManager;
        private IProjectActionManager projectActionManager;
        private IGroupTaskManager groupTaskManager;
        private ITaskManager taskManager;
        private IAccountManager accountManager;
        private IEmailManager emailManager;
        private IBackgroundJobMgr backgroundJobManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager; // User Story 1901
        private IParentChildTaskManager parentChildTaskManager;
        private IRequestAdditionalInfoFileManager requestAdditionalInfoFilesManager;
        private IReviewFileCommentManager reviewFileCommentManager;
        private IReviewFileStatusManager reviewFileStatusManager;
        private IProd_GroupTasksManager prod_GroupTasksManager;
        private ITaskFile_FolderMappingManager taskFile_FolderMappingManager;
        private IFHANumberRequestManager fhaRequestManager;
        private IAppProcessManager appProcessManager;
        private IProd_TaskXrefManager prod_TaskXrefManager;

        private IOPAManager iOPAForm;
        IPAMReportManager _plmReportMgr;
        IWebSecurityWrapper webSecurity;
        private IProductionQueueManager productionQueue;
        private IProd_NextStageManager prod_NextStageManager;



        public ProductionClosingController()
            : this(
                new ProjectActionFormManager(), new TaskManager(), new AccountManager(), new EmailManager(),
                new BackgroundJobMgr(), new ProjectActionManager(), new GroupTaskManager(),
                new NonCriticalRepairsRequestManager(), new ParentChildTaskManager(),
                new RequestAdditionalInfoFilesManager(), new ReviewFileCommentManager(), new ReviewFileStatusManager(), new WebSecurityWrapper(), new OPAManager(), new Prod_GroupTasksManager(), new TaskFile_FolderMappingManager(), new FHANumberRequestManager(), new PAMReportsManager(), new AppProcessManager(), new Prod_TaskXrefManager(), new ProductionQueueManager(), new Prod_NextStageManager())
        {

        }

        private ProductionClosingController(IProjectActionFormManager _projectActionFormManager, ITaskManager _taskManager,
          IAccountManager _accountManager, IEmailManager _emailManager, IBackgroundJobMgr backgroundManager,
          IProjectActionManager _projectActionManager, IGroupTaskManager _groupTaskManager,
          INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
          IParentChildTaskManager _parentChildTaskManager,
          IRequestAdditionalInfoFileManager _requestAdditionalInfoFilesManager,
          IReviewFileCommentManager _reviewFileCommentManager,
           IReviewFileStatusManager _reviewFileStatusManager,
          IWebSecurityWrapper webSec,
          IOPAManager _opaManager,
          IProd_GroupTasksManager _prod_GroupTasksManager, ITaskFile_FolderMappingManager _taskFile_FolderMappingManager, FHANumberRequestManager _fhaRequestManager, IPAMReportManager plmReportMgr, IAppProcessManager _appProcessManager, IProd_TaskXrefManager _prodTaskXrefManager, IProductionQueueManager _productionQueueManager, IProd_NextStageManager _prod_NextStageManager)
        {
            projectActionFormManager = _projectActionFormManager;
            taskManager = _taskManager;
            accountManager = _accountManager;
            emailManager = _emailManager;
            backgroundJobManager = backgroundManager;
            projectActionManager = _projectActionManager;
            groupTaskManager = _groupTaskManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager; // User Story 1901
            parentChildTaskManager = _parentChildTaskManager;
            requestAdditionalInfoFilesManager = _requestAdditionalInfoFilesManager;
            reviewFileCommentManager = _reviewFileCommentManager;
            reviewFileStatusManager = _reviewFileStatusManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;// User Story 1901
            webSecurity = webSec;
            iOPAForm = _opaManager;
            prod_GroupTasksManager = _prod_GroupTasksManager;
            taskFile_FolderMappingManager = _taskFile_FolderMappingManager;
            fhaRequestManager = _fhaRequestManager;
            _plmReportMgr = plmReportMgr;
            appProcessManager = _appProcessManager;
            prod_TaskXrefManager = _prodTaskXrefManager;
            productionQueue = _productionQueueManager;
            prod_NextStageManager = _prod_NextStageManager;

        }

        private void InitializeViewModel(OPAViewModel AppProcessViewModel, bool IsIRR)
        {
            ViewBag.SaveCheckListFormURL = Url.Action("ApproveCheckListForm");

            AppProcessViewModel.IsEditMode = false;
            var projectActionList = appProcessManager.GetAllotherloanTypes();
            if (IsIRR)
            {
                foreach (var model in projectActionList)
                {
                    AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
                    {
                        Text = model.ProjectTypeName,
                        Value = model.ProjectTypeId.ToString()
                    });
                }
            }

            AppProcessViewModel.LenderId = UserPrincipal.Current.UserData.LenderId.Value;
            AppProcessViewModel.LenderName = projectActionFormManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            AppProcessViewModel.RequesterName = UserPrincipal.Current.FullName;
            AppProcessViewModel.ProjectActionDate = DateTime.Today;
            AppProcessViewModel.ServicerSubmissionDate = null;
            AppProcessViewModel.IsAgreementAccepted = false;
            AppProcessViewModel.RequestDate = DateTime.UtcNow;
            AppProcessViewModel.CreatedBy = UserPrincipal.Current.UserId;
            AppProcessViewModel.CreatedOn = DateTime.UtcNow;
            //Initiate the new grouptaskinstanceid
            AppProcessViewModel.GroupTaskInstanceId = Guid.NewGuid();
            AppProcessViewModel.ModifiedOn = DateTime.UtcNow;
            AppProcessViewModel.ModifiedBy = UserPrincipal.Current.UserId;
            IList<TaskFileModel> TasFilelist = new List<TaskFileModel>();
            AppProcessViewModel.TaskFileList = TasFilelist;
            AppProcessViewModel.IsViewFromGroupTask = false;
            List<OPAHistoryViewModel> History = new List<OPAHistoryViewModel>();
            AppProcessViewModel.OpaHistoryViewModel = History;
            // User Story 1901
            GetDisclaimerText(AppProcessViewModel);

        }

        private void GetDisclaimerText(OPAViewModel model)
        {
            model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("APPLICATION REQUEST");
        }


        public ActionResult ClosingForAllExceptConstruction()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.IsIRRequest = false;
            AppProcessViewModel.Isotherloantype = false;
            InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            AppProcessViewModel.AvailableFHANumbersList = prod_NextStageManager.GetFhasforNxtStage((int)PageType.ExecutedClosing, (int)UserPrincipal.Current.LenderId);
            ViewBag.ConstructionTitle = "Non-Construction Executed Closing";
            AppProcessViewModel.pageTypeId = (int)PageType.ExecutedClosing;
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            AppProcessViewModel.TransAccessStatus = checkResult.status;
            return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
        }

        public ActionResult DraftClosing()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.IsIRRequest = false;
            AppProcessViewModel.Isotherloantype = false;
            InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            AppProcessViewModel.AvailableFHANumbersList = prod_NextStageManager.GetFhasforNxtStage((int)PageType.ClosingAllExceptConstruction, (int)UserPrincipal.Current.LenderId);
            ViewBag.ConstructionTitle = "Non-Construction Draft Closing";
            AppProcessViewModel.FormName = "Non-Construction Draft Closing";
            AppProcessViewModel.pageTypeId = (int)PageType.ClosingAllExceptConstruction;
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            AppProcessViewModel.TransAccessStatus = checkResult.status;
            return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
        }

        //public ActionResult Amendments()
        //{
        //    OPAViewModel AppProcessViewModel = new OPAViewModel();
        //    AppProcessViewModel.IsIRRequest = false;
        //    AppProcessViewModel.Isotherloantype = false;
        //    InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
        //    AppProcessViewModel.AvailableFHANumbersList = prod_NextStageManager.GetFhasforNxtStage((int)PageType.Amendments, (int)UserPrincipal.Current.LenderId);
        //    ViewBag.ConstructionTitle = "Amendments";
        //    AppProcessViewModel.FormName = "Amendments";
        //    AppProcessViewModel.pageTypeId = (int)PageType.Amendments;
        //    Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
        //    AppProcessViewModel.TransAccessStatus = checkResult.status;
        //    return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
        //}

        public ActionResult ClosingForConstructionInsuredAdvances()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.IsIRRequest = false;
            AppProcessViewModel.Isotherloantype = false;
            InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            AppProcessViewModel.AvailableFHANumbersList = prod_NextStageManager.GetFhasforNxtStage((int)PageType.ClosingConstructionInsuredAdvances, (int)UserPrincipal.Current.LenderId);
            ViewBag.ConstructionTitle = "Closing Request (Construction Initial Closing)";
            AppProcessViewModel.pageTypeId = (int)PageType.ClosingConstructionInsuredAdvances;
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            AppProcessViewModel.TransAccessStatus = checkResult.status;
            return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
        }

        public ActionResult ClosingforInsuredUponCompletion()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.IsIRRequest = false;
            AppProcessViewModel.Isotherloantype = false;
            InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            AppProcessViewModel.AvailableFHANumbersList = prod_NextStageManager.GetFhasforNxtStage((int)PageType.ClosingConstructionInsuranceUponCompletion, (int)UserPrincipal.Current.LenderId);
            ViewBag.ConstructionTitle = "Closing Request (Construction Final Closing)";
            AppProcessViewModel.pageTypeId = (int)PageType.ClosingConstructionInsuranceUponCompletion;
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            AppProcessViewModel.TransAccessStatus = checkResult.status;
            return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
        }

    }
}
