﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessService.Interfaces;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using Model.Production;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.BusinessService.Interfaces;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;
using HUDHealthcarePortal.Helpers;
using BusinessService.Interfaces.InternalExternalTask;
using BusinessService.InternalExternalTask;
using System.Collections;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class ProductionMyTaskController : Controller
    {
        //
        // GET: /ProductionMyTask/
        private IProductionQueueManager _productionQueueManager;
        private IProd_TaskXrefManager _prodTaskXrefManager;
        private ITaskManager _taskManager;
        private IFHANumberRequestManager _fhaNumberRequestManager;
        private IParentChildTaskManager _parentChildTaskManager;
        private IProjectActionFormManager _projectActionFormManager;
        private IAccountManager accountManager;
        private IInternalExternalTaskManager internalExternalTaskManger;//skumar-form290
        private IEmailManager _emailManager = null;//hareeshnew
        private IBackgroundJobMgr _backgroundManager = null;


        public ProductionMyTaskController()
            : this(new ProjectActionFormManager(), new ParentChildTaskManager(), new ProductionQueueManager(), new Prod_TaskXrefManager(), new TaskManager(), new FHANumberRequestManager(), new AccountManager(), new InternalExternalTaskManager(), new EmailManager(), new BackgroundJobMgr())
        {
            
        }

        public ProductionMyTaskController(IProjectActionFormManager projectActionFormManager, IParentChildTaskManager parentChildTaskManager, IProductionQueueManager productionQueueManager, IProd_TaskXrefManager prodTaskXrefManager,
                                          ITaskManager taskManager,IFHANumberRequestManager fhaNumberRequestManager, IAccountManager _accountManager, IInternalExternalTaskManager _internalExternalTaskManager, IEmailManager emailManager, IBackgroundJobMgr backgroundManager)
        {
            _emailManager = emailManager;
            _backgroundManager = backgroundManager;

            _projectActionFormManager = projectActionFormManager;
            _parentChildTaskManager = parentChildTaskManager;
           _productionQueueManager = productionQueueManager;
            _prodTaskXrefManager = prodTaskXrefManager;
            _taskManager = taskManager;
            _fhaNumberRequestManager = fhaNumberRequestManager;
            accountManager = _accountManager;
            internalExternalTaskManger = _internalExternalTaskManager;
        }
        public ActionResult Index(string projName = null, string prodTaskType = null, string FHANumbers = null)
        {
            #region  Filling Project Names for Search
            //karri#205
            ArrayList alProjNames = new ArrayList();
            ArrayList alProdTaskTypes = new ArrayList();
            var currentUser = UserPrincipal.Current;
            //Added by siddesh
            ArrayList alFHAnumbers = new ArrayList();
            //Added by siddesh
            IList<String> iFHAnumbersList = _taskManager.getFHAList(UserPrincipal.Current.UserName);
            if (FHANumbers != null && !string.IsNullOrEmpty(FHANumbers))
                _taskManager.setLookUpValues(FHANumbers);
            var productionTasks = _productionQueueManager.GetProductionTaskByUserName(currentUser.UserName, currentUser.UserRole)
                               .OrderByDescending(m => m.ProductionTaskType[0]).ThenByDescending(m => m.LastUpdated);
            if (productionTasks != null)
                foreach (var item in productionTasks)
                {
                    if (!alProjNames.Contains(item.ProjectName))
                        alProjNames.Add(item.ProjectName);

                    if (!alProdTaskTypes.Contains(item.ProductionTaskType))
                        alProdTaskTypes.Add(item.ProductionTaskType);
                    //Added by siddesh
                    if (!alFHAnumbers.Contains(item.TaskName))
                    {
                        if (item.TaskName.Contains("-"))
                        {
                            alFHAnumbers.Add(item.TaskName.Substring(item.TaskName.IndexOf("(") + 1).Substring(0, 9));
                        }
                    }
                }

            IList<String> iProjNamesList = new List<string>();
            IList<String> iProdTypesList = new List<string>();
            //Added by siddesh #265
            //IList<string> iFHAnumbersList = new List<string>();
            alProjNames.Sort();//ASC
            foreach (string value in alProjNames)
                iProjNamesList.Add(value);
            ViewBag.ProjNamesList = iProjNamesList;

            alProdTaskTypes.Sort();//ASC
            foreach (string value in alProdTaskTypes)
                iProdTypesList.Add(value);
            ViewBag.ProdTypeList = iProdTypesList;

            @ViewData["BindingProjName"] = projName;
            //Added by sidddesh #265
            alFHAnumbers.Sort();//ASC
            foreach (string value in alFHAnumbers)
            {
                if (value.Contains("-"))
                {
                    iFHAnumbersList.Add(value);
                }
            }
            ViewBag.FHAnumberList = iFHAnumbersList;

            @ViewData["BindingFHANumber"] = FHANumbers;

            //assing the searching project name to global variable so that it can be used in the result set fetching functions
            if (projName != null && !string.IsNullOrEmpty(projName))
                TempData["projName"] = projName;
            if (prodTaskType != null && !string.IsNullOrEmpty(prodTaskType))
            {
                //for some reason the string 'FHA# REQUEST' is coming as 'FHA', hence this fix
                if (prodTaskType.Equals("FHA"))
                    prodTaskType = "FHA# REQUEST";

                @ViewData["BindingProdTaskType"] = prodTaskType;

                TempData["prodTaskType"] = prodTaskType;
            }
            //Added by siddesh #265 /
            if (FHANumbers != null && !string.IsNullOrEmpty(FHANumbers))
                TempData["FHANumbers"] = FHANumbers;
            //End of filling search for Proj names
            #endregion

            var userName = UserPrincipal.Current.UserName;
            if (RoleManager.IsUserLenderRole(userName) || RoleManager.IsInspectionContractor(userName))
            {
              
                return View("~/Views/Production/ProductionMyTask/MyTaskLenderView.cshtml");
            }
           
            return View("~/Views/Production/ProductionMyTask/Index.cshtml");
        }

        public JsonResult GetMyTasks(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var currentUser = UserPrincipal.Current;
            int form290Request = (int)TaskStep.Form290Request;
            int form290Complete = (int)TaskStep.Form290Complete;
            //var model = new ProductionMyTaskModel();

            //karri#205:commented as result set depends on search key words implemented in the new function getMyFilteredTasks()
            //var productionTasks = _productionQueueManager.GetProductionTaskByUserName(currentUser.UserName, currentUser.UserRole)
            //                    .OrderByDescending(m=>m.ProductionTaskType[0]).ThenByDescending(m=>m.LastUpdated);            

            //karri#205;new methods to return result sets ased on search operations
            var productionTasks = getMyFilteredTasks(currentUser.UserName, currentUser.UserRole).OrderByDescending(m => m.ProductionTaskType[0]);
            // #234
           ArrayList al90Days = new ArrayList();
            List<ProductionMyTaskModel> pm = new List<ProductionMyTaskModel>();
          
            //also required taskname,projname, duration
            //var model = new ProductionMyTaskModel();
         // model = productionTasks.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.groupid).SelectMany(gr => gr).OrderBy(o => o.orderby);

            if (productionTasks != null)
            {
                foreach (var prodtype in productionTasks)
                {
                    var pdmy = new ProductionMyTaskModel();
                    pdmy.AssignedTo = prodtype.AssignedTo;
                    pdmy.Duration = prodtype.Duration;
                    pdmy.TaskName = prodtype.TaskName;
                    pdmy.ProjectName = prodtype.ProjectName;
                    // added by harish 07092019
                    if (prodtype.ProductionType == "Underwriter/Closer" && prodtype.ProductionTaskType == "APPLICATION REQUEST")
                    {
                        // #234
                        if (prodtype.TaskName.Contains("FC Request (") && prodtype.TaskName.StartsWith("FC Request (") && prodtype.TaskName.Contains(")"))
                        {

                            if (prodtype.Duration.Equals(90) && prodtype.ProjectName != null && prodtype.TaskName != null)
                            {
                                al90Days.Add(prodtype.Duration.ToString() + "$" + prodtype.ProjectName + "$" + prodtype.TaskName);
                                pm.Add(pdmy);
                            }

                            prodtype.DurationLeft = prodtype.Duration >= 90 ? prodtype.DurationDays + " (" + (120 - prodtype.Duration).ToString() + " Days Left)" : prodtype.DurationDays;
                        }

                        else
                        {
                            prodtype.DurationLeft = prodtype.Duration + " Days";
                        }
                    }

                }
            }

            // #234
            //sending mailing alerts here for 90 days.
            if (al90Days.Count > 0)
            {
                for (int cnt = 0; cnt <= pm.Count - 1; cnt++)
                {
                    _backgroundManager.SendFirmCommitmentNotificationEmail(_emailManager, pm[cnt]);
                }
            }

            if (productionTasks != null)
            {
                foreach (var item in productionTasks)
                {
                    //item.LastUpdated = TimezoneManager.ToMyTimeFromUtc(item.LastUpdated);
                    item.LastUpdated = item.LastUpdated;
                    if (item.ProductionTaskType == "AMENDMENTS")
						item.ProductionTaskType = "AMENDMENTS REQUEST";
                }
            }
            int totalrecods = productionTasks.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
			productionTasks = productionTasks.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.groupid).SelectMany(gr => gr).OrderBy(o => o.orderby);
			var results = productionTasks.Skip(pageIndex * pageSize).Take(pageSize).OrderBy(m=>m.ProductionTaskType);
            //var outresults = results.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.groupid).SelectMany(gr => gr).OrderBy(m => m.StatusId).ToList();
            //var outresults = results.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().OrderBy(m=>m.StatusId).ToList();
            // var outresults = results.GroupBy(s => s.groupid).SelectMany(gr => gr).ToList().OrderBy(m => m.StatusId == 18 ? 1 : m.StatusId == 17 ? 2 : m.StatusId == 20 ? 2 : m.StatusId == 19 ? 3 : 3).ToList();

            //Naveen 23/08/2019 (ordered status)
            //var outresults = results.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.groupid).SelectMany(gr => gr).OrderBy(m => m.StatusId == 18 ? 1 : m.StatusId == 17 ? 2 : m.StatusId == 20 ? 2 : m.StatusId == 19 ? 3 :3).ToList();
            var outresults = results.OrderBy(x => x.StatusId == 15 || x.StatusId == 19 ? 3 : x.StatusId == 16 ? 2 : x.StatusId == 18 || x.StatusId == 20 ? 1 : 3).GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList();


            //var outresults = results.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.groupid).SelectMany(gr => gr).OrderBy(o=>o.orderby).OrderBy(m => m.StatusId == 18 ? 1 : m.StatusId == 17 ? 2 : m.StatusId == 20 ? 2 : m.StatusId == 19 ? 3 : 3).ToList(); 
            var olstForm290 = outresults.Where(m => m.ProductionType == "Form 290").ToList();
            if (olstForm290.Count > 0)
            {
                var duplicateKeys = olstForm290.GroupBy(x => x.TaskName)
                        .Where(group => group.Count() > 1)
                        .Select(group => group.Key);

                foreach (string item in duplicateKeys)
                {
                    var dupNames = olstForm290.Where(m => m.TaskName == item).ToList();
                    ProductionMyTaskModel objForm290Request = dupNames.Where(m => m.StatusId == form290Request).FirstOrDefault();
                    ProductionMyTaskModel objForm290Complete = dupNames.Where(m => m.StatusId == form290Complete).FirstOrDefault();

                    if (objForm290Complete != null && objForm290Request != null)
                        outresults.Remove(objForm290Request);
                }
            }

            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = outresults.OrderByDescending(x=>x.ProductionTaskType[0]).ToList(),

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //karri#205
        private IEnumerable<ProductionMyTaskModel> getMyFilteredTasks(string UserName, string UserRole)
        {
            //search based on project name//TempData["prodType"]

            //if only fileter is by proj name
            if (TempData["projName"] != null && !string.IsNullOrEmpty(TempData["projName"].ToString())
                && (TempData["prodTaskType"] == null || string.IsNullOrEmpty(TempData["prodTaskType"].ToString())))
            {
                var productionTasks1 = _productionQueueManager.GetProductionTaskByUserName(UserName, UserRole)
                                .Where(m => m.ProjectName.ToUpper().Contains(TempData["projName"].ToString().ToUpper()))
                                .OrderByDescending(m => m.ProductionTaskType[0]).ThenByDescending(m => m.LastUpdated);

                return productionTasks1;
            }

            //if only fileter is by proj type
            if (TempData["prodTaskType"] != null && !string.IsNullOrEmpty(TempData["prodTaskType"].ToString())
                && (TempData["projName"] == null || string.IsNullOrEmpty(TempData["projName"].ToString())))
            {
                var productionTasks1 = _productionQueueManager.GetProductionTaskByUserName(UserName, UserRole)
                                .Where(m => m.ProductionTaskType == TempData["prodTaskType"].ToString())
                                .OrderByDescending(m => m.ProductionTaskType[0]).ThenByDescending(m => m.LastUpdated);

                return productionTasks1;
            }
            //Added by siddesh #265//
            //if only fileter is by FHAnumber 
            if (TempData["FHANumbers"] != null && !string.IsNullOrEmpty(TempData["FHANumbers"].ToString())
                && (TempData["projName"] == null || string.IsNullOrEmpty(TempData["projName"].ToString())) && (TempData["prodTaskType"] == null || string.IsNullOrEmpty(TempData["prodTaskType"].ToString())))
            {
                var productionTasks1 = _productionQueueManager.GetProductionTaskByUserName(UserName, UserRole)
                                .Where(m => m.TaskName.ToUpper().Contains(TempData["FHANumbers"].ToString().ToUpper()))
                                .OrderByDescending(m => m.TaskName[0]).ThenByDescending(m => m.LastUpdated);

                return productionTasks1;
            }

            //if fileter is by proj type and proj name
            if (TempData["prodTaskType"] != null && !string.IsNullOrEmpty(TempData["prodTaskType"].ToString())
                && (TempData["projName"] != null && !string.IsNullOrEmpty(TempData["projName"].ToString())))
            {
                var productionTasks1 = _productionQueueManager.GetProductionTaskByUserName(UserName, UserRole)
                                .Where(m => m.ProductionType == TempData["prodTaskType"].ToString())
                                .Where(m => m.ProjectName.ToUpper().Contains(TempData["projName"].ToString().ToUpper()))
                                .OrderByDescending(m => m.ProductionTaskType[0]).ThenByDescending(m => m.LastUpdated);

                return productionTasks1;
            }

            //default results set, No Search 
            var productionTasks = _productionQueueManager.GetProductionTaskByUserName(UserName, UserRole)
                                .OrderByDescending(m => m.ProductionTaskType[0]).ThenByDescending(m => m.LastUpdated);

            return productionTasks;
        }

        [HttpGet]
        public ActionResult GetProductionTaskDetail(Guid parentChildInstanceId, int fhaRequestType)
        {
            
            var fhaRequestViewModel = _fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(parentChildInstanceId);
            if (Request.QueryString.Count > 2)
            {
                if (Request.QueryString["IsFromPam"] == "LenderPam")
                {
                    fhaRequestViewModel.IsLenderPAMReport = true;
                }
                else
                {
                    fhaRequestViewModel.IsPAMReport = true;
                }
                
            }
            FHARequestController.SetModelValues(fhaRequestViewModel);
            fhaRequestViewModel.LenderUserId = fhaRequestViewModel.CreatedBy;
            fhaRequestViewModel.LenderUserName = accountManager.GetUserById(fhaRequestViewModel.CreatedBy).UserName;
            var fhaController = new FHARequestController();
            fhaController.PopulateWeeklyFeedFHANumberList(fhaRequestViewModel);
            FHARequestController.SetDropdownValuesForFHARequestForm(fhaRequestViewModel);
            fhaRequestViewModel.IsDisclaimerAccepted = true;
            fhaRequestViewModel.FhaRequestType = fhaRequestType;
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            fhaRequestViewModel.TransAccessStatus = checkResult.status;
            if (RoleManager.IsUserLamBamLar(UserPrincipal.Current.UserName) ||
                (fhaRequestViewModel.IsFhaInsertComplete && fhaRequestType == (int) PageType.FhaRequest)
                || (fhaRequestViewModel.IsPortfolioComplete && fhaRequestType == (int) ProductionView.Portfolio) ||
                (fhaRequestViewModel.IsCreditReviewAttachComplete && fhaRequestType == (int) ProductionView.CreditReview))
            {
                fhaRequestViewModel.IsMyTask = true;
                return View("~/Views/Production/FHARequest/FHARequestLenderReadOnlyView.cshtml", fhaRequestViewModel);
            }
            return View("~/Views/Production/FHARequest/FHARequestProductionUserView.cshtml", fhaRequestViewModel);
        }

        
        public ActionResult GetComments(Guid fhaRequestId, int type)
        {   
            var fhaNumberRequest = _fhaNumberRequestManager.GetFhaRequestById(fhaRequestId);

            if (fhaNumberRequest != null)
            {
                var subTasks = _prodTaskXrefManager.GetProductionSubTasks(fhaNumberRequest.TaskinstanceId);
                var productionUserMsg = new ProductionComments();
                LenderComment lenderComment = new LenderComment
                {
                   
                    CommentText = fhaNumberRequest.Comments,
                    LenderName = _fhaNumberRequestManager.GetLenderName(fhaNumberRequest.LenderId)
                };
                
                              
                
                if (type == (int)ProductionView.CreditReview)
                {
                    var creditReview = subTasks.FirstOrDefault(m => m.ViewId == type);
                    var prodUser = _productionQueueManager.GetUserById(creditReview.AssignedTo);
                    List<ProductionUserComments> productionUserComments = new List<ProductionUserComments>
                    {
                        new ProductionUserComments()
                        {
                            CommentText = fhaNumberRequest.CreditreviewComments,
                            CommentType =
                                EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(type.ToString())),
                            ProductionUserName =string.Format("{0} {1}",prodUser.FirstName,prodUser.LastName)
                        }

                    };
                    productionUserMsg.LenderComment = lenderComment;
                    productionUserMsg.ProductionUserComments = productionUserComments;


                }
                else if (type == (int)ProductionView.Portfolio)
                {

                    var portfolio = subTasks.FirstOrDefault(m => m.ViewId == type);
                    var prodUser = _productionQueueManager.GetUserById(portfolio.AssignedTo);
                    List<ProductionUserComments> productionUserComments = new List<ProductionUserComments>
                    {
                        new ProductionUserComments()
                        {
                            CommentText = fhaNumberRequest.PortfolioComments,
                            CommentType =
                                EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(type.ToString())),
                            ProductionUserName = string.Format("{0} {1}",prodUser.FirstName,prodUser.LastName)
                        }

                    };
                    
                    productionUserMsg.LenderComment = lenderComment;
                    productionUserMsg.ProductionUserComments = productionUserComments;
                }
                else
                {
                    var fhaInset = _taskManager.GetTasksByTaskInstanceId(fhaNumberRequest.TaskinstanceId).LastOrDefault();
                    var creditReview = subTasks.FirstOrDefault(m => m.ViewId == (int)ProductionView.CreditReview);
                    if (creditReview != null)
                    {
                        var prodUserCR = _productionQueueManager.GetUserById(creditReview.AssignedTo);
                        var portfolio = subTasks.FirstOrDefault(m => m.ViewId == (int)ProductionView.Portfolio);
                        if (portfolio != null)
                        {   var prodUserPF = _productionQueueManager.GetUserById(portfolio.AssignedTo);
                            var prodUserFHAInsert = new UserInfoModel();
                            if (fhaInset != null)
                            {
                                prodUserFHAInsert = _productionQueueManager.GetUserInfoByUsername(fhaInset.AssignedTo);
                            }

                            List<ProductionUserComments> productionUserComments = new List<ProductionUserComments>
                            {
                                new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.InsertFHAComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.InsertFha.ToString())),
                                    ProductionUserName = prodUserFHAInsert== null?"Not Assigned":string.Format("{0} {1}",prodUserFHAInsert.FirstName,prodUserFHAInsert.LastName)
                                },
                                new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.CreditreviewComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.CreditReview.ToString())),
                                    ProductionUserName = string.Format("{0} {1}",prodUserCR.FirstName,prodUserCR.LastName)
                                },
                                new ProductionUserComments()
                                {
                                    CommentText = fhaNumberRequest.PortfolioComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.Portfolio.ToString())),
                                    ProductionUserName = string.Format("{0} {1}",prodUserPF.FirstName,prodUserPF.LastName)
                                }
                            };
                    
                            productionUserMsg.LenderComment = lenderComment;
                            productionUserMsg.ProductionUserComments = productionUserComments;
                        }
                    }
                     else
                    {
                        var prodUserFHAInsert = new UserInfoModel();
                        if (fhaInset != null)
                        {
                            prodUserFHAInsert = _productionQueueManager.GetUserInfoByUsername(fhaInset.AssignedTo);
                        }
                        productionUserMsg.LenderComment = lenderComment;
                        productionUserMsg.ProductionUserComments =new List<ProductionUserComments> {
                            new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.InsertFHAComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.InsertFha.ToString())),
                                    ProductionUserName = prodUserFHAInsert == null ? "Not Assigned" : string.Format("{0} {1}", prodUserFHAInsert.FirstName, prodUserFHAInsert.LastName)
                                }
                                };

                    }
                }
                return View("~/Views/Production/ProductionMyTask/comments.cshtml", productionUserMsg);
            }
            
            return View("~/Views/Production/ProductionMyTask/index.cshtml");
        }
        private void MyTaskProdNumbers()
        {
            var currentUser = UserPrincipal.Current;
            var productionTasks = _productionQueueManager.GetProductionTaskByUserName(currentUser.UserName, currentUser.UserRole);

            ViewBag.TotalFhaInsert = productionTasks.Where(m=>m.FhaRequestType==(int)PageType.FhaRequest).Count();
            ViewBag.TotalCreditReviews = productionTasks.Where(m => m.FhaRequestType == (int)ProductionView.CreditReview).Count();
            ViewBag.TotalPortfolios = productionTasks.Where(m => m.FhaRequestType == (int)ProductionView.Portfolio).Count();
            //ViewBag.AppProcessInProcess = productionTasks.Where(m => m.FhaRequestType == (int)PageType.ProductionApplication).Count();

        }
        private void MyTaskLenderNumbers()
        {
            var currentUser = UserPrincipal.Current;
            var productionTasks = _productionQueueManager.GetProductionTaskByUserName(currentUser.UserName, currentUser.UserRole);
            ViewBag.FHAInQueue = productionTasks.Where(m =>m.FhaRequestType==(int)PageType.FhaRequest && m.StatusId ==(int) ProductionFhaRequestStatus.InQueue).Count();
            ViewBag.FHAInProcess = productionTasks.Where(m => m.FhaRequestType == (int)PageType.FhaRequest && m.StatusId == (int)ProductionFhaRequestStatus.InProcess).Count();
            ViewBag.AppProcessInQueue = productionTasks.Where(m => m.FhaRequestType == (int)PageType.ProductionApplication && m.StatusId == (int)ProductionAppProcessStatus.InQueue).Count();
            ViewBag.AppProcessInProcess = productionTasks.Where(m => m.FhaRequestType == (int)PageType.ProductionApplication && m.StatusId == (int)ProductionAppProcessStatus.InProcess).Count();

        }

        public ActionResult GetTaskDetail(Guid taskInstanceId)
        {
            var formUploadData = new OPAViewModel();
            var isChildTask = false;
            var task = new TaskModel();
            var xrefmodel = new Prod_TaskXrefModel();

            formUploadData.isChildTaskOpen = false;

            if (_parentChildTaskManager.IsParentTaskAvailable(taskInstanceId))
            {
                task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
               
                formUploadData = _projectActionFormManager.GetProdOPAByChildTaskId(task.TaskInstanceId);
                 formUploadData.viewId = 0;
                formUploadData.ChildTaskInstanceId = task.TaskInstanceId;
                isChildTask = true;
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    formUploadData.IsLoggedInUserLender = false;
                }
            }
            else
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }

                else if (RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    task = _taskManager.GetLatestTaskByTaskXrefid(taskInstanceId);
                    if(task == null)
                    {
                        task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                        taskInstanceId = _prodTaskXrefManager.GetUnderwriterTaskXrefIdByParentTaskInstanceId(task.TaskInstanceId);
                    }
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);

                    //logic to check is an child open task  and for confirmation popup flag
                    formUploadData.isChildTaskOpen = false;
                    List<TaskModel> childTaskList = _projectActionFormManager.GetProdChildTasksByXrefId(task.TaskInstanceId, false);
                    if (childTaskList != null && childTaskList.Count > 0)
                    {

                        foreach (var childList in childTaskList)
                        {

                            if (childList.TaskStepId == (int)TaskStep.OPAAddtionalInformation)
                            {
                                formUploadData.isChildTaskOpen = true;
                                break;
                            }

                        }
                    }
                     xrefmodel = _taskManager.GetReviewerViewIdByXrefTaskInstanceId(taskInstanceId);
                    formUploadData.viewId = xrefmodel!=null?xrefmodel.ViewId:0;
                    formUploadData.reviertaskStatus = xrefmodel != null ?xrefmodel.Status:0;
                    formUploadData.taskxrefId = taskInstanceId;
                    formUploadData.IsLoggedInUserLender = false;
                }

            }
            // based on task step, decide if is edit mode or readonly mode
            bool isEditMode = true;
            //if (task.TaskStepId == 17)
            //{
            //    task.TaskStepId = 14;
            //}

            if(xrefmodel!= null && xrefmodel.CompletedOn != null && xrefmodel.Status == (int) TaskStep.ProjectActionRequestComplete )
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequest && RoleManager.IsCurrentUserLenderRoles() ||RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)ProductionAppProcessStatus.Request && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            


            bool hasTaskOpenByUser = false;
            formUploadData.IschildTask = isChildTask;

            formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                    ? new TaskOpenStatusModel()
                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                        TaskOpenStatusModel; //Know y and how

            formUploadData.TaskGuid = task.TaskInstanceId;
            formUploadData.taskxrefId = taskInstanceId;
            formUploadData.Concurrency = _taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
            formUploadData.SequenceId = task.SequenceId;
            formUploadData.AssignedBy = task.AssignedBy;
            formUploadData.AssignedTo = task.AssignedTo;
            formUploadData.IsReassigned = task.IsReAssigned;
            formUploadData.TaskId = task.TaskId;
            formUploadData.PropertyId = _projectActionFormManager.GetProdPropertyInfo(formUploadData.FhaNumber).PropertyId;
            formUploadData.TaskStepId = task.TaskStepId;
            formUploadData.IsEditMode = isEditMode;
            formUploadData.pageTypeId =(int) task.PageTypeId;
            hasTaskOpenByUser = formUploadData.TaskOpenStatus.HasTaskOpenByUser(UserPrincipal.Current.UserName);

            // save task opened in task db
            //if (formUploadData.TaskOpenStatus != null)
            //{
            //    formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
            //    task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus,
            //        typeof(TaskOpenStatusModel), Encoding.Unicode);
            //    _taskManager.UpdateTask(task);
            //}

            //Logic for reassign of parent task after a child request is created //Know y and how
            if (_parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
            {
                var parenttask = _parentChildTaskManager.GetParentTask(task.TaskInstanceId);

                //if (parenttask != null)
                //{
                //    var newreassignedbyAe = string.Empty;

                //    newreassignedbyAe = taskReAssignmentManager.GetReassignedAE(parenttask.ParentTaskInstanceId);
                //    if (newreassignedbyAe != string.Empty)
                //        formUploadData.AssignedBy = newreassignedbyAe;
                //}
            }
            //Flag to identify Pam Report
            if (Request.QueryString.Count>1)
            {
                if (Request.QueryString["IsFromPam"] == "LenderPam")
                {
                    formUploadData.IsLenderPAMReport = true;
                }
                else
                {
                    formUploadData.IsPAMReport = true;
                }
            }
            //formUploadData.ServicerSubmissionDate =
            //  TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
            formUploadData.ServicerSubmissionDate =
             (DateTime)formUploadData.ServicerSubmissionDate;
            TempData["ApplicationRequestFormData"] = formUploadData;
            TempData["hasTaskOpenByUser"] = hasTaskOpenByUser;

            return RedirectToAction("GetApplicationFormDetail", "ProductionApplication", new { model = formUploadData });


        }
		public ActionResult GetForm290Task(Guid taskInstanceId)
		{
			List<TaskFileModel> olistTaskFileModel = new List<TaskFileModel>();

			// throw new Exception("hi");
			var task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
			//var groupTaskModel = new GroupTaskModel();
			try
			{//skumar-form290
				if (task != null)
				{
					ViewBag.Form290TaskInstanceId = task.TaskInstanceId;
					var getPropertyInfo = _projectActionFormManager.GetProdPropertyInfo(task.FHANumber);
					var form290Task = _productionQueueManager.GetForm290TaskBySingleTaskInstanceID(taskInstanceId);
					var opaTask = _projectActionFormManager.GetOPAByTaskInstanceId(form290Task.ClosingTaskInstanceID);

                    //karri, venkat#832
                    opaTask.Form290ClosingPreCheckConditionsValue = 0;
                    int retValue = _projectActionFormManager.GetForm290ClosingPreChecksValue(task.FHANumber);
                    opaTask.Form290ClosingPreCheckConditionsValue = retValue;

                    opaTask.PropertyId = getPropertyInfo.PropertyId;
					opaTask.PropertyName = getPropertyInfo.PropertyName;
					opaTask.ProjectActionName = EnumType.GetEnumDescription(
												EnumType.Parse<ProdProjectTypes>(opaTask.ProjectActionTypeId.ToString()));

					opaTask.TaskStepId = task.TaskStepId;
					opaTask.pageTypeId = (int)task.PageTypeId;
					opaTask.PropertyAddress = new AddressModel()
					{
						AddressLine1 = getPropertyInfo.StreetAddress,
						City = getPropertyInfo.City,
						StateCode = getPropertyInfo.State,
						ZIP = getPropertyInfo.Zipcode
					};

					opaTask.GroupTaskInstanceId = taskInstanceId;//skumar-form290- assign taskinstanceid to grouptaskinstanceid
					opaTask.pageTypeId = task.PageTypeId.HasValue ? task.PageTypeId.Value : 0;//skumar-form290 - get pagetype
					if (opaTask.pageTypeId == 0)
					{
						string formNameConfiguration = ConfigurationManager.AppSettings["PAGEID_ZERO"];
						opaTask.pageTypeId = !string.IsNullOrEmpty(formNameConfiguration) ? Convert.ToInt32(formNameConfiguration) : 0;
					}
					olistTaskFileModel = _taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceId).ToList();
                    opaTask.IsProdQueuQueue = true;
                    opaTask.IsProdQueuInprocess = true;
					if (olistTaskFileModel.Count() > 0)
						ViewBag.FilesUploaded = "FilesAlreadyUploaded";

					return View("~/Views/Production/Form290Request/Form290.cshtml", opaTask);
				}
			}
			catch (Exception e)
			{
				return HttpNotFound();
			}

			return HttpNotFound();
		}

		[HttpPost]
        public ActionResult GetForm290Task(FormCollection pFormCollection)
        {
            return RedirectToAction("Index", "ProductionMyTask");
        }


        public JsonResult GetUploadedDocuments(string sidx, string sord, int page, int rows, Guid taskInstanceID)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            // string userName="";
            var uploadedFilesByInstanceID = taskInstanceID == Guid.Empty ? new List<TaskFileModel>() : _taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceID).ToList();
            //var uploadedFilesByInstanceID = taskInstanceID == Guid.Empty ? new List<TaskFileModel>() : new List<TaskFileModel>(); //taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceID).ToList();
            var uploadedFiles = (from data in uploadedFilesByInstanceID 
                                 //.Where(m => m.FileType == uploadType)
                                 let userName = internalExternalTaskManger.GetUserNameById(data.CreatedBy) //skumar-form290 - Name
                                 //let userName = internalExternalTaskManger.GetUserInfoById(data.CreatedBy).UserName
                                 select new
                                 {
                                     TaskFileID = data.TaskFileId,
                                     TaskInstanceID = data.TaskInstanceId,
                                     data.FileType,
                                     userName = "",//internalExternalTaskManger.GetUserInfoById(data.CreatedBy).UserName,
                                     Name = userName,
                                     Role = data.RoleName,//RoleManager.GetUserRoles(userName).FirstOrDefault(),
                                     data.FileName,
                                     data.FileSize,
                                     data.UploadTime
                                 });
            int totalrecods = uploadedFiles.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var results = uploadedFiles.Skip(pageIndex * pageSize).Take(pageSize);



            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="FolderKey"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ActionResult Form290Upload(int pFolderKey, string pModel)
        {

            ViewBag.uploadurl = "/ProductionApplication/SaveDropzoneJsUploadedFiles";
            string taskxrefId = string.Empty;

            //ViewBag.redirecturl = "/ProductionMyTask/GetTaskDetail?taskInstanceId=";
            //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();

            var folderuploadModel = new FolderUploadModel { FolderKey = pFolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = pModel };
            return View("~/Views/InternalExternalTask/_DragDropFileUpload_InternExternal.cshtml", folderuploadModel);
        }


        //public ActionResult Form290Upload(Guid taskInstanceID)
        //{

        //    ViewBag.uploadurl = "/ProductionApplication/SaveDropzoneJsUploadedFiles";
        //    string taskxrefId = string.Empty;

        //    //ViewBag.redirecturl = "/ProductionMyTask/GetTaskDetail?taskInstanceId=";
        //    //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();

        //    var folderuploadModel = new FolderUploadModel { FolderKey = 1, CreatedBy = UserPrincipal.Current.UserId, ParentModel = "" };
        //    return View("~/Views/InternalExternalTask/_DragDropFileUpload_InternExternal.cshtml", folderuploadModel);
        //}

    }
}
