﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BusinessService.Interfaces;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using Model.Production;
using BusinessService.ProjectAction;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class FHARequestController : Controller
    {
        //

        // GET: /FHARequest/       
        private static IFHANumberRequestManager fhaRequestManager;
        private static IAccountManager accountManager;
        private IProd_GroupTasksManager groupTaskManager;
        private ITaskManager taskManager;
        private IProd_TaskXrefManager prodTaskXrefManager;
        private IBackgroundJobMgr backgroundJobManager;
        private IProjectActionFormManager projectActionFormManager;
        private IProd_RestfulWebApiTokenRequest WebApiTokenRequest;
        private IProd_RestfulWebApiDocumentUpload WebApiUpload;
        private IProd_RestfulWebApiUpdate WebApiUpdate;
        private IProd_RestfulWebApiDownload WebApiDownload;
        private IProd_RestfulWebApiAddPropertyInfo WebApiAddProporty;
        private IProd_NextStageManager prod_NextStageManager;

        public FHARequestController()
            : this(new FHANumberRequestManager(), new AccountManager(), new Prod_GroupTasksManager(), new TaskManager(),
            new Prod_TaskXrefManager(), new BackgroundJobMgr(), new ProjectActionFormManager(),
            new Prod_RestfulWebApiTokenRequest(), new Prod_RestfulWebApiDocumentUpload(), new Prod_RestfulWebApiUpdate(),
            new Prod_RestfulWebApiDownload(), new Prod_NextStageManager(), new Prod_RestfulWebApiAddPropertyInfo())
        {

        }

        public FHARequestController(FHANumberRequestManager _fhaRequestManager, AccountManager _accountManager,
            Prod_GroupTasksManager _groupTaskManager, TaskManager _taskManager, Prod_TaskXrefManager _prodTaskXrefManager,
            BackgroundJobMgr _backgroundManager, ProjectActionFormManager _projectActionFormMananger,
            Prod_RestfulWebApiTokenRequest _WebApiTokenRequest, Prod_RestfulWebApiDocumentUpload _WebApiUpload,
            Prod_RestfulWebApiUpdate _WebApiUpdate, Prod_RestfulWebApiDownload _WebApiDownload, Prod_NextStageManager _prod_NextStageManager, Prod_RestfulWebApiAddPropertyInfo _WebApiAddProporty)
        {
            fhaRequestManager = _fhaRequestManager;
            accountManager = _accountManager;
            groupTaskManager = _groupTaskManager;
            taskManager = _taskManager;
            prodTaskXrefManager = _prodTaskXrefManager;
            backgroundJobManager = _backgroundManager;
            projectActionFormManager = _projectActionFormMananger;
            WebApiUpload = _WebApiUpload;
            WebApiDownload = _WebApiDownload;
            WebApiTokenRequest = _WebApiTokenRequest;
            WebApiUpdate = _WebApiUpdate;
            prod_NextStageManager = _prod_NextStageManager;
            WebApiAddProporty = _WebApiAddProporty;
        }

        public ActionResult Index()
        {
            var model = new FHANumberRequestViewModel();
            if (UserPrincipal.Current.LenderId != null) model.LenderId = (int)UserPrincipal.Current.LenderId;
            SetModelValues(model);
            model.LenderUserName = UserPrincipal.Current.UserName;
            model.LenderUserId = UserPrincipal.Current.UserId;
           // PopulateWeeklyFeedFHANumberList(model);
            PopulateWeeklyFeedFHANumber(model);
            return View("~/Views/Production/FHARequest/FHARequestLenderView.cshtml", model);
        }

        public static void SetModelValues(FHANumberRequestViewModel model)
        {
            if (accountManager == null) accountManager = new AccountManager();
            if (fhaRequestManager == null) fhaRequestManager = new FHANumberRequestManager();
            var activityTypes = fhaRequestManager.GetAllActivityTypes();
            foreach (var type in activityTypes)
            {
                model.AllActivityTypes.Add(new SelectListItem()
                {
                    Text = type.ActivityName,
                    Value = type.ActivityTypeId.ToString()
                });
            }

            var borrowerTypes = fhaRequestManager.GetAllBorrowerTypes();
            foreach (var type in borrowerTypes)
            {
                model.AllBorrowerTypes.Add(new SelectListItem()
                {
                    Text = type.BorrowerTypeName,
                    Value = type.BorrowerTypeId.ToString()
                });
            }

            var projectTypes = fhaRequestManager.GetAllProjectTypes();
            foreach (var type in projectTypes)
            {
                model.AllProjectTypes.Add(new SelectListItem()
                {
                    Text = type.ProjectTypeName,
                    Value = type.ProjectTypeId.ToString()
                });
            }

            var currentLoanTypes = fhaRequestManager.GetAllProjectTypes();
            foreach (var type in currentLoanTypes)
            {
                model.AllCurrentLoanTypes.Add(new SelectListItem()
                {
                    Text = type.ProjectTypeName,
                    Value = type.ProjectTypeId.ToString()
                });
            }

            var mortgageInsuranceTypes = fhaRequestManager.GetALlMortgageInsuranceTypes();
            foreach (var type in mortgageInsuranceTypes)
            {
                if (type.LoanTypeName == "Insurance Upon Completion" || type.LoanTypeId == 1)
                {
                    model.AllMortgageInsuranceTypes.Add(new SelectListItem()
                    {
                        Text = type.LoanTypeName,
                        Value = type.LoanTypeId.ToString(),
                        Selected = true
                    });
                }
                else
                {
                    model.AllMortgageInsuranceTypes.Add(new SelectListItem()
                    {
                        Text = type.LoanTypeName,
                        Value = type.LoanTypeId.ToString()
                    });
                }
            }

            var cmsRatings = fhaRequestManager.GetCMSStarRatings();
            foreach (var rating in cmsRatings)
            {
                model.AllCmsStarRatings.Add(new SelectListItem()
                {
                    Text = rating.RatingName,
                    Value = rating.RatingId.ToString()
                });
            }

            var stateCodes = accountManager.GetAllStates();
            foreach (var state in stateCodes)
            {
                model.AllStateCodes.Add(new SelectListItem() { Text = state.Key, Value = state.Key });
            }

            model.AllStagesForConstruction.Add(new SelectListItem() { Text = "Select Stage", Value = "0" });
            model.AllStagesForConstruction.Add(new SelectListItem() { Text = "Single Stage", Value = "1" });
            model.AllStagesForConstruction.Add(new SelectListItem() { Text = "Two Stage", Value = "2" });

            model.AllPortfolioStatus.Add(new SelectListItem() { Text = "New", Value = "1" });
            model.AllPortfolioStatus.Add(new SelectListItem() { Text = "Existing", Value = "2" });
            model.AllPortfolioStatus.Add(new SelectListItem() { Text = "NA", Value = "3" });

            model.DisclaimerText = fhaRequestManager.GetDisclaimerMsgByPageType("FHA# REQUEST");
            model.LenderName = fhaRequestManager.GetLenderName(model.LenderId);
            //model.IsLIHTC =  model.IsLIHTC != null ? false : true;

        }

        public static void SetDropdownValuesForFHARequestForm(FHANumberRequestViewModel fhaRequestViewModel)
        {
            foreach (var item in fhaRequestViewModel.AllActivityTypes)
            {
                if (int.Parse(item.Value) == fhaRequestViewModel.ActivityTypeId)
                {
                    item.Selected = true;
                    fhaRequestViewModel.SelectedActivityTypeName = item.Text;
                }
            }
            foreach (var item in fhaRequestViewModel.AllProjectTypes)
            {
                if (int.Parse(item.Value) == fhaRequestViewModel.ProjectTypeId)
                {
                    item.Selected = true;
                    fhaRequestViewModel.SelectedProjectTypeName = item.Text;
                }
            }
            foreach (var item in fhaRequestViewModel.AllBorrowerTypes)
            {
                if (int.Parse(item.Value) == fhaRequestViewModel.BorrowerTypeId)
                {
                    item.Selected = true;
                    fhaRequestViewModel.SelectedBorrowerTypeName = item.Text;
                }
            }
            foreach (var item in fhaRequestViewModel.AllMortgageInsuranceTypes)
            {
                if (int.Parse(item.Value) == fhaRequestViewModel.LoanTypeId)
                {
                    item.Selected = true;
                    fhaRequestViewModel.SelectedMortgageInsuranceTypeName = item.Text;
                }
            }
            foreach (var item in fhaRequestViewModel.AllPortfolioStatus)
            {
                if (int.Parse(item.Value) == fhaRequestViewModel.IsNewOrExistOrNAPortfolio)
                {
                    item.Selected = true;
                    fhaRequestViewModel.SelectedPortfolioStatusName = item.Text;
                }
            }
            foreach (var item in fhaRequestViewModel.AllCmsStarRatings)
            {
                if (int.Parse(item.Value) == fhaRequestViewModel.CMSStarRating)
                {
                    item.Selected = true;
                    fhaRequestViewModel.SelectedCmsStarRating = item.Text;
                }
            }
            if (fhaRequestViewModel.PropertyAddressId != 0)
            {
                foreach (var item in fhaRequestViewModel.AllStateCodes)
                {
                    if (item.Value == fhaRequestViewModel.PropertyState)
                    {
                        item.Selected = true;
                        fhaRequestViewModel.SelectedStateName = item.Text;
                    }
                }
            }
            if (fhaRequestViewModel.ConstructionStageTypeId != 0)
            {
                foreach (var item in fhaRequestViewModel.AllStagesForConstruction.Where(item => int.Parse(item.Value) == fhaRequestViewModel.ConstructionStageTypeId))
                {
                    item.Selected = true;
                    fhaRequestViewModel.SelectedConstructionStageName = item.Text;
                }
            }

            if (fhaRequestViewModel.LoanAmount != 0)
            {
                fhaRequestViewModel.ConfirmLoanAmount = fhaRequestViewModel.LoanAmount;
            }

            foreach (var item in fhaRequestViewModel.AllCurrentLoanTypes)
            {
                if (int.Parse(item.Value) == fhaRequestViewModel.CurrentLoanTypeId)
                {
                    item.Selected = true;
                    fhaRequestViewModel.SelectedCurrentLoanTypeName = item.Text;
                }
            }
            //foreach (var item in fhaRequestViewModel.AvailableFHANumbersList)
            //{
            //    if (item == fhaRequestViewModel.CurrentFHANumber)
            //    {
            //        item.Selected = true;
            //        fhaRequestViewModel.SelectedCurrentLoanTypeName = item.Text;
            //    }
            //}

        }


        /*
         * Method not required: Congressional district is a input text box
         * public JsonResult GetCongressionalDistrictsByZipCode(int zipCode)
        {
            var result = fhaRequestManager.GetCongressionalDistrictByZipCode(zipCode);
            var congressionalDistrictList = new List<SelectListItem>();
            foreach (var item in result)
            {
                congressionalDistrictList.Add(new SelectListItem()
                {
                    Text = item.CongressionalDistrict.ToString(),
                    Value = item.CongressionalDTId.ToString()
                });
            }
            JsonResult json = Json(congressionalDistrictList, JsonRequestBehavior.AllowGet);
            return json;
        }*/
        //FHANumberRequestViewModel model,
        [HttpPost]
        public ActionResult SubmitForm(FHANumberRequestViewModel fhaRequestViewModel)
        {
            var model = fhaRequestViewModel;
          // if (ModelState.IsValid || model.SubmitType == "Save")
           // {
           //if (model.SubmitType == "Save")
            //{ 
                if (model.IsMidLargePortfolio != null)
                {
                    model.IsCreditReviewRequired = model.IsMidLargePortfolio.Value;

                    model.IsPortfolioRequired = true;
                }
                else
                {
                    model.IsPortfolioRequired = false;
                    model.IsCreditReviewRequired = false;
                }
                if (model.CurrentLoanTypeId == null)
                {
                    model.CurrentLoanTypeId = 0;
                }

                if (model.ActivityTypeId == null)
                {
                    model.ActivityTypeId = 0;
                }

                if (model.Portfolio_Number == null)
                {
                    model.Portfolio_Number = 0;
                }

                if (model.PropertyId == null)
                {
                    model.PropertyId = 0;
                }

                if (model.CMSStarRating == null)
                {
                    model.CMSStarRating = 0;
                }
                model.PropertyAddressModel.AddressLine1 = model.PropertyStreetAddress;
                model.PropertyAddressModel.City = model.PropertyCity;
                model.PropertyAddressModel.StateCode = model.PropertyState;
                model.PropertyAddressModel.ZIP = model.PropertyZip;

                model.LenderContactAddressModel.Email = model.LenderContactEmail;
                model.LenderContactAddressModel.PhonePrimary = model.LenderContactPhone;
                Prod_GroupTasksModel groupTaskModel;
                switch (model.SubmitType)
                {
                    case "Save":
                        model.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel = new Prod_GroupTasksModel();
                        groupTaskModel.PageTypeId = 4;
                        groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedOn = DateTime.UtcNow;
                        groupTaskModel.IsDisclaimerAccepted = model.IsDisclaimerAccepted;
                        groupTaskModel.ServicerComments = model.Comments;

                        if (model.TaskinstanceId != Guid.Empty)
                        {
                            groupTaskModel.TaskInstanceId = model.TaskinstanceId;
                            groupTaskManager.UpdateGroupTask(groupTaskModel);
                            fhaRequestManager.UpdateFHARequest(model);
                        }
                        else if (model.TaskinstanceId == Guid.Empty)
                        {
                            groupTaskModel.TaskInstanceId = Guid.NewGuid();
                            groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                            groupTaskModel.CreatedOn = DateTime.UtcNow;
                            groupTaskManager.AddProdGroupTasks(groupTaskModel);
                            model.TaskinstanceId = groupTaskModel.TaskInstanceId;

                            fhaRequestManager.AddFHARequest(model);
                        }
                        break;
                    case "Submit":
                        model.RequestStatus = (int)RequestStatus.Submit;
                        model.RequestSubmitDate = DateTime.UtcNow;
                        if (model.TaskinstanceId == Guid.Empty)
                        {
                            model.TaskinstanceId = Guid.NewGuid();
                            fhaRequestManager.AddFHARequest(model);
                        }
                        else
                        {
                            groupTaskModel = new Prod_GroupTasksModel();
                            groupTaskModel.PageTypeId = 4;
                            groupTaskModel.RequestStatus = (int)RequestStatus.Submit;
                            groupTaskModel.InUse = UserPrincipal.Current.UserId;
                            groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                            groupTaskModel.ModifiedOn = DateTime.UtcNow;
                            groupTaskModel.TaskInstanceId = model.TaskinstanceId;
                            groupTaskModel.IsDisclaimerAccepted = model.IsDisclaimerAccepted;
                            groupTaskManager.UpdateGroupTask(groupTaskModel);
                            fhaRequestManager.UpdateFHARequest(model);
                        }

                        var assignedBy = UserPrincipal.Current.UserName;
                        var userid = UserPrincipal.Current.UserId;
                        var userRole = UserPrincipal.Current.UserRole;
                        var returnCode = taskManager.CreateFHARequestTasks(assignedBy, model.TaskinstanceId, userid,
                            userRole,
                            model.IsPortfolioRequired, model.IsCreditReviewRequired);
                        break;
                }
          // }

            return View("~/Views/Production/FHARequest/FHARequestLenderView.cshtml", model);
        }

        public ActionResult ApproveFHARequest(FHANumberRequestViewModel fhaRequestViewModel)
        {
            var model = fhaRequestViewModel;
            if (ModelState.IsValid)
            {

                if (model.CurrentLoanTypeId == null)
                {
                    model.CurrentLoanTypeId = 0;
                }
                if (model.ActivityTypeId == null)
                {
                    model.ActivityTypeId = 0;
                }
                if (model.Portfolio_Number == null)
                {
                    model.Portfolio_Number = 0;
                }
                if (model.PropertyId == null)
                {
                    model.PropertyId = 0;
                }
                if (model.CMSStarRating == null)
                {
                    model.CMSStarRating = 0;
                }
                model.PropertyAddressModel.AddressLine1 = model.PropertyStreetAddress;
                model.PropertyAddressModel.City = model.PropertyCity;
                model.PropertyAddressModel.StateCode = model.PropertyState;
                model.PropertyAddressModel.ZIP = model.PropertyZip;

                model.LenderContactAddressModel.Email = model.LenderContactEmail;
                model.LenderContactAddressModel.PhonePrimary = model.LenderContactPhone;
                var latestTask = taskManager.GetLatestTaskByTaskInstanceId(model.TaskinstanceId);
                var cancelComments = model.CancelComments;
                if ((model.PropertyId != null && model.PropertyId > 0) && (model.SubmitType == "Insert" || model.SubmitType == "Credit"))
                {
                    if (string.IsNullOrEmpty(model.FHANumber))
                    {
                        model.FHANumber = "";
                    }
                    var modelProperty = new RestfulWebApiAddPropertyModel();
                    var WebApiResult = new RestfulWebApiResultModel();
                    var request = new RestfulWebApiTokenResultModel();
                    var JsonSteing =
                              "{\"propertyId\":\"" + model.PropertyId + "\"," +
                              "\"propertyName\":\"" + model.ProjectName + "\"," +
                              "\"city\":\"" + model.PropertyCity + "\"," +
                              "\"state\":\"" + model.PropertyState + "\"," +
                              "\"zip\":\"" + model.PropertyZip + "\"}";
                    // "\"fhaNumber\":\"" + model.FHANumber + "\"," +
                    //  "\"loanType\":" + 0 + "," +
                    //"\"loanAmount\":0,"  +
                    //"\"closingDate\":\"\","  +
                    // "\"lenderName\":\"" + model.LenderContactName + "\"}";
                    request = WebApiTokenRequest.RequestToken();
                    WebApiResult = WebApiAddProporty.AddPropertyInfoUsingWebApi(JsonSteing, request.access_token);

                    if (WebApiResult.key != "200" && WebApiResult.key != "201")
                    {

                        Exception ex = new Exception(WebApiResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        return Json(new { success = false, result = "failed" }, JsonRequestBehavior.AllowGet);

                    }

                }
                switch (model.SubmitType)
                {
                    case "Insert":
                        model.IsFhaInsertComplete = true;
                        prodTaskXrefManager.UpdateTaskXrefAndFHARequest(model.TaskinstanceId,
                            (int)PageType.FhaRequest, model.InsertFHAComments, model.FHANumber, "", 0);
                        fhaRequestManager.UpdateFHARequest(model);
                        backgroundJobManager.SendFHAInsertCompletionEmail(new EmailManager(), latestTask.AssignedBy,
                            UserPrincipal.Current.UserName, model);
                        break;
                    case "Portfolio":
                        model.IsPortfolioComplete = true;
                        prodTaskXrefManager.UpdateTaskXrefAndFHARequest(model.TaskinstanceId,
                            (int)ProductionView.Portfolio, model.PortfolioComments, "", model.Portfolio_Name, (int)model.Portfolio_Number);
                        fhaRequestManager.UpdateFHARequest(model);
                        break;
                    case "Credit":
                        model.IsCreditReviewAttachComplete = true;
                        if (model.CreditReviewDate != null)
                            prodTaskXrefManager.UpdateTaskXrefAndFHARequest(model.TaskinstanceId,
                                (int)ProductionView.CreditReview, model.CreditreviewComments, "", "", 0);
                        fhaRequestManager.UpdateFHARequest(model);
                        break;
                    case "Deny":
                        model.RequestStatus = (int)RequestStatus.Deny;
                        model.IsFhaInsertComplete = true;
                        model.IsPortfolioComplete = model.IsPortfolioRequired;
                        model.IsCreditReviewAttachComplete = model.IsCreditReviewRequired;
                        break;
                    case "Save":
                        fhaRequestManager.UpdateFHARequest(model);
                        return View("~/Views/Production/FHARequest/FHARequestLenderView.cshtml", model);
                        break;
                    case "Cancel":
                        if (model.FhaRequestType == (int)PageType.FhaRequest)
                        {
                            model.RequestStatus = (int)RequestStatus.Cancel;
                            model.IsFhaInsertComplete = true;
                            model.IsPortfolioComplete = model.IsPortfolioRequired;
                            model.IsCreditReviewAttachComplete = model.IsCreditReviewRequired;
                            model.IsPortfolioCancelled = true;
                            model.IsCreditCancelled = true;
                        }
                        else if (model.FhaRequestType == (int)ProductionView.Portfolio)
                        {
                            model.IsPortfolioComplete = true;
                            model.IsPortfolioCancelled = true;
                            model.CancelComments = "";
                        }
                        else if (model.FhaRequestType == (int)ProductionView.CreditReview)
                        {
                            model.IsCreditReviewAttachComplete = true;
                            model.IsCreditCancelled = true;
                            model.CancelComments = "";
                        }
                        break;
                }
                if (!(model.RequestStatus == (int)RequestStatus.Deny || model.RequestStatus == (int)RequestStatus.Cancel))
                {
                    if (model.IsPortfolioRequired && model.IsCreditReviewRequired)
                    {
                        if (model.IsFhaInsertComplete && model.IsPortfolioComplete && model.IsCreditReviewAttachComplete)
                        {
                            model.IsReadyForApplication = true;
                            model.RequestStatus = (int)RequestStatus.Approve;

                        }
                    }
                    else if (model.IsPortfolioRequired && !model.IsCreditReviewRequired)
                    {
                        if (model.IsFhaInsertComplete && model.IsPortfolioComplete)
                        {
                            model.IsReadyForApplication = true;
                            model.RequestStatus = (int)RequestStatus.Approve;
                        }
                    }
                    else if (!model.IsPortfolioRequired && !model.IsCreditReviewRequired)
                    {
                        if (model.IsFhaInsertComplete)
                        {
                            model.IsReadyForApplication = true;
                            model.RequestStatus = (int)RequestStatus.Approve;
                        }
                    }
                }


                if (model.RequestStatus == (int)RequestStatus.Approve || model.RequestStatus == (int)RequestStatus.Deny
                    || model.RequestStatus == (int)RequestStatus.Cancel || model.IsPortfolioCancelled || model.IsCreditCancelled)
                {
                    var groupTaskModel = new Prod_GroupTasksModel();
                    groupTaskModel.PageTypeId = 4;
                    groupTaskModel.RequestStatus = model.RequestStatus;
                    groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                    groupTaskModel.ModifiedOn = DateTime.UtcNow;
                    groupTaskModel.TaskInstanceId = model.TaskinstanceId;
                    groupTaskModel.IsDisclaimerAccepted = model.IsDisclaimerAccepted;
                    groupTaskManager.UpdateGroupTask(groupTaskModel);
                    if (model.RequestStatus == (int)RequestStatus.Deny)
                        prodTaskXrefManager.UpdateTaskXrefForFHARequestDeny(model.TaskinstanceId, model.DenyComments);
                    else if (model.RequestStatus == (int)RequestStatus.Cancel || model.IsPortfolioCancelled || model.IsCreditCancelled)
                        prodTaskXrefManager.UpdateTaskXrefForFHARequestCancel(model.TaskinstanceId, cancelComments, model.FhaRequestType);
                    fhaRequestManager.UpdateFHARequest(model);

                    if (model.RequestStatus == (int)RequestStatus.Approve || model.RequestStatus == (int)RequestStatus.Deny
                        || model.RequestStatus == (int)RequestStatus.Cancel)
                    {
                        var taskList = new List<TaskModel>();
                        var task = new TaskModel();

                        task.TaskInstanceId = model.TaskinstanceId;
                        task.AssignedBy = UserPrincipal.Current.UserName;
                        task.AssignedTo = latestTask.AssignedBy;
                        task.SequenceId = latestTask.SequenceId + 1;
                        task.FHANumber = model.FHANumber;
                        task.StartTime = DateTime.UtcNow;
                        //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                        task.MyStartTime = task.StartTime;
                        task.IsReAssigned = false;
                        task.PageTypeId = (int)PageType.FhaRequest;
                        task.TaskStepId = (int)ProductionFhaRequestStatus.Completed;
                        taskList.Add(task);
                        taskManager.SaveTask(taskList, new TaskFileModel());
                        if (model.RequestStatus == (int)RequestStatus.Approve)
                        {
                            backgroundJobManager.SendFHARequestCompletionEmail(new EmailManager(), task.AssignedTo,
                                task.AssignedBy, model);


                            if (model.ProjectTypeId == (int)ProdProjectTypes.Construction241 ||
                                model.ProjectTypeId == (int)ProdProjectTypes.ConstructionNC ||
                                model.ProjectTypeId == (int)ProdProjectTypes.ConstructionSR)
                            {
                                if (model.ConstructionStageTypeId == 1)
                                {
                                    var nextStage = new Prod_NextStageModel();
                                    nextStage.NextStgId = Guid.NewGuid();
                                    nextStage.CompletedPgTypeId = (int)PageType.FhaRequest;
                                    nextStage.CompletedPgTaskInstanceId = model.TaskinstanceId;
                                    nextStage.NextPgTypeId = (int)PageType.ConstructionSingleStage;
                                    nextStage.ModifiedOn = DateTime.UtcNow;
                                    nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                                    nextStage.FhaNumber = model.FHANumber;
                                    nextStage.IsNew = true;
                                    prod_NextStageManager.AddNextStage(nextStage);
                                }
                                var nextStage1 = new Prod_NextStageModel();
                                nextStage1.NextStgId = Guid.NewGuid();
                                nextStage1.CompletedPgTypeId = (int)PageType.FhaRequest;
                                nextStage1.CompletedPgTaskInstanceId = model.TaskinstanceId;
                                nextStage1.NextPgTypeId = (int)PageType.ConstructionTwoStageInitial;
                                nextStage1.ModifiedOn = DateTime.UtcNow;
                                nextStage1.ModifiedBy = UserPrincipal.Current.UserId;
                                nextStage1.FhaNumber = model.FHANumber;
                                nextStage1.IsNew = true;
                                prod_NextStageManager.AddNextStage(nextStage1);

                                var nextStage2 = new Prod_NextStageModel();
                                nextStage2.NextStgId = Guid.NewGuid();
                                nextStage2.CompletedPgTypeId = (int)PageType.FhaRequest;
                                nextStage2.CompletedPgTaskInstanceId = model.TaskinstanceId;
                                nextStage2.NextPgTypeId = (int)PageType.ConstructionTwoStageFinal;
                                nextStage2.ModifiedOn = DateTime.UtcNow;
                                nextStage2.ModifiedBy = UserPrincipal.Current.UserId;
                                nextStage2.FhaNumber = model.FHANumber;
                                nextStage2.IsNew = true;
                                prod_NextStageManager.AddNextStage(nextStage2);
                            }
                            else
                            {
                                var nextStage = new Prod_NextStageModel();
                                nextStage.NextStgId = Guid.NewGuid();
                                nextStage.CompletedPgTypeId = (int)PageType.FhaRequest;
                                nextStage.CompletedPgTaskInstanceId = model.TaskinstanceId;
                                nextStage.NextPgTypeId = (int)PageType.ProductionApplication;
                                nextStage.ModifiedOn = DateTime.UtcNow;
                                nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                                nextStage.FhaNumber = model.FHANumber;
                                nextStage.IsNew = true;
                                prod_NextStageManager.AddNextStage(nextStage);
                            }

                        }

                    }

                }

            }
            return View("~/Views/Production/FHARequest/FHARequestLenderView.cshtml", model);
        }

        [HttpPost]
        public string UploadFile(FHANumberRequestViewModel fhaRequestViewModel)
        {
            var result = projectActionFormManager.GetProdPropertyInfo(fhaRequestViewModel.FHANumber);
            if (result.PropertyId == 0)
            {

                var modelProperty = new RestfulWebApiAddPropertyModel();
                var WebApiResult = new RestfulWebApiResultModel();
                var request1 = new RestfulWebApiTokenResultModel();
                var JsonSteing =
                          "{\"propertyId\":\"" + fhaRequestViewModel.PropertyId + "\"," +
                          "\"propertyName\":\"" + fhaRequestViewModel.ProjectName + "\"," +
                          "\"city\":\"" + fhaRequestViewModel.PropertyCity + "\"," +
                          "\"state\":\"" + fhaRequestViewModel.PropertyState + "\"," +
                          "\"zip\":\"" + fhaRequestViewModel.PropertyZip + "\"}";

                request1 = WebApiTokenRequest.RequestToken();
                WebApiResult = WebApiAddProporty.AddPropertyInfoUsingWebApi(JsonSteing, request1.access_token);

                if (WebApiResult.key != "200" && WebApiResult.key != "201")
                {

                    Exception ex = new Exception(WebApiResult.message);
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    return "failed";

                }

            }
            var uploadmodel = new RestfulWebApiUploadModel()
            {
                propertyID = fhaRequestViewModel.PropertyId.ToString(),
                indexType = "1",
                indexValue = fhaRequestViewModel.FHANumber,
                pdfConvertableValue = "false",
                // folderNames = "Production/WS",
                folderNames = "Production/FHA Number Request",
            };
            if (string.IsNullOrEmpty(uploadmodel.indexValue))
            {
                uploadmodel.indexValue = "FHA-NUMBER";
            }
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //karri#162
            //request = WebApiTokenRequest.RequestToken();           

            foreach (string Filename in Request.Files)
            {
                //string documentType = TaskHelper.GetUntilOrEmpty(Filename, "_");
                HttpPostedFileBase myFile = Request.Files[Filename];
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();
                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = fhaRequestViewModel.ProjectName + "_" +
                                         fhaRequestViewModel.ProjectTypeId + "_" +
                                         fhaRequestViewModel.TaskinstanceId + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);
                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    //karri#162
                    request = WebApiTokenRequest.RequestToken();

                    WebApiUploadResult = WebApiUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "FHACredit");

                    if (!string.IsNullOrEmpty(WebApiUploadResult.status) && WebApiUploadResult.status.ToLower() == "success")
                    {
                        if (WebApiUploadResult.status.ToLower() == "success")
                        {
                            System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                            var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
                            DateTime.UtcNow.ToString(), systemFileName, fhaRequestViewModel.TaskinstanceId);
                            taskFile.DocId = WebApiUploadResult.docId;
                            taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                            taskFile.API_upload_status = WebApiUploadResult.status;
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                            taskManager.SaveGroupTaskFileModel(taskFile);
                            //isUploaded = true;
                        }

                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        return "failed";
                    }



                }
            }

            return "Success";
        }
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        public ActionResult GetGroupTaskDetail(Prod_GroupTasksModel groupTaskModel)
        {
            var model = fhaRequestManager.GetFhaRequestByTaskInstanceId(groupTaskModel.TaskInstanceId);
            SetModelValues(model);
            model.LenderUserName = UserPrincipal.Current.UserName;
            model.LenderUserId = UserPrincipal.Current.UserId;
            //PopulateWeeklyFeedFHANumberList(model);
            PopulateWeeklyFeedFHANumber(model);
            SetDropdownValuesForFHARequestForm(model);
            model.IsDisclaimerAccepted = groupTaskModel.IsDisclaimerAccepted;
            if (groupTaskModel.RequestStatus == (int)RequestStatus.Submit || groupTaskModel.RequestStatus == (int)RequestStatus.Approve)
            {
                model.IsGroupTask = true;
                return View("~/Views/Production/FHARequest/FHARequestLenderReadOnlyView.cshtml", model);
            }
            return View("~/Views/Production/FHARequest/FHARequestLenderView.cshtml", model);
        }

        public JsonResult GetPropertyInfo(string currentFha)
        {
            var result = projectActionFormManager.GetProdPropertyInfo(currentFha);
            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        [HttpGet]
        public FileResult DownloadTaskFile(string taskfileid)
        {
            string JsonSteing = "";

            RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();

            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));


            if (taskFile != null)
            {
                if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                {
                    request = WebApiTokenRequest.RequestToken();
                    JsonSteing = "{\"docId\":" + taskFile.DocId + ",\"version\":" + taskFile.Version + "}";
                    Stream streamResult = WebApiDownload.DownloadDocumentUsingWebApi(JsonSteing, request.access_token, taskFile.FileName);
                }
                else
                {
                    string filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                    var fileInfo = new System.IO.FileInfo(filePath);
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition",
                    String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                    Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    Response.WriteFile(filePath);
                }

                Response.End();
            }

            return null;
        }

        [HttpPost]
        public string DeleteFile(int taskfileid)
        {
            var request = new RestfulWebApiTokenResultModel();
            var APIresult = new RestfulWebApiResultModel();
            var UpdateInfo = new RestfulWebApiUpdateModel();
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            var success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid));
                if (success == 1)
                {
                    if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                    {
                        request = WebApiTokenRequest.RequestToken();
                        UpdateInfo.docId = taskFile.DocId;
                        UpdateInfo.documentStatus = "Inactive";
                        UpdateInfo.propertyId = "";
                        UpdateInfo.documentType = "";
                        UpdateInfo.indexType = "";
                        UpdateInfo.indexValue = "500";

                        APIresult = WebApiUpdate.UpdateDocument(UpdateInfo, request.access_token);
                        if (!string.IsNullOrEmpty(APIresult.status) && APIresult.status.ToLower() == "success")
                        {
                            return "Success";
                        }
                    }
                    else
                    {
                        System.IO.File.Delete(pathForSaving + "\\" + taskFile.SystemFileName);

                        if (success == 1)
                            return "Success";
                    }

                }
            }


            return "Success";
        }

        public ActionResult InitiateIrrRequest()
        {
            var model = new FHANumberRequestViewModel();
            if (UserPrincipal.Current.LenderId != null)
            {
                model.LenderId = (int)UserPrincipal.Current.LenderId;
                model.LenderName = fhaRequestManager.GetLenderName(model.LenderId);
            }
            model.LenderUserName = UserPrincipal.Current.UserName;
            model.LenderUserId = UserPrincipal.Current.UserId;
            //PopulateWeeklyFeedFHANumberList(model);
            PopulateWeeklyFeedFHANumber(model);
            //model.DisclaimerText = fhaRequestManager.GetDisclaimerMsgByPageType("FHARequest");
            return View("~/Views/Production/FHARequest/InitiateIrrRequestForm.cshtml", model);
        }

        public void PopulateWeeklyFeedFHANumberList(FHANumberRequestViewModel model)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(model.LenderUserName))
            {
                fhaNumberList =
                    projectActionFormManager.GetAllowedFhaByLenderUserId(model.LenderUserId);
            }

            model.AvailableFHANumbersList = fhaNumberList;
        }

        public JsonResult GetPropertyInfoForIrr(string selectedFhaNumber)
        {
            var result = projectActionFormManager.GetPropertyInfo(selectedFhaNumber);
            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        [HttpPost]
        public ActionResult SubmitIrrForm(FHANumberRequestViewModel model)
        {
            if (model != null)
            {
                model.ProjectTypeId = fhaRequestManager.GetProjectTypebyName("IRR");
                model.TaskinstanceId = Guid.Empty;
                model.RequestStatus = (int)RequestStatus.Approve;
                model.CurrentFHANumber = model.FHANumber;
                model.RequestSubmitDate = DateTime.UtcNow;
                model.IsReadyForApplication = true;
                fhaRequestManager.AddFhaRequestForIrr(model);

                var nextStage = new Prod_NextStageModel();
                nextStage.NextStgId = Guid.NewGuid();
                nextStage.CompletedPgTypeId = (int)PageType.FhaRequest;
                nextStage.NextPgTypeId = (int)PageType.ProductionApplication;
                nextStage.ModifiedOn = DateTime.UtcNow;
                nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                nextStage.FhaNumber = model.FHANumber;
                nextStage.IsNew = true;
                prod_NextStageManager.AddNextStage(nextStage);
                backgroundJobManager.SendIrrInitiationSuccess(new EmailManager(), model, UserPrincipal.Current.UserName);
                return View("~/Views/Production/FHARequest/InitiateIrrRequestForm.cshtml", model);
            }
            return null;
        }

        public string CheckIfIrrExists(string fhaNumber)
        {
            return fhaRequestManager.IsRequestExistsForFhaNumber(fhaNumber) ? "success" : "failure";
        }

        public ActionResult GetAgingFHAReport()
        {
            return View("~/Views/Production/FHARequest/AgingFHAReport.cshtml");
        }
        public JsonResult GetAgingFHAReportList(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;


            var agingReportList = fhaRequestManager.GetFhaAgingReport();



            if (agingReportList != null)
            {
                int totalrecods = agingReportList.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);


                var result = agingReportList.ToList().OrderByDescending(s => s.FhaRequestCompletionDate);
                var results = result.Skip(pageIndex * pageSize).Take(pageSize);

                var jsonData = new
                {
                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = results,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = agingReportList,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }

        public string CheckIfFhaNumberExists(string fhaNumber)
        {
            return fhaRequestManager.IsRequestExistsForFhaNumber(fhaNumber) ? "success" : "failure";
        }
        //#293 Hareesh
        public void PopulateWeeklyFeedFHANumber(FHANumberRequestViewModel model)
        {
            var fhaNumberList = new List<string>();

            fhaNumberList =
                projectActionFormManager.GetAllowedFhaByLender().ToList();
            

            model.AvailableFHANumbersList = fhaNumberList;
        }
    }
}
