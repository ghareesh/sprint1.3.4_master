﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;
using BusinessService.AssetManagement;
using BusinessService;
using Model;
using Model.Production;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using BusinessService.ManagementReport;
using System.Configuration;
using System.Collections;
using Rotativa;
using Repository.Interfaces;
using Repository;
using Repository.Interfaces.Production;
using Repository.Production;


namespace HUDHealthcarePortal.Controllers.Production
{
    public class ProductionApplicationController : Controller
    {

        private IProjectActionFormManager projectActionFormManager;
        private IProjectActionManager projectActionManager;
        private IGroupTaskManager groupTaskManager;
        private ITaskManager taskManager;
        private IAccountManager accountManager;
        private IEmailManager emailManager;
        private IBackgroundJobMgr backgroundJobManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager; // User Story 1901
        private IParentChildTaskManager parentChildTaskManager;
        private IRequestAdditionalInfoFileManager requestAdditionalInfoFilesManager;
        private IReviewFileCommentManager reviewFileCommentManager;
        private IReviewFileStatusManager reviewFileStatusManager;
        private IProd_GroupTasksManager prod_GroupTasksManager;
        private ITaskFile_FolderMappingManager taskFile_FolderMappingManager;
        private IFHANumberRequestManager fhaRequestManager;
        private IAppProcessManager appProcessManager;
        private IProd_TaskXrefManager prod_TaskXrefManager;
        private IProd_RestfulWebApiTokenRequest WebApiTokenRequest;
        private IProd_RestfulWebApiDocumentUpload WebApiDocumentUpload;
        private IProd_RestfulWebApiUpdate prodRestfulWebApiUpdate;
        private IProd_RestfulWebApiReplace WebApiDocumentReplace;
        private IProd_NextStageManager prod_NextStageManager;
        private IProd_DocumentType prod_DocumentType;
        private IProd_RestfulWebApiDownload webApiDownload;
        private IProd_RestfulWebApiDelete WebApiDelete;
        private ISubFolderStructureRepository folderManager;
        //private IProd_SharepointScreenRepository sharepointScreen;
        private IProd_SharepointScreenManager sharepointScreenManager;
        private IProductionQueueManager productionQueueManager;

        private TaskReAssignmentManager taskReAssignmentManager;
        private IOPAManager iOPAForm;
        IPAMReportManager _plmReportMgr;
        IWebSecurityWrapper webSecurity;
        private IProductionQueueManager productionQueue;
        private readonly ILookupManager lookupMgr;
        private string DefaultDocID = ConfigurationManager.AppSettings["DefaultDocId"].ToString();


        public ProductionApplicationController()
            : this(
                new ProjectActionFormManager(), new TaskManager(), new AccountManager(), new EmailManager(),
                new BackgroundJobMgr(), new ProjectActionManager(), new GroupTaskManager(),
                new NonCriticalRepairsRequestManager(), new ParentChildTaskManager(),
                new RequestAdditionalInfoFilesManager(), new ReviewFileCommentManager(), new ReviewFileStatusManager(), new WebSecurityWrapper(), new OPAManager(), new Prod_GroupTasksManager(),
            new TaskFile_FolderMappingManager(), new FHANumberRequestManager(), new PAMReportsManager(), new AppProcessManager(), new Prod_TaskXrefManager(), new ProductionQueueManager(),
            new Prod_RestfulWebApiTokenRequest(), new Prod_RestfulWebApiDocumentUpload(), new Prod_RestfulWebApiUpdate(), new Prod_RestfulWebApiReplace(), new Prod_NextStageManager(),
            new LookupManager(), new Prod_DocumentType(), new Prod_RestfulWebApiDownload(), new Prod_RestfulWebApiDelete(), new SubFolderStructureRepository(),
            //new Prod_SharepointScreenRepository(),
            new Prod_SharepointScreenManager()//skumar-form290
           )
        {

        }

        private ProductionApplicationController(IProjectActionFormManager _projectActionFormManager, ITaskManager _taskManager,
          IAccountManager _accountManager, IEmailManager _emailManager, IBackgroundJobMgr backgroundManager,
          IProjectActionManager _projectActionManager, IGroupTaskManager _groupTaskManager,
          INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
          IParentChildTaskManager _parentChildTaskManager,
          IRequestAdditionalInfoFileManager _requestAdditionalInfoFilesManager,
          IReviewFileCommentManager _reviewFileCommentManager,
           IReviewFileStatusManager _reviewFileStatusManager,
          IWebSecurityWrapper webSec,
         IOPAManager _opaManager,
          IProd_GroupTasksManager _prod_GroupTasksManager, ITaskFile_FolderMappingManager _taskFile_FolderMappingManager, FHANumberRequestManager _fhaRequestManager, IPAMReportManager plmReportMgr,
            IAppProcessManager _appProcessManager, IProd_TaskXrefManager _prodTaskXrefManager, IProductionQueueManager _productionQueueManager, IProd_RestfulWebApiTokenRequest _WebApiTokenRequest,
            IProd_RestfulWebApiDocumentUpload _WebApiDocumentUpload, IProd_RestfulWebApiUpdate _prodRestfulWebApiUpdate, IProd_RestfulWebApiReplace _WebApiDocumentReplace,
            IProd_NextStageManager _prod_NextStageManager, ILookupManager _lookupMgr, IProd_DocumentType _DocumentType, IProd_RestfulWebApiDownload _webApiDownload,
            IProd_RestfulWebApiDelete _WebApiDelete, ISubFolderStructureRepository _folderManager,
            //IProd_SharepointScreenRepository _sharepointScreen, 
            IProd_SharepointScreenManager _sharepointScreenManager
           )
        {
            projectActionFormManager = _projectActionFormManager;
            taskManager = _taskManager;
            accountManager = _accountManager;
            emailManager = _emailManager;
            backgroundJobManager = backgroundManager;
            projectActionManager = _projectActionManager;
            groupTaskManager = _groupTaskManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager; // User Story 1901
            parentChildTaskManager = _parentChildTaskManager;
            requestAdditionalInfoFilesManager = _requestAdditionalInfoFilesManager;
            reviewFileCommentManager = _reviewFileCommentManager;
            reviewFileStatusManager = _reviewFileStatusManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;// User Story 1901
            webSecurity = webSec;
            iOPAForm = _opaManager;
            prod_GroupTasksManager = _prod_GroupTasksManager;
            taskFile_FolderMappingManager = _taskFile_FolderMappingManager;
            fhaRequestManager = _fhaRequestManager;
            _plmReportMgr = plmReportMgr;
            appProcessManager = _appProcessManager;
            prod_TaskXrefManager = _prodTaskXrefManager;
            productionQueue = _productionQueueManager;
            WebApiTokenRequest = _WebApiTokenRequest;
            WebApiDocumentUpload = _WebApiDocumentUpload;
            prodRestfulWebApiUpdate = _prodRestfulWebApiUpdate;
            prod_NextStageManager = _prod_NextStageManager;
            WebApiDocumentReplace = _WebApiDocumentReplace;
            lookupMgr = _lookupMgr;
            prod_DocumentType = _DocumentType;
            webApiDownload = _webApiDownload;
            WebApiDelete = _WebApiDelete;
            folderManager = _folderManager;
            //sharepointScreen = _sharepointScreen;
            sharepointScreenManager = _sharepointScreenManager;
        }


        public ActionResult Index()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.IsIRRequest = false;
            AppProcessViewModel.Isotherloantype = false;
            AppProcessViewModel.IsAddressChange = true;
            InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            AppProcessViewModel.pageTypeId = (int)PageType.ProductionApplication;
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            AppProcessViewModel.TransAccessStatus = checkResult.status;

            return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessViewModel);
        }

        public ActionResult IndexIRR()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.IsIRRequest = true;
            AppProcessViewModel.Isotherloantype = true;
            InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            AppProcessViewModel.pageTypeId = (int)PageType.ProductionApplication;
            return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessViewModel);
        }

        //public ActionResult GetDetail(OPAViewModel AppProcessViewModel)
        //{
        //    AppProcessViewModel.pageTypeId = (int)PageType.ProductionApplication; 
        //    return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessViewModel);
        //}
        [ValidateInput(false)]
        public ActionResult Reload()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();

            string GroupTaskInstanceId = Request.Params[0];
            AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(5);
            //foreach (var model in projectActionList)
            //{
            //    AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
            //    {
            //        Text = model.ProjectActionName,
            //        Value = model.ProjectActionID.ToString()
            //    });
            //}

            var result = projectActionFormManager.GetProdPropertyInfo(AppProcessViewModel.FhaNumber);
            if (result != null)
            {
                AppProcessViewModel.PropertyId = result.PropertyId;
                AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                AppProcessViewModel.PropertyAddress.StateCode = result.State;
                AppProcessViewModel.PropertyAddress.City = result.City;
                AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
            }
            AppProcessViewModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectTypebyID(AppProcessViewModel.ProjectActionTypeId);

            var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            if (grouptaskmodel != null)
            {
                AppProcessViewModel.ServicerComments = grouptaskmodel.ServicerComments;
                AppProcessViewModel.pageTypeId = grouptaskmodel.PageTypeId;
            }
            GetDisclaimerText(AppProcessViewModel);
            //AppProcessViewModel.pageTypeId = (int)PageType.ProductionApplication; 
            if (TempData["UploadFailed"] != null)
            {
                ViewBag.UploadFailed = TempData["UploadFailed"].ToString();
            }

            if (AppProcessViewModel.pageTypeId == (int)PageType.ConstructionSingleStage)
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    AppProcessViewModel.AvailableFHANumbersList = fhaRequestManager.GetReadyforConSingleStageFhas();
                }
                ViewBag.ConstructionTitle = "Construction Single Stage Request";
                return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
            }
            else if (AppProcessViewModel.pageTypeId == (int)PageType.ConstructionTwoStageInitial)
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    AppProcessViewModel.AvailableFHANumbersList = fhaRequestManager.GetReadyforConTwoStageFhas();
                }
                ViewBag.ConstructionTitle = "Construction Two Stage - Initial Stage Request";
                return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
            }
            else if (AppProcessViewModel.pageTypeId == (int)PageType.ConstructionTwoStageFinal)
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    AppProcessViewModel.AvailableFHANumbersList = fhaRequestManager.GetReadyforConTwoStageFhas();
                }
                ViewBag.ConstructionTitle = "Construction Two Stage - Final Stage Request";
                return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
            }

            else if (AppProcessViewModel.pageTypeId == (int)PageType.ConstructionManagement)
            {

                AppProcessViewModel.AvailableFHANumbersList = prod_NextStageManager.GetFhasforNxtStage(9, (int)UserPrincipal.Current.LenderId);

                ViewBag.ConstructionTitle = "Construction Management Request";
                return View("~/Views/Production/ConstructionManagement/LenderConstructionMgmntRequest.cshtml", AppProcessViewModel);
            }
            else if (AppProcessViewModel.pageTypeId == (int)PageType.ClosingAllExceptConstruction)
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    AppProcessViewModel.AvailableFHANumbersList = fhaRequestManager.GetReadyforConTwoStageFhas();
                }
                ViewBag.ConstructionTitle = "Non-Construction Draft Closing";
                return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
            }
            else if (AppProcessViewModel.pageTypeId == (int)PageType.ExecutedClosing)
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    AppProcessViewModel.AvailableFHANumbersList = fhaRequestManager.GetReadyforConTwoStageFhas();
                }
                ViewBag.ConstructionTitle = "Non-Construction Executed Closing";
                return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessViewModel);
            }
            //else if (AppProcessViewModel.pageTypeId == (int)PageType.Amendments)
            //{
            //    ViewBag.ConstructionTitle = "Amendments";
            //    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessViewModel);
            //}
            else
            {
                PopulateFHANumberList(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
                return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessViewModel);
            }
        }


        public ActionResult ReloadlenderReadOnly()
        {
            //Umesh
            OPAViewModel AppProcessViewModel = new OPAViewModel();

            string GroupTaskInstanceId = Request.Params[0];
            //AppProcessViewModel = projectActionFormManager.GetProdOPAByChildTaskId(new Guid(GroupTaskInstanceId));
            ////IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(5);
            ////foreach (var model in projectActionList)
            ////{
            ////    AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
            ////    {
            ////        Text = model.ProjectActionName,
            ////        Value = model.ProjectActionID.ToString()
            ////    });
            ////}

            //var result = projectActionFormManager.GetProdPropertyInfo(AppProcessViewModel.FhaNumber);
            //if (result != null)
            //{
            //    AppProcessViewModel.PropertyId = result.PropertyId;
            //    AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
            //    AppProcessViewModel.PropertyAddress.StateCode = result.State;
            //    AppProcessViewModel.PropertyAddress.City = result.City;
            //    AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
            //}
            //AppProcessViewModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
            //AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectTypebyID(AppProcessViewModel.ProjectActionTypeId);

            //var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            //if (grouptaskmodel != null)
            //{
            //    AppProcessViewModel.ServicerComments = grouptaskmodel.ServicerComments;
            //}

            //PopulateFHANumberList(AppProcessViewModel);
            //GetDisclaimerText(AppProcessViewModel);
            // return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml", AppProcessViewModel);
            return RedirectToAction("GetTaskDetail", "ProductionMyTask", new { taskInstanceId = new Guid(GroupTaskInstanceId) });
        }

        public List<Prod_OPAHistoryViewModel> GetProdOpaHistory(Guid taskInstanceId, string PropertyId, string FhaNumber, int userId, int viewId, bool isEdit)
        {
            return projectActionFormManager.GetProdOpaHistory(taskInstanceId, PropertyId, FhaNumber, userId, viewId, isEdit);

        }


        public JsonResult GetProdOpaHistoryGridContent(string sidx, string sord, int page, int rows, Guid taskInstanceId, string PropertyId, string FhaNumber)
        {

            int userId = UserPrincipal.Current.UserId;

            int viewId = 1;

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            // List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();

            var GridContent = GetProdOpaHistory(taskInstanceId, PropertyId, FhaNumber, userId, viewId, false);

            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                }

            }

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);

            var result = GridContent.ToList().OrderBy(s => s.id);
            //var result = GridContent.ToList().OrderBy(a => a.folderNodeName);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);



            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        //Umesh


        public JsonResult GetWPUpload(string sidx, string sord, int page, int rows, Guid? taskInstanceId, int Status, int PageTypeId)
        {
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            var GridContent = projectActionFormManager.GetGetWPUpload(taskInstanceId.Value, Status, PageTypeId);

            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    if (!string.IsNullOrEmpty(item.parent))
                    {
                        var DocTypeList = appProcessManager.GetMappedDocTypesbyFolder(Convert.ToInt32(item.parent));

                        if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                        {

                            item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                        }

                        //item.DocTypesList = "<select data-val='true' data-val-number='The field BorrowerTypeId must be a number." +
                        //                "data-val-required='The BorrowerTypeId field is required.' id=_'ddlDocType'  name='BorrowerTypeId'>" +
                        //                "<option value=''>Select Document Type</option>" +
                        //                "<option value='1'>Profit</option>" +
                        //                "<option value='2'>Non-Profit</option></select>";
                    }


                }

            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalrecods = GridContent != null ? GridContent.Count() : 0;
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);


            var result = GridContent != null ? GridContent.ToList().OrderBy(a => a.id) : null;
            var results = result != null ? result.Skip(pageIndex * pageSize).Take(pageSize) : result;



            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetProdLenderUploadFileGridContent(string sidx, string sord, int page, int rows, Guid? taskInstanceId, string PropertyId, string FhaNumber, string FormName)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            var GridContent = projectActionFormManager.GetProdLenderUpload(taskInstanceId.Value, PropertyId, FhaNumber);


            if (FormName == "10")
            {

                GridContent = GridContent.Where(x => x.folderNodeName.Trim() == "Section 17: Closing" || x.parent == "30" || x.fileId != null).ToList();
                GridContent.RemoveAll(x => x.id == "5");
            }
            else if (FormName == "17")
            {

                GridContent = GridContent.Where(x => x.folderNodeName.Trim() == "Section 17: Closing" || x.parent == "30" || x.fileId != null).ToList();
                GridContent.RemoveAll(x => x.id == "5" || ((x.PageTypeId != int.Parse(FormName) && x.id != "30") && x.parent != "30"));
            }
            else
            {

                GridContent.RemoveAll(x => x.id == "30");
            }

            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                GridContent = GridContent.Where(x => x.PageTypeId == Convert.ToInt32(FormName) || x.PageTypeId == 0).ToList();
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    if (item.fileId != null)
                    {
                        var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
                        if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                        || currentUserRole == "BackupAccountManager")
                            item.isUploadedByMe = true;
                    }
                    if (!string.IsNullOrEmpty(item.parent))
                    {
                        var DocTypeList = appProcessManager.GetMappedDocTypesbyFolder(Convert.ToInt32(item.parent));

                        var doc = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                        //harish added 00042020
                        if (item.DocTypeID == "2082" && (item.fileName.ToUpper().StartsWith("OTHERS_") || item.fileName.ToUpper().StartsWith("OTHER_")))
                        {
                            // var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.shortname_id.Split('_').First()));
                            var matched = item.fileName.Split('_').Last();
                            // item.fileName = matched;
                            var matchedinfo = DocTypeList.Where(x => item.fileName.Split('_').Last().Contains(x.DocumentType));
                            if (matchedinfo.Any())
                            {
                                item.DocTypeID = matchedinfo.First().DocumentTypeId.ToString();
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {

                                    item.fileName = taskfile.FileName;
                                    taskfile.DocTypeID = item.DocTypeID;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                    // taskManager.UpdateFileNameInTaskFile(taskfile);
                                }
                            }
                            else
                            {
                                var matchedfile = item.fileName.Substring(7, item.fileName.Length - 7);

                                var matchedinfodata = DocTypeList.Where(x => matchedfile.Contains(x.ShortDocTypeName + "_" + x.DocumentType));
                                if (matchedinfodata.Any())
                                {
                                    item.DocTypeID = matchedinfodata.First().DocumentTypeId.ToString();
                                    var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                    if (taskfile != null)
                                    {

                                        item.fileName = taskfile.FileName;
                                        taskfile.DocTypeID = item.DocTypeID;
                                        taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        // taskManager.UpdateFileNameInTaskFile(taskfile);
                                    }
                                }
                            }
                            // item.fileName = matched;
                            // item.folderNodeName = matched;
                            // item.folderNodeName = matched;
                            //var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                            //if (taskfile != null)
                            //{

                            //    taskfile.FileName = item.fileName;
                            //    taskManager.UpdateFileNameInTaskFile(taskfile);
                            //}
                        }



                        if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                        {
                            //harish new line of code for checking file name and document(shortnameid) 09-04-2020
                            if (DocTypeList.Where(x => item.fileName.Contains(x.ShortDocTypeName + "_" + x.DocumentType)).Any())
                            //if (DocTypeList.Where(x => x.shortname_id.Contains(item.fileName)).Any())
                            {
                                item.fileName = item.fileName;
                                item.folderNodeName = item.fileName;
                                var matchedinfo = DocTypeList.Where(x => item.fileName.Contains(x.ShortDocTypeName + "_" + x.DocumentType)).First();
                                item.DocTypeID = matchedinfo.DocumentTypeId.ToString();
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    taskfile.FileName = item.fileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);
                                }

                            }

                            else
                            {
                                //DocTypeList.Where(x => item.fileName.Contains(x.ShortDocTypeName + "_" + x.DocumentType)).First();
                                var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.ShortDocTypeName + "_" + x.DocumentType.Split('_').First()));
                                if (matched.Any())
                                {

                                   // item.fileName = matched.First().ShortDocTypeName + "_" + matched.First().DocumentType;
                                    item.DocTypeID = matched.First().DocumentTypeId.ToString();
                                    var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                    if (taskfile != null)
                                    {
                                        taskfile.DocTypeID = item.DocTypeID;
                                       // string ext = Path.GetExtension(taskfile.FileName);
                                        taskfile.FileName = item.fileName;
                                        taskManager.UpdateDoctyidinTaskfile(taskfile);
                                    }
                                }
                                else
                                {
                                    item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                                }
                            }



                            //var matchedinfo = DocTypeList.Where(x => item.fileName.Contains(x.ShortDocTypeName + "_" + x.DocumentType)).First();


                            //item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                        }

                        //item.DocTypesList = "<select data-val='true' data-val-number='The field BorrowerTypeId must be a number." +
                        //                "data-val-required='The BorrowerTypeId field is required.' id=_'ddlDocType'  name='BorrowerTypeId'>" +
                        //                "<option value=''>Select Document Type</option>" +
                        //                "<option value='1'>Profit</option>" +
                        //                "<option value='2'>Non-Profit</option></select>";

                    }
                    else
                    {
                        if (item.SubfolderSequence != null)
                        {
                            item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                        }
                    }


                }

            }

            //  int pageIndex = Convert.ToInt32(page) - 1;
            // int pageSize = rows;
            int totalrecods = GridContent.Count();

            //totalrecods = totalrecods / 2;
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            //if (FormName == "10")
            //{

            //    GridContent = GridContent.Where(x => x.id == "5").ToList();
            //}


            var results = GridContent.Skip(pageIndex * pageSize).Take(pageSize);





            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,
                // rows = jsondat1,

            };
            // return Json(jsonData, JsonRequestBehavior.AllowGet);
            var jsonresult = Json(jsonData, JsonRequestBehavior.AllowGet);
            jsonresult.MaxJsonLength = int.MaxValue;//10022020
            return jsonresult;

        }

        [ValidateInput(false)]
        public ActionResult SaveDropzoneJsUploadedFiles(FolderUploadModel fileFolderModel)
        {
            OPAViewModel AppProcessModel = new OPAViewModel();
            int userID = 0;
            string userModel = string.Empty;
            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;
            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            string value = string.Empty;
            string GroupTaskInstanceId = string.Empty;
            int pageTypeId = 0;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();
            //Logic for check  whether a task Exists
            var parentdict = GetParentModelDictionary(ParentModel);
            bool IsTaskavilable = false;

            try
            {
                if (parentdict.Count > 0)
                {

                    if (parentdict.ContainsKey("GroupTaskInstanceId") && !string.IsNullOrEmpty((string)parentdict["GroupTaskInstanceId"]))
                    {
                        GroupTaskInstanceId = (string)parentdict["GroupTaskInstanceId"];

                    }
                    if (parentdict.ContainsKey("pageTypeId") && !string.IsNullOrEmpty((string)parentdict["pageTypeId"]))
                    {

                        pageTypeId = Convert.ToInt32((string)parentdict["pageTypeId"]);

                    }
                    var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(new Guid(GroupTaskInstanceId));
                    if (grouptaskmodel == null)
                    {
                        var prodgroupTaskModel = new Prod_GroupTasksModel();
                        prodgroupTaskModel.TaskInstanceId = new Guid(GroupTaskInstanceId);
                        prodgroupTaskModel.InUse = UserPrincipal.Current.UserId;

                        prodgroupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        prodgroupTaskModel.PageTypeId = pageTypeId;
                        //prodgroupTaskModel.PageTypeId = (int) PageType.ProductionApplication;
                        prodgroupTaskModel.IsDisclaimerAccepted = false;
                        prodgroupTaskModel.ModifiedOn = DateTime.UtcNow;
                        prodgroupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        prodgroupTaskModel.CreatedOn = DateTime.UtcNow;
                        prodgroupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        prodgroupTaskModel.ServicerComments = parentdict["ServicerComments"] != null ? (string)parentdict["ServicerComments"] : "";
                        var ProdGroupTaskId = prod_GroupTasksManager.AddProdGroupTasks(prodgroupTaskModel);

                        //Save OpaForm

                        AppProcessModel.ProjectActionName = parentdict["ProjectActionName"] != null ? (string)parentdict["ProjectActionName"] : "";

                        AppProcessModel.ServicerComments = "";


                        AppProcessModel.PropertyName = parentdict["PropertyName"] != null ? (string)parentdict["PropertyName"] : "";
                        AppProcessModel.RequesterName = parentdict["RequesterName"] != null ? (string)parentdict["RequesterName"] : "";
                        AppProcessModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        //  var  FHA = Request.Form["FHA"];
                        AppProcessModel.IsAddressChange = parentdict["IsAddressChange"] != null ? Convert.ToBoolean(parentdict["IsAddressChange"]) : false;
                        //model.FhaNumber = FHA;
                        //Insert the non critical request and get the generated id, insert into view model
                        AppProcessModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : ""; ;
                        //AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        AppProcessModel.ServicerSubmissionDate = null;
                        AppProcessModel.RequestStatus = (int)RequestStatus.Draft;
                        AppProcessModel.CreatedOn = DateTime.UtcNow;
                        AppProcessModel.ModifiedOn = DateTime.UtcNow;
                        AppProcessModel.CreatedBy = UserPrincipal.Current.UserId;
                        AppProcessModel.ModifiedBy = UserPrincipal.Current.UserId;
                        AppProcessModel.ProjectActionDate = DateTime.UtcNow;
                        AppProcessModel.RequestDate = DateTime.UtcNow;
                        AppProcessModel.TaskInstanceId = ProdGroupTaskId;
                        AppProcessModel.pageTypeId = pageTypeId;
                        AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        var projectActionFormId = projectActionFormManager.SaveOPAForm(AppProcessModel);

                        InstanceID = ProdGroupTaskId;
                        AppProcessModel.GroupTaskInstanceId = InstanceID;
                        AppProcessModel.ProjectActionFormId = projectActionFormId;
                    }
                    else
                    {
                        AppProcessModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        AppProcessModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : "";
                        AppProcessModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
                        AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        //skumar-form290 requires following information
                        AppProcessModel.pageTypeId = pageTypeId;
                        if (parentdict.ContainsKey("ModifiedBy"))
                        {
                            if (parentdict["ModifiedBy"] != null)
                            {
                                userModel = (string)parentdict["ModifiedBy"];
                                if (string.IsNullOrEmpty(userModel))
                                    userModel = (string)parentdict["CreatedBy"];
                                userID = Convert.ToInt32(userModel);
                            }
                            AppProcessModel.userId = userID;
                        }

                    }
                }
                var success = this.Upload(AppProcessModel, Convert.ToInt32(FolderKey));

                if (success)
                {
                    var nextStage = new Prod_NextStageModel();

                    nextStage.NextPgTypeId = pageTypeId;
                    nextStage.ModifiedOn = DateTime.UtcNow;
                    nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                    nextStage.FhaNumber = AppProcessModel.FhaNumber;
                    nextStage.NextPgTaskInstanceId = AppProcessModel.GroupTaskInstanceId;
                    nextStage.IsNew = false;
                    prod_NextStageManager.UpdateTaskInstanceIdForNxtStage(nextStage);
                    // Venkatesh They are passing the page type id by default as production application I cahnge it dinamic
                    //AppProcessModel.pageTypeId = (int)PageType.ProductionApplication;
                    AppProcessModel.pageTypeId = pageTypeId;

                }
                else
                {
                    ViewBag.UploadFailed = "True";
                    TempData["UploadFailed"] = "True";

                }
                if (pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessModel);
                }
                else
                {
                    return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessModel);
                }
            }
            catch (Exception e)
            {
                //todo:skumar: redirect to error page
                return RedirectToAction("Login", "Account");
            }
        }

        //Code for WP Grid Change Umesh

        [ValidateInput(false)]
        public bool SaveDropzoneJsUploadedFilesHUD(FolderUploadModel fileFolderModel)
        {
            OPAViewModel AppProcessModel = new OPAViewModel();

            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;
            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            string value = string.Empty;
            string GroupTaskInstanceId = string.Empty;
            int pageTypeId = 0;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();
            //Logic for check  whether a task Exists
            var parentdict = GetParentModelDictionary(ParentModel);
            bool IsTaskavilable = false;

            if (parentdict.Count > 0)
            {

                AppProcessModel.ProjectActionName = parentdict["ProjectActionName"] != null ? (string)parentdict["ProjectActionName"] : "";

                AppProcessModel.ServicerComments = "";


                AppProcessModel.PropertyName = parentdict["PropertyName"] != null ? (string)parentdict["PropertyName"] : "";
                AppProcessModel.RequesterName = parentdict["RequesterName"] != null ? (string)parentdict["RequesterName"] : "";
                AppProcessModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                //  var  FHA = Request.Form["FHA"];
                AppProcessModel.IsAddressChange = parentdict["IsAddressChange"] != null ? Convert.ToBoolean(parentdict["IsAddressChange"]) : false;
                //model.FhaNumber = FHA;
                //Insert the non critical request and get the generated id, insert into view model
                AppProcessModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : ""; ;
                AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                AppProcessModel.ServicerSubmissionDate = null;
                AppProcessModel.RequestStatus = (int)RequestStatus.Draft;
                AppProcessModel.CreatedOn = DateTime.UtcNow;
                AppProcessModel.ModifiedOn = DateTime.UtcNow;
                AppProcessModel.CreatedBy = UserPrincipal.Current.UserId;
                AppProcessModel.ModifiedBy = UserPrincipal.Current.UserId;
                AppProcessModel.ProjectActionDate = DateTime.UtcNow;
                AppProcessModel.RequestDate = DateTime.UtcNow;
                AppProcessModel.pageTypeId = parentdict["pageTypeId"] != null ? Convert.ToInt32(parentdict["pageTypeId"]) : 0;
                //  AppProcessModel.TaskInstanceId = ProdGroupTaskId;
                AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                var projectActionFormId = projectActionFormManager.SaveOPAForm(AppProcessModel);


                AppProcessModel.GroupTaskInstanceId = new Guid((string)parentdict["TaskGuid"]);
                AppProcessModel.ProjectActionFormId = projectActionFormId;


                AppProcessModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;





            }
            var success = this.Upload(AppProcessModel, Convert.ToInt32(FolderKey));

            if (success)
            {
                isSavedSuccessfully = true;
            }
            else
            {
                isSavedSuccessfully = false;
                ViewBag.UploadFailed = "True";
                TempData["UploadFailed"] = "True";

            }
            //var files = new List<ViewDataUploadFilesResult>();
            //foreach (string fileName in Request.Files)
            //{
            //    HttpPostedFileBase file = Request.Files[fileName];

            //    //You can Save the file content here

            //    isSavedSuccessfully = true;
            //    files.Add(new ViewDataUploadFilesResult()
            //    {
            //        name = file.FileName,
            //        size = file.ContentLength,
            //        type = file.ContentType,
            //        url = "/Home/Download/" + file.FileName,
            //        delete_url = "/Home/Delete/" + file.FileName,
            //        thumbnail_url = "",
            //        delete_type = "GET",
            //    });
            //}

            //return RedirectToAction("Index");
            return isSavedSuccessfully;
        }

        //Code for Uploading


        [ValidateInput(false)]
        public ActionResult SaveLenderRAIDropzoneJsUploadedFiles(FolderUploadModel fileFolderModel)
        {
            OPAViewModel AppProcessModel = new OPAViewModel();

            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;
            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            string value = string.Empty;
            string GroupTaskInstanceId = string.Empty;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();
            //Logic for check  whether a task Exists
            var parentdict = GetParentModelDictionary(ParentModel);
            bool IsTaskavilable = false;

            if (parentdict.Count > 0)
            {

                if (parentdict.ContainsKey("TaskGuid") && !string.IsNullOrEmpty((string)parentdict["TaskGuid"]))
                {
                    GroupTaskInstanceId = (string)parentdict["TaskGuid"];

                }

                var task = taskManager.GetLatestTaskByTaskInstanceId(new Guid(GroupTaskInstanceId));
                AppProcessModel = projectActionFormManager.GetProdOPAByChildTaskId(task.TaskInstanceId);
                AppProcessModel.viewId = 0;
                AppProcessModel.IsLoggedInUserLender = true;
                AppProcessModel.GroupTaskInstanceId = task.TaskInstanceId;
                AppProcessModel.TaskGuid = task.TaskInstanceId;
                //  formUploadData.taskxrefId = taskInstanceId;
                AppProcessModel.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                AppProcessModel.SequenceId = task.SequenceId;
                AppProcessModel.AssignedBy = task.AssignedBy;
                AppProcessModel.AssignedTo = task.AssignedTo;
                AppProcessModel.IsReassigned = task.IsReAssigned;
                AppProcessModel.TaskId = task.TaskId;
                AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                AppProcessModel.TaskStepId = task.TaskStepId;
                AppProcessModel.pageTypeId = (int)task.PageTypeId;
                AppProcessModel.LenderComment = parentdict["LenderComment"] != null ? (string)parentdict["LenderComment"] : "";

            }
            //Code for Uploading
            //umesh check and changing the save to the parentaskinstanceID
            if (parentChildTaskManager.IsParentTaskAvailable(AppProcessModel.GroupTaskInstanceId.Value))
            {
                AppProcessModel.GroupTaskInstanceId = projectActionFormManager.GetProdOPAByChildTaskId(AppProcessModel.GroupTaskInstanceId.Value).TaskInstanceId;
            }
            //AppProcessModel.GroupTaskInstanceId = AppProcessModel.TaskInstanceId;
            var success = this.Upload(AppProcessModel, Convert.ToInt32(FolderKey));
            if (!success)
            {
                ViewBag.UploadFailed = "True";
                TempData["UploadFailed"] = "True";
            }
            else
            {
                AppProcessModel.ChildTaskInstanceId = AppProcessModel.GroupTaskInstanceId.Value;

                var subTasks = prod_TaskXrefManager.GetProductionSubTasks(AppProcessModel.TaskInstanceId);
                if (subTasks != null)
                {
                    foreach (var xref in subTasks)
                    {
                        // Save ReviewFileStatus
                        int reviewerUserId = (int)xref.AssignedTo;
                        int currentUserId = UserPrincipal.Current.UserId;
                        taskManager.SaveReviewFileStatus(AppProcessModel.ChildTaskInstanceId, reviewerUserId, currentUserId, xref.ViewId);
                    }
                }
            }
            //Review file status Save



            return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml", AppProcessModel);
            // return RedirectToAction("GetTaskDetail","ProductionMyTask", new   { taskInstanceId= AppProcessModel.TaskGuid   });



        }

        public class ViewDataUploadFilesResult
        {
            public string name { get; set; }
            public int size { get; set; }
            public string type { get; set; }
            public string url { get; set; }
            public string delete_url { get; set; }
            public string thumbnail_url { get; set; }
            public string delete_type { get; set; }
        }

        public string CheckIfProjectActionExistsForFhaNumber(string fhaNumber)
        {
            var result = projectActionFormManager.GetProdOPAByFha(fhaNumber);

            if (result != null)
            {
                return result.RequestStatus.ToString();
            }
            else
            {

                return "NA";

            }

        }

        private void InitializeViewModel(OPAViewModel AppProcessViewModel, bool IsIRR)
        {
            ViewBag.SaveCheckListFormURL = Url.Action("ApproveCheckListForm");
            PopulateFHANumberList(AppProcessViewModel, IsIRR);
            AppProcessViewModel.IsEditMode = false;
            var projectActionList = appProcessManager.GetAllotherloanTypes();
            if (IsIRR)
            {
                foreach (var model in projectActionList)
                {
                    AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
                    {
                        Text = model.ProjectTypeName,
                        Value = model.ProjectTypeId.ToString()
                    });
                }
            }

            AppProcessViewModel.LenderId = UserPrincipal.Current.UserData.LenderId.Value;
            AppProcessViewModel.LenderName =
                projectActionFormManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            AppProcessViewModel.RequesterName = UserPrincipal.Current.FullName;
            AppProcessViewModel.ProjectActionDate = DateTime.Today;
            AppProcessViewModel.ServicerSubmissionDate = null;
            AppProcessViewModel.IsAgreementAccepted = false;
            AppProcessViewModel.RequestDate = DateTime.UtcNow;
            AppProcessViewModel.CreatedBy = UserPrincipal.Current.UserId;
            AppProcessViewModel.CreatedOn = DateTime.UtcNow;
            //Initiate the new grouptaskinstanceid
            AppProcessViewModel.GroupTaskInstanceId = Guid.NewGuid();
            AppProcessViewModel.ModifiedOn = DateTime.UtcNow;
            AppProcessViewModel.ModifiedBy = UserPrincipal.Current.UserId;
            IList<TaskFileModel> TasFilelist = new List<TaskFileModel>();
            AppProcessViewModel.TaskFileList = TasFilelist;
            AppProcessViewModel.IsViewFromGroupTask = false;
            List<OPAHistoryViewModel> History = new List<OPAHistoryViewModel>();
            AppProcessViewModel.OpaHistoryViewModel = History;
            // User Story 1901
            GetDisclaimerText(AppProcessViewModel);

        }

        private void PopulateFHANumberList(OPAViewModel AppProcessViewModel, bool IsIRR)
        {
            var fhaNumberList = new List<string>();
            if (!IsIRR)
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    fhaNumberList = fhaRequestManager.GetReadyforAppFhas();

                }
            }

            else
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    fhaNumberList =
                        projectActionFormManager.GetAllowedFhaByLenderUserId(UserPrincipal.Current.UserId);
                }

            }

            AppProcessViewModel.AvailableFHANumbersList = fhaNumberList;
        }

        private void GetDisclaimerText(OPAViewModel model)
        {
            model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("APPLICATION REQUEST");
        }

        public ActionResult LenderSubmit(OPAViewModel AppProcessViewModel, string submitButton, string submitButton1)
        {
            //skumar- remove duplicate fha numbers
            if (!string.IsNullOrEmpty(AppProcessViewModel.FhaNumber))
            {
                var fhaNumbers = AppProcessViewModel.FhaNumber.Split(',');
                if (fhaNumbers.Length > 0)
                {
                    AppProcessViewModel.FhaNumber = fhaNumbers[0];
                }
            }
            if (submitButton1 == "Submit")
            {
                submitButton = submitButton1;
            }
            switch (submitButton)
            {
                case "Submit":

                    return (Submit(AppProcessViewModel));

                case "Save":

                    return Save(AppProcessViewModel);
                case @"Save and Check-in":

                    return (SaveCheckin(AppProcessViewModel));

                default:

                    return (SaveCheckin(AppProcessViewModel));
            }
        }
        [HttpGet]
        private ActionResult Submit(OPAViewModel AppProcessModel)
        {
            var FilesUploaded = taskManager.GetAllTaskFileByTaskInstanceId((Guid)AppProcessModel.GroupTaskInstanceId);
            if (FilesUploaded != null && !FilesUploaded.Any())
            {
                var fhaNumber = AppProcessModel.FhaNumber;
                if (fhaNumber != null && fhaNumber.Contains(","))
                {
                    AppProcessModel.FhaNumber = fhaNumber.Split(',')[0];
                }
                AppProcessModel.IsFileUploaded = true;
                TempData["AppProcessModel"] = AppProcessModel;

                if (AppProcessModel.pageTypeId == 10 || AppProcessModel.pageTypeId == 17)
                {
                    return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", AppProcessModel);
                }
                if (AppProcessModel.pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessModel);
                }
                else
                {
                    return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessModel);
                }
            }
            var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(AppProcessModel.TaskInstanceId)
                            select new
                            {
                                filelist.FileId,
                                filelist.DocTypeID,
                                docId = filelist.DocId,
                                Name = filelist.FileName
                            }).Where(m => string.IsNullOrEmpty(m.DocTypeID));


            if (FileList.Count() > 0)
            {
                //if(AppProcessModel.pageTypeId==18)
                AppProcessModel.IsChangeDocTypePopupDisplayed = true;
                TempData["AppProcessModel"] = AppProcessModel;
                if (AppProcessModel.pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessModel);
                }
                else
                {
                    return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessModel);
                }
            }

            else if (ModelState.IsValid)
            {
                //  var task = taskManager.GetLatestTaskByTaskInstanceId(AppProcessModel.GroupTaskInstanceId.Value);
                // if (task == null )
                //{
                var model = _plmReportMgr.GetOPARequestId(AppProcessModel.ProjectActionFormId);

                AppProcessModel.ServicerSubmissionDate = DateTime.UtcNow;
                AppProcessModel.RequestStatus = (int)RequestStatus.Submit;
                AppProcessModel.CreatedOn = DateTime.UtcNow;
                AppProcessModel.ModifiedOn = DateTime.UtcNow;
                AppProcessModel.CreatedBy = UserPrincipal.Current.UserId;
                AppProcessModel.ModifiedBy = UserPrincipal.Current.UserId;
                AppProcessModel.ProjectActionDate = DateTime.UtcNow;
                AppProcessModel.RequestDate = DateTime.UtcNow;
                if (model == null)
                {
                    AppProcessModel.TaskInstanceId = AppProcessModel.GroupTaskInstanceId.Value;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(AppProcessModel);
                    AppProcessModel.ProjectActionFormId = projectActionFormId;

                    this.SaveTask(false, AppProcessModel);
                }
                else
                {
                    this.SaveTask(true, AppProcessModel);
                    AppProcessModel.TaskInstanceId = AppProcessModel.GroupTaskInstanceId.Value;
                    projectActionFormManager.UpdateProjectActionRequestForm(AppProcessModel);

                }
                // }


                AppProcessModel.IsChangeDocTypePopupDisplayed = false;

            }
            //string alertTitle = String.Empty;
            //alertTitle = "Application Request Submited Successfully";
            //TempData["PopupText"] = "Application Request Submited Successfully.";
            //PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
            //                  true, "GenericPopup");

            if (!AppProcessModel.IsAddressChange)
            {
                backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager,
                    "The property address is not listed correctly",
                          AppProcessModel.PropertyName, AppProcessModel.FhaNumber);
            }

            //karri#830//commented as this auto closing(identified by amar 13apr2020) is not required
            //// Project details closing and pdf generate
            //if (AppProcessModel.pageTypeId == 17)
            //{
            //    var pdfgenerated = GenerateSharepointPdf(AppProcessModel.TaskInstanceId, AppProcessModel.FhaNumber, AppProcessModel.PropertyId);
            //    Prod_Assainedto Assainedto = new Prod_Assainedto();
            //    Assainedto = projectActionFormManager.AssignedTo(AppProcessModel.pageTypeId, AppProcessModel.FhaNumber);
            //    AppProcessModel.AssignedTo = Assainedto.UserName;
            //    taskManager.SaveReviewFileStatus(AppProcessModel.TaskInstanceId, UserPrincipal.Current.UserId, UserPrincipal.Current.UserId, AppProcessModel.viewId);

            //}

            backgroundJobManager.SendAppProcessSubmitEmail(new EmailManager(), AppProcessModel);
            AppProcessModel.IsApprovedPopupDisplayedForAe = true;
            AppProcessModel.PopupTarget = "#divsubmit";
            if (AppProcessModel.pageTypeId == 18)
            {
                // return RedirectToAction("Index", "ProductionGroupTask"); 

                return View("~/Views/Production/Amendments/Index.cshtml", AppProcessModel);
            }
            else
            {
                //AppProcessModel.pageTypeId = (int)PageType.ProductionApplication;
                return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessModel);
            }

        }

        private void SaveTask(bool Exists, OPAViewModel AppProcessModel)
        {
            Guid? InstanceId = Guid.NewGuid();
            var taskList = new List<TaskModel>();
            var task = new TaskModel();
            ProductionTaskAssignmentModel model = new ProductionTaskAssignmentModel();
            if (Exists)
            {
                InstanceId = AppProcessModel.GroupTaskInstanceId;
            }

            task.AssignedBy = UserPrincipal.Current.UserName;

            task.Notes = AppProcessModel.ServicerComments ?? string.Empty;
            task.SequenceId = 0;
            task.StartTime = DateTime.UtcNow;
            //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
            task.MyStartTime = task.StartTime;
            task.TaskInstanceId = InstanceId.Value;
            if (AppProcessModel.FhaNumber.Length > 9 && AppProcessModel.FhaNumber.Contains(','))
            {
                task.FHANumber = AppProcessModel.FhaNumber.Substring(0, 9);
            }
            else
            {
                task.FHANumber = AppProcessModel.FhaNumber;
            }

            //karri#830//commented as this auto closing(identified by amar 13apr2020) is not required
            //if (AppProcessModel.pageTypeId == 17)
            //{
            //    Prod_Assainedto Assainedto = new Prod_Assainedto();
            //    Assainedto = projectActionFormManager.AssignedTo(AppProcessModel.pageTypeId, task.FHANumber);
            //    task.AssignedTo = Assainedto.UserName;
            //    task.TaskStepId = 15;


            //    var currentUser = UserPrincipal.Current;

            //    model.CurrentUserId = currentUser.UserId;
            //    model.UserName = currentUser.UserName;
            //    model.Status = (int)ProductionFhaRequestStatus.Completed;
            //    model.AssignedTo = Assainedto.UserID;
            //    model.AssignedBy = currentUser.UserId;
            //    model.AssignedToUserId = currentUser.UserId;
            //    model.AssignedToViewId = 1;
            //    model.ModifiedBy = currentUser.UserId;
            //    model.ModifiedOn = DateTime.UtcNow;
            //    model.CompletedOn = DateTime.UtcNow;
            //    model.Status = 15;
            //    model.ViewId = 1;
            //    model.IsReviewer = false;
            //    model.TaskXrefid = Guid.NewGuid();
            //    model.TaskInstanceId = task.TaskInstanceId;
            //    //if (model.TaskInstanceId == Guid.Empty)
            //    //    prod_TaskXrefManager.AssignProductionFhaRequest(model);
            //    //else productionQueue.AssignProductionFhaInsert(model);


            //}
            //else
            {
                task.AssignedTo = null;
                task.TaskStepId = 17;
            }

            //task.AssignedTo = null;
            task.PageTypeId = AppProcessModel.pageTypeId;
            //task.PageTypeId = (int)PageType.ProductionApplication; 
            //task.TaskStepId = (int)ProductionFhaRequestStatus.InQueue;
            //task.TaskStepId = 17;
            taskList.Add(task);
            var taskFile = new TaskFileModel();
            if (taskList.Count > 0)
            {
                taskManager.SaveTask(taskList, taskFile);


            }

            //karri#830//commented as this auto closing(identified by amar 13apr2020) is not required
            //if (AppProcessModel.pageTypeId == 17)
            //{
            //    string TaskId = projectActionFormManager.GetTaskId(AppProcessModel.pageTypeId, task.FHANumber, model.TaskInstanceId);
            //    model.TaskId = Convert.ToInt32(TaskId);
            //    prod_TaskXrefManager.AddTaskXref(model);

            //    //var pdfgenerated = GenerateSharepointPdf(model.TaskInstanceId, task.FHANumber, model.PropertyId);
            //}

        }
        private ActionResult Save(OPAViewModel AppProcessModel)
        {
            if (ModelState.IsValid)
            {
                var model = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(AppProcessModel.GroupTaskInstanceId.Value);
                if (model == null)
                {
                    var prodgroupTaskModel = new Prod_GroupTasksModel();
                    prodgroupTaskModel.TaskInstanceId = AppProcessModel.GroupTaskInstanceId.Value;
                    prodgroupTaskModel.InUse = UserPrincipal.Current.UserId;

                    prodgroupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                    prodgroupTaskModel.PageTypeId = AppProcessModel.pageTypeId;
                    //prodgroupTaskModel.PageTypeId = (int)PageType.ProductionApplication; ;
                    prodgroupTaskModel.IsDisclaimerAccepted = false;
                    prodgroupTaskModel.ModifiedOn = DateTime.UtcNow;
                    prodgroupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                    prodgroupTaskModel.CreatedOn = DateTime.UtcNow;
                    prodgroupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                    prodgroupTaskModel.ServicerComments = AppProcessModel.ServicerComments;
                    var ProdGroupTaskId = prod_GroupTasksManager.AddProdGroupTasks(prodgroupTaskModel);

                    //Opa Save
                    AppProcessModel.ServicerSubmissionDate = null;
                    AppProcessModel.RequestStatus = (int)RequestStatus.Draft;
                    AppProcessModel.CreatedOn = DateTime.UtcNow;
                    AppProcessModel.ModifiedOn = DateTime.UtcNow;
                    AppProcessModel.CreatedBy = UserPrincipal.Current.UserId;
                    AppProcessModel.ModifiedBy = UserPrincipal.Current.UserId;
                    AppProcessModel.ProjectActionDate = DateTime.UtcNow;
                    AppProcessModel.RequestDate = DateTime.UtcNow;
                    AppProcessModel.TaskInstanceId = ProdGroupTaskId;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(AppProcessModel);
                }

                else
                {

                    AppProcessModel.TaskInstanceId = model.TaskInstanceId;
                    prod_GroupTasksManager.UpdateProdGroupTask(AppProcessModel);
                    projectActionFormManager.UpdateProjectActionRequestForm(AppProcessModel);
                }


            }
            AppProcessModel.IsApprovedPopupDisplayedForAe = true;
            AppProcessModel.PopupTarget = "#divHtml";
            if (AppProcessModel.pageTypeId == 18)
            {
                // return RedirectToAction("Index", "ProductionGroupTask");                
                return View("~/Views/Production/Amendments/Index.cshtml", AppProcessModel);
            }
            else
            {
                // return RedirectToAction("Index", "ProductionGroupTask"); 
                //AppProcessModel.pageTypeId = (int)PageType.ProductionApplication;
                return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessModel);
            }
        }
        private ActionResult SaveCheckin(OPAViewModel AppProcessModel)
        {

            if (ModelState.IsValid)
            {
                var model = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(AppProcessModel.GroupTaskInstanceId.Value);
                if (model == null)
                {
                    var prodgroupTaskModel = new Prod_GroupTasksModel();
                    prodgroupTaskModel.TaskInstanceId = AppProcessModel.GroupTaskInstanceId.Value;
                    prodgroupTaskModel.InUse = 0;

                    prodgroupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                    prodgroupTaskModel.PageTypeId = AppProcessModel.pageTypeId;
                    //prodgroupTaskModel.PageTypeId = (int)PageType.ProductionApplication; 
                    prodgroupTaskModel.IsDisclaimerAccepted = false;
                    prodgroupTaskModel.ModifiedOn = DateTime.UtcNow;
                    prodgroupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                    prodgroupTaskModel.CreatedOn = DateTime.UtcNow;
                    prodgroupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                    prodgroupTaskModel.ServicerComments = AppProcessModel.ServicerComments;
                    var ProdGroupTaskId = prod_GroupTasksManager.AddProdGroupTasks(prodgroupTaskModel);

                    //Opa Save
                    AppProcessModel.ServicerSubmissionDate = null;
                    AppProcessModel.RequestStatus = (int)RequestStatus.Draft;
                    AppProcessModel.CreatedOn = DateTime.UtcNow;
                    AppProcessModel.ModifiedOn = DateTime.UtcNow;
                    AppProcessModel.CreatedBy = UserPrincipal.Current.UserId;
                    AppProcessModel.ModifiedBy = UserPrincipal.Current.UserId;
                    AppProcessModel.ProjectActionDate = DateTime.UtcNow;
                    AppProcessModel.RequestDate = DateTime.UtcNow;
                    AppProcessModel.TaskInstanceId = ProdGroupTaskId;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(AppProcessModel);
                }

                else
                {
                    AppProcessModel.TaskInstanceId = model.TaskInstanceId;
                    prod_GroupTasksManager.Checkin(AppProcessModel);
                    projectActionFormManager.UpdateProjectActionRequestForm(AppProcessModel);
                }


            }
            AppProcessModel.IsApprovedPopupDisplayedForAe = true;
            AppProcessModel.PopupTarget = "#divHtml";
            if (AppProcessModel.pageTypeId == 18)
            {
                return View("~/Views/Production/Amendments/Index.cshtml", AppProcessModel);
            }
            else
            {
                // return RedirectToAction("Index", "ProductionGroupTask"); 
                //AppProcessModel.pageTypeId = (int)PageType.ProductionApplication;
                return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessModel);
            }
        }
        [ValidateInput(false)]
        public ActionResult UploadPopupLoad(int FolderKey, string Model)
        {
            var parentdict = GetParentModelDictionary(Model);
            ViewBag.uploadurl = "/ProductionApplication/SaveDropzoneJsUploadedFiles";

            if (parentdict != null)
            {
                var PagetypeID = parentdict.FirstOrDefault(x => x.Key == "pageTypeId").Value;
                if (Convert.ToInt32(PagetypeID) == 18)
                {
                    ViewBag.redirecturl = "/Amendments/Reload/GroupTaskInstanceId?=";
                }
                else
                {
                    ViewBag.redirecturl = "/ProductionApplication/Reload/GroupTaskInstanceId?=";
                    //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();
                }
            }

            var folderuploadModel = new FolderUploadModel { FolderKey = FolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };


            return View("~/Views/Production/_Shared/_DragDropFileUpload.cshtml", folderuploadModel);
        }

        [ValidateInput(false)]
        public ActionResult UploadPopupLoadHUD(int FolderKey, string Model)
        {
            var parentdict = GetParentModelDictionary(Model);
            ViewBag.uploadurl = "/ProductionApplication/SaveDropzoneJsUploadedFilesHUD";
            string taskxrefId = string.Empty;



            if (parentdict.ContainsKey("taskxrefId") && !string.IsNullOrEmpty((string)parentdict["taskxrefId"]))
            {
                taskxrefId = (string)parentdict["taskxrefId"];

            }

            ViewBag.redirecturl = "/ProductionMyTask/GetTaskDetail?taskInstanceId=";
            //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();

            var folderuploadModel = new FolderUploadModel { FolderKey = FolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };


            return View("~/Views/Production/_Shared/_DragDropFileUpload_HUD.cshtml", folderuploadModel);
        }

        /// <summary>
        ///pad the numeric portion in  order by clause
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string PadNumbers(string input)
        {
            return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        }


        public ActionResult FolderRearrangePopupLoad(Guid taskInstanceId, string Model)
        {
            var nums = "0123456789".ToCharArray();//skumar:20180104 for bug#1133 : Rearranged and copy of files
            var reordermodel = new FileReorderModel { TaskInstanceId = taskInstanceId };

            var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceId)
                            select new
                            {
                                filelist.TaskFileId,
                                Name = filelist.FileName

                            });

            ViewBag.FileList = FileList;
            var tempFolderList = appProcessManager.GetFolderbyViewId(1)
                          .ToList()
                          .OrderBy(x => PadNumbers(x.FolderName))
                          .ThenBy(x => x.FolderName);
            // Venkatesh start
            var parentdict = GetParentModelDictionary(Model);
            if (parentdict != null)
            {
                string pageIdApp = ConfigurationManager.AppSettings["APPLICATION REQUEST"] ?? ConfigurationSettings.AppSettings["APPLICATION REQUEST"];
                string pageIdAmend = ConfigurationManager.AppSettings["AMENDMENTS"] ?? ConfigurationSettings.AppSettings["AMENDMENTS"];
                string pageIdNCDC = ConfigurationManager.AppSettings["NON-CONSTRUCTION DRAFT CLOSING"] ?? ConfigurationSettings.AppSettings["NON-CONSTRUCTION DRAFT CLOSING"];
                string pageIdNCEC = ConfigurationManager.AppSettings["NON-CONSTRUCTION EXECUTED CLOSING"] ?? ConfigurationSettings.AppSettings["NON-CONSTRUCTION EXECUTED CLOSING"];
                int appPageId = !string.IsNullOrEmpty(pageIdApp) ? Convert.ToInt32(pageIdApp) : 5;
                int amendPageId = !string.IsNullOrEmpty(pageIdAmend) ? Convert.ToInt32(pageIdAmend) : 18;
                int ncdcPageId = !string.IsNullOrEmpty(pageIdNCDC) ? Convert.ToInt32(pageIdNCDC) : 10;
                int ncecPageId = !string.IsNullOrEmpty(pageIdNCEC) ? Convert.ToInt32(pageIdNCEC) : 17;

                var PagetypeID = parentdict.FirstOrDefault(x => x.Key == "pageTypeId").Value;
                if (Convert.ToInt32(PagetypeID) == amendPageId)
                {
                    tempFolderList = appProcessManager.GetFolderbyViewId(4)
                                             .ToList()
                                             .OrderBy(x => PadNumbers(x.FolderName))
                                             .ThenBy(x => x.FolderName);
                    tempFolderList = tempFolderList.OrderBy(x => x.FolderKey);
                    reordermodel.pageTypeId = Convert.ToInt32(PagetypeID);
                }
                //skumar-start: update folder keys w.r.t pagetype
                if (Convert.ToInt32(PagetypeID) == ncdcPageId || Convert.ToInt32(PagetypeID) == ncecPageId)
                {
                    List<int> ncFolderKeys = ControllerHelper.GetProd_FolderStructureNCFolderKey();
                    tempFolderList = tempFolderList.Where(t => ncFolderKeys.Contains(t.FolderKey)).ToList().OrderBy(x => PadNumbers(x.FolderName)).ThenBy(x => x.FolderName);
                }
                if (Convert.ToInt32(PagetypeID) == appPageId)
                {
                    List<int> appFolderKeys = ControllerHelper.GetProd_FolderStructureAppFolderKey();
                    tempFolderList = tempFolderList.Where(t => appFolderKeys.Contains(t.FolderKey)).ToList().OrderBy(x => PadNumbers(x.FolderName)).ThenBy(x => x.FolderName);
                }
                //skumar-end: update folder keys w.r.t pagetype
            }
            // Vennkatesh end
            var folderlist = (from folders in tempFolderList
                              select new
                              {
                                  folders.FolderKey,
                                  Name = folders.FolderName

                              });

            ViewBag.FromFolder = new SelectList(folderlist, "FolderKey", "Name");
            ViewBag.AvailableFolders = new SelectList(folderlist, "FolderKey", "Name");
            ViewBag.ListOfFiles = new SelectList(FileList, "TaskFileId", "Name");

            return View("~/Views/Production/ApplicationRequest/popup/ReArrangeFolder.cshtml", reordermodel);
        }
        public ActionResult FileCopyPopupLoad(Guid taskInstanceId, string Model)
        {
            var nums = "0123456789".ToCharArray();//skumar:20180104 for bug#1133 : Rearranged and copy of files
            var reordermodel = new FileReorderModel { TaskInstanceId = taskInstanceId };

            var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceId)
                            select new
                            {
                                filelist.TaskFileId,
                                Name = filelist.FileName

                            });

            ViewBag.FileList = FileList;

            var tempFolderList = appProcessManager.GetFolderbyViewId(1)
                          .ToList()
                          .OrderBy(x => PadNumbers(x.FolderName))
                          .ThenBy(x => x.FolderName);
            // Venkatesh start
            var parentdict = GetParentModelDictionary(Model);
            if (parentdict != null)
            {
                string pageIdApp = ConfigurationManager.AppSettings["APPLICATION REQUEST"] ?? ConfigurationSettings.AppSettings["APPLICATION REQUEST"];
                string pageIdAmend = ConfigurationManager.AppSettings["AMENDMENTS"] ?? ConfigurationSettings.AppSettings["AMENDMENTS"];
                string pageIdNCDC = ConfigurationManager.AppSettings["NON-CONSTRUCTION DRAFT CLOSING"] ?? ConfigurationSettings.AppSettings["NON-CONSTRUCTION DRAFT CLOSING"];
                string pageIdNCEC = ConfigurationManager.AppSettings["NON-CONSTRUCTION EXECUTED CLOSING"] ?? ConfigurationSettings.AppSettings["NON-CONSTRUCTION EXECUTED CLOSING"];
                int appPageId = !string.IsNullOrEmpty(pageIdApp) ? Convert.ToInt32(pageIdApp) : 5;
                int amendPageId = !string.IsNullOrEmpty(pageIdAmend) ? Convert.ToInt32(pageIdAmend) : 18;
                int ncdcPageId = !string.IsNullOrEmpty(pageIdNCDC) ? Convert.ToInt32(pageIdNCDC) : 10;
                int ncecPageId = !string.IsNullOrEmpty(pageIdNCEC) ? Convert.ToInt32(pageIdNCEC) : 17;
                var PagetypeID = parentdict.FirstOrDefault(x => x.Key == "pageTypeId").Value;
                reordermodel.pageTypeId = Convert.ToInt32(PagetypeID);
                //skumar-start: update folder keys w.r.t pagetype
                if (Convert.ToInt32(PagetypeID) == amendPageId)
                {
                    tempFolderList = appProcessManager.GetFolderbyViewId(4)
                                             .ToList()
                                             .OrderBy(x => PadNumbers(x.FolderName))
                                             .ThenBy(x => x.FolderName);
                    tempFolderList = tempFolderList.OrderBy(x => x.FolderKey);
                }
                if (Convert.ToInt32(PagetypeID) == ncdcPageId || Convert.ToInt32(PagetypeID) == ncecPageId)
                {
                    List<int> ncFolderKeys = ControllerHelper.GetProd_FolderStructureNCFolderKey();
                    tempFolderList = tempFolderList.Where(t => ncFolderKeys.Contains(t.FolderKey)).ToList().OrderBy(x => PadNumbers(x.FolderName)).ThenBy(x => x.FolderName);
                }
                if (Convert.ToInt32(PagetypeID) == appPageId)
                {
                    List<int> appFolderKeys = ControllerHelper.GetProd_FolderStructureAppFolderKey();
                    tempFolderList = tempFolderList.Where(t => appFolderKeys.Contains(t.FolderKey)).ToList().OrderBy(x => PadNumbers(x.FolderName)).ThenBy(x => x.FolderName);
                }
                //skumar-end: update folder keys w.r.t pagetype
            }
            // Vennkatesh end
            var folderlist = (from folders in tempFolderList
                              select new
                              {
                                  folders.FolderKey,
                                  Name = folders.FolderName

                              });


            //var folderlist = (from folders in appProcessManager.GetFolderbyViewId(1)
            //                  select new
            //                  {
            //                      folders.FolderKey,
            //                      Name = folders.FolderName

            //                  });
            ViewBag.FromFolder = new SelectList(folderlist, "FolderKey", "Name");
            ViewBag.AvailableFolders = new SelectList(folderlist, "FolderKey", "Name");
            ViewBag.ListOfFiles = new SelectList(FileList, "TaskFileId", "Name");

            return View("~/Views/Production/ApplicationRequest/popup/CopyFiles.cshtml", reordermodel);
        }
        public ActionResult DocTypeRearrangePopup(Guid taskInstanceId)
        {
            var reordermodel = new FileReorderModel { TaskInstanceId = taskInstanceId };
            var docTypeList = appProcessManager.GetAllDocTypes();

            var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceId)
                            select new
                            {
                                filelist.FileId,
                                docId = filelist.DocId,
                                Name = String.Format("{0}( {1} )", filelist.FileName, filelist.DocId)
                            });
            var folderlist = (from folders in appProcessManager.GetFolderbyViewId(1)
                              select new
                              {
                                  folders.FolderKey,
                                  Name = folders.FolderName

                              });

            ViewBag.FileList = FileList;
            ViewBag.FromFolder = new SelectList(folderlist, "FolderKey", "Name");
            ViewBag.AvailableFolders = new SelectList(folderlist, "FolderKey", "Name");
            ViewBag.ListOfFiles = new SelectList(FileList, "FileId", "Name");
            ViewBag.DocTypeList = new SelectList(docTypeList, "DocumentTypeId", "DocumentType");
            return View("~/Views/Production/ApplicationRequest/popup/ReArrangeDocType.cshtml", reordermodel);
        }

        //public ActionResult ChangeDocumentTypePopup(Guid taskinstanceId, string fileId)
        //{
        //    var FileName = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(taskinstanceId)
        //                    select new
        //                    {
        //                        filelist.FileId,
        //                        filelist.DocTypeID,
        //                        docId = filelist.DocId,
        //                        Name = filelist.FileName
        //                    }).Where(m => m.FileId ==Convert.ToInt32(fileId));

        //        var docTypeList = appProcessManager.GetAllDocTypes();
        //        docTypeList.Add(new DocumentTypeModel() { DocumentType = ConfigurationManager.AppSettings["DefaultDocName"].ToString(), DocumentTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultDocId"]) });

        //        var folderlist = (from folders in appProcessManager.GetFolderbyViewId(1)
        //                          select new
        //                          {
        //                              folders.FolderKey,
        //                              Name = folders.FolderName

        //                          }).ToList();

        //        ViewBag.FileName = FileName.FirstOrDefault().Name;
        //        ViewBag.FromFolder = new SelectList(folderlist, "FolderKey", "Name");
        //        ViewBag.AvailableFolders = new SelectList(folderlist, "FolderKey", "Name");

        //        ViewBag.DocTypeList = new SelectList(docTypeList, "DocumentTypeId", "DocumentType");

        //        return View("~/Views/Production/ApplicationRequest/popup/ChangeDocType.cshtml");




        //}
        //public ActionResult ChildDocTypePopup(Guid taskinstanceId, Guid groupTaskId)
        //{                  
        //    var Files = new DocumentTypeModel();
        //    Files.TaskInstanceId = taskinstanceId;
        //    Files.groupTaskID = groupTaskId;
        //    var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(taskinstanceId)
        //                    select new
        //                    {
        //                        filelist.FileId,
        //                        filelist.DocTypeID,
        //                        docId = filelist.DocId,
        //                        Name = filelist.FileName
        //                    }).Where(m => string.IsNullOrEmpty(m.DocTypeID));


        //    if (FileList.Count() > 0)
        //    {
        //        var reordermodel = new FileReorderModel { TaskInstanceId = taskinstanceId };
        //        //var docTypeList = appProcessManager.GetAllDocTypes();
        //        //docTypeList.Add(new DocumentTypeModel() { DocumentType = ConfigurationManager.AppSettings["DefaultDocName"].ToString(), DocumentTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultDocId"]) });
        //        var docTypeList = new List<DocumentTypeModel>();
        //        var folderlist = (from folders in appProcessManager.GetFolderbyViewId(1)
        //                          select new
        //                          {
        //                              folders.FolderKey,
        //                              Name = folders.FolderName

        //                          });

        //        ViewBag.FileList = FileList;
        //        ViewBag.FromFolder = new SelectList(folderlist, "FolderKey", "Name");
        //        ViewBag.AvailableFolders = new SelectList(folderlist, "FolderKey", "Name");
        //        ViewBag.ListOfFiles = new SelectList(FileList, "FileId", "Name");
        //        ViewBag.DocTypeList = new SelectList(docTypeList, "DocumentTypeId", "DocumentType");

        //        return View("~/Views/Production/ApplicationRequest/popup/ChildDocTypePopup.cshtml", Files);

        //    }

        //    else
        //    {
        //        var formUploadData = ReloadApplication(taskinstanceId);
        //        return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml", formUploadData);
        //    }
        //}
        public ActionResult ConfirmationPopupLoad(Guid taskxrefid)
        {


            var taskAssignmentModel = new ProductionTaskAssignmentModel { TaskXrefid = taskxrefid };


            return View("~/Views/Production/ApplicationRequest/popup/AppProcessApprovePopup.cshtml", taskAssignmentModel);
        }

        //public ActionResult ChangeDocTypeConfirmationPopup(string fileID, int DocTypeId, string taskInstanceId)

        //{
        //    DocumentTypeModel model = new DocumentTypeModel();
        //    model.FileIdlist = fileID;
        //    model.DocumentTypeId = DocTypeId;
        //    return View("~/Views/Production/ApplicationRequest/popup/DocTypeConfirmationfPopup.cshtml", model);
        //}
        public ActionResult DocumentMappingpopupLoad(Guid taskInstanceId, string Model)
        {
            var reordermodel = new FileReorderModel { TaskInstanceId = taskInstanceId };

            var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceId)
                            select new
                            {
                                filelist.TaskFileId,
                                Name = filelist.FileName

                            });

            ViewBag.FileList = FileList;


            var folderlist = (from folders in appProcessManager.GetFolderbyViewId(1)
                              select new
                              {
                                  folders.FolderKey,
                                  Name = folders.FolderName

                              });

            List<SelectListItem> DocumentTypes = new List<SelectListItem>();
            DocumentTypes.Add(new SelectListItem() { Text = "232 Amortization Schedule", Value = "1" });
            DocumentTypes.Add(new SelectListItem() { Text = "290 Packet", Value = "2" });
            DocumentTypes.Add(new SelectListItem() { Text = "Western Cape", Value = "WC" });
            DocumentTypes.Add(new SelectListItem() { Text = "4128 Environmental Review", Value = "3" });
            DocumentTypes.Add(new SelectListItem() { Text = "Advance of Escrow Funds", Value = "4" });

            ViewBag.DocumentTypes = new SelectList(DocumentTypes, "Value", "Text");
            ViewBag.FromFolder = new SelectList(folderlist, "FolderKey", "Name");

            ViewBag.ListOfFiles = new SelectList(FileList, "TaskFileId", "Name");

            return View("~/Views/Production/ApplicationRequest/popup/DocumentMapping.cshtml", reordermodel);
        }

        public JsonResult GetFilesBasedonFolder(string FoldeKeys, string TaskInstanceid)
        {
            Guid Instanceid = new Guid(TaskInstanceid);
            var keyarry = FoldeKeys.Split(',');
            var keyintarr = Array.ConvertAll(keyarry, p => Convert.ToInt32(p));
            var result = appProcessManager.GetMappedFilesbyFolder(keyintarr, Instanceid);

            List<SelectListItem> temp = new List<SelectListItem>();
            var multilist = new MultiSelectList(result.ToList().Select(p => new KeyValuePair<Guid, string>(p.TaskFileId, p.FileName)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");
            temp = multilist.ToList();

            return Json(new { temp }, JsonRequestBehavior.AllowGet);
        }
        [ValidateInput(false)]
        public ActionResult LenderReUploadPopupLoad(int FolderKey, string Model)
        {
            var parentdict = GetParentModelDictionary(Model);
            ViewBag.uploadurl = "/ProductionApplication/SaveLenderRAIDropzoneJsUploadedFiles";

            ViewBag.redirecturl = "/ProductionApplication/ReloadlenderReadOnly/GroupTaskInstanceId?=";
            //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();

            var folderuploadModel = new FolderUploadModel { FolderKey = FolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };


            return View("~/Views/Production/_Shared/_DragDropFileUpload.cshtml", folderuploadModel);
        }

        private Dictionary<string, object> GetParentModelDictionary(string Model)
        {
            Dictionary<string, object> mydict = new Dictionary<string, object>();
            string[] reslts = Model.Split(new char[] { '|', '|', '|', '|' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string s in reslts)
            {
                var temp = JObject.Parse(s);

                if (!mydict.ContainsKey(((Newtonsoft.Json.Linq.JValue)(temp["name"])).Value.ToString()))
                {
                    mydict.Add(((Newtonsoft.Json.Linq.JValue)(temp["name"])).Value.ToString(), ((Newtonsoft.Json.Linq.JValue)(temp["value"])).Value.ToString());
                }

            }
            return mydict;
        }
        [ValidateInput(false)]
        private bool Upload(OPAViewModel AppProcessViewModel, int FolderKey)
        {
            var FolderInfo = projectActionFormManager.GetFolderInfoByKey(FolderKey);
            var PageName = projectActionFormManager.FormNameBYPageTypeID(AppProcessViewModel.pageTypeId);
            string folderNames = string.Empty;
            string formNameConfiguration = ConfigurationManager.AppSettings["FORM 290"] ?? ConfigurationSettings.AppSettings["FORM 290"];
            string amendmentConfig = ConfigurationManager.AppSettings["Amendments"] ?? ConfigurationSettings.AppSettings["Amendments290"];
            int ammendmentPageId = !string.IsNullOrEmpty(amendmentConfig) ? Convert.ToInt32(amendmentConfig) : 0;
            int form290PageId = !string.IsNullOrEmpty(formNameConfiguration) ? Convert.ToInt32(formNameConfiguration) : 0;
            if (FolderInfo != null)
            {
                foreach (Prod_SubFolderStructureModel folder in FolderInfo)
                {


                    if (AppProcessViewModel.pageTypeId == form290PageId)//skumar-form290 - folder map 
                    {
                        folderNames = "/" + AppProcessViewModel.PropertyId.ToString() + "/" + AppProcessViewModel.FhaNumber + "/Production/ExecutedClosing/WorkProduct/Form290/";
                    }
                    else if (AppProcessViewModel.pageTypeId == ammendmentPageId)//skumar-2044 amendemtns=18
                    {
                        folderNames = "/" + AppProcessViewModel.PropertyId.ToString() + "/" + AppProcessViewModel.FhaNumber + "/Production/Closing/WorkProduct/Amendments/";
                    }
                    else if (FolderInfo[0].ViewTypeId == 2)
                    {
                        folderNames = "Production/" + PageName + "/" + "WorkProduct/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                    }

                    else
                    {
                        //Consider the case where there is no SubfolderSequence
                        folderNames = "Production/" + PageName + "/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                    }
                }

            }
            var uploadmodel = new RestfulWebApiUploadModel()
            {
                propertyID = AppProcessViewModel.PropertyId.ToString(),
                indexType = "1",
                indexValue = AppProcessViewModel.FhaNumber,
                //pdfConvertableValue = "false",
                pdfConvertableValue = "false",
                folderKey = FolderKey,
                folderNames = folderNames
            };
            //karri#162
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //request = WebApiTokenRequest.RequestToken();

            bool isUploaded = false;

            foreach (string Filename in Request.Files)
            {
                // var TEMP = Request.Files[Filename];
                //HttpPostedFileBase Temp;

                HttpPostedFileBase myFile = Request.Files[Filename];
                var t = myFile.InputStream;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();

                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = AppProcessViewModel.FhaNumber + "_" +
                                         AppProcessViewModel.ProjectActionTypeId + "_" +
                                         AppProcessViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);

                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {

                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = true;

                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    //karri#162,#171
                    request = WebApiTokenRequest.RequestToken();

                    if (AppProcessViewModel.pageTypeId == 18)
                    {
                        WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, FolderInfo[0].FolderName + "_" + FolderInfo[0].SubfolderSequence);
                    }
                    else
                    {
                        WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");
                    }


                    if (System.IO.File.Exists(Path.Combine(pathForSaving, systemFileName)))
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));

                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {

                        isUploaded = true;
                        var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(AppProcessViewModel.GroupTaskInstanceId.ToString()));
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        //if (AppProcessViewModel.pageTypeId == 18)
                        //{
                        //string subFolder = string.Empty;
                        //subFolder = !string.IsNullOrEmpty(FolderInfo[0].SubfolderSequence) ? FolderInfo[0].SubfolderSequence.Trim() : subFolder;
                        //if (!string.IsNullOrEmpty(subFolder))
                        //{
                        //    taskFile.FileName = FolderInfo[0].FolderName + "_" + FolderInfo[0].SubfolderSequence.Trim() + "!" + myFile.FileName;
                        //}
                        //else
                        //{
                        //    taskFile.FileName = FolderInfo[0].FolderName + "_" + subFolder + "!" + myFile.FileName;
                        //}
                        //}

                        // harish added 0904202 
                        if (myFile.FileName.ToUpper().StartsWith("OTHERS_") || myFile.FileName.ToUpper().StartsWith("OTHER_"))
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        if (WebApiUploadResult.documentType != DefaultDocID)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        else if (FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductClosing) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductUnderwriting) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductLoanCommittee))
                        {
                            //taskFile.DocTypeID = DefaultDocID;
                            taskFile.FileType = FileType.WP;
                        }
                        else if (AppProcessViewModel.pageTypeId == 18)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }

                        taskFile.RoleName = UserPrincipal.Current.UserRole;
                        var taskfileid = taskManager.SaveGroupTaskFileModel(taskFile);
                        var mappingmodel = new TaskFile_FolderMappingModel();
                        mappingmodel.FolderKey = FolderKey;
                        mappingmodel.TaskFileId = taskfileid;
                        mappingmodel.CreatedOn = DateTime.UtcNow;

                        taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);

                        //Code for saving new files on RAI
                        if (AppProcessViewModel.TaskGuid != null)
                        {
                            var childTaskNewFile = new ChildTaskNewFileModel();
                            childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
                            childTaskNewFile.ChildTaskInstanceId = AppProcessViewModel.TaskGuid.Value;
                            childTaskNewFile.TaskFileInstanceId = taskfileid;
                            projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);

                        }
                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        isUploaded = false;

                    }


                }
            }
            return isUploaded;
        }
        //private bool Upload(OPAViewModel AppProcessViewModel, int FolderKey)
        //{

        //    RestfulWebApiUploadModel uploadmodel = new RestfulWebApiUploadModel();
        //    RestfulWebApiResultModel WebApiUploadResult = new RestfulWebApiResultModel();
        //    RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();

        //    request = WebApiTokenRequest.RequestToken();

        //    bool isUploaded = false;
        //    uploadmodel.documentType = "550";
        //    //uploadmodel.propertyID = AppProcessViewModel.PropertyId.ToString();
        //    uploadmodel.propertyID = "7892";
        //    uploadmodel.indexType = "1";
        //    uploadmodel.indexValue = "10021";
        //    uploadmodel.pdfConvertableValue = "false";



        //    foreach (string Filename in Request.Files)
        //    {
        //        HttpPostedFileBase myFile = Request.Files[Filename];

        //        uploadmodel.document = myFile;



        //        if (myFile != null && myFile.ContentLength != 0)
        //        {
        //            double fileSize = (myFile.ContentLength) / 1024;
        //            Random randoms = new Random();

        //            var UniqueId = randoms.Next(0, 99999);

        //            var systemFileName = AppProcessViewModel.FhaNumber + "_" +
        //                                 AppProcessViewModel.ProjectActionTypeId + "_" +
        //                                 AppProcessViewModel.GroupTaskInstanceId + "_" + UniqueId +
        //                                 Path.GetExtension(myFile.FileName);

        //            string pathForSaving = Server.MapPath("~/Uploads");
        //            WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token);

        //            if (WebApiUploadResult.status == null || WebApiUploadResult.status.ToLower() != "success")
        //            {
        //                if (this.CreateFolderIfNeeded(pathForSaving))
        //                {
        //                    try
        //                    {
        //                        // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
        //                        myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
        //                        isUploaded = true;

        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        //message = string.Format("File upload failed: {0}", ex.Message);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                isUploaded = true;
        //            }


        //            var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(AppProcessViewModel.GroupTaskInstanceId.ToString()));
        //            var taskfileid = taskManager.SaveGroupTaskFileModel(taskFile);
        //            var mappingmodel = new TaskFile_FolderMappingModel();
        //            mappingmodel.FolderKey = FolderKey;
        //            mappingmodel.TaskFileId = taskfileid;
        //            mappingmodel.CreatedOn = DateTime.UtcNow;
        //            taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);

        //            //Code for saving new files on RAI
        //            if(AppProcessViewModel.TaskGuid!=null)
        //            {
        //              var childTaskNewFile = new ChildTaskNewFileModel();
        //                childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
        //                childTaskNewFile.ChildTaskInstanceId = AppProcessViewModel.TaskGuid.Value;
        //                childTaskNewFile.TaskFileInstanceId = taskfileid;
        //                projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);
        //            }
        //        }
        //    }
        //    return isUploaded;
        //}

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        //Code for Firm Commitment Sent
        public string FirmCommitmentSent()
        {
            //var Instanceid = new Guid(Request.QueryString[0]);
            //string Instanceid= Request.QueryString[0];
            var taskxref = new Guid(Request.QueryString[1]);
            var pagetypeid = (int)(Convert.ToInt32(Request.QueryString[2]));
            string message = "Success";
            try
            {

                var parentTask = taskManager.GetTasksByTaskInstanceId(new Guid(Request.QueryString[0])).ToList();
                if (taskManager.IsFirmCommitmentCompleted(new Guid(Request.QueryString[0])))
                {
                    return "Completed";
                }
                if (taskManager.IsFirmCommitmentExists(new Guid(Request.QueryString[0])))
                {
                    return "Exists";
                }

                var fileTask =
                    taskManager.GetFilesByTaskInstanceIdAndFileType(new Guid(Request.QueryString[0]), FileType.FWP);
                if (fileTask.Count == 0)
                {
                    return "No File";
                }

                var taskList = new List<TaskModel>();
                var childTask = new TaskModel();


                childTask.TaskInstanceId = Guid.NewGuid();
                childTask.SequenceId = 0;
                childTask.AssignedBy = UserPrincipal.Current.UserName;
                childTask.AssignedTo = parentTask[0].AssignedBy;
                childTask.StartTime = DateTime.UtcNow;
                childTask.TaskStepId = (int)ProductionAppProcessStatus.Request;
                //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                childTask.MyStartTime = childTask.StartTime;
                childTask.IsReAssigned = false;
                childTask.Concurrency = parentTask[0].Concurrency;
                childTask.DataStore1 = parentTask[0].DataStore1;
                //adding fhanumber and pagetype      
                childTask.FHANumber = parentTask[0].FHANumber;
                childTask.PageTypeId = parentTask[0].PageTypeId;
                //childTask.PageTypeId = (int)PageType.ProductionApplication; 
                var childTaskOpenStatusModel = new TaskOpenStatusModel();
                childTaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                childTask.TaskOpenStatus = XmlHelper.Serialize(childTaskOpenStatusModel,
                    typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(childTask);

                var requestAdditionalChildTask = new ParentChildTaskModel();
                requestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                requestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                requestAdditionalChildTask.ParentTaskInstanceId = taskxref;
                requestAdditionalChildTask.ChildTaskInstanceId = childTask.TaskInstanceId;
                requestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();

                if (taskList.Count > 0)
                {
                    taskManager.SaveTask(taskList, new List<TaskFileModel>());
                }
                parentChildTaskManager.AddParentChildTask(requestAdditionalChildTask);

                //karri-MH232-79//APPLICATION REQUEST//CONSTRUCTION SINGLE STAGE//CONSTRUCTION TWO STAGE INITIAL//CONSTRUCTION TWO STAGE FINAL
                //moving FHA Number to the next process
                //only when UW with Production Appplicaiton or first process starts
                if (pagetypeid == 5 || pagetypeid == 6 || pagetypeid == 7 || pagetypeid == 8)
                {
                    var nextStage = new Prod_NextStageModel();
                    nextStage.NextStgId = Guid.NewGuid();
                    nextStage.CompletedPgTypeId = pagetypeid;
                    nextStage.CompletedPgTaskInstanceId = parentTask[0].TaskInstanceId;
                    nextStage.NextPgTypeId = GetNextStagePageTypeId(pagetypeid);
                    nextStage.ModifiedOn = DateTime.UtcNow;
                    nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                    nextStage.FhaNumber = parentTask[0].FHANumber;
                    nextStage.IsNew = true;
                    prod_NextStageManager.AddNextStage(nextStage);
                }

            }
            catch (Exception ex)
            {
                message = "Failure";
            }
            return message;
        }

        //Code for Firm Commitment Lender Reply Sent
        public string FirmCommitmentReply()
        {
            //string Instanceid= Request.[0];

            string message = "Success";
            try
            {
                var parentTask = taskManager.GetTasksByTaskInstanceId(new Guid(Request.QueryString[0])).ToList();
                var parentChildTask = parentChildTaskManager.GetParentTask(new Guid(Request.QueryString[0]));
                var xrefTask = prod_TaskXrefManager.GetProductionSubtaskById(parentChildTask.ParentTaskInstanceId);
                var fileTask =
                    taskManager.GetFilesByTaskInstanceIdAndFileType(xrefTask.TaskInstanceId, FileType.FWP);
                var IsFileExists = false;
                //var IsFileExists = true;
                if (fileTask.Any())
                {
                    foreach (var item in fileTask)
                    {
                        //if (item.CreatedBy != null && RoleManager.IsProductionUser(accountManager.GetUserById((int)item.CreatedBy).UserName))
                        if (item.CreatedBy != null && RoleManager.IsUserLenderRole(accountManager.GetUserById((int)item.CreatedBy).UserName))
                        {
                            IsFileExists = true;
                        }
                    }
                }
                if (!IsFileExists)
                {
                    return "No File";
                }


                var taskList = new List<TaskModel>();
                var task = new TaskModel();

                string alertTitle = String.Empty;

                task.AssignedTo = parentTask[0].AssignedBy;
                task.AssignedBy = UserPrincipal.Current.UserName;


                task.TaskStepId = (int)ProductionAppProcessStatus.Response;


                task.DataStore1 = null;
                task.Notes = "Firm Commitment sent";
                task.SequenceId = parentTask[0].SequenceId + 1; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = parentTask[0].TaskInstanceId;
                task.IsReAssigned = parentTask[0].IsReAssigned;
                task.Concurrency = parentTask[0].Concurrency;
                //adding fhanumber and pagetype 
                task.FHANumber = parentTask[0].FHANumber;
                task.PageTypeId = parentTask[0].PageTypeId;
                //task.PageTypeId = (int)PageType.ProductionApplication;
                var taskOpenStatusModel = new TaskOpenStatusModel();
                taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(task);
                var taskFile = new List<TaskFileModel>();

                if (taskList.Count > 0)
                {
                    taskManager.SaveTask(taskList, new List<TaskFileModel>());
                }



            }
            catch (Exception ex)
            {
                message = "Failure";
            }
            return message;
        }
        private string SaveExternalReviewerUpdates(string editData)
        {
            var currentDate = DateTime.UtcNow;
            JObject json = JObject.Parse(editData);
            var xrefTaskInstanceId = json["taskInstanceId"];
            var fileUpdates = json["fileUpdates"];
            var nfrComment = json["nfrComment"];
            var isNewfile = json["IsNewfile"];
            var reviewStatusList = new List<ReviewFileStatusModel>();
            var reviewCommentList = new List<ReviewFileCommentModel>();
            var fileRenameList = new List<Tuple<Guid, string, string>>();
            var newFileRequestModel = new NewFileRequestModel();
            var isRAI = false;

            Prod_TaskXrefModel xrefTaskDetails = taskManager.GetReviewerViewIdByXrefTaskInstanceId((Guid)xrefTaskInstanceId);



            //retrieve status and comments list and save in respective tables
            foreach (JObject content in fileUpdates.Children<JObject>())
            {
                var fileStatusModel = new ReviewFileStatusModel();


                var isApproved = false;


                var isFileRenamed = false;
                Tuple<Guid, string, string> fileRenameTuple;

                var taskFileId = new Guid();
                var fileReName = "";
                var fileExt = "";

                foreach (JProperty prop in content.Properties())
                {
                    if (prop.Name == "fileId")
                    {
                        taskFileId = taskManager.GetTaskFileById(int.Parse(prop.Value.ToString())).TaskFileId;
                        fileStatusModel = reviewFileStatusManager.GetReviewFileStatusByTaskFileIdandViewId(taskFileId, xrefTaskDetails.ViewId);

                    }
                    else if (prop.Name == "ReqAdditionalInfo")
                    {
                        if (prop.Value.ToString() == "True")
                        {
                            //create child req
                            fileStatusModel.Status = 6;
                            isRAI = true;
                        }
                    }
                    else if (prop.Name == "Comments")
                    {
                        if (prop.Value.ToString().Trim().Length > 0)
                        {
                            var fileCommentModel = new ReviewFileCommentModel();
                            fileCommentModel.ReviewFileCommentId = Guid.NewGuid();
                            fileCommentModel.CreatedOn = currentDate;
                            fileCommentModel.Comment = prop.Value.ToString();
                            fileCommentModel.FileTaskId = taskFileId;
                            fileCommentModel.ReviewerProdViewId = xrefTaskDetails.ViewId;
                            fileCommentModel.CreatedBy = fileStatusModel.ReviewerUserId;
                            reviewCommentList.Add(fileCommentModel);
                        }
                    }
                    else if (prop.Name == "Approve")
                    {
                        if (prop.Value.ToString() == "True" && fileStatusModel.Status != 5)
                        {
                            fileStatusModel.Status = 5;
                            isApproved = true;
                        }
                    }
                    else if (prop.Name == "isFileRenamed")
                    {
                        if (prop.Value.ToString() == "True")
                        {
                            isFileRenamed = true;
                        }
                    }
                    else if (prop.Name == "fileRename")
                    {
                        fileReName = prop.Value.ToString();
                    }
                    else if (prop.Name == "fileExt")
                    {
                        fileExt = prop.Value.ToString();
                    }
                }
                if (isFileRenamed)
                {
                    fileRenameTuple = new Tuple<Guid, string, string>(taskFileId, fileReName, fileExt);
                    fileRenameList.Add(fileRenameTuple);
                }
                //if (isRAI == true || isApproved == true)
                //{
                //To do - Add view id
                fileStatusModel.ModifiedOn = currentDate;
                fileStatusModel.ModifiedBy = fileStatusModel.ReviewerUserId;
                reviewStatusList.Add(fileStatusModel);
                // }

            }
            //Creating child task for Request Additional info files
            var parentTask = taskManager.GetTasksByTaskInstanceId(xrefTaskDetails.TaskInstanceId).ToList();

            try
            {

                if (reviewStatusList.Any())
                {
                    reviewFileStatusManager.UpdateReviewFileStatus(reviewStatusList);
                }
                if (reviewCommentList.Any())
                {
                    reviewFileCommentManager.SaveReviewFileComment(reviewCommentList);
                }
                if (isRAI || (bool)(isNewfile) == true)
                {
                    //var model = projectActionFormManager.GetOPAByTaskInstanceId(parentTask[0].TaskInstanceId);
                    //if (childTask.AssignedTo != null && childTask.AssignedBy != null)
                    //{
                    //    if (childTask.TaskStepId == 16)
                    //    {
                    //        model.RequestStatus = 7;
                    //    }
                    //    if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(new EmailManager(), childTask.AssignedTo,
                    //        childTask.AssignedBy, model))
                    //    {
                    //    }
                    //}
                }

                return "Success";

            }
            catch (Exception ex)
            {
                return "Failure";
            }



        }

        public string SubmitRaiPendingList(string editData)
        {
            JObject json = JObject.Parse(editData);
            var xrefTaskInstanceId = json["taskInstanceId"];
            var raiList = json["raiList"];

            var childTask = new TaskModel();
            childTask.TaskInstanceId = Guid.NewGuid();

            var reviewStatusList = new List<ReviewFileStatusModel>();
            var reviewCommentList = new List<ReviewFileCommentModel>();
            var requestAdditionalInfoFilesList = new List<RequestAdditionalInfoFileModel>();

            var fileCommentModel = new ReviewFileCommentModel();
            var fileStatusModel = new ReviewFileStatusModel();
            var requestAdditionalInfoFilesModel = new RequestAdditionalInfoFileModel();


            Prod_TaskXrefModel xrefTaskDetails = taskManager.GetReviewerViewIdByXrefTaskInstanceId((Guid)xrefTaskInstanceId);

            bool isInOpenChildTask = false;
            bool isRAI = false;
            int userId = 0;
            int viewId = 0;
            string comment = string.Empty;
            var taskFileId = new Guid();


            //retrieve status and comments list and save in respective tables
            foreach (JObject content in raiList.Children<JObject>())
            {


                isInOpenChildTask = false;
                isRAI = false;
                userId = 0;
                viewId = 0;
                comment = string.Empty;
                taskFileId = new Guid();

                fileCommentModel = new ReviewFileCommentModel();
                fileStatusModel = new ReviewFileStatusModel();

                foreach (JProperty prop in content.Properties())
                {
                    if (prop.Name == "fileId")
                    {
                        taskFileId = taskManager.GetTaskFileById(int.Parse(prop.Value.ToString())).TaskFileId;

                        fileCommentModel.FileTaskId = taskFileId;
                    }
                    else if (prop.Name == "ReqAdditionalInfo")
                    {
                        if (prop.Value.ToString().ToUpper() == "TRUE")
                        {
                            isRAI = true;
                        }
                        else
                        {
                            isRAI = false;
                        }
                    }
                    else if (prop.Name == "Comments")
                    {
                        if (prop.Value.ToString().Trim().Length > 0)
                        {

                            comment = prop.Value.ToString().Trim();
                        }
                    }
                    else if (prop.Name == "isInOpenChildTask")
                    {
                        if (prop.Value.ToString().ToUpper() == "TRUE")
                        {
                            isInOpenChildTask = true;
                        }
                        else
                        {
                            isInOpenChildTask = false;
                        }
                    }
                    else if (prop.Name == "userId")
                    {
                        userId = Convert.ToInt32(prop.Value.ToString());
                    }
                    else if (prop.Name == "viewId")
                    {
                        viewId = Convert.ToInt32(prop.Value.ToString());
                    }

                }

                ////Save comment
                //fileCommentModel.ReviewFileCommentId = Guid.NewGuid();
                //fileCommentModel.CreatedOn = DateTime.UtcNow;
                //fileCommentModel.Comment = comment;
                //fileCommentModel.FileTaskId = taskFileId;
                //fileCommentModel.ReviewerProdViewId = xrefTaskDetails.ViewId;
                //fileCommentModel.CreatedBy = fileStatusModel.ReviewerUserId;
                //reviewCommentList.Add(fileCommentModel);

                //update task status
                fileStatusModel = reviewFileStatusManager.GetReviewFileStatusByTaskFileIdandViewId(taskFileId, viewId);
                fileStatusModel.Status = (int)ReviewFileStatusList.RAI;
                fileStatusModel.ModifiedOn = DateTime.UtcNow;
                fileStatusModel.ModifiedBy = UserPrincipal.Current.UserId;
                reviewStatusList.Add(fileStatusModel);

                if (!isInOpenChildTask)
                {
                    //add files to child task 
                    var requestAdditionalInfoFiles = new RequestAdditionalInfoFileModel();
                    requestAdditionalInfoFiles.ChildTaskInstanceId = childTask.TaskInstanceId;
                    requestAdditionalInfoFiles.RequestAdditionalFileId = Guid.NewGuid();
                    requestAdditionalInfoFiles.TaskFileId = taskFileId;
                    requestAdditionalInfoFilesList.Add(requestAdditionalInfoFiles);
                }


            }
            //Creating child task for Request Additional info files
            var parentTask = taskManager.GetTasksByTaskInstanceId(xrefTaskDetails.TaskInstanceId).ToList();

            var taskList = new List<TaskModel>();

            if (requestAdditionalInfoFilesList.Any())
            {
                childTask.SequenceId = 0;
                childTask.AssignedBy = UserPrincipal.Current.UserName;
                childTask.AssignedTo = parentTask[0].AssignedBy;
                childTask.StartTime = DateTime.UtcNow;
                childTask.TaskStepId = (int)TaskStep.OPAAddtionalInformation;
                //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                childTask.MyStartTime = childTask.StartTime;
                childTask.IsReAssigned = false;
                childTask.Concurrency = parentTask[0].Concurrency;
                childTask.DataStore1 = parentTask[0].DataStore1;
                //adding fhanumber and pagetype      
                childTask.FHANumber = parentTask[0].FHANumber;
                childTask.PageTypeId = parentTask[0].PageTypeId;
                //childTask.PageTypeId = (int)PageType.ProductionApplication;
                var childTaskOpenStatusModel = new TaskOpenStatusModel();
                childTaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                childTask.TaskOpenStatus = XmlHelper.Serialize(childTaskOpenStatusModel,
                    typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(childTask);
            }

            try
            {

                if (requestAdditionalInfoFilesList.Any())
                {
                    var requestAdditionalChildTask = new ParentChildTaskModel();
                    requestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                    requestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                    requestAdditionalChildTask.ParentTaskInstanceId = (Guid)xrefTaskInstanceId;
                    requestAdditionalChildTask.ChildTaskInstanceId = childTask.TaskInstanceId;
                    requestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();

                    if (taskList.Count > 0)
                    {
                        taskManager.SaveTask(taskList, new List<TaskFileModel>());
                    }
                    parentChildTaskManager.AddParentChildTask(requestAdditionalChildTask);
                    requestAdditionalInfoFilesManager.SaveRequestAdditionalInfoFiles(requestAdditionalInfoFilesList);
                    ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
                }

                if (reviewStatusList.Any())
                {
                    reviewFileStatusManager.UpdateReviewFileStatus(reviewStatusList);
                }
                //if (reviewCommentList.Any())
                //{
                //    reviewFileCommentManager.SaveReviewFileComment(reviewCommentList);
                //}
                if (requestAdditionalInfoFilesList.Any())
                {
                    var model = projectActionFormManager.GetOPAByTaskInstanceId(parentTask[0].TaskInstanceId);
                    if (childTask.AssignedTo != null && childTask.AssignedBy != null)
                    {
                        if (childTask.TaskStepId == 16)
                        {
                            model.RequestStatus = 7;
                        }
                        //if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(new EmailManager(), childTask.AssignedTo,
                        //    childTask.AssignedBy, model))
                        //{
                        //}
                    }
                }
                if (requestAdditionalInfoFilesList.Any())
                {
                    return "Request";
                }
                else
                {
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return "Failure";
            }


        }

        private string SaveInternalReviewerUpdates(string editData)
        {
            JObject json = JObject.Parse(editData);
            var xrefTaskInstanceId = json["taskInstanceId"];
            var fileUpdates = json["fileUpdates"];
            var nfrComment = json["nfrComment"];
            var isNewfile = json["IsNewfile"];
            var reviewStatusList = new List<ReviewFileStatusModel>();
            var reviewCommentList = new List<ReviewFileCommentModel>();
            var fileRenameList = new List<Tuple<Guid, string, string>>();
            var newFileRequestModel = new NewFileRequestModel();
            var isRAI = false;

            Prod_TaskXrefModel xrefTaskDetails = taskManager.GetReviewerViewIdByXrefTaskInstanceId((Guid)xrefTaskInstanceId);
            var requestAdditionalInfoFilesList = new List<RequestAdditionalInfoFileModel>();
            var childTask = new TaskModel();
            childTask.TaskInstanceId = Guid.NewGuid();

            //retrieve status and comments list and save in respective tables
            foreach (JObject content in fileUpdates.Children<JObject>())
            {
                var fileStatusModel = new ReviewFileStatusModel();


                var isApproved = false;


                var isFileRenamed = false;
                Tuple<Guid, string, string> fileRenameTuple;

                var taskFileId = new Guid();
                var fileReName = "";
                var fileExt = "";
                var fileStatus = 0;
                var fileId = 0;
                foreach (JProperty prop in content.Properties())
                {
                    if (prop.Name == "fileId")
                    {
                        fileId = int.Parse(prop.Value.ToString());
                        taskFileId = taskManager.GetTaskFileById(int.Parse(prop.Value.ToString())).TaskFileId;
                        fileStatusModel = reviewFileStatusManager.GetReviewFileStatusByTaskFileIdandViewId(taskFileId, xrefTaskDetails.ViewId);

                    }
                    else if (prop.Name == "ReqAdditionalInfo")
                    {
                        if (prop.Value.ToString() == "True" && fileStatusModel.Status != 4)
                        {
                            //create child req
                            fileStatusModel.Status = 4;
                            isRAI = true;
                        }
                    }
                    else if (prop.Name == "Comments")
                    {
                        if (prop.Value.ToString().Trim().Length > 0)
                        {
                            var fileCommentModel = new ReviewFileCommentModel();
                            fileCommentModel.ReviewFileCommentId = Guid.NewGuid();
                            fileCommentModel.CreatedOn = DateTime.UtcNow;
                            fileCommentModel.Comment = prop.Value.ToString();
                            fileCommentModel.FileTaskId = taskFileId;
                            fileCommentModel.ReviewerProdViewId = xrefTaskDetails.ViewId;
                            fileCommentModel.CreatedBy = fileStatusModel.ReviewerUserId;
                            reviewCommentList.Add(fileCommentModel);
                        }
                    }
                    else if (prop.Name == "Approve")
                    {
                        if (prop.Value.ToString() == "True" && fileStatusModel.Status != 5)
                        {
                            fileStatusModel.Status = 5;
                            isApproved = true;
                        }
                    }
                    else if (prop.Name == "isFileRenamed")
                    {
                        if (prop.Value.ToString() == "True")
                        {
                            isFileRenamed = true;
                        }
                    }
                    else if (prop.Name == "fileRename")
                    {
                        fileReName = prop.Value.ToString();
                    }
                    else if (prop.Name == "fileExt")
                    {
                        fileExt = prop.Value.ToString();
                    }
                    else if (prop.Name == "fileReviewStatusId")
                    {
                        fileStatus = Convert.ToInt16(prop.Value.ToString());
                    }
                }
                if (isFileRenamed)
                {
                    var taskFileModel = taskManager.GetTaskFileById(fileId);
                    if (taskFileModel != null && !string.IsNullOrEmpty(taskFileModel.DocTypeID))
                    {
                        var shortform = prod_DocumentType.GetDocumentTypeById(int.Parse(taskFileModel.DocTypeID)).ShortDocTypeName;
                        fileReName = shortform + '_' + fileReName;
                        fileRenameTuple = new Tuple<Guid, string, string>(taskFileId, fileReName, fileExt);
                        fileRenameList.Add(fileRenameTuple);
                    }

                }
                if (isRAI == true || isApproved == true)
                {
                    //To do - Add view id
                    fileStatusModel.ModifiedOn = DateTime.UtcNow;
                    fileStatusModel.ModifiedBy = fileStatusModel.ReviewerUserId;
                    reviewStatusList.Add(fileStatusModel);
                }
                if (fileStatus != (int)ReviewFileStatusList.RAI && fileStatusModel.Status == 4)
                {
                    var requestAdditionalInfoFiles = new RequestAdditionalInfoFileModel();
                    requestAdditionalInfoFiles.ChildTaskInstanceId = childTask.TaskInstanceId;
                    requestAdditionalInfoFiles.RequestAdditionalFileId = Guid.NewGuid();
                    requestAdditionalInfoFiles.TaskFileId = taskFileId;
                    requestAdditionalInfoFilesList.Add(requestAdditionalInfoFiles);
                }

            }
            //Creating child task for Request Additional info files
            var parentTask = taskManager.GetTasksByTaskInstanceId(xrefTaskDetails.TaskInstanceId).ToList();

            var requestAdditionalChildTask = new ParentChildTaskModel();

            var taskList = new List<TaskModel>();

            if (!requestAdditionalInfoFilesList.Any())
            {
                isRAI = false;
            }

            if (isRAI || (bool)(isNewfile) == true)
            {


                childTask.SequenceId = 0;
                childTask.AssignedBy = UserPrincipal.Current.UserName;
                childTask.AssignedTo = parentTask[0].AssignedBy;
                childTask.StartTime = DateTime.UtcNow;
                childTask.TaskStepId = (int)TaskStep.OPAAddtionalInformation;
                //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                childTask.MyStartTime = childTask.StartTime;
                childTask.IsReAssigned = false;
                childTask.Concurrency = parentTask[0].Concurrency;
                childTask.DataStore1 = parentTask[0].DataStore1;
                //adding fhanumber and pagetype      
                childTask.FHANumber = parentTask[0].FHANumber;
                childTask.PageTypeId = parentTask[0].PageTypeId;
                //childTask.PageTypeId = (int)PageType.ProductionApplication;
                var childTaskOpenStatusModel = new TaskOpenStatusModel();
                childTaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                childTask.TaskOpenStatus = XmlHelper.Serialize(childTaskOpenStatusModel,
                    typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(childTask);


                requestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                requestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                requestAdditionalChildTask.ParentTaskInstanceId = (Guid)xrefTaskInstanceId;
                requestAdditionalChildTask.ChildTaskInstanceId = childTask.TaskInstanceId;
                requestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();
            }




            //foreach (var item in reviewStatusList)
            //{
            //    if (item.Status == 4)
            //    {
            //        var requestAdditionalInfoFiles = new RequestAdditionalInfoFileModel();
            //        requestAdditionalInfoFiles.ChildTaskInstanceId = childTask.TaskInstanceId;
            //        requestAdditionalInfoFiles.RequestAdditionalFileId = Guid.NewGuid();
            //        requestAdditionalInfoFiles.TaskFileId = item.TaskFileId;
            //        requestAdditionalInfoFilesList.Add(requestAdditionalInfoFiles);
            //    }
            //}

            if (!string.IsNullOrEmpty((string)nfrComment))
            {
                newFileRequestModel.ChildTaskInstanceId = childTask.TaskInstanceId;
                newFileRequestModel.Comments = (string)nfrComment;
                newFileRequestModel.RequestedBy = UserPrincipal.Current.UserId;
                newFileRequestModel.RequestedOn = DateTime.UtcNow;
                newFileRequestModel.ReviewerProdViewId = xrefTaskDetails.ViewId;
            }
            try
            {
                if (!string.IsNullOrEmpty((string)nfrComment))
                {
                    projectActionFormManager.SaveNewFileRequest(newFileRequestModel);
                }
                if (isRAI || (bool)(isNewfile) == true)
                {
                    if (taskList.Count > 0)
                    {
                        taskManager.SaveTask(taskList, new List<TaskFileModel>());
                    }
                    parentChildTaskManager.AddParentChildTask(requestAdditionalChildTask);
                    requestAdditionalInfoFilesManager.SaveRequestAdditionalInfoFiles(requestAdditionalInfoFilesList);
                    ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
                }
                if (fileRenameList.Any())
                {
                    taskManager.RenameFiles(fileRenameList);
                }
                if (reviewStatusList.Any())
                {
                    reviewFileStatusManager.UpdateReviewFileStatus(reviewStatusList);
                }
                if (reviewCommentList.Any())
                {
                    reviewFileCommentManager.SaveReviewFileComment(reviewCommentList);
                }
                if (isRAI || (bool)(isNewfile) == true)
                {
                    var model = projectActionFormManager.GetOPAByTaskInstanceId(parentTask[0].TaskInstanceId);
                    if (childTask.AssignedTo != null && childTask.AssignedBy != null)
                    {
                        if (childTask.TaskStepId == 16)
                        {
                            model.RequestStatus = 7;
                        }
                        //OS-#2103 & #2125 the below mail functionality commented ,we now enabled because lender not receiving any mails regarding RAI task 
                        if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(new EmailManager(), childTask.AssignedTo,
                            childTask.AssignedBy, model))
                        {
                        }
                    }
                }
                if (isRAI || (bool)(isNewfile) == true)
                {
                    return "Request";
                }
                else
                {
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return "Failure";
            }


        }

        public string SaveReviewerFileUpdates(string editData)
        {
            if (RoleManager.IsReviewer(UserPrincipal.Current.UserName))
            {
                return SaveExternalReviewerUpdates(editData);
            }
            else
            {
                return SaveInternalReviewerUpdates(editData);
            }
        }


        public JsonResult GetPropertyInfo(string selectedFhaNumber, string IsIRRequest)
        {
            var result = new PropertyInfoModel();
            if (IsIRRequest == "False")
            {
                result = projectActionFormManager.GetProdPropertyInfo(selectedFhaNumber);
            }
            else
            {
                result = projectActionFormManager.GetPropertyInfo(selectedFhaNumber);
            }

            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        /* original ***
        public JsonResult GetRAIOpaHistoryGridContent(string sidx, string sord, int page, int rows,
         Guid taskInstanceId, string PropertyId, string FhaNumber)
        {

            bool isEditMode = true;
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            if (task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequest && (RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName)))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && !RoleManager.IsCurrentUserLenderRoles())
            {
                isEditMode = false;
            }

            if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = true;
            }


            var requestAddtionalTask = new List<Prod_OPAHistoryViewModel>();

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;


            requestAddtionalTask = GetProdOpaHistory(taskInstanceId, PropertyId, FhaNumber, 0, 0, isEditMode);

            if (requestAddtionalTask.Count > 0)
            {
                var RAILists = new List<Prod_OPAHistoryViewModel>();

                RAILists = (from model in requestAddtionalTask
                            where model.actionTaken == "Request Addtional Information" || model.actionTaken == null
                            select model).ToList();

                var NewFileRequest = new List<Prod_OPAHistoryViewModel>();

                NewFileRequest = (from model in requestAddtionalTask
                                  where model.actionTaken != "Request Addtional Information" && model.actionTaken != null
                                  orderby model.FolderSortingNumber
                                  select model).ToList();
                requestAddtionalTask = NewFileRequest.Concat(RAILists.OrderBy(x => x.id)).ToList();
                int totalrecods = requestAddtionalTask.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                if (sord.ToUpper() == "DESC")
                {

                    requestAddtionalTask = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList();

                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    requestAddtionalTask = requestAddtionalTask.OrderBy(s => s.submitDate).ToList();
                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                //Setting the date based on the Time Zone
                if (requestAddtionalTask != null)
                {
                    foreach (var item in requestAddtionalTask)
                    {
                        item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                        item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                        if (!string.IsNullOrEmpty(Convert.ToString(item.FolderKey)))
                        {
                            var DocTypeList = appProcessManager.GetMappedDocTypesbyFolder(item.FolderKey);

                            if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null && item.fileId != null)
                            {

                                item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                            }

                            if (item.SubfolderSequence != null)
                            {
                                item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                            }


                        }
                    }

                }
                var jsonData = new
                {

                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = requestAddtionalTask,

                };




                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = requestAddtionalTask,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }
		*/

        /// <summary>
        /// Documentation Grid
        /// </summary>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="taskInstanceId"></param>
        /// <param name="PropertyId"></param>
        /// <param name="FhaNumber"></param>
        /// <returns></returns>
        public JsonResult GetRAIOpaHistoryGridContent(string sidx, string sord, int page, int rows,
        Guid taskInstanceId, string PropertyId, string FhaNumber)
        {
            bool isEditMode = true;
            bool isCompleted = false;
            string pageIdApp = ConfigurationManager.AppSettings["APPLICATION REQUEST"] ?? ConfigurationSettings.AppSettings["APPLICATION REQUEST"];
            string pageIdAmend = ConfigurationManager.AppSettings["AMENDMENTS"] ?? ConfigurationSettings.AppSettings["AMENDMENTS"];
            string pageIdNCDC = ConfigurationManager.AppSettings["NON-CONSTRUCTION DRAFT CLOSING"] ?? ConfigurationSettings.AppSettings["NON-CONSTRUCTION DRAFT CLOSING"];
            string pageIdNCEC = ConfigurationManager.AppSettings["NON-CONSTRUCTION EXECUTED CLOSING"] ?? ConfigurationSettings.AppSettings["NON-CONSTRUCTION EXECUTED CLOSING"];
            int appPageId = !string.IsNullOrEmpty(pageIdApp) ? Convert.ToInt32(pageIdApp) : 5;
            int amendPageId = !string.IsNullOrEmpty(pageIdAmend) ? Convert.ToInt32(pageIdAmend) : 18;
            int ncdcPageId = !string.IsNullOrEmpty(pageIdNCDC) ? Convert.ToInt32(pageIdNCDC) : 10;
            int ncecPageId = !string.IsNullOrEmpty(pageIdNCEC) ? Convert.ToInt32(pageIdNCEC) : 17;

            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            int pagetypeID = task.PageTypeId.HasValue ? task.PageTypeId.Value : 0;

            if (task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
                isCompleted = true;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequest && (RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName)))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && !RoleManager.IsCurrentUserLenderRoles())
            {
                isEditMode = false;
            }

            if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = true;
            }


            var requestAddtionalTask = new List<Prod_OPAHistoryViewModel>();

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            requestAddtionalTask = GetProdOpaHistory(taskInstanceId, PropertyId, FhaNumber, 0, 0, isEditMode);

            if (requestAddtionalTask.Count > 0)
            {
                var RAILists = new List<Prod_OPAHistoryViewModel>();
                var NewFileRequest = new List<Prod_OPAHistoryViewModel>();
                var NewFilesRequest = new List<Prod_OPAHistoryViewModel>();

                NewFilesRequest = (from model in requestAddtionalTask
                                       //where ( model.actionTaken == "New File Request(s)" || model.actionTaken == null) && model.folderNodeName == "New File Request(s)"
                                   where model.folderNodeName == "New File Request(s)"
                                   select model).ToList();
                if (NewFilesRequest.Count() == 0)
                {
                    RAILists = (from model in requestAddtionalTask
                                where model.actionTaken == "Request Addtional Information" || model.actionTaken == null
                                select model).ToList();


                    NewFileRequest = (from model in requestAddtionalTask
                                      where model.actionTaken != "Request Addtional Information" && model.actionTaken != null
                                      orderby model.FolderSortingNumber
                                      select model).ToList();
                    requestAddtionalTask = NewFileRequest.Concat(RAILists.OrderBy(x => x.id)).ToList();

                    if (sord.ToUpper() == "DESC")
                    {
                        requestAddtionalTask = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList();
                        requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                    }
                    else
                    {
                        requestAddtionalTask = requestAddtionalTask.OrderBy(s => s.submitDate).ToList();
                        requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                    }
                }
                else
                {
                    if (!isCompleted)
                    {
                        //skumar-start: update folder keys w.r.t pagetype
                        if (Convert.ToInt32(pagetypeID) == ncdcPageId || Convert.ToInt32(pagetypeID) == ncecPageId)
                        {
                            List<int> ncFolderKeys = ControllerHelper.GetProd_FolderStructureNCFolderKey();
                            //requestAddtionalTask = requestAddtionalTask.Where(t => ncFolderKeys.Contains(t.FolderKey)).ToList().OrderBy(x => PadNumbers(x.FolderName)).ThenBy(x => x.FolderName);
                            requestAddtionalTask = requestAddtionalTask.Where(t => ncFolderKeys.Contains(t.FolderKey)).ToList();
                        }
                        if (Convert.ToInt32(pagetypeID) == appPageId)
                        {
                            List<int> appFolderKeys = ControllerHelper.GetProd_FolderStructureAppFolderKey();
                            requestAddtionalTask = requestAddtionalTask.Where(t => appFolderKeys.Contains(t.FolderKey)).ToList();
                        }
                        //skumar-end: update folder keys w.r.t pagetype
                    }
                }
                int totalrecods = requestAddtionalTask.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                //Setting the date based on the Time Zone
                if (requestAddtionalTask != null)
                {
                    foreach (var item in requestAddtionalTask)
                    {
                        //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                        //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                        item.uploadDate = item.uploadDate;
                        item.submitDate = item.submitDate;
                        if (!string.IsNullOrEmpty(Convert.ToString(item.FolderKey)))
                        {
                            var DocTypeList = appProcessManager.GetMappedDocTypesbyFolder(item.FolderKey);

                            //harish added this below line to check whether the file name contains others_ it will split after underscore like others_test -while excuting the below line as file name as test and it will save in taskfile table 
                            // 09042020 

                            if (item.DocTypeID == "2082" && (item.fileName.ToUpper().StartsWith("OTHERS_") || item.fileName.ToUpper().StartsWith("OTHER_")))
                            {

                                var matched = item.fileName.Split('_').Last();
                                var matchedinfo = DocTypeList.Where(x => item.fileName.Split('_').Last().Contains(x.DocumentType));
                                if (matchedinfo.Any())
                                {
                                    item.DocTypeID = matchedinfo.First().DocumentTypeId.ToString();
                                    var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                    if (taskfile != null)
                                    {

                                        item.fileName = taskfile.FileName;
                                        taskfile.DocTypeID = item.DocTypeID;
                                        taskManager.UpdateDoctyidinTaskfile(taskfile);

                                    }
                                }
                                else
                                {
                                    var matchedfile = item.fileName.Substring(7, item.fileName.Length - 7);

                                    var matchedinfodata = DocTypeList.Where(x => matchedfile.Contains(x.ShortDocTypeName + "_" + x.DocumentType));
                                    if (matchedinfodata.Any())
                                    {
                                        item.DocTypeID = matchedinfodata.First().DocumentTypeId.ToString();
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {

                                            item.fileName = taskfile.FileName;
                                            taskfile.DocTypeID = item.DocTypeID;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        }
                                    }
                                }
                                
                            }





                            if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null && item.fileId != null)
                            {
                                //harish new line of code for checking file name and document(ShortDocTypeName &DocumentType) 09-04-2020
                                if (DocTypeList.Where(x => item.fileName.Contains(x.ShortDocTypeName + "_" + x.DocumentType)).Any())
                                {
                                    item.fileName = item.fileName;
                                    item.folderNodeName = item.fileName;
                                    var matchedinfo = DocTypeList.Where(x => item.fileName.Contains(x.ShortDocTypeName + "_" + x.DocumentType)).First();
                                    item.DocTypeID = matchedinfo.DocumentTypeId.ToString();
                                    var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                    if (taskfile != null)
                                    {
                                        taskfile.DocTypeID = item.DocTypeID;
                                        taskfile.FileName = item.fileName;
                                        taskManager.UpdateDoctyidinTaskfile(taskfile);
                                    }

                                }

                                else
                                {
                                    var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.ShortDocTypeName + "_" + x.DocumentType.Split('_').First()));
                                    if (matched.Any())
                                    {

                                        //item.fileName = matched.First().ShortDocTypeName + "_" + matched.First().DocumentType;
                                        item.DocTypeID = matched.First().DocumentTypeId.ToString();
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            //string ext = Path.GetExtension(taskfile.FileName);
                                            taskfile.FileName = item.fileName;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);
                                        }
                                    }
                                    else
                                    {
                                        item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                                    }
                                }


                                //item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                            }

                            if (item.SubfolderSequence != null)
                            {
                                item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                            }
                        }
                    }

                }
                var jsonData = new
                {

                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = requestAddtionalTask,
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = requestAddtionalTask,
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ConfirmationPopup()
        {
            OPAViewModel model1 = (OPAViewModel)TempData["ApplicationModelforConfirmationPopup"];
            return PartialView("~/Views/Production/ApplicationRequest/ConfirmationPopup.cshtml", model1);
        }
        public ActionResult GenericPopup()
        {
            return PartialView("~/Views/Production/ApplicationRequest/popup/_ProductionGenericPopup.cshtml");
        }
        [HttpPost]
        public ActionResult ApproveOrDenyApplicationRequest(OPAViewModel model)
        {
            model.NoOfPendingRAI = appProcessManager.GetProdPendingRAI(model.TaskInstanceId).Count();
            //Route to UpdateHUDUsersCompletesReview if amendments
            if (model.pageTypeId == (int)PageType.Amendments && model.viewId != (int)ProductionView.WLM)
                model.viewId = (int)ProductionView.WLM;
            if (model.viewId == (int)ProductionView.UnderWriter)
            {
                return UpdateUnderwriterApproveOrDeny(model);
            }
            else if (RoleManager.IsReviewer(UserPrincipal.Current.UserName))
            {
                return UpdateReviewersCompletesReview(model);
            }
            else
            {
                return UpdateHUDUsersCompletesReview(model);
            }
        }

        private ActionResult UpdateUnderwriterApproveOrDeny(OPAViewModel model)
        {

            bool isChildApproved = true;
            model.ShowVisibility = true;
            string alertTitle = string.Empty;
            string alertText = string.Empty;
            //karri-MH232-79
            ArrayList alTaskinstanceIDs = new ArrayList();


            if (ModelState.IsValid && model.TaskStepId != (int)TaskStep.ProjectActionRequestComplete)
            {
                //karri-MH232-79//proxess for closing all subtasks except Firmcommitment
                //The assigned Underwriter should be able to complete their review and it will only close out RAIs. 
                //All subtasks and firm commitment tasks will remain open.
                //Load Only Non Firm Commitments tasks but not FC-Firm Commiments//can close all except Firm Commitments
                //RAI (240-22003)Construction NC
                var productionTasks1 = productionQueue.GetProductionTaskByUserName(UserPrincipal.Current.UserName, UserPrincipal.Current.UserRole)
                               .Where(m => m.TaskName.ToUpper().Contains(model.FhaNumber.ToUpper()) && m.Status.Equals("RAI"))
                               .OrderByDescending(m => m.ProductionTaskType[0]).ThenByDescending(m => m.LastUpdated).OrderByDescending(m => m.ProductionTaskType[0]);

                //fircommitment records starts with FC Request (240-22003)Construction NC
                var FCData = productionTasks1.Where(m => m.TaskName.ToUpper().StartsWith("FC Request".ToUpper())).ToList();
                var RAIsData = productionTasks1.Where(m => m.TaskName.ToUpper().StartsWith("RAI".ToUpper())).ToList();

                List<Guid> RAIsList = new List<Guid>();
                if (RAIsData != null && RAIsData.Count() > 0)
                    foreach (var item in RAIsData)
                    {
                        RAIsList.Add(item.ParentChildInstanceId);
                    }

                List<TaskModel> childTaskList = projectActionFormManager.GetProdChildTasksByXrefId(model.TaskInstanceId, false).Where(x => RAIsList.Contains(x.TaskInstanceId)).ToList();



                //int openReviewerTask = prod_TaskXrefManager.GetCountOfReviewersTaskPendingForUW(model.TaskInstanceId);

                if (model.IsApprovedPopupDisplayedForAe == false && model.DenyStatus != "Yes" && model.DenyStatus == null)
                {
                    if (childTaskList != null && childTaskList.Count > 0)
                    {

                        foreach (var childList in childTaskList)
                        {

                            if (childList.TaskStepId == (int)TaskStep.OPAAddtionalInformation)
                            {
                                isChildApproved = false;
                                model.IsApprovedPopupDisplayedForAe = false;
                                TempData["ApplicationModelforConfirmationPopup"] = model;
                                break;
                            }

                        }
                    }

                    if (isChildApproved == false)
                    {

                        model.ShowVisibility = false;
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 450, 350, "Confirmation", false, false, true, false, false, true, "ConfirmationPopup");
                        }

                        return View("~/Views/Production/ApplicationRequest/ApplicationRequestHUDReviewer.cshtml", model);
                    }
                }


                //foreach(var i in taskManager.UpdateTaskStatus1(model.TaskInstanceId))
                //{

                //        if (i.Status == 15)
                //        {
                //            //Naveen harish srinivas commented below line 19-11-2019
                //            model.RequestStatus = (int)RequestStatus.Approve;
                //            model.IsEditMode = false;
                //        }
                //        else
                //        {
                //            model.RequestStatus = 1;
                //            model.IsEditMode = true;
                //        }

                //}
                if (model.RequestStatus != (int)RequestStatus.Deny)
                {
                    //Naveen harish srinivas commented below line 19-11-2019
                    //#434
                    model.RequestStatus = (int)RequestStatus.Approve;
                    model.IsEditMode = false;
                }

                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;

                var taskList = new List<TaskModel>();
                var nextStage = new Prod_NextStageModel();
                var parentTask = new TaskModel();
                string assignedBy;
                string assignedTo;
                var requestAdditionalInfoFilesList = new List<RequestAdditionalInfoFileModel>();
                model.GroupTaskInstanceId = model.TaskGuid;
                TempData["IsReAssigned"] = model.IsReassigned.HasValue && model.IsReassigned.Value == true
                    ? "true"
                    : "false";


                if (model.RequestStatus == (int)RequestStatus.RequestAdditionalInfo)
                {
                    TempData["PopupText"] = "Additional Request is Sent.";
                    alertTitle = "Additional Request Sent";
                    var childTask = new TaskModel();
                    childTask.TaskInstanceId = Guid.NewGuid();
                    childTask.SequenceId = 0;
                    childTask.AssignedBy = UserPrincipal.Current.UserName;
                    childTask.AssignedTo = model.AssignedBy;
                    childTask.StartTime = DateTime.UtcNow;
                    childTask.TaskStepId = (int)TaskStep.OPAAddtionalInformation;
                    //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                    childTask.MyStartTime = childTask.StartTime;
                    childTask.IsReAssigned = false;
                    childTask.Concurrency = model.Concurrency;
                    //adding fhanumber and pagetype      
                    childTask.FHANumber = model.FhaNumber;
                    childTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                    var childTaskOpenStatusModel = new TaskOpenStatusModel();
                    childTaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    childTask.TaskOpenStatus = XmlHelper.Serialize(childTaskOpenStatusModel,
                        typeof(TaskOpenStatusModel),
                        Encoding.Unicode);
                    taskList.Add(childTask);
                    model.RequestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                    model.RequestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                    model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskGuid;
                    model.RequestAdditionalChildTask.ChildTaskInstanceId = childTask.TaskInstanceId;
                    model.RequestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();
                    childTask.Notes = model.RequestAdditionalComment;
                    assignedBy = childTask.AssignedBy;
                    assignedTo = childTask.AssignedTo;
                    var taskFileIdList =
                        reviewFileStatusManager.GetRequestAdditionalFileIdByParentTaskInstanceId((Guid)model.TaskGuid);
                    foreach (var item in taskFileIdList)
                    {
                        var requestAdditionalInfoFiles = new RequestAdditionalInfoFileModel();
                        requestAdditionalInfoFiles.ChildTaskInstanceId = childTask.TaskInstanceId;
                        requestAdditionalInfoFiles.RequestAdditionalFileId = Guid.NewGuid();
                        requestAdditionalInfoFiles.TaskFileId = item;
                        requestAdditionalInfoFilesList.Add(requestAdditionalInfoFiles);
                    }

                    model.IsRAI = true;
                }
                else
                {
                    parentTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                    if (model.RequestStatus == (int)RequestStatus.Approve)
                    {
                        alertText = string.Format("{0} Request Review completed.", projectActionFormManager.GetPageTypeNameById(model.pageTypeId));
                        alertTitle = "Complete Review";
                        TempData["PopupText"] = alertText;
                    }
                    else
                    {
                        alertText = string.Format("{0} Request Review was denied.", projectActionFormManager.GetPageTypeNameById(model.pageTypeId));
                        TempData["PopupText"] = alertText;
                        alertTitle = "Denied";
                    }
                    model.SequenceId++;
                    parentTask.SequenceId = (int)model.SequenceId;
                    parentTask.StartTime = DateTime.UtcNow;
                    //parentTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(parentTask.StartTime);
                    parentTask.MyStartTime = parentTask.StartTime;
                    parentTask.TaskInstanceId = model.TaskInstanceId;
                    parentTask.IsReAssigned = model.IsReassigned;
                    parentTask.Concurrency = model.Concurrency;
                    parentTask.AssignedTo = model.AssignedBy;
                    parentTask.AssignedBy = UserPrincipal.Current.UserName;
                    parentTask.Notes = model.AEComments;
                    //adding fhanumber and pagetype 
                    parentTask.FHANumber = model.FhaNumber;
                    parentTask.PageTypeId = model.pageTypeId;
                    //parentTask.PageTypeId = (int)PageType.ProductionApplication;
                    var taskOpenStatusModel = new TaskOpenStatusModel();
                    taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    parentTask.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                        Encoding.Unicode);
                    taskList.Add(parentTask);
                    assignedBy = parentTask.AssignedBy;
                    assignedTo = parentTask.AssignedTo;

                    nextStage.NextStgId = Guid.NewGuid();
                    nextStage.CompletedPgTypeId = model.pageTypeId;
                    nextStage.CompletedPgTaskInstanceId = model.TaskInstanceId;
                    nextStage.NextPgTypeId = GetNextStagePageTypeId(model.pageTypeId);
                    nextStage.ModifiedOn = DateTime.UtcNow;
                    nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                    nextStage.FhaNumber = model.FhaNumber;
                    nextStage.IsNew = true;
                }

                if (childTaskList != null && childTaskList.Count > 0 && !model.IsRAI)
                {
                    var childTask = new TaskModel();
                    var childtaskOpenStatusModel = new TaskOpenStatusModel();

                    foreach (TaskModel childTaskListItem in childTaskList)
                    {
                        if (childTaskListItem.TaskStepId != (int)TaskStep.ProjectActionRequestComplete)
                        {
                            childTask = new TaskModel();

                            childTask.TaskInstanceId = childTaskListItem.TaskInstanceId;
                            childTaskListItem.SequenceId++;
                            childTask.SequenceId = (int)childTaskListItem.SequenceId;
                            childTask.AssignedBy = UserPrincipal.Current.UserName;
                            childTask.AssignedTo = model.AssignedBy;
                            childTask.StartTime = DateTime.UtcNow;
                            //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                            childTask.MyStartTime = childTask.StartTime;
                            childTask.Notes = model.AEComments;
                            childTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                            childTask.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);//
                            childtaskOpenStatusModel = new TaskOpenStatusModel();
                            childtaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                            childTask.TaskOpenStatus = XmlHelper.Serialize(childtaskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                            childTask.IsReAssigned = model.IsReassigned;
                            childTask.Concurrency = model.Concurrency;
                            childTask.FHANumber = model.FhaNumber;
                            childTask.PageTypeId = model.pageTypeId;
                            //childTask.PageTypeId = (int)PageType.ProductionApplication;

                            taskList.Add(childTask);
                        }
                    }
                }

                var taskFile = new List<TaskFileModel>();
                try
                {
                    if (taskList.Count > 0)
                    {
                        if (nextStage != null && model.RequestStatus == (int)RequestStatus.Approve)
                        {
                            //karri-MH232-79//APPLICATION REQUEST//CONSTRUCTION SINGLE STAGE//CONSTRUCTION TWO STAGE INITIAL//CONSTRUCTION TWO STAGE FINAL
                            //since FHA availability for next stage is alrady done when Firmcomitment is raised, we skip the same step here for 6,7,8,9
                            //for first start up application process,when UW rasies Firmcommitment request, the FHA Number will be already
                            //allocated for the closing process for the lender as initiated below function. we have already done for pagetypeid=5
                            //in Firmcommitment Request itself, hence not required here for pagetypeid=5
                            if (model.pageTypeId != 5 && model.pageTypeId != 6 && model.pageTypeId != 7 && model.pageTypeId != 8)
                                prod_NextStageManager.AddNextStage(nextStage);
                        }
                        taskManager.SaveTask(taskList, taskFile);
                        taskManager.UpdateTaskStatus(model.TaskInstanceId);
                    }
                    if (model.IsRAI)
                    {
                        parentChildTaskManager.AddParentChildTask(model.RequestAdditionalChildTask);
                        requestAdditionalInfoFilesManager.SaveRequestAdditionalInfoFiles(requestAdditionalInfoFilesList);
                    }
                    else
                    {
                        model.MytaskId = projectActionFormManager.GetLatestTaskId(parentTask.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(model);
                        //Naveen Hareesh (Commented by)
                        //projectActionFormManager.UpdateProjectActionRequestForm(model);
                        groupTaskManager.UpdateOPAGroupTask(model);
                    }

                    backgroundJobManager.SendProjectActionSubmissionResponseEmail(emailManager, assignedTo, assignedBy, model);
                    //    {
                    bool isUpdateSuccess = prod_TaskXrefManager.UpdateTasXrefCompleteStatus(model.taskxrefId, model.AEComments);

                    //PDf Generation 
                    if (isUpdateSuccess && (model.pageTypeId == (int)PageType.ClosingAllExceptConstruction || model.pageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion))
                    {
                        if (model.pageTypeId != 10)
                        {
                            var pdfgenerated = GenerateSharepointPdf(model.TaskInstanceId, model.FhaNumber, model.PropertyId);
                        }
                    }

                    //


                    if (!model.IsApprovedPopupDisplayedForAe)
                    {
                        int userId = UserPrincipal.Current.UserId;
                        model.IsApprovedPopupDisplayedForAe = true;
                        PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                            true, "GenericPopup");
                        model.ProjectActionName =
                            projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                        model.IsAgreementAccepted = true; // User Story 1901
                        GetDisclaimerText(model); // User Story 1901
                        model.IsApprovedPopupDisplayedForAe = true;
                        model.PopupTarget = "#divHtml";

                        return View("~/Views/Production/ApplicationRequest/ApplicationRequestHUDReviewer.cshtml", model);
                    }
                    //   }

                }
                catch (DbUpdateConcurrencyException)
                {

                }
            }
            model.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
            return RedirectToAction("Index", "ProductionMyTask");


        }

        /// <summary>
        /// update completion status in taskdb and taskxref
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ActionResult UpdateHUDUsersCompletesReview(OPAViewModel model)
        {
            bool isUpdateSuccess = false;
            bool isChildApproved = true;
            bool isAmendmentProductionUserOnly = false;
            bool isAmendmentUserSkipSign = false;
            model.ShowVisibility = true;
            string alertTitle = string.Empty;
            string alertText = string.Empty;
            Prod_FormAmendmentTaskModel objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
            IList<Prod_FormAmendmentTaskModel> olistProd_FormAmendmentTaskModel = new List<Prod_FormAmendmentTaskModel>();

            if (ModelState.IsValid)
            {
                List<TaskModel> childTaskList = projectActionFormManager.GetProdChildTasksByXrefId(model.taskxrefId, true);
                alertText = string.Format("{0} Request Review completed.", projectActionFormManager.GetPageTypeNameById(model.pageTypeId));

                if (model.IsApprovedPopupDisplayedForAe == false && model.DenyStatus != "Yes" && model.DenyStatus == null)
                {
                    if (childTaskList != null && childTaskList.Count > 0)
                    {

                        foreach (var childList in childTaskList)
                        {

                            if (childList.TaskStepId == (int)TaskStep.OPAAddtionalInformation)
                            {
                                isChildApproved = false;
                                model.IsApprovedPopupDisplayedForAe = false;
                                TempData["ApplicationModelforConfirmationPopup"] = model;
                                break;
                            }

                        }
                    }

                    if (isChildApproved == false)
                    {

                        model.ShowVisibility = false;
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 450, 350, "Confirmation", false, false, true, false, false, true, "ConfirmationPopup");
                        }

                        return View("~/Views/Production/ApplicationRequest/ApplicationRequestHUDReviewer.cshtml", model);
                    }
                }



                //if (model.RequestStatus != (int)RequestStatus.Deny)
                //{
                //    //Naveen harish srinivas commented below line 19-11-2019
                //    model.RequestStatus = (int)RequestStatus.Approve;
                //    model.IsEditMode = false;
                //    model.GroupTaskInstanceId = model.TaskGuid;
                //    //model.GroupTaskInstanceId = model.TaskGuid;
                //    groupTaskManager.UpdateOPAGroupTask(model);
                //}

                if ((model.pageTypeId == (int)PageType.Amendments) && (model.viewId == (int)ProductionView.WLM) && (model.userRole == HUDRole.ProductionUser.ToString()))
                {
                    //exit if amendment template is not done
                    olistProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentByTaskInstanceId(model.TaskInstanceId);
                    if (olistProd_FormAmendmentTaskModel != null && olistProd_FormAmendmentTaskModel.Count > 0)
                        objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.Where(o => o.CreatedBy == model.userId).FirstOrDefault();
                    if (objProd_FormAmendmentTaskModel == null)
                        isAmendmentUserSkipSign = true;
                    isUpdateSuccess = false;
                    isAmendmentProductionUserOnly = true;
                }
                else
                {
                    //exit if amendment template is not done
                    olistProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentByTaskInstanceId(model.TaskInstanceId);
                    if (olistProd_FormAmendmentTaskModel != null && olistProd_FormAmendmentTaskModel.Count > 0)
                        objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.Where(o => o.CreatedBy == model.userId).FirstOrDefault();
                    if (objProd_FormAmendmentTaskModel == null)
                        isAmendmentUserSkipSign = true;
                    if (!isAmendmentUserSkipSign)
                    {
                        isUpdateSuccess = prod_TaskXrefManager.UpdateTasXrefCompleteStatus(model.taskxrefId, model.AEComments);
                        ////Create DAP
                        //Prod_TaskXrefModel objProd_TaskXrefModel = new Prod_TaskXrefModel()
                        //{
                        //	TaskXrefid = Guid.NewGuid(),
                        //	TaskInstanceId = objProd_FormAmendmentTaskModel.TaskInstanceId.Value,
                        //	TaskId = objProd_FormAmendmentTaskModel.TaskId,
                        //	AssignedBy = UserPrincipal.Current.UserId,
                        //	AssignedTo = objProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId,
                        //	IsReviewer = false,
                        //	Status = Convert.ToInt32(ApplicationAndClosingType.Amendments),
                        //	AssignedDate = DateTime.UtcNow,
                        //	ViewId = Convert.ToInt32(ProductionView.DAP),
                        //	ModifiedOn = DateTime.UtcNow
                        //};
                        //prod_TaskXrefManager.AddTaskXref(objProd_TaskXrefModel);

                    }
                }
                var taskList = new List<TaskModel>();
                if (isUpdateSuccess)
                {

                    TempData["PopupText"] = alertText;
                    alertTitle = "Complete Review";

                    if (childTaskList != null && childTaskList.Count > 0 && !model.IsRAI)
                    {


                        var childTask = new TaskModel();
                        var childtaskOpenStatusModel = new TaskOpenStatusModel();

                        foreach (TaskModel childTaskListItem in childTaskList)
                        {
                            if (childTaskListItem.TaskStepId != (int)TaskStep.ProjectActionRequestComplete)
                            {
                                childTask = new TaskModel();

                                childTask.TaskInstanceId = childTaskListItem.TaskInstanceId;
                                childTaskListItem.SequenceId++;
                                childTask.SequenceId = (int)childTaskListItem.SequenceId;
                                childTask.AssignedBy = UserPrincipal.Current.UserName;
                                childTask.AssignedTo = childTaskListItem.AssignedTo;
                                childTask.StartTime = DateTime.UtcNow;
                                //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                                childTask.MyStartTime = childTask.StartTime;
                                childTask.Notes = model.AEComments;
                                childTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;

                                childtaskOpenStatusModel = new TaskOpenStatusModel();
                                childtaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                                childTask.TaskOpenStatus = XmlHelper.Serialize(childtaskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                                childTask.IsReAssigned = childTaskListItem.IsReAssigned;
                                childTask.Concurrency = childTaskListItem.Concurrency;
                                childTask.FHANumber = model.FhaNumber;
                                childTask.PageTypeId = model.pageTypeId;
                                // childTask.PageTypeId = (int)PageType.ProductionApplication;

                                taskList.Add(childTask);
                            }
                        }
                    }
                }
                else
                {

                    TempData["PopupText"] = "An error occured.";
                    alertTitle = "Error";
                }

                if (isAmendmentProductionUserOnly)
                {
                    alertText = string.Format("{0} Request Review in process and waiting for WLM complete review.", projectActionFormManager.GetPageTypeNameById(model.pageTypeId));
                    TempData["PopupText"] = alertText;
                    alertTitle = "Review Status";
                }

                if (isAmendmentUserSkipSign)
                {
                    alertText = string.Format("{0} Request Review incomplete.Please complete Amendment Template.", projectActionFormManager.GetPageTypeNameById(model.pageTypeId));
                    TempData["PopupText"] = alertText;
                    alertTitle = "Review Status";
                }

                foreach (var i in taskManager.UpdateTaskStatus1(model.TaskInstanceId))
                {
                    if (taskManager.UpdateTaskStatus1(model.TaskInstanceId).Count() == 4)
                    {
                        if (i.Status == 15)
                        {
                            //Naveen harish srinivas commented below line 19-11-2019
                            model.RequestStatus = (int)RequestStatus.Approve;
                            model.IsEditMode = false;
                            model.TaskInstanceId = model.TaskInstanceId;
                            groupTaskManager.UpdateOPAGroupTask(model);
                        }
                        else
                        {
                            model.TaskInstanceId = model.TaskInstanceId;
                            model.RequestStatus = 1;
                            model.IsEditMode = true;
                            groupTaskManager.UpdateOPAGroupTask(model);
                            break;
                        }


                    }
                }
                var taskFile = new List<TaskFileModel>();
                try
                {
                    if (taskList.Count > 0)
                    {
                        taskManager.SaveTask(taskList, taskFile);
                    }

                    //     if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(emailManager, assignedTo,
                    //         assignedBy, model))
                    //    {
                    if (!model.IsApprovedPopupDisplayedForAe)
                    {
                        int userId = UserPrincipal.Current.UserId;
                        model.IsApprovedPopupDisplayedForAe = true;
                        PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                            true, "GenericPopup");
                        model.ProjectActionName =
                            projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                        model.IsAgreementAccepted = true; // User Story 1901
                        GetDisclaimerText(model); // User Story 1901
                        model.IsApprovedPopupDisplayedForAe = true;
                        model.PopupTarget = "#divHtml";

                        return View("~/Views/Production/ApplicationRequest/ApplicationRequestHUDReviewer.cshtml", model);
                    }


                    //}
                }
                catch (DbUpdateConcurrencyException)
                {

                }


            }

            return RedirectToAction("Index", "ProductionMyTask");


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ActionResult UpdateReviewersCompletesReview(OPAViewModel model)
        {
            model.ShowVisibility = true;
            string alertTitle = string.Empty;
            string alertText = string.Empty;
            if (ModelState.IsValid)
            {
                alertText = string.Format("{0} Request Review completed.", projectActionFormManager.GetPageTypeNameById(model.pageTypeId));
                bool isUpdateSuccess = prod_TaskXrefManager.UpdateTasXrefCompleteStatus(model.taskxrefId, model.AEComments);
                if (isUpdateSuccess)
                {
                    //if (model.pageTypeId == (int)PageType.ClosingAllExceptConstruction)
                    //{
                    //	TempData["PopupText"] = "Closing Request Review completed.";
                    //}
                    //else
                    //{
                    //	TempData["PopupText"] = "Application Request Review completed.";
                    //}
                    //alertTitle = "Approved";
                    alertTitle = "Approved";
                    TempData["PopupText"] = alertText;
                }
                else
                {

                    TempData["PopupText"] = "An error occured.";
                    alertTitle = "Error";
                }
                try
                {

                    //     if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(emailManager, assignedTo,
                    //         assignedBy, model))
                    //    {
                    if (!model.IsApprovedPopupDisplayedForAe)
                    {
                        int userId = UserPrincipal.Current.UserId;
                        model.IsApprovedPopupDisplayedForAe = true;
                        PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                            true, "GenericPopup");
                        model.ProjectActionName =
                            projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                        model.IsAgreementAccepted = true; // User Story 1901
                        GetDisclaimerText(model); // User Story 1901
                        model.IsApprovedPopupDisplayedForAe = true;
                        model.PopupTarget = "#divHtml";

                        return View("~/Views/Production/ApplicationRequest/ApplicationRequestHUDReviewer.cshtml", model);
                    }
                    //   }
                }
                catch (DbUpdateConcurrencyException)
                {

                }
            }

            return RedirectToAction("Index", "ProductionMyTask");
        }

        [HttpGet]
        public ActionResult GetApplicationFormDetail(OPAViewModel model)
        {

            if (model == null)
            {

                model = (OPAViewModel)TempData.Peek("ApplicationRequestFormData");
                // added by harish ,naveen ,srinivas
                foreach (var i in taskManager.UpdateTaskStatus1(model.TaskInstanceId).OrderBy(x => x.ViewId))
                {
                    if (model.viewId == i.ViewId && (i.Status == 15 || i.Status == 19))
                    {
                        model.IsEditMode = false;
                        model.RequestStatus = 2;
                    }
                    else if (model.viewId == i.ViewId && i.Status == 18)
                    {
                        model.IsEditMode = true;
                    }
                }
            }

            if (model != null)
            {
                //karri-MH232-80//process for checcking if Firmcommitment is raised before closing the review
                //The assigned Underwriter should be able to complete their review and it will only close out RAIs. 
                //All subtasks and firm commitment tasks will remain open.
                //Load Only Non Firm Commitments tasks but not FC-Firm Commiments//can close all except Firm Commitments
                //RAI (240-22003)Construction NC
                if (model.pageTypeId == 5 || model.pageTypeId == 6 || model.pageTypeId == 7 || model.pageTypeId == 8)//for application process, 6, 7, 8??
                {
                    var productionTasks1 = productionQueue.GetProductionTaskByUserName(UserPrincipal.Current.UserName, UserPrincipal.Current.UserRole)
                                   .Where(m => m.TaskName.ToUpper().Contains(model.FhaNumber.ToUpper()) && m.Status.Equals("RAI"))
                                   .OrderByDescending(m => m.ProductionTaskType[0]).ThenByDescending(m => m.LastUpdated).OrderByDescending(m => m.ProductionTaskType[0]);

                    //fircommitment records starts with FC Request (240-22003)Construction NC
                    var FCData = productionTasks1.Where(m => (m.StatusId == 20 || m.StatusId == 21) && (m.TaskName.ToUpper().StartsWith("FC Request".ToUpper()) || m.TaskName.ToUpper().StartsWith("FC Response".ToUpper()))).ToList();
                    var RAIsData = productionTasks1.Where(m => m.Status.ToUpper().StartsWith("RAI".ToUpper())).ToList();
                    if (FCData == null || FCData.Count() == 0)
                        model.isFCRaised = 0;
                    else
                        model.isFCRaised = 1;
                }
                else
                    model.isFCRaised = 1;

                model.NoOfPendingRAI = appProcessManager.GetProdPendingRAI(model.TaskInstanceId).Count();
                bool hasTaskOpenByUser = TempData["hasTaskOpenByUser"] != null
                        ? (bool)TempData["hasTaskOpenByUser"]
                    : false;
                //model.ProjectActionName = projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                model.ProjectActionName = appProcessManager.GetProjectTypebyID(model.ProjectActionTypeId);
                var propertyInfoModel = projectActionFormManager.GetProdPropertyInfo(model.FhaNumber);
                if (propertyInfoModel != null)
                {
                    model.PropertyId = propertyInfoModel.PropertyId;
                    model.PropertyAddress.AddressLine1 = propertyInfoModel.StreetAddress;
                    model.PropertyAddress.City = propertyInfoModel.City;
                    model.PropertyAddress.StateCode = propertyInfoModel.State;
                    model.PropertyAddress.ZIP = propertyInfoModel.Zipcode;
                }
                model.IsRAI = false;
                //05052020 OS -support -2031 and 2048 added by siddu $ hari for task open after completion of task by appraiser,enivronment and survey 
                if (model.viewId != 1 && model.AssignedTo == null)
                {
                    model.AssignedTo = accountManager.GetUserNameById(model.CreatedBy);
                }
                if ((model.RequestStatus == (int)RequestStatus.Approve ||
                     model.RequestStatus == (int)RequestStatus.Deny) && !hasTaskOpenByUser &&
                    (RoleManager.IsUserLenderRole(model.AssignedTo) ||
                     RoleManager.IsInspectionContractor(model.AssignedTo)) && !model.IsPAMReport &&
                    (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) ||
                     RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName)))
                {

                    var task = new TaskModel();
                    var taskList = new List<TaskModel>();
                    task.AssignedBy = model.AssignedBy;
                    task.AssignedTo = model.AssignedTo;
                    task.StartTime = DateTime.UtcNow;
                    //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                    task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                    task.TaskOpenStatus = XmlHelper.Serialize(model.TaskOpenStatus, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                    task.IsReAssigned = model.IsReassigned;
                    task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                    //adding fhanumber and pagetype 
                    task.FHANumber = model.FhaNumber;
                    task.PageTypeId = model.pageTypeId;
                    //task.PageTypeId = (int)PageType.ProductionApplication;
                    var taskFile = new TaskFileModel();
                    try
                    {
                        // task.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                        task.DataStore1 = null;
                        taskList.Add(task);
                        if (taskList.Count > 0)
                        {
                            taskManager.SaveTask(taskList, taskFile);
                        }
                        if (!parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
                        {
                            model.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                            projectActionFormManager.UpdateTaskId(model);
                        }

                    }
                    catch (Exception e)
                    {
                        ErrorSignal.FromCurrentContext().Raise(e);
                        throw new InvalidOperationException(
                            string.Format("Error project action request form detail: {0}", e.InnerException),
                            e.InnerException);
                    }
                }
                model.hasReviewComments = true;
                model.userName = UserPrincipal.Current.FullName;
                model.userRole = UserPrincipal.Current.UserRole;
                model.userId = UserPrincipal.Current.UserId;

                model.GridVisibility = "ShowForLender";
                model.IsAgreementAccepted = true; // User Story 1901
                GetDisclaimerText(model); // User Story 1901   
                Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
                model.TransAccessStatus = checkResult.status;
                if (TempData["UploadFailed"] != null)
                {
                    ViewBag.UploadFailed = TempData["UploadFailed"].ToString();
                }
                if (model.IschildTask != null && model.IschildTask == true)
                {
                    if (model.TaskStepId < 20)
                    {
                        model.GroupTaskInstanceId = model.ChildTaskInstanceId;
                        var list = GetProdOpaHistory(model.ChildTaskInstanceId, model.PropertyId.ToString(), model.FhaNumber, 0, 0, false).ToList();
                        ViewBag.IsFileAttached = list.Where(x => x.childFileName != "").Count() > 0;
                        return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml",
                            model);

                    }
                    else
                    {
                        model.TaskGuid = model.TaskInstanceId;
                        return View("~/Views/Production/ApplicationRequest/ApplicationParentRequest.cshtml", model);
                    }

                }
                else if (RoleManager.IsCurrentUserLenderRoles() ||
                         RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    // model.GroupTaskInstanceId = model.ChildTaskInstanceId;
                    return View("~/Views/Production/ApplicationRequest/ApplicationParentRequest.cshtml", model);

                }
                else
                {
                    // model.GroupTaskInstanceId = model.ChildTaskInstanceId;
                    return View("~/Views/Production/ApplicationRequest/ApplicationRequestHUDReviewer.cshtml", model);
                }
            }
            else
            {
                return RedirectToAction("Index", "ProductionMyTask");
            }


        }

        [HttpPost]
        public void ExternalReviewerTaskAsignment(ProductionTaskAssignmentModel model)
        {

            model.IsReviewer = false;
            model.AssignedBy = UserPrincipal.Current.UserId;
            model.Status = (int)ProductionAppProcessStatus.InProcess;
            model.ViewId = model.AssignedToViewId;
            model.ModifiedOn = DateTime.UtcNow;
            model.ModifiedBy = UserPrincipal.Current.UserId;
            model.AssignedTo = model.AssignedToUserId;
            model.TaskXrefid = Guid.NewGuid();
            prod_TaskXrefManager.AddTaskXref(model);


            taskManager.SaveReviewFileStatus(model.TaskInstanceId, model.AssignedToUserId, UserPrincipal.Current.UserId);


        }

        public ActionResult LenderChildSubmit(OPAViewModel model, Guid? taskGuid, Guid? grouptaskGuid, int? sequenceId, byte[] concurrency)
        {
            var toemails = new List<string>();

            var tmp = Url.Action("FolderRearrangePopupLoad", "ProductionApplication");
            var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(model.TaskInstanceId)
                            select new
                            {
                                filelist.FileId,
                                filelist.DocTypeID,
                                docId = filelist.DocId,
                                Name = filelist.FileName,
                                filelist.FileType
                            }).Where(m => string.IsNullOrEmpty(m.DocTypeID) && m.FileType != FileType.WP);


            if (FileList.Count() > 0)
            {
                model.IsChangeDocTypePopupDisplayed = true;
                model.IschildTask = true;
                TempData["AppProcessModel"] = model;
                //return RedirectToAction("ApplicationRequestRAIHUDandReadOnly", new { taskinstanceId = model.TaskInstanceId, grouptaskinstanceId = model.GroupTaskInstanceId, comments = model.LenderComment, redirectfrom = "RAIRequest" });
                return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml", model);
            }

            else //if (TempData["ChildTaskSaved"] == null)
            {
                model.IsChangeDocTypePopupDisplayed = false;
                if (ModelState.IsValid)
                {
                    // var updatedconcurrency = taskManager.GetConcurrencyTimeStamp((Guid)model.TaskGuid);
                    model.ModifiedBy = UserPrincipal.Current.UserId;
                    model.ModifiedOn = DateTime.UtcNow;


                    var taskList = new List<TaskModel>();
                    var task = new TaskModel();

                    string alertTitle = String.Empty;

                    task.AssignedTo = model.AssignedBy;
                    task.AssignedBy = UserPrincipal.Current.UserName;

                    model.RequestStatus = (int)RequestStatus.Submit;

                    TempData["PopupText"] = "Additional Information is Sent.";
                    alertTitle = "Additional Information Sent";
                    task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                    model.Concurrency = concurrency;
                    model.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0;
                    task.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                    task.Notes = model.LenderComment;
                    task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                    task.StartTime = DateTime.UtcNow;
                    //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                    task.IsReAssigned = model.IsReassigned;
                    task.Concurrency = concurrency;
                    //adding fhanumber and pagetype 
                    task.FHANumber = model.FhaNumber;
                    task.PageTypeId = model.pageTypeId;
                    //task.PageTypeId = (int)PageType.ProductionApplication; 
                    var taskOpenStatusModel = new TaskOpenStatusModel();
                    taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                        Encoding.Unicode);
                    taskList.Add(task);
                    var taskFile = new List<TaskFileModel>();

                    // Update Status 
                    Guid ChildTaskInstanceId = model.ChildTaskInstanceId;
                    var results = reviewFileStatusManager.GetTaskFileIdByChildTaskInstanceId(ChildTaskInstanceId);
                    var resultset = results;
                    foreach (var newresults in results)
                    {
                        reviewFileStatusManager.UpdateReviewFileStatusForTaskFileId(newresults.TaskFileId);
                    }

                    try
                    {
                        if (taskList.Count > 0)
                        {
                            taskManager.SaveTask(taskList, taskFile);
                            TempData["ChildTaskSaved"] = "True";
                        }
                        model.ServicerComments = model.LenderComment;
                        //   if (backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo, task.AssignedBy,
                        //       model))
                        //  {
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                                true, "GenericPopup");
                            //model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
                            model.ProjectActionName =
                                projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                            // return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
                        }
                        // }
                    }
                    catch (DbUpdateConcurrencyException)
                    {

                    }
                    model.IsChangeDocTypePopupDisplayed = false;
                }



                var list = prod_TaskXrefManager.GetProductionSubTasks(model.TaskInstanceId);
                foreach (var item in list)
                {
                    toemails.Add(productionQueue.GetUserById(item.AssignedTo).UserName);

                }
            }

            backgroundJobManager.SendAppProcessLenderRAISubmitEmail(new EmailManager(), toemails, model);
            return RedirectToAction("Index", "ProductionMyTask");
        }

        public string SaveRAIToTaskFileHistory(string propertyid, string Fhanumber, string TaskfileId)
        {
            RestfulWebApiUpdateModel replacemodel = new RestfulWebApiUpdateModel();
            RestfulWebApiResultModel WebApiUploadResult = new RestfulWebApiResultModel();
            RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();

            var fileId = Request.Form["fileId"];
            var childTaskInstanceId = Request.Form["childInstanceId"];
            //var isNewFile = false;
            var files = Request.Files;
            var taskFileModel = new TaskFileModel();
            var opaProjectActionViewModel = new OPAViewModel();
            // Guid taskInstanceIdForFileTable = new Guid();
            var childTaskNewFile = new ChildTaskNewFileModel();
            //replacemodel.docId = taskManager.GetTaskFileById(Convert.ToInt32(TaskfileId)).DocId;
            var taskFileInfo = taskManager.GetTaskFileById(Convert.ToInt32(TaskfileId));
            replacemodel.docId = taskFileInfo.DocId;
            replacemodel.documentType = taskFileInfo.DocTypeID;
            string Updateresult = "";
            if (fileId != "")
            {
                taskFileModel = taskManager.GetTaskFileById(int.Parse(fileId));
                opaProjectActionViewModel = projectActionFormManager.GetOPAByTaskInstanceId(taskFileModel.TaskInstanceId);
            }


            foreach (string file in files)
            {
                //var expectedFileName = Request.Form["expectedFileName"];
                //Get filename from DB
                var expectedFileName = taskFileModel.FileName;
                HttpPostedFileBase myFile = Request.Files[file];

                if (myFile != null && myFile.ContentLength != 0)
                {

                    if (expectedFileName != null &&
                             Path.GetExtension(expectedFileName) != Path.GetExtension(myFile.FileName))
                    {
                        expectedFileName = expectedFileName.Replace(Path.GetExtension(expectedFileName),
                            Path.GetExtension(myFile.FileName));

                    }
                    Guid taskInstanceIdForFileTable = new Guid(childTaskInstanceId);
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();
                    var UniqueId = randoms.Next(0, 99999);
                    var systemFileName = opaProjectActionViewModel.FhaNumber + "_" +
                                         opaProjectActionViewModel.ProjectActionTypeId + "_" +
                                         taskInstanceIdForFileTable + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);
                    string pathForSaving = Server.MapPath("~/Uploads");

                    request = WebApiTokenRequest.RequestToken();

                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    if (!string.IsNullOrEmpty(replacemodel.docId))
                    {
                        WebApiUploadResult = WebApiDocumentReplace.ReplaceDocument(replacemodel, request.access_token, myFile);

                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                        {
                            var taskFile = PoupuLateRequestAdditionalTaskFile(myFile, fileSize, UniqueId.ToString(),
                             expectedFileName, systemFileName.ToString(),
                             taskInstanceIdForFileTable);
                            taskFile.API_upload_status = WebApiUploadResult.status;
                            taskFile.DocId = WebApiUploadResult.docId;
                            taskFile.DocTypeID = taskFileInfo.DocTypeID;
                            taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                            var childTaskFileId = taskManager.SaveGroupTaskFileModel(taskFile);

                            var mappingmodel = new TaskFile_FolderMappingModel();
                            mappingmodel.FolderKey = taskManager.GetFolderKeyForChildFileId(taskFileModel.TaskFileId);
                            mappingmodel.TaskFileId = childTaskFileId;
                            mappingmodel.CreatedOn = DateTime.UtcNow;
                            taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);

                            if (fileId != "")
                            {
                                var taskFileHistoryModel = new TaskFileHistoryModel();
                                taskFileHistoryModel.TaskFileHistoryId = Guid.NewGuid();
                                taskFileHistoryModel.ChildTaskInstanceId = new Guid(childTaskInstanceId);
                                taskFileHistoryModel.ParentTaskFileId = taskFileModel.TaskFileId;
                                taskFileHistoryModel.ChildTaskFileId = childTaskFileId;
                                projectActionFormManager.SaveLenderRequestAdditionalFiles(taskFileHistoryModel);
                                var result =
                                   reviewFileStatusManager.UpdateReviewFileStatusForRequestAdditionalResponse(
                                       taskFileModel.TaskFileId);
                                return result ? "Success" : "Failure";
                            }
                            Updateresult = "Success";
                        }
                        else
                        {
                            Updateresult = "Failure";
                        }
                    }

                }
            }
            return Updateresult;
        }

        public string DeleteFileAndChildFile(int taskfileid)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            var success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid));
                if (success == 1)
                {
                    System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
                    if (projectActionFormManager.IsFileHistoryExists(taskFile.TaskFileId))
                    {
                        success = projectActionFormManager.DeleteChildTaskFile(taskFile.TaskFileId);
                        if (success == 1)
                            return "Success";
                    }
                    return "Success";
                }
            }

            return "Failure";
        }

        private TaskFileModel PoupuLateRequestAdditionalTaskFile(HttpPostedFileBase fileStream, double fileSize, string checkListId, string expectedFileName, string systemFileName, Guid taskInstanceId)
        {
            var taskFile = new TaskFileModel();
            taskFile.FileName = expectedFileName;
            taskFile.GroupFileType = checkListId;
            taskFile.UploadTime = DateTime.UtcNow;
            // taskFile.FileName = fileStream.FileName;
            taskFile.FileSize = fileSize;
            taskFile.SystemFileName = systemFileName;
            taskFile.TaskInstanceId = taskInstanceId;
            taskFile.CreatedBy = UserPrincipal.Current.UserId;
            //BinaryReader rdr = null;
            try
            {
                //rdr = new BinaryReader(fileStream.InputStream);
                taskFile.FileData = null;//rdr.ReadBytes(fileStream.ContentLength);
            }
            finally
            {
                // Ensure Streams do not get left to garbage collection, the data can persist.
                //if (rdr != null)
                //{
                //    rdr.Close();
                //    rdr.Dispose();
                //    fileStream.InputStream.Flush();
                //}
                //fileStream.InputStream.Close();
                //fileStream.InputStream.Dispose();
            }

            return taskFile;

        }

        /// <summary>
        /// Send email to lender based on pageid retrieved from configurations
        /// </summary>
        /// <param name="taskInstanceId"></param>
        /// <returns></returns>
        public ActionResult SendEmailToLender(Guid taskInstanceId)
        {
            //get pageid from the configurations
            //string frmR4R = ConfigurationManager.AppSettings["R4R"];//1
            //string frmNCRE = ConfigurationManager.AppSettings["NCRE"];//2
            //string frmOPA = ConfigurationManager.AppSettings["OPA"];//3
            //string frmFHA_REQUEST = ConfigurationManager.AppSettings["FHA# REQUEST"];//4
            //string frmAPPLICATION_REQUEST = ConfigurationManager.AppSettings["APPLICATION REQUEST"];//5
            //string frmCONSTRUCTION_SINGLE_STAGE = ConfigurationManager.AppSettings["CONSTRUCTION SINGLE STAGE"];//6
            //string frmCONSTRUCTION_TWO_STAGE_INITIAL = ConfigurationManager.AppSettings["CONSTRUCTION TWO STAGE INITIAL"];//7
            //string frmCONSTRUCTION_TWO_STAGE_FINAL = ConfigurationManager.AppSettings["CONSTRUCTION TWO STAGE FINAL"];//8
            //string frmCONSTRUCTION_MANAGEMENT = ConfigurationManager.AppSettings["CONSTRUCTION MANAGEMENT"];//9
            //string frmNON_CONSTRUCTION_DRAFT_CLOSING = ConfigurationManager.AppSettings["NON-CONSTRUCTION DRAFT CLOSING"];//10
            //string frmCLOSING_TWO_STAGE_INITIAL = ConfigurationManager.AppSettings["CLOSING TWO STAGE INITIAL"];//11
            //string frmCLOSING_TWO_STAGE_FINAL = ConfigurationManager.AppSettings["CLOSING TWO STAGE FINAL"];//12
            //string frmFORM_290 = ConfigurationManager.AppSettings["FORM 290"];//16
            //string frmNON_CONSTRUCTION_EXECUTED_CLOSING = ConfigurationManager.AppSettings["NON-CONSTRUCTION EXECUTED CLOSING"];//17

            //int frm_R4R = !string.IsNullOrEmpty(frmR4R) ? Convert.ToInt32(frmR4R) : 1;//1
            //int frm_NCRE = !string.IsNullOrEmpty(frmNCRE) ? Convert.ToInt32(frmNCRE) :2;//2
            //int frm_OPA = !string.IsNullOrEmpty(frmOPA) ? Convert.ToInt32(frmOPA) :3;//3
            //int frm_FHA_REQUEST = !string.IsNullOrEmpty(frmFHA_REQUEST) ? Convert.ToInt32(frmFHA_REQUEST) : 4;//4
            //int frm_APPLICATION_REQUEST = !string.IsNullOrEmpty(frmAPPLICATION_REQUEST) ? Convert.ToInt32(frmAPPLICATION_REQUEST) : 5;//5
            //int frm_CONSTRUCTION_SINGLE_STAGE = !string.IsNullOrEmpty(frmCONSTRUCTION_SINGLE_STAGE) ? Convert.ToInt32(frmCONSTRUCTION_SINGLE_STAGE) : 6;//6
            //int frm_CONSTRUCTION_TWO_STAGE_INITIAL = !string.IsNullOrEmpty(frmCONSTRUCTION_TWO_STAGE_INITIAL) ? Convert.ToInt32(frmCONSTRUCTION_TWO_STAGE_INITIAL) : 7;//7
            //int frm_CONSTRUCTION_TWO_STAGE_FINAL = !string.IsNullOrEmpty(frmCONSTRUCTION_TWO_STAGE_FINAL) ? Convert.ToInt32(frmCONSTRUCTION_TWO_STAGE_FINAL) : 8;//8
            //int frm_CONSTRUCTION_MANAGEMENT = !string.IsNullOrEmpty(frmCONSTRUCTION_MANAGEMENT) ? Convert.ToInt32(frmCONSTRUCTION_MANAGEMENT) : 9;//9
            //int frm_NON_CONSTRUCTION_DRAFT_CLOSING = !string.IsNullOrEmpty(frmNON_CONSTRUCTION_DRAFT_CLOSING) ? Convert.ToInt32(frmNON_CONSTRUCTION_DRAFT_CLOSING) : 10;//10
            //int frm_CLOSING_TWO_STAGE_INITIAL = !string.IsNullOrEmpty(frmCLOSING_TWO_STAGE_INITIAL) ? Convert.ToInt32(frmCLOSING_TWO_STAGE_INITIAL) : 11;//11
            //int frm_CLOSING_TWO_STAGE_FINAL = !string.IsNullOrEmpty(frmCLOSING_TWO_STAGE_FINAL) ? Convert.ToInt32(frmCLOSING_TWO_STAGE_FINAL) : 12;//12
            //int frm_FORM_290 = !string.IsNullOrEmpty(frmFORM_290) ? Convert.ToInt32(frmFORM_290) : 16;//16
            //int frm_NON_CONSTRUCTION_EXECUTED_CLOSING = !string.IsNullOrEmpty(frmNON_CONSTRUCTION_EXECUTED_CLOSING) ? Convert.ToInt32(frmNON_CONSTRUCTION_EXECUTED_CLOSING) : 17;//17
            bool isForm290 = false;
            var EmailToLendertModel = new Prod_EmailToLenderModel();
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            EmailToLendertModel.SendToEmail = task.AssignedBy;
            var fhaRequest = fhaRequestManager.GetFhaRequestByFhaNumber(task.FHANumber);
            var emailTypes = fhaRequest.ProjectTypeId == (int)ProdProjectTypes.IRR ? lookupMgr.GetAllEmailTypes().Where
                                                                                 (t1 => t1.EmailTypeId == (Int32)EmailType.FIRMAP ||
                                                            t1.EmailTypeId == (Int32)EmailType.SSTGS || t1.EmailTypeId == (Int32)EmailType.IRRAP) :
                                                            lookupMgr.GetAllEmailTypes().Where
                                                                                 (t1 => t1.EmailTypeId == (Int32)EmailType.FIRMAP ||
                                                            t1.EmailTypeId == (Int32)EmailType.SSTGS);
            var productionUser = (from prodUsers in productionQueue.GetProductionUsers()
                                  select new
                                  {
                                      prodUsers.UserName,
                                      Name = String.Format("{0} {1} {2} {3} {4}", prodUsers.FirstName, prodUsers.LastName, "( ", prodUsers.RoleName, " )")

                                  }).OrderBy(o => o.Name);
            const int a = 1;
            switch (task.PageTypeId)
            {
                case 5:
                    var Email_Type_5 = (from emailType in emailTypes
                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_5, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 6:
                    var Email_Type_6 = (from emailType in lookupMgr.GetAllEmailTypes().Where
                                 (t1 => t1.EmailTypeId == (Int32)EmailType.FIRMAP ||
                                      t1.EmailTypeId == (Int32)EmailType.SSTGS)

                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_6, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 7:
                    var Email_Type_7 = (from emailType in lookupMgr.GetAllEmailTypes().Where
                                  (t1 => t1.EmailTypeId == (Int32)EmailType.FIRMAP ||
                                      t1.EmailTypeId == (Int32)EmailType.INISUB)

                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_7, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 8:
                    var Email_Type_8 = (from emailType in lookupMgr.GetAllEmailTypes()

                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_8, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 9:
                    var Email_Type_9 = (from emailType in lookupMgr.GetAllEmailTypes().Where
                               (t1 => t1.EmailTypeId == (Int32)EmailType.FRMCMMTMNT ||
                                  t1.EmailTypeId == (Int32)EmailType.ERSTRAPP ||
                                    t1.EmailTypeId == (Int32)EmailType.INCLDATE)

                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_9, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 10:
                    var emialtype10 = (from emailType in lookupMgr.GetAllEmailTypes().Where(t1 => t1.EmailTypeId == (Int32)EmailType.IRRCLSG)
                                       select new
                                       {
                                           emailType.EmailTypeId,
                                           emailType.EmailTypeCd,
                                           emailType.EmailTypeDescription
                                       }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(emialtype10, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 16: //skumar-frm260
                    var emailType16 = (from emailType in lookupMgr.GetAllEmailTypes().Where(t1 => t1.EmailTypeId == (Int32)EmailType.FRM290CLOS)
                                       select new
                                       {
                                           emailType.EmailTypeId,
                                           emailType.EmailTypeCd,
                                           emailType.EmailTypeDescription
                                       }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(emailType16, "EmailTypeCd", "EmailTypeDescription");
                    isForm290 = true;
                    break;
                case 18:
                    var emailType18 = (from emailType in lookupMgr.GetAllEmailTypes().Where(t1 => t1.EmailTypeId == (Int32)EmailType.WLMAMEND1)
                                       select new
                                       {
                                           emailType.EmailTypeId,
                                           emailType.EmailTypeCd,
                                           emailType.EmailTypeDescription
                                       }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(emailType18, "EmailTypeCd", "EmailTypeDescription");
                    break;
            }
            EmailToLendertModel.CCToEmailList = new SelectList(productionUser, "UserName", "Name");
            EmailToLendertModel.TaskInstanceID = taskInstanceId;
            return View("~/Views/Production/ApplicationRequest/popup/SendEmailToLender.cshtml", EmailToLendertModel);
        }
        [HttpPost]
        public JsonResult EmailToLender(Guid taskInstanceID, string ToEmail, string CCEmails, string template)
        {
            List<string> Emaillist = new List<string>();
            string result = string.Empty;
            if (!string.IsNullOrEmpty(CCEmails))
            {
                Emaillist = (new List<string>(CCEmails.Split(',')));
            }
            if (template.Equals(EmailType.FRM290CLOS.ToString()))
            {
                string accountExecutive = string.Empty;
                var form290Task = productionQueue.GetForm290TaskBySingleTaskInstanceID(taskInstanceID);
                if (form290Task != null && form290Task.ClosingTaskInstanceID != null)
                {
                    accountExecutive = sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(form290Task.ClosingTaskInstanceID);
                }
                else
                {
                    accountExecutive = sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(taskInstanceID);
                }

                var Model = new Prod_EmailToLenderModel()
                {
                    CCEmails = Emaillist,
                    SendToEmail = ToEmail,
                    EmailTemplateId = template,
                    TaskInstanceID = taskInstanceID,
                    AccountExecutive = accountExecutive,
                };
                result = backgroundJobManager.SendLenderNotificationsEmail(new EmailManager(), Model);


                if (result == "true")
                {
                    var form290model = new TaskModel()
                    {
                        TaskInstanceId = taskInstanceID,
                        SequenceId = 1,
                        FHANumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceID).FHANumber,
                        TaskStepId = (int)TaskStep.Form290Complete,
                        PageTypeId = (int)PageType.Form290,
                        AssignedBy = UserPrincipal.Current.UserName,
                        AssignedTo = UserPrincipal.Current.UserName,
                        //StartTime = DateTime.Now
                        StartTime = DateTime.UtcNow
                    };
                    productionQueue.AddTask(form290model);

                }

            }
            else
            {
                if (template.Equals(EmailType.WLMAMEND2.ToString()))
                {
                    if (Emaillist.Count > 0)
                        ToEmail = Emaillist.FirstOrDefault().ToString();
                }
                var Model = new Prod_EmailToLenderModel()
                {
                    CCEmails = Emaillist,
                    SendToEmail = ToEmail,
                    EmailTemplateId = template,
                    TaskInstanceID = taskInstanceID,
                    AccountExecutive = string.Empty,//accountExecutive,//sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(taskInstanceID),
                };
                result = backgroundJobManager.SendLenderNotificationsEmail(new EmailManager(), Model);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskInstanceID"></param>
        /// <param name="ToEmail"></param>
        /// <param name="CCEmails"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Form290EmailToLender(Guid pTaskInstanceID, string pToEmail, string pCCEmails, string pTemplate)
        {
            List<string> emailList = new List<string>();
            string accountExecutive = string.Empty;
            if (!string.IsNullOrEmpty(pCCEmails))
            {
                emailList = (new List<string>(pCCEmails.Split(',')));
            }

            //skumar-form290
            var form290Task = productionQueue.GetForm290TaskBySingleTaskInstanceID(pTaskInstanceID);
            if (form290Task != null && form290Task.ClosingTaskInstanceID != null)
            {
                accountExecutive = sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(form290Task.ClosingTaskInstanceID);
            }
            else
            {
                accountExecutive = sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(pTaskInstanceID);
            }

            var Model = new Prod_EmailToLenderModel()
            {
                CCEmails = emailList,
                SendToEmail = pToEmail,
                EmailTemplateId = pTemplate,
                TaskInstanceID = pTaskInstanceID,
                AccountExecutive = accountExecutive,
            };
            string result = backgroundJobManager.SendLenderNotificationsEmail(new EmailManager(), Model);


            if (result == "true")
            {
                var form290model = new TaskModel()
                {
                    TaskInstanceId = pTaskInstanceID,
                    SequenceId = 1,
                    FHANumber = taskManager.GetLatestTaskByTaskInstanceId(pTaskInstanceID).FHANumber,
                    TaskStepId = (int)TaskStep.Form290Complete,
                    PageTypeId = (int)PageType.Form290,
                    AssignedBy = UserPrincipal.Current.UserName,
                    AssignedTo = UserPrincipal.Current.UserName,
                    //StartTime = DateTime.Now
                    StartTime = DateTime.UtcNow
                };
                productionQueue.AddTask(form290model);

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Form290SendEmailToLender()
        {
            return RedirectToAction("Index", "ProductionMyTask");
        }

        public ActionResult SendEmailToProdUser(Guid taskInstanceId)
        {
            var emailToProdUser = new Prod_EmailToLenderModel();
            //var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            //emailToProdUser.SendToEmail = task.AssignedBy;
            //var fhaRequest = fhaRequestManager.GetFhaRequestByFhaNumber(task.FHANumber);
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);

            var ProdemailType = (from emailType in lookupMgr.GetAllEmailTypes().Where(t1 => t1.EmailTypeId == (Int32)EmailType.IRRLMAR || t1.EmailTypeId == (Int32)EmailType.FRM290CLOS)
                                 select new
                                 {
                                     emailType.EmailTypeId,
                                     emailType.EmailTypeCd,
                                     emailType.EmailTypeDescription
                                 }).OrderBy(o => o.EmailTypeDescription);


            var productionUser = (from prodUsers in productionQueue.GetProductionUsers()
                                  select new
                                  {
                                      prodUsers.UserName,
                                      Name = String.Format("{0} {1} {2} {3} {4}", prodUsers.FirstName, prodUsers.LastName, "( ", prodUsers.RoleName, " )")

                                  }).OrderBy(o => o.Name);
            emailToProdUser.Emailtemplate = new SelectList(ProdemailType, "EmailTypeCd", "EmailTypeDescription");
            emailToProdUser.CCToEmailList = new SelectList(productionUser, "UserName", "Name");
            emailToProdUser.OtherEmails = task.AssignedBy;
            emailToProdUser.TaskInstanceID = taskInstanceId;
            return View("~/Views/Production/ApplicationRequest/popup/SendEmailToProdUsers.cshtml", emailToProdUser);
        }

        /// <summary>
        /// Email to WLM users only
        /// </summary>
        /// <param name="taskInstanceId"></param>
        /// <returns></returns>
        public ActionResult SendEmailToProdWLMUser(Guid taskInstanceId)
        {
            var emailToProdUser = new Prod_EmailToLenderModel();
            var ProdemailType = (from emailType in lookupMgr.GetAllEmailTypes().Where(t1 => t1.EmailTypeId == (Int32)EmailType.WLMAMEND)
                                 select new
                                 {
                                     emailType.EmailTypeId,
                                     emailType.EmailTypeCd,
                                     emailType.EmailTypeDescription
                                 }).OrderBy(o => o.EmailTypeDescription);


            var productionUser = (from prodUsers in productionQueue.GetProductionWLMUsers()
                                  select new
                                  {
                                      prodUsers.UserName,
                                      Name = String.Format("{0} {1} {2} {3} {4}", prodUsers.FirstName, prodUsers.LastName, "( ", prodUsers.RoleName, " )")

                                  }).OrderBy(o => o.Name);
            emailToProdUser.Emailtemplate = new SelectList(ProdemailType, "EmailTypeCd", "EmailTypeDescription");
            emailToProdUser.CCToEmailList = new SelectList(productionUser, "UserName", "Name");

            emailToProdUser.TaskInstanceID = taskInstanceId;
            return View("~/Views/Production/ApplicationRequest/popup/SendEmailToProdWLMUsers.cshtml", emailToProdUser);
        }

        [HttpPost]
        public JsonResult EmailToProdWLMUsers(Guid taskInstanceID, string ToEmail, string CCEmails, string template)
        {
            List<string> Emaillist = new List<string>();
            string result = string.Empty;
            var toEmailList = new List<string>();
            if (!string.IsNullOrEmpty(ToEmail))
            {
                toEmailList = (new List<string>(ToEmail.Split(',')));
            }
            if (!string.IsNullOrEmpty(CCEmails))
            {
                Emaillist = (new List<string>(CCEmails.Split(',')));
            }

            var Model = new prod_EmailToProdUser()
            {
                CCEmails = Emaillist,
                ToProdUsers = toEmailList,
                EmailTemplateId = template,
                TaskInstanceID = taskInstanceID,
            };

            if (template.Equals(EmailType.WLMAMEND.ToString()))
            {
                Model.SendToEmail = ToEmail;
                result = backgroundJobManager.SendLenderNotificationsEmail(new EmailManager(), Model);

            }
            else
            {
                result = backgroundJobManager.SendEmailToProdUsers(new EmailManager(), Model);
            }
            //update taskxref with wlm id
            var d = accountManager.GetUserByUsername(ToEmail);
            prod_TaskXrefManager.UpdateTaskXrefForAmendments(taskInstanceID, d.UserID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EmailToProdUsers(Guid taskInstanceID, string ToEmail, string CCEmails, string template)
        {
            List<string> Emaillist = new List<string>();
            var toEmailList = new List<string>();
            string result = string.Empty;

            if (!string.IsNullOrEmpty(ToEmail))
            {
                toEmailList = (new List<string>(ToEmail.Split(',')));
            }
            if (!string.IsNullOrEmpty(CCEmails))
            {
                Emaillist = (new List<string>(CCEmails.Split(',')));
            }
            if (template.Equals(EmailType.FRM290CLOS.ToString()))
            {
                string accountExecutive = string.Empty;
                var form290Task = productionQueue.GetForm290TaskBySingleTaskInstanceID(taskInstanceID);
                if (form290Task != null && form290Task.ClosingTaskInstanceID != null)
                {
                    accountExecutive = sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(form290Task.ClosingTaskInstanceID);
                }
                else
                {
                    accountExecutive = sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(taskInstanceID);
                }

                var Model = new Prod_EmailToLenderModel()
                {
                    CCEmails = Emaillist,
                    SendToEmail = ToEmail,
                    EmailTemplateId = template,
                    TaskInstanceID = taskInstanceID,
                    AccountExecutive = accountExecutive,
                };
                result = backgroundJobManager.SendLenderNotificationsEmail(new EmailManager(), Model);

                if (result == "true")
                {
                    var form290model = new TaskModel()
                    {
                        TaskInstanceId = taskInstanceID,
                        SequenceId = 1,
                        FHANumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceID).FHANumber,
                        TaskStepId = (int)TaskStep.Form290Complete,
                        PageTypeId = (int)PageType.Form290,
                        AssignedBy = UserPrincipal.Current.UserName,
                        AssignedTo = UserPrincipal.Current.UserName,
                        //StartTime = DateTime.Now
                        StartTime = DateTime.UtcNow
                    };
                    productionQueue.AddTask(form290model);
                }
            }
            else
            {
                var Model = new prod_EmailToProdUser()
                {
                    CCEmails = Emaillist,
                    ToProdUsers = toEmailList,
                    EmailTemplateId = template,
                    TaskInstanceID = taskInstanceID,
                };
                result = backgroundJobManager.SendEmailToProdUsers(new EmailManager(), Model);
            }



            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public ActionResult ExternalReviewerAssignment(Guid taskInstanceId)
        {
            var taskAssignmentModel = new ProductionTaskAssignmentModel { TaskInstanceId = taskInstanceId };
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            if (task != null)
            {
                taskAssignmentModel.TaskId = task.TaskId;
            }
            var curentUser = UserPrincipal.Current.UserName;
            taskAssignmentModel.isWLM = RoleManager.IsProductionWLM(curentUser);
            var productionUser = (from prodUsers in productionQueue.GetUnAssignedExternalReviewers()
                                  select new
                                  {
                                      prodUsers.UserID,
                                      Name = String.Format("{0} {1} {2} {3} {4}", prodUsers.FirstName, prodUsers.LastName, "( ", prodUsers.RoleName, " )")

                                  });


            var AvailableViews = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(2, taskInstanceId)
                                  select new
                                  {
                                      Views.ViewId,
                                      Name = Views.ViewName

                                  });
            //if (!taskAssignmentModel.isWLM)
            //{
            //    productionUser = productionUser.ToList().Where(a => a.UserID == UserPrincipal.Current.UserId);
            //}

            ViewBag.AssignedToUserId = new SelectList(productionUser, "UserID", "Name");
            ViewBag.AssignedToViewId = new SelectList(AvailableViews, "ViewId", "Name");

            return View("~/Views/Production/ApplicationRequest/popup/ExternalReviewerTaskAsignment.cshtml", taskAssignmentModel);
        }

        public ActionResult AssignOPAToExternalReviewerAssignment(Guid taskInstanceId)
        {
            var taskAssignmentModel = new ProductionTaskAssignmentModel { TaskInstanceId = taskInstanceId };
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            if (task != null)
            {
                taskAssignmentModel.TaskId = task.TaskId;
            }
            var curentUser = UserPrincipal.Current.UserName;
            taskAssignmentModel.isWLM = RoleManager.IsProductionWLM(curentUser); //
            var IsoUsers = (from isoUsers in productionQueue.GetALLInternalSpecialOptionUsers()
                            select new
                            {
                                isoUsers.UserID,
                                Name = String.Format("{0} {1} {2} {3} {4}", isoUsers.FirstName, isoUsers.LastName, "( ", isoUsers.RoleName, " )")

                            });


            //var AvailableViews = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(2, taskInstanceId)
            //                      select new
            //                      {
            //                          Views.ViewId,
            //                          Name = Views.ViewName

            //                      });

            ViewBag.AssignedToUserId = new SelectList(IsoUsers, "UserID", "Name");
            //ViewBag.AssignedToViewId = new SelectList(AvailableViews, "ViewId", "Name");

            return View("~/Views/AssetManagement/ExternalReviewerPopup.cshtml", taskAssignmentModel);
        }

        [HttpPost]
        public void ExternalTaskAsignment(ProductionTaskAssignmentModel model)
        {

            model.IsReviewer = false;
            model.AssignedBy = UserPrincipal.Current.UserId;
            model.Status = (int)ProductionAppProcessStatus.InProcess;
            model.ViewId = model.AssignedToViewId;
            model.ModifiedOn = DateTime.UtcNow;
            model.ModifiedBy = UserPrincipal.Current.UserId;
            model.AssignedTo = model.AssignedToUserId;
            model.TaskXrefid = Guid.NewGuid();
            prod_TaskXrefManager.AddTaskXref(model);

            //Logic to Separate the UnderWriter
            if (model.AssignedToViewId == 1)
            {
                var task = taskManager.GetLatestTaskByTaskInstanceId(model.TaskInstanceId);
                if (task != null)
                {

                    task.AssignedTo = productionQueue.GetUserById(model.AssignedToUserId).UserName;
                    task.Notes = model.Comments;
                    taskManager.UpdateTaskAssignment(task);
                }
            }

            taskManager.SaveReviewFileStatus(model.TaskInstanceId, model.AssignedToUserId, UserPrincipal.Current.UserId, model.AssignedToViewId);


        }


        [HttpPost]
        public ActionResult SaveComments(ProductionTaskAssignmentModel model)
        {
            var OPamodel = RegenrateModel(model.TaskXrefid);
            return ApproveOrDenyApplicationRequest(OPamodel);
            // OPamodel
            //return RedirectToAction("ApproveOrDenyApplicationRequest", new { model = OPamodel });
        }

        [HttpPost]
        public void SaveFilestoFolder(FileReorderModel model)
        {
            var list = model.FileIdlist.Split(',');

            foreach (string item in list)
            {
                //var DocTypeList = appProcessManager.GetMappedDocTypesbyFolder(Convert.ToInt32(model.AvailableFolders));

                var mappingmodel = new TaskFile_FolderMappingModel();
                mappingmodel.FolderKey = model.AvailableFolders;
                mappingmodel.TaskFileId = new Guid(item);
                mappingmodel.CreatedOn = DateTime.UtcNow;
                taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                var FolderInfo = projectActionFormManager.GetFolderInfoByKey(mappingmodel.FolderKey);
                var taskFileInfo = taskManager.GetTaskFileByTaskFileId(new Guid(item));

                if (model.pageTypeId == 18)
                {
                    taskFileInfo.DocTypeID = ConfigurationManager.AppSettings["DefaultDocId"];
                    string[] tokens = taskFileInfo.FileName.Split('!');
                    taskFileInfo.FileName = FolderInfo[0].FolderName + "_A ! " + tokens[1];
                }
                else
                {
                    if (taskFileInfo != null && model.AvailableFolders != model.FromFolder)
                    {
                        var DocType = new DocumentTypeModel();

                        DocType = prod_DocumentType.GetDocumentTypeId(taskFileInfo.FileName, model.AvailableFolders);
                        if (DocType.DocumentTypeId > 0)
                        {
                            taskFileInfo.DocTypeID = DocType.DocumentTypeId.ToString();
                        }
                        else
                        {
                            taskFileInfo.DocTypeID = null;
                        }
                    }
                }
                taskManager.UpdateDocType(taskFileInfo);


            }


        }
        [HttpPost]
        public void CopyFileToDifferentFolder(FileReorderModel model)
        {
            var list = model.FileIdlist.Split(',');
            var PageName = projectActionFormManager.FormNameBYPageTypeID(model.pageTypeId);

            foreach (string item in list)
            {
                var taskFileInfo = taskManager.GetTaskFileById(Convert.ToInt32(item));
                if (taskFileInfo != null && model.AvailableFolders != model.FromFolder)
                {
                    var formUploadData = ReloadApplication(taskFileInfo.TaskInstanceId);
                    var request = WebApiTokenRequest.RequestToken();
                    var JsonSteing = "{\"docId\":" + taskFileInfo.DocId + ",\"version\":" + taskFileInfo.Version + "}";
                    var byteResult = webApiDownload.DownloadByteFile(JsonSteing, request.access_token, taskFileInfo.FileName);
                    var FolderInfo = projectActionFormManager.GetFolderInfoByKey(model.AvailableFolders);
                    string folderName = "Production/" + PageName + "/" + FolderInfo[0].FolderName;
                    if (model.pageTypeId == 18)
                    {
                        //taskFileInfo.DocTypeID = ConfigurationManager.AppSettings["DefaultDocId"];
                        string[] tokens = taskFileInfo.FileName.Split('!');
                        taskFileInfo.FileName = FolderInfo[0].FolderName + "_A ! " + tokens[1];
                    }
                    var uploadmodel = new RestfulWebApiUploadModel()
                    {
                        propertyID = formUploadData.PropertyId.ToString(),
                        indexType = "1",
                        indexValue = formUploadData.FhaNumber,
                        documentType = ConfigurationManager.AppSettings["DefaultDocId"].ToString(),
                        pdfConvertableValue = "false",
                        folderNames = folderName,
                        folderKey = model.AvailableFolders

                    };

                    var WebApiUploadResult = new RestfulWebApiResultModel();
                    WebApiUploadResult = WebApiDocumentUpload.uploadCopiedFile(uploadmodel, request.access_token, byteResult, taskFileInfo.FileName);

                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {
                        Random randoms = new Random();
                        var UniqueId = randoms.Next(0, 99999);

                        var systemFileName = formUploadData.FhaNumber + "_" +
                                             formUploadData.ProjectActionTypeId + "_" +
                                             formUploadData.GroupTaskInstanceId + "_" + UniqueId +
                                             Path.GetExtension(taskFileInfo.FileName);
                        taskFileInfo.GroupFileType = UniqueId.ToString();
                        taskFileInfo.SystemFileName = systemFileName;
                        taskFileInfo.DocId = WebApiUploadResult.docId;
                        taskFileInfo.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFileInfo.API_upload_status = WebApiUploadResult.status;
                        if (model.pageTypeId == 18)
                        {
                            taskFileInfo.DocTypeID = ConfigurationManager.AppSettings["DefaultDocId"];
                        }
                        else
                        {
                            taskFileInfo.DocTypeID = null;
                        }


                        var taskfileid = taskManager.SaveGroupTaskFileModel(taskFileInfo);
                        var mappingmodel = new TaskFile_FolderMappingModel();
                        mappingmodel.FolderKey = model.AvailableFolders;
                        mappingmodel.TaskFileId = taskfileid;
                        mappingmodel.CreatedOn = DateTime.UtcNow;
                        taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                        var newTaskFileInfo = taskManager.GetTaskFileByTaskFileId(taskfileid);
                        var DocType = new DocumentTypeModel();
                        DocType = prod_DocumentType.GetDocumentTypeId(newTaskFileInfo.FileName, model.AvailableFolders);
                        if (model.pageTypeId == 18)
                        {
                            newTaskFileInfo.DocTypeID = ConfigurationManager.AppSettings["DefaultDocId"];
                        }
                        else
                        {
                            if (DocType.DocumentTypeId > 0)
                            {
                                newTaskFileInfo.DocTypeID = DocType.DocumentTypeId.ToString();
                            }
                            else
                            {
                                newTaskFileInfo.DocTypeID = null;
                            }
                        }

                        taskManager.UpdateDocType(newTaskFileInfo);
                    }


                }
            }

        }


        public JsonResult GetProdReviewersTaskStatus(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            // List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();

            var GridContent = appProcessManager.GetProdReviewersTaskStatus(taskInstanceId);

            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.CompletedOn = TimezoneManager.ToMyTimeFromUtc(item.CompletedOn);
                    //item.CreatedOn = TimezoneManager.ToMyTimeFromUtc(item.CreatedOn);
                    item.CompletedOn = item.CompletedOn;
                    item.CreatedOn = item.CreatedOn;
                }

            }

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);



            //var results = GridContent.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = GridContent,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetProdPendingRAI(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            // List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();

            var GridContent = appProcessManager.GetProdPendingRAI(taskInstanceId);

            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.CreatedOn = TimezoneManager.ToMyTimeFromUtc(item.CreatedOn);
                    item.CreatedOn = item.CreatedOn;

                }

            }
            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);



            //var results = GridContent.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = GridContent,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }


        //public JsonResult GetProdOpaHistoryFolderList(string sidx, string sord, int page, int rows, Guid taskInstanceId,string  pViewId=null)

        public JsonResult GetProdOpaHistoryFolderList(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var GridContent = appProcessManager.GetFolderListbyTaskInstanceId(taskInstanceId);

            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    if (!string.IsNullOrEmpty(item.SubfolderSequence))
                    {
                        item.FolderName = item.FolderName + "_" + item.SubfolderSequence.Trim();
                    }
                }
            }

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = GridContent,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetProdOpaHistoryFileList(string sidx, string sord, int page, int rows, Guid taskInstanceId, int folderKey, int viewId)
        public JsonResult GetProdOpaHistoryFileList(string sidx, string sord, int page, int rows, Guid taskInstanceId, int folderKey, int viewId)
        {
            int userId = UserPrincipal.Current.UserId;
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var GridContent = projectActionFormManager.GetProdOpaHistoryFileList(taskInstanceId, userId, viewId, folderKey);
            //to delete duplicate records -harish and Siddesh
            GridContent = GridContent.GroupBy(o => new { o.fileId }).Select(o => o.FirstOrDefault()).ToList();
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                }

            }

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);



            // var results = GridContent.Skip(pageIndex * pageSize).Take(pageSize);

            string[] fileRenameArray;
            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    if (!string.IsNullOrEmpty(item.fileRename))
                    {
                        fileRenameArray = new String[] { };
                        fileRenameArray = item.fileRename.Split('_');
                        if (fileRenameArray.Length == 1)
                        {
                            //11302017 - Bug 716 - IndexOutOfRangeException
                            item.fileRename = fileRenameArray[0];//fileRenameArray[1]
                        }
                        else
                        {
                            item.fileRename = "";
                            for (int i = 1; i < fileRenameArray.Length; i++)
                            {
                                item.fileRename += fileRenameArray[i];
                                if (i != fileRenameArray.Length - 1)
                                {
                                    item.fileRename += "_";
                                }
                            }
                        }
                    }

                }

            }

            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = GridContent,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetFilesForSelectedFolder(string FoldeKeys, string TaskInstanceid)
        {
            Guid Instanceid = new Guid(TaskInstanceid);
            var keyarry = FoldeKeys.Split(',');
            var keyintarr = Array.ConvertAll(keyarry, p => Convert.ToInt32(p));
            var result = appProcessManager.GetMappedFilesbyFolder(keyintarr, Instanceid);
            List<SelectListItem> temp = new List<SelectListItem>();
            var multilist = new MultiSelectList(result.ToList().Select(p => new KeyValuePair<int, string>(p.FileId, String.Format("{0}( {1} )", p.FileName, p.DocId))).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");
            temp = multilist.ToList();

            return Json(new { temp }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFilesToChangeDocTypeForSelectedFolder(string FoldeKeys, string TaskInstanceid)
        {
            Guid Instanceid = new Guid(TaskInstanceid);
            var keyarry = FoldeKeys.Split(',');
            var keyintarr = Array.ConvertAll(keyarry, p => Convert.ToInt32(p));
            var result = appProcessManager.GetMappedFilesbyFolder(keyintarr, Instanceid).Where(m => string.IsNullOrEmpty(m.DocTypeID));
            List<SelectListItem> temp = new List<SelectListItem>();
            var multilist = new MultiSelectList(result.ToList().Select(p => new KeyValuePair<int, string>(p.FileId, String.Format("{0}", p.FileName))).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");
            temp = multilist.ToList();

            return Json(new { temp }, JsonRequestBehavior.AllowGet);
        }

        private string GetDocTypesdropdownlist(IEnumerable<DocumentTypeModel> DocTypeModel, int? fileid)
        {

            StringBuilder sb = new StringBuilder();
            string defaultDocTypeId = ConfigurationManager.AppSettings["DefaultDocId"].ToString();
            sb.AppendLine("<select id='" + fileid.ToString() + "_ddlDocType'");
            sb.AppendLine("onchange='ChangrDocTypeConfirmation(fileId =" + fileid.ToString() + ")'  style='width: 200px;'>");
            sb.AppendLine("<option value='0'>Select New File Name</option>");

            foreach (DocumentTypeModel doctype in DocTypeModel)
            {
                string DocName = "";
                if (!string.IsNullOrEmpty(doctype.ShortDocTypeName))
                {
                    DocName = doctype.ShortDocTypeName + "_" + doctype.DocumentType;
                }
                else
                {
                    DocName = doctype.DocumentType;
                }
                sb.AppendLine("<option value='" + doctype.DocumentTypeId.ToString() + "' id='" + doctype.DocumentTypeId.ToString() + "'>" + DocName + "</option>");
            }
            sb.AppendLine("<option value='" + defaultDocTypeId + "'>Others</option></select> ");

            return sb.ToString();
        }
        public JsonResult GetProdOpaHistoryReviewersList(string sidx, string sord, int page, int rows, int fileId, int viewId)
        {
            int userId = UserPrincipal.Current.UserId;

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();
            List<OPAHistoryViewModel> GridContentTemp = new List<OPAHistoryViewModel>();

            GridContent = projectActionFormManager.GetProdOpaHistoryReviewersList(userId, viewId, fileId);

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);

            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate =item.uploadDate;
                    item.submitDate = item.submitDate;
                }

            }

            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = GridContent,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public void updateDoumentType(string taskFiles, int docTypeId)
        {
            if (taskFiles != null)
            {
                var fileIds = taskFiles.Split(',');
                var tokenResult = WebApiTokenRequest.RequestToken();
                foreach (var fileId in fileIds)
                {
                    var taskFileInfo = taskManager.GetTaskFileById(Int32.Parse(fileId));
                    if (taskFileInfo != null && taskFileInfo.DocId != null)
                    {
                        var docInfo = new RestfulWebApiUpdateModel()
                        {
                            docId = taskFileInfo.DocId,
                            documentType = docTypeId.ToString()
                        };
                        var result = prodRestfulWebApiUpdate.UpdateDocument(docInfo, tokenResult.access_token);
                        if (result != null && result.status.ToLower() == "success")
                        {
                            taskFileInfo.DocTypeID = docTypeId.ToString();
                            taskManager.UpdateDocType(taskFileInfo);
                        }
                    }




                }
            }

        }
        // [HttpGet]

        //public ActionResult ApplicationRequestRAIHUDandReadOnly(Guid taskinstanceId,Guid grouptaskinstanceId, string comment)
        //{
        //    OPAViewModel model = (OPAViewModel)TempData["AppProcessModel"];
        //    if (model == null)
        //    {
        //        model = ReloadApplication(grouptaskinstanceId);

        //    }
        // if (!model.IsChangeDocTypePopupDisplayed)
        // {

        //     var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(taskinstanceId)
        //                     select new
        //                     {
        //                         filelist.FileId,
        //                         filelist.DocTypeID,
        //                         docId = filelist.DocId,
        //                         Name = filelist.FileName
        //                     }).Where(m => string.IsNullOrEmpty(m.DocTypeID));


        //     if (FileList.Count() > 0)
        //     {
        //         model.IsChangeDocTypePopupDisplayed = true;

        //     }
        //     else
        //     {

        //         return (LenderChildSubmit(model, model.TaskGuid, model.GroupTaskInstanceId, model.SequenceId, model.Concurrency));

        //     }
        // }

        // return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml", model);
        //}
        public ActionResult LenderApplicationRequest(Guid taskinstanceId)
        {
            OPAViewModel model = (OPAViewModel)TempData["AppProcessModel"];
            if (model == null)
            {
                model = ReloadApplication(taskinstanceId);

            }
            if (!model.IsChangeDocTypePopupDisplayed)
            {

                var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(taskinstanceId)
                                select new
                                {
                                    filelist.FileId,
                                    filelist.DocTypeID,
                                    docId = filelist.DocId,
                                    Name = filelist.FileName
                                }).Where(m => string.IsNullOrEmpty(m.DocTypeID));


                if (FileList.Count() > 0)
                {
                    model.IsChangeDocTypePopupDisplayed = true;
                }
                else
                {

                    return (Submit(model));

                }
            }

            return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", model);
        }
        // public ActionResult changeDoumentType(string fileId, int docTypeId, Guid taskinstanceId, Guid grouptaskinstanceId, string redirectFrom)
        public ActionResult changeDoumentType(string fileId, string docType, string DocName, Guid taskinstanceId, string referrer)
        {




            //if (fileId != null)
            //{

            var tokenResult = WebApiTokenRequest.RequestToken();

            var taskFileInfo = taskManager.GetTaskFileById(Int32.Parse(fileId));
            var task = taskManager.GetTasksByTaskInstanceId(taskFileInfo.TaskInstanceId).FirstOrDefault();
            var formUploadData = ReloadApplication(taskFileInfo.TaskInstanceId);

            if (taskFileInfo != null)
            {
                if (taskFileInfo.DocId != null)
                {
                    var docInfo = new RestfulWebApiUpdateModel()
                    {
                        docId = taskFileInfo.DocId,
                        documentType = docType.ToString(),
                        propertyId = formUploadData.PropertyId.ToString(),
                        indexType = "1",
                        indexValue = formUploadData.FhaNumber,
                        documentStatus = "Active"

                    };
                    var result = prodRestfulWebApiUpdate.UpdateDocument(docInfo, tokenResult.access_token);
                    if (result != null && result.status.ToLower() == "success")
                    {
                        taskFileInfo.DocTypeID = docType.ToString();
                        string ext = Path.GetExtension(taskFileInfo.FileName);
                        if (!string.IsNullOrEmpty(DocName))
                        {
                            taskFileInfo.FileName = DocName + ext;
                        }

                        taskManager.UpdateDocType(taskFileInfo);

                    }
                }
                else
                {
                    var docInfo = new RestfulWebApiUpdateModel()
                    {
                        documentType = docType.ToString()
                    };

                    taskFileInfo.DocTypeID = docType.ToString();
                    taskManager.UpdateDocType(taskFileInfo);

                }

            }
            if (referrer == "RAI")
            {
                return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml", formUploadData);
            }
            return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", formUploadData);
        }
        private OPAViewModel RegenrateModel(Guid taskInstanceId)
        {
            var formUploadData = new OPAViewModel();
            var isChildTask = false;
            var task = new TaskModel();

            formUploadData.isChildTaskOpen = false;

            if (parentChildTaskManager.IsParentTaskAvailable(taskInstanceId))
            {
                task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);

                formUploadData = projectActionFormManager.GetProdOPAByChildTaskId(task.TaskInstanceId);
                formUploadData.viewId = 0;
                formUploadData.ChildTaskInstanceId = task.TaskInstanceId;
                isChildTask = true;
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    formUploadData.IsLoggedInUserLender = false;
                }
            }
            else
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    task = taskManager.GetLatestTaskByTaskXrefid(taskInstanceId);
                    formUploadData = projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);

                    //logic to check is an child open task  and for confirmation popup flag
                    formUploadData.isChildTaskOpen = false;
                    List<TaskModel> childTaskList = projectActionFormManager.GetProdChildTasksByXrefId(task.TaskInstanceId, false);
                    if (childTaskList != null && childTaskList.Count > 0)
                    {

                        foreach (var childList in childTaskList)
                        {

                            if (childList.TaskStepId == (int)TaskStep.OPAAddtionalInformation)
                            {
                                formUploadData.isChildTaskOpen = true;
                                break;
                            }

                        }
                    }
                    Prod_TaskXrefModel xrefmodel = taskManager.GetReviewerViewIdByXrefTaskInstanceId(taskInstanceId);
                    formUploadData.viewId = xrefmodel.ViewId;
                    formUploadData.reviertaskStatus = xrefmodel.Status;
                    formUploadData.taskxrefId = taskInstanceId;
                    formUploadData.IsLoggedInUserLender = false;
                }

            }
            // based on task step, decide if is edit mode or readonly mode
            bool isEditMode = true;
            if (task.TaskStepId == 17)
            {
                task.TaskStepId = 14;
            }
            if (task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequest && RoleManager.IsCurrentUserLenderRoles())
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && !RoleManager.IsCurrentUserLenderRoles())
            {
                isEditMode = false;
            }


            bool hasTaskOpenByUser = false;
            formUploadData.IschildTask = isChildTask;

            formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                    ? new TaskOpenStatusModel()
                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                        TaskOpenStatusModel; //Know y and how

            formUploadData.TaskGuid = task.TaskInstanceId;
            formUploadData.taskxrefId = taskInstanceId;
            formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
            formUploadData.SequenceId = task.SequenceId;
            formUploadData.AssignedBy = task.AssignedBy;
            formUploadData.AssignedTo = task.AssignedTo;
            formUploadData.IsReassigned = task.IsReAssigned;
            formUploadData.TaskId = task.TaskId;
            formUploadData.PropertyId = projectActionFormManager.GetProdPropertyInfo(formUploadData.FhaNumber).PropertyId;
            formUploadData.TaskStepId = task.TaskStepId;
            formUploadData.IsEditMode = isEditMode;
            hasTaskOpenByUser = formUploadData.TaskOpenStatus.HasTaskOpenByUser(UserPrincipal.Current.UserName);

            //// save task opened in task db
            //if (formUploadData.TaskOpenStatus != null)
            //{
            //    formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
            //    task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus,
            //        typeof(TaskOpenStatusModel), Encoding.Unicode);
            //    _taskManager.UpdateTask(task);
            //}



            //formUploadData.ServicerSubmissionDate =
            //  TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
            formUploadData.ServicerSubmissionDate =
              (DateTime)formUploadData.ServicerSubmissionDate;


            return formUploadData;


        }


        private int GetNextStagePageTypeId(int completedPageTypeId)
        {
            if (completedPageTypeId == (int)PageType.ProductionApplication)
            {
                return (int)PageType.ClosingAllExceptConstruction;
            }
            else if (completedPageTypeId == (int)PageType.ConstructionSingleStage
                || completedPageTypeId == (int)PageType.ConstructionTwoStageInitial
                || completedPageTypeId == (int)PageType.ConstructionTwoStageFinal
                )
            {
                return (int)PageType.ClosingConstructionInsuredAdvances;
            }
            else if (completedPageTypeId == (int)PageType.ClosingConstructionInsuredAdvances)
            {
                return (int)PageType.ClosingConstructionInsuranceUponCompletion;
            }
            else if (completedPageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion)
            {
                return (int)PageType.ConstructionManagement;
            }
            else if (completedPageTypeId == (int)PageType.ClosingAllExceptConstruction)
            {
                //return (int)PageType.ClosingAllExceptConstruction;
                return (int)PageType.ExecutedClosing;
            }
            else
            {
                return 0;
            }

        }
        private OPAViewModel ReloadApplication(Guid taskinstanceid)
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            var Groupmodel = prod_GroupTasksManager.GetGroupTaskByTaskInstanceId(taskinstanceid);

            AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(Groupmodel.TaskInstanceId);
            var result = projectActionFormManager.GetProdPropertyInfo(AppProcessViewModel.FhaNumber);
            if (result != null)
            {
                AppProcessViewModel.PropertyId = result.PropertyId;
                AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                AppProcessViewModel.PropertyAddress.StateCode = result.State;
                AppProcessViewModel.PropertyAddress.City = result.City;
                AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
            }
            AppProcessViewModel.GroupTaskInstanceId = Groupmodel.TaskInstanceId;

            if (Groupmodel != null)
            {
                AppProcessViewModel.ServicerComments = Groupmodel.ServicerComments;

            }

            this.GetDisclaimerText(AppProcessViewModel, "APPLICATION REQUEST");

            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectTypebyID(AppProcessViewModel.ProjectActionTypeId);
            AppProcessViewModel.pageTypeId = Groupmodel.PageTypeId;


            return AppProcessViewModel;

        }
        private void GetDisclaimerText(OPAViewModel model, string Type)
        {
            model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType(Type);
        }

        public ActionResult SharepointForPdf(Guid taskInstanceId)
        {
            var queueController = new ProductionQueueController();
            var sharePointData = queueController.GetSharepointDataForPdf(taskInstanceId);
            return View("~/Views/Production/ProductionQueue/SharepointSinglePage/SharepointForPdf.cshtml", sharePointData);
        }
        public bool GenerateSharepointPdf(Guid taskInstanceId, string fhaNumber, int propertyId)
        {
            try
            {
                var queueController = new ProductionQueueController();
                var sharePointData = queueController.GetSharepointDataForPdf(taskInstanceId);
                var sharepointPdf = new ViewAsPdf("~/Views/Production/ProductionQueue/SharepointSinglePage/SharepointForPdf.cshtml", sharePointData);
                var binary = sharepointPdf.BuildPdf(ControllerContext);
                var uploadmodel = new RestfulWebApiUploadModel()
                {
                    propertyID = propertyId.ToString(),
                    indexType = "1",
                    indexValue = fhaNumber,
                    pdfConvertableValue = "false",
                    documentType = DefaultDocID,
                };
                var WebApiUploadResult = new RestfulWebApiResultModel();
                var request = new RestfulWebApiTokenResultModel();
                request = WebApiTokenRequest.RequestToken();

                var result = WebApiDocumentUpload.uploadSharepointPdfFile(uploadmodel, request.access_token, binary);


                Random randoms = new Random();
                var fileName = result.fileName == null ? "SharePointPdf.pdf" : result.fileName;
                var fileSize = binary.Length;
                var UniqueId = randoms.Next(0, 99999);
                var systemFileName = fhaNumber + "_" + taskInstanceId + "_" + UniqueId + Path.GetExtension(fileName);
                var taskFile = ControllerHelper.PopulateGroupTaskFileForSharepoint(fileName, fileSize, "SharePointPdf", DateTime.UtcNow.ToString(), systemFileName.ToString(), taskInstanceId);

                if (result.status != null)
                {
                    if (result.status.ToLower() == "success")
                    {
                        taskFile.API_upload_status = result.status;
                        taskFile.Version = Convert.ToInt32(result.version);
                        taskFile.DocId = result.docId;
                        //if (result.documentType != DefaultDocID)
                        taskFile.DocTypeID = result.documentType;
                        taskManager.SaveGroupTaskFileModel(taskFile);

                    }

                }  //Store information locally and regenerate it agian
                else
                {
                    taskFile.DocTypeID = DefaultDocID;
                    taskManager.SaveGroupTaskFileModel(taskFile);
                }
            }
            catch (Exception e)
            {

                return false;
            }

            return true;
        }

        public string DeleteFolder(int folderkey, string PropertyId, string FhaNumber)
        {
            RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
            RestfulWebApiResultModel APIresult = new RestfulWebApiResultModel();
            request = WebApiTokenRequest.RequestToken();

            string status = "Failed";

            var FolderInfo = projectActionFormManager.GetFolderInfoByKey(folderkey);
            string folderNames = string.Empty;

            if (FolderInfo != null)
            {
                foreach (Prod_SubFolderStructureModel folder in FolderInfo)
                {
                    folderNames = "/" + PropertyId + "/" + FhaNumber + "/" + "Production/" + folder.FolderName + "_" + folder.SubfolderSequence.Trim() + "/";
                }

            }
            if (FolderInfo != null)
            {
                if (UserPrincipal.Current.UserId == FolderInfo[0].CreatedBy || (UserPrincipal.Current.UserRole == "BackupAccountManager" || UserPrincipal.Current.UserRole == "LenderAccountManager"))
                {

                }
                else
                {
                    return "unauthorized";
                }
            }

            string json = "{\"folderName\":\"" + folderNames + "\"}";
            int result = 0;


            var FileIds = projectActionFormManager.GetFileIDByFolderKey(folderkey);
            if (FileIds.Count > 0)
            {
                return "HasFile";

            }
            else
            {
                APIresult = WebApiDelete.DeleteDocumentFolder(json, request.access_token);
                if (!string.IsNullOrEmpty(APIresult.status) && APIresult.key == "200")
                {
                    result = folderManager.DeleteSubFolderByID(Convert.ToInt32(folderkey));
                    return "Success";
                }
                else if (!string.IsNullOrEmpty(APIresult.status) && APIresult.key == "201")
                {
                    result = folderManager.DeleteSubFolderByID(Convert.ToInt32(folderkey));
                    return "Success";
                }
                else
                {
                    return "Failed";
                }
            }



        }
        public string AddFolder(int folderkey, string PropertyId, string FhaNumber, string GroupTaskInstanceId, int PageTypeId)
        {


            var model = new Prod_SubFolderStructureModel();
            char CurentSubfolderSequence = 'A';
            var SubFolders = projectActionFormManager.GetProdSubFolders(folderkey, PropertyId, FhaNumber);
            if (SubFolders != null)
            {
                SubFolders.RemoveAll(x => x.PageTypeId != PageTypeId);
            }
            if (SubFolders == null || SubFolders.Count == 0)
            {
                model.SubfolderSequence = "B";
            }
            else
            {
                foreach (Prod_SubFolderStructureModel mdel in SubFolders)
                {
                    if (CurentSubfolderSequence < Convert.ToChar(mdel.SubfolderSequence.Trim()))
                    {
                        CurentSubfolderSequence = Convert.ToChar(mdel.SubfolderSequence.Trim());
                    }
                }
                if (CurentSubfolderSequence == 'Z' || CurentSubfolderSequence == 'z')
                    model.SubfolderSequence = "AA";

                else
                    model.SubfolderSequence = ((char)((CurentSubfolderSequence) + 1)).ToString();
            }
            if (PageTypeId == 10)
            {
                model.FolderName = "Non-Construction Draft Closing";
            }
            if (PageTypeId == 17)
            {
                model.FolderName = "Non-Construction Executed Closing";
            }
            model.FolderKey = folderkey;
            model.ProjectNo = PropertyId;
            model.FhaNo = FhaNumber;
            model.CreatedBy = UserPrincipal.Current.UserId;
            model.CreatedOn = DateTime.UtcNow;
            model.PageTypeId = PageTypeId;
            model.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
            int result = projectActionFormManager.SaveProdSubFolders(model);
            if (result > 0)
            {
                var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(new Guid(GroupTaskInstanceId));
                if (grouptaskmodel == null)
                {
                    var prodgroupTaskModel = new Prod_GroupTasksModel();
                    prodgroupTaskModel.TaskInstanceId = new Guid(GroupTaskInstanceId);
                    prodgroupTaskModel.InUse = UserPrincipal.Current.UserId;

                    prodgroupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                    prodgroupTaskModel.PageTypeId = PageTypeId;
                    //prodgroupTaskModel.PageTypeId = (int) PageType.ProductionApplication;
                    prodgroupTaskModel.IsDisclaimerAccepted = false;
                    prodgroupTaskModel.ModifiedOn = DateTime.UtcNow;
                    prodgroupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                    prodgroupTaskModel.CreatedOn = DateTime.UtcNow;
                    prodgroupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                    prodgroupTaskModel.ServicerComments = "";
                    var ProdGroupTaskId = prod_GroupTasksManager.AddProdGroupTasks(prodgroupTaskModel);


                    //Save OpaForm
                    var OPAModel = new OPAViewModel();
                    PropertyInfoModel propInfo = projectActionFormManager.GetProdPropertyInfo(FhaNumber);
                    OPAModel.ProjectActionName = appProcessManager.GetProjectTypebyID(propInfo.ProjectTypeID);

                    OPAModel.ServicerComments = "";
                    OPAModel.PropertyName = propInfo.PropertyName;
                    OPAModel.RequesterName = "";
                    OPAModel.ProjectActionTypeId = propInfo.ProjectTypeID;
                    if (propInfo.IsAddressChange != null)
                    {
                        OPAModel.IsAddressChange = (bool)propInfo.IsAddressChange;
                    }

                    OPAModel.FhaNumber = FhaNumber;
                    OPAModel.ServicerSubmissionDate = null;
                    OPAModel.RequestStatus = (int)RequestStatus.Draft;
                    OPAModel.CreatedOn = DateTime.UtcNow;
                    OPAModel.ModifiedOn = DateTime.UtcNow;
                    OPAModel.CreatedBy = UserPrincipal.Current.UserId;
                    OPAModel.ModifiedBy = UserPrincipal.Current.UserId;
                    OPAModel.ProjectActionDate = DateTime.UtcNow;
                    OPAModel.RequestDate = DateTime.UtcNow;
                    OPAModel.TaskInstanceId = ProdGroupTaskId;
                    OPAModel.PropertyId = propInfo.ProjectTypeID;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAModel);


                    var nextStage = new Prod_NextStageModel();

                    nextStage.NextPgTypeId = PageTypeId;
                    nextStage.ModifiedOn = DateTime.UtcNow;
                    nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                    nextStage.FhaNumber = FhaNumber;
                    nextStage.NextPgTaskInstanceId = new Guid(GroupTaskInstanceId);
                    nextStage.IsNew = false;
                    prod_NextStageManager.UpdateTaskInstanceIdForNxtStage(nextStage);

                }
                return "Success";
            }
            else
                return "Failed";
        }
        public void MultipleDelete(string taskFileIDs)
        {
            var taskFileIdList = taskFileIDs.Split(',');
            foreach (var taskFileId in taskFileIdList)
            {
                DeleteFile(Convert.ToInt32(taskFileId));
            }

        }
        private bool DeleteFile(int fileID)
        {
            RestfulWebApiTokenResultModel requestAPI = new RestfulWebApiTokenResultModel();
            RestfulWebApiResultModel resultAPI = new RestfulWebApiResultModel();
            RestfulWebApiUpdateModel updateAPI = new RestfulWebApiUpdateModel();

            TaskFileModel taskFile = taskManager.GetTaskFileById(fileID);
            var success = 0;
            string pathForSaving = "";
            if (taskFile != null)
            {
                if (UserPrincipal.Current.UserId == taskFile.CreatedBy
                    || (UserPrincipal.Current.UserRole == "BackupAccountManager"
                    || UserPrincipal.Current.UserRole == "LenderAccountManager"))
                {

                    pathForSaving = Server.MapPath("~/Uploads");

                    success = taskManager.DeleteTaskFileByFileID(fileID);
                    if (success == 1)
                    {
                        if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                        {
                            requestAPI = WebApiTokenRequest.RequestToken();

                            resultAPI = WebApiDelete.DeleteDocumentUsingWebApi(taskFile.DocId, requestAPI.access_token);
                            if (!string.IsNullOrEmpty(resultAPI.status) && resultAPI.key == "200")
                            {
                                return true;
                            }
                        }
                        else
                        {
                            pathForSaving = Server.MapPath("~/Uploads");


                            System.IO.File.Delete(pathForSaving + "\\" + taskFile.SystemFileName);

                            if (success == 1)
                                return true;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }


            return false;
        }
        //public bool IsFileExistsForChildTask(string childTaskInstanceId)
        //{
        //    return projectActionFormManager.IsFileExistsForChildTask(new Guid(childTaskInstanceId));

        //}

    }
}
