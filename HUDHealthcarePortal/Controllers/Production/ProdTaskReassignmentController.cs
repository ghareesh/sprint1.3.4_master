﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;
using BusinessService.AssetManagement;
using BusinessService;
using Model;
using Model.Production;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using BusinessService.ManagementReport;
using System.Configuration;
using System.Collections;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class ProdTaskReassignmentController : Controller
    {
        //
        private IProductionPAMReportManager _plmProdReportMgr;
        private IProductionReassignReportManager _reassignReportMgr;
        private ITaskManager _taskManager;
        private IBackgroundJobMgr _backgroundJobManager;
        private IEmailManager _emailManager;
        private ITaskManager taskManager;
        public ProdTaskReassignmentController()
            : this(new ProductionPAMReportManager(), new TaskManager(), new ProductionReassignReportManager(), new BackgroundJobMgr(), new EmailManager(), new TaskManager())
        {

        }

        public ProdTaskReassignmentController(IProductionPAMReportManager PAMProdReportManager, ITaskManager taskManager, IProductionReassignReportManager reassignReportMgr, IBackgroundJobMgr backgroundJobManager, IEmailManager emailManager, ITaskManager _taskManager)
        {
            _plmProdReportMgr = PAMProdReportManager;
            _taskManager = taskManager;
            _reassignReportMgr = reassignReportMgr;
            _backgroundJobManager = backgroundJobManager;
            _emailManager = emailManager;
            taskManager = _taskManager;
        }

       

        public ActionResult Index(string prodUserIds = null, string fromDate = null, string toDate = null,
           string status = null, string projectAction = null, string isApplicationOrTaskLevel = null)
        {
            ViewBag.ExcelExportAction = "ExportProdPAMReport";
         
            ViewBag.GetProjectTypesByAppTypeAction = Url.Action("GetProjectTypesByAppType");
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            var viewModel = InitializeViewModel(prodUserIds, fromDate, toDate, status, projectAction,
                isApplicationOrTaskLevel);
            return View("~/Views/Production/ProductionTaskReassignment/ProdReassignmentReport.cshtml", viewModel);
           
        }

        public JsonResult GetProjectTypesByAppType(string value)
        {
            List<object> projectTypeObjs = new List<object>();
            var proejctTypes = _plmProdReportMgr.GetProjectTypesByAppType(value);
            projectTypeObjs = new List<object>(proejctTypes.Count);
            foreach (var m in proejctTypes)
            {
                projectTypeObjs.Add(new { Key = m.ProjectTypeId, Value = m.ProjectTypeName });
            }
            return Json(new { data = projectTypeObjs }, JsonRequestBehavior.AllowGet);
        }



        private ProductionReasignReportModel InitializeViewModel(string prodUserIds, string fromDate, string toDate,
            string status, string projectAction, string isApplicationOrTaskLevel)
        {            
            var viewModel = new ProductionReasignReportModel(ReportType.ProdTaskReassignment);
            var userName = UserPrincipal.Current.UserName;
            string prodWLMName = string.Empty;

            var isDefaultSearch = false;
            var prodUsers = _plmProdReportMgr.GetProductionUsers().ToList();
            ;

            if (RoleManager.IsProductionWLM(userName))
            {
                prodWLMName = UserPrincipal.Current.FullName;
            }

            var prodPageTypes = _plmProdReportMgr.GetProductionPageTypes();
           
            var prodProjectTypes = _plmProdReportMgr.GetProjectTypeModels();
            viewModel.ProductionUsers = new MultiSelectList(prodUsers, "UserID", "Name");
            viewModel.ProductionPageTypes = new MultiSelectList(prodPageTypes, "PageTypeId", "PageTypeDescription");
         
            viewModel.ProductionProjecTypeList = new MultiSelectList(prodProjectTypes, "ProjectTypeId",
                "ProjectTypeName");

            return viewModel;
        }

        public JsonResult GetTReassignTasks(string sidx, string sord, int page, int rows, string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate,bool search=false)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var currentUser = UserPrincipal.Current;
            //format date
            //DateTime? fromUTCDate = null;
            // DateTime? toUTCDate = null;
            //if(fromDate!=null)
            //{
            //    fromUTCDate =  TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(fromDate));
            //}

            //if (toDate != null)
            //{
            //   toUTCDate = TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(toDate));
            //}
            var reassigntasks = new ProductionReasignReportModel(ReportType.ProdTaskReassignment);

            object jsonData=null;
            if (search)
            {
                reassigntasks = _reassignReportMgr.GetProdReassignmentTasks(appType, programType, prodUserIds,
fromDate == null ? (DateTime?)null : DateTime.Parse(fromDate.ToString()),
fromDate == null ? (DateTime?)null : DateTime.Parse(toDate.ToString()));

                if (reassigntasks != null)
                {
                    foreach (var item in reassigntasks.ReassignGridProductionlList)
                    {
                        //item.StartDate = TimezoneManager.ToMyTimeFromUtc(item.StartDate);
                        //item.EndDate = TimezoneManager.ToMyTimeFromUtc(item.EndDate);
                        item.StartDate = item.StartDate;
                        item.EndDate = item.EndDate;
                    }



                    int totalrecods = reassigntasks.ReassignGridProductionlList.Count;
                    var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);









                    var results = reassigntasks.ReassignGridProductionlList.Skip(pageIndex * pageSize).Take(pageSize);
                    var outresults = results.GroupBy(s => s.Groupid).SelectMany(gr => gr).ToList();
                    jsonData = new
                    {
                        total = totalpages,
                        page,
                        records = totalrecods,
                        rows = outresults,

                    };
                }


            }
            
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void ReassignTasks(List<string> kvp, int AssignUserId,string AssignetoName, int FromProdUser, string AssigneFromName)
        {
            var AssignetoEmail = _plmProdReportMgr. GetUserInfoByID(AssignUserId);
            var AssigneFromEmail = _plmProdReportMgr.GetUserInfoByID(FromProdUser);
            _reassignReportMgr.ReassignTasks(kvp, AssignUserId);
            foreach (var item in kvp)
            {
                string[] pair = item.Split('|');
                
               bool Result= _backgroundJobManager.SendProdTempTaskReassignmentEmail(_emailManager, AssigneFromEmail, AssigneFromName, AssignetoEmail, AssignetoName, pair[2],pair[3], pair[4], pair[5]);
            }
            
        }



    }
}
