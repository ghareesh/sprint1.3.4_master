﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;
using BusinessService.AssetManagement;
using BusinessService;
using Model;
using Model.Production;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using BusinessService.ManagementReport;
using System.Configuration;
using System.Collections;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class LenderProdTaskReassignmentController : Controller
    {
        //
        private IProductionPAMReportManager _plmProdReportMgr;
        private ILenderProductionReassignReportManager _reassignReportMgr;
        private ITaskManager _taskManager;
        private IBackgroundJobMgr _backgroundJobManager;
        private IEmailManager _emailManager;
        private ITaskManager taskManager;
        private IAccountManager accountManager;
        public LenderProdTaskReassignmentController()
            : this(new ProductionPAMReportManager(), new TaskManager(), new LenderProductionReassignReportManager(), new BackgroundJobMgr(), new EmailManager(), new TaskManager(), new AccountManager())
        {

        }

        public LenderProdTaskReassignmentController(IProductionPAMReportManager PAMProdReportManager, ITaskManager taskManager, ILenderProductionReassignReportManager reassignReportMgr, IBackgroundJobMgr backgroundJobManager, IEmailManager emailManager, ITaskManager _taskManager, IAccountManager _accountManager)
        {
            _plmProdReportMgr = PAMProdReportManager;
            _taskManager = taskManager;
            _reassignReportMgr = reassignReportMgr;
            _backgroundJobManager = backgroundJobManager;
            _emailManager = emailManager;
            taskManager = _taskManager;
            accountManager = _accountManager;
        }



        public ActionResult Index(string prodUserIds = null, string fromDate = null, string toDate = null,
           string status = null, string projectAction = null, string isApplicationOrTaskLevel = null)
        {
            ViewBag.ExcelExportAction = "ExportProdPAMReport";

            ViewBag.GetProjectTypesByAppTypeAction = Url.Action("GetProjectTypesByAppType");
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            var viewModel = InitializeViewModel(prodUserIds, fromDate, toDate, status, projectAction,
                isApplicationOrTaskLevel);
            return View("~/Views/Production/ProductionTaskReassignment/LenderProdReassignmentReport.cshtml", viewModel);

        }

        public JsonResult GetProjectTypesByAppType(string value)
        {
            List<object> projectTypeObjs = new List<object>();
            var proejctTypes = _plmProdReportMgr.GetProjectTypesByAppType(value);
            projectTypeObjs = new List<object>(proejctTypes.Count);
            foreach (var m in proejctTypes)
            {
                projectTypeObjs.Add(new { Key = m.ProjectTypeId, Value = m.ProjectTypeName });
            }
            return Json(new { data = projectTypeObjs }, JsonRequestBehavior.AllowGet);
        }



        private LenderProductionReasignReportModel InitializeViewModel(string prodUserIds, string fromDate, string toDate,
            string status, string projectAction, string isApplicationOrTaskLevel)
        {
            var viewModel = new LenderProductionReasignReportModel(ReportType.ProdTaskReassignment);
            var userName = UserPrincipal.Current.UserName;
            string prodWLMName = string.Empty;

            var isDefaultSearch = false;
            var LenderUsers = _plmProdReportMgr.GetAllLenderUsers(UserPrincipal.Current.LenderId).ToList();
            ;

            if (RoleManager.IsUserLenderRole(userName) || RoleManager.IsUserLamBamLar(userName))
            {
                prodWLMName = UserPrincipal.Current.FullName;
            }

            var prodPageTypes = _plmProdReportMgr.GetProductionPageTypes();

            var prodProjectTypes = _plmProdReportMgr.GetProjectTypeModels();
            viewModel.LenderUser = new MultiSelectList(LenderUsers, "UserID", "Name");
            viewModel.ProductionPageTypes = new MultiSelectList(prodPageTypes, "PageTypeId", "PageTypeDescription");

            viewModel.ProductionProjecTypeList = new MultiSelectList(prodProjectTypes, "ProjectTypeId",
                "ProjectTypeName");

            return viewModel;
        }
        public JsonResult GetTReassignTasks(string sidx, string sord, int page, int rows, string appType, string programType, string lenderUserIds, DateTime? fromDate, DateTime? toDate, bool search = false)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var currentUser = UserPrincipal.Current;
           
            var reassigntasks = new LenderProductionReasignReportModel(ReportType.ProdTaskReassignment);

            object jsonData = null;
            if (search)
            {
                reassigntasks = _reassignReportMgr.GetLenderProdReassignmentTasks(appType, programType, lenderUserIds,
fromDate == null ? (DateTime?)null : DateTime.Parse(fromDate.ToString()),
fromDate == null ? (DateTime?)null : DateTime.Parse(toDate.ToString()));

                if (reassigntasks != null)
                {
                    foreach (var item in reassigntasks.ReassignGridProductionlList)
                    {
                        //item.StartDate = TimezoneManager.ToMyTimeFromUtc(item.StartDate);
                        //item.EndDate = TimezoneManager.ToMyTimeFromUtc(item.EndDate);
                        item.StartDate = item.StartDate;
                        item.EndDate = item.EndDate;
                        item.ProcessedBy = accountManager.GetFullUserNameById(item.ProcessedById);
                    }

                    int totalrecods = reassigntasks.ReassignGridProductionlList.Count;
                    var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);

                    var results = reassigntasks.ReassignGridProductionlList.Skip(pageIndex * pageSize).Take(pageSize);
                    var outresults = results.GroupBy(s => s.Groupid).SelectMany(gr => gr).ToList();
                    jsonData = new
                    {
                        total = totalpages,
                        page,
                        records = totalrecods,
                        rows = outresults.OrderBy(x=>x.FhaRequestType).ToList(),

                    };
                }


            }

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }




      

        [HttpPost]
        public void ReassignTasks(List<string> kvp, int AssignUserId, string AssignetoName, int FromProdUser, string AssigneFromName)
        {
            var AssignetoEmail = _plmProdReportMgr.GetUserInfoByID(AssignUserId);
            var AssigneFromEmail = _plmProdReportMgr.GetUserInfoByID(FromProdUser);
            _reassignReportMgr.LenderReassignTasks(kvp, AssignUserId);
            foreach (var item in kvp)
            {
                string[] pair = item.Split('|');

                bool Result = _backgroundJobManager.SendLenderProdTempTaskReassignmentEmail(_emailManager, AssigneFromEmail, AssigneFromName, AssignetoEmail, AssignetoName, pair[2], pair[3], pair[4], pair[5]);
            }

        }


      



    }
}
