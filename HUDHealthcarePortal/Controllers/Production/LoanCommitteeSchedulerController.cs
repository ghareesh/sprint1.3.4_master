﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;
using BusinessService.AssetManagement;
using BusinessService;
using Model;
using Model.Production;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using BusinessService.ManagementReport;
using System.Configuration;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class LoanCommitteeSchedulerController : Controller
    {
        private IProjectActionFormManager projectActionFormManager;
        private IProjectActionManager projectActionManager;
        private IGroupTaskManager groupTaskManager;
        private ITaskManager taskManager;
        private IAccountManager accountManager;
        private IEmailManager emailManager;
        private IBackgroundJobMgr backgroundJobManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager;
        private IParentChildTaskManager parentChildTaskManager;
        private IRequestAdditionalInfoFileManager requestAdditionalInfoFilesManager;
        private IReviewFileCommentManager reviewFileCommentManager;
        private IReviewFileStatusManager reviewFileStatusManager;
        private IProd_GroupTasksManager prod_GroupTasksManager;
        private ITaskFile_FolderMappingManager taskFile_FolderMappingManager;
        private IFHANumberRequestManager fhaRequestManager;
        private IAppProcessManager appProcessManager;
        private IProd_TaskXrefManager prod_TaskXrefManager;
        private IOPAManager iOPAForm;
        IPAMReportManager _plmReportMgr;
        IWebSecurityWrapper webSecurity;
        private IProductionQueueManager productionQueue;
        private IProd_NextStageManager prod_NextStageManager;
        //private IProd_RestfulWebApiTokenRequest WebApiTokenRequest;
        private IProd_LoanCommitteeManager Prod_LoanCommitteeManager;

        public LoanCommitteeSchedulerController()
            : this(
                new ProjectActionFormManager(), new TaskManager(), new AccountManager(), new EmailManager(),
                new BackgroundJobMgr(), new ProjectActionManager(), new GroupTaskManager(),
                new NonCriticalRepairsRequestManager(), new ParentChildTaskManager(),
                new RequestAdditionalInfoFilesManager(), new ReviewFileCommentManager(), new ReviewFileStatusManager(),
                new WebSecurityWrapper(), new OPAManager(), new Prod_GroupTasksManager(), new TaskFile_FolderMappingManager(),
                new FHANumberRequestManager(), new PAMReportsManager(), new AppProcessManager(), new Prod_TaskXrefManager(),
                new ProductionQueueManager(), new Prod_NextStageManager(), new Prod_LoanCommitteeManager())
        {

        }

        private LoanCommitteeSchedulerController(IProjectActionFormManager _projectActionFormManager, ITaskManager _taskManager,
          IAccountManager _accountManager, IEmailManager _emailManager, IBackgroundJobMgr backgroundManager,
          IProjectActionManager _projectActionManager, IGroupTaskManager _groupTaskManager,
          INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
          IParentChildTaskManager _parentChildTaskManager,
          IRequestAdditionalInfoFileManager _requestAdditionalInfoFilesManager,
          IReviewFileCommentManager _reviewFileCommentManager,
           IReviewFileStatusManager _reviewFileStatusManager,
          IWebSecurityWrapper webSec,
          IOPAManager _opaManager,
          IProd_GroupTasksManager _prod_GroupTasksManager, ITaskFile_FolderMappingManager _taskFile_FolderMappingManager, FHANumberRequestManager _fhaRequestManager, IPAMReportManager plmReportMgr,
          //IProd_RestfulWebApiTokenRequest _WebApiTokenRequest,  
          IAppProcessManager _appProcessManager, IProd_TaskXrefManager _prodTaskXrefManager, IProductionQueueManager _productionQueueManager, IProd_NextStageManager _prod_NextStageManager, IProd_LoanCommitteeManager _Prod_LoanCommitteeManager)
        {
            projectActionFormManager = _projectActionFormManager;
            taskManager = _taskManager;
            accountManager = _accountManager;
            emailManager = _emailManager;
            backgroundJobManager = backgroundManager;
            projectActionManager = _projectActionManager;
            groupTaskManager = _groupTaskManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager; // User Story 1901
            parentChildTaskManager = _parentChildTaskManager;
            requestAdditionalInfoFilesManager = _requestAdditionalInfoFilesManager;
            reviewFileCommentManager = _reviewFileCommentManager;
            reviewFileStatusManager = _reviewFileStatusManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;// User Story 1901
            webSecurity = webSec;
            iOPAForm = _opaManager;
            prod_GroupTasksManager = _prod_GroupTasksManager;
            taskFile_FolderMappingManager = _taskFile_FolderMappingManager;
            fhaRequestManager = _fhaRequestManager;
            _plmReportMgr = plmReportMgr;
            appProcessManager = _appProcessManager;
            prod_TaskXrefManager = _prodTaskXrefManager;
            productionQueue = _productionQueueManager;
            prod_NextStageManager = _prod_NextStageManager;
            //WebApiTokenRequest = _WebApiTokenRequest;
            Prod_LoanCommitteeManager = _Prod_LoanCommitteeManager;

        }

        public ActionResult Index()
        {
            LoanCommitteeFiltersModel LoanCommitteeFiltersModel = new LoanCommitteeFiltersModel();

            var GridContent = Prod_LoanCommitteeManager.GetLCFiltersValues();
            //GridContent= GridContent.Where(f => string.IsNullOrEmpty(f.LCRecommendation) == null).Select(n => n.LCRecommendation="NA");
            var projectTypes = fhaRequestManager.GetAllProjectTypesForLC();
            LoanCommitteeFiltersModel.AllProjectTypeNames = projectTypes.GroupBy(x => x.ProjectTypeName).Select(i => new SelectListItem() { Text = i.First().ProjectTypeName.ToString(), Value = i.First().ProjectTypeId.ToString() }).Distinct().OrderBy(x => x.Text).ToList();
            LoanCommitteeFiltersModel.AllOHPAppraisalReviewer = GridContent.Where(x => x.Appraiser != null).GroupBy(x => x.Appraiser).Select(i => new SelectListItem() { Text = i.First().Appraiser.ToString() }).Distinct().OrderBy(x => x.Text).ToList();
            LoanCommitteeFiltersModel.AllOGCAttorney = GridContent.Where(x => x.Ogc != null).GroupBy(x => x.Ogc).Select(i => new SelectListItem() { Text = i.First().Ogc.ToString() }).Distinct().OrderBy(x => x.Text).ToList();
            LoanCommitteeFiltersModel.AllWorkloadManager = GridContent.Where(x => x.WLM != null).GroupBy(x => x.WLM).Select(i => new SelectListItem() { Text = i.First().WLM.ToString() }).Distinct().OrderBy(x => x.Text).ToList();
            //LoanCommitteeFiltersModel.AllSequenceNumber = GridContent.OrderBy(y=>y.SequenceId).GroupBy(x => x.SequenceId).Select(i => new SelectListItem() { Text = i.First().SequenceId.ToString() }).Distinct().ToList();
            LoanCommitteeFiltersModel.AllSequenceNumber = new List<SelectListItem>();
            LoanCommitteeFiltersModel.AllSequenceNumber.Add(new SelectListItem() { Text = "Approve", Value = "1" });
            LoanCommitteeFiltersModel.AllSequenceNumber.Add(new SelectListItem() { Text = "Reject", Value = "0" });
            LoanCommitteeFiltersModel.AllSequenceNumber.Add(new SelectListItem() { Text = "Withdrawn", Value = "2" });
            LoanCommitteeFiltersModel.AllSequenceNumber.Add(new SelectListItem() { Text = "Request Additional Information", Value = "3" });
            LoanCommitteeFiltersModel.AllDay = GridContent.GroupBy(x => x.LoanCommitteDate.Value.ToString("dddd")).Select(i => new SelectListItem() { Text = i.First().LoanCommitteDate.Value.ToString("dddd"), Value = i.First().LoanCommitteDate.ToString() }).Distinct().OrderBy(x => Convert.ToDateTime(x.Value)).ToList();
            LoanCommitteeFiltersModel.AllLender = GridContent.Where(x => x.Lender != null).GroupBy(x => x.Lender).Select(i => new SelectListItem() { Text = i.First().Lender.ToString(), Value = i.First().Lender.ToString() }).Distinct().OrderBy(x => x.Text).ToList();
            LoanCommitteeFiltersModel.AllLoanCommitteeDate = GridContent.OrderBy(y => y.LoanCommitteDate).GroupBy(x => x.LoanCommitteDate.Value.ToString("MM/dd/yyyy")).Select(i => new SelectListItem() { Text = i.First().LoanCommitteDate.Value.ToString("MM/dd/yyyy") }).Distinct().ToList();
            LoanCommitteeFiltersModel.AllProjectType = GridContent.GroupBy(x => x.ProjectType).Select(i => new SelectListItem() { Text = i.First().ProjectType.ToString() }).Distinct().OrderBy(x => x.Text).ToList();
            LoanCommitteeFiltersModel.AllUnderwriter = GridContent.Where(x => x.Underwriter != null).GroupBy(x => x.Underwriter).Select(i => new SelectListItem() { Text = i.First().Underwriter.ToString() }).Distinct().OrderBy(x => x.Text).ToList();

            return View("~/Views/Production/LoanCommitteeScheduler/LoanCommitteeSchedulerMain.cshtml", LoanCommitteeFiltersModel);
        }
        public ActionResult GetLCGridResults()
        {
            var GridContent = Prod_LoanCommitteeManager.GetLCFiltersValues();
            GridContent = GridContent.OrderBy(x=>x.LoanCommitteDate.Value.ToString("MM/dd/yyyy")).ThenBy(y=>y.SequenceId).ToList();
            string output = JsonConvert.SerializeObject(GridContent);
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWPUpload(Guid? taskInstanceId)
        {
            int Status = 1;
            int PageTypeId = 10;
            string sidx = "";
            string sord = "";
            int page = 1;
            int rows = 100;
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            var GridContent = projectActionFormManager.GetGetWPUpload(taskInstanceId.Value, Status, PageTypeId);
            GridContent = GridContent.Where(x => x.id == "8" || x.parent == "8").ToList();
            //GridContent = GridContent.Where(x => x.parent == "8").ToList();
            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    //if (!string.IsNullOrEmpty(item.parent))
                    //{
                    //    var DocTypeList = appProcessManager.GetMappedDocTypesbyFolder(Convert.ToInt32(item.parent));
                    //    if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                    //    {
                    //        //item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                    //    }
                    //}
                }
            }
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalrecods = GridContent != null ? GridContent.Count() : 0;
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var result = GridContent != null ? GridContent.ToList().OrderBy(a => a.id) : null;
            var results = result != null ? result.Skip(pageIndex * pageSize).Take(pageSize) : result;
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetWorkProduct(string taskInstanceId)
        {
            OPAViewModel OPAViewModel = new OPAViewModel();
            OPAViewModel.TaskInstanceId = Guid.Parse(taskInstanceId);
            return PartialView("~/Views/Production/LoanCommitteeScheduler/_LoanCommitteeWorkProductDocumentGrid.cshtml", OPAViewModel);
        }
        public ActionResult UpdateLCGridResults(string Model)
        {
            string update = "Success";
            try
            {

                IList<Prod_LoanCommitteeViewModel> UpdatedGridData = JsonConvert.DeserializeObject<IList<Prod_LoanCommitteeViewModel>>(Model);

                var isInfoUpdated = Prod_LoanCommitteeManager.UpdateLCGridResults(UpdatedGridData);
            }
            catch (Exception ex)
            {
                update = "Fail";
            }
            //var GridContent = Prod_LoanCommitteeManager.GetLCFiltersValues();

            //string output = JsonConvert.SerializeObject(GridContent);
            return Json(update, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetLCselectedFilterValues(LoanCommitteeFiltersModel Model)
        {

            var GridContent = Prod_LoanCommitteeManager.GetLCFiltersValues();

            GridContent = ControllerHelper.FilterResultForLoanCommitteeScheduler(GridContent, Model);

            String output = JsonConvert.SerializeObject(GridContent);
            return Json(output);
        }

         public ActionResult GetLCDataByDate(LoanCommitteeFiltersModel Model)
        {
            var GridContent = Prod_LoanCommitteeManager.GetLCFiltersValues();

            if (Model.LoanCommitteeDate != null)
            {
                GridContent = GridContent.Where(x => x.LoanCommitteDate == Model.LoanCommitteeDate).ToList();
            }
           
            String output = JsonConvert.SerializeObject(GridContent);
            return Json(output);
        }
    }
}
