﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using Rotativa;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;
using BusinessService.AssetManagement;
using BusinessService;
using Model;
using Model.Production;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using BusinessService.ManagementReport;
using System.Configuration;
using System.Drawing;


namespace HUDHealthcarePortal.Controllers.Production
{
	public class AmendmentsController : Controller
	{

		private IProjectActionFormManager projectActionFormManager;
		private IProjectActionManager projectActionManager;
		private IGroupTaskManager groupTaskManager;
		private ITaskManager taskManager;
		private IAccountManager accountManager;
		private IEmailManager emailManager;
		private IBackgroundJobMgr backgroundJobManager;
		private INonCriticalRepairsRequestManager nonCriticalRepairsManager;
		private IParentChildTaskManager parentChildTaskManager;
		private IRequestAdditionalInfoFileManager requestAdditionalInfoFilesManager;
		private IReviewFileCommentManager reviewFileCommentManager;
		private IReviewFileStatusManager reviewFileStatusManager;
		private IProd_GroupTasksManager prod_GroupTasksManager;
		private ITaskFile_FolderMappingManager taskFile_FolderMappingManager;
		private IFHANumberRequestManager fhaRequestManager;
		private IAppProcessManager appProcessManager;
		private IProd_TaskXrefManager prod_TaskXrefManager;
		private IOPAManager iOPAForm;
		IPAMReportManager _plmReportMgr;
		IWebSecurityWrapper webSecurity;
		private IProductionQueueManager productionQueue;
		private IProd_NextStageManager prod_NextStageManager;
		private IProd_RestfulWebApiTokenRequest WebApiTokenRequest;
		private IProd_RestfulWebApiDocumentUpload WebApiDocumentUpload;
		private IProd_LoanCommitteeManager Prod_LoanCommitteeManager;
		private string DefaultDocID = ConfigurationManager.AppSettings["DefaultDocId"].ToString();


		public AmendmentsController()
			: this(
				new ProjectActionFormManager(),
				new TaskManager(),
				new AccountManager(),
				new EmailManager(),
				new BackgroundJobMgr(),
				new ProjectActionManager(),
				new GroupTaskManager(),
				new NonCriticalRepairsRequestManager(),
				new ParentChildTaskManager(),
				new RequestAdditionalInfoFilesManager(),
				new ReviewFileCommentManager(),
				new ReviewFileStatusManager(),
				new WebSecurityWrapper(),
				new OPAManager(),
				new Prod_GroupTasksManager(),
				new TaskFile_FolderMappingManager(),
				new FHANumberRequestManager(),
				new PAMReportsManager(),
				new Prod_RestfulWebApiTokenRequest(),
				new AppProcessManager(),
				new Prod_TaskXrefManager(),
				new ProductionQueueManager(),
				new Prod_NextStageManager(),
				new Prod_RestfulWebApiDocumentUpload(),
				new Prod_LoanCommitteeManager())
		{

		}

		private AmendmentsController(IProjectActionFormManager _projectActionFormManager,
			ITaskManager _taskManager,
		  IAccountManager _accountManager,
		  IEmailManager _emailManager,
		  IBackgroundJobMgr backgroundManager,
		  IProjectActionManager _projectActionManager,
		  IGroupTaskManager _groupTaskManager,
		  INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
		  IParentChildTaskManager _parentChildTaskManager,
		  IRequestAdditionalInfoFileManager _requestAdditionalInfoFilesManager,
		  IReviewFileCommentManager _reviewFileCommentManager,
		   IReviewFileStatusManager _reviewFileStatusManager,
		  IWebSecurityWrapper webSec,
		  IOPAManager _opaManager,
		  IProd_GroupTasksManager _prod_GroupTasksManager,
		  ITaskFile_FolderMappingManager _taskFile_FolderMappingManager,
		  FHANumberRequestManager _fhaRequestManager,
		  IPAMReportManager plmReportMgr,
		  IProd_RestfulWebApiTokenRequest _WebApiTokenRequest,
		  IAppProcessManager _appProcessManager,
		  IProd_TaskXrefManager _prodTaskXrefManager,
		  IProductionQueueManager _productionQueueManager,
		  IProd_NextStageManager _prod_NextStageManager,
		   IProd_RestfulWebApiDocumentUpload _webApiUpload,
		   IProd_LoanCommitteeManager _prod_LoanCommitteeManager)
		{
			projectActionFormManager = _projectActionFormManager;
			taskManager = _taskManager;
			accountManager = _accountManager;
			emailManager = _emailManager;
			backgroundJobManager = backgroundManager;
			projectActionManager = _projectActionManager;
			groupTaskManager = _groupTaskManager;
			nonCriticalRepairsManager = _nonCriticalRepairsManager; // User Story 1901
			parentChildTaskManager = _parentChildTaskManager;
			requestAdditionalInfoFilesManager = _requestAdditionalInfoFilesManager;
			reviewFileCommentManager = _reviewFileCommentManager;
			reviewFileStatusManager = _reviewFileStatusManager;
			webSecurity = webSec;
			iOPAForm = _opaManager;
			prod_GroupTasksManager = _prod_GroupTasksManager;
			taskFile_FolderMappingManager = _taskFile_FolderMappingManager;
			fhaRequestManager = _fhaRequestManager;
			_plmReportMgr = plmReportMgr;
			WebApiTokenRequest = _WebApiTokenRequest;
			appProcessManager = _appProcessManager;
			prod_TaskXrefManager = _prodTaskXrefManager;
			productionQueue = _productionQueueManager;
			prod_NextStageManager = _prod_NextStageManager;
			WebApiDocumentUpload = _webApiUpload;
			Prod_LoanCommitteeManager = _prod_LoanCommitteeManager;

		}
		private void InitializeViewModel(OPAViewModel AppProcessViewModel, bool IsIRR)
		{
			ViewBag.SaveCheckListFormURL = Url.Action("ApproveCheckListForm");

			AppProcessViewModel.IsEditMode = false;
			//var projectActionList = appProcessManager.GetAllotherloanTypes();
			//if (IsIRR)
			//{
			//    foreach (var model in projectActionList)
			//    {
			//        AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
			//        {
			//            Text = model.ProjectTypeName,
			//            Value = model.ProjectTypeId.ToString()
			//        });
			//    }
			//}

			AppProcessViewModel.LenderId = UserPrincipal.Current.UserData.LenderId.Value;
			AppProcessViewModel.LenderName = projectActionFormManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
			AppProcessViewModel.RequesterName = UserPrincipal.Current.FullName;
			AppProcessViewModel.ProjectActionDate = DateTime.Today;
			AppProcessViewModel.ServicerSubmissionDate = null;
			AppProcessViewModel.IsAgreementAccepted = false;
			AppProcessViewModel.RequestDate = DateTime.UtcNow;
			AppProcessViewModel.CreatedBy = UserPrincipal.Current.UserId;
			AppProcessViewModel.CreatedOn = DateTime.UtcNow;
			//Initiate the new grouptaskinstanceid
			AppProcessViewModel.GroupTaskInstanceId = Guid.NewGuid();
			AppProcessViewModel.ModifiedOn = DateTime.UtcNow;
			AppProcessViewModel.ModifiedBy = UserPrincipal.Current.UserId;
			IList<TaskFileModel> TasFilelist = new List<TaskFileModel>();
			AppProcessViewModel.TaskFileList = TasFilelist;
			AppProcessViewModel.IsViewFromGroupTask = false;
			List<OPAHistoryViewModel> History = new List<OPAHistoryViewModel>();
			AppProcessViewModel.OpaHistoryViewModel = History;
			GetDisclaimerText(AppProcessViewModel);

		}
		public ActionResult Index()
		{
			OPAViewModel AppProcessViewModel = new OPAViewModel();
			AppProcessViewModel.IsIRRequest = false;
			AppProcessViewModel.Isotherloantype = false;
			AppProcessViewModel.FormName = "Amendments";
			AppProcessViewModel.pageTypeId = (int)PageType.Amendments;
			InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
			AppProcessViewModel.AvailableFHANumbersList = prod_NextStageManager.GetFhasforNxtStage((int)PageType.Amendments, (int)UserPrincipal.Current.LenderId);
			ViewBag.ConstructionTitle = "Amendments";
			Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
			AppProcessViewModel.TransAccessStatus = checkResult.status;
			return View("~/Views/Production/Amendments/Index.cshtml", AppProcessViewModel);
		}

		[ValidateInput(false)]
		public ActionResult Reload()
		{
			OPAViewModel AppProcessViewModel = new OPAViewModel();

			string GroupTaskInstanceId = Request.Params[0];
			AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));

			var result = projectActionFormManager.GetProdPropertyInfo(AppProcessViewModel.FhaNumber);
			if (result != null)
			{
				AppProcessViewModel.PropertyId = result.PropertyId;
				AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
				AppProcessViewModel.PropertyAddress.StateCode = result.State;
				AppProcessViewModel.PropertyAddress.City = result.City;
				AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
			}
			AppProcessViewModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
			AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectTypebyID(AppProcessViewModel.ProjectActionTypeId);

			var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(new Guid(GroupTaskInstanceId));
			if (grouptaskmodel != null)
			{
				AppProcessViewModel.ServicerComments = grouptaskmodel.ServicerComments;
				AppProcessViewModel.pageTypeId = grouptaskmodel.PageTypeId;
			}
			GetDisclaimerText(AppProcessViewModel);

			if (TempData["UploadFailed"] != null)
			{
				ViewBag.UploadFailed = TempData["UploadFailed"].ToString();
			}

			if (AppProcessViewModel.pageTypeId == (int)PageType.Amendments)
			{
				if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
				{
					AppProcessViewModel.AvailableFHANumbersList = fhaRequestManager.GetReadyforConTwoStageFhas();
				}
				ViewBag.ConstructionTitle = "Amendments";
				return View("~/Views/Production/Amendments/Index.cshtml", AppProcessViewModel);
			}
			else
			{
				/* Waivers to be implemented in this section */
				return View("~/Views/Production/Amendments/Index.cshtml", AppProcessViewModel);
			}
		}

		public JsonResult GetProdLenderUploadFileGridContent(string sidx, string sord, int page, int rows, Guid? taskInstanceId, string PropertyId, string FhaNumber, string FormName)
		{
			if (taskInstanceId == null)
			{
				taskInstanceId = Guid.Empty;
			}

			var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId.Value, PropertyId, FhaNumber);
            GridContent = GridContent.Where(x => x.id != "39" && x.id != "40" && x.id != "41").ToList();
            //to delete duplicate records
            GridContent = GridContent.GroupBy(o => new { o.fileId }).Select(o => o.FirstOrDefault()).ToList();

            if (GridContent != null)
			{
				var currentUserId = UserPrincipal.Current.UserId;
				var currentUserRole = UserPrincipal.Current.UserRole;
				foreach (var item in GridContent)
				{
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    if (item.fileId != null)
					{
						var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
						if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
														|| currentUserRole == "BackupAccountManager")
							item.isUploadedByMe = true;
					}
					//if (!string.IsNullOrEmpty(item.parent))
					//{
					//var DocTypeList = appProcessManager.GetMappedDocTypesbyFolder(Convert.ToInt32(item.parent));

					//if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
					//{

					// item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
					//}
					//}
					else
					{
						if (item.SubfolderSequence != null)
						{
							item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
						}
					}
				}
			}

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalrecods = GridContent.Count();
			var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
			var result = GridContent.ToList();//.OrderBy(a => a.id);
			var results = result.Skip(pageIndex * pageSize).Take(pageSize);
			var jsonData = new
			{
				total = totalpages,
				page,
				records = totalrecods,
				rows = results.OrderBy(x=>x.uploadDate),

			};
			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		//[ValidateInput(false)]
		//private bool Upload(OPAViewModel AppProcessViewModel, int FolderKey)
		//{
		//    var FolderInfo = projectActionFormManager.GetFolderInfoByKey(FolderKey);
		//    var PageName = projectActionFormManager.FormNameBYPageTypeID(AppProcessViewModel.pageTypeId);
		//    string folderNames = string.Empty;
		//    string formNameConfiguration = ConfigurationManager.AppSettings["FORM 290"] ?? ConfigurationSettings.AppSettings["FORM 290"];
		//    int form290PageId = !string.IsNullOrEmpty(formNameConfiguration) ? Convert.ToInt32(formNameConfiguration) : 0;
		//    if (FolderInfo != null)
		//    {
		//        foreach (Prod_SubFolderStructureModel folder in FolderInfo)
		//        {


		//            if (AppProcessViewModel.pageTypeId == form290PageId)//skumar-form290 - folder map 
		//            {
		//                folderNames = "/" + AppProcessViewModel.PropertyId.ToString() + "/" + AppProcessViewModel.FhaNumber + "/Production/ExecutedClosing/WorkProduct/Form290/";
		//            }
		//            else if (FolderInfo[0].ViewTypeId == 2)
		//            {
		//                folderNames = "Production/" + PageName + "/" + "WorkProduct/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());

		//            }
		//            else if (AppProcessViewModel.pageTypeId == 18)//skumar-form290 - folder map 
		//            {
		//                folderNames = "Production/" + PageName + "/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
		//            }
		//            else
		//            {
		//                //Consider the case where there is no SubfolderSequence
		//                folderNames = "Production/" + PageName + "/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
		//            }

		//        }

		//    }
		//    var uploadmodel = new RestfulWebApiUploadModel()
		//    {
		//        propertyID = AppProcessViewModel.PropertyId.ToString(),
		//        indexType = "1",
		//        indexValue = AppProcessViewModel.FhaNumber,
		//        //pdfConvertableValue = "false",
		//        pdfConvertableValue = "false",
		//        folderKey = FolderKey,
		//        folderNames = folderNames
		//    };
		//    var WebApiUploadResult = new RestfulWebApiResultModel();
		//    var request = new RestfulWebApiTokenResultModel();

		//    request = WebApiTokenRequest.RequestToken();

		//    bool isUploaded = false;


		//    foreach (string Filename in Request.Files)
		//    {
		//        // var TEMP = Request.Files[Filename];
		//        //HttpPostedFileBase Temp;

		//        HttpPostedFileBase myFile = Request.Files[Filename];
		//        var t = myFile.InputStream;
		//        if (myFile != null && myFile.ContentLength != 0)
		//        {
		//            double fileSize = (myFile.ContentLength) / 1024;
		//            Random randoms = new Random();

		//            var UniqueId = randoms.Next(0, 99999);

		//            var systemFileName = AppProcessViewModel.FhaNumber + "_" +
		//                                 AppProcessViewModel.ProjectActionTypeId + "_" +
		//                                 AppProcessViewModel.GroupTaskInstanceId + "_" + UniqueId +
		//                                 Path.GetExtension(myFile.FileName);

		//            string pathForSaving = Server.MapPath("~/Uploads");
		//            if (this.CreateFolderIfNeeded(pathForSaving))
		//            {
		//                try
		//                {

		//                    // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
		//                    myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
		//                    isUploaded = true;

		//                }
		//                catch (Exception ex)
		//                {
		//                    //message = string.Format("File upload failed: {0}", ex.Message);
		//                }
		//            }
		//            WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");
		//            System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));

		//            if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
		//            {

		//                isUploaded = true;
		//                var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(AppProcessViewModel.GroupTaskInstanceId.ToString()));
		//                taskFile.DocId = WebApiUploadResult.docId;
		//                taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
		//                taskFile.API_upload_status = WebApiUploadResult.status;
		//                if (WebApiUploadResult.documentType != DefaultDocID)
		//                {
		//                    taskFile.DocTypeID = WebApiUploadResult.documentType;
		//                }
		//                else if (FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductClosing) ||
		//                        FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductUnderwriting) ||
		//                        FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductLoanCommittee))
		//                {
		//                    //taskFile.DocTypeID = DefaultDocID;
		//                    taskFile.FileType = FileType.WP;
		//                }

		//                //skumar-form290- get role name
		//                if (AppProcessViewModel.pageTypeId == form290PageId)//skumar-form290 - get role name 
		//                {
		//                    if (Session["UserRole"] != null)
		//                        taskFile.RoleName = (Session["UserRole"]).ToString();
		//                    //var a = accountManager.GetFullUserNameById(AppProcessViewModel.userId);
		//                    //taskFile.CreatedBy = AppProcessViewModel.userId;                            
		//                }

		//                var taskfileid = taskManager.SaveGroupTaskFileModel(taskFile);
		//                var mappingmodel = new TaskFile_FolderMappingModel();
		//                mappingmodel.FolderKey = FolderKey;
		//                mappingmodel.TaskFileId = taskfileid;
		//                mappingmodel.CreatedOn = DateTime.UtcNow;

		//                taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);

		//                //Code for saving new files on RAI
		//                if (AppProcessViewModel.TaskGuid != null)
		//                {
		//                    var childTaskNewFile = new ChildTaskNewFileModel();
		//                    childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
		//                    childTaskNewFile.ChildTaskInstanceId = AppProcessViewModel.TaskGuid.Value;
		//                    childTaskNewFile.TaskFileInstanceId = taskfileid;
		//                    projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);

		//                }
		//            }
		//            else
		//            {
		//                Exception ex = new Exception(WebApiUploadResult.message);
		//                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
		//                System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
		//                isUploaded = false;
		//            }


		//        }
		//    }
		//    return isUploaded;
		//}
		private void GetDisclaimerText(OPAViewModel model)
		{
			if (model.pageTypeId == 18)
			{
				model.FormName = "AMENDMENTS";
			}
			model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType(model.FormName.Trim());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pTaskxrefId"></param>
		/// <param name="pTaskInstanceId"></param>
		/// <returns></returns>
		public PartialViewResult AttachDocSignature(string pTaskxrefId = null, string pTaskInstanceId = null)
		{
			List<TaskModel> lstTaskModel = new List<TaskModel>();
			int userId = UserPrincipal.Current.UserId;
			string pathForSaving = Server.MapPath("~/Templates/Sig");
			HUDSignatureModel objHUDSignatureModel = new HUDSignatureModel();
			Prod_LoanCommitteeViewModel objProd_LoanCommitteeViewModel = new Prod_LoanCommitteeViewModel();
			Prod_FormAmendmentTaskModel objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
			objHUDSignatureModel.UserId = userId;
			objHUDSignatureModel = projectActionFormManager.GetHUDSignatureByUserId(userId);
			bool isFolder = ControllerHelper.CreateFolderIfNeeded(pathForSaving);
			//test taskinstanceid
			if (RoleManager.IsProductionUser(UserPrincipal.Current.UserName))
			{
				var olistProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentByTaskInstanceId(new Guid(pTaskInstanceId));
				if (olistProd_FormAmendmentTaskModel != null && olistProd_FormAmendmentTaskModel.Count > 0)
					objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.Where(o => o.CreatedBy == userId).FirstOrDefault();
				var objTaskModel = taskManager.GetTasksByTaskInstanceId(new Guid(pTaskInstanceId)).Where(o => o.PageTypeId == (int)PageType.Amendments);
				if (objProd_FormAmendmentTaskModel != null && string.IsNullOrEmpty(objProd_FormAmendmentTaskModel.FHANumber))
				{
					objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
					objProd_FormAmendmentTaskModel.TaskId = objTaskModel.OrderByDescending(o => o.SequenceId).FirstOrDefault().TaskId;
					//auto populate (new)
					//var objTaskModel = taskManager.GetTasksByTaskInstanceId(new Guid(pTaskInstanceId));
					var model = fhaRequestManager.GetFhaRequestByFhaNumber(objTaskModel.FirstOrDefault().FHANumber);
					var propertyInfoModel = projectActionFormManager.GetProdPropertyInfo(objTaskModel.FirstOrDefault().FHANumber);
					if (propertyInfoModel != null)
					{
						StringBuilder sb = new StringBuilder();
						sb.AppendFormat("{0} {1} {2} {3}", propertyInfoModel.StreetAddress, propertyInfoModel.City, propertyInfoModel.State, propertyInfoModel.Zipcode);
						string address = sb.ToString();
						objProd_FormAmendmentTaskModel.ProjectName = propertyInfoModel.PropertyName;
						objProd_FormAmendmentTaskModel.ProjectAddress = address;
						objProd_FormAmendmentTaskModel.PropertyId = propertyInfoModel.PropertyId;
					}
					objProd_FormAmendmentTaskModel.FHANumber = objTaskModel.FirstOrDefault().FHANumber;
					objProd_LoanCommitteeViewModel = Prod_LoanCommitteeManager.GetLoanCommitteeDetailByFha(objProd_FormAmendmentTaskModel.FHANumber);
					if (objProd_LoanCommitteeViewModel != null && objProd_LoanCommitteeViewModel.LoanAmount > 0)
						objProd_FormAmendmentTaskModel.LoanAmount = objProd_LoanCommitteeViewModel.LoanAmount;
					//lender contact name
					objProd_FormAmendmentTaskModel.LastName = !string.IsNullOrEmpty(model.LenderContactName) ? model.LenderContactName : string.Empty;

					//Loan amount
					if (objProd_FormAmendmentTaskModel.LoanAmount < 1)
						objProd_FormAmendmentTaskModel.LoanAmount = model.LoanAmount > 0 ? model.LoanAmount : 0.00M;
					//interest rate
					objProd_FormAmendmentTaskModel.InterestRate = model.ProposedInterestRate > 0 ? model.ProposedInterestRate : 0.00M;
					//Lender company name
					objProd_FormAmendmentTaskModel.LenderCompanyName = ControllerHelper.GetLenderCompanyName(model.LenderId);
					//Update lender last name
					string[] ssize = objProd_FormAmendmentTaskModel.LastName.Split(null);
					if (ssize.Length >= 2)
						objProd_FormAmendmentTaskModel.LastName = ssize[ssize.Length - 1].ToString();
					//AmendmentIssuedDate
					lstTaskModel = taskManager.GetTasksByTaskInstanceId(new Guid(pTaskInstanceId)).ToList();
					objProd_FormAmendmentTaskModel.FirmAmendmentIssueDate = lstTaskModel.Where(o => o.PageTypeId == (int)PageType.Amendments).OrderBy(o => o.StartTime).First().StartTime;
					//Firm Commitment issue date
					objProd_FormAmendmentTaskModel.FirmCommitmentSignedDate = ControllerHelper.GetFirmCommitmentIssuedDate(new Guid(pTaskInstanceId));

					//if (ssize.Length == 2)
					//{
					//	objProd_FormAmendmentTaskModel.LastName = ssize[1].ToString();
					//}
					//else
					//{
					//	objProd_FormAmendmentTaskModel.LastName = accountManager.GetUserLastNameByFirstName(ssize[0].ToString());
					//}
					objProd_FormAmendmentTaskModel.FirmAmendmentNum = ControllerHelper.GetNewFirmAmendmentNum(objProd_FormAmendmentTaskModel.FHANumber);
				}
				else
				{
					objProd_FormAmendmentTaskModel.TaskId = objTaskModel.OrderByDescending(o => o.SequenceId).FirstOrDefault().TaskId;
					var propertyInfoModel = projectActionFormManager.GetProdPropertyInfo(objTaskModel.FirstOrDefault().FHANumber);
					if (propertyInfoModel != null)
					{
						StringBuilder sb = new StringBuilder();
						sb.AppendFormat("{0} {1} {2} {3}", propertyInfoModel.StreetAddress, propertyInfoModel.City, propertyInfoModel.State, propertyInfoModel.Zipcode);
						string address = sb.ToString();
						objProd_FormAmendmentTaskModel.ProjectName = propertyInfoModel.PropertyName;
						objProd_FormAmendmentTaskModel.ProjectAddress = address;
						objProd_FormAmendmentTaskModel.PropertyId = propertyInfoModel.PropertyId;
					}
					//AmendmentIssuedDate
					lstTaskModel = taskManager.GetTasksByTaskInstanceId(new Guid(pTaskInstanceId)).ToList();
					objProd_FormAmendmentTaskModel.FirmAmendmentIssueDate = lstTaskModel.Where(o => o.PageTypeId == (int)PageType.Amendments).OrderBy(o => o.StartTime).First().StartTime;
					//Firm Commitment issue date
					objProd_FormAmendmentTaskModel.FirmCommitmentSignedDate = ControllerHelper.GetFirmCommitmentIssuedDate(new Guid(pTaskInstanceId));

				}
				objProd_FormAmendmentTaskModel.RoleType = HUDRole.ProductionUser.ToString();
				objProd_FormAmendmentTaskModel.TaskxrefIdUI = pTaskxrefId;
				objProd_FormAmendmentTaskModel.TaskInstanceIdUI = pTaskInstanceId;
				objProd_FormAmendmentTaskModel.CurrentSignature = objHUDSignatureModel.Signature;
				//objProd_FormAmendmentTaskModel.WLMCurrentSignature = objHUDSignatureModel.Signature;
				objProd_FormAmendmentTaskModel.CreatedBy = userId;
                //objProd_FormAmendmentTaskModel.AgentSignatureFileName = userId.ToString() + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";
                objProd_FormAmendmentTaskModel.AgentSignatureFileName = userId.ToString() + DateTime.UtcNow.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";

            }
			if (RoleManager.IsProductionWLM(UserPrincipal.Current.UserName))
			{
				var olistProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentByTaskInstanceId(new Guid(pTaskInstanceId));
				if (olistProd_FormAmendmentTaskModel != null && olistProd_FormAmendmentTaskModel.Count > 1)
				{
					objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.Where(o => o.CreatedBy == userId).FirstOrDefault();
				}
				else if (olistProd_FormAmendmentTaskModel != null && olistProd_FormAmendmentTaskModel.Count == 1)
				{
					objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.FirstOrDefault();
					objProd_FormAmendmentTaskModel.CreatedBy = userId;
				}
				var objTaskModel = taskManager.GetTasksByTaskInstanceId(new Guid(pTaskInstanceId)).Where(o => o.PageTypeId == (int)PageType.Amendments);
				//if (objProd_FormAmendmentTaskModel == null )
				if (objProd_FormAmendmentTaskModel != null && string.IsNullOrEmpty(objProd_FormAmendmentTaskModel.FHANumber))
				{
					objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
					//get information entered by closer
					objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.FirstOrDefault();
					if (objProd_FormAmendmentTaskModel == null)
						objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
					var model = fhaRequestManager.GetFhaRequestByFhaNumber(objTaskModel.FirstOrDefault().FHANumber);
					var propertyInfoModel = projectActionFormManager.GetProdPropertyInfo(objTaskModel.FirstOrDefault().FHANumber);
					if (propertyInfoModel != null)
					{
						StringBuilder sb = new StringBuilder();
						sb.AppendFormat("{0} {1} {2} {3}", propertyInfoModel.StreetAddress, propertyInfoModel.City, propertyInfoModel.State, propertyInfoModel.Zipcode);
						string address = sb.ToString();
						objProd_FormAmendmentTaskModel.ProjectName = propertyInfoModel.PropertyName;
						objProd_FormAmendmentTaskModel.ProjectAddress = address;
						objProd_FormAmendmentTaskModel.PropertyId = propertyInfoModel.PropertyId;
					}
					objProd_FormAmendmentTaskModel.TaskId = objTaskModel.OrderByDescending(o => o.SequenceId).FirstOrDefault().TaskId;
					objProd_FormAmendmentTaskModel.FHANumber = objTaskModel.FirstOrDefault().FHANumber;
					objProd_LoanCommitteeViewModel = Prod_LoanCommitteeManager.GetLoanCommitteeDetailByFha(objProd_FormAmendmentTaskModel.FHANumber);
					if (objProd_LoanCommitteeViewModel != null && objProd_LoanCommitteeViewModel.LoanAmount > 0)
						objProd_FormAmendmentTaskModel.LoanAmount = objProd_LoanCommitteeViewModel.LoanAmount;

					//lender contact name
					objProd_FormAmendmentTaskModel.LastName = !string.IsNullOrEmpty(model.LenderContactName) ? model.LenderContactName : string.Empty;

					//Loan amount
					if (objProd_FormAmendmentTaskModel.LoanAmount < 1)
						objProd_FormAmendmentTaskModel.LoanAmount = model.LoanAmount > 0 ? model.LoanAmount : 0.00M;
					//interest rate
					objProd_FormAmendmentTaskModel.InterestRate = model.ProposedInterestRate > 0 ? model.ProposedInterestRate : 0.00M;
					//Lender company name
					objProd_FormAmendmentTaskModel.LenderCompanyName = ControllerHelper.GetLenderCompanyName(model.LenderId);
					//Update lender last name
					string[] ssize = objProd_FormAmendmentTaskModel.LastName.Split(null);
					if (ssize.Length >= 2)
						objProd_FormAmendmentTaskModel.LastName = ssize[ssize.Length - 1].ToString();
					//AmendmentIssuedDate
					lstTaskModel = taskManager.GetTasksByTaskInstanceId(new Guid(pTaskInstanceId)).ToList();
					objProd_FormAmendmentTaskModel.FirmAmendmentIssueDate = lstTaskModel.Where(o => o.PageTypeId == (int)PageType.Amendments).OrderBy(o => o.StartTime).First().StartTime;
					//Firm Commitment issue date
					objProd_FormAmendmentTaskModel.FirmCommitmentSignedDate = ControllerHelper.GetFirmCommitmentIssuedDate(new Guid(pTaskInstanceId));
					objProd_FormAmendmentTaskModel.FirmAmendmentNum = ControllerHelper.GetNewFirmAmendmentNum(objProd_FormAmendmentTaskModel.FHANumber);

				}
				else
				{
					objProd_FormAmendmentTaskModel.TaskId = objTaskModel.OrderByDescending(o => o.SequenceId).FirstOrDefault().TaskId;
					var propertyInfoModel = projectActionFormManager.GetProdPropertyInfo(objTaskModel.FirstOrDefault().FHANumber);
					if (propertyInfoModel != null)
					{
						StringBuilder sb = new StringBuilder();
						sb.AppendFormat("{0} {1} {2} {3}", propertyInfoModel.StreetAddress, propertyInfoModel.City, propertyInfoModel.State, propertyInfoModel.Zipcode);
						string address = sb.ToString();
						objProd_FormAmendmentTaskModel.ProjectName = propertyInfoModel.PropertyName;
						objProd_FormAmendmentTaskModel.ProjectAddress = address;
						objProd_FormAmendmentTaskModel.PropertyId = propertyInfoModel.PropertyId;
					}
					if (string.IsNullOrEmpty(objProd_FormAmendmentTaskModel.FHANumber))
						objProd_FormAmendmentTaskModel.FHANumber = objTaskModel.FirstOrDefault().FHANumber;
					if (objProd_FormAmendmentTaskModel.LoanAmount == 0)
					{
						objProd_LoanCommitteeViewModel = Prod_LoanCommitteeManager.GetLoanCommitteeDetailByFha(objProd_FormAmendmentTaskModel.FHANumber);
						if (objProd_LoanCommitteeViewModel != null)
							objProd_FormAmendmentTaskModel.LoanAmount = objProd_LoanCommitteeViewModel.LoanAmount;
					}

					//AmendmentIssuedDate
					lstTaskModel = taskManager.GetTasksByTaskInstanceId(new Guid(pTaskInstanceId)).ToList();
					objProd_FormAmendmentTaskModel.FirmAmendmentIssueDate = lstTaskModel.Where(o => o.PageTypeId == (int)PageType.Amendments).OrderBy(o => o.StartTime).First().StartTime;
					//Firm Commitment issue date
					objProd_FormAmendmentTaskModel.FirmCommitmentSignedDate = ControllerHelper.GetFirmCommitmentIssuedDate(new Guid(pTaskInstanceId));


				}
				objProd_FormAmendmentTaskModel.RoleType = HUDRole.ProductionWlm.ToString();

				objProd_FormAmendmentTaskModel.TaskxrefIdUI = pTaskxrefId;
				objProd_FormAmendmentTaskModel.TaskInstanceIdUI = pTaskInstanceId;
				//objProd_FormAmendmentTaskModel.CurrentSignature = objHUDSignatureModel.Signature;
				objProd_FormAmendmentTaskModel.WLMCurrentSignature = objHUDSignatureModel.Signature;
				objProd_FormAmendmentTaskModel.CreatedBy = userId;
				//objProd_FormAmendmentTaskModel.AgentSignatureFileName = userId.ToString() + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";



				//signatures
				var objModel = prod_TaskXrefManager.GetProductionSubtaskById(new Guid(pTaskxrefId));
				if (objModel != null)
				{
					objHUDSignatureModel = projectActionFormManager.GetHUDSignatureByUserId(objModel.AssignedBy.Value);
					objProd_FormAmendmentTaskModel.CurrentSignature = objHUDSignatureModel.Signature;
					objProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId = objModel.AssignedBy.Value;
					objProd_FormAmendmentTaskModel.WLMSignatureId = userId;
					objHUDSignatureModel = projectActionFormManager.GetHUDSignatureByUserId(userId);
					objProd_FormAmendmentTaskModel.WLMCurrentSignature = objHUDSignatureModel.Signature;
                    //objProd_FormAmendmentTaskModel.AgentSignatureFileName = objProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId.ToString() + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";
                    objProd_FormAmendmentTaskModel.AgentSignatureFileName = objProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId.ToString() + DateTime.UtcNow.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";
                    //objProd_FormAmendmentTaskModel.WLMSignatureFileName = userId.ToString() + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";
                    objProd_FormAmendmentTaskModel.WLMSignatureFileName = userId.ToString() + DateTime.UtcNow.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";

                }
			}

			//popuplate lookups
			objProd_FormAmendmentTaskModel.FirmCommitmentAmendmentTypes = projectActionFormManager.GetAllFirmCommitmentAmendmentTypes();
			objProd_FormAmendmentTaskModel.SaluatationTypes = projectActionFormManager.GetAllSaluatationTypes();
			objProd_FormAmendmentTaskModel.PartyPropertyAddressTypes = projectActionFormManager.GetAllPartyTypes();
			objProd_FormAmendmentTaskModel.PartyPropertyNameTypes = projectActionFormManager.GetAllPartyTypes();
			objProd_FormAmendmentTaskModel.AdditionalInitialDepositTypes = projectActionFormManager.GetAllDepositTypes();
			objProd_FormAmendmentTaskModel.EscrowEstimateTypes = projectActionFormManager.GetAllEscrowEstimateTypes();
			objProd_FormAmendmentTaskModel.RepairCostEstimateTypes = projectActionFormManager.GetAllRepairCostEstimateTypes();
			objProd_FormAmendmentTaskModel.SpecialConditionTypes = projectActionFormManager.GetAllSpecialConditionTypes();
			objProd_FormAmendmentTaskModel.ExhibitLetterTypes = projectActionFormManager.GetAllExhibitLetterTypes();
			return PartialView("~/Views/Production/Amendments/_AttachAmendments.cshtml", objProd_FormAmendmentTaskModel);

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pProd_FormAmendmentTaskModel"></param>
		/// <returns></returns>
		public ActionResult AmendmentsTemplate(Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel)
		{
			//pProd_FormAmendmentTaskModel.AgentPictureLocation = "~/Uploads/HUD/10.png";
			pProd_FormAmendmentTaskModel.AgentPictureLocation = "~/Templates/Sig/" + pProd_FormAmendmentTaskModel.AgentSignatureFileName;
			pProd_FormAmendmentTaskModel.WlmPictureLocation = "~/Templates/Sig/" + pProd_FormAmendmentTaskModel.WLMSignatureFileName;

			if (!string.IsNullOrEmpty(pProd_FormAmendmentTaskModel.Party_Propert_Name))
				pProd_FormAmendmentTaskModel.Party_Propert_Name_cb = true;
			if (!string.IsNullOrEmpty(pProd_FormAmendmentTaskModel.Party_Property_Address_Name))
				pProd_FormAmendmentTaskModel.Party_Property_Address_Name_cb = true;
			//if (pProd_FormAmendmentTaskModel.LoanAmount >0)
			//	pProd_FormAmendmentTaskModel.LoanAmount_cb = true;
			//if (pProd_FormAmendmentTaskModel.InterestRate > 0)
			//	pProd_FormAmendmentTaskModel.InterestRate_cb = true;
			//if (pProd_FormAmendmentTaskModel.MonthlyPayment > 0)
			//	pProd_FormAmendmentTaskModel.MonthlyPayment_cb = true;
			//if (pProd_FormAmendmentTaskModel.DifferentMonthlyPayment > 0)
			//	pProd_FormAmendmentTaskModel.DifferentMonthlyPayment_cb = true;
			//if (pProd_FormAmendmentTaskModel.CommitmentTerminationDate.HasValue)
			//	pProd_FormAmendmentTaskModel.CommitmentTerminationDate_cb = true;
			//if (pProd_FormAmendmentTaskModel.R4RAmount > 0)
			//	pProd_FormAmendmentTaskModel.R4RAmount_cb = true;
			//if (pProd_FormAmendmentTaskModel.CriticalRepairCost > 0)
			//	pProd_FormAmendmentTaskModel.CriticalRepairCost_cb = true;
			//if (pProd_FormAmendmentTaskModel.RemainingRepairCostExihibitC > 0)
			//	pProd_FormAmendmentTaskModel.RemainingRepairCostExihibitC_cb = true;
			//if (pProd_FormAmendmentTaskModel.SpecialCondition > 0)
			//	pProd_FormAmendmentTaskModel.SpecialCondition_cb = true;
			//if (pProd_FormAmendmentTaskModel.AnnualLeasePayment > 0)
			//	pProd_FormAmendmentTaskModel.AnnualLeasePayment_cb = true;
			//if (!string.IsNullOrEmpty(pProd_FormAmendmentTaskModel.Other))
			//	pProd_FormAmendmentTaskModel.Other_cb = true;
			//if (pProd_FormAmendmentTaskModel.ExhibitLetter > 0)
			//	pProd_FormAmendmentTaskModel.ExhibitLetter_cb = true;

			return PartialView("~/Views/Production/Amendments/AmendmentsTemplate.cshtml", pProd_FormAmendmentTaskModel);
			//return View(pProd_FormAmendmentTaskModel);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pFormData"></param>
		/// <param name="pProd_FormAmendmentTaskModel"></param>
		/// <returns></returns>

		[HttpPost]
		public ActionResult AttachDocSignature(System.Web.Mvc.FormCollection pFormData, Prod_FormAmendmentTaskModel pProd_FormAmendmentTaskModel)
		{
			DateTime today = DateTime.Today;
			string agentName = string.Empty;
			string role = string.Empty;
			int userId = UserPrincipal.Current.UserId;
			HUDSignatureModel objHUDSignatureModel = new HUDSignatureModel();
			UserViewModel objUserViewModel = accountManager.GetUserById(UserPrincipal.Current.UserId);
			StringBuilder sb = new StringBuilder();
			ViewAsPdf generatePdf;
			string sigPathForSaving = Server.MapPath("~/Templates/Sig/");
			string fileNameWitPath = string.Empty;
			sb.AppendFormat("{0} {1} {2}", objUserViewModel.FirstName, (!string.IsNullOrEmpty(objUserViewModel.MiddleName) ? objUserViewModel.MiddleName : string.Empty), objUserViewModel.LastName);
			agentName = sb.ToString();
			if (!string.IsNullOrEmpty(pProd_FormAmendmentTaskModel.TaskInstanceIdUI))
				pProd_FormAmendmentTaskModel.TaskInstanceId = new Guid(pProd_FormAmendmentTaskModel.TaskInstanceIdUI);
			if (!string.IsNullOrEmpty(pProd_FormAmendmentTaskModel.TaskxrefIdUI))
				pProd_FormAmendmentTaskModel.TaskXrefid = new Guid(pProd_FormAmendmentTaskModel.TaskxrefIdUI);

			if (pProd_FormAmendmentTaskModel.CommitmentTerminationDate == null)
				pProd_FormAmendmentTaskModel.CommitmentTerminationDate = today;
			if (pProd_FormAmendmentTaskModel.FirmAmendmentIssueDate == null)
				pProd_FormAmendmentTaskModel.FirmAmendmentIssueDate = today;
			if (pProd_FormAmendmentTaskModel.FirmCommitmentSignedDate == null)
				pProd_FormAmendmentTaskModel.FirmCommitmentSignedDate = today;
			//get signature model for any users(production user/preodcutionwlm)
			objHUDSignatureModel = projectActionFormManager.GetHUDSignatureByUserId(userId);




			if (RoleManager.IsProductionUser(UserPrincipal.Current.UserName))
			{
				role = HUDRole.ProductionUser.ToString();
				pProd_FormAmendmentTaskModel.CreatedBy = userId;
				pProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId = userId;
				pProd_FormAmendmentTaskModel.AuthorizedAgent = agentName;
				pProd_FormAmendmentTaskModel.CreatedOn = DateTime.UtcNow;
				pProd_FormAmendmentTaskModel.ModifiedOn = DateTime.UtcNow;
				pProd_FormAmendmentTaskModel.CurrentSignature = objHUDSignatureModel.Signature;
				fileNameWitPath = sigPathForSaving + pProd_FormAmendmentTaskModel.AgentSignatureFileName;// userSigFile;
				ControllerHelper.WriterSignatureFile(fileNameWitPath, objHUDSignatureModel.Signature);
				pProd_FormAmendmentTaskModel.AgentPictureLocation = "~/Templates/Sig/" + pProd_FormAmendmentTaskModel.AgentSignatureFileName;
			}
			if (RoleManager.IsProductionWLM(UserPrincipal.Current.UserName))
			{
				role = HUDRole.ProductionWlm.ToString();
				//get agent signature 
				objUserViewModel = accountManager.GetUserById(pProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId);
				pProd_FormAmendmentTaskModel.WLMSignature = objHUDSignatureModel.Signature;
				objHUDSignatureModel = projectActionFormManager.GetHUDSignatureByUserId(pProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId);
				pProd_FormAmendmentTaskModel.CurrentSignature = objHUDSignatureModel.Signature;
				fileNameWitPath = sigPathForSaving + pProd_FormAmendmentTaskModel.AgentSignatureFileName;// userSigFile;
				ControllerHelper.WriterSignatureFile(fileNameWitPath, pProd_FormAmendmentTaskModel.CurrentSignature);
				pProd_FormAmendmentTaskModel.WLM = agentName;
				sb = new StringBuilder();
				sb.AppendFormat("{0} {1} {2}", objUserViewModel.FirstName, (!string.IsNullOrEmpty(objUserViewModel.MiddleName) ? objUserViewModel.MiddleName : string.Empty), objUserViewModel.LastName);
				agentName = sb.ToString();
				pProd_FormAmendmentTaskModel.AuthorizedAgent = agentName;

				fileNameWitPath = sigPathForSaving + pProd_FormAmendmentTaskModel.WLMSignatureFileName;// userSigFile;
				ControllerHelper.WriterSignatureFile(fileNameWitPath, pProd_FormAmendmentTaskModel.WLMSignature);
				//pProd_FormAmendmentTaskModel.CreatedOn = DateTime.UtcNow;
				//pProd_FormAmendmentTaskModel.ModifiedOn = DateTime.UtcNow;
				pProd_FormAmendmentTaskModel.AgentPictureLocation = "~/Templates/Sig/" + pProd_FormAmendmentTaskModel.AgentSignatureFileName;
				pProd_FormAmendmentTaskModel.WlmPictureLocation = "~/Templates/Sig/" + pProd_FormAmendmentTaskModel.WLMSignatureFileName;

			}
			projectActionFormManager.SaveAmendment(pProd_FormAmendmentTaskModel);

			if (!string.IsNullOrEmpty(pProd_FormAmendmentTaskModel.Party_Propert_Name))
				pProd_FormAmendmentTaskModel.Party_Propert_Name_cb = true;
			if (!string.IsNullOrEmpty(pProd_FormAmendmentTaskModel.Party_Property_Address_Name))
				pProd_FormAmendmentTaskModel.Party_Property_Address_Name_cb = true;
			if (pProd_FormAmendmentTaskModel.LoanAmount > 0)
				pProd_FormAmendmentTaskModel.LoanAmount_cb = true;
			if (pProd_FormAmendmentTaskModel.InterestRate > 0)
				pProd_FormAmendmentTaskModel.InterestRate_cb = true;
			if (pProd_FormAmendmentTaskModel.MonthlyPayment > 0)
				pProd_FormAmendmentTaskModel.MonthlyPayment_cb = true;
			if (pProd_FormAmendmentTaskModel.DifferentMonthlyPayment > 0)
				pProd_FormAmendmentTaskModel.DifferentMonthlyPayment_cb = true;
			if (pProd_FormAmendmentTaskModel.CommitmentTerminationDate.HasValue)
				pProd_FormAmendmentTaskModel.CommitmentTerminationDate_cb = true;
			if (pProd_FormAmendmentTaskModel.R4RAmount > 0)
				pProd_FormAmendmentTaskModel.R4RAmount_cb = true;
			if (pProd_FormAmendmentTaskModel.CriticalRepairCost > 0)
				pProd_FormAmendmentTaskModel.CriticalRepairCost_cb = true;
			if (pProd_FormAmendmentTaskModel.RemainingRepairCostExihibitC > 0)
				pProd_FormAmendmentTaskModel.RemainingRepairCostExihibitC_cb = true;
			if (pProd_FormAmendmentTaskModel.SpecialCondition > 0)
				pProd_FormAmendmentTaskModel.SpecialCondition_cb = true;
			if (pProd_FormAmendmentTaskModel.AnnualLeasePayment > 0)
				pProd_FormAmendmentTaskModel.AnnualLeasePayment_cb = true;
			if (!string.IsNullOrEmpty(pProd_FormAmendmentTaskModel.Other))
				pProd_FormAmendmentTaskModel.Other_cb = true;
			if (pProd_FormAmendmentTaskModel.ExhibitLetter > 0)
				pProd_FormAmendmentTaskModel.ExhibitLetter_cb = true;

			generatePdf = new ViewAsPdf("~/Views/Production/Amendments/AmendmentsTemplate.cshtml", pProd_FormAmendmentTaskModel);
			var binary = generatePdf.BuildPdf(ControllerContext);
			//System.IO.File.WriteAllBytes(Server.MapPath("~/Templates/Amndmnt_Template2.pdf"), binary);

			var uploadmodel = new RestfulWebApiUploadModel()
			{
				propertyID = pProd_FormAmendmentTaskModel.PropertyId.ToString(),
				indexType = "1",
				indexValue = pProd_FormAmendmentTaskModel.FHANumber,
				pdfConvertableValue = "false",
				documentType = DefaultDocID,
				folderNames = "/" + pProd_FormAmendmentTaskModel.PropertyId.ToString() + "/" + pProd_FormAmendmentTaskModel.FHANumber + "/Production/Amendments/WorkProduct/Amendments/",
			};
			var WebApiUploadResult = new RestfulWebApiResultModel();
			var request = new RestfulWebApiTokenResultModel();
			request = WebApiTokenRequest.RequestToken();

			var result = WebApiDocumentUpload.uploadSharepointPdfFile(uploadmodel, request.access_token, binary);
			Random randoms = new Random();
			var fileName = result.fileName == null ? "AmendmentsTemplate.pdf" : result.fileName;
			fileName = fileName.Replace("SharePointData", "AmendmentsTemplate");
			if (RoleManager.IsProductionWLM(UserPrincipal.Current.UserName))
				fileName = fileName.Replace("SharePointData", "WLMAmendmentsTemplate");

			var fileSize = binary.Length;
			var UniqueId = randoms.Next(0, 99999);
			var systemFileName = pProd_FormAmendmentTaskModel.FHANumber + "_" + pProd_FormAmendmentTaskModel.TaskInstanceIdUI + "_" + UniqueId + Path.GetExtension(fileName);
			System.IO.File.WriteAllBytes(Server.MapPath("~/Uploads/" + systemFileName), binary);

			var taskFile = ControllerHelper.PopulateGroupTaskFileForSharepoint(fileName, fileSize, "AmendmentsTemplate", DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(pProd_FormAmendmentTaskModel.TaskInstanceIdUI));
			if (result.status != null)
			{
				if (result.status.ToLower() == "success")
				{
					taskFile.API_upload_status = result.status;
					taskFile.Version = Convert.ToInt32(result.version);
					taskFile.DocId = result.docId;
					taskFile.DocTypeID = result.documentType;
					taskManager.SaveGroupTaskFileModel(taskFile);
					var mappingmodel = new TaskFile_FolderMappingModel();
					mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.AmendmentTemplate);
					mappingmodel.TaskFileId = taskFile.TaskFileId;
					mappingmodel.CreatedOn = DateTime.UtcNow;
					taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
				}

			}  //Store information locally and regenerate it agian
			else
			{
				taskFile.DocTypeID = DefaultDocID;
				taskManager.SaveGroupTaskFileModel(taskFile);
				var mappingmodel = new TaskFile_FolderMappingModel();
				mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.AmendmentTemplate);
				mappingmodel.TaskFileId = taskFile.TaskFileId;
				mappingmodel.CreatedOn = DateTime.UtcNow;
				taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
			}


			if (RoleManager.IsProductionUser(UserPrincipal.Current.UserName))
			{
				bool isInsert = prod_TaskXrefManager.FindTasXrefForAmendmentWLMView(pProd_FormAmendmentTaskModel.TaskXrefid.Value);
				if (isInsert)
				{
					//insert into tasksref for wlm signature
					//pProd_FormAmendmentTaskModel.TaskInstanceId
					//string TaskId = projectActionFormManager.GetTaskId(Convert.ToInt32(ApplicationAndClosingType.Amendments), pProd_FormAmendmentTaskModel.FHANumber, pProd_FormAmendmentTaskModel.TaskInstanceId.Value);
					Prod_TaskXrefModel model = new Prod_TaskXrefModel()
					{
						TaskXrefid = Guid.NewGuid(),
						TaskInstanceId = pProd_FormAmendmentTaskModel.TaskInstanceId.Value,
						TaskId = pProd_FormAmendmentTaskModel.TaskId,
						AssignedBy = UserPrincipal.Current.UserId,
						IsReviewer = false,
						Status = Convert.ToInt32(ApplicationAndClosingType.Amendments),
						AssignedDate = DateTime.UtcNow,
						ViewId = Convert.ToInt32(ProductionView.WLM),
						ModifiedOn = DateTime.UtcNow
					};
					prod_TaskXrefManager.AddTaskXref(model);
				}
			}

			if (RoleManager.IsProductionWLM(UserPrincipal.Current.UserName))
			{
				List<Guid> olistTaskXrefids = prod_TaskXrefManager.GetTasXrefForAmendmentWLMView(pProd_FormAmendmentTaskModel.TaskXrefid.Value);
				foreach (var taskXrefid in olistTaskXrefids)
				{
					prod_TaskXrefManager.UpdateTasXrefCompleteStatus(taskXrefid, "success");
				}

				//Create DAP
				Prod_TaskXrefModel objProd_TaskXrefModel = new Prod_TaskXrefModel()
				{
					TaskXrefid = Guid.NewGuid(),
					TaskInstanceId = pProd_FormAmendmentTaskModel.TaskInstanceId.Value,
					TaskId = pProd_FormAmendmentTaskModel.TaskId,
					AssignedBy = UserPrincipal.Current.UserId,
					AssignedTo = pProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId,
					IsReviewer = false,
					Status = Convert.ToInt32(ApplicationAndClosingType.Amendments),
					AssignedDate = DateTime.UtcNow,
					ViewId = Convert.ToInt32(ProductionView.DAP),
					ModifiedOn = DateTime.UtcNow
				};
				prod_TaskXrefManager.AddTaskXref(objProd_TaskXrefModel);
			}

			//delete the signature files
			ControllerHelper.DeleteFile(sigPathForSaving, pProd_FormAmendmentTaskModel.AgentSignatureFileName);
			ControllerHelper.DeleteFile(sigPathForSaving, pProd_FormAmendmentTaskModel.WLMSignatureFileName);

			return RedirectToAction("GetTaskDetail", new RouteValueDictionary(
				new { controller = "ProductionMyTask", action = "GetTaskDetail", taskInstanceId = pProd_FormAmendmentTaskModel.TaskxrefIdUI }));

		}


		//[HttpPost]
		//public ActionResult UploadSignatureImage(string imageData, string userId,  string userSigFile)
		//{
		//	int retSigId = 0;
		//	HUDSignatureModel objHUDSignatureModel = new HUDSignatureModel();
		//	HUDSignatureModel existingHUDSignatureModel = new HUDSignatureModel();
		//	byte[] data = null;
		//	if (string.IsNullOrEmpty(imageData))
		//	{
		//		objHUDSignatureModel = projectActionFormManager.GetHUDSignatureByUserId(Convert.ToInt32(userId));
		//		data = objHUDSignatureModel.Signature;
		//	}
		//	else
		//	{
		//		 data = Convert.FromBase64String(imageData);
		//	}


		//	//byte[] data = Convert.FromBase64String(imageData);
		//	objHUDSignatureModel.Signature = data;
		//	objHUDSignatureModel.UserId = Convert.ToInt32(userId);
		//	//string sigPathForSaving = Server.MapPath("~/Templates/Sig/Hud");
		//	//string fileNameWitPath = sigPathForSaving + objHUDSignatureModel.UserId.ToString() + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";
		//	string sigPathForSaving1 = Server.MapPath("~/Templates/Sig/Hud");
		//	string fileNameWitPath1 = sigPathForSaving1 + objHUDSignatureModel.UserId.ToString() + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";

		//	string sigPathForSaving = Server.MapPath("~/Templates/Sig/");
		//	string fileNameWitPath = sigPathForSaving + userSigFile;

		//	using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
		//	{
		//		using (BinaryWriter bw = new BinaryWriter(fs))
		//		{
		//			bw.Write(data);
		//			bw.Close();
		//		}
		//	}

		//	existingHUDSignatureModel = projectActionFormManager.GetHUDSignatureByUserId(Convert.ToInt32(userId));
		//	if (existingHUDSignatureModel != null && existingHUDSignatureModel.Signature== null)
		//	{
		//		retSigId = projectActionFormManager.AddNewSignature(objHUDSignatureModel);
		//	}
		//	else
		//	{
		//		projectActionFormManager.UpdateHUDSignatureByUserId(objHUDSignatureModel);
		//	}


		//	return null;
		//}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="selectedFhaNumber"></param>
		/// <param name="IsIRRequest"></param>
		/// <returns></returns>
		public JsonResult GetPropertyInfoForAmendment(string selectedFhaNumber, string IsIRRequest)
		{
			var result = new PropertyInfoModel();
			AmendmentPropertyInfo amendmentPropertyInfo = new AmendmentPropertyInfo();
			JsonResult json = null;
			string closerName = string.Empty;
			int AmendmentExist = 0;
			string status = string.Empty;

			AmendmentExist = taskManager.CheckAmendmentExist(selectedFhaNumber, 18, 1);
			closerName = taskManager.GetAssignedCloserByFHANumber(selectedFhaNumber);

			if (AmendmentExist == 0)
			{
				if (IsIRRequest == "False")
				{
					result = projectActionFormManager.GetProdPropertyInfo(selectedFhaNumber);

				}
				else
				{
					result = projectActionFormManager.GetPropertyInfo(selectedFhaNumber);
				}

				amendmentPropertyInfo = new AmendmentPropertyInfo
				{
					ActiveDecCaseInd = result.ActiveDecCaseInd,
					AddressId = result.AddressId,
					City = result.City,
					InitialEndorsementDate = result.InitialEndorsementDate,
					IsAddressChange = result.IsAddressChange,
					ProjectType = result.ProjectType,
					ProjectTypeID = result.ProjectTypeID,
					PropertyId = result.PropertyId,
					PropertyName = result.PropertyName,
					ReacScore = result.ReacScore,
					State = result.State,
					StreetAddress = result.StreetAddress,
					TroubledCode = result.TroubledCode,
					Zipcode = result.Zipcode,
					IsCloserAssigned = !string.IsNullOrEmpty(closerName) ? true : false
				};
				if (amendmentPropertyInfo != null)
				{
					json = Json(amendmentPropertyInfo, JsonRequestBehavior.AllowGet);
				}
				//if (result != null)
				//            {
				//                json = Json(result, JsonRequestBehavior.AllowGet);
				//            }

			}
			else
			{
				status = PreviousAmendmentStage.ApplicationExist.ToString();
				AmendmentExist = taskManager.CheckAmendmentExist(selectedFhaNumber, 10, 1);
				if (AmendmentExist > 0)
					status = PreviousAmendmentStage.DraftExist.ToString();
				AmendmentExist = taskManager.CheckAmendmentExist(selectedFhaNumber, 15, 1);
				if (AmendmentExist > 0)
					status = PreviousAmendmentStage.ExecutedExist.ToString();
				json = Json(status, JsonRequestBehavior.AllowGet);
				//json = Json("Exist", JsonRequestBehavior.AllowGet);
			}
			return json;
		}


		/// <summary>
		/// Amendment section for  SP
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <param name="pTaskInstanceId"></param>
		/// <param name="pFHANumber"></param>
		/// <returns></returns>
		public JsonResult GetAmendmentInfoForSharepoint(string sidx, string sord, int page, int rows, Guid pTaskInstanceId, string pFHANumber)
		{
			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			Prod_FormAmendmentTaskModelSP objProd_FormAmendmentTaskModelSP = new Prod_FormAmendmentTaskModelSP();
			List<Prod_FormAmendmentTaskModelSP> olistProd_FormAmendmentTaskModelSP = new List<Prod_FormAmendmentTaskModelSP>();
			IList<Prod_FormAmendmentTaskModel> olistProd_FormAmendmentTaskModel = new List<Prod_FormAmendmentTaskModel>();
			string wlmUsername = string.Empty;
			string prodUsername = string.Empty;
			string dtsUWCloser = string.Empty;
			string dtsWLM = string.Empty;
			string DtsDAPCloser = string.Empty;

			if (!string.IsNullOrEmpty(pFHANumber))
			{
				olistProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentByFHANumber(pFHANumber);
			}
			else
			{
				olistProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentByTaskInstanceId(pTaskInstanceId);
			}


			foreach (var item in olistProd_FormAmendmentTaskModel)
			{
				List<Prod_FormAmendmentTaskModelSP> objList = new List<Prod_FormAmendmentTaskModelSP>();
				objList = olistProd_FormAmendmentTaskModelSP.Where(o => o.TaskInstanceId == item.TaskInstanceId).ToList();
				if (objList.Count() > 0 && item.WLMSignatureId > 0)
				{
					if (item.ModifiedOn.HasValue)
					{
						dtsUWCloser = item.ModifiedOn.Value.ToShortDateString();
					}
					else
					{
						dtsUWCloser = item.CreatedOn.ToShortDateString();
					}
					objList.FirstOrDefault().UWCloserDts = dtsUWCloser;
					continue;
				}
				StringBuilder builder = new StringBuilder();
				if (!string.IsNullOrEmpty(item.Party_Propert_Name))
					builder.Append(AmendmentTypeSP.Party_Propert_Name_cb.GetDescription() + "/");
				if (!string.IsNullOrEmpty(item.Party_Property_Address_Name))
					builder.Append(AmendmentTypeSP.Party_Property_Address_Name_cb.GetDescription() + "/");
				if (item.LoanAmount > 0.0M)
					builder.Append(AmendmentTypeSP.LoanAmount_cb.GetDescription() + "/");
				if (item.InterestRate > 0.0M)
					builder.Append(AmendmentTypeSP.InterestRate_cb.GetDescription() + "/");
				if (item.MonthlyPayment > 0.0M)
					builder.Append(AmendmentTypeSP.MonthlyPayment_cb.GetDescription() + "/");
				if (item.DifferentMonthlyPayment > 0.0M)
					builder.Append(AmendmentTypeSP.DifferentMonthlyPayment_cb.GetDescription() + "/");
				if (item.CommitmentTerminationDate.HasValue)
					builder.Append(AmendmentTypeSP.CommitmentTerminationDate_cb.GetDescription() + "/");
				if (item.R4RAmount > 0.0M)
					builder.Append(AmendmentTypeSP.R4RAmount_cb.GetDescription() + "/");
				if (item.CriticalRepairCost > 0.0M)
					builder.Append(AmendmentTypeSP.CriticalRepairCost_cb.GetDescription() + "/");
				if (item.RemainingRepairCostExihibitC > 0.0M)
					builder.Append(AmendmentTypeSP.RemainingRepairCostExihibitC_cb.ToString() + "/");
				if (item.SpecialCondition > 0)
					builder.Append(AmendmentTypeSP.SpecialCondition_cb.GetDescription() + "/");
				if (item.AnnualLeasePayment > 0.0M)
					builder.Append(AmendmentTypeSP.AnnualLeasePayment_cb.GetDescription() + "/");
				if (!string.IsNullOrEmpty(item.Other))
					builder.Append(AmendmentTypeSP.Other_cb.GetDescription() + "/");
				if (item.ExhibitLetter > 0)
					builder.Append(AmendmentTypeSP.ExhibitLetter_cb.GetDescription() + "/");

				prodUsername = accountManager.GetUserNameById(item.AuthorizedAgentSignatureId);
				wlmUsername = accountManager.GetUserNameById(item.WLMSignatureId);
				if (!string.IsNullOrEmpty(wlmUsername))
				{
					if (item.ModifiedOn.HasValue)
					{
						dtsWLM = item.ModifiedOn.Value.ToShortDateString();// + "  " + item.ModifiedOn.Value.ToShortTimeString();
						dtsUWCloser = item.CreatedOn.ToShortDateString();// + "  " + item.ModifiedOn.Value.ToShortTimeString();
					}
					else
					{
						dtsWLM = item.CreatedOn.ToShortDateString();// + "  " + item.ModifiedOn.Value.ToShortTimeString();
						dtsUWCloser = item.CreatedOn.ToShortDateString();// + "  " + item.ModifiedOn.Value.ToShortTimeString();
					}
				}

				objProd_FormAmendmentTaskModelSP = new Prod_FormAmendmentTaskModelSP
				{
					AmendmentNumber = item.FirmAmendmentNum,
					AmendmentTemplate = "Request " + item.FirmAmendmentNum.ToString(),
					AmendmentTemplateID = item.AmendmentTemplateID,
					AmendmentType = builder.ToString(),
					AssignedUWCloser = accountManager.GetFullUserNameById(item.AuthorizedAgentSignatureId),
					AssignedWLM = accountManager.GetFullUserNameById(item.WLMSignatureId),
					DAPCompletedByCloser = item.DAPCompletedDate,
					DAPCloserDts = string.Empty,
					WLMDts = dtsWLM,
					UWCloserDts = dtsUWCloser,
					FHANumber = item.FHANumber,
					SubmittedBy = accountManager.GetFullUserNameById(item.CreatedBy),
					SubmittedOn = item.ModifiedOn.Value,
					TaskInstanceId = item.TaskInstanceId
				};
				olistProd_FormAmendmentTaskModelSP.Add(objProd_FormAmendmentTaskModelSP);

			}

			int totalrecods = olistProd_FormAmendmentTaskModelSP.Count();
			var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
			var result = olistProd_FormAmendmentTaskModelSP.ToList().OrderBy(s => s.AmendmentNumber);
			var results = result.Skip(pageIndex * pageSize).Take(pageSize);
			var jsonData = new
			{
				total = totalpages,
				page,
				records = totalrecods,
				rows = results,

			};
			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}



		public ActionResult DapDataWindow(string pTaskInstanceId, string pFHANum = null)
		{



			return View("~/Views/Production/ProductionQueue/SharepointSinglePage/_AmendmentDAPDialog.cshtml");
		}


		//[HttpPost]
		//public ActionResult UpdateDapSP(string pAmendmentTemplateId,string pDapDte)
		//{
		//	Prod_FormAmendmentTaskModel objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
		//	int amendmentTemplateId = Convert.ToInt32(pAmendmentTemplateId);
		//	objProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentById(amendmentTemplateId);
		//	if (!string.IsNullOrEmpty(pDapDte))
		//		objProd_FormAmendmentTaskModel.DAPCompletedDate =  DateTime.Parse(pDapDte);
		//	projectActionFormManager.SaveDapFromAmendmentSP(objProd_FormAmendmentTaskModel);
		//	return null;
		//}


		/// <summary>
		/// Update DAP date from SP
		/// </summary>
		/// <param name="pAmendmentTemplateId"></param>
		/// <param name="pDapDte"></param>
		/// <returns></returns>
		public string UpdateDapSP(string pAmendmentTemplateId, string pDapDte)
		{
			Prod_FormAmendmentTaskModel objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
			int amendmentTemplateId = Convert.ToInt32(pAmendmentTemplateId);
			objProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentById(amendmentTemplateId);
			if (!string.IsNullOrEmpty(pDapDte))
				objProd_FormAmendmentTaskModel.DAPCompletedDate = DateTime.Parse(pDapDte);
			projectActionFormManager.SaveDapFromAmendmentSP(objProd_FormAmendmentTaskModel);
			return "Success";
		}


		/// <summary>
		/// complete closer DAP status
		/// </summary>
		/// <param name="pAmendmentTemplateId"></param>
		/// <param name="pDapDte"></param>
		/// <param name="pLoanAmt"></param>
		/// <returns></returns>
		public string CompleteDapSP(string pAmendmentTemplateId, string pDapDte, decimal pLoanAmt)
		{
			Prod_FormAmendmentTaskModel objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
			int amendmentTemplateId = Convert.ToInt32(pAmendmentTemplateId);
			objProd_FormAmendmentTaskModel = projectActionFormManager.GetFormAmendmentById(amendmentTemplateId);
			if (!string.IsNullOrEmpty(pDapDte))
				objProd_FormAmendmentTaskModel.DAPCompletedDate = DateTime.Parse(pDapDte);
			if (pLoanAmt > 0.0M)
				objProd_FormAmendmentTaskModel.LoanAmount = pLoanAmt;
			projectActionFormManager.SaveDapFromAmendmentSP(objProd_FormAmendmentTaskModel);
			taskManager.UpdateTaskStatus(objProd_FormAmendmentTaskModel.TaskInstanceId.Value);
			return "Success";
		}

		/// <summary>
		/// List Amendment Files on SP 
		/// </summary>
		/// <param name="taskInstanceId"></param>
		/// <returns></returns>
		//public JsonResult GetWPUpload(string sidx, string sord, int page, int rows, Guid? taskInstanceId, int Status, int PageTypeId)
		public JsonResult GetWPUpload(Guid? taskInstanceId)
		{
			int Status = 1;
			int PageTypeId = 18;
			string sidx = "";
			string sord = "";
			int page = 1;
			int rows = 100;
			if (taskInstanceId == null)
			{
				taskInstanceId = Guid.Empty;
			}

			var GridContent = projectActionFormManager.GetGetWPUpload(taskInstanceId.Value, Status, PageTypeId);
			GridContent = GridContent.Where(x => x.id == "38" || x.parent == "38").ToList();
			//Setting the date based on the Time Zone
			if (GridContent != null)
			{
				foreach (var item in GridContent)
				{
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                }
			}
			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalrecods = GridContent != null ? GridContent.Count() : 0;
			var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
			var result = GridContent != null ? GridContent.ToList().OrderBy(a => a.id) : null;
			var results = result != null ? result.Skip(pageIndex * pageSize).Take(pageSize) : result;
			var jsonData = new
			{
				total = totalpages,
				page,
				records = totalrecods,
				rows = results,
			};
			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}
	}
}
