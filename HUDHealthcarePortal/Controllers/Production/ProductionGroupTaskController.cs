﻿using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using Model.Production;
using Model.ProjectAction;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class ProductionGroupTaskController : Controller
    {
        private IProd_GroupTasksManager prod_GroupTasksManager;
        private IProjectActionFormManager projectActionFormManager;
        private IProjectActionManager projectActionManager;
        //Added by siddesh #265//
        private ITaskManager _taskManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager;
        private IAppProcessManager appProcessManager;
        private IProd_RestfulWebApiTokenRequest webApiTokenRequest;
        public ProductionGroupTaskController()
            : this(new TaskManager(),
                 new Prod_GroupTasksManager(), new ProjectActionFormManager(), new ProjectActionManager(), new NonCriticalRepairsRequestManager(), new AppProcessManager(), new Prod_RestfulWebApiTokenRequest())
        {

        }

        private ProductionGroupTaskController(ITaskManager taskManager,
          IProd_GroupTasksManager _prod_GroupTasksManager, IProjectActionFormManager _projectActionFormManager, IProjectActionManager _projectActionManager, INonCriticalRepairsRequestManager _nonCriticalRepairsManager, IAppProcessManager _appProcessManager, IProd_RestfulWebApiTokenRequest _webApiTokenRequest)
        {

            prod_GroupTasksManager = _prod_GroupTasksManager;
            projectActionFormManager = _projectActionFormManager;
            projectActionManager = _projectActionManager;
            //Added by siddesh #265//
            _taskManager = taskManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;
            appProcessManager = _appProcessManager;
            webApiTokenRequest = _webApiTokenRequest;

        }

        public ActionResult Index(string projActName = null, string fhanumber = null)
        {
            //karri#205, added below code and input parameter
            ArrayList alProjActNames = new ArrayList();
            //Added by siddesh #265//
            ArrayList alFHAnumbers = new ArrayList();
            IList<String> iFHAnumbersList = _taskManager.getFHAList(UserPrincipal.Current.UserName);
            if (fhanumber != null && !string.IsNullOrEmpty(fhanumber))
                _taskManager.setLookUpValues(fhanumber);
            var GridContent = prod_GroupTasksManager.GetGroupTask(UserPrincipal.Current.UserId);
            if (GridContent != null)
                foreach (var item in GridContent)
                {
                    //"(St Camillus Residential Health Care Facility) Refinance 223(a)(7)";                  
                    //St Camillus Residential Health Care Facility
                    string projactionname = item.ProjectActionName.Substring(1).Substring(0, item.ProjectActionName.IndexOf(")") - 1);
                    if (!alProjActNames.Contains(projactionname))
                        alProjActNames.Add(projactionname);
                }
            alProjActNames.Sort();
            IList<String> iProjActionNamesList = new List<string>();
            foreach (string value in alProjActNames)
                iProjActionNamesList.Add(value);
            ViewBag.ProjActionNamesList = iProjActionNamesList;
            @ViewData["BindingProjActionName"] = projActName;
            //Added by siddesh #265//
            alFHAnumbers.Sort();//ASC
            foreach (string value in iProjActionNamesList)
            {
                if (value.Contains("-"))
                {
                    iFHAnumbersList.Add(value);
                }
            }

            ViewBag.FHAnumberList = iFHAnumbersList;

            @ViewData["BindingFHANumber"] = fhanumber;
            //assing the searching project name to so that it can be used in the result set fetching functions
            if (projActName != null && !string.IsNullOrEmpty(projActName))
                TempData["projActName"] = projActName;
            //Added by siddesh #265//
            if (fhanumber != null && !string.IsNullOrEmpty(fhanumber))
                TempData["FHANumbers"] = fhanumber;
            //end of searching values from project action names

            return View("~/Views/Production/ProductionGroupTask/GroupTaskView.cshtml");

        }
        public JsonResult GetGroupTasks(string sidx, string sord, int page, int rows)
        {
            // var data = new GroupTaskModel();
            //data.TaskId = 1;
            //data.Status = "Success";

            dynamic Grouptask = new JObject();
            Grouptask.Id = "1";
            Grouptask.TaskName = "Lender_Fha_Request";
            Grouptask.Status = "Success";
            Grouptask.UnLock = "UnLock";
            Grouptask.Lender = "BerKedia";
            Grouptask.Type = "FHARequest";
            Grouptask.Role = "LAM";
            Grouptask.InUseBy = "Fake Lam";
            Grouptask.CreatedDate = DateTime.UtcNow;
            Grouptask.SubmitedDate = DateTime.UtcNow;
            Grouptask.UnLock = "UnLock";


            var jsonData = new
            {

                total = 1,
                page,
                records = 1,
                rows = Grouptask,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);



        }

        public ActionResult GetGroupTaskDetail(int taskId)
        {


            var groupTaskModel = prod_GroupTasksManager.GetGroupTaskAById(taskId);
            switch (groupTaskModel.PageTypeId)
            {
                case (int)PageType.FhaRequest:
                    return RedirectToAction("GetGroupTaskDetail", "FHARequest", groupTaskModel);
                case (int)PageType.ProductionApplication:
                    var model = this.ReloadApplication(taskId);
                    // return RedirectToAction("GetDetail", "ProductionApplication");
                    model.pageTypeId = (int)PageType.ProductionApplication;
                    return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", model);
                case (int)PageType.ConstructionSingleStage:
                    var singleStageModel = this.ReloadApplication(taskId);
                    // return RedirectToAction("GetDetail", "ProductionApplication");
                    singleStageModel.pageTypeId = (int)PageType.ConstructionSingleStage;
                    return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", singleStageModel);
                case (int)PageType.ConstructionTwoStageInitial:
                    var initialStageModel = this.ReloadApplication(taskId);
                    // return RedirectToAction("GetDetail", "ProductionApplication");
                    initialStageModel.pageTypeId = (int)PageType.ConstructionTwoStageInitial;
                    return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", initialStageModel);

                case (int)PageType.ConstructionManagement:
                    var ConstMgmnt = this.ReloadApplication(taskId);
                    // return RedirectToAction("GetDetail", "ProductionApplication");
                    ConstMgmnt.pageTypeId = (int)PageType.ConstructionManagement;
                    ViewBag.ConstructionTitle = "Construction Management Request Form";
                    return View("~/Views/Production/ConstructionManagement/LenderConstructionMgmntRequest.cshtml", ConstMgmnt);
                case (int)PageType.ConstructionTwoStageFinal:
                    var finalStageModel = this.ReloadApplication(taskId);
                    // return RedirectToAction("GetDetail", "ProductionApplication");
                    finalStageModel.pageTypeId = (int)PageType.ConstructionTwoStageFinal;
                    return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", finalStageModel);

                case (int)PageType.ClosingAllExceptConstruction:
                    var closingALL = this.ReloadApplication(taskId);
                    // return RedirectToAction("GetDetail", "ProductionApplication");
                    ViewBag.ConstructionTitle = "Non-Construction Draft Closing";
                    closingALL.pageTypeId = (int)PageType.ClosingAllExceptConstruction;
                    return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", closingALL);

                case (int)PageType.ExecutedClosing:
                    var ExecutedClosing = this.ReloadApplication(taskId);
                    // return RedirectToAction("GetDetail", "ProductionApplication");
                    ViewBag.ConstructionTitle = "Non-Construction Executed Closing";
                    ExecutedClosing.pageTypeId = (int)PageType.ExecutedClosing;
                    return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", ExecutedClosing);

                case (int)PageType.ClosingConstructionInsuredAdvances:
                    var modl = this.ReloadApplication(taskId);
                    ViewBag.ConstructionTitle = "Closing Request (Initial Closing)";
                    modl.pageTypeId = (int)PageType.ClosingConstructionInsuredAdvances;
                    return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", modl);

                case (int)PageType.ClosingConstructionInsuranceUponCompletion:
                    var modlClosingConst = this.ReloadApplication(taskId);
                    ViewBag.ConstructionTitle = "Closing Request (Final Closing)";
                    modlClosingConst.pageTypeId = (int)PageType.ClosingConstructionInsuranceUponCompletion;
                    return View("~/Views/Production/ConstructionRequest/LenderConstructionRequest.cshtml", modlClosingConst);
                case (int)PageType.Amendments:
                    var modlAmendments = this.ReloadApplication(taskId);
                    ViewBag.ConstructionTitle = "Amendments";
                    modlAmendments.pageTypeId = (int)PageType.Amendments;
                    return View("~/Views/Production/Amendments/Index.cshtml", modlAmendments);

            }
            return null;
        }



        public JsonResult GetProdGroupTaskGridContent(string sidx, string sord, int page, int rows)
        {


            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            // List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();

            //karri#205; commented below for a new method created
            //var GridContent = prod_GroupTasksManager.GetGroupTask(UserPrincipal.Current.UserId);
            var GridContent = getFilteredProjActionNames(UserPrincipal.Current.UserId);

            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.CreatedOn = TimezoneManager.ToMyTimeFromUtc(item.CreatedOn);
                    item.CreatedOn = item.CreatedOn;
                    if (item.ServicerSubmissionDate != null)
                    {
                        //item.ServicerSubmissionDate = TimezoneManager.ToMyTimeFromUtc(item.ServicerSubmissionDate);
                        item.ServicerSubmissionDate = item.ServicerSubmissionDate;
                    }

                }

            }

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);


            var result = GridContent.ToList();
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);



            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        //karri#205
        private List<Prod_GroupTasksModel> getFilteredProjActionNames(int userid)
        {
            if (TempData["projActName"] != null && !string.IsNullOrEmpty(TempData["projActName"].ToString()))
            {
                return prod_GroupTasksManager.GetGroupTask(userid).Where(a => a.ProjectActionName.ToUpper().Contains(TempData["projActName"].ToString().ToUpper())).ToList(); ;
            }

            //Added by siddesh #265//           
            //Filter by FHAnumber
            if (TempData["FHANumbers"] != null && !string.IsNullOrEmpty(TempData["FHANumbers"].ToString()))
            {
                return prod_GroupTasksManager.GetGroupTask(userid).Where(a => a.ProjectActionName.ToUpper().Contains(TempData["FHANumbers"].ToString().ToUpper())).ToList();

            }


            //by default 
            return prod_GroupTasksManager.GetGroupTask(userid);
        }

        public OPAViewModel ReloadApplication(int taskid)

        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            var Groupmodel = prod_GroupTasksManager.GetGroupTaskAById(taskid);

            AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(Groupmodel.TaskInstanceId);
            //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(5);
            //foreach (var model in projectActionList)
            //{
            //    AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
            //    {
            //        Text = model.ProjectActionName,
            //        Value = model.ProjectActionID.ToString()
            //    });
            //}

            var result = projectActionFormManager.GetProdPropertyInfo(AppProcessViewModel.FhaNumber);
            if (result != null)
            {
                AppProcessViewModel.PropertyId = result.PropertyId;
                AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                AppProcessViewModel.PropertyAddress.StateCode = result.State;
                AppProcessViewModel.PropertyAddress.City = result.City;
                AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
            }
            AppProcessViewModel.GroupTaskInstanceId = Groupmodel.TaskInstanceId;


            if (Groupmodel != null)
            {
                AppProcessViewModel.ServicerComments = Groupmodel.ServicerComments;
                AppProcessViewModel.GroupTaskId = Groupmodel.TaskId;
            }

            this.GetDisclaimerText(AppProcessViewModel, "APPLICATION REQUEST");
            //return View("~/Views/Production/ApplicationRequest/LenderApplicationRequest.cshtml", AppProcessViewModel);
            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectTypebyID(AppProcessViewModel.ProjectActionTypeId);
            AppProcessViewModel.pageTypeId = Groupmodel.PageTypeId;
            //AppProcessViewModel.pageTypeId = (int)PageType.ProductionApplication; 
            return AppProcessViewModel;

        }

        private void GetDisclaimerText(OPAViewModel model, string Type)
        {
            model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType(Type);
        }

        public ActionResult Unlock(int taskid)
        {
            prod_GroupTasksManager.UnlockGroupTask(taskid);
            return RedirectToAction("Index");
        }
    }


}
