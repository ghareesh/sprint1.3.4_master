﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BusinessService.Interfaces;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using Microsoft.Ajax.Utilities;
using Model;
using Model.Production;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using Core;
using Core.Utilities;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using BusinessService.ProjectAction;
using System.Globalization;
using Rotativa;
using Rotativa.Options;
using System.Net.Http;
using System.IO;
using HUDHealthcarePortal.Helpers;
using Repository.Interfaces;
using System.Configuration;
using System.Collections;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class ProductionQueueController : Controller
    {
        private IProductionQueueManager productionQueue;
        private IProd_TaskXrefManager prod_TaskXrefManager;
        private ITaskManager taskManager;
        private IFHANumberRequestManager fhaNumberRequestManager;
        private IProd_NoteManager prod_noteManager;
        private IAppProcessManager appProcessManager;
        private IProd_ViewManager prodviewmanager;
        private IBackgroundJobMgr backgroundJobManager;
        private IProjectActionFormManager _projectActionFormManager;
        private IProd_SharepointScreenManager sharepointScreenManager;
        private IProd_RestfulWebApiDocumentUpload uploadApiManager;
        private IProd_RestfulWebApiTokenRequest webApiTokenRequest;
        private IProd_RestfulWebApiDownload webApiDownload;
        private IAccountManager accountManager;
        private IProd_LoanCommitteeManager loanCommitteManager;


        public ProductionQueueController()
            : this(new TaskManager(), new Prod_TaskXrefManager(), new ProductionQueueManager(), new FHANumberRequestManager(), new Prod_NoteManager(), new AppProcessManager(),
            new Prod_ViewManager(), new BackgroundJobMgr(), new ProjectActionFormManager(), new Prod_SharepointScreenManager(), new Prod_RestfulWebApiDocumentUpload(),
            new Prod_RestfulWebApiTokenRequest(), new Prod_RestfulWebApiDownload(), new AccountManager(), new Prod_LoanCommitteeManager())
        {

        }
        public ProductionQueueController(ITaskManager _taskManager, IProd_TaskXrefManager _prodTaskXrefManager, IProductionQueueManager _productionQueueManager,
                                          IFHANumberRequestManager _fhaNumberRequestManager, IProd_NoteManager _prodNoteManager, IAppProcessManager _appProcessManager,
            IProd_ViewManager _prodviewmanager, IBackgroundJobMgr backgroundManager, IProjectActionFormManager projectActionFormManager,
            IProd_SharepointScreenManager _sharepointScreenManager, IProd_RestfulWebApiDocumentUpload _prod_RestfulWebApiDocumentUpload,
            IProd_RestfulWebApiTokenRequest _prod_WebApiTokenRequest, IProd_RestfulWebApiDownload _webApiDownload, IAccountManager _accountManager,
            IProd_LoanCommitteeManager _loanCommitteeManager)
        {

            taskManager = _taskManager;
            prod_TaskXrefManager = _prodTaskXrefManager;
            productionQueue = _productionQueueManager;
            fhaNumberRequestManager = _fhaNumberRequestManager;
            prod_noteManager = _prodNoteManager;
            appProcessManager = _appProcessManager;
            prodviewmanager = _prodviewmanager;
            backgroundJobManager = backgroundManager;
            _projectActionFormManager = projectActionFormManager;
            sharepointScreenManager = _sharepointScreenManager;
            uploadApiManager = _prod_RestfulWebApiDocumentUpload;
            webApiTokenRequest = _prod_WebApiTokenRequest;
            webApiDownload = _webApiDownload;
            accountManager = _accountManager;
            loanCommitteManager = _loanCommitteeManager;
        }
        // GET: /ProductionTask/

        public ActionResult Index()
        {
            //karri#205
            IList<String> SearchableData = getSearchableData(0);//0 mean all types of requests and tasks and projects
            //ViewData["BindingFHANumber"] = "123456";
            ViewBag.SearchableData = SearchableData;

            //karri#264
            IList<String> AssignedSearchableData = getAssignedSearchableData(0);//0 mean all types of requests and tasks and projects
            //ViewData["BindingFHAAndProjName"] = "123456";
            ViewBag.AssignedSearchableData = AssignedSearchableData;

            //karri#264
            IList<String> CompletedSearchableData = getCompletedSearchableData(0);//0 mean all types of requests and tasks and projects
            //ViewData["BindingFHAAndProjName"] = "123456";
            ViewBag.CompletedSearchableData = CompletedSearchableData;

            PopulateLookUps();

            var isHudAdmin = RoleManager.IsHudAdmin(UserPrincipal.Current.UserName);
            if (isHudAdmin)
                return View("~/Views/Production/ProductionQueue/HudAdminReadOnly/ProductionQueueReadOnly.cshtml");
            return View("~/Views/Production/ProductionQueue/Index.cshtml");
        }

        public ActionResult AssignedProductionTasks()
        {
            PopulateLookUps();
            return PartialView("~/Views/Production/ProductionQueue/AssignedProductionTasks.cshtml");
        }

        public ActionResult TaskAsignment(Guid taskXrefid)
        {
            var xrefTask = prod_TaskXrefManager.GetProductionSubtaskById(taskXrefid);
            var taskAssignmentModel = new ProductionTaskAssignmentModel { TaskXrefid = taskXrefid, TaskName = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(xrefTask.ViewId.ToString())) };
            var curentUser = UserPrincipal.Current.UserName;
            taskAssignmentModel.isWLM = RoleManager.IsProductionWLM(curentUser);
            var productionUser = (from prodUsers in productionQueue.GetProductionUsers()
                                  select new
                                  {
                                      prodUsers.UserID,
                                      Name = String.Format("{0} {1}", prodUsers.FirstName, prodUsers.LastName)

                                  });

            ViewBag.AssignedToUserId = new SelectList(productionUser, "UserID", "Name");
            return View("~/Views/Production/ProductionQueue/TaskAsignment.cshtml", taskAssignmentModel);
        }

        [HttpPost]
        public void UpdateTaskAsignment(ProductionTaskAssignmentModel model)
        {
            var currentUser = UserPrincipal.Current;
            if (currentUser != null)
            {
                if (model != null)
                {
                    model.AssignedToUserId = model.AssignedToUserId;
                    model.CurrentUserId = currentUser.UserId;
                    model.UserName = productionQueue.GetUserById(model.AssignedToUserId).UserName;
                    model.Status = (int)ProductionFhaRequestStatus.InProcess;
                    model.AssignedDate = DateTime.UtcNow;
                    if (model.PageTypeId == (int)PageType.Form290)
                    {
                        model.Status = (int)TaskStep.Form290Request;
                        productionQueue.AssignForm290Task(model);
                    }
                    else
                    {

                        if (model.TaskInstanceId == Guid.Empty)
                            prod_TaskXrefManager.AssignProductionFhaRequest(model);
                        else
                        {
                            productionQueue.AssignProductionFhaInsert(model);
                            //FHANumberRequestViewModel FHANumberRequestViewModel = new FHANumberRequestViewModel();
                            //FHANumberRequestViewModel.TaskinstanceId = model.TaskInstanceId;
                            //fhaNumberRequestManager.UpdateFHARequest(FHANumberRequestViewModel);
                            fhaNumberRequestManager.FhataskAssignedfromQueDateUpdate(model.TaskInstanceId);
                        }

                    }


                }

            }
        }
        public ActionResult FhaInsertAssignment(Guid taskInstanceId)
        {
            var taskAssignmentModel = new ProductionTaskAssignmentModel { TaskInstanceId = taskInstanceId, TaskName = "FHA # Insert" };
            var curentUser = UserPrincipal.Current.UserName;
            taskAssignmentModel.isWLM = RoleManager.IsProductionWLM(curentUser);
            var productionUser = (from prodUsers in productionQueue.GetProductionUsers()
                                  select new
                                  {
                                      prodUsers.UserID,
                                      Name = String.Format("{0} {1}", prodUsers.FirstName, prodUsers.LastName)

                                  });

            ViewBag.AssignedToUserId = new SelectList(productionUser, "UserID", "Name");
            return View("~/Views/Production/ProductionQueue/TaskAsignment.cshtml", taskAssignmentModel);
        }
        public ActionResult Form290Assignement(Guid taskInstanceId)
        {
            var assignmentModel = new ProductionTaskAssignmentModel
            {
                TaskInstanceId = taskInstanceId,
                TaskName = "Form 290",
                PageTypeId = (int)PageType.Form290
            };
            var curentUser = UserPrincipal.Current.UserName;
            assignmentModel.isWLM = RoleManager.IsProductionWLM(curentUser);
            var productionUser = (from prodUsers in productionQueue.GetProductionUsers()
                                  select new
                                  {
                                      prodUsers.UserID,
                                      Name = String.Format("{0} {1}", prodUsers.FirstName, prodUsers.LastName)

                                  });

            ViewBag.AssignedToUserId = new SelectList(productionUser, "UserID", "Name");
            return View("~/Views/Production/ProductionQueue/TaskAsignment.cshtml", assignmentModel);
        }
        private IEnumerable<ProductionQueueLenderInfo> GetForm290Task(List<ProductionQueueLenderInfo> appRequests, List<Guid> taskInstanceId)
        {
            var form290Requests = (from form290 in productionQueue.GetForm290TaskByTaskInstanceID(taskInstanceId)
                                   select new
                                   {
                                       form290.TaskInstanceID,
                                       form290.ClosingTaskInstanceID

                                   }
                                   ).ToList();

            //productionQueue.GetForm290TaskByTaskInstanceID(taskInstanceId.Single()).Select(m => m.ClosingTaskInstanceID);
            var unAssignedForm290Task = (from closingRequest in appRequests
                                         join task in form290Requests
                                         on closingRequest.TaskinstanceId equals task.ClosingTaskInstanceID
                                         select new ProductionQueueLenderInfo()
                                         {
                                             ClosingInstanceID = task.ClosingTaskInstanceID,
                                             TaskinstanceId = task.TaskInstanceID,
                                             projectName = closingRequest.projectName,
                                             LenderName = closingRequest.LenderName,
                                             LoanType = closingRequest.LoanType,
                                             LoanAmount = closingRequest.LoanAmount,
                                             IsLIHTC = closingRequest.IsLIHTC,
                                             CreatedBy = closingRequest.CreatedBy,
                                             ModifiedOn = closingRequest.ModifiedOn

                                         }).ToList();


            //appRequests.Where(m => form290Requests.Contains(m.TaskinstanceId)).ToList();

            return unAssignedForm290Task;
        }
        public JsonResult GetProductionTasks(string sidx, string sord, int page, int rows, int applicationType)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var productionTasks = applicationType == 0 ? productionQueue.GetProductionQueues().ToList() :
                                                   productionQueue.GetProductionQueueByType(applicationType).ToList();

            //var c = productionTasks.Where(x => x.FHANumber == "042-22131");
            //karri#434;restricting the FHA Requests only for unassaigned tab
            //productionTasks = (from q in productionTasks
            //                   where q.PageTypeId == 4 && (q.AssignedTo == "Queue" || q.AssignedTo == null)
            //                   select q).ToList();

            var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
            var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

            //test
           // var ac = productionTasks.Where(x => x.TaskId == 31279).Select(x=> x.TaskInstanceId);
            //test end

            var notAssignedTasks = productionQueue.GetCompletedFHAsAndNotAssignedChild();
            productionTasks = productionTasks.Union(notAssignedTasks).ToList();

            
            var applicationRequests = appProcessManager.GetAppRequests().ToList();
            var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
            List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
            if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
            {
                submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
            }
            // var from290Requests = GetForm290Task(applicationRequests, form290TaskInstances);

            //var form290Requests = productionQueue.GetForm290TaskByTaskInstanceID(form290TaskInstances.Single()).Select(m=>m.ClosingTaskInstanceID);
            //var unAssignedForm290Task = applicationRequests.Where(m => form290Requests.Contains(m.TaskinstanceId)).ToList();


            var productionTasksForQueue = (from fhaRequest in productionQueue.GetFhaRequests(distnictInstanceId).ToList()
                                           join task in productionTasks
                    //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
                    on fhaRequest.TaskinstanceId equals task.TaskInstanceId
                                           // naveen ,harish 
                                           where task.PageTypeId == 4 && (task.AssignedTo == "Queue"||task.AssignedTo == null)

                                           select new
                                           {
                                               task.TaskInstanceId,
                                               task.TaskId,
                                               TaskName = fhaRequest.projectName,
                                              // PropertyName = fhaRequest.PropertyName,
                                               task.SequenceId,
                                               task.PageTypeId,
                                               task.StartTime,
                                               task.AssignedBy,
                                               task.AssignedTo,
                                               Lender = fhaRequest.LenderName,
                                               // task.TaskStepId
                                                //task.TaskStepId,
                                                
                                               ModofiedOn = fhaRequest.ModifiedOn,
                                               Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                               DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                                               IsLIHTC = fhaRequest.IsLIHTC == true ? "Yes" : "",
                                               LoanType = fhaRequest.LoanType,

                                               LoanAmount = fhaRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture),

                                               

                                           }).Union(from appRequest in applicationRequests
                                                    join task in productionTasks
                                                  //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
                                                  on appRequest.TaskinstanceId equals task.TaskInstanceId
                                                    select new
                                                    {
                                                        task.TaskInstanceId,
                                                        task.TaskId,
                                                        TaskName = appRequest.projectName,
                                                       // PropertyName = appRequest.PropertyName,
                                                        task.SequenceId,
                                                        task.PageTypeId,
                                                        // task.TaskStepId,
                                                        //task.TaskStepId,
                                                        //task.StartTime,
                                                        StartTime = ControllerHelper.GetProdQStartDate(task.TaskInstanceId),
                                                        task.AssignedBy,
                                                        task.AssignedTo,
                                                        Lender = appRequest.LenderName,
                                                        ModofiedOn = appRequest.ModifiedOn,
                                                        Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                                        DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                                                        //IsLIHTC = "",
                                                        IsLIHTC = appRequest.IsLIHTC == true ? "Yes" : "",
                                                        LoanType = appRequest.LoanType,
                                                        LoanAmount = appRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture)
                                                    })
                .Union(from closeRequest in submittedForm290
                       join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
                          on closeRequest.TaskinstanceId equals task.TaskInstanceId
                       select new
                       {
                           task.TaskInstanceId,
                           task.TaskId,
                           TaskName = closeRequest.projectName,
                           //PropertyName = closeRequest.PropertyName,
                           task.SequenceId,
                           task.PageTypeId,
                           task.StartTime,
                           task.AssignedBy,
                           task.AssignedTo,
                           //task.TaskStepId,
                          // TaskStepId = task.TaskStepId,
                           Lender = closeRequest.LenderName,
                           ModofiedOn = closeRequest.ModifiedOn,
                           Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                           DaysInQueue = (DateTime.Now.Date - task.StartTime.Date).Days + " Day(s)",
                           //IsLIHTC = "",
                           IsLIHTC = closeRequest.IsLIHTC == true ? "Yes" : "",
                           LoanType = closeRequest.LoanType,
                           LoanAmount = closeRequest.LoanAmount.ToString("C", CultureInfo.CurrentCulture)
                       }).ToList();
            //
            //  productionTasksForQueue = productionTasksForQueue.OrderBy(x => x.IsLIHTC);


            //

            int totalrecods = productionTasksForQueue.Count();

            
                                       
                























          var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            //Task:4596
            if (applicationType > 0)
            {
                productionTasksForQueue = productionTasksForQueue.Where(x => x.PageTypeId == applicationType).ToList();
            }

            var results = productionTasksForQueue.Skip(pageIndex * pageSize).Take(pageSize);
            //var results = productionTasksForQueue;
            // added by hareesh 
            var actualResults = new List<int>();
            var taskids = new List<int?>();
            foreach (var task in results)
            {
                string prdtype = "";
                if (applicationType == ((Int32)PageType.FhaRequest))
                    prdtype = "FHA";
                else if (applicationType == ((Int32)PageType.ProductionApplication))
                    prdtype = "Produciton Application";
                else if (applicationType == ((Int32)PageType.ConstructionSingleStage))
                    prdtype = "Single Stage";
                else if (applicationType == ((Int32)PageType.ConstructionTwoStageInitial))
                    prdtype = "2 Stage Initial";
                else if (applicationType == ((Int32)PageType.ConstructionTwoStageFinal))
                    prdtype = "2 Stage Final";
                else if (applicationType == ((Int32)PageType.ClosingAllExceptConstruction))
                    prdtype = "Draft Closing";
                else if (applicationType == ((Int32)PageType.ExecutedClosing))
                    prdtype = "Executed Closing";
                else if (applicationType == ((Int32)PageType.ClosingConstructionInsuredAdvances))
                    prdtype = "closing advances";
                else if (applicationType == ((Int32)PageType.ClosingConstructionInsuranceUponCompletion))
                    prdtype = "closing completion";
                else if (applicationType == ((Int32)PageType.ConstructionManagement))
                    prdtype = "construction mgt";
                else if (applicationType == ((Int32)PageType.Form290))
                    prdtype = "Form 290";
                else if (applicationType == ((Int32)PageType.Amendments))
                    prdtype = "Amendment";
                // hareesh 
                if (prdtype == "Form 290")
                    actualResults.Add(task.TaskId);

                var subtasks = GetSubTasksByTaskInstanceId(task.TaskInstanceId, prdtype, "").ToList();

                //var subtasks = GetFHARequestDetail(task.TaskInstanceId, prdtype, "");
                // // naveen ,harish
                if (prdtype == "FHA")
                {
                    var fhatask = subtasks.Where(x => x.AssignedTo == "Queue" || x.AssignedTo == "");
                    if (!fhatask.Any())
                    {
                        actualResults.Add(task.TaskId);
                    }
                    else if (fhatask.Any())
                    {
                        actualResults.Add(task.TaskId);

                    }
                    else
                    {
                        int tid = task.TaskId;
                    }

                }



                var lstTasks = subtasks.Where(x => x.Status == "Unassigned" /*&& (x.FhaType == "UnderWriter" || x.FhaType == "Underwriter/Closer")*/).ToList();
                var taskid = (from q in subtasks
                              where q.ViewId == 1
                              select new { q.TaskId }).ToList();

                if (taskid.Count() > 0)
                {
                    foreach (var a in taskid)
                        taskids.Add(a.TaskId);
                }

                if (lstTasks.Any())
                {
                    var lstFhaType = subtasks.Where(x => x.FhaType == "UnderWriter" || x.FhaType == "Underwriter/Closer");
                    if (!lstFhaType.Any())
                    {
                        actualResults.Add(task.TaskId);
                    }
                    else if (lstTasks.Any())
                    {
                        actualResults.Add(task.TaskId);

                    }
                    else
                    {
                        int tid = task.TaskId;
                    }
                }
            }

            var actualTasks = applicationType > 0 ? results.Where(x => actualResults.Contains(x.TaskId)) : results;

            actualTasks = actualTasks.Where(x => !taskids.Contains(x.TaskId)).ToList();
            //actualTasks = actualTasks.Where(x => x.Type == "Production Application").ToList();

            

            //  results = results.Where(x => !taskids.Contains(x.TaskId)).ToList();
           // var b = actualTasks.Where(x => x.TaskId ==31146);

            //var actualTasks = (from q in actualTasks
            //                where q.st != "In Proccess" && (q.FhaType == "UnderWriter" || q.FhaType == "Underwriter/Closer")
            //                select q).ToList();




            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = actualTasks.OrderBy(y => y.ModofiedOn).OrderByDescending(x => x.IsLIHTC),
                // rows = results.OrderBy(y => y.ModofiedOn).OrderByDescending(x => x.IsLIHTC),

            };



            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        //karri#205
        private IList<String> getSearchableData(int applicationType = 0)
        {
            var productionTasks = applicationType == 0 ? productionQueue.GetProductionQueues().ToList() :
                                                   productionQueue.GetProductionQueueByType(applicationType).ToList();
            var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
            var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

            var notAssignedTasks = productionQueue.GetCompletedFHAsAndNotAssignedChild();
            productionTasks = productionTasks.Union(notAssignedTasks).ToList();

            //karri#434;restricting the FHA Requests only for unassaigned tab
            productionTasks = (from q in productionTasks
                               where q.PageTypeId == 4 && (q.AssignedTo == "Queue" || q.AssignedTo == null)
                               select q).ToList();

            var applicationRequests = appProcessManager.GetAppRequests().ToList();
            var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
            List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
            if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
            {
                submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
            }

            var productionTasksForQueue = (from fhaRequest in productionQueue.GetFhaRequests(distnictInstanceId).ToList()
                                           join task in productionTasks
                    on fhaRequest.TaskinstanceId equals task.TaskInstanceId
                                           select new
                                           {
                                               task.TaskInstanceId,
                                               task.TaskId,
                                               task.PageTypeId,
                                               TaskName = fhaRequest.projectName,
                                               //PropertyName = fhaRequest.PropertyName

                                           }).Union(from appRequest in applicationRequests
                                                    join task in productionTasks
                                                  //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
                                                  on appRequest.TaskinstanceId equals task.TaskInstanceId
                                                    select new
                                                    {
                                                        task.TaskInstanceId,
                                                        task.TaskId,
                                                        task.PageTypeId,
                                                        TaskName = appRequest.projectName,
                                                       // PropertyName = appRequest.PropertyName

                                                    })
                .Union(from closeRequest in submittedForm290
                       join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
                          on closeRequest.TaskinstanceId equals task.TaskInstanceId
                       select new
                       {
                           task.TaskInstanceId,
                           task.TaskId,
                           task.PageTypeId,
                           TaskName = closeRequest.projectName,
                           //PropertyName = closeRequest.PropertyName

                       }).ToList();

            IList<String> searchableDataList = new List<string>();

            ArrayList searchableData = new ArrayList();
            //productionTasksForQueue = productionTasksForQueue.ToList();
            var myTasks = productionTasksForQueue.ToList();
            foreach (var item in myTasks)
            {
                //if (item.PropertyName != null && !string.IsNullOrEmpty(item.PropertyName))

                //    searchableData.Add(item.PropertyName);



                if (item.TaskName != null && !string.IsNullOrEmpty(item.TaskName) && item.TaskName != "fharequest")
                {
                    if (item.TaskName.Contains("(") && item.TaskName.StartsWith("(") && item.TaskName.Contains(")"))
                    {
                        string possibledata = item.TaskName.Substring(1).Substring(0, item.TaskName.IndexOf(")") - 1);
                        if (!searchableData.Contains(possibledata))
                            searchableData.Add(possibledata);
                    }
                    else
                    {
                        if (!searchableData.Contains(item.TaskName.ToString()))
                            searchableData.Add(item.TaskName.ToString());
                    }


                }



            }
            searchableData.Sort();//ASC

            foreach (string value in searchableData)
            {
                //karri:#325
                string txtValue = string.Empty;

                // txtValue = value;
                if (value.Contains("("))
                    txtValue = value.Substring(0, value.IndexOf("("));
                else
                    txtValue = value;

                searchableDataList.Add(txtValue);
            }


            return searchableDataList;

        }

        public JsonResult GetAllProductionTasks(ProductionQueueLenderInfo model, string sidx, string sord, int page, int rows, int applicationType, int status, string fhaOrProjName = null)
        {
            int statusId = 0;
            //int statusId = 17;
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            IEnumerable<TaskModel> productionTasks;

            if (applicationType == (int)PageType.Form290)
            {
                if (status == (int)TaskStep.InProcess) status = (int)TaskStep.Form290Request;
                if (status == (int)TaskStep.Complete) status = (int)TaskStep.Form290Complete;

            }
            if (applicationType == (int)PageType.NotSpecified)
            {
                productionTasks = status == 0 ? productionQueue.GetProductionTasks().ToList() : productionQueue.GetProductionTaskByStatus(status).ToList();

                //take only application proces records
                //var appprcs = productionTasks.Where(x => x.PageTypeId == 5 ).ToList();
                ////remove all application proces records
                //productionTasks = productionTasks.Where(x => x.PageTypeId != 5).ToList();
                ////take only completed application process recrods haivng taskstepid=15
                //var stepid15recs = appprcs.Where(x => x.TaskStepId == 15).ToList();
                ////remove completed reords from "appprcs"
                ////now this objects contains non 15 and rest of records
                //appprcs = appprcs.Where(x => x.TaskStepId != 15).ToList();
                //appprcs = (from q in stepid15recs
                //           join p in appprcs on q.TaskInstanceId equals p.TaskInstanceId
                //           where p.TaskStepId != 18
                //           select p ).ToList();

                ////union these result to main productiontasks object
                //productionTasks = productionTasks.Union(appprcs);
                //var c = productionTasks.Where(x => x.FHANumber == "022-22040");

                //filter completed records
                // appprcs = (from q in appprcs where q.TaskStepId==15)

                //karri#434;restricting the FHA Requests only for assaigned tab harish commented
                //productionTasks = (from q in productionTasks
                //                   where q.PageTypeId == 4 && (q.AssignedTo != "Queue" && q.AssignedTo != null)
                //                   select q).ToList();
                

            }

            else
            {
                if (applicationType != (int)PageType.FhaRequest && status == (int)ProductionAppProcessStatus.Completed)
                    status = (int)ProductionAppProcessStatus.ProjectActionRequestComplete;
                productionTasks = status == 0 ? productionQueue.GetProductionTaskByType(applicationType) : productionQueue.GetProductionTaskByTypeAndStatus(applicationType, status).ToList();

                //take only application proces records
                //var appprcs = productionTasks.Where(x => x.PageTypeId == 5).ToList();
                ////remove all application proces records
                //productionTasks = productionTasks.Where(x => x.PageTypeId != 5).ToList();
                ////take only completed application process recrods haivng taskstepid=15
                //var stepid15recs = appprcs.Where(x => x.TaskStepId == 15).ToList();
                ////remove completed reords from "appprcs"
                ////now this objects contains non 15 and rest of records
                //appprcs = appprcs.Where(x => x.TaskStepId != 15).ToList();
                //appprcs = (from q in stepid15recs
                //           join p in appprcs on q.TaskInstanceId equals p.TaskInstanceId
                //           where  p.TaskStepId != 18
                //           let ab = GetSubTasksByTaskInstanceId(q.TaskInstanceId, "Production Application" ,"").Where(x=>x.ViewId ==1 && x.Status != "Review Completed")
                //                     select p).ToList();

                ////union these result to main productiontasks object
                //productionTasks = productionTasks.Union(appprcs);

                //var a = productionTasks.Where(x => x.TaskId == 31222);
            }
            var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
            var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();


            var applicationRequests = appProcessManager.GetAppRequests().ToList();
            var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
            List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
            if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
            {
                submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
            }

            var fhaRequestTasks = (from task in taskModels.Where(m => m.PageTypeId == (int)PageType.FhaRequest)
                                   join fhaRequest in productionQueue.GetFhaRequests(distnictInstanceId).ToList()
                                   //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
                                   on task.TaskInstanceId equals fhaRequest.TaskinstanceId

                                   select new
                                   {
                                       task.TaskInstanceId,
                                       task.TaskId,
                                       TaskName = fhaRequest.projectName,
                                       PropertyName = fhaRequest.PropertyName,
                                       task.SequenceId,
                                       task.PageTypeId,
                                       task.StartTime,
                                       task.AssignedBy,
                                       task.AssignedTo,
                                       ModofiedOn = fhaRequest.ModifiedOn,
                                       Lender = fhaRequest.LenderName,
                                       //Naveen 238 (31july and 01aug: status as been corrected by writing query to right status)
                                       
                                       //karri:#434;commented below 
                                       //Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(task.TaskStepId.ToString())),
                                       
                                       //Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(((from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                       //                                                                                  select new { }).Count() == 0 ? 19 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                       //                                                                                                                       where q.Status == 17 && task.TaskStepId == 17
                                       //                                                                                                                      select new { }).Count() > 0 ? 18 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                       //                                                                                                                                                          where q.Status == 19 && task.TaskStepId == 19
                                       //                                                                                                                                                           select new { }).Count() > 0 ? 19 : 18).ToString())),

                                       //karri#434
                                       Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(((from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                         select new { }).Count() == 0 ? 19 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                              where (q.Status == 17 || q.Status == 18) && (task.TaskStepId == 17 || task.TaskStepId == 18 || task.TaskStepId == 19)
                                                                                                                                                              select new { }).Count() > 0 ? 18 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                                                                  where q.Status == 19 && task.TaskStepId == 19
                                                                                                                                                                                                  select new { }).Count() > 0 ? 19 : 18).ToString())),

                                       Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                       DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                       LoanType = fhaRequest.LoanType,
                                       TaskStepId = task.TaskStepId,
                                       ShowStatus = "0"
                                   });

            //Naveen 238 this is for testing query
            //foreach (var a in fhaRequestTasks)
            //{
            //    var test = ((from q in prod_TaskXrefManager.GetProductionSubTasks(a.TaskInstanceId)
            //                 select new { }).Count() == 0 ? 19 : (from q in prod_TaskXrefManager.GetProductionSubTasks(a.TaskInstanceId)
            //                 where q.Status == 17 && a.TaskStepId == 17
            //                 select new { }).Count() > 0 ? 17 : (from q in prod_TaskXrefManager.GetProductionSubTasks(a.TaskInstanceId)
            //                                                     where q.Status == 19 && a.TaskStepId == 19
            //                                                     select new { }).Count() > 0 ? 19 : 17);


            //    var ad = (from q in prod_TaskXrefManager.GetProductionSubTasks(a.TaskInstanceId)
            //              select new { }).Count();

            //}

            // naveen harish commneted below line
            List<int> fhasubtasksInProgress = new List<int>();

            foreach (var task in fhaRequestTasks)
            {
                var fhasubtasks = GetSubTasksByTaskInstanceId(task.TaskInstanceId, "FHA", "").ToList();

                var acd = (from q in fhasubtasks
                           where (q.ViewId == 9 && q.Status == "In Queue") || (q.ViewId == 10 && q.Status == "In Queue")
                           select q.TaskId).ToList();

                if (acd.Count() > 0)
                {
                    fhasubtasksInProgress.Add(
                        task.TaskId
                    );


                }



            }

            fhaRequestTasks = fhaRequestTasks.Where(x => fhasubtasksInProgress.Contains(x.TaskId));



            var productionApTasks = (from appRequest in applicationRequests
                                     join task in productionTasks
             on appRequest.TaskinstanceId equals task.TaskInstanceId
                                     select new
                                     {
                                         task.TaskInstanceId,
                                         task.TaskId,
                                         //TODO:task Name will  be filled when Production Application user story completed
                                         TaskName = appRequest.projectName,
                                         PropertyName = appRequest.PropertyName,
                                         task.SequenceId,
                                         task.PageTypeId,
                                         task.StartTime,
                                         task.AssignedBy,
                                         task.AssignedTo,
                                         ModofiedOn = DateTime.UtcNow,
                                         //ModofiedOn =ControllerHelper.GetTaskXrefLastupdated(task.TaskInstanceId, task.TaskStepId),
                                         //TODO:Lender Name will  be filled when Production Application user story completed
                                         Lender = appRequest.LenderName,
                                         //Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                         Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(((from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                           where (q.Status == 15 && q.ViewId == 1)
                                                                                                                           select new { }).Count() > 0 ? 19 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                               where (q.Status == 18 && q.ViewId == 1)
                                                                                                                                                               select new { }).Count() > 0 ? 18 : 17).ToString())),
                                         Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                         DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
                                         TaskStepId = task.TaskStepId,
                                         ShowStatus = "0"
                                     }).ToList();

           // var av = productionApTasks.Where(x => x.TaskName.Contains("043-22149"));

            var form290AssignedTasks = (from closeRequest in submittedForm290
                                        join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
                                           on closeRequest.TaskinstanceId equals task.TaskInstanceId
                                        select new
                                        {
                                            task.TaskInstanceId,
                                            task.TaskId,
                                            //TODO:task Name will  be filled when Production Application user story completed
                                            TaskName = closeRequest.projectName,
                                            PropertyName = closeRequest.PropertyName,
                                            task.SequenceId,
                                            task.PageTypeId,
                                            task.StartTime,
                                            task.AssignedBy,
                                            task.AssignedTo,
                                            ModofiedOn = DateTime.UtcNow,
                                            //ModofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(task.TaskInstanceId,task.TaskStepId),
                                            //TODO:Lender Name will  be filled when Production Application user story completed
                                            Lender = closeRequest.LenderName,
                                            Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                            Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                            DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                            LoanType = closeRequest.LoanType,
                                            TaskStepId = task.TaskStepId,
                                            ShowStatus = "0"
                                        }).ToList();

            var olistForm290AssignedTasks = form290AssignedTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus = x.ShowStatus
            }).ToList();

            var olistProductionApTasks = productionApTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus = x.ShowStatus
            }).ToList();

            var olistFhaRequestTasks = fhaRequestTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus = x.ShowStatus
            }).ToList();

            foreach (var i in olistProductionApTasks)
            {
                DateTime StartTime = ControllerHelper.GetProdQStartDate(i.TaskInstanceId);
                DateTime lastUpdated = ControllerHelper.GetTaskXrefLastupdated(i.TaskInstanceId, i.TaskStepId);
                i.StartTime = StartTime;
                i.ModofiedOn = lastUpdated;
            }
            foreach (var i in olistForm290AssignedTasks)
            {
                if (i.Status == "In Queue")
                    statusId = 17;
                if (i.Status == "In Process")
                    statusId = 18;
                if (i.Status == "Firm Commitment Request")
                    statusId = 20;
                if (i.Status == "Application Request Completed")
                    statusId = 19;
                if (i.Status == "Firm Commitment Response")
                    statusId = 21;
                if (i.Status == "Application Complete")
                    statusId = 15;
                if (i.Status == "Form290 In Process")
                    statusId = 22;
                if (i.Status == "Form290 Request Completed")
                    statusId = 23;

                var modofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(i.TaskInstanceId, statusId);

                var o1 = olistProductionApTasks.Where(o => o.TaskId == i.TaskId).ToList();
                var o2 = olistFhaRequestTasks.Where(o => o.TaskId == i.TaskId).ToList();
                if (o1.Count() > 0)
                {
                    o1.ForEach(p => p.ModofiedOn = modofiedOn);
                    o1.ForEach(p => p.StartTime = i.StartTime);
                }
                if (o2.Count() > 0)
                {
                    o2.ForEach(p => p.StartTime = i.StartTime);
                    o2.ForEach(p => p.ModofiedOn = modofiedOn);
                }
            }
            var allProductionTasks = olistFhaRequestTasks.Union(olistProductionApTasks).Union(olistForm290AssignedTasks).ToList();

            //karri#264
            if (fhaOrProjName != null && !string.IsNullOrEmpty(fhaOrProjName))
            {
                foreach (var pro in allProductionTasks)
                {
                    if (string.IsNullOrEmpty(pro.PropertyName))
                    {
                        pro.PropertyName = "NA";
                    }

                }
                allProductionTasks = allProductionTasks.Where(x => x.TaskName.ToUpper().Contains(fhaOrProjName.ToString().ToUpper()) || fhaOrProjName.ToString().ToUpper().Contains(x.PropertyName.ToUpper())).ToList();
               // var a = allProductionTasks.Where(x => x.TaskId == 31222);
            }

            //karri#434,uncommented, commented
            //var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First())).Where(q => q.Status == "FHA Request Completed" || q.Status == "Application Complete" || q.Status == "Form290 Request Completed" || q.Status == "In Process");
            //var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.Last())).Where(q => /*q.Status != "FHA Request Completed" && q.Status != "Application Complete" && q.Status != "Form290 Request Completed" && q.Status != "In Process" || q.Status == "FHA Request Completed" || q.Status == "Application Complete" || q.Status == "Form290 Request Completed" ||*/ q.Status == "In Process"||q.Status == "In Proccess" ||q.Status == "Form290 In Process");
            //var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First())).Where(q => q.Status != "FHA Request Completed" && q.Status != "Application Complete" && q.Status != "Form290 Request Completed" && q.Status != "In Process" || q.Status == "FHA Request Completed" || q.Status == "Application Complete" || q.Status == "Form290 Request Completed" || q.Status == "In Process" || q.Status == "In Proccess" || q.Status == "Form290 In Process");
            var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.Last()));

            int totalrecods = groupedTasks.Count();
             var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
             var results = groupedTasks.Skip(pageIndex * pageSize).Take(pageSize);
           // var results = groupedTasks;
            //hareesh added code
            var actualResults = new List<int>();
            foreach (var task in results)
            {
                string prdtype = "";
                if (applicationType == ((Int32)PageType.FhaRequest))
                    prdtype = "FHA";
                else if (applicationType == ((Int32)PageType.ProductionApplication))
                    prdtype = "Production Application";
                else if (applicationType == ((Int32)PageType.ConstructionSingleStage))
                    prdtype = "Single Stage";
                else if (applicationType == ((Int32)PageType.ConstructionTwoStageInitial))
                    prdtype = "2 Stage Initial";
                else if (applicationType == ((Int32)PageType.ConstructionTwoStageFinal))
                    prdtype = "2 Stage Final";
                else if (applicationType == ((Int32)PageType.ClosingAllExceptConstruction))
                    prdtype = "Draft Closing";
                else if (applicationType == ((Int32)PageType.ExecutedClosing))
                    prdtype = "Executed Closing";
                else if (applicationType == ((Int32)PageType.ClosingConstructionInsuredAdvances))
                    prdtype = "closing advances";
                else if (applicationType == ((Int32)PageType.ClosingConstructionInsuranceUponCompletion))
                    prdtype = "closing completion";
                else if (applicationType == ((Int32)PageType.ConstructionManagement))
                    prdtype = "construction mgt";
                else if (applicationType == ((Int32)PageType.Form290))
                    prdtype = "Form 290";
                else if (applicationType == ((Int32)PageType.Amendments))
                    prdtype = "Amendment";

                if (applicationType == 0)
                    prdtype = task.Type;
                // hareesh added below line to get form 290 as in process
                //if (prdtype == "Form 290")
                //    actualResults.Add(task.TaskId);
                var subtasks = GetSubTasksByTaskInstanceId(task.TaskInstanceId, prdtype, "").ToList();
                //var subtasks = GetFHARequestDetail(task.TaskInstanceId, prdtype, "");
                var lstTasks = subtasks.Where(x => x.Status == "Review Completed").ToList();

                var lstUnassignedtask = subtasks.Where(x => x.Status == "Unassigned").ToList();
                if (lstUnassignedtask.Any())
                {
                    task.ShowStatus = "1";
                }
                //var lstTasks = (from q in subtasks
                //                where q.Status == "Review Completed"
                //                where q.Status != "Unassigned" && (q.FhaType == "UnderWriter" || q.FhaType == "Underwriter/Closer")
                //                select q).ToList();
                if (lstTasks.Count() == subtasks.Count())
                {
                    var lstFhaType = subtasks.Where(x => x.FhaType == "UnderWriter" || x.FhaType == "Underwriter/Closer");
                    if (!lstFhaType.Any())
                    {
                        actualResults.Add(task.TaskId);
                    }
                    else
                    {
                        int tid = task.TaskId;
                    }
                    // actualResults.Add(task.TaskId);
                }

            }

            var actualTasks = applicationType > 0 ? results.Where(x => !actualResults.Contains(x.TaskId)) : results;
           // var b = actualTasks.Where(x => x.TaskId == 31222);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = actualTasks.Where(t => t.TaskName != "t").ToList(),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }


        /*
                // old code
                public JsonResult GetAllProductionTasks(string sidx, string sord, int page, int rows, int applicationType, int status)
                {
                    int statusId = 0;
                    int pageIndex = Convert.ToInt32(page) - 1;
                    int pageSize = rows;
                    IEnumerable<TaskModel> productionTasks;
                    if (applicationType == (int)PageType.Form290)
                    {
                        if (status == (int)TaskStep.InProcess) status = (int)TaskStep.Form290Request;
                        if (status == (int)TaskStep.Complete) status = (int)TaskStep.Form290Complete;

                    }
                    if (applicationType == (int)PageType.NotSpecified)
                    {
                        productionTasks = status == 0 ? productionQueue.GetProductionTasks().ToList() : productionQueue.GetProductionTaskByStatus(status).ToList();

                        var a = productionTasks.Where(x => x.TaskId == 29151);
                    }
                    else
                    {
                        if (applicationType != (int)PageType.FhaRequest && status == (int)ProductionAppProcessStatus.Completed)
                            status = (int)ProductionAppProcessStatus.ProjectActionRequestComplete;
                        productionTasks = status == 0 ? productionQueue.GetProductionTaskByType(applicationType) : productionQueue.GetProductionTaskByTypeAndStatus(applicationType, status).ToList();

                    }
                    var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
                    var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

                    var applicationRequests = appProcessManager.GetAppRequests().ToList();
                    var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
                    List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
                    if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
                    {
                        submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
                    }


                    var fhaRequestTasks = (from task in taskModels.Where(m => m.PageTypeId == (int)PageType.FhaRequest)
                                           join fhaRequest in productionQueue.GetFhaRequests(distnictInstanceId).ToList()
                                           //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
                                           on task.TaskInstanceId equals fhaRequest.TaskinstanceId

                                           select new
                                           {
                                               task.TaskInstanceId,
                                               task.TaskId,
                                               TaskName = fhaRequest.projectName,
                                               task.SequenceId,
                                               task.PageTypeId,
                                               task.StartTime,
                                               task.AssignedBy,
                                               task.AssignedTo,
                                               ModofiedOn = fhaRequest.ModifiedOn,
                                               Lender = fhaRequest.LenderName,
                                               Status =
                                               EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(task.TaskStepId.ToString())),
                                               Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                               DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                               LoanType = fhaRequest.LoanType,
                                               TaskStepId = task.TaskStepId
                                           });
                    var productionApTasks = (from appRequest in applicationRequests
                                             join task in productionTasks
                     on appRequest.TaskinstanceId equals task.TaskInstanceId
                                             select new
                                             {
                                                 task.TaskInstanceId,
                                                 task.TaskId,
                                                 //TODO:task Name will  be filled when Production Application user story completed
                                                 TaskName = appRequest.projectName,
                                                 task.SequenceId,
                                                 task.PageTypeId,
                                                 task.StartTime,
                                                 task.AssignedBy,
                                                 task.AssignedTo,
                                                 ModofiedOn = DateTime.UtcNow,
                                                 //ModofiedOn =ControllerHelper.GetTaskXrefLastupdated(task.TaskInstanceId, task.TaskStepId),
                                                 //TODO:Lender Name will  be filled when Production Application user story completed
                                                 Lender = appRequest.LenderName,
                                                 Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                                 Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                                 DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                                 LoanType = appRequest.LoanType,
                                                 TaskStepId = task.TaskStepId
                                             }).ToList();

                    var form290AssignedTasks = (from closeRequest in submittedForm290
                                                join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
                                                   on closeRequest.TaskinstanceId equals task.TaskInstanceId
                                                select new
                                                {
                                                    task.TaskInstanceId,
                                                    task.TaskId,
                                                    //TODO:task Name will  be filled when Production Application user story completed
                                                    TaskName = closeRequest.projectName,
                                                    task.SequenceId,
                                                    task.PageTypeId,
                                                    task.StartTime,
                                                    task.AssignedBy,
                                                    task.AssignedTo,
                                                    ModofiedOn = DateTime.UtcNow,
                                                    //ModofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(task.TaskInstanceId,task.TaskStepId),
                                                    //TODO:Lender Name will  be filled when Production Application user story completed
                                                    Lender = closeRequest.LenderName,
                                                    Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                                    Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                                    DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                                    LoanType = closeRequest.LoanType,
                                                    TaskStepId = task.TaskStepId
                                                }).ToList();

                    var olistForm290AssignedTasks = form290AssignedTasks.Select(x => new AnonymousTaskMode
                    {
                        TaskInstanceId = x.TaskInstanceId,
                        TaskId = x.TaskId,
                        TaskName = x.TaskName,
                        SequenceId = x.SequenceId,
                        PageTypeId = x.PageTypeId,
                        StartTime = x.StartTime,
                        AssignedBy = x.AssignedBy,
                        AssignedTo = x.AssignedTo,
                        ModofiedOn = x.ModofiedOn,
                        Lender = x.Lender,
                        Status = x.Status,
                        Type = x.Type,
                        DaysInQueue = x.DaysInQueue,
                        LoanType = x.LoanType,
                        TaskStepId = x.TaskStepId
                    }).ToList();

                    var olistProductionApTasks = productionApTasks.Select(x => new AnonymousTaskMode
                    {
                        TaskInstanceId = x.TaskInstanceId,
                        TaskId = x.TaskId,
                        TaskName = x.TaskName,
                        SequenceId = x.SequenceId,
                        PageTypeId = x.PageTypeId,
                        StartTime = x.StartTime,
                        AssignedBy = x.AssignedBy,
                        AssignedTo = x.AssignedTo,
                        ModofiedOn = x.ModofiedOn,
                        Lender = x.Lender,
                        Status = x.Status,
                        Type = x.Type,
                        DaysInQueue = x.DaysInQueue,
                        LoanType = x.LoanType,
                        TaskStepId = x.TaskStepId
                    }).ToList();

                    var olistFhaRequestTasks = fhaRequestTasks.Select(x => new AnonymousTaskMode
                    {
                        TaskInstanceId = x.TaskInstanceId,
                        TaskId = x.TaskId,
                        TaskName = x.TaskName,
                        SequenceId = x.SequenceId,
                        PageTypeId = x.PageTypeId,
                        StartTime = x.StartTime,
                        AssignedBy = x.AssignedBy,
                        AssignedTo = x.AssignedTo,
                        ModofiedOn = x.ModofiedOn,
                        Lender = x.Lender,
                        Status = x.Status,
                        Type = x.Type,
                        DaysInQueue = x.DaysInQueue,
                        LoanType = x.LoanType,
                        TaskStepId = x.TaskStepId
                    }).ToList();

                    foreach (var i in olistProductionApTasks)
                    {
                        DateTime StartTime = ControllerHelper.GetProdQStartDate(i.TaskInstanceId);
                        DateTime lastUpdated = ControllerHelper.GetTaskXrefLastupdated(i.TaskInstanceId, i.TaskStepId);
                        i.StartTime = StartTime;
                        i.ModofiedOn = lastUpdated;
                    }
                    foreach (var i in olistForm290AssignedTasks)
                    {
                        if (i.Status == "In Queue")
                            statusId = 17;
                        if (i.Status == "In Process")
                            statusId = 18;
                        if (i.Status == "Firm Commitment Request")
                            statusId = 20;
                        if (i.Status == "Application Request Completed")
                            statusId = 19;
                        if (i.Status == "Firm Commitment Response")
                            statusId = 21;
                        if (i.Status == "Application Complete")
                            statusId = 15;
                        if (i.Status == "Form290 In Process")
                            statusId = 22;
                        if (i.Status == "Form290 Request Completed")
                            statusId = 23;

                        var modofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(i.TaskInstanceId, statusId);

                        var o1 = olistProductionApTasks.Where(o => o.TaskId == i.TaskId).ToList();
                        var o2 = olistFhaRequestTasks.Where(o => o.TaskId == i.TaskId).ToList();
                        if (o1.Count() > 0)
                        {
                            o1.ForEach(p => p.ModofiedOn = modofiedOn);
                            o1.ForEach(p => p.StartTime = i.StartTime);
                        }
                        if (o2.Count() > 0)
                        {
                            o2.ForEach(p => p.StartTime = i.StartTime);
                            o2.ForEach(p => p.ModofiedOn = modofiedOn);
                        }
                    }
                    var allProductionTasks = olistFhaRequestTasks.Union(olistProductionApTasks).Union(olistForm290AssignedTasks).ToList();
                    var groupedTasks = allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First());
                    int totalrecods = groupedTasks.Count();
                    var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                    var results = groupedTasks.Skip(pageIndex * pageSize).Take(pageSize);

                    var jsonData = new
                    {
                        total = totalpages,
                        page,
                        records = totalrecods,
                        rows = results,

                    };
                    return Json(jsonData, JsonRequestBehavior.AllowGet);

                }*/



        //#84 Modified extra parmeter to separate functionality fha request and non Fha request
        public JsonResult GetFHARequestDetail(Guid taskInstanceId, string prdType, string viewsCrdDate)
        {
            var productionSubTasks = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId).ToList();

            //#84 Commneted Old Json data object , creadted object data1 and data2
            //bool isprojTypeFHAReq = false;
            //if (productionSubTasks.Count.Equals(2)
            //    && productionSubTasks[0].ViewId.Equals(9)
            //    && productionSubTasks[1].ViewId.Equals(10))
            //    isprojTypeFHAReq = true;

            //if (isprojTypeFHAReq)
            //{
            //    var jsonData1 = (from data in productionSubTasks
            //                    select new
            //                    {
            //                        data.ViewId,
            //                        data.ModifiedOn,
            //                        data.CompletedOn,
            //                        data.TaskId,
            //                        data.TaskXrefid,
            //                        data.TaskInstanceId,
            //                        AssignedBy = productionQueue.GetUserById(data.AssignedBy).FirstName + " " + productionQueue.GetUserById(data.AssignedBy).LastName,
            //                        AssignedTo = data.AssignedTo != null ? productionQueue.GetUserById(data.AssignedTo).FirstName + " " + productionQueue.GetUserById(data.AssignedTo).LastName : "",
            //                        Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(data.Status.ToString())),
            //                        FhaType = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(data.ViewId.ToString()))
            //                    }).OrderBy(m => m.ViewId);

            //    return Json(jsonData1.OrderBy(x => x.FhaType), JsonRequestBehavior.AllowGet);
            //}

            var Data1 = (from data in productionSubTasks
                         select new
                         {
                             ViewId = data.ViewId,
                             ModifiedOn = data.ModifiedOn,
                             CompletedOn = data.CompletedOn,
                             TaskId = data.TaskId,
                             TaskXrefid = data.TaskXrefid,
                             TaskInstanceId = data.TaskInstanceId,
                             AssignedBy = productionQueue.GetUserById(data.AssignedBy).FirstName + " " + productionQueue.GetUserById(data.AssignedBy).LastName,
                             AssignedTo = data.AssignedTo != null ? productionQueue.GetUserById(data.AssignedTo).FirstName + " " + productionQueue.GetUserById(data.AssignedTo).LastName : "",
                             Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(data.Status.ToString())),
                             FhaType = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(data.ViewId.ToString()))
                         }).ToList();

            //var Data2 = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(1, taskInstanceId)
            //             where Views.ViewId != (int)ProductionView.WLM
            //             select new
            //             {
            //                 Views.ViewId,
            //                 Name = Views.ViewName

            //             }).ToList();

            //#84 
            List<Prod_Taskassigned> jsonData = new List<Prod_Taskassigned>();
            //karri;#85
            List<Prod_Taskassigned> jsonFHAData = new List<Prod_Taskassigned>();

            //naveen ,harish
            List<Prod_Taskassigned> jsonFHAData1 = new List<Prod_Taskassigned>();

            foreach (var q in Data1)
            {
                jsonData.Add(new Prod_Taskassigned
                {
                    ViewId = q.ViewId,
                    ModifiedOn = q.ModifiedOn,
                    CompletedOn = q.CompletedOn,
                    TaskId = q.TaskId,
                    TaskXrefid = q.TaskXrefid,
                    TaskInstanceId = q.TaskInstanceId,
                    AssignedBy = q.AssignedBy,
                    AssignedTo = q.AssignedTo,
                    Status = q.Status,
                    FhaType = q.FhaType
                });
            }

            //karri;#85;functionality only for FHA Requests
            if (prdType == "FHA")
            {
                var fhaRequestViewModel = fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(taskInstanceId);
                string MstrLsProp = "NO";
                if (Convert.ToBoolean(fhaRequestViewModel.IsMasterLeaseProposed))
                    MstrLsProp = "YES";

                //orderby is NOT making the 'Mastr Leased Proposed' text in the last in order
                //hence first sorting the oridingal data, and then ading the master lease text from fha request table
                jsonFHAData = jsonData.OrderBy(x => x.FhaType).ToList();

                jsonFHAData.Add(new Prod_Taskassigned
                {
                    Status = MstrLsProp,
                    FhaType = "Master Lease Proposed?",
                    ModifiedOn = fhaRequestViewModel.ModifiedOn,
                    AssignedTo = " "//this empty space is must,else button to assign will appear

                });

            }
            //end of #85

            if (prdType == "Produciton Application")
            {
                var Data2 = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(1, taskInstanceId)
                             where Views.ViewId != (int)ProductionView.WLM
                             select new
                             {
                                 ViewId = Views.ViewId,
                                 ModifiedOn = "Unassigned",
                                 CompletedOn = "Unassigned",
                                 TaskId = "Unassigned",
                                 TaskXrefid = "Unassigned",
                                 TaskInstanceId = "Unassigned",
                                 AssignedBy = "Unassigned",
                                 AssignedTo = "Unassigned",
                                 Status = "Unassigned",
                                 FhaType = Views.ViewName

                             }).ToList();

                // Data2 = Data2.Where(q => q.Status == "In Proccess").ToList();


                foreach (var q in Data2)
                {
                    jsonData.Add(new Prod_Taskassigned
                    {
                        ViewId = q.ViewId,
                        // added datetime to modifiedon
                        ModifiedOn = viewsCrdDate == null || viewsCrdDate == "" ? new DateTime() : Convert.ToDateTime(Convert.ToDateTime(viewsCrdDate).ToShortDateString()),
                        //ModifiedOn = Convert.ToDateTime(viewsCrdDate.ToShortDateString()),
                        //CompletedOn = Convert.ToDateTime(" "),
                        //TaskId = 0,
                        //TaskXrefid = ,
                        //TaskInstanceId = q.TaskInstanceId,
                        AssignedBy = q.AssignedBy,
                        AssignedTo = q.AssignedTo,
                        Status = q.Status,
                        FhaType = q.FhaType
                    });
                }
            }

            // naveen ,harish
            // harish ,naveen added new subtask functiona for closing process
            if (prdType == "Draft Closing")
            {
                var Data3 = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(1, taskInstanceId)
                             where Views.ViewId != (int)ProductionView.WLM
                             select new
                             {
                                 ViewId = Views.ViewId,
                                 ModifiedOn = "Unassigned",
                                 CompletedOn = "Unassigned",
                                 TaskId = "Unassigned",
                                 TaskXrefid = "Unassigned",
                                 TaskInstanceId = "Unassigned",
                                 AssignedBy = "Unassigned",
                                 AssignedTo = "Unassigned",
                                 Status = "Unassigned",
                                 FhaType = Views.ViewName

                             }).ToList();

                // Data2 = Data2.Where(q => q.Status == "In Proccess").ToList();


                foreach (var q in Data3)
                {
                    jsonData.Add(new Prod_Taskassigned
                    {
                        ViewId = q.ViewId,
                        // added datetime to modifiedon
                        ModifiedOn = viewsCrdDate == null || viewsCrdDate == "" ? new DateTime() : Convert.ToDateTime(Convert.ToDateTime(viewsCrdDate).ToShortDateString()),
                        //ModifiedOn = Convert.ToDateTime(viewsCrdDate.ToShortDateString()),
                        //CompletedOn = Convert.ToDateTime(" "),
                        //TaskId = 0,
                        //TaskXrefid = ,
                        //TaskInstanceId = q.TaskInstanceId,
                        AssignedBy = q.AssignedBy,
                        AssignedTo = q.AssignedTo,
                        Status = q.Status,
                        FhaType = q.FhaType
                    });
                }
            }

            //karri;#85;
            if (prdType == "FHA")
            {
                return Json(jsonFHAData, JsonRequestBehavior.AllowGet);
            }
            //if (prdType == "Draft Closing")
            //{
            //    return Json(jsonFHAData1, JsonRequestBehavior.AllowGet);
            //}
            
            

                return Json(jsonData.OrderBy(x => x.FhaType), JsonRequestBehavior.AllowGet);

        }

        //#84 Modified by Hareesh
        public List<Prod_Taskassigned> GetSubTasksByTaskInstanceId(Guid taskInstanceId, string prdType, string viewsCrdDate)
        {
            var productionSubTasks = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId).ToList();


            //#84 Commneted Old Json data object , creadted object data1 and data2
            //bool isprojTypeFHAReq = false;
            //if (productionSubTasks.Count.Equals(2)
            //    && productionSubTasks[0].ViewId.Equals(9)
            //    && productionSubTasks[1].ViewId.Equals(10))
            //    isprojTypeFHAReq = true;

            //if (isprojTypeFHAReq)
            //{
            //    var jsonData1 = (from data in productionSubTasks
            //                    select new
            //                    {
            //                        data.ViewId,
            //                        data.ModifiedOn,
            //                        data.CompletedOn,
            //                        data.TaskId,
            //                        data.TaskXrefid,
            //                        data.TaskInstanceId,
            //                        AssignedBy = productionQueue.GetUserById(data.AssignedBy).FirstName + " " + productionQueue.GetUserById(data.AssignedBy).LastName,
            //                        AssignedTo = data.AssignedTo != null ? productionQueue.GetUserById(data.AssignedTo).FirstName + " " + productionQueue.GetUserById(data.AssignedTo).LastName : "",
            //                        Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(data.Status.ToString())),
            //                        FhaType = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(data.ViewId.ToString()))
            //                    }).OrderBy(m => m.ViewId);

            //    return Json(jsonData1.OrderBy(x => x.FhaType), JsonRequestBehavior.AllowGet);
            //}

            var Data1 = (from data in productionSubTasks
                         select new
                         {
                             ViewId = data.ViewId,
                             ModifiedOn = data.ModifiedOn,
                             CompletedOn = data.CompletedOn,
                             TaskId = data.TaskId,
                             TaskXrefid = data.TaskXrefid,
                             TaskInstanceId = data.TaskInstanceId,
                             AssignedBy = productionQueue.GetUserById(data.AssignedBy).FirstName + " " + productionQueue.GetUserById(data.AssignedBy).LastName,
                             AssignedTo = data.AssignedTo != null ? productionQueue.GetUserById(data.AssignedTo).FirstName + " " + productionQueue.GetUserById(data.AssignedTo).LastName : "",
                             Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(data.Status.ToString())),
                             FhaType = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(data.ViewId.ToString())),
                             Statusid = data.Status

                         }).ToList();

            //var Data2 = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(1, taskInstanceId)
            //             where Views.ViewId != (int)ProductionView.WLM
            //             select new
            //             {
            //                 Views.ViewId,
            //                 Name = Views.ViewName

            //             }).ToList();

            //#84 
            List<Prod_Taskassigned> jsonData = new List<Prod_Taskassigned>();
            //karri;#85
            List<Prod_Taskassigned> jsonFHAData = new List<Prod_Taskassigned>();
            // Naveen and harish added list 20-11-2019
            List<Prod_Taskassigned> jsonFHAData1 = new List<Prod_Taskassigned>();

            foreach (var q in Data1)
            {
                jsonData.Add(new Prod_Taskassigned
                {
                    ViewId = q.ViewId,
                    ModifiedOn = q.ModifiedOn,
                    CompletedOn = q.CompletedOn,
                    TaskId = q.TaskId,
                    TaskXrefid = q.TaskXrefid,
                    TaskInstanceId = q.TaskInstanceId,
                    AssignedBy = q.AssignedBy,
                    AssignedTo = q.AssignedTo,
                    Status = q.Status,
                    FhaType = q.FhaType,
                    //Statusid = q.Statusid,
                });
            }

            //karri;#85;functionality only for FHA Requests
            if (prdType == "FHA")
            {
                var fhaRequestViewModel = fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(taskInstanceId);
                string MstrLsProp = "NO";
                if (Convert.ToBoolean(fhaRequestViewModel.IsMasterLeaseProposed))
                    MstrLsProp = "YES";

                //orderby is NOT making the 'Mastr Leased Proposed' text in the last in order
                //hence first sorting the oridingal data, and then ading the master lease text from fha request table
                jsonFHAData = jsonData.OrderBy(x => x.FhaType).ToList();

                jsonFHAData.Add(new Prod_Taskassigned
                {
                    Status = MstrLsProp,
                    FhaType = "Master Lease Proposed?",
                    ModifiedOn = fhaRequestViewModel.ModifiedOn,
                    AssignedTo = " "//this empty space is must,else button to assign will appear

                });

            }
            //end of #85

            if (prdType == "Production Application" || prdType == "Produciton Application") 
            {
                var Data2 = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(1, taskInstanceId)
                             where Views.ViewId != (int)ProductionView.WLM
                             select new
                             {
                                 ViewId = Views.ViewId,
                                 ModifiedOn = "Unassigned",
                                 CompletedOn = "Unassigned",
                                 TaskId = "Unassigned",
                                 TaskXrefid = "Unassigned",
                                 TaskInstanceId = "Unassigned",
                                 AssignedBy = "Unassigned",
                                 AssignedTo = "Unassigned",
                                 Status = "Unassigned",
                                 FhaType = Views.ViewName

                             }).ToList();

                // Data2 = Data2.Where(q => q.Status == "In Proccess").ToList();


                foreach (var q in Data2)
                {
                    jsonData.Add(new Prod_Taskassigned
                    {
                        ViewId = q.ViewId,
                        // added datetime to modifiedon
                        ModifiedOn = viewsCrdDate == null || viewsCrdDate == "" ? new DateTime() : Convert.ToDateTime(Convert.ToDateTime(viewsCrdDate).ToShortDateString()),
                        //ModifiedOn = Convert.ToDateTime(viewsCrdDate.ToShortDateString()),
                        //CompletedOn = Convert.ToDateTime(" "),
                        //TaskId = 0,
                        //TaskXrefid = ,
                        //TaskInstanceId = q.TaskInstanceId,
                        AssignedBy = q.AssignedBy,
                        AssignedTo = q.AssignedTo,
                        Status = q.Status,
                        FhaType = q.FhaType
                    });
                }
            }

            // harish ,naveen added new subtask functiona for closing process
            if (prdType == "Draft Closing" || prdType == "closing advances" || prdType == "closing advances" || prdType == "closing completion")
            {
                var Data3 = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(1, taskInstanceId)
                             where Views.ViewId != (int)ProductionView.WLM
                             select new
                             {
                                 ViewId = Views.ViewId,
                                 ModifiedOn = "Unassigned",
                                 CompletedOn = "Unassigned",
                                 TaskId = "Unassigned",
                                 TaskXrefid = "Unassigned",
                                 TaskInstanceId = "Unassigned",
                                 AssignedBy = "Unassigned",
                                 AssignedTo = "Unassigned",
                                 Status = "Unassigned",
                                 FhaType = Views.ViewName

                             }).ToList();

                // Data2 = Data2.Where(q => q.Status == "In Proccess").ToList();


                foreach (var q in Data3)
                {
                    jsonData.Add(new Prod_Taskassigned
                    {
                        ViewId = q.ViewId,
                        // added datetime to modifiedon
                        ModifiedOn = viewsCrdDate == null || viewsCrdDate == "" ? new DateTime() : Convert.ToDateTime(Convert.ToDateTime(viewsCrdDate).ToShortDateString()),
                        //ModifiedOn = Convert.ToDateTime(viewsCrdDate.ToShortDateString()),
                        //CompletedOn = Convert.ToDateTime(" "),
                        //TaskId = 0,
                        //TaskXrefid = ,
                        //TaskInstanceId = q.TaskInstanceId,
                        AssignedBy = q.AssignedBy,
                        AssignedTo = q.AssignedTo,
                        Status = q.Status,
                        FhaType = q.FhaType
                    });
                }
            }

            //karri;#85;
            if (prdType == "FHA")
                return jsonFHAData;
            // added naveen and harish
            //if (prdType == "Draft Closing")
            //    return jsonFHAData1;
            
            return jsonData;

        }


        public JsonResult GetAdvancedSearch(string sidx, string sord, int page, int rows, int applicationType, string dateFrom, string dateTo, int lenderId, int? loanType, string searchText)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            IEnumerable<FilteredProductionTasksModel> productionTasks;
            int loanTypeId = loanType == null ? 0 : (int)loanType;
            var fromUTCDate = dateFrom == null ? DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) : TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(dateFrom));
            var toUTCDate = dateTo == null ? DateTime.Now : TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(dateTo));
            productionTasks = productionQueue.GetFilteredProductionTasks(applicationType, fromUTCDate, toUTCDate.AddDays(1), lenderId, loanTypeId);
            //var e = productionTasks.Where(x => x.TaskId == 31279);
            //karri#205
            if (searchText != null && !string.IsNullOrEmpty(searchText))
            {
                //foreach (var pro in productionTasks)
                //{
                //    if (string.IsNullOrEmpty(pro.PropertyName))
                //    {
                //        pro.PropertyName = "NA";
                //    }
                //}
               // productionTasks = productionTasks.Where(m => searchText.ToUpper().Contains(m.TaskName.ToUpper()) || searchText.ToUpper().Contains(m.PropertyName.ToUpper())).ToList();
               // productionTasks = productionTasks.Where(m => searchText.ToUpper().Contains(m.TaskName.ToUpper())).ToList();
                productionTasks = productionQueue.GetFilteredProductionTasks(applicationType, fromUTCDate, toUTCDate.AddDays(1), lenderId, loanTypeId).Where(m => m.TaskName.ToUpper().Contains(searchText.ToUpper())).ToList();
            }
            else
            {
                productionTasks = productionQueue.GetFilteredProductionTasks(applicationType, fromUTCDate, toUTCDate.AddDays(1), lenderId, loanTypeId);
            }

            //karri#434;restricting the FHA Requests only for unassaigned tab
            productionTasks = (from q in productionTasks
                               where q.PageTypeId == 4 && (q.AssignedTo == "Queue" || q.AssignedTo == null)
                               select q).ToList();

            var jsonData = productionTasks;
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void AssignToMe(ProductionTaskAssignmentModel model)
        {
            var currentUser = UserPrincipal.Current;
            if (RoleManager.IsProductionUser(currentUser.UserName) || RoleManager.IsProductionWLM(currentUser.UserName))
            {
                model.AssignedToUserId = currentUser.UserId;
                model.CurrentUserId = currentUser.UserId;
                model.UserName = currentUser.UserName;
                model.Status = (int)ProductionFhaRequestStatus.InProcess;
                model.AssignedDate = DateTime.UtcNow;
                if (model.PageTypeId == (int)PageType.Form290)
                {
                    productionQueue.AssignForm290Task(model);
                }
                else
                {

                    if (model.TaskInstanceId == Guid.Empty)
                        prod_TaskXrefManager.AssignProductionFhaRequest(model);
                    else
                    {
                        productionQueue.AssignProductionFhaInsert(model);
                        fhaNumberRequestManager.FhataskAssignedfromQueDateUpdate(model.TaskInstanceId);
                    }
                }

            }

        }

        [HttpPost]
        public void Assign(ProductionTaskAssignmentModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                //Save Entry to Xref Table
                if (model.SelectedViewIds != null || model.SelectedViewIds != string.Empty)
                {
                    var listviewIds = model.SelectedViewIds.Split(',');
                    foreach (string id in listviewIds)
                    {
                        model.ViewId = Convert.ToInt32(id);
                        prod_TaskXrefManager.AddTaskXref(model);
                    }

                }
                // Save ReviewFileStatus
                int reviewerUserId = (int)model.AssignedTo;
                int currentUserId = UserPrincipal.Current.UserId;
                taskManager.SaveReviewFileStatus(model.TaskInstanceId, reviewerUserId, currentUserId, model.ViewId);


            }

        }

        public ActionResult GetFhaRequestFormDetail(Guid taskInstanceId)
        {
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            if (task != null)
            {
                if (task.PageTypeId == (int)PageType.FhaRequest)
                {
                    var fhaRequestViewModel = fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(taskInstanceId);
                    FHARequestController.SetModelValues(fhaRequestViewModel);
                    FHARequestController.SetDropdownValuesForFHARequestForm(fhaRequestViewModel);
                    fhaRequestViewModel.IsDisclaimerAccepted = true;
                    fhaRequestViewModel.FhaRequestType = (int)ProductionView.InsertFha;
                    fhaRequestViewModel.IsQueue = true;
                    return View("~/Views/Production/FHARequest/FHARequestLenderReadOnlyView.cshtml", fhaRequestViewModel);
                }
                else
                {
                    //TODDO:ProductionApplication Readonly form will be populated here;
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        private void PopulateLookUps()
        {
            var applicationType = new[]
            {
                new {ID = (int)PageType.NotSpecified, Name = "All"},
                new {ID = (int)PageType.FhaRequest, Name = "FHA Request"},
                new {ID = (int)PageType.ProductionApplication, Name = "Production Application"},
                new {ID = (int)PageType.ConstructionSingleStage, Name = "Construction single Stage Application"},
                new {ID = (int)PageType.ConstructionTwoStageInitial, Name = "Construction two stage Initial Application"},
                new {ID = (int)PageType.ConstructionTwoStageFinal, Name = "Construction two stage Final Application"},
                new {ID = (int)PageType.ClosingAllExceptConstruction, Name = "Non Construction Draft Closing"},
                 new {ID = (int)PageType.ExecutedClosing, Name = "Non Construction Executed Closing"},
                 new {ID = (int)PageType.ClosingConstructionInsuredAdvances, Name = "Construction Initial Closing"},
                  new {ID = (int)PageType.ClosingConstructionInsuranceUponCompletion, Name = "Construction Final Closing"},
                   new {ID = (int)PageType.ConstructionManagement, Name = "Construction Management"},
                   new {ID = (int)PageType.Form290, Name="Form 290"},
                   new {ID = (int)PageType.Amendments, Name="Amendment"}

            }.ToList();

            var status = new[]
            {
                 new {StatusID=(int)ProductionFhaRequestStatus.NotSpecified,StatusName="All"},
                 new {StatusID=(int)ProductionFhaRequestStatus.InProcess,StatusName="In Process"},
                 new {StatusID=(int)ProductionFhaRequestStatus.Completed,StatusName="Completed"}

            }.ToList();
            ViewBag.StatusID = new SelectList(status, "StatusID", "StatusName", 0);
            ViewBag.ApplicationType = new SelectList(applicationType, "ID", "Name", 0);
            List<ProjectTypeModel> placeHolder = new List<ProjectTypeModel>(){
                new ProjectTypeModel (){ProjectTypeId=0,ProjectTypeName="All"}
            };
            List<ProjectTypeModel> loanTypes = fhaNumberRequestManager.GetAllProjectTypes().ToList();
            ViewBag.LoanType = new SelectList(placeHolder.Union(loanTypes), "ProjectTypeId", "ProjectTypeName", 0);

            List<FhaSubmittedLendersModel> LenderplaceHolder = new List<FhaSubmittedLendersModel>(){
                new FhaSubmittedLendersModel (){LenderID=0,Lender_Name="All"}
            };
            var allLenders = fhaNumberRequestManager.GetFhaSubmittedLenders().ToList();
            ViewBag.LenderID = new SelectList(LenderplaceHolder.Union(allLenders), "LenderID", "Lender_Name");
        }


        public ActionResult ApplicationAssignment(Guid taskInstanceId)
        {
            var taskAssignmentModel = new ProductionTaskAssignmentModel { TaskInstanceId = taskInstanceId };
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            if (task != null)
            {
                taskAssignmentModel.TaskId = task.TaskId;
                taskAssignmentModel.PageTypeId = (int)task.PageTypeId;
            }
            var curentUser = UserPrincipal.Current.UserName;
            taskAssignmentModel.isWLM = RoleManager.IsProductionWLM(curentUser);
            var productionUser = (from prodUsers in productionQueue.GetUnAssignedProductionUsers()
                                  select new
                                  {
                                      prodUsers.UserID,
                                      Name = String.Format("{0} {1} {2} {3} {4}", prodUsers.FirstName, prodUsers.LastName, "( ", prodUsers.RoleName, " )")

                                  });

            var AvailableViews = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(1, taskInstanceId)
                                  where Views.ViewId != (int)ProductionView.WLM
                                  select new
                                  {
                                      Views.ViewId,
                                      Name = Views.ViewName

                                  });

            if (task.PageTypeId == (int)PageType.Amendments)
            {
                AvailableViews = AvailableViews.Where(p => p.ViewId == (int)ProductionView.UnderWriter);
            }
            //if (!taskAssignmentModel.isWLM)
            //{
            //    productionUser = productionUser.ToList().Where(a => a.UserID == UserPrincipal.Current.UserId);
            //}

            ViewBag.AssignedToUserId = new SelectList(productionUser, "UserID", "Name");
            ViewBag.AssignedToViewId = new SelectList(AvailableViews, "ViewId", "Name");

            return View("~/Views/Production/ProductionQueue/AppTaskAsignment.cshtml", taskAssignmentModel);
        }

        [HttpPost]
        public void UpdateApplicationTaskAsignment(ProductionTaskAssignmentModel model)
        {
            //umesh
            model.IsReviewer = false;
            model.AssignedBy = UserPrincipal.Current.UserId;
            model.Status = (int)ProductionAppProcessStatus.InProcess;
            model.ViewId = model.AssignedToViewId;
            model.ModifiedOn = DateTime.UtcNow;
            model.ModifiedBy = UserPrincipal.Current.UserId;
            model.AssignedTo = model.AssignedToUserId;
            model.TaskXrefid = Guid.NewGuid();
            model.AssignedDate = DateTime.UtcNow;
            prod_TaskXrefManager.AddTaskXref(model);



            //Logic to Separate the UnderWriter
            if (model.AssignedToViewId == 1)
            {
                var task = taskManager.GetLatestTaskByTaskInstanceId(model.TaskInstanceId);
                if (task != null)
                {

                    task.AssignedTo = productionQueue.GetUserById(model.AssignedToUserId).UserName;
                    if (task.PageTypeId == (int)PageType.Amendments)
                    {
                        task.TaskStepId = (int)TaskStep.InProcess;
                    }
                    taskManager.UpdateTaskAssignment(task);
                }
            }

            taskManager.SaveReviewFileStatus(model.TaskInstanceId, model.AssignedToUserId, UserPrincipal.Current.UserId, model.AssignedToViewId);

            var taskbyId = taskManager.GetTaskById(model.TaskId);

            //New code for pushing out of queue
            var Xreflist = prod_TaskXrefManager.GetProductionSubTasksforIR(model.TaskInstanceId);
            if (Xreflist != null)
            {


                var AvailableViews = (from Views in prodviewmanager.GetViewsbyType(1)
                                      select new
                                      {
                                          Views.ViewId,
                                          Name = Views.ViewName

                                      });

                if (Xreflist.Count() == AvailableViews.Count())
                {
                    //Update to make it in Process
                    var task = taskManager.GetLatestTaskByTaskInstanceId(model.TaskInstanceId);
                    task.TaskStepId = 18;
                    taskManager.UpdateTask(task);
                }
            }
            var AppProcessModel = new OPAViewModel();
            var parenttask = taskManager.GetLatestTaskByTaskInstanceId(model.TaskInstanceId);
            AppProcessModel = _projectActionFormManager.GetOPAByTaskInstanceId(parenttask.TaskInstanceId);
            AppProcessModel.pageTypeId = model.PageTypeId;
            var usermodel = productionQueue.GetUserById(model.AssignedToUserId);
            backgroundJobManager.SendAppProcessAssignmentEmail(new EmailManager(), usermodel.UserName, AppProcessModel);
            //// US 1929 : Send Ammendment Assigned email to UW/Closer
            backgroundJobManager.SendAmmendmentAssignmentEmailToUWCloser(new EmailManager(), usermodel.UserName, AppProcessModel);

        }

        [HttpPost]
        public void SelfAssignApplicationTask(ProductionTaskAssignmentModel model)
        {
            //umesh
            model.IsReviewer = false;
            model.AssignedBy = UserPrincipal.Current.UserId;
            model.Status = (int)ProductionAppProcessStatus.InProcess;
            model.ViewId = model.AssignedToViewId;
            model.ModifiedOn = DateTime.UtcNow;
            model.ModifiedBy = UserPrincipal.Current.UserId;
            model.AssignedTo = UserPrincipal.Current.UserId;
            model.TaskXrefid = Guid.NewGuid();
            model.AssignedDate = DateTime.UtcNow;
            prod_TaskXrefManager.AddTaskXref(model);

            //Logic to Separate the UnderWriter
            if (model.AssignedToViewId == 1)
            {
                var task = taskManager.GetLatestTaskByTaskInstanceId(model.TaskInstanceId);
                if (task != null)
                {

                    task.AssignedTo = UserPrincipal.Current.UserName;
                    if (task.PageTypeId == (int)PageType.Amendments)
                    {
                        task.TaskStepId = (int)TaskStep.InProcess;
                    }
                    taskManager.UpdateTaskAssignment(task);
                }
            }

            taskManager.SaveReviewFileStatus(model.TaskInstanceId, UserPrincipal.Current.UserId, UserPrincipal.Current.UserId, model.AssignedToViewId);


            //New code for pushing out of queue
            var Xreflist = prod_TaskXrefManager.GetProductionSubTasksforIR(model.TaskInstanceId);
            if (Xreflist != null)
            {


                var AvailableViews = (from Views in prodviewmanager.GetViewsbyType(1)
                                      select new
                                      {
                                          Views.ViewId,
                                          Name = Views.ViewName

                                      });

                if (Xreflist.Count() == AvailableViews.Count())
                {
                    //Update to make it in Process
                    var task = taskManager.GetLatestTaskByTaskInstanceId(model.TaskInstanceId);
                    task.TaskStepId = 18;
                    taskManager.UpdateTask(task);
                }
            }

            var AppProcessModel = new OPAViewModel();
            var parenttask = taskManager.GetLatestTaskByTaskInstanceId(model.TaskInstanceId);
            AppProcessModel = _projectActionFormManager.GetOPAByTaskInstanceId(parenttask.TaskInstanceId);

            var usermodel = productionQueue.GetUserById(model.AssignedTo);
            //no email for 'Assign to Me'
            //backgroundJobManager.SendAppProcessAssignmentEmail(new EmailManager(), usermodel.UserName, AppProcessModel);



        }

        //Production Note
        public ActionResult GetNoteByTask(Guid taskInstanceId)
        {
            List<Prod_NoteModel> notes = prod_noteManager.GetAllTaskNotes(taskInstanceId).ToList();
            foreach (var note in notes)
            {
                note.CreatedByUserName = productionQueue.GetUserById(note.CreatedBy).UserName;
            }

            if (notes.Count < 1)
            {
                notes.Add(new Prod_NoteModel
                {
                    TaskInstanceId = taskInstanceId
                });
            }
            return View("~/Views/Production/ProductionQueue/Notes.cshtml", notes);
        }

        public int AddNote(Guid taskInstanceId, string notes)
        {
            var noteId = 0;
            if (taskInstanceId != null && notes != "")
            {
                var userNote = new Prod_NoteModel
                {
                    TaskInstanceId = taskInstanceId,
                    DateCreated = DateTime.UtcNow,
                    CreatedBy = UserPrincipal.Current.UserId,
                    Note = notes
                };
                noteId = prod_noteManager.AddNote(userNote);
            }

            return noteId;

        }

        public void Deletenote(int noteId)
        {
            prod_noteManager.DeleteNote(noteId);
        }
        public ActionResult GetSharepointDateView(Guid taskInstanceId, string pAssignedTo = null, string pProjectStage = null, int sharePointSection = 0, string pOrderBy = null)
        {
            ViewBag.taskInstanceId = taskInstanceId;
            // var task = taskManager.GetTasksByTaskInstanceId(taskInstanceId).FirstOrDefault();

            var types = productionQueue.GetApplicationAndClosingType(taskInstanceId);
            var appandclosingType = (from appAndClosetype in types.Where(m => m.PageTypeId != (int)PageType.Form290)
                                     select new
                                     {
                                         ID = appAndClosetype.TaskInstanceId,
                                         Name = EnumType.GetEnumDescription(EnumType.Parse<ApplicationAndClosingType>(appAndClosetype.PageTypeId.ToString()))
                                     });
            //// Implementation for Single Page Sharepoint screeen
            //if (IsClosingCompleted(taskInstanceId))
            //{
            //     List<ProdApplicationSharepointData> singleSharepointDataList = new List<ProdApplicationSharepointData>();
            //       foreach(var appType in types)
            //       {

            //            var singleSharepointData = new ProdApplicationSharepointData();
            //            var sharepointData = sharepointScreenManager.GetSharepointDataById(appType.TaskInstanceId);
            //            //var fhaRequest = fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(sharepointData.TaskinstanceId);
            //            singleSharepointData.ProdAppName = EnumType.GetEnumDescription(EnumType.Parse<ApplicationAndClosingType>(appType.PageTypeId.ToString()));
            //            singleSharepointData.ApplicationDetails = fhaNumberRequestManager.GetApplicationDetailsForSharePointScreen(appType.TaskInstanceId);
            //            singleSharepointData.GeneralInformation = appProcessManager.GetGeneralInfoDetailsForSharepoint(appType.TaskInstanceId);
            //            singleSharepointData.MiscellaneousInformation = GetMisellaneousInfo(taskInstanceId);
            //            singleSharepointData.ReviewersList = GetAllReviewersInfo(appType.TaskInstanceId);
            //            singleSharepointData.ClosingInfo = (appType.PageTypeId == (int)ApplicationAndClosingType.closingInital
            //                                                  || appType.PageTypeId == (int)ApplicationAndClosingType.closingFinal) == true ? GetClosingInfo(appType.TaskInstanceId)
            //                                                   : new ClosingInfo() { TaskInstanceId = appType.TaskInstanceId };
            //            singleSharepointData.PageTypeId = appType.PageTypeId;
            //            singleSharepointDataList.Add(singleSharepointData);
            //      }

            //    return View("~/Views/Production/ProductionQueue/SharepointSinglePage/SharepointSinglePage.cshtml", singleSharepointDataList);  
            //}



            //End of single page share point screen
            if (string.IsNullOrEmpty(pAssignedTo))
            {
                if (!string.IsNullOrEmpty(pOrderBy))
                {
                    if (Convert.ToInt32(pOrderBy) == (int)SharepointSectionFromMyTasks.Amendment)
                    {
                        pAssignedTo = SharepointSectionFromMyTasks.Amendment.ToString();
                        ViewBag.ProjectStage = "Amendments";
                    }
                    if (Convert.ToInt32(pOrderBy) == (int)SharepointSectionFromMyTasks.Application)
                    {
                        pAssignedTo = SharepointSectionFromMyTasks.Application.ToString();
                        ViewBag.ProjectStage = "Application";
                    }
                }

            }
            ViewBag.ProductionAppType = new SelectList(appandclosingType, "ID", "Name", 0);
            //ViewBag.ProjectStage = pProjectStage;
            ViewBag.AssignedTo = pAssignedTo;
            ViewBag.SharePointSection = sharePointSection;

            if (pProjectStage == "Draft Closing")
            {
                ViewBag.ProjectStage = (from closingType in appandclosingType.Where(m => m.Name == "Non-Construction Draft Closing")
                                        select new
                                        {
                                            Id = closingType.ID
                                        }).FirstOrDefault().Id;
            }

            var sharePointData = new ProdApplicationSharepointData();

            //Get Application Details
            if (appandclosingType != null && appandclosingType.Count() > 0 && appandclosingType.First() != null)
            {
                sharePointData.ApplicationDetails = fhaNumberRequestManager.GetApplicationDetailsForSharePointScreen(appandclosingType.First().ID);
            }
            else
            {
                sharePointData.ApplicationDetails = new ApplicationDetailViewModel();
            }
            if (sharePointData.AmendmentInfo == null)
            {
                sharePointData.AmendmentInfo = new Prod_FormAmendmentTaskModel();
            }
            else
            {
                sharePointData.AmendmentInfo.DAPCompletedDate = DateTime.Today.Date;
                sharePointData.AmendmentInfo.LoanAmount = 0.0M;
            }
            //sharePointData.GeneralInformation = new GeneralInformationViewModel();
            //sharePointData.MiscellaneousInformation = new MiscellaneousInformationViewModel();
            return View("~/Views/Production/ProductionQueue/SharepointDataEntry.cshtml", sharePointData);
        }
        public ActionResult GetSharepointForPdf(Guid taskInstanceId)
        {
            var types = productionQueue.GetApplicationAndClosingType(taskInstanceId);
            List<ProdApplicationSharepointData> singleSharepointDataList = new List<ProdApplicationSharepointData>();
            foreach (var appType in types)
            {

                var singleSharepointData = new ProdApplicationSharepointData();
                var sharepointData = sharepointScreenManager.GetSharepointDataById(appType.TaskInstanceId);
                //var fhaRequest = fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(sharepointData.TaskinstanceId);

                if (appType.PageTypeId == (int)PageType.Amendments)
                {
                    string wlmUsername = string.Empty;
                    string prodUsername = string.Empty;
                    string dtsUWCloser = string.Empty;
                    string dtsWLM = string.Empty;
                    string DtsDAPCloser = string.Empty;
                    List<Prod_FormAmendmentTaskModel> olistProd_FormAmendmentTaskModel = new List<Prod_FormAmendmentTaskModel>();
                    Prod_FormAmendmentTaskModel objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
                    singleSharepointData.AmendmentList = new List<AmendmentsModel>();
                    olistProd_FormAmendmentTaskModel = _projectActionFormManager.GetFormAmendmentByTaskInstanceId(appType.TaskInstanceId).ToList();

                    if (olistProd_FormAmendmentTaskModel.Count() > 0)
                    {
                        objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.OrderByDescending(p => p.ModifiedOn).FirstOrDefault();
                    }
                    else
                    {
                        objProd_FormAmendmentTaskModel = olistProd_FormAmendmentTaskModel.FirstOrDefault();

                    }
                    StringBuilder builder = new StringBuilder();
                    if (!string.IsNullOrEmpty(objProd_FormAmendmentTaskModel.Party_Propert_Name))
                        builder.Append(AmendmentTypeSP.Party_Propert_Name_cb.GetDescription() + "/");
                    if (!string.IsNullOrEmpty(objProd_FormAmendmentTaskModel.Party_Property_Address_Name))
                        builder.Append(AmendmentTypeSP.Party_Property_Address_Name_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.LoanAmount > 0.0M)
                        builder.Append(AmendmentTypeSP.LoanAmount_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.InterestRate > 0.0M)
                        builder.Append(AmendmentTypeSP.InterestRate_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.MonthlyPayment > 0.0M)
                        builder.Append(AmendmentTypeSP.MonthlyPayment_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.DifferentMonthlyPayment > 0.0M)
                        builder.Append(AmendmentTypeSP.DifferentMonthlyPayment_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.CommitmentTerminationDate.HasValue)
                        builder.Append(AmendmentTypeSP.CommitmentTerminationDate_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.R4RAmount > 0.0M)
                        builder.Append(AmendmentTypeSP.R4RAmount_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.CriticalRepairCost > 0.0M)
                        builder.Append(AmendmentTypeSP.CriticalRepairCost_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.RemainingRepairCostExihibitC > 0.0M)
                        builder.Append(AmendmentTypeSP.RemainingRepairCostExihibitC_cb.ToString() + "/");
                    if (objProd_FormAmendmentTaskModel.SpecialCondition > 0)
                        builder.Append(AmendmentTypeSP.SpecialCondition_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.AnnualLeasePayment > 0.0M)
                        builder.Append(AmendmentTypeSP.AnnualLeasePayment_cb.GetDescription() + "/");
                    if (!string.IsNullOrEmpty(objProd_FormAmendmentTaskModel.Other))
                        builder.Append(AmendmentTypeSP.Other_cb.GetDescription() + "/");
                    if (objProd_FormAmendmentTaskModel.ExhibitLetter > 0)
                        builder.Append(AmendmentTypeSP.ExhibitLetter_cb.GetDescription() + "/");

                    prodUsername = accountManager.GetUserNameById(objProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId);
                    wlmUsername = accountManager.GetUserNameById(objProd_FormAmendmentTaskModel.WLMSignatureId);
                    if (!string.IsNullOrEmpty(wlmUsername))
                    {
                        if (objProd_FormAmendmentTaskModel.ModifiedOn.HasValue)
                        {
                            dtsWLM = objProd_FormAmendmentTaskModel.ModifiedOn.Value.ToShortDateString();
                            dtsUWCloser = objProd_FormAmendmentTaskModel.CreatedOn.ToShortDateString();
                        }
                        else
                        {
                            dtsWLM = objProd_FormAmendmentTaskModel.CreatedOn.ToShortDateString();
                            dtsUWCloser = objProd_FormAmendmentTaskModel.CreatedOn.ToShortDateString();
                        }
                    }

                    AmendmentsModel objAmendmentsModel = new AmendmentsModel
                    {
                        AmendmentNumber = objProd_FormAmendmentTaskModel.FirmAmendmentNum,
                        AmendmentTemplate = "Request " + objProd_FormAmendmentTaskModel.FirmAmendmentNum.ToString(),
                        AmendmentType = builder.ToString(),
                        AssignedUWCloser = accountManager.GetFullUserNameById(objProd_FormAmendmentTaskModel.AuthorizedAgentSignatureId),
                        AssignedWLM = accountManager.GetFullUserNameById(objProd_FormAmendmentTaskModel.WLMSignatureId),
                        DAPCompletedByCloser = objProd_FormAmendmentTaskModel.DAPCompletedDate,
                        WLMDts = dtsWLM,
                        UWCloserDts = dtsUWCloser,
                        FHANumber = objProd_FormAmendmentTaskModel.FHANumber,
                        SubmittedBy = accountManager.GetFullUserNameById(objProd_FormAmendmentTaskModel.CreatedBy),
                        SubmittedOn = objProd_FormAmendmentTaskModel.ModifiedOn.Value

                    };
                    singleSharepointData.AmendmentList.Add(objAmendmentsModel);
                }
                else
                {
                    singleSharepointData.ProdAppName = EnumType.GetEnumDescription(EnumType.Parse<ApplicationAndClosingType>(appType.PageTypeId.ToString()));
                    singleSharepointData.ApplicationDetails = fhaNumberRequestManager.GetApplicationDetailsForSharePointScreen(appType.TaskInstanceId);
                    singleSharepointData.GeneralInformation = (appType.PageTypeId == (int)PageType.ClosingAllExceptConstruction ||
                                                              appType.PageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion ||
                                                              appType.PageTypeId == (int)PageType.ClosingConstructionInsuredAdvances) == true ? new GeneralInformationViewModel() { TaskInstanceId = appType.TaskInstanceId } :
                                                              appProcessManager.GetGeneralInfoDetailsForSharepoint(appType.TaskInstanceId);
                    singleSharepointData.MiscellaneousInformation = (appType.PageTypeId == (int)PageType.ClosingAllExceptConstruction ||
                                                              appType.PageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion ||
                                                              appType.PageTypeId == (int)PageType.ClosingConstructionInsuredAdvances) == true ? new MiscellaneousInformationViewModel() { TaskInstanceId = appType.TaskInstanceId }
                                                              : GetMisellaneousInfo(appType.TaskInstanceId);
                    singleSharepointData.ReviewersList = GetAllReviewersInfo(appType.TaskInstanceId);
                    singleSharepointData.ClosingInfo = (appType.PageTypeId == (int)ApplicationAndClosingType.closingInital
                                                          || appType.PageTypeId == (int)ApplicationAndClosingType.closingFinal) == true ? GetClosingInfo(appType.TaskInstanceId)
                                                           : new ClosingInfo() { TaskInstanceId = appType.TaskInstanceId };
                    singleSharepointData.PageTypeId = appType.PageTypeId;
                    singleSharepointData.AmendmentList = new List<AmendmentsModel>();
                }
                singleSharepointDataList.Add(singleSharepointData);
            }
            var closingInstance = types.Where(m => m.PageTypeId == (int)PageType.ClosingAllExceptConstruction || m.PageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion);
            var closingTaskInstanceId = closingInstance.Any() != false ? closingInstance.FirstOrDefault().TaskInstanceId : Guid.Empty;
            ViewBag.ClosingTaskInstanceId = closingTaskInstanceId;
            /***
             * checking  if pdf is generated or not.
             * 
             * */
            bool isPdfGenerated = false;
            var allFiles = taskManager.GetAllTaskFileByTaskInstanceId(closingTaskInstanceId);
            if (allFiles.Any())
            {
                var sharepointPdf = allFiles.Where(m => m.FileType == FileType.SharePointPdf).LastOrDefault();
                if (null != sharepointPdf)
                {
                    if (!string.IsNullOrEmpty(sharepointPdf.API_upload_status) && sharepointPdf.API_upload_status.ToLower() == "success")
                    {
                        isPdfGenerated = true;
                    }
                }
            }
            ViewBag.IsPdfGenerated = isPdfGenerated;
            return View("~/Views/Production/ProductionQueue/SharepointSinglePage/SharepointSinglePage.cshtml", singleSharepointDataList);
        }
        public JsonResult ClosingCompleted(Guid taskInstanceId)
        {
            var isClosingCompleted = IsClosingCompleted(taskInstanceId);
            return Json(isClosingCompleted, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetReviewerInfo(Guid taskInstanceId, int reviewertype)
        {
            var subTask = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId).Where(m => m.ViewId == reviewertype).FirstOrDefault();
            ViewBag.taskInstanceId = taskInstanceId;
            if (subTask == null)
                return PartialView("~/Views/Production/ProductionQueue/_ReviewersInfo.cshtml", new ReviewerInfo());
            var user = productionQueue.GetUserById(subTask.AssignedTo);
            var sharepointData = sharepointScreenManager.GetSharepointDataById(taskInstanceId);


            var selectedColumn = getColumnProperty(reviewertype);
            /** Reflection to get column name and value 
             *  to update reviewer comment column
             **/
            var columnValue = sharepointData == null ? null : sharepointData.GetType().GetProperty(selectedColumn);
            var reviewerComment = columnValue == null ? "" : columnValue.GetValue(sharepointData, null);


            var Reviewer = new ReviewerInfo
            {
                TaskXrefId = subTask.TaskXrefid,
                IsAssigned = true,
                AssignedTo = user.FirstName + " " + user.LastName,
                DateAssigned = subTask.ModifiedOn,
                DateCompleted = subTask.CompletedOn == null ? subTask.ModifiedOn : subTask.CompletedOn,
                StatusId = subTask.Status,
                Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(subTask.Status.ToString())),
                Reviewertype = (subTask.ViewId == (int)ProductionView.ContractUW || subTask.ViewId == (int)ProductionView.ContractCloser) == true ? "Contractor" : EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())),
                ReviewerTypeId = subTask.ViewId,
                Comments = sharepointData == null ? "" : reviewerComment == null ? "" : reviewerComment.ToString().Replace(">", Environment.NewLine),
                ContractorRole = (subTask.ViewId == (int)ProductionView.ContractUW || subTask.ViewId == (int)ProductionView.ContractCloser) == true ? EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())) : null,

            };
            if (reviewertype == (int)ProductionView.Attorney)
            {
                var stateCodes = accountManager.GetAllStates().Where(m => m.Value != "FK").ToList();

                stateCodes.Insert(0, new KeyValuePair<string, string>("00", "PLEASE SELECT STATE CODE"));
                var address = sharepointData == null ? new AddressModel() : sharepointData.OgcAddressId != 0 ? sharepointScreenManager.GetAddressById(sharepointData.OgcAddressId) : new AddressModel();
                var selectedStateCode = address == null ? "PLEASE SELECT STATE CODE" : address.StateCode;
                ViewBag.StateCode = new SelectList(stateCodes, "key", "Value", selectedStateCode);
                Reviewer.Address = address;
                return PartialView("~/Views/Production/ProductionQueue/_OGCInfo.cshtml", Reviewer);

            }
            return PartialView("~/Views/Production/ProductionQueue/_ReviewersInfo.cshtml", Reviewer);

        }
        public ActionResult GetBackupOGCInfo(Guid taskInstanceId, int reviewertype)
        {
            var subTask = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId).Where(m => m.ViewId == reviewertype).FirstOrDefault();
            var productionUser = (from prodUsers in productionQueue.GetProductionUsers()
                                  select new
                                  {
                                      prodUsers.UserID,
                                      Name = String.Format("{0} {1}", prodUsers.FirstName, prodUsers.LastName)

                                  });

            var stateCodes = accountManager.GetAllStates().Where(m => m.Value != "FK").ToList();

            stateCodes.Insert(0, new KeyValuePair<string, string>("00", "PLEASE SELECT STATE CODE"));

            ViewBag.AssignedToUserId = new SelectList(productionUser, "UserID", "Name");
            var sharepointData = sharepointScreenManager.GetSharepointDataById(taskInstanceId);
            var address = sharepointData == null ? new AddressModel() : sharepointData.BackupOgcAddressId != 0 ? sharepointScreenManager.GetAddressById(sharepointData.BackupOgcAddressId) : new AddressModel();
            var selectedStateCode = address == null ? "PLEASE SELECT STATE CODE" : address.StateCode;
            ViewBag.StateCode = new SelectList(stateCodes, "key", "Value", selectedStateCode);
            ViewBag.taskInstanceId = taskInstanceId;
            if (subTask != null)
            {
                var user = productionQueue.GetUserById(subTask.AssignedTo);
                var backupOgc = new ReviewerInfo
                {
                    TaskXrefId = subTask.TaskXrefid,
                    IsAssigned = true,
                    AssignedTo = user.FirstName + " " + user.LastName,
                    DateAssigned = subTask.ModifiedOn,
                    DateCompleted = subTask.CompletedOn == null ? subTask.ModifiedOn : subTask.CompletedOn,
                    StatusId = subTask.Status,
                    Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(subTask.Status.ToString())),
                    Reviewertype = (subTask.ViewId == (int)ProductionView.ContractUW || subTask.ViewId == (int)ProductionView.ContractCloser) == true ? "Contractor" : EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())),
                    ReviewerTypeId = subTask.ViewId,
                    Address = address

                };
                return PartialView("~/Views/Production/ProductionQueue/_BackupOGCInfo.cshtml", backupOgc);

            }



            return PartialView("~/Views/Production/ProductionQueue/_BackupOGCInfo.cshtml", new ReviewerInfo());

        }
        public void UpdateComment(Guid taskInstacneId, int reviwerType, string comment)
        {
            var fhaNumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstacneId).FHANumber;
            sharepointScreenManager.AddOrUpdateComments(taskInstacneId, fhaNumber, comment, reviwerType);
        }
        public JsonResult GetContractors(Guid taskInstanceId)
        {
            var subTasks = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId);
            if (subTasks.Any())
            {
                var jsonData = (from contractor in subTasks.Where(m => m.ViewId == (int)ProductionView.ContractUW || m.ViewId == (int)ProductionView.ContractCloser)
                                select new
                                {
                                    Id = contractor.TaskXrefid,
                                    contractorRole = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(contractor.ViewId.ToString()))

                                });
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGeneralInfo(Guid taskInstanceId)
        {
            var task = taskManager.GetTasksByTaskInstanceId(taskInstanceId).FirstOrDefault();
            if (task != null)
            {
                if (task.PageTypeId == (int)PageType.ClosingAllExceptConstruction || task.PageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion || task.PageTypeId == (int)PageType.ClosingConstructionInsuredAdvances)
                    return PartialView("~/Views/Production/ProductionQueue/_GeneralInformation.cshtml", new GeneralInformationViewModel() { TaskInstanceId = taskInstanceId });
            }

            var generalInfoDetails = appProcessManager.GetGeneralInfoDetailsForSharepoint(taskInstanceId);
            return PartialView("~/Views/Production/ProductionQueue/_GeneralInformation.cshtml", generalInfoDetails);
        }

        public ActionResult GetMiscellaneousInfo(Guid taskInstanceId)
        {
            var task = taskManager.GetTasksByTaskInstanceId(taskInstanceId).FirstOrDefault();
            if (task != null)
            {
                if (task.PageTypeId == (int)PageType.ClosingAllExceptConstruction || task.PageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion || task.PageTypeId == (int)PageType.ClosingConstructionInsuredAdvances)
                    return PartialView("~/Views/Production/ProductionQueue/_MiscellaneousInformation.cshtml", new MiscellaneousInformationViewModel() { TaskInstanceId = taskInstanceId });
            }
            var miscInfoDetails = appProcessManager.GetContractUWDetails(taskInstanceId);
            miscInfoDetails.FHANumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId).FHANumber;
            sharepointScreenManager.GetAllSharePointAEs()
                .ToList()
                .ForEach(
                    p =>
                        miscInfoDetails.AEList.Add(new SelectListItem()
                        {
                            Text = p.SharePointAEName,
                            Value = p.SharePointAEId.ToString()

                        }));
            miscInfoDetails.TaskInstanceId = taskInstanceId;
            miscInfoDetails = sharepointScreenManager.GetSharePointDetailsForMiscellaneousInfo(miscInfoDetails);
            if (miscInfoDetails.SelectedAeId > 0)
            {
                foreach (var sharepointAEId in miscInfoDetails.AEList)
                {
                    if (int.Parse(sharepointAEId.Value) == miscInfoDetails.SelectedAeId)
                    {
                        sharepointAEId.Selected = true;
                    }
                }
            }
            return PartialView("~/Views/Production/ProductionQueue/_MiscellaneousInformation.cshtml", miscInfoDetails);
        }

        [HttpPost]
        public string SaveApplicationDetails(string fhaNumber, string loanAmount)
        {
            var isLoanAmountUpdated = fhaNumberRequestManager.UpdateLoanAmount(fhaNumber, decimal.Parse(loanAmount));
            return isLoanAmountUpdated ? "Success" : "Failure";
        }

        [HttpPost]
        public string SaveGeneralInformationDetails(GeneralInformationViewModel model)
        {
            var isInfoUpdated = sharepointScreenManager.SaveOrUpdateGeneralInformationDetailsForSharepoint(model);
            return isInfoUpdated ? "Success" : "Failure";
        }

        public ActionResult GetContractorInfo(Guid taskInstanceId, int reviewertype)
        {
            var subTask = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId).Where(m => m.ViewId == reviewertype).FirstOrDefault();
            ViewBag.taskInstanceId = taskInstanceId;
            if (subTask == null)
                return PartialView("~/Views/Production/ProductionQueue/_ContractorsInfo.cshtml", new ContractorInfo());
            var user = productionQueue.GetUserById(subTask.AssignedTo);
            var sharepointData = sharepointScreenManager.GetSharepointComments(taskInstanceId);
            var Reviewer = new ContractorInfo
            {
                TaskXrefId = subTask.TaskXrefid,
                IsAssigned = true,
                AssignedTo = user.FirstName + " " + user.LastName,
                DateAssigned = subTask.ModifiedOn.Date,
                DateCompleted = subTask.CompletedOn == null ? subTask.ModifiedOn : subTask.CompletedOn,
                StatusId = subTask.Status,
                Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(subTask.Status.ToString())),
                Reviewertype = (subTask.ViewId == (int)ProductionView.ContractUW || subTask.ViewId == (int)ProductionView.ContractCloser) == true ? "Contractor" : EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())),
                ReviewerTypeId = subTask.ViewId,
                Comments = sharepointData == null ? "" : sharepointData.ContractorComment == null ? "" : sharepointData.ContractorComment.Replace(">", Environment.NewLine),
                ContractorRole = (subTask.ViewId == (int)ProductionView.ContractUW || subTask.ViewId == (int)ProductionView.ContractCloser) == true ? EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())) : null,
                ContractPrice = sharepointData == null ? 0 : sharepointData.ContractorContractPrice,
                SecondaryAmountPaid = sharepointData == null ? 0 : sharepointData.ContractorSecondaryAmtPaid,
                AmountPaid = sharepointData == null ? 0 : sharepointData.ContractorAmountPaid
            };

            return PartialView("~/Views/Production/ProductionQueue/_ContractorsInfo.cshtml", Reviewer);

        }

        public ActionResult GetCloserInfo(Guid taskInstanceId, int reviewertype)
        {
            var allSubTasks = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId);
            Prod_TaskXrefModel subTask = null;
            ViewBag.taskInstanceId = taskInstanceId;

            var productionTask = taskManager.GetTasksByTaskInstanceId(taskInstanceId).FirstOrDefault();
            if (productionTask != null)
            {
                if (productionTask.PageTypeId == (int)ApplicationAndClosingType.closingInital || productionTask.PageTypeId == (int)ApplicationAndClosingType.closingFinal
                                                 || productionTask.PageTypeId == (int)ApplicationAndClosingType.Draftclosing || productionTask.PageTypeId == (int)ApplicationAndClosingType.closingTwoStageFinal)
                {
                    subTask = allSubTasks.Where(m => m.ViewId == reviewertype).FirstOrDefault();
                }
            }
            if (subTask == null) return PartialView("~/Views/Production/ProductionQueue/_CloserInfo.cshtml", new ClosingInfo() { TaskInstanceId = taskInstanceId });

            var user = productionQueue.GetUserById(subTask.AssignedTo);


            var contractCloser = allSubTasks.Where(m => m.ViewId == (int)ProductionView.ContractCloser).FirstOrDefault();
            UserViewModel ContractorUserInfo = null;
            if (contractCloser != null)
            {
                ContractorUserInfo = productionQueue.GetUserById(contractCloser.AssignedTo);
            }
            var allSharepointAEs = sharepointScreenManager.GetAllSharePointAEs();
            allSharepointAEs.Insert(0, new Prod_SharePointAccountExecutivesViewModel() { SharePointAEId = 0, SharePointAEName = "---PLEASE SELECT USER---" });
            var sharepointData = sharepointScreenManager.GetSharePointClosingInfo(taskInstanceId);
            var selectedProgramSpecialistId = sharepointData == null ? 0 : sharepointData.ClosingProgramSpecialistId == 0 ? 0 : sharepointData.ClosingProgramSpecialistId;
            var selectedAEId = sharepointData == null ? 0 : sharepointData.ClosingAEId == 0 ? 0 : sharepointData.ClosingAEId;
            var selectedCloserId = sharepointData == null ? 0 : sharepointData.ClosingCloserId == 0 ? 0 : sharepointData.ClosingCloserId;

            ViewBag.ClosingProgramSpecialistId = new SelectList(allSharepointAEs, "SharePointAEId", "SharePointAEName", selectedProgramSpecialistId);
            ViewBag.ClosingAEId = new SelectList(allSharepointAEs, "SharePointAEId", "SharePointAEName", selectedAEId);
            ViewBag.ClosingCloserId = new SelectList(allSharepointAEs, "SharePointAEId", "SharePointAEName", selectedCloserId);


            var closerInfo = new ClosingInfo()
            {
                TaskXrefId = subTask.TaskXrefid,
                TaskInstanceId = taskInstanceId,
                IsAssigned = true,
                AssignedTo = user.FirstName + " " + user.LastName,
                DateAssigned = subTask.ModifiedOn.Date,
                DateCompleted = subTask.CompletedOn == null ? subTask.ModifiedOn.Date : subTask.CompletedOn,
                StatusId = subTask.Status,
                Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(subTask.Status.ToString())),
                Reviewertype = (subTask.ViewId == (int)ProductionView.UnderWriter) == true ? "Closer" : EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())),
                ReviewerTypeId = subTask.ViewId,
                //IsCloserContractor = sharepointData== null ? true:sharepointData.IsCloserContractor,
                IsCloserContractor = ContractorUserInfo == null ? false : true,
                //ContractorContactName = sharepointData == null?"":sharepointData.ContractorContactName,
                ContractorContactName = ContractorUserInfo == null ? "" : String.Format("{0} {1} ({2})", ContractorUserInfo.FirstName, ContractorUserInfo.LastName, ContractorUserInfo.UserName),
                ClosingProgramSpecialistId = sharepointData == null ? 0 : sharepointData.ClosingProgramSpecialistId,
                IsCostCertRecieved = sharepointData == null ? false : sharepointData.IsCostCertRecieved,
                CostCertRecievedDate = sharepointData == null ? null : sharepointData.CostCertRecievedDate,
                IsCostCertCompletedAndIssued = sharepointData == null ? false : sharepointData.IsCostCertCompletedAndIssued,
                CostCertIssueDate = sharepointData == null ? null : sharepointData.CostCertIssueDate,
                Is290Completed = sharepointData == null ? false : sharepointData.Is290Completed,
                Two90CompletedDate = sharepointData == null ? null : sharepointData.Two90CompletedDate,
                IsActiveNCRE = sharepointData == null ? false : sharepointData.IsActiveNCRE,
                NCREDueDate = sharepointData == null ? null : sharepointData.NCREDueDate,
                NCREInitalBalance = sharepointData == null ? 0 : sharepointData.NCREInitalBalance,
                Comments = sharepointData == null ? "" : sharepointData.ClosingComment == null ? "" : sharepointData.ClosingComment,
                ClosingAEId = sharepointData == null ? 0 : sharepointData.ClosingAEId,
                ClosingCloserId = sharepointData == null ? 0 : sharepointData.ClosingCloserId,
                InitialClosingDate = sharepointData == null ? null : sharepointData.InitialClosingDate,
                ClosingPackageRecievedDate = sharepointData == null ? null : sharepointData.ClosingPackageRecievedDate,
                ClosingDateCreatedOn = sharepointData == null ? null : sharepointData.ClosingDateCreatedOn,
                ClosingDateModifiedOn = sharepointData == null ? null : sharepointData.ClosingDateModifiedOn

            };
            return PartialView("~/Views/Production/ProductionQueue/_CloserInfo.cshtml", closerInfo);
        }
        public void SaveOrUpdateContractorsPayment(Guid taskInstanceId, decimal contractorPrice, decimal secondaryAmntPaid, decimal amountPaid)
        {
            var fhaNumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId).FHANumber;
            sharepointScreenManager.SaveOrUpdateContractorsPayment(taskInstanceId, fhaNumber, contractorPrice, secondaryAmntPaid, amountPaid);
        }
        public void SaveOrUpdateClosingInfo(ClosingInfo closingInfoModel)
        {
            var task = taskManager.GetLatestTaskByTaskInstanceId(closingInfoModel.TaskInstanceId);
            var fhaNumber = task.FHANumber;
            //taskManager.GetLatestTaskByTaskInstanceId(closingInfoModel.TaskInstanceId).FHANumber;
            bool isFirstTimeClosingDate = sharepointScreenManager.SaveOrUpdateClosingInfo(closingInfoModel, fhaNumber);
            if (isFirstTimeClosingDate)
            {
                CreateForm290(task);
            }


        }
        private void CreateForm290(TaskModel taskModel)
        {

            if (taskModel != null)
            {
                var form290Model = new Prod_Form290TaskModel()
                {
                    TaskInstanceID = Guid.NewGuid(),
                    ClosingTaskInstanceID = taskModel.TaskInstanceId,
                    CreatedDate = DateTime.Now,
                    CreatedByUserName = UserPrincipal.Current.UserName,
                    Status = 1
                };

                var result = productionQueue.CreateForm290Task(form290Model);
                if (result != null)
                {
                    var form290Task = new TaskModel()
                    {
                        TaskInstanceId = result,
                        SequenceId = 0,
                        AssignedBy = "Unassigned",
                        AssignedTo = "Queue",
                        StartTime = DateTime.Now,
                        TaskStepId = (int)ProductionAppProcessStatus.InQueue,
                        PageTypeId = (int)PageType.Form290,
                        FHANumber = taskModel.FHANumber
                    };

                    productionQueue.AddTask(form290Task);
                }

            }

        }
        private string getColumnProperty(int reviewertype)
        {
            switch (reviewertype)
            {
                case (int)ProductionView.ContractUW: return "ContractorComment";
                case (int)ProductionView.Environmentalist: return "EnvironmentalComment";
                case (int)ProductionView.Attorney: return "OGCComment";
                case (int)ProductionView.Survey: return "SurveyComment";
                case (int)ProductionView.ContractCloser: return "ClosingComment"; break;
                case (int)ProductionView.Appraiser: return "AppraisalComment";
                default: return ""; break;
            }
        }
        [HttpPost]
        public string SaveMiscellaneousInformationDetails(MiscellaneousInformationViewModel model)
        {
            var isInfoUpdated = sharepointScreenManager.SaveOrUpdateMiscellaneousInfoForSharepoint(model);
            return isInfoUpdated ? "Success" : "Failure";
        }

        public ActionResult GetApplicationDetails(Guid taskInstanceId)
        {
            var applicationDetails = fhaNumberRequestManager.GetApplicationDetailsForSharePointScreen(taskInstanceId);
            return PartialView("~/Views/Production/ProductionQueue/_ApplicationDetails.cshtml", applicationDetails);
        }
        private List<ContractorInfo> GetAllReviewersInfo(Guid taskInstanceId)
        {


            var subTasks = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId).Where(m => m.ViewId == (int)ProductionView.Survey || m.ViewId == (int)ProductionView.Environmentalist
                                                                                            || m.ViewId == (int)ProductionView.Attorney || m.ViewId == (int)ProductionView.BackupOGC || m.ViewId == (int)ProductionView.Appraiser
                                                                                             || m.ViewId == (int)ProductionView.ContractUW);
            List<ContractorInfo> reviewersList = new List<ContractorInfo>();


            var sharepointData = sharepointScreenManager.GetSharepointDataById(taskInstanceId);
            AddressModel address = null;

            foreach (var subTask in subTasks)
            {
                //var sharepointData = sharepointScreenManager.GetSharepointComments(taskInstanceId);
                if (subTask.ViewId == (int)ProductionView.BackupOGC)
                    address = sharepointData == null ? new AddressModel() : sharepointData.BackupOgcAddressId != 0 ? sharepointScreenManager.GetAddressById(sharepointData.BackupOgcAddressId) : new AddressModel();

                else if (subTask.ViewId == (int)ProductionView.Attorney)
                    address = sharepointData == null ? new AddressModel() : sharepointData.OgcAddressId != 0 ? sharepointScreenManager.GetAddressById(sharepointData.OgcAddressId) : new AddressModel();


                var selectedColumn = getColumnProperty(subTask.ViewId);
                /** Reflection to get column name and value 
                 *  to update reviewer comment column
                 **/
                var columnValue = sharepointData == null ? null : sharepointData.GetType().GetProperty(selectedColumn);
                var reviewerComment = columnValue == null ? "" : columnValue.GetValue(sharepointData, null);
                var user = productionQueue.GetUserById(subTask.AssignedTo);
                var Reviewer = new ContractorInfo
                {
                    TaskXrefId = subTask.TaskXrefid,
                    IsAssigned = true,
                    AssignedTo = user.FirstName + " " + user.LastName,
                    DateAssigned = subTask.ModifiedOn,
                    DateCompleted = subTask.CompletedOn == null ? subTask.ModifiedOn : subTask.CompletedOn,
                    StatusId = subTask.Status,
                    Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(subTask.Status.ToString())),
                    Reviewertype = (subTask.ViewId == (int)ProductionView.ContractUW) == true ? "Contractor" : EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())),
                    ReviewerTypeId = subTask.ViewId,
                    Comments = sharepointData == null ? "" : reviewerComment == null ? "" : reviewerComment.ToString().Replace(">", Environment.NewLine),
                    ContractorRole = (subTask.ViewId == (int)ProductionView.ContractUW || subTask.ViewId == (int)ProductionView.ContractCloser) == true ? EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())) : null,
                    ContractPrice = sharepointData == null ? 0 : sharepointData.ContractorContractPrice,
                    SecondaryAmountPaid = sharepointData == null ? 0 : sharepointData.ContractorSecondaryAmtPaid,
                    AmountPaid = sharepointData == null ? 0 : sharepointData.ContractorAmountPaid,
                    Address = address
                };
                reviewersList.Add(Reviewer);
            }

            return reviewersList;
        }
        private MiscellaneousInformationViewModel GetMisellaneousInfo(Guid taskInstanceId)
        {
            var miscInfoDetails = appProcessManager.GetContractUWDetails(taskInstanceId);
            miscInfoDetails.FHANumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId).FHANumber;
            sharepointScreenManager.GetAllSharePointAEs()
                .ToList()
                .ForEach(
                    p =>
                        miscInfoDetails.AEList.Add(new SelectListItem()
                        {
                            Text = p.SharePointAEName,
                            Value = p.SharePointAEId.ToString()

                        }));
            miscInfoDetails.TaskInstanceId = taskInstanceId;
            miscInfoDetails = sharepointScreenManager.GetSharePointDetailsForMiscellaneousInfo(miscInfoDetails);
            var allAEs = miscInfoDetails.AEList.ToList();
            miscInfoDetails.SelectedAeName = miscInfoDetails.SelectedAeId == 0 ? "Not Assigned" : allAEs.Where(m => m.Value == miscInfoDetails.SelectedAeId.ToString()).FirstOrDefault().Text;
            return miscInfoDetails;
        }
        public ActionResult SharepointForPdf(Guid taskInstanceId)
        {
            var sharePointData = GetSharepointDataForPdf(taskInstanceId);

            return View("~/Views/Production/ProductionQueue/SharepointSinglePage/SharepointForPdf.cshtml", sharePointData);
        }
        public ActionResult GeneratePDF(Guid taskInstanceId)
        {
            var sharePointData = GetSharepointDataForPdf(taskInstanceId);
            var sharepointPdf = new ViewAsPdf("~/Views/Production/ProductionQueue/SharepointSinglePage/SharepointForPdf.cshtml", sharePointData);
            //var sharepointPdf = new ActionAsPdf("SharepointForPdf", new { taskInstanceId = taskInstanceId });
            //sharepointPdf.FormsAuthenticationCookieName = System.Web.Security.FormsAuthentication.FormsCookieName;          


            return sharepointPdf;

        }
        public ActionResult ReGeneratePDF(Guid taskInstanceId, string fhaNumber, int propertyId)
        {
            var appControl = new ProductionApplicationController();
            var result = appControl.GenerateSharepointPdf(taskInstanceId, fhaNumber, propertyId);

            return RedirectToAction("GetSharepointForPdf", new { taskInstanceId = taskInstanceId });
        }
        private bool IsClosingCompleted(Guid taskInstanceId)
        {
            var prodTypes = productionQueue.GetApplicationAndClosingType(taskInstanceId);
            foreach (var prodType in prodTypes)
            {
                if ((prodType.PageTypeId == (int)ApplicationAndClosingType.closingFinal || prodType.PageTypeId == (int)ApplicationAndClosingType.closingTwoStageFinal || prodType.PageTypeId == (int)ApplicationAndClosingType.Executedclosing) && prodType.TaskStepId == (int)ProductionAppProcessStatus.ProjectActionRequestComplete)
                    return true;
            }
            return false;
        }
        private ClosingInfo GetClosingInfo(Guid taskInstanceId)
        {

            var allSubTasks = prod_TaskXrefManager.GetProductionSubTasks(taskInstanceId);
            Prod_TaskXrefModel subTask = null;
            ViewBag.taskInstanceId = taskInstanceId;

            var productionTask = taskManager.GetTasksByTaskInstanceId(taskInstanceId).FirstOrDefault();
            if (productionTask != null)
            {
                if (productionTask.PageTypeId == (int)ApplicationAndClosingType.closingInital || productionTask.PageTypeId == (int)ApplicationAndClosingType.closingFinal)
                {
                    subTask = allSubTasks.Where(m => m.ViewId == (int)ProductionView.UnderWriter).FirstOrDefault();
                }
            }
            if (subTask == null) return (new ClosingInfo() { TaskInstanceId = taskInstanceId });

            var user = productionQueue.GetUserById(subTask.AssignedTo);


            var contractCloser = allSubTasks.Where(m => m.ViewId == (int)ProductionView.ContractCloser).FirstOrDefault();
            UserViewModel ContractorUserInfo = null;
            if (contractCloser != null)
            {
                ContractorUserInfo = productionQueue.GetUserById(contractCloser.AssignedTo);
            }
            var allSharepointAEs = sharepointScreenManager.GetAllSharePointAEs();
            var sharepointData = sharepointScreenManager.GetSharePointClosingInfo(taskInstanceId);
            var selectedAEId = sharepointData == null ? 1 : sharepointData.ClosingProgramSpecialistId == 0 ? 1 : sharepointData.ClosingProgramSpecialistId;
            ViewBag.ClosingProgramSpecialistId = new SelectList(allSharepointAEs, "SharePointAEId", "SharePointAEName", selectedAEId);

            var closerInfo = new ClosingInfo()
            {
                TaskXrefId = subTask.TaskXrefid,
                TaskInstanceId = taskInstanceId,
                IsAssigned = true,
                AssignedTo = user.FirstName + " " + user.LastName,
                DateAssigned = subTask.ModifiedOn.Date,
                DateCompleted = subTask.CompletedOn == null ? subTask.ModifiedOn.Date : subTask.CompletedOn,
                StatusId = subTask.Status,
                Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(subTask.Status.ToString())),
                Reviewertype = (subTask.ViewId == (int)ProductionView.UnderWriter) == true ? "Closer" : EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(subTask.ViewId.ToString())),
                ReviewerTypeId = subTask.ViewId,
                //IsCloserContractor = sharepointData== null ? true:sharepointData.IsCloserContractor,
                IsCloserContractor = ContractorUserInfo == null ? false : true,
                //ContractorContactName = sharepointData == null?"":sharepointData.ContractorContactName,
                ContractorContactName = ContractorUserInfo == null ? "" : String.Format("{0} {1} ({2})", ContractorUserInfo.FirstName, ContractorUserInfo.LastName, ContractorUserInfo.UserName),
                ClosingProgramSpecialistId = sharepointData == null ? 0 : sharepointData.ClosingProgramSpecialistId,
                ClosingProgramSpecialistName = sharepointData == null ? "Not Assigned" : sharepointData.ClosingProgramSpecialistId == 0 ? "Not Assigned" :
                                               allSharepointAEs.Where(m => m.SharePointAEId == sharepointData.ClosingProgramSpecialistId).FirstOrDefault().SharePointAEName,
                IsCostCertRecieved = sharepointData == null ? false : sharepointData.IsCostCertRecieved,
                CostCertRecievedDate = sharepointData == null ? null : sharepointData.CostCertRecievedDate,
                IsCostCertCompletedAndIssued = sharepointData == null ? false : sharepointData.IsCostCertCompletedAndIssued,
                CostCertIssueDate = sharepointData == null ? null : sharepointData.CostCertIssueDate,
                Is290Completed = sharepointData == null ? false : sharepointData.Is290Completed,
                Two90CompletedDate = sharepointData == null ? null : sharepointData.Two90CompletedDate,
                IsActiveNCRE = sharepointData == null ? false : sharepointData.IsActiveNCRE,
                NCREDueDate = sharepointData == null ? null : sharepointData.NCREDueDate,
                NCREInitalBalance = sharepointData == null ? 0 : sharepointData.NCREInitalBalance,
                Comments = sharepointData == null ? "" : sharepointData.ClosingComment == null ? "" : sharepointData.ClosingComment,
                ClosingAEId = sharepointData == null ? 0 : sharepointData.ClosingAEId,
                ClosingCloserId = sharepointData == null ? 0 : sharepointData.ClosingCloserId,
                InitialClosingDate = sharepointData == null ? null : sharepointData.InitialClosingDate,
                ClosingPackageRecievedDate = sharepointData == null ? null : sharepointData.ClosingPackageRecievedDate,
                ClosingAEName = sharepointData == null ? "Not Assigned" : sharepointData.ClosingAEId == 0 ? "Not Assigned" :
                                               allSharepointAEs.Where(m => m.SharePointAEId == sharepointData.ClosingAEId).FirstOrDefault().SharePointAEName,
                ClosingCloserName = sharepointData == null ? "Not Assigned" : sharepointData.ClosingCloserId == 0 ? "Not Assigned" :
                                             allSharepointAEs.Where(m => m.SharePointAEId == sharepointData.ClosingCloserId).FirstOrDefault().SharePointAEName,


            };
            return closerInfo;


        }
        public ActionResult GetAttachFileInfo(Guid taskInstanceId, string fileType)
        {
            var listOfSections = Enum.GetValues(typeof(SharepointSections)).Cast<SharepointSections>().Where(e => (e != SharepointSections.all)).Select(e => new SelectListItem() { Text = EnumType.GetEnumDescription(e), Value = e.ToString() }).ToList();
            foreach (var item in listOfSections)
            {
                if (item.Value == fileType)
                {
                    item.Selected = true;
                }
            }
            ViewBag.AttachSectionNames = listOfSections;
            ViewBag.SearchSectionNames = Enum.GetValues(typeof(SharepointSections)).Cast<SharepointSections>().Select(e => new SelectListItem() { Text = EnumType.GetEnumDescription(e), Value = e.ToString() });
            var attachFilesViewModel = new AttachFilesViewModel();
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            attachFilesViewModel.TransAccessStatus = checkResult.status;
            attachFilesViewModel.FHANumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId).FHANumber;
            attachFilesViewModel.TaskInstanceId = taskInstanceId;
            return PartialView("~/Views/Production/ProductionQueue/_AttachSharepointFiles.cshtml", attachFilesViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskInstanceId"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public ActionResult GetAmendmentInfo(Guid taskInstanceId, string fileType)
        {
            var objAmendmentSPViewModel = new AmendmentSPViewModel();
            objAmendmentSPViewModel.AmendmentSubmitted = new List<string>();
            objAmendmentSPViewModel.FHANumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId).FHANumber;
            objAmendmentSPViewModel.TaskInstanceId = taskInstanceId;
            objAmendmentSPViewModel.AmendmentSubmitted.Add("1");
            objAmendmentSPViewModel.AmendmentSubmitted.Select(m => new SelectListItem { Text = m, Value = m });
            ViewBag.AmendmentSubmitted = objAmendmentSPViewModel.AmendmentSubmitted;
            return PartialView("~/Views/Production/ProductionQueue/_AmendmentSP.cshtml", objAmendmentSPViewModel);
        }

        public JsonResult GetUploadedFilesForSharepoint(string sidx, string sord, int page, int rows,
            Guid taskInstanceId, string fileType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;


            //var sharepointFileType = fileType != null ? (FileType) Enum.Parse(typeof (FileType), fileType): FileType.NotSpecified;
            //var uploadedFiles = taskManager.GetFilesByTaskInstanceIdAndFileType(taskInstanceId, sharepointFileType);
            var uploadedFiles = taskManager.GetTaskFileForSharepoint(taskInstanceId, fileType);

            //Setting the date based on the Time Zone
            if (uploadedFiles != null)
            {
                string tempStr;
                foreach (var item in uploadedFiles)
                {
                    item.UploadedOn = item.UploadedOn.ToMyTimeFromUtc();
                    item.SectionName = EnumType.GetEnumDescription((FileType)Enum.Parse(typeof(FileType), item.SectionName));
                    tempStr = sharepointScreenManager.GetNameAndRoleForUser(int.Parse(item.UploadedBy));
                    item.RoleName = EnumType.GetEnumDescription((HUDRole)Enum.Parse(typeof(HUDRole), tempStr.Split(',')[2]));
                    item.UploadedBy = tempStr.Split(',')[0] + " " + tempStr.Split(',')[1];
                }
            }

            if (uploadedFiles != null)
            {
                int totalrecods = uploadedFiles.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);


                var result = uploadedFiles.ToList().OrderBy(s => s.FileId);
                var results = result.Skip(pageIndex * pageSize).Take(pageSize);

                var jsonData = new
                {
                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = results,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = uploadedFiles,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }

        public List<ProdApplicationSharepointData> GetSharepointDataForPdf(Guid taskInstanceId)
        {
            var types = productionQueue.GetApplicationAndClosingType(taskInstanceId);
            List<ProdApplicationSharepointData> singleSharepointDataList = new List<ProdApplicationSharepointData>();
            foreach (var appType in types)
            {

                var singleSharepointData = new ProdApplicationSharepointData();
                var sharepointData = sharepointScreenManager.GetSharepointDataById(appType.TaskInstanceId);
                //var fhaRequest = fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(sharepointData.TaskinstanceId);
                singleSharepointData.ProdAppName = EnumType.GetEnumDescription(EnumType.Parse<ApplicationAndClosingType>(appType.PageTypeId.ToString()));
                singleSharepointData.ApplicationDetails = fhaNumberRequestManager.GetApplicationDetailsForSharePointScreen(appType.TaskInstanceId);
                singleSharepointData.GeneralInformation = (appType.PageTypeId == (int)PageType.ClosingAllExceptConstruction ||
                                                          appType.PageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion ||
                                                          appType.PageTypeId == (int)PageType.ClosingConstructionInsuredAdvances) == true ? new GeneralInformationViewModel() { TaskInstanceId = appType.TaskInstanceId }
                                                          : appProcessManager.GetGeneralInfoDetailsForSharepoint(appType.TaskInstanceId);
                singleSharepointData.MiscellaneousInformation = (appType.PageTypeId == (int)PageType.ClosingAllExceptConstruction ||
                                                          appType.PageTypeId == (int)PageType.ClosingConstructionInsuranceUponCompletion ||
                                                          appType.PageTypeId == (int)PageType.ClosingConstructionInsuredAdvances) == true ? new MiscellaneousInformationViewModel() { TaskInstanceId = appType.TaskInstanceId }
                                                          : GetMisellaneousInfo(taskInstanceId);
                singleSharepointData.ReviewersList = GetAllReviewersInfo(appType.TaskInstanceId);
                singleSharepointData.ClosingInfo = (appType.PageTypeId == (int)ApplicationAndClosingType.closingInital
                                                      || appType.PageTypeId == (int)ApplicationAndClosingType.closingFinal) == true ? GetClosingInfo(appType.TaskInstanceId)
                                                       : new ClosingInfo() { TaskInstanceId = appType.TaskInstanceId };
                singleSharepointData.PageTypeId = appType.PageTypeId;
                singleSharepointDataList.Add(singleSharepointData);
            }
            return singleSharepointDataList;
        }


        [HttpGet]
        public FileResult DownloadSharepointFile(Guid taskInstanceId)
        {


            var allFiles = taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceId);
            if (allFiles.Any())
            {
                var sharepointPdf = allFiles.Where(m => m.FileType == FileType.SharePointPdf).LastOrDefault();
                if (null != sharepointPdf)
                {
                    RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition",
                    String.Format("attachment;filename=\"{0}\"", sharepointPdf.FileName));

                    if (!string.IsNullOrEmpty(sharepointPdf.API_upload_status) && sharepointPdf.API_upload_status.ToLower() == "success")
                    {
                        request = webApiTokenRequest.RequestToken();
                        var jsonData = "{\"docId\":" + sharepointPdf.DocId + ",\"version\":" + sharepointPdf.Version + "}";
                        Stream streamResult = webApiDownload.DownloadDocumentUsingWebApi(jsonData, request.access_token, sharepointPdf.FileName);
                    }
                    else
                    {
                        //GeneratePDF 
                    }
                    Response.End();
                }

            }
            return null;
        }
        [HttpPost]
        public void AssignBackupOGC(Guid taskInstanceId, int userId)
        {
            var model = new ProductionTaskAssignmentModel
            {
                IsReviewer = false,
                AssignedBy = UserPrincipal.Current.UserId,
                Status = (int)ProductionAppProcessStatus.InProcess,
                ViewId = (int)ProductionView.BackupOGC,
                ModifiedOn = DateTime.UtcNow,
                ModifiedBy = UserPrincipal.Current.UserId,
                AssignedTo = userId,
                TaskXrefid = Guid.NewGuid(),
                TaskInstanceId = taskInstanceId
            };
            prod_TaskXrefManager.AddTaskXref(model);
        }
        [HttpPost]
        public void SaveOrUpdateOgcAddress(AddressModel address, Guid taskInstanceId, int reviewertypeId)
        {
            var fhaNumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId).FHANumber;

            sharepointScreenManager.SaveOrUpdateOgcAddress(address, fhaNumber, taskInstanceId, reviewertypeId);

        }

        [HttpPost]
        public string UploadFile(AttachFilesViewModel attachFilesViewModel)
        {
            string isUploaded = "";
            try
            {

                var uploadmodel = new RestfulWebApiUploadModel()
                {
                    propertyID = _projectActionFormManager.GetProdPropertyInfo(attachFilesViewModel.FHANumber).PropertyId.ToString(),
                    indexType = "1",
                    indexValue = attachFilesViewModel.FHANumber,
                    pdfConvertableValue = "false",
                    folderKey = 0,
                    folderNames = "Production/Project Information",
                };
                var WebApiUploadResult = new RestfulWebApiResultModel();
                var request = new RestfulWebApiTokenResultModel();
                foreach (string Filename in Request.Files)
                {
                    HttpPostedFileBase myFile = Request.Files[Filename];
                    if (myFile != null && myFile.ContentLength != 0)
                    {
                        double fileSize = (myFile.ContentLength) / 1024;
                        Random randoms = new Random();
                        var UniqueId = randoms.Next(0, 99999);

                        var systemFileName = attachFilesViewModel.FHANumber + "_" +
                                             attachFilesViewModel.TaskInstanceId + "_" + UniqueId +
                                             Path.GetExtension(myFile.FileName);
                        string pathForSaving = Server.MapPath("~/Uploads");
                        if (this.CreateFolderIfNeeded(pathForSaving))
                        {
                            try
                            {
                                myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        request = webApiTokenRequest.RequestToken();
                        WebApiUploadResult = uploadApiManager.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");


                        if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                        {
                            System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                            var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
                            DateTime.UtcNow.ToString(), systemFileName, attachFilesViewModel.TaskInstanceId);
                            taskFile.GroupFileType =
                                Enum.Parse(typeof(FileType), attachFilesViewModel.SelectedSectionName).ToString();
                            taskFile.DocId = WebApiUploadResult.docId;
                            taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                            taskFile.API_upload_status = WebApiUploadResult.status;
                            taskFile.DocTypeID = ConfigurationManager.AppSettings["SPDocType"].ToString(); ;
                            taskManager.SaveGroupTaskFileModel(taskFile);
                            isUploaded = "Success";

                        }
                        else
                        {
                            Exception ex = new Exception(WebApiUploadResult.message);
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = "failed";
                        }
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return isUploaded;
        }

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return true;
        }

        public ActionResult GetComments(Guid fhaRequestId, int type)
        {
            var fhaNumberRequest = fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(fhaRequestId);

            if (fhaNumberRequest != null)
            {
                var subTasks = prod_TaskXrefManager.GetProductionSubTasks(fhaNumberRequest.TaskinstanceId);
                var productionUserMsg = new ProductionComments();
                LenderComment lenderComment = new LenderComment
                {

                    CommentText = fhaNumberRequest.Comments,
                    LenderName = fhaNumberRequestManager.GetLenderName(fhaNumberRequest.LenderId)
                };

                var fhaInset = taskManager.GetTasksByTaskInstanceId(fhaNumberRequest.TaskinstanceId).LastOrDefault();
                var creditReview = subTasks.FirstOrDefault(m => m.ViewId == (int)ProductionView.CreditReview);
                if (creditReview != null)
                {
                    var prodUserCR = productionQueue.GetUserById(creditReview.AssignedTo);
                    var portfolio = subTasks.FirstOrDefault(m => m.ViewId == (int)ProductionView.Portfolio);
                    if (portfolio != null)
                    {
                        var prodUserPF = productionQueue.GetUserById(portfolio.AssignedTo);
                        var prodUserFHAInsert = new UserInfoModel();
                        if (fhaInset != null)
                        {
                            prodUserFHAInsert = productionQueue.GetUserInfoByUsername(fhaInset.AssignedTo);
                        }

                        List<ProductionUserComments> productionUserComments = new List<ProductionUserComments>
                            {
                                new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.InsertFHAComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.InsertFha.ToString())),
                                    ProductionUserName = prodUserFHAInsert== null?"Not Assigned":string.Format("{0} {1}",prodUserFHAInsert.FirstName,prodUserFHAInsert.LastName)
                                },
                                new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.CreditreviewComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.CreditReview.ToString())),
                                    ProductionUserName = string.Format("{0} {1}",prodUserCR.FirstName,prodUserCR.LastName)
                                },
                                new ProductionUserComments()
                                {
                                    CommentText = fhaNumberRequest.PortfolioComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.Portfolio.ToString())),
                                    ProductionUserName = string.Format("{0} {1}",prodUserPF.FirstName,prodUserPF.LastName)
                                }
                            };

                        productionUserMsg.LenderComment = lenderComment;
                        productionUserMsg.ProductionUserComments = productionUserComments;
                    }
                }
                else
                {
                    var prodUserFHAInsert = new UserInfoModel();
                    if (fhaInset != null)
                    {
                        prodUserFHAInsert = productionQueue.GetUserInfoByUsername(fhaInset.AssignedTo);
                    }
                    productionUserMsg.LenderComment = lenderComment;
                    productionUserMsg.ProductionUserComments = new List<ProductionUserComments> {
                            new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.InsertFHAComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.InsertFha.ToString())),
                                    ProductionUserName = prodUserFHAInsert == null ? "Not Assigned" : string.Format("{0} {1}", prodUserFHAInsert.FirstName, prodUserFHAInsert.LastName)
                                }
                                };

                }

                return View("~/Views/Production/ProductionMyTask/comments.cshtml", productionUserMsg);
            }
            return View("~/Views/Production/ProductionQueue/index.cshtml");
        }

        public ActionResult GetLoanCommitteeInfo(Guid taskInstanceId)
        {
            var GetSubmitedDates = loanCommitteManager.GetSubmitedDates(taskInstanceId);

            //if (GetSubmitedDates.Count > 0)
            //{
            //    Prod_LoanCommitteeViewModel Prod_LoanCommitteeViewModel = new Prod_LoanCommitteeViewModel();
            //    Prod_LoanCommitteeViewModel.FHANumber = GetSubmitedDates[0].FHANumber;
            //    PopulateDropdownForLC(Prod_LoanCommitteeViewModel);
            //    Prod_LoanCommitteeViewModel.A7Presentation = 0;
            //    Prod_LoanCommitteeViewModel.TwoStageSubmittal = 0;
            //    Prod_LoanCommitteeViewModel.ApplicationTaskinstanceId = taskInstanceId;
            //    Prod_LoanCommitteeViewModel.LoanCommitteDateSubmitedList = GetSubmitedDates.Where(y => y.LoanCommitteDate != null).Select(x => new SelectListItem { Text = x.LoanCommitteDate.Value.ToString("MM/dd/yyyy") }).ToList();
            //    Prod_LoanCommitteeViewModel.ChangeToLoanCommitteeDateList = new List<SelectListItem>();
            //    Prod_LoanCommitteeViewModel.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Reschedule Date", Value = "0" });
            //    Prod_LoanCommitteeViewModel.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Extend Date", Value = "1" });
            //    Prod_LoanCommitteeViewModel.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Edit Date", Value = "2" });
            //    ViewData["DatesList"] = loanCommitteManager.GetLoanCommitteeDatesToDisable().ToArray();
            //    return PartialView("~/Views/Production/ProductionQueue/_LoanCommittee.cshtml", Prod_LoanCommitteeViewModel);
            //}
            //else
            //{
            var lcInfoDetails = loanCommitteManager.GetLoanCommitteeDetails(taskInstanceId);

            if (lcInfoDetails == null || GetSubmitedDates.Count > 0)
            {

                var fhaNumber = taskManager.GetFHANumberByTaskInstanceId(taskInstanceId);
                var fhaRequestModel = fhaNumberRequestManager.GetFhaRequestByFhaNumber(fhaNumber);
                lcInfoDetails = new Prod_LoanCommitteeViewModel();
                //
                lcInfoDetails.LoanCommitteDateSubmitedList = GetSubmitedDates.Where(y => y.LoanCommitteDate != null).Select(x => new SelectListItem { Text = x.LoanCommitteDate.Value.ToString("MM/dd/yyyy") }).OrderBy(z => z.Text).ToList();
                lcInfoDetails.ChangeToLoanCommitteeDateList = new List<SelectListItem>();
                lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Reschedule Date", Value = "0" });
                lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Extend Date", Value = "1" });
                lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Edit Date", Value = "2" });
                // 
                lcInfoDetails.FHANumber = fhaRequestModel.FHANumber;
                lcInfoDetails.ApplicationTaskinstanceId = taskInstanceId;
                lcInfoDetails.ProjectName = fhaRequestModel.ProjectName;
                lcInfoDetails.Lender = fhaNumberRequestManager.GetLenderName(fhaRequestModel.LenderId);
                lcInfoDetails.LenderId = fhaRequestModel.LenderId;
                lcInfoDetails.ProjectType = fhaRequestModel.ProjectTypeId;
                lcInfoDetails.ProjectTypeName = fhaNumberRequestManager.GetProjectTypeById(fhaRequestModel.ProjectTypeId);
                lcInfoDetails.NonProfit = fhaNumberRequestManager.GetBorrowerTypeById(fhaRequestModel.BorrowerTypeId);
                lcInfoDetails.LoanAmount = fhaRequestModel.LoanAmount;
                var uwId = prod_TaskXrefManager.GetReviewerUserIdByTaskInstanceId(taskInstanceId, (int)ProductionView.UnderWriter);
                lcInfoDetails.Underwriter = accountManager.GetFullUserNameById(uwId);

                var wlmId = prod_TaskXrefManager.GetWLMUserIdByTaskInstanceId(taskInstanceId);
                if (wlmId == 0)
                {
                    lcInfoDetails.WLM = "";
                }
                else
                {
                    var wlm = sharepointScreenManager.GetNameAndRoleForUser(wlmId);
                    if (wlm != null)
                    {
                        if (loanCommitteManager.IsUserProductionWlm(wlmId))
                        {
                            lcInfoDetails.WLM = wlm.Split(',')[0] + " " + wlm.Split(',')[1];
                        }
                    }
                    lcInfoDetails.WLM = lcInfoDetails.WLM ?? "";

                }

                var ogcId = prod_TaskXrefManager.GetReviewerUserIdByTaskInstanceId(taskInstanceId, (int)ProductionView.Attorney);
                lcInfoDetails.Ogc = accountManager.GetFullUserNameById(ogcId);


                var apprId = prod_TaskXrefManager.GetReviewerUserIdByTaskInstanceId(taskInstanceId, (int)ProductionView.Appraiser);
                lcInfoDetails.Appraiser = accountManager.GetFullUserNameById(apprId);

                PopulateDropdownForLC(lcInfoDetails);
                lcInfoDetails.A7Presentation = 0;
                lcInfoDetails.TwoStageSubmittal = 0;
            }
            else
            {

                lcInfoDetails.ProjectTypeName = fhaNumberRequestManager.GetProjectTypeById((int)lcInfoDetails.ProjectType);
                PopulateDropdownForLC(lcInfoDetails);
                if (lcInfoDetails.ProjectScheduledForLC != null)
                {
                    foreach (var item in lcInfoDetails.ProjectScheduledLC)
                    {
                        if (int.Parse(item.Value) == lcInfoDetails.ProjectScheduledForLC)
                        {
                            item.Selected = true;
                        }
                    }
                }

                if (lcInfoDetails.LCRecommendation != null)
                {
                    foreach (var item in lcInfoDetails.RecommendationLC)
                    {
                        if (int.Parse(item.Value) == lcInfoDetails.LCRecommendation)
                        {
                            item.Selected = true;
                        }
                    }
                }

                if (lcInfoDetails.A7Presentation != null)
                {
                    foreach (var item in lcInfoDetails.A7PresentationLC)
                    {
                        if (int.Parse(item.Value) == lcInfoDetails.A7Presentation)
                        {
                            item.Selected = true;
                        }
                    }
                }
                else
                {
                    lcInfoDetails.A7Presentation = 0;
                }

                if (lcInfoDetails.TwoStageSubmittal != null)
                {
                    foreach (var item in lcInfoDetails.TwoStageSubmittalLC)
                    {
                        if (int.Parse(item.Value) == lcInfoDetails.TwoStageSubmittal)
                        {
                            item.Selected = true;
                        }
                    }
                }
                else
                {
                    lcInfoDetails.TwoStageSubmittal = 0;
                }
                if (!string.IsNullOrEmpty(lcInfoDetails.LCComments))
                    lcInfoDetails.DisplayLCComments = lcInfoDetails.LCComments.Replace(">", Environment.NewLine);
                lcInfoDetails.LCComments = string.Empty;
            }
            ViewData["DatesList"] = loanCommitteManager.GetLoanCommitteeDatesToDisable().ToArray();
            return PartialView("~/Views/Production/ProductionQueue/_LoanCommittee.cshtml", lcInfoDetails);
            // }
        }
        public ActionResult GetLoanCommitteeSelctedDateInfo(Guid taskInstanceId, string SelectedDate)
        {
            var lcInfoDetails = loanCommitteManager.GetLoanCommitteeSelctedDateInfo(taskInstanceId, SelectedDate);

            //if (lcInfoDetails == null)
            //{
            //    //var GetSubmitedDates = loanCommitteManager.GetSubmitedDates(taskInstanceId);
            //    ////lcInfoDetails.LoanCommitteDateSubmitedList = GetSubmitedDates.Where(y => y.LoanCommitteDate.Value == Convert.ToDateTime(SelectedDate)).Select(x => new SelectListItem { Text = x.LoanCommitteDate.Value.ToString("MM/dd/yyyy"), Selected = true }).ToList();
            //    //lcInfoDetails.LoanCommitteDateSubmitedList = GetSubmitedDates.Where(y => y.LoanCommitteDate != null).Select(x => new SelectListItem { Text = x.LoanCommitteDate.Value.ToString("MM/dd/yyyy") }).ToList();
            //    //lcInfoDetails.ChangeToLoanCommitteeDateList = new List<SelectListItem>();
            //    //lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Reschedule Date", Value = "0" });
            //    //lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Extend Date", Value = "1" });
            //    //lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Edit Date", Value = "2" });

            //    var fhaNumber = taskManager.GetFHANumberByTaskInstanceId(taskInstanceId);
            //    var fhaRequestModel = fhaNumberRequestManager.GetFhaRequestByFhaNumber(fhaNumber);
            //    lcInfoDetails = new Prod_LoanCommitteeViewModel();
            //    lcInfoDetails.FHANumber = fhaRequestModel.FHANumber;
            //    lcInfoDetails.ApplicationTaskinstanceId = taskInstanceId;
            //    lcInfoDetails.ProjectName = fhaRequestModel.ProjectName;
            //    lcInfoDetails.Lender = fhaNumberRequestManager.GetLenderName(fhaRequestModel.LenderId);
            //    lcInfoDetails.LenderId = fhaRequestModel.LenderId;
            //    lcInfoDetails.ProjectType = fhaRequestModel.ProjectTypeId;
            //    lcInfoDetails.ProjectTypeName = fhaNumberRequestManager.GetProjectTypeById(fhaRequestModel.ProjectTypeId);
            //    lcInfoDetails.NonProfit = fhaNumberRequestManager.GetBorrowerTypeById(fhaRequestModel.BorrowerTypeId);
            //    lcInfoDetails.LoanAmount = fhaRequestModel.LoanAmount;
            //    var uwId = prod_TaskXrefManager.GetReviewerUserIdByTaskInstanceId(taskInstanceId, (int)ProductionView.UnderWriter);
            //    lcInfoDetails.Underwriter = accountManager.GetFullUserNameById(uwId);

            //    var wlmId = prod_TaskXrefManager.GetWLMUserIdByTaskInstanceId(taskInstanceId);
            //    if (wlmId == 0)
            //    {
            //        lcInfoDetails.WLM = "";
            //    }
            //    else
            //    {
            //        var wlm = sharepointScreenManager.GetNameAndRoleForUser(wlmId);
            //        if (wlm != null)
            //        {
            //            if (loanCommitteManager.IsUserProductionWlm(wlmId))
            //            {
            //                lcInfoDetails.WLM = wlm.Split(',')[0] + " " + wlm.Split(',')[1];
            //            }
            //        }
            //        lcInfoDetails.WLM = lcInfoDetails.WLM ?? "";

            //    }

            //    var ogcId = prod_TaskXrefManager.GetReviewerUserIdByTaskInstanceId(taskInstanceId, (int)ProductionView.Attorney);
            //    lcInfoDetails.Ogc = accountManager.GetFullUserNameById(ogcId);


            //    var apprId = prod_TaskXrefManager.GetReviewerUserIdByTaskInstanceId(taskInstanceId, (int)ProductionView.Appraiser);
            //    lcInfoDetails.Appraiser = accountManager.GetFullUserNameById(apprId);

            //    PopulateDropdownForLC(lcInfoDetails);
            //    lcInfoDetails.A7Presentation = 0;
            //    lcInfoDetails.TwoStageSubmittal = 0;
            //}
            //else
            //{

            lcInfoDetails.ProjectTypeName = fhaNumberRequestManager.GetProjectTypeById((int)lcInfoDetails.ProjectType);
            PopulateDropdownForLC(lcInfoDetails);
            if (lcInfoDetails.ProjectScheduledForLC != null)
            {
                foreach (var item in lcInfoDetails.ProjectScheduledLC)
                {
                    if (int.Parse(item.Value) == lcInfoDetails.ProjectScheduledForLC)
                    {
                        item.Selected = true;
                    }
                }
            }

            if (lcInfoDetails.LCRecommendation != null)
            {
                foreach (var item in lcInfoDetails.RecommendationLC)
                {
                    if (int.Parse(item.Value) == lcInfoDetails.LCRecommendation)
                    {
                        item.Selected = true;
                    }
                }
            }

            if (lcInfoDetails.A7Presentation != null)
            {
                foreach (var item in lcInfoDetails.A7PresentationLC)
                {
                    if (int.Parse(item.Value) == lcInfoDetails.A7Presentation)
                    {
                        item.Selected = true;
                    }
                }
            }
            else
            {
                lcInfoDetails.A7Presentation = 0;
            }

            if (lcInfoDetails.TwoStageSubmittal != null)
            {
                foreach (var item in lcInfoDetails.TwoStageSubmittalLC)
                {
                    if (int.Parse(item.Value) == lcInfoDetails.TwoStageSubmittal)
                    {
                        item.Selected = true;
                    }
                }
            }
            else
            {
                lcInfoDetails.TwoStageSubmittal = 0;
            }
            if (!string.IsNullOrEmpty(lcInfoDetails.LCComments))
                lcInfoDetails.DisplayLCComments = lcInfoDetails.LCComments.Replace(">", Environment.NewLine);
            lcInfoDetails.LCComments = string.Empty;
            var GetSubmitedDates = loanCommitteManager.GetSubmitedDates(taskInstanceId);
            //lcInfoDetails.LoanCommitteDateSubmitedList = GetSubmitedDates.Where(y => y.LoanCommitteDate.Value == Convert.ToDateTime(SelectedDate)).Select(x => new SelectListItem { Text = x.LoanCommitteDate.Value.ToString("MM/dd/yyyy"), Selected = true }).ToList();
            lcInfoDetails.LoanCommitteDateSubmitedList = GetSubmitedDates.Select(x => new SelectListItem { Text = x.LoanCommitteDate.Value.ToString("MM/dd/yyyy") }).ToList();
            if (lcInfoDetails.LoanCommitteDateSubmitedList != null)
            {
                foreach (var item in lcInfoDetails.LoanCommitteDateSubmitedList)
                {
                    if (item.Text == SelectedDate)
                    {
                        item.Selected = true;
                    }
                }
            }
            lcInfoDetails.ChangeToLoanCommitteeDateList = new List<SelectListItem>();
            lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Reschedule Date" });
            lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Extend Date" });
            lcInfoDetails.ChangeToLoanCommitteeDateList.Add(new SelectListItem() { Text = "Edit Date" });
            //}
            ViewData["DatesList"] = loanCommitteManager.GetLoanCommitteeDatesToDisable().ToArray();
            return PartialView("~/Views/Production/ProductionQueue/_LoanCommittee.cshtml", lcInfoDetails);

        }
        public Guid SaveLCInformationDetails(Prod_LoanCommitteeViewModel lcModel)
        {
            if (lcModel != null && lcModel.LCComments != null)
            {
                if (lcModel.LCComments.StartsWith(",")) lcModel.LCComments = lcModel.LCComments.Remove(0, 1);
            }
            var isInfoUpdated = loanCommitteManager.SaveLoanCommitteeDetails(lcModel);
            return isInfoUpdated;
        }

        private Prod_LoanCommitteeViewModel PopulateDropdownForLC(Prod_LoanCommitteeViewModel lcInfoDetails)
        {
            lcInfoDetails.ProjectScheduledLC = new List<SelectListItem>();
            lcInfoDetails.ProjectScheduledLC.Add(new SelectListItem() { Text = "Yes", Value = "1" });
            lcInfoDetails.ProjectScheduledLC.Add(new SelectListItem() { Text = "No", Value = "0", Selected = true });
            lcInfoDetails.RecommendationLC = new List<SelectListItem>();
            lcInfoDetails.RecommendationLC.Add(new SelectListItem() { Text = "Approve", Value = "1" });
            lcInfoDetails.RecommendationLC.Add(new SelectListItem() { Text = "Reject", Value = "0" });
            lcInfoDetails.RecommendationLC.Add(new SelectListItem() { Text = "Withdrawn", Value = "2" });
            lcInfoDetails.RecommendationLC.Add(new SelectListItem() { Text = "Request Additional Information", Value = "3" });
            lcInfoDetails.RecommendationLC.Add(new SelectListItem() { Text = "Long Term Hold", Value = "4" });
            lcInfoDetails.A7PresentationLC = new List<SelectListItem>();
            lcInfoDetails.A7PresentationLC.Add(new SelectListItem() { Text = "Yes", Value = "1" });
            lcInfoDetails.A7PresentationLC.Add(new SelectListItem() { Text = "No", Value = "0", Selected = true });
            lcInfoDetails.TwoStageSubmittalLC = new List<SelectListItem>();
            lcInfoDetails.TwoStageSubmittalLC.Add(new SelectListItem() { Text = "Yes", Value = "1" });
            lcInfoDetails.TwoStageSubmittalLC.Add(new SelectListItem() { Text = "No", Value = "0", Selected = true });
            lcInfoDetails.LCDecisionList = new List<SelectListItem>();
            lcInfoDetails.LCDecisionList.Add(new SelectListItem() { Text = "Approve", Value = "1" });
            lcInfoDetails.LCDecisionList.Add(new SelectListItem() { Text = "Reject", Value = "0" });
            lcInfoDetails.LCDecisionList.Add(new SelectListItem() { Text = "Withdrawn", Value = "2" });
            lcInfoDetails.LCDecisionList.Add(new SelectListItem() { Text = "Request Additional Information", Value = "3" });
            lcInfoDetails.LCDecisionList.Add(new SelectListItem() { Text = "Long Term Hold", Value = "4" });
            return lcInfoDetails;
        }

        public int GetLCDateCount(DateTime lcDate)
        {
            return loanCommitteManager.GetLCDateCount(lcDate);
        }

        public IList<string> GetLoanCommitteeDatesToDisable()
        {
            return loanCommitteManager.GetLoanCommitteeDatesToDisable();
        }

        //Naveen 230 : add below method to display completed tasks
        public JsonResult GetAllCompletedProductionTasks(string sidx, string sord, int page, int rows, int applicationType, int status, string fhaOrProjName = null)
        {
            int statusId = 0;
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            status = 0;
            IEnumerable<TaskModel> productionTasks;
            if (applicationType == (int)PageType.Form290)
            {
                if (status == (int)TaskStep.InProcess) status = (int)TaskStep.Form290Request;
                if (status == (int)TaskStep.Complete) status = (int)TaskStep.Form290Complete;

            }
            if (applicationType == (int)PageType.NotSpecified)
            {
                productionTasks = status == 0 ? productionQueue.GetProductionTasks().ToList() : productionQueue.GetProductionTaskByStatus(status).ToList();
                //var c = productionTasks.Where(x=>x.TaskId == 31222);
                ////karri#434;restricting the FHA Requests only for unassaigned tab
                //productionTasks = (from q in productionTasks
                //                   where q.PageTypeId == 4 && (q.AssignedTo != "Queue" || q.AssignedTo != null)
                //                   select q).ToList();
            }
            else
            {
                if (applicationType != (int)PageType.FhaRequest && status == (int)ProductionAppProcessStatus.Completed)
                    status = (int)ProductionAppProcessStatus.ProjectActionRequestComplete;
                productionTasks = status == 0 ? productionQueue.GetProductionTaskByType(applicationType) : productionQueue.GetProductionTaskByTypeAndStatus(applicationType, status).ToList();
               // var c = productionTasks.Where(x => x.FHANumber == "666-00501");

            }
            var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
            var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

            var applicationRequests = appProcessManager.GetAppRequests().ToList();
            var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
            List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
            if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
            {
                submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
            }


            var fhaRequestTasks = (from task in taskModels.Where(m => m.PageTypeId == (int)PageType.FhaRequest)
                                   join fhaRequest in productionQueue.GetFhaRequests(distnictInstanceId).ToList()
                                   //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
                                   on task.TaskInstanceId equals fhaRequest.TaskinstanceId


                                   select new
                                   {
                                       task.TaskInstanceId,
                                       task.TaskId,
                                       TaskName = fhaRequest.projectName,
                                       PropertyName = fhaRequest.PropertyName,
                                       task.SequenceId,
                                       task.PageTypeId,
                                       task.StartTime,
                                       task.AssignedBy,
                                       task.AssignedTo,
                                       ModofiedOn = fhaRequest.ModifiedOn,
                                       Lender = fhaRequest.LenderName,

                                       //Naveen 238 (31july and 01aug: status as been corrected by writing query to right status)
                                       // Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(task.TaskStepId.ToString())),
                                       Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(((from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                         select new { }).Count() == 0 ? 19 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                              where q.Status == 17 && task.TaskStepId == 17
                                                                                                                                                              select new { }).Count() > 0 ? 18 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                                                                  where q.Status == 19 && task.TaskStepId == 19
                                                                                                                                                                                                  select new { }).Count() > 0 ? 19 : 18).ToString())),
                                       Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                       DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                       LoanType = fhaRequest.LoanType,
                                       TaskStepId = task.TaskStepId,
                                       ShowStatus ="0"
                                   });

            //Naveen Srinivas Hareesh added below foreach
            //fhaRequestTasks = fhaRequestTasks.Where(x => x.TaskName.Contains("666-00501")).ToList();
            List<int> fhasubtasksInProgress = new List<int>();

            foreach (var task in fhaRequestTasks)
            {
                var fhasubtasks = GetSubTasksByTaskInstanceId(task.TaskInstanceId, "FHA", "").ToList();

                var acd = (from q in fhasubtasks
                           where (q.ViewId == 9 && q.Status == "In Queue") || (q.ViewId == 10 && q.Status == "In Queue")
                           select q.TaskId).ToList();

                if (acd.Count() > 0)
                {
                    fhasubtasksInProgress.Add(
                        task.TaskId
                    );


                 }



            }

            fhaRequestTasks = fhaRequestTasks.Where(x => !fhasubtasksInProgress.Contains(x.TaskId));
           // fhaRequestTasks = fhaRequestTasks.Where(x => x.TaskStepId != 17).ToList();
            //string a = "";
            //Naveen 238 this is for testing query
            //foreach (var a in fhaRequestTasks)
            //{
            //    var test = ((from q in prod_TaskXrefManager.GetProductionSubTasks(a.TaskInstanceId)
            //                 select new { }).Count() == 0 ? 19 : (from q in prod_TaskXrefManager.GetProductionSubTasks(a.TaskInstanceId)
            //                 where q.Status == 17 && a.TaskStepId == 17
            //                 select new { }).Count() > 0 ? 17 : (from q in prod_TaskXrefManager.GetProductionSubTasks(a.TaskInstanceId)
            //                                                     where q.Status == 19 && a.TaskStepId == 19
            //                                                     select new { }).Count() > 0 ? 19 : 17);


            //    var ad = (from q in prod_TaskXrefManager.GetProductionSubTasks(a.TaskInstanceId)
            //              select new { }).Count();

            //}


            

            var productionApTasks = (from appRequest in applicationRequests
                                     join task in productionTasks on appRequest.TaskinstanceId equals task.TaskInstanceId
                                     select new
                                     {
                                         task.TaskInstanceId,
                                         task.TaskId,
                                         //TODO:task Name will  be filled when Production Application user story completed
                                         TaskName = appRequest.projectName,
                                         PropertyName = appRequest.PropertyName,
                                         task.SequenceId,
                                         task.PageTypeId,
                                         task.StartTime,
                                         task.AssignedBy,
                                         task.AssignedTo,
                                         ModofiedOn = DateTime.UtcNow,
                                         //ModofiedOn =ControllerHelper.GetTaskXrefLastupdated(task.TaskInstanceId, task.TaskStepId),
                                         //TODO:Lender Name will  be filled when Production Application user story completed
                                         Lender = appRequest.LenderName,
                                         Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                         Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                         DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
                                         TaskStepId = task.TaskStepId,
                                         ShowStatus = "0"
                                     }).ToList();

            var form290AssignedTasks = (from closeRequest in submittedForm290
                                        join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
                                           on closeRequest.TaskinstanceId equals task.TaskInstanceId
                                        select new
                                        {
                                            task.TaskInstanceId,
                                            task.TaskId,
                                            //TODO:task Name will  be filled when Production Application user story completed
                                            TaskName = closeRequest.projectName,
                                            PropertyName = closeRequest.PropertyName,
                                            task.SequenceId,
                                            task.PageTypeId,
                                            task.StartTime,
                                            task.AssignedBy,
                                            task.AssignedTo,
                                            ModofiedOn = DateTime.UtcNow,
                                            //ModofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(task.TaskInstanceId,task.TaskStepId),
                                            //TODO:Lender Name will  be filled when Production Application user story completed
                                            Lender = closeRequest.LenderName,
                                            Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                            Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                            DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                            LoanType = closeRequest.LoanType,
                                            TaskStepId = task.TaskStepId,
                                            ShowStatus = "0"
                                        }).ToList();

            var olistForm290AssignedTasks = form290AssignedTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus =x.ShowStatus
            }).ToList();

            var olistProductionApTasks = productionApTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus = x.ShowStatus
            }).ToList();

            var olistFhaRequestTasks = fhaRequestTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus = x.ShowStatus
            }).ToList();

            foreach (var i in olistProductionApTasks)
            {
                DateTime StartTime = ControllerHelper.GetProdQStartDate(i.TaskInstanceId);
                DateTime lastUpdated = ControllerHelper.GetTaskXrefLastupdated(i.TaskInstanceId, i.TaskStepId);
                i.StartTime = StartTime;
                i.ModofiedOn = lastUpdated;
            }
            foreach (var i in olistForm290AssignedTasks)
            {
                if (i.Status == "In Queue")
                    statusId = 17;
                if (i.Status == "In Process")
                    statusId = 18;
                if (i.Status == "Firm Commitment Request")
                    statusId = 20;
                if (i.Status == "Application Request Completed")
                    statusId = 19;
                if (i.Status == "Firm Commitment Response")
                    statusId = 21;
                if (i.Status == "Application Complete")
                    statusId = 15;
                if (i.Status == "Form290 In Process")
                    statusId = 22;
                if (i.Status == "Form290 Request Completed")
                    statusId = 23;

                var modofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(i.TaskInstanceId, statusId);

                var o1 = olistProductionApTasks.Where(o => o.TaskId == i.TaskId).ToList();
                var o2 = olistFhaRequestTasks.Where(o => o.TaskId == i.TaskId).ToList();
                if (o1.Count() > 0)
                {
                    o1.ForEach(p => p.ModofiedOn = modofiedOn);
                    o1.ForEach(p => p.StartTime = i.StartTime);
                }
                if (o2.Count() > 0)
                {
                    o2.ForEach(p => p.StartTime = i.StartTime);
                    o2.ForEach(p => p.ModofiedOn = modofiedOn);
                }
            }
            var allProductionTasks = olistFhaRequestTasks.Union(olistProductionApTasks).Union(olistForm290AssignedTasks).ToList();
            if (fhaOrProjName != null && !string.IsNullOrEmpty(fhaOrProjName))
            {
                foreach (var pro in allProductionTasks)
                {
                    if (string.IsNullOrEmpty(pro.PropertyName))
                    {
                        pro.PropertyName = "NA";
                    }
                    //if(string.IsNullOrEmpty(pro.TaskName) && pro.TaskName !="t")
                    //{
                    //    pro.TaskName = "";
                    //}


                }
                //Naveen Hareesh added (x.TaskStepId == 15 to below line of code)
                allProductionTasks = allProductionTasks.Where(x => x.TaskStepId == 15 && (x.TaskName.ToUpper().Contains(fhaOrProjName.ToString().ToUpper()) || fhaOrProjName.ToString().ToUpper().Contains(x.PropertyName.ToUpper()))).ToList();
                // allProductionTasks = allProductionTasks.Where(m => fhaOrProjName.ToString().ToUpper().Contains(m.TaskName.ToUpper()) || fhaOrProjName.ToString().ToUpper().Contains(m.PropertyName.ToUpper())).ToList();
            }

            ////karri#264
            //if (fhaOrProjName != null && !string.IsNullOrEmpty(fhaOrProjName))
            //{
            //    allProductionTasks = allProductionTasks.Where(m => m.TaskName.ToUpper().Contains(fhaOrProjName.ToString().ToUpper())).ToList();
            //}

           // var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First()));
           var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(x=>x.Last()).Where(x => x.Status == "FHA Request Completed" || x.Status == "Application Complete" || x.Status == "Form290 Request Completed").ToList());



            int totalrecods = groupedTasks.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var results = groupedTasks.Skip(pageIndex * pageSize).Take(pageSize);
            // Added by hareesh

            //karritemp
           //results = results.Where(x => x.TaskName.Contains("012-30123")).ToList();

            var actualResults = new List<int>();
            foreach (var task in results)
            {
                string prdtype = "";
                if (applicationType == ((Int32)PageType.FhaRequest))
                    prdtype = "FHA";
                else if (applicationType == ((Int32)PageType.ProductionApplication))
                    prdtype = "Produciton Application";
                else if (applicationType == ((Int32)PageType.ConstructionSingleStage))
                    prdtype = "Single Stage";
                else if (applicationType == ((Int32)PageType.ConstructionTwoStageInitial))
                    prdtype = "2 Stage Initial";
                else if (applicationType == ((Int32)PageType.ConstructionTwoStageFinal))
                    prdtype = "2 Stage Final";
                else if (applicationType == ((Int32)PageType.ClosingAllExceptConstruction))
                    prdtype = "Draft Closing";
                else if (applicationType == ((Int32)PageType.ExecutedClosing))
                    prdtype = "Executed Closing";
                else if (applicationType == ((Int32)PageType.ClosingConstructionInsuredAdvances))
                    prdtype = "closing advances";
                else if (applicationType == ((Int32)PageType.ClosingConstructionInsuranceUponCompletion))
                    prdtype = "closing completion";
                else if (applicationType == ((Int32)PageType.ConstructionManagement))
                    prdtype = "construction mgt";
                else if (applicationType == ((Int32)PageType.Form290))
                    prdtype = "Form 290";
                else if (applicationType == ((Int32)PageType.Amendments))
                    prdtype = "Amendment";
                // hareesh added below line to differentiate Form 290 form 25-11-2019
                if (prdtype == "Form 290")
                    actualResults.Add(task.TaskId);
                var subtasks = GetSubTasksByTaskInstanceId(task.TaskInstanceId, prdtype, "").ToList();

                var subtasks1 = (from q in subtasks
                            where (q.ViewId == 9 && q.Status == "In Queue") || (q.ViewId == 10 && q.Status == "In Queue")
                            select q).ToList();

                //if (subtasks1.Count() == subtasks.Count())
                    if (subtasks1.Count() > 0)
                    {
                    //var lstFhaType = subtasks.Where(q => (q.ViewId == 9 && q.Status == "In Queue") || (q.ViewId == 10 && q.Status == "In Queue"));
                    actualResults.Add(task.TaskId);
                    // actualResults.Add(task.TaskId);
                }


                var lstTasks = subtasks.Where(x => x.Status == "Review Completed" && (x.FhaType == "UnderWriter" || x.FhaType == "Underwriter/Closer")).Select(x=>x.TaskId);
                if (subtasks.Count < 4)
                {
                    task.ShowStatus = "1";
                     //var lstUnassignedtask = subtasks.Where(x => x.Status == "Unassigned").ToList();
                     //if (lstUnassignedtask.Any())
                     //{
                    //task.ShowStatus = "1";
                   //}
                }
                //if (lstTasks.Count() != subtasks.Count())
                //{
                //    actualResults.Add(task.TaskId);

                //    //var lstFhaType = subtasks.Where(x => x.FhaType == "UnderWriter" || x.FhaType == "Underwriter/Closer");
                //    //if (!lstFhaType.Any())
                //    //{
                //    //    actualResults.Add(task.TaskId);
                //    //}
                //    //else
                //    //{
                //    //    int tid = task.TaskId;
                //    //}

                //    // actualResults.Add(task.TaskId);
                //}
                if(lstTasks.Any())
                {
                    foreach(var i in lstTasks)
                    //actualResults.Add(task.TaskId);
                    actualResults.Add((int)i);
                }
                else
                {
                    int tid = task.TaskId;
                }

            }

            var actualTasks = applicationType > 0 ? results.Where(x => !actualResults.Contains(x.TaskId)) : results;




            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = actualTasks.Where(t => t.TaskName != "t").ToList(),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //karri#264
        private IList<String> getAssignedSearchableData(int applicationType = 0, int status = 0)
        {

            int statusId = 0;
            //int pageIndex = Convert.ToInt32(page) - 1;
            //int pageSize = rows;
            IEnumerable<TaskModel> productionTasks;
            if (applicationType == (int)PageType.Form290)
            {
                if (status == (int)TaskStep.InProcess) status = (int)TaskStep.Form290Request;
                if (status == (int)TaskStep.Complete) status = (int)TaskStep.Form290Complete;

            }
            if (applicationType == (int)PageType.NotSpecified)
            {
                productionTasks = status == 0 ? productionQueue.GetProductionTasks().ToList() : productionQueue.GetProductionTaskByStatus(status).ToList();

                //karri#434;restricting the FHA Requests only for assaigned tab
                productionTasks = (from q in productionTasks
                                   where q.PageTypeId == 4 && (q.AssignedTo != "Queue" && q.AssignedTo != null)
                                   select q).ToList();
            }
            else
            {
                if (applicationType != (int)PageType.FhaRequest && status == (int)ProductionAppProcessStatus.Completed)
                    status = (int)ProductionAppProcessStatus.ProjectActionRequestComplete;
                productionTasks = status == 0 ? productionQueue.GetProductionTaskByType(applicationType) : productionQueue.GetProductionTaskByTypeAndStatus(applicationType, status).ToList();

            }
            var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
            var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

            var applicationRequests = appProcessManager.GetAppRequests().ToList();
            var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
            List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
            if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
            {
                submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
            }


            var fhaRequestTasks = (from task in taskModels.Where(m => m.PageTypeId == (int)PageType.FhaRequest)
                                   join fhaRequest in productionQueue.GetFhaRequests(distnictInstanceId).ToList()
                                   //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
                                   on task.TaskInstanceId equals fhaRequest.TaskinstanceId

                                   select new
                                   {
                                       task.TaskInstanceId,
                                       task.TaskId,
                                       TaskName = fhaRequest.projectName,
                                       PropertyName = fhaRequest.PropertyName,
                                       task.SequenceId,
                                       task.PageTypeId,
                                       task.StartTime,
                                       task.AssignedBy,
                                       task.AssignedTo,
                                       ModofiedOn = fhaRequest.ModifiedOn,
                                       Lender = fhaRequest.LenderName,
                                       //Naveen 238 (31july and 01aug: status as been corrected by writing query to right status)
                                       //Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(task.TaskStepId.ToString());
                                       Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(((from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                         select new { }).Count() == 0 ? 19 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                              where q.Status == 17 && task.TaskStepId == 17
                                                                                                                                                              select new { }).Count() > 0 ? 18 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                                                                  where q.Status == 19 && task.TaskStepId == 19
                                                                                                                                                                                                  select new { }).Count() > 0 ? 19 : 18).ToString())),
                                       Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                       DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                       LoanType = fhaRequest.LoanType,
                                       TaskStepId = task.TaskStepId
                                   });


            var productionApTasks = (from appRequest in applicationRequests
                                     join task in productionTasks
             on appRequest.TaskinstanceId equals task.TaskInstanceId
                                     select new
                                     {
                                         task.TaskInstanceId,
                                         task.TaskId,
                                         //TODO:task Name will  be filled when Production Application user story completed
                                         TaskName = appRequest.projectName,
                                         PropertyName = appRequest.PropertyName,
                                         task.SequenceId,
                                         task.PageTypeId,
                                         task.StartTime,
                                         task.AssignedBy,
                                         task.AssignedTo,
                                         ModofiedOn = DateTime.UtcNow,
                                         //ModofiedOn =ControllerHelper.GetTaskXrefLastupdated(task.TaskInstanceId, task.TaskStepId),
                                         //TODO:Lender Name will  be filled when Production Application user story completed
                                         Lender = appRequest.LenderName,
                                         Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                         Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                         DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
                                         TaskStepId = task.TaskStepId
                                     }).ToList();

            var form290AssignedTasks = (from closeRequest in submittedForm290
                                        join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
                                           on closeRequest.TaskinstanceId equals task.TaskInstanceId
                                        select new
                                        {
                                            task.TaskInstanceId,
                                            task.TaskId,
                                            //TODO:task Name will  be filled when Production Application user story completed
                                            TaskName = closeRequest.projectName,
                                            PropertyName = closeRequest.PropertyName,
                                            task.SequenceId,
                                            task.PageTypeId,
                                            task.StartTime,
                                            task.AssignedBy,
                                            task.AssignedTo,
                                            ModofiedOn = DateTime.UtcNow,
                                            //ModofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(task.TaskInstanceId,task.TaskStepId),
                                            //TODO:Lender Name will  be filled when Production Application user story completed
                                            Lender = closeRequest.LenderName,
                                            Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                            Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                            DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                            LoanType = closeRequest.LoanType,
                                            TaskStepId = task.TaskStepId
                                        }).ToList();

            var olistForm290AssignedTasks = form290AssignedTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId
            }).ToList();

            var olistProductionApTasks = productionApTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId
            }).ToList();

            var olistFhaRequestTasks = fhaRequestTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId
            }).ToList();

            foreach (var i in olistProductionApTasks)
            {
                DateTime StartTime = ControllerHelper.GetProdQStartDate(i.TaskInstanceId);
                DateTime lastUpdated = ControllerHelper.GetTaskXrefLastupdated(i.TaskInstanceId, i.TaskStepId);
                i.StartTime = StartTime;
                i.ModofiedOn = lastUpdated;
            }
            foreach (var i in olistForm290AssignedTasks)
            {
                if (i.Status == "In Queue")
                    statusId = 17;
                if (i.Status == "In Process")
                    statusId = 18;
                if (i.Status == "Firm Commitment Request")
                    statusId = 20;
                if (i.Status == "Application Request Completed")
                    statusId = 19;
                if (i.Status == "Firm Commitment Response")
                    statusId = 21;
                if (i.Status == "Application Complete")
                    statusId = 15;
                if (i.Status == "Form290 In Process")
                    statusId = 22;
                if (i.Status == "Form290 Request Completed")
                    statusId = 23;

                var modofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(i.TaskInstanceId, statusId);

                var o1 = olistProductionApTasks.Where(o => o.TaskId == i.TaskId).ToList();
                var o2 = olistFhaRequestTasks.Where(o => o.TaskId == i.TaskId).ToList();
                if (o1.Count() > 0)
                {
                    o1.ForEach(p => p.ModofiedOn = modofiedOn);
                    o1.ForEach(p => p.StartTime = i.StartTime);
                }
                if (o2.Count() > 0)
                {
                    o2.ForEach(p => p.StartTime = i.StartTime);
                    o2.ForEach(p => p.ModofiedOn = modofiedOn);
                }
            }
            var allProductionTasks = olistFhaRequestTasks.Union(olistProductionApTasks).Union(olistForm290AssignedTasks).ToList();

            var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First())).Where(q => q.Status != "FHA Request Completed" && q.Status != "Application Complete" && q.Status != "Form290 Request Completed");

            IList<String> searchableDataList = new List<string>();

            ArrayList searchableData = new ArrayList();

            //productionTasksForQueue = productionTasksForQueue.ToList();
            var myTasks = groupedTasks.ToList();
            foreach (var item in myTasks)
            {
                if (item.PropertyName != null && !string.IsNullOrEmpty(item.PropertyName))
                    searchableData.Add(item.PropertyName);
                if (item.TaskName != null && !string.IsNullOrEmpty(item.TaskName) && item.TaskName != "fharequest")
                {
                    if (item.TaskName.Contains("(") && item.TaskName.StartsWith("(") && item.TaskName.Contains(")"))
                    {
                        string possibledata = item.TaskName.Substring(1).Substring(0, item.TaskName.IndexOf(")") - 1);
                        if (!searchableData.Contains(possibledata))
                            searchableData.Add(possibledata);
                    }
                    else
                    {
                        if (!searchableData.Contains(item.TaskName.ToString()))
                            searchableData.Add(item.TaskName.ToString());
                    }

                }
                else if (item.PropertyName != null && !string.IsNullOrEmpty(item.PropertyName))
                {
                    searchableData.Add(item.PropertyName.ToString());
                }
            }
            searchableData.Sort();//ASC

            foreach (string value in searchableData)
                searchableDataList.Add(value);


            return searchableDataList;
        }

        //karri#264
        private IList<String> getCompletedSearchableData(int applicationType = 0, int status = 0)
        {
            string sidx = null;
            string sord = "asc";
            int page = 1;
            int rows = 40;
            applicationType = 0;
            //naveen 11/09/2019
            status = 19;
            string fhaOrProjName = null;

            int statusId = 0;
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            IEnumerable<TaskModel> productionTasks;
            if (applicationType == (int)PageType.Form290)
            {
                if (status == (int)TaskStep.InProcess) status = (int)TaskStep.Form290Request;
                if (status == (int)TaskStep.Complete) status = (int)TaskStep.Form290Complete;

            }
            if (applicationType == (int)PageType.NotSpecified)
            {
                productionTasks = status == 0 ? productionQueue.GetProductionTasks().ToList() : productionQueue.GetProductionTaskByStatus(status).ToList();
            }
            else
            {
                if (applicationType != (int)PageType.FhaRequest && status == (int)ProductionAppProcessStatus.Completed)
                    status = (int)ProductionAppProcessStatus.ProjectActionRequestComplete;
                productionTasks = status == 0 ? productionQueue.GetProductionTaskByType(applicationType) : productionQueue.GetProductionTaskByTypeAndStatus(applicationType, status).ToList();

            }
            var taskModels = productionTasks as IList<TaskModel> ?? productionTasks.ToList();
            var distnictInstanceId = taskModels.Select(m => m.TaskInstanceId).Distinct().ToList();

            var applicationRequests = appProcessManager.GetAppRequests().ToList();
            var form290TaskInstances = taskModels.Where(m => m.PageTypeId == (int)PageType.Form290).Select(m => m.TaskInstanceId).Distinct().ToList();
            List<ProductionQueueLenderInfo> submittedForm290 = new List<ProductionQueueLenderInfo>();
            if (applicationRequests.Count > 0 && form290TaskInstances.Count > 0)
            {
                submittedForm290 = GetForm290Task(applicationRequests, form290TaskInstances).ToList();
            }


            var fhaRequestTasks = (from task in taskModels.Where(m => m.PageTypeId == (int)PageType.FhaRequest)
                                   join fhaRequest in productionQueue.GetFhaRequests(distnictInstanceId).ToList()
                                   //fhatRequests.Where(m =>TaskinstanceId.Contains(m.distnictInstanceId)).ToList()
                                   on task.TaskInstanceId equals fhaRequest.TaskinstanceId

                                   select new
                                   {
                                       task.TaskInstanceId,
                                       task.TaskId,
                                       TaskName = fhaRequest.projectName,
                                       PropertyName = fhaRequest.PropertyName,
                                       task.SequenceId,
                                       task.PageTypeId,
                                       task.StartTime,
                                       task.AssignedBy,
                                       task.AssignedTo,
                                       ModofiedOn = fhaRequest.ModifiedOn,
                                       Lender = fhaRequest.LenderName,

                                       //Naveen 238 (31july and 01aug: status as been corrected by writing query to right status)
                                       //Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(task.TaskStepId.ToString());
                                       Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(((from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                         select new { }).Count() == 0 ? 19 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                              where q.Status == 17 && task.TaskStepId == 17
                                                                                                                                                              select new { }).Count() > 0 ? 18 : (from q in prod_TaskXrefManager.GetProductionSubTasks(task.TaskInstanceId)
                                                                                                                                                                                                  where q.Status == 19 && task.TaskStepId == 19
                                                                                                                                                                                                  select new { }).Count() > 0 ? 19 : 18).ToString())),
                                       Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                       DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                       LoanType = fhaRequest.LoanType,
                                       TaskStepId = task.TaskStepId,
                                       ShowStatus = "0"
                                   });


            var productionApTasks = (from appRequest in applicationRequests
                                     join task in productionTasks on appRequest.TaskinstanceId equals task.TaskInstanceId
                                     select new
                                     {
                                         task.TaskInstanceId,
                                         task.TaskId,
                                         //TODO:task Name will  be filled when Production Application user story completed
                                         TaskName = appRequest.projectName,
                                         PropertyName = appRequest.PropertyName,
                                         task.SequenceId,
                                         task.PageTypeId,
                                         task.StartTime,
                                         task.AssignedBy,
                                         task.AssignedTo,
                                         ModofiedOn = DateTime.UtcNow,
                                         //ModofiedOn =ControllerHelper.GetTaskXrefLastupdated(task.TaskInstanceId, task.TaskStepId),
                                         //TODO:Lender Name will  be filled when Production Application user story completed
                                         Lender = appRequest.LenderName,
                                         Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                         Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                         DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                         LoanType = appRequest.LoanType,
                                         TaskStepId = task.TaskStepId,
                                         ShowStatus = "0"
                                     }).ToList();

            var form290AssignedTasks = (from closeRequest in submittedForm290
                                        join task in productionTasks.Where(m => m.PageTypeId == (int)PageType.Form290)
                                           on closeRequest.TaskinstanceId equals task.TaskInstanceId
                                        select new
                                        {
                                            task.TaskInstanceId,
                                            task.TaskId,
                                            //TODO:task Name will  be filled when Production Application user story completed
                                            TaskName = closeRequest.projectName,
                                            PropertyName = closeRequest.PropertyName,
                                            task.SequenceId,
                                            task.PageTypeId,
                                            task.StartTime,
                                            task.AssignedBy,
                                            task.AssignedTo,
                                            ModofiedOn = DateTime.UtcNow,
                                            //ModofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(task.TaskInstanceId,task.TaskStepId),
                                            //TODO:Lender Name will  be filled when Production Application user story completed
                                            Lender = closeRequest.LenderName,
                                            Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionAppProcessStatus>(task.TaskStepId.ToString())),
                                            Type = EnumType.GetEnumDescription(EnumType.Parse<PageType>(task.PageTypeId.ToString())),
                                            DaysInQueue = (DateTime.Now - task.StartTime).Days + " Day(s)",
                                            LoanType = closeRequest.LoanType,
                                            TaskStepId = task.TaskStepId,
                                            ShowStatus = "0"
                                        }).ToList();

            var olistForm290AssignedTasks = form290AssignedTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus = x.ShowStatus
            }).ToList();

            var olistProductionApTasks = productionApTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus = x.ShowStatus
            }).ToList();

            var olistFhaRequestTasks = fhaRequestTasks.Select(x => new AnonymousTaskMode
            {
                TaskInstanceId = x.TaskInstanceId,
                TaskId = x.TaskId,
                TaskName = x.TaskName,
                PropertyName = x.PropertyName,
                SequenceId = x.SequenceId,
                PageTypeId = x.PageTypeId,
                StartTime = x.StartTime,
                AssignedBy = x.AssignedBy,
                AssignedTo = x.AssignedTo,
                ModofiedOn = x.ModofiedOn,
                Lender = x.Lender,
                Status = x.Status,
                Type = x.Type,
                DaysInQueue = x.DaysInQueue,
                LoanType = x.LoanType,
                TaskStepId = x.TaskStepId,
                ShowStatus = x.ShowStatus
            }).ToList();

            foreach (var i in olistProductionApTasks)
            {
                DateTime StartTime = ControllerHelper.GetProdQStartDate(i.TaskInstanceId);
                DateTime lastUpdated = ControllerHelper.GetTaskXrefLastupdated(i.TaskInstanceId, i.TaskStepId);
                i.StartTime = StartTime;
                i.ModofiedOn = lastUpdated;
            }
            foreach (var i in olistForm290AssignedTasks)
            {
                if (i.Status == "In Queue")
                    statusId = 17;
                if (i.Status == "In Process")
                    statusId = 18;
                if (i.Status == "Firm Commitment Request")
                    statusId = 20;
                if (i.Status == "Application Request Completed")
                    statusId = 19;
                if (i.Status == "Firm Commitment Response")
                    statusId = 21;
                if (i.Status == "Application Complete")
                    statusId = 15;
                if (i.Status == "Form290 In Process")
                    statusId = 22;
                if (i.Status == "Form290 Request Completed")
                    statusId = 23;

                var modofiedOn = ControllerHelper.LastUpdatedForm290ProdTaskByInstanceId(i.TaskInstanceId, statusId);

                var o1 = olistProductionApTasks.Where(o => o.TaskId == i.TaskId).ToList();
                var o2 = olistFhaRequestTasks.Where(o => o.TaskId == i.TaskId).ToList();
                if (o1.Count() > 0)
                {
                    o1.ForEach(p => p.ModofiedOn = modofiedOn);
                    o1.ForEach(p => p.StartTime = i.StartTime);
                }
                if (o2.Count() > 0)
                {
                    o2.ForEach(p => p.StartTime = i.StartTime);
                    o2.ForEach(p => p.ModofiedOn = modofiedOn);
                }
            }
            var allProductionTasks = olistFhaRequestTasks.Union(olistProductionApTasks).Union(olistForm290AssignedTasks).ToList();

            //var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First()));
            var groupedTasks = (allProductionTasks.GroupBy(m => new { m.TaskInstanceId }).Select(g => g.First())).Where(x => x.Status == "FHA Request Completed" || x.Status == "Application Complete" || x.Status == "Form290 Request Completed").ToList();

            IList<String> searchableDataList = new List<string>();

            ArrayList searchableData = new ArrayList();

            //productionTasksForQueue = productionTasksForQueue.ToList();
            var myTasks = groupedTasks.ToList();
            foreach (var item in myTasks)
            {
                if (item.PropertyName != null && !string.IsNullOrEmpty(item.PropertyName))
                    searchableData.Add(item.PropertyName);
                if (item.TaskName != null && !string.IsNullOrEmpty(item.TaskName) && item.TaskName != "fharequest")
                {
                    if (item.TaskName.Contains("(") && item.TaskName.StartsWith("(") && item.TaskName.Contains(")"))
                    {
                        string possibledata = item.TaskName.Substring(1).Substring(0, item.TaskName.IndexOf(")") - 1);
                        if (!searchableData.Contains(possibledata))
                            searchableData.Add(possibledata);
                    }
                    else
                    {
                        if (!searchableData.Contains(item.TaskName.ToString()))
                            searchableData.Add(item.TaskName.ToString());
                    }

                }
                else
                {
                    if (!searchableData.Contains(item.TaskName.ToString()))
                        searchableData.Add(item.TaskName.ToString());
                }

            }
            searchableData.Sort();//ASC

            foreach (string value in searchableData)
                searchableDataList.Add(value);


            return searchableDataList;
        }


    }
}
