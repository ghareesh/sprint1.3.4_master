﻿using BusinessService.Interfaces;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class ProductionQueuelinktabsController : Controller
    {
        private IProductionQueueManager productionQueue;
        private IProd_TaskXrefManager prod_TaskXrefManager;
        private ITaskManager taskManager;
        private IFHANumberRequestManager fhaNumberRequestManager;
        private IProd_NoteManager prod_noteManager;
        private IAppProcessManager appProcessManager;
        private IProd_ViewManager prodviewmanager;
        private IBackgroundJobMgr backgroundJobManager;
        private IProjectActionFormManager _projectActionFormManager;
        private IProd_SharepointScreenManager sharepointScreenManager;
        private IProd_RestfulWebApiDocumentUpload uploadApiManager;
        private IProd_RestfulWebApiTokenRequest webApiTokenRequest;
        private IProd_RestfulWebApiDownload webApiDownload;
        private IAccountManager accountManager;
        private IProd_LoanCommitteeManager loanCommitteManager;
        //added by siddu @21042020//
        private IParentChildTaskManager _parentChildTaskManager;
        //private ITaskManager _taskManager;
        // private IProd_TaskXrefManager _prodTaskXrefManager;
        string[] authorsList;
        string[] authorsListcompleted;
        string[] apptypearr;
        List<Prod_Taskassigned> lstTasksmain = new List<Prod_Taskassigned>();
        IEnumerable<TaskModel> productionTasks;
        IEnumerable<TaskModel> productionTasksmultiplefilterdata;
        IEnumerable<FilteredProductionTasksModel> productionTasksunasiignedfilter;
        //ArrayList getpagetypeidunassignedsearch = new ArrayList();
        public ProductionQueuelinktabsController()
            : this(new TaskManager(), new Prod_TaskXrefManager(), new ProductionQueueManager(), new FHANumberRequestManager(), new Prod_NoteManager(), new AppProcessManager(),
            new Prod_ViewManager(), new BackgroundJobMgr(), new ProjectActionFormManager(), new Prod_SharepointScreenManager(), new Prod_RestfulWebApiDocumentUpload(),
            new Prod_RestfulWebApiTokenRequest(), new Prod_RestfulWebApiDownload(), new AccountManager(), new Prod_LoanCommitteeManager(), new ParentChildTaskManager())
        {

        }
        public ProductionQueuelinktabsController(ITaskManager _taskManager, IProd_TaskXrefManager _prodTaskXrefManager, IProductionQueueManager _productionQueueManager,
                                          IFHANumberRequestManager _fhaNumberRequestManager, IProd_NoteManager _prodNoteManager, IAppProcessManager _appProcessManager,
            IProd_ViewManager _prodviewmanager, IBackgroundJobMgr backgroundManager, IProjectActionFormManager projectActionFormManager,
            IProd_SharepointScreenManager _sharepointScreenManager, IProd_RestfulWebApiDocumentUpload _prod_RestfulWebApiDocumentUpload,
            IProd_RestfulWebApiTokenRequest _prod_WebApiTokenRequest, IProd_RestfulWebApiDownload _webApiDownload, IAccountManager _accountManager,
            IProd_LoanCommitteeManager _loanCommitteeManager, IParentChildTaskManager parentChildTaskManager)
        {

            taskManager = _taskManager;
            prod_TaskXrefManager = _prodTaskXrefManager;
            productionQueue = _productionQueueManager;
            fhaNumberRequestManager = _fhaNumberRequestManager;
            prod_noteManager = _prodNoteManager;
            appProcessManager = _appProcessManager;
            prodviewmanager = _prodviewmanager;
            backgroundJobManager = backgroundManager;
            _projectActionFormManager = projectActionFormManager;
            sharepointScreenManager = _sharepointScreenManager;
            uploadApiManager = _prod_RestfulWebApiDocumentUpload;
            webApiTokenRequest = _prod_WebApiTokenRequest;
            webApiDownload = _webApiDownload;
            accountManager = _accountManager;
            loanCommitteManager = _loanCommitteeManager;
            _parentChildTaskManager = parentChildTaskManager;
        }

        //Added by siddu #841 @21042020
        public ActionResult GetTaskDetail(Guid taskInstanceId)
        {
            var formUploadData = new OPAViewModel();
            var isChildTask = false;
            var task = new TaskModel();
            var xrefmodel = new Prod_TaskXrefModel();

            formUploadData.isChildTaskOpen = false;

            if (_parentChildTaskManager.IsParentTaskAvailable(taskInstanceId))
            {
                task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);

                formUploadData = _projectActionFormManager.GetProdOPAByChildTaskId(task.TaskInstanceId);
                formUploadData.viewId = 0;
                formUploadData.ChildTaskInstanceId = task.TaskInstanceId;
                isChildTask = true;
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    formUploadData.IsLoggedInUserLender = false;
                }
            }
            else
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }

                else if (RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    task = taskManager.GetLatestTaskByTaskXrefid(taskInstanceId);
                    if (task == null)
                    {
                        task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                        taskInstanceId = prod_TaskXrefManager.GetUnderwriterTaskXrefIdByParentTaskInstanceId(task.TaskInstanceId);
                    }
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);

                    //logic to check is an child open task  and for confirmation popup flag
                    formUploadData.isChildTaskOpen = false;
                    List<TaskModel> childTaskList = _projectActionFormManager.GetProdChildTasksByXrefId(task.TaskInstanceId, false);
                    if (childTaskList != null && childTaskList.Count > 0)
                    {

                        foreach (var childList in childTaskList)
                        {

                            if (childList.TaskStepId == (int)TaskStep.OPAAddtionalInformation)
                            {
                                formUploadData.isChildTaskOpen = true;
                                break;
                            }

                        }
                    }
                    xrefmodel = taskManager.GetReviewerViewIdByXrefTaskInstanceId(taskInstanceId);
                    formUploadData.viewId = xrefmodel != null ? xrefmodel.ViewId : 0;
                    formUploadData.reviertaskStatus = xrefmodel != null ? xrefmodel.Status : 0;
                    formUploadData.taskxrefId = taskInstanceId;
                    formUploadData.IsLoggedInUserLender = false;
                }

            }
            // based on task step, decide if is edit mode or readonly mode
            bool isEditMode = true;
            //if (task.TaskStepId == 17)
            //{
            //    task.TaskStepId = 14;
            //}

            if (xrefmodel != null && xrefmodel.CompletedOn != null && xrefmodel.Status == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequest && RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)ProductionAppProcessStatus.Request && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }



            bool hasTaskOpenByUser = false;
            formUploadData.IschildTask = isChildTask;

            formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                    ? new TaskOpenStatusModel()
                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                        TaskOpenStatusModel; //Know y and how

            formUploadData.TaskGuid = task.TaskInstanceId;
            formUploadData.taskxrefId = taskInstanceId;
            formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
            formUploadData.SequenceId = task.SequenceId;
            formUploadData.AssignedBy = task.AssignedBy;
            formUploadData.AssignedTo = task.AssignedTo;
            formUploadData.IsReassigned = task.IsReAssigned;
            formUploadData.TaskId = task.TaskId;
            formUploadData.PropertyId = _projectActionFormManager.GetProdPropertyInfo(formUploadData.FhaNumber).PropertyId;
            formUploadData.TaskStepId = task.TaskStepId;
            formUploadData.IsEditMode = isEditMode;
            formUploadData.pageTypeId = (int)task.PageTypeId;
            hasTaskOpenByUser = formUploadData.TaskOpenStatus.HasTaskOpenByUser(UserPrincipal.Current.UserName);

            // save task opened in task db
            //if (formUploadData.TaskOpenStatus != null)
            //{
            //    formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
            //    task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus,
            //        typeof(TaskOpenStatusModel), Encoding.Unicode);
            //    _taskManager.UpdateTask(task);
            //}

            //Logic for reassign of parent task after a child request is created //Know y and how
            if (_parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
            {
                var parenttask = _parentChildTaskManager.GetParentTask(task.TaskInstanceId);

                //if (parenttask != null)
                //{
                //    var newreassignedbyAe = string.Empty;

                //    newreassignedbyAe = taskReAssignmentManager.GetReassignedAE(parenttask.ParentTaskInstanceId);
                //    if (newreassignedbyAe != string.Empty)
                //        formUploadData.AssignedBy = newreassignedbyAe;
                //}
            }
            //Flag to identify Pam Report
            if (Request.QueryString.Count > 1)
            {
                if (Request.QueryString["IsFromPam"] == "LenderPam")
                {
                    formUploadData.IsLenderPAMReport = true;
                }
                else
                {
                    formUploadData.IsPAMReport = true;
                }
            }
            formUploadData.IsProdQueuCompleted = true;


            //formUploadData.ServicerSubmissionDate =
            //  TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
            formUploadData.ServicerSubmissionDate =
           (DateTime)formUploadData.ServicerSubmissionDate;
            TempData["ApplicationRequestFormData"] = formUploadData;
            TempData["hasTaskOpenByUser"] = hasTaskOpenByUser;

            return RedirectToAction("GetApplicationFormDetail", "ProductionApplication", new { model = formUploadData });


        }

        //Added by siddu #841 @21042020
        public ActionResult GetunassignedTaskDetail(Guid taskInstanceId)
        {
            var formUploadData = new OPAViewModel();
            var isChildTask = false;
            var task = new TaskModel();
            var xrefmodel = new Prod_TaskXrefModel();

            formUploadData.isChildTaskOpen = false;

            if (_parentChildTaskManager.IsParentTaskAvailable(taskInstanceId))
            {
                task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);

                formUploadData = _projectActionFormManager.GetProdOPAByChildTaskId(task.TaskInstanceId);
                formUploadData.viewId = 0;
                formUploadData.ChildTaskInstanceId = task.TaskInstanceId;
                isChildTask = true;
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    formUploadData.IsLoggedInUserLender = false;
                }
            }
            else
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }

                else if (RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    task = taskManager.GetLatestTaskByTaskXrefid(taskInstanceId);
                    if (task == null)
                    {
                        task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                        taskInstanceId = prod_TaskXrefManager.GetUnderwriterTaskXrefIdByParentTaskInstanceId(task.TaskInstanceId);
                    }
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);

                    //logic to check is an child open task  and for confirmation popup flag
                    formUploadData.isChildTaskOpen = false;
                    List<TaskModel> childTaskList = _projectActionFormManager.GetProdChildTasksByXrefId(task.TaskInstanceId, false);
                    if (childTaskList != null && childTaskList.Count > 0)
                    {

                        foreach (var childList in childTaskList)
                        {

                            if (childList.TaskStepId == (int)TaskStep.OPAAddtionalInformation)
                            {
                                formUploadData.isChildTaskOpen = true;
                                break;
                            }

                        }
                    }
                    xrefmodel = taskManager.GetReviewerViewIdByXrefTaskInstanceId(taskInstanceId);
                    formUploadData.viewId = xrefmodel != null ? xrefmodel.ViewId : 0;
                    formUploadData.reviertaskStatus = xrefmodel != null ? xrefmodel.Status : 0;
                    formUploadData.taskxrefId = taskInstanceId;
                    formUploadData.IsLoggedInUserLender = false;
                }

            }
            // based on task step, decide if is edit mode or readonly mode
            bool isEditMode = true;
            //if (task.TaskStepId == 17)
            //{
            //    task.TaskStepId = 14;
            //}

            if (xrefmodel != null && xrefmodel.CompletedOn != null && xrefmodel.Status == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequest && RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)ProductionAppProcessStatus.Request && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }



            bool hasTaskOpenByUser = false;
            formUploadData.IschildTask = isChildTask;

            formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                    ? new TaskOpenStatusModel()
                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                        TaskOpenStatusModel; //Know y and how

            formUploadData.TaskGuid = task.TaskInstanceId;
            formUploadData.taskxrefId = taskInstanceId;
            formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
            formUploadData.SequenceId = task.SequenceId;
            formUploadData.AssignedBy = task.AssignedBy;
            formUploadData.AssignedTo = task.AssignedTo;
            formUploadData.IsReassigned = task.IsReAssigned;
            formUploadData.TaskId = task.TaskId;
            formUploadData.PropertyId = _projectActionFormManager.GetProdPropertyInfo(formUploadData.FhaNumber).PropertyId;
            formUploadData.TaskStepId = task.TaskStepId;
            formUploadData.IsEditMode = isEditMode;
            formUploadData.pageTypeId = (int)task.PageTypeId;
            hasTaskOpenByUser = formUploadData.TaskOpenStatus.HasTaskOpenByUser(UserPrincipal.Current.UserName);

            // save task opened in task db
            //if (formUploadData.TaskOpenStatus != null)
            //{
            //    formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
            //    task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus,
            //        typeof(TaskOpenStatusModel), Encoding.Unicode);
            //    _taskManager.UpdateTask(task);
            //}

            //Logic for reassign of parent task after a child request is created //Know y and how
            if (_parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
            {
                var parenttask = _parentChildTaskManager.GetParentTask(task.TaskInstanceId);

                //if (parenttask != null)
                //{
                //    var newreassignedbyAe = string.Empty;

                //    newreassignedbyAe = taskReAssignmentManager.GetReassignedAE(parenttask.ParentTaskInstanceId);
                //    if (newreassignedbyAe != string.Empty)
                //        formUploadData.AssignedBy = newreassignedbyAe;
                //}
            }
            //Flag to identify Pam Report
            if (Request.QueryString.Count > 1)
            {
                if (Request.QueryString["IsFromPam"] == "LenderPam")
                {
                    formUploadData.IsLenderPAMReport = true;
                }
                else
                {
                    formUploadData.IsPAMReport = true;
                }
            }
            formUploadData.IsProdQueuQueue = true;

            //formUploadData.ServicerSubmissionDate =
            //  TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
            formUploadData.ServicerSubmissionDate =
              (DateTime)formUploadData.ServicerSubmissionDate;
            TempData["ApplicationRequestFormData"] = formUploadData;
            TempData["hasTaskOpenByUser"] = hasTaskOpenByUser;

            return RedirectToAction("GetApplicationFormDetail", "ProductionApplication", new { model = formUploadData });


        }
        //Added by siddu #841 @21042020
        public ActionResult GetassignedTaskDetail(Guid taskInstanceId)
        {
            var formUploadData = new OPAViewModel();
            var isChildTask = false;
            var task = new TaskModel();
            var xrefmodel = new Prod_TaskXrefModel();

            formUploadData.isChildTaskOpen = false;

            if (_parentChildTaskManager.IsParentTaskAvailable(taskInstanceId))
            {
                task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);

                formUploadData = _projectActionFormManager.GetProdOPAByChildTaskId(task.TaskInstanceId);
                formUploadData.viewId = 0;
                formUploadData.ChildTaskInstanceId = task.TaskInstanceId;
                isChildTask = true;
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    formUploadData.IsLoggedInUserLender = false;
                }
            }
            else
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }

                else if (RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    task = taskManager.GetLatestTaskByTaskXrefid(taskInstanceId);
                    if (task == null)
                    {
                        task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                        taskInstanceId = prod_TaskXrefManager.GetUnderwriterTaskXrefIdByParentTaskInstanceId(task.TaskInstanceId);
                    }
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);

                    //logic to check is an child open task  and for confirmation popup flag
                    formUploadData.isChildTaskOpen = false;
                    List<TaskModel> childTaskList = _projectActionFormManager.GetProdChildTasksByXrefId(task.TaskInstanceId, false);
                    if (childTaskList != null && childTaskList.Count > 0)
                    {

                        foreach (var childList in childTaskList)
                        {

                            if (childList.TaskStepId == (int)TaskStep.OPAAddtionalInformation)
                            {
                                formUploadData.isChildTaskOpen = true;
                                break;
                            }

                        }
                    }
                    xrefmodel = taskManager.GetReviewerViewIdByXrefTaskInstanceId(taskInstanceId);
                    formUploadData.viewId = xrefmodel != null ? xrefmodel.ViewId : 0;
                    formUploadData.reviertaskStatus = xrefmodel != null ? xrefmodel.Status : 0;
                    formUploadData.taskxrefId = taskInstanceId;
                    formUploadData.IsLoggedInUserLender = false;
                }

            }
            // based on task step, decide if is edit mode or readonly mode
            bool isEditMode = true;
            //if (task.TaskStepId == 17)
            //{
            //    task.TaskStepId = 14;
            //}

            if (xrefmodel != null && xrefmodel.CompletedOn != null && xrefmodel.Status == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequest && RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)ProductionAppProcessStatus.Request && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }



            bool hasTaskOpenByUser = false;
            formUploadData.IschildTask = isChildTask;

            formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                    ? new TaskOpenStatusModel()
                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                        TaskOpenStatusModel; //Know y and how

            formUploadData.TaskGuid = task.TaskInstanceId;
            formUploadData.taskxrefId = taskInstanceId;
            formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
            formUploadData.SequenceId = task.SequenceId;
            formUploadData.AssignedBy = task.AssignedBy;
            formUploadData.AssignedTo = task.AssignedTo;
            formUploadData.IsReassigned = task.IsReAssigned;
            formUploadData.TaskId = task.TaskId;
            formUploadData.PropertyId = _projectActionFormManager.GetProdPropertyInfo(formUploadData.FhaNumber).PropertyId;
            formUploadData.TaskStepId = task.TaskStepId;
            formUploadData.IsEditMode = isEditMode;
            formUploadData.pageTypeId = (int)task.PageTypeId;
            hasTaskOpenByUser = formUploadData.TaskOpenStatus.HasTaskOpenByUser(UserPrincipal.Current.UserName);

            // save task opened in task db
            //if (formUploadData.TaskOpenStatus != null)
            //{
            //    formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
            //    task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus,
            //        typeof(TaskOpenStatusModel), Encoding.Unicode);
            //    _taskManager.UpdateTask(task);
            //}

            //Logic for reassign of parent task after a child request is created //Know y and how
            if (_parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
            {
                var parenttask = _parentChildTaskManager.GetParentTask(task.TaskInstanceId);

                //if (parenttask != null)
                //{
                //    var newreassignedbyAe = string.Empty;

                //    newreassignedbyAe = taskReAssignmentManager.GetReassignedAE(parenttask.ParentTaskInstanceId);
                //    if (newreassignedbyAe != string.Empty)
                //        formUploadData.AssignedBy = newreassignedbyAe;
                //}
            }
            //Flag to identify Pam Report
            if (Request.QueryString.Count > 1)
            {
                if (Request.QueryString["IsFromPam"] == "LenderPam")
                {
                    formUploadData.IsLenderPAMReport = true;
                }
                else
                {
                    formUploadData.IsPAMReport = true;
                }
            }
            formUploadData.IsProdQueuInprocess = true;

            //formUploadData.ServicerSubmissionDate =
            //  TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
            formUploadData.ServicerSubmissionDate =
            (DateTime)formUploadData.ServicerSubmissionDate;
            TempData["ApplicationRequestFormData"] = formUploadData;
            TempData["hasTaskOpenByUser"] = hasTaskOpenByUser;

            return RedirectToAction("GetApplicationFormDetail", "ProductionApplication", new { model = formUploadData });


        }

    }
}
