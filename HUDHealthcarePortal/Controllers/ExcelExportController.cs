﻿using System.Globalization;
using System.Web.Routing;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using Core.Utilities;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Model;
using SpreadsheetGear;
using BusinessService.ProjectDetailReport;
using System.Configuration;
using BusinessService;
using Model.Production;
using System.Text;
using Newtonsoft.Json;
using BusinessService.Production;
using BusinessService.Interfaces.Production;
using System.IO;

namespace HUDHealthcarePortal.Controllers
{
    [Authorize]
    public class ExcelExportController : Controller
    {
        IUploadDataManager uploadDataMgr;
        private IAccountManager accountMgr;
        private IMissingProjectReportManager missingProjectReportMgr;
        private IPortfolioManager portfolioMgr;
        private IProjectReportsManager projectReportsMngr;
        private IPAMReportManager pamReportMgr;
        private ILenderPAMReportManager lenderPamReportMgr;
        private IProjectDetailReportManager _projectDetailReportManager;
        private IPDRUserPreferenceManager _pdrUserPreferenceManager;
        public string Querydate = "";

        //Production
        private IProductionPAMReportManager _plmProdReportMgr;
        private IProd_AdhocDataManager adhocDataManager;
        private IProd_LoanCommitteeManager loanCommitteeManager;
        private IFHANumberRequestManager fhaNumberRequestManager;


        public ExcelExportController()
            : this(
                new UploadDataManager(), new AccountManager(), new MissingProjectReportManager(), new PortfolioManager(),
                new ProjectReportsManager(), new PAMReportsManager(), new LenderPAMReportManager(), new ProjectDetailReportManager(),
                new PDRUserPreferenceManager(), new ProductionPAMReportManager(), new Prod_AdhocDataManager(), new Prod_LoanCommitteeManager(),
                new FHANumberRequestManager())
        {
            Querydate = ConfigurationManager.AppSettings["Querydate"].ToString();
        }

        public ExcelExportController(IUploadDataManager uploadDataManager, IAccountManager accountManager,
            IMissingProjectReportManager missingProjectReportManager, IPortfolioManager portfolioManager,
            IProjectReportsManager projectReportsManager, IPAMReportManager pamReportManager,
            ILenderPAMReportManager lenderPamReportManager, IProjectDetailReportManager projectDetailReportManager,
            IPDRUserPreferenceManager pdrUserPreferenceManager, IProductionPAMReportManager PAMProdReportManager,
            IProd_AdhocDataManager _adhocDataManager, IProd_LoanCommitteeManager _loanCommitteeManager,
            IFHANumberRequestManager _fhaNumberRequestManager)
        {
            uploadDataMgr = uploadDataManager;
            accountMgr = accountManager;
            missingProjectReportMgr = missingProjectReportManager;
            portfolioMgr = portfolioManager;
            projectReportsMngr = projectReportsManager;
            lenderPamReportMgr = lenderPamReportManager;
            pamReportMgr = pamReportManager;
            _projectDetailReportManager = projectDetailReportManager;
            _pdrUserPreferenceManager = pdrUserPreferenceManager;
            Querydate = ConfigurationManager.AppSettings["Querydate"].ToString();
            _plmProdReportMgr = PAMProdReportManager;
            adhocDataManager = _adhocDataManager;
            loanCommitteeManager = _loanCommitteeManager;
            fhaNumberRequestManager = _fhaNumberRequestManager;
        }

        public ActionResult ExportViewUploadAll()
        {
            var userRoles = RoleManager.GetUserRoles(UserPrincipal.Current.UserName);
            var uploadedData = uploadDataMgr
                    .GetUploadDataBySearch(UserPrincipal.Current.UserId, userRoles).ToList();
            uploadedData = uploadedData.OrderBy(p => p.ProjectName).ToList();

            // if user is AE or WLM, filter out inaccessible fhas
            bool isUserAeOrWlm = RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName);
            // get all fha belong to the user
            List<string> fhasForAeOrWlm = null;
            if (isUserAeOrWlm)
            {
                var uploadMgr = new UploadDataManager();
                fhasForAeOrWlm = uploadMgr.GetAllowedFhasForUser(UserPrincipal.Current.UserId).ToList();
            }
            List<DerivedUploadData> uploadedDataFiltered = new List<DerivedUploadData>();
            foreach (var item in uploadedData)
            {
                if (!isUserAeOrWlm || (fhasForAeOrWlm != null && fhasForAeOrWlm.Contains(item.FHANumber)))
                    uploadedDataFiltered.Add(item);
            }

            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Project Report");

            ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "ProjectReport");

            ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(uploadedDataFiltered, cells, "ProjectReport");

            //GetResponse(workbook, "Project Report " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Project Report " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }


        public ActionResult ExportViewUploadByTime(DateTime timestamp, int userId)
        {
            var uploadedData = uploadDataMgr.GetUploadDataByTime(timestamp).Where(p => p.UserID == userId).OrderBy(p => p.ProjectName).ToList();
            uploadedData =
                uploadedData.OrderByDescending(p => Convert.ToInt32(p.MonthsInPeriod))
                    .ThenByDescending(p => Convert.ToDateTime(p.PeriodEnding))
                    .ToList();
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Uploaded Data By Time");

            ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "ProjectReport");

            ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(uploadedData, cells, "ProjectReport");

            //GetResponse(workbook, "Upload Detail by Time " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Upload Detail by Time " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }

        public ActionResult ExportViewUploadByCategory(string category, string quarter)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Project Report");

            ViewBag.ExcelExportAction = "";

            var uploadedData = !string.IsNullOrEmpty(quarter) ? uploadDataMgr.GetUploadDataByCategoryAndQuarter(category, quarter).ToList()
                : uploadDataMgr.GetUploadDataByCategory(category).ToList();
            uploadedData = uploadedData.OrderBy(p => p.ProjectName).ToList();

            ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "ProjectReport");
            ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(uploadedData, cells, "ProjectReport");
            //GetResponse(workbook, "Project Report " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Project Report " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }

        public ActionResult ExportViewUploadBySearch(string timestamp, string fhanumber, string category, string quarter)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Project Report");
            var uploadedData = new List<DerivedUploadData>();
            string reportName = "Project Report By Search";

            if (!string.IsNullOrEmpty(timestamp))
            {
                uploadedData = uploadDataMgr.GetUploadDataByTimeForSearch(Convert.ToDateTime(timestamp)).ToList();
                reportName = "Upload Detail By Search";
            }
            else if (!string.IsNullOrEmpty(category))
            {
                uploadedData = !string.IsNullOrEmpty(quarter)
                    ? uploadDataMgr.GetUploadDataByCategoryAndQuarterBySearch(category,
                        quarter).ToList()
                    : uploadDataMgr.GetUploadDataByCategoryForProjectReport(category).Entities.ToList();
            }
            else
            {
                var userRoles = RoleManager.GetUserRoles(UserPrincipal.Current.UserName);
                uploadedData = uploadDataMgr.GetUploadDataBySearch(UserPrincipal.Current.UserId, userRoles).ToList();
            }

            uploadedData = uploadedData.Where(p => p.FHANumber == fhanumber).OrderBy(p => p.ProjectName).ToList();

            ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "ProjectReport");
            ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(uploadedData, cells, "ProjectReport");
            //GetResponse(workbook, reportName + DateTime.Now.ToString(" MM-dd-yyyy HHmmss"));
            GetResponse(workbook, reportName + DateTime.UtcNow.ToString(" MM-dd-yyyy HHmmss"));

            return View();
        }

        public ActionResult ExportUploadStatusByLenderId(string strlenderId, string selectedQtr)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Upload Status");

            // if user is AE or WLM, filter out inaccessible lenders from all lenders
            bool isUserAeOrWlm = RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName);
            // get all lenders belong to the user
            List<int> lendersForAeOrWlm = null;

            if (isUserAeOrWlm)
            {
                lendersForAeOrWlm = accountMgr.GetLenderIdsByAeOrWlmForQtr(UserPrincipal.Current.UserName, selectedQtr).ToList();
            }

            int lenderId = int.Parse(strlenderId);
            //var statuses = uploadDataMgr.GetUploadStatusByLenderId(lenderId).OrderBy(p => p.LenderName).ToList();
            var statuses = uploadDataMgr.GetUploadStatusDetailByQuarter(selectedQtr, lenderId);
            var statusesFiltered = new List<UploadStatusViewModel>();

            foreach (var item in statuses)
            {
                if (!isUserAeOrWlm || (lendersForAeOrWlm != null
                    && lendersForAeOrWlm.Contains(item.LenderID)))
                    statusesFiltered.Add(item);
            }

            ExcelAndPrintHelper.GetHeaderValuesForUploadStatus(cells);
            ExcelAndPrintHelper.SetDataValuesForUploadStatus(statusesFiltered, cells);
            //GetResponse(workbook, "Upload Status " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Upload Status " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }

        public ActionResult ExportUploadStatusDetailByLenderId(string strlenderId, string selectedQtr)
        {
            /*old excel implementation
             * var dictColumnHeadersToShow = GetUploadStatusDetailColumnHeaderDict();
            var excelMgr = new ExcelManager<UploadStatusDetailModel>(dictColumnHeadersToShow);
            var byteArray = ExcelHelper.GetExcelBytes<UploadStatusDetailModel>(excelMgr, statuses);
            return File(byteArray, "application/vnd.ms-excel", "UploadStatusByLender.xls");*/

            //excel with spreadsheetgear
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Upload Status Detail");
            int lenderId = int.Parse(strlenderId);
            var statuses = uploadDataMgr.GetUploadStatusDetailByLenderId(lenderId, selectedQtr).ToList();

            ExcelAndPrintHelper.GetHeaderValuesForUploadStatusDetail(cells);
            ExcelAndPrintHelper.SetDataValuesForUploadStatusDetail(statuses, cells);
            //GetResponse(workbook, "Upload Status Detail By Lender " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Upload Status Detail By Lender " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }

        public ActionResult ExportMissingProjectDetailByLenderId(string lenderId, string quarter)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Missing Project");
            int id = int.Parse(lenderId);
            var statuses = uploadDataMgr.GetMissingProjectsForLender(id, quarter).OrderBy(p => p.ProjectName).ToList();

            ExcelAndPrintHelper.GetHeaderValuesForMissingProjectDetail(cells);
            ExcelAndPrintHelper.SetDataValuesForMissingProjectDetail(statuses, cells);
            //GetResponse(workbook, "Missing Project Detail " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Missing Project Detail " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }
        public ActionResult ExportUserUploadReport()
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "My Upload Data");
            var uploadedData = uploadDataMgr.GetDataUploadSummaryByUserId(UserPrincipal.Current.UserId).ToList();

            ExcelAndPrintHelper.GetHeaderValuesForMyUploadReport(cells);
            ExcelAndPrintHelper.SetDataValuesForMyUploadReport(uploadedData, cells);
            //GetResponse(workbook, "My Upload Report " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "My Upload Report " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }
        //Non Acction Method
        [NonAction]
        public ReportModel ApplyFilter(ReportModel reportmodel, string[] listPe, int[] listQtrint, string[] listPFNo, int[] listPIDint, string[] listFHA, bool showException)
        {


            reportmodel.ReportGridModelList.ToList().ForEach(x =>
            {
                if (x.PortfolioNumber == "0")
                {
                    x.PortfolioNumber = "";
                    //Do Something
                }

                if (x.PortfolioName == "0")
                {
                    x.PortfolioName = "";
                    //Do Something
                }
            });
            if (showException)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(m => m.IsProjectError == 1).OrderByDescending(m => m.DataInserted).ToList();
            }
            //Pf naumber filteration
            if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }
            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
            }
            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).ToList();
            }

            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            //End of PF Number


            //FHA Number Filteration
            else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            //End  of Fha Combination

            else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listFHA.Contains(s.FhaNumber)).ToList();
            }

            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            //New Condition
            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            //New Condition

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
            }

            //New Condition
            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            //New Condition

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
            }

            //New Condition

            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }
            else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }


            var ProjectCnt = reportmodel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();
            var ErrorCnt = (from od in reportmodel.ReportGridModelList where od.IsProjectError == 1 select od.FhaNumber).Distinct().Count();
            reportmodel.ReportHeaderModel.TotalProperties = ProjectCnt;
            reportmodel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;
            var TotalNoiError = (from od in reportmodel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
            var TotalDSCRError = (from od in reportmodel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
            var TotalADRError = (from od in reportmodel.ReportGridModelList where od.IsADRError == 1 select od).Count();
            var TotalOpRevError = (from od in reportmodel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
            var TotalColumErrors = reportmodel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

            reportmodel.ReportHeaderModel.TotalProperties = ProjectCnt;
            reportmodel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;
            reportmodel.ReportHeaderModel.TotalDSCRError = TotalDSCRError;
            reportmodel.ReportHeaderModel.TotalNoiError = TotalNoiError;
            reportmodel.ReportHeaderModel.TotalOpRevError = TotalOpRevError;
            reportmodel.ReportHeaderModel.TotalADRError = TotalADRError;
            reportmodel.ReportHeaderModel.TotalColError = TotalColumErrors;


            reportmodel.ReportGridModelList.OrderByDescending(a => a.DataInserted);
            //ViewBag.ProjectCnt = ProjectCnt;
            //ViewBag.ErrorCnt = ErrorCnt;
            //Session["umesh"] = ProjectCnt;
            return reportmodel;
        }
        public ActionResult ExportProjectDetailSuperUser(string userName)
        {
            string[] listPe = new string[0];

            string[] listPID = new string[0];
            string[] listFHA = new string[0];
            string[] listPFNo = new string[0];
            int[] listQtrint = new int[0];
            int[] listPIDint = new int[0];
            string[] lispfname = new string[0];
            bool showException = false;

            if (Session["listPe"] != null)
                listPe = Session["listPe"] as string[];

            if (Session["listQtrint"] != null)
                listQtrint = Session["listQtrint"] as int[];

            if (Session["listPFNo"] != null)
                listPFNo = Session["listPFNo"] as string[];

            if (Session["listPIDint"] != null)
                listPIDint = Session["listPIDint"] as int[];

            if (Session["listFHA"] != null)
                listFHA = Session["listFHA"] as string[];

            if (Session["lispfname"] != null)
                lispfname = Session["lispfname"] as string[];
            if (Session["showException"] != null)
                showException = (bool)Session["showException"];

            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectDetail SuperUser");

            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";



            var reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUser(userName, HUDRole.SuperUser.ToString(), Querydate);


            ApplyFilter(reportModel, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);


            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cells, "SuperUser", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cellsSearch, "SuperUser", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);



                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);




                reportModel.ReportGridModelList.Insert(0, ErrorModel);

                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "SuperUser");

            }
            //Session["listPe"] = null;
            //Session["listQtrint"] = null;
            //Session["listPFNo"] = null;



            //workbook.Worksheets[0].Select();          
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "Project Detailt For SuperUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Project Detailt For SuperUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportProjectErrorSuperUser(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var role = UserPrincipal.Current.UserRole;
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectError SuperUser");
            var reportModel = _projectDetailReportManager.GetErrorReportForSuperUser(userName, HUDRole.SuperUser.ToString(), Querydate);
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderErrorDetail(reportModel, cells, "SuperUser", role);
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderErrorDetail(reportModel, cellsSearch, "SuperUser", role);

                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);



                reportModel.ReportGridModelList.ToList().ForEach(x =>
                {
                    if (x.PortfolioNumber == "0")
                    {
                        x.PortfolioNumber = "";
                    }

                    if (x.PortfolioName == "0")
                    {
                        x.PortfolioName = "";
                    }
                });

                reportModel.ReportGridModelList.Insert(0, ErrorModel);
                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "SuperUser");

            }
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "ProjectError For SuperUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectError For SuperUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }


        public ActionResult ExportProjectErrorWLM(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var role = HUDRole.WorkflowManager.ToString();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectError WLM");
            var reportModel = _projectDetailReportManager.GetErrorReportForWorkloadManager(userName, HUDRole.WorkflowManager.ToString(), Querydate);
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";
            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderErrorDetail(reportModel, cells, "WorkflowManager", role);
                var cellsSearch = workbook.Worksheets[1].Cells;

                ExcelAndPrintHelper.GetHeaderErrorDetail(reportModel, cellsSearch, "WorkflowManager", role);


                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);



                reportModel.ReportGridModelList.ToList().ForEach(x =>
                {
                    if (x.PortfolioNumber == "0")
                    {
                        x.PortfolioNumber = "";
                    }

                    if (x.PortfolioName == "0")
                    {
                        x.PortfolioName = "";
                    }
                });

                reportModel.ReportGridModelList.Insert(0, ErrorModel);
                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "WorkflowManager");

            }
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "ProjectError For WLM " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectError For WLM " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportProjectErrorAE(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var role = HUDRole.AccountExecutive.ToString();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectError WLM");
            var reportModel = _projectDetailReportManager.GetErrorReportForAccountExecutive(userName, HUDRole.AccountExecutive.ToString(), Querydate);
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";
            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderErrorDetail(reportModel, cells, "AccountExecutive", role);
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderErrorDetail(reportModel, cellsSearch, "AccountExecutive", role);
                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);



                reportModel.ReportGridModelList.ToList().ForEach(x =>
                {
                    if (x.PortfolioNumber == "0")
                    {
                        x.PortfolioNumber = "";
                    }

                    if (x.PortfolioName == "0")
                    {
                        x.PortfolioName = "";
                    }
                });

                reportModel.ReportGridModelList.Insert(0, ErrorModel);
                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "AccountExecutive");

            }
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "ProjectError For WLM " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectError For WLM " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }


        public ActionResult ExportProjectErrorInternalSpecialOptionUser(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var role = HUDRole.InternalSpecialOptionUser.ToString();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectError SpecialOptionUser");
            var reportModel = _projectDetailReportManager.GetErrorReportForInternalSpecialOptionUser(userName, HUDRole.InternalSpecialOptionUser.ToString(), Querydate);
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";
            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderErrorDetail(reportModel, cells, "InternalSpecialOptionUser", role);
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderErrorDetail(reportModel, cellsSearch, "InternalSpecialOptionUser", role);

                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);



                reportModel.ReportGridModelList.ToList().ForEach(x =>
                {
                    if (x.PortfolioNumber == "0")
                    {
                        x.PortfolioNumber = "";
                    }

                    if (x.PortfolioName == "0")
                    {
                        x.PortfolioName = "";
                    }
                });

                reportModel.ReportGridModelList.Insert(0, ErrorModel);
                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "InternalSpecialOptionUser");

            }
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "ProjectError For InternalSpecialOptionUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectError For InternalSpecialOptionUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }


        public ActionResult ExportProjectAECurQrt(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectCurQrt AE");
            var reportModel = _projectDetailReportManager.GetCurrentQuarterReportForAccountExecutive(userName, HUDRole.AccountExecutive.ToString(), Querydate);

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderCurQrtr(cells, "AccountExecutive");
                ExcelAndPrintHelper.SetDataValuesForCurQrtrAEReport(reportModel, cells, "AccountExecutive");

            }
            //GetResponse(workbook, "ProjectCurQrt For AE " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectCurQrt For AE " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }


        public ActionResult ExportProjectInternalSpecialOptionUserCurQrt(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectCurQrt ISU");
            var reportModel = _projectDetailReportManager.GetCurrentQuarterReportForInternalSpecialOptionUser(userName, HUDRole.InternalSpecialOptionUser.ToString(), Querydate);

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderCurQrtr(cells, "InternalSpecialOptionUser");
                ExcelAndPrintHelper.SetDataValuesForCurQrtrAEReport(reportModel, cells, "InternalSpecialOptionUser");

            }
            //GetResponse(workbook, "ProjectCurQrt For InternalSpecialOptionUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectCurQrt For InternalSpecialOptionUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportProjectWLMCurQrt(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectCurQrt WLM");
            var reportModel = _projectDetailReportManager.GetCurrentQuarterReportForAccountExecutive(userName, HUDRole.WorkflowManager.ToString(), Querydate);

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderCurQrtr(cells, "WorkflowManager");
                ExcelAndPrintHelper.SetDataValuesForCurQrtrAEReport(reportModel, cells, "WorkflowManager");

            }
            //GetResponse(workbook, "ProjectCurQrt For WLM " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectCurQrt For WLM " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportProjectSuperUserCurQrt(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectCurQrt SuperUser");
            var reportModel = _projectDetailReportManager.GetCurrentQuarterReportForAccountExecutive(userName, HUDRole.SuperUser.ToString(), Querydate);

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderCurQrtr(cells, "SuperUser");
                ExcelAndPrintHelper.SetDataValuesForCurQrtrAEReport(reportModel, cells, "SuperUser");

            }
            //GetResponse(workbook, "ProjectCurQrt For SuperUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectCurQrt For SuperUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportPTReportSuperUser(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "PT Report SuperUser");
            var reportModel = _projectDetailReportManager.GetPTReportForSuperUser(userName, HUDRole.SuperUser.ToString(), Querydate);

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderPTDetail(cells, "SuperUser");
                ExcelAndPrintHelper.SetDataValuesForPTDetail(reportModel, cells, "SuperUser");

            }
            //GetResponse(workbook, "PT Report For SuperUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "PT Report For SuperUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportPTReportWLM(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "PT Report WLM");
            var reportModel = _projectDetailReportManager.GetPTReportForWorkloadManager(userName, HUDRole.WorkflowManager.ToString(), Querydate);

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderPTDetail(cells, "WorkflowManager");
                ExcelAndPrintHelper.SetDataValuesForPTDetail(reportModel, cells, "WorkflowManager");

            }
            //GetResponse(workbook, "PT Report For WorkflowManager " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "PT Report For WorkflowManager " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportPTReportAE(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "PT Report AE");
            var reportModel = _projectDetailReportManager.GetPTReportForAccountExecutive(userName, HUDRole.AccountExecutive.ToString(), Querydate);

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderPTDetail(cells, "AccountExecutive");
                ExcelAndPrintHelper.SetDataValuesForPTDetail(reportModel, cells, "AccountExecutive");

            }
            //GetResponse(workbook, "PT Report For AccountExecutive " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "PT Report For AccountExecutive " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportPTReportInternalSpecialOptionUser(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "PT Report ISU");
            var reportModel = _projectDetailReportManager.GetPTReportForInternalSpecialOptionUser(userName, HUDRole.InternalSpecialOptionUser.ToString(), Querydate);

            if (reportModel != null)
            {

                ExcelAndPrintHelper.GetHeaderPTDetail(cells, "InternalSpecialOptionUser");
                ExcelAndPrintHelper.SetDataValuesForPTDetail(reportModel, cells, "InternalSpecialOptionUser");

            }
            //GetResponse(workbook, "PT Report For InternalSpecialOptionUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "PT Report For InternalSpecialOptionUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportProjectDetailWLM(string userName)
        {
            string[] listPe = new string[0];

            string[] listPID = new string[0];
            string[] listFHA = new string[0];
            string[] listPFNo = new string[0];
            int[] listQtrint = new int[0];
            int[] listPIDint = new int[0];
            string[] lispfname = new string[0];
            bool showException = false;

            if (Session["listPe"] != null)
                listPe = Session["listPe"] as string[];

            if (Session["listQtrint"] != null)
                listQtrint = Session["listQtrint"] as int[];

            if (Session["listPFNo"] != null)
                listPFNo = Session["listPFNo"] as string[];

            if (Session["listPIDint"] != null)
                listPIDint = Session["listPIDint"] as int[];

            if (Session["listFHA"] != null)
                listFHA = Session["listFHA"] as string[];

            if (Session["lispfname"] != null)
                lispfname = Session["lispfname"] as string[];
            if (Session["showException"] != null)
                showException = (bool)Session["showException"];

            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectDetail WLM");
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForWorkloadManager(userName, HUDRole.WorkflowManager.ToString(), Querydate);
            if (listPe != null && listQtrint != null)
            {
                ApplyFilter(reportModel, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);
            }
            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cells, "WorkflowManager", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cellsSearch, "WorkflowManager", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);


                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);
                reportModel.ReportGridModelList.Insert(0, ErrorModel);

                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "WorkflowManager");
            }

            //Session["listPe"] = null;
            //Session["listQtrint"] = null;
            //Session["listPFNo"] = null;
            //workbook.Worksheets[0].Select();
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "Project Detailt For WLM " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Project Detailt For WLM " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportProjectDetailAE(string userName)
        {

            string[] listPe = new string[0];

            string[] listPID = new string[0];
            string[] listFHA = new string[0];
            string[] listPFNo = new string[0];
            int[] listQtrint = new int[0];
            int[] listPIDint = new int[0];
            string[] lispfname = new string[0];
            bool showException = false;
            if (Session["listPe"] != null)
                listPe = Session["listPe"] as string[];

            if (Session["listQtrint"] != null)
                listQtrint = Session["listQtrint"] as int[];

            if (Session["listPFNo"] != null)
                listPFNo = Session["listPFNo"] as string[];

            if (Session["listPIDint"] != null)
                listPIDint = Session["listPIDint"] as int[];

            if (Session["listFHA"] != null)
                listFHA = Session["listFHA"] as string[];

            if (Session["lispfname"] != null)
                lispfname = Session["lispfname"] as string[];
            if (Session["showException"] != null)
                showException = (bool)Session["showException"];
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectDetail AE");
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForAccountExecutive(userName, HUDRole.AccountExecutive.ToString(), Querydate);
            if (listPe != null && listQtrint != null)
            {
                ApplyFilter(reportModel, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);
            }
            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cells, "AccountExecutive", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cellsSearch, "AccountExecutive", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);
                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);


                reportModel.ReportGridModelList.Insert(0, ErrorModel);
                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "AccountExecutive");
            }

            //Session["listPe"] = null;
            //Session["listQtrint"] = null;
            //Session["listPFNo"] = null;
            // workbook.Worksheets[0].Select();
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "Project Detail For WLM " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Project Detail For WLM " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();

        }

        public ActionResult ExportProjectDetailInternalSpecialOptionUser(string userName)
        {

            string[] listPe = new string[0];

            string[] listPID = new string[0];
            string[] listFHA = new string[0];
            string[] listPFNo = new string[0];
            int[] listQtrint = new int[0];
            int[] listPIDint = new int[0];
            string[] lispfname = new string[0];
            bool showException = false;

            if (Session["listPe"] != null)
                listPe = Session["listPe"] as string[];

            if (Session["listQtrint"] != null)
                listQtrint = Session["listQtrint"] as int[];

            if (Session["listPFNo"] != null)
                listPFNo = Session["listPFNo"] as string[];

            if (Session["listPIDint"] != null)
                listPIDint = Session["listPIDint"] as int[];

            if (Session["listFHA"] != null)
                listFHA = Session["listFHA"] as string[];

            if (Session["lispfname"] != null)
                lispfname = Session["lispfname"] as string[];
            if (Session["showException"] != null)
                showException = (bool)Session["showException"];
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectDetail ISU");
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForInternalSpecialOptionUser(userName, HUDRole.InternalSpecialOptionUser.ToString(), Querydate);
            if (listPe != null && listQtrint != null)
            {
                ApplyFilter(reportModel, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);
            }
            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cells, "InternalSpecialOptionUser", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cellsSearch, "InternalSpecialOptionUser", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);
                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);


                reportModel.ReportGridModelList.Insert(0, ErrorModel);
                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "InternalSpecialOptionUser");
            }

            //Session["listPe"] = null;
            //Session["listQtrint"] = null;
            //Session["listPFNo"] = null;
            // workbook.Worksheets[0].Select();
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "Project Detail For InternalSpecialOptionUser  " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Project Detail For InternalSpecialOptionUser  " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();

        }

        public ActionResult ExportProjectDetailLender(string userName)
        {
            string[] listPe = new string[0];

            string[] listPID = new string[0];
            string[] listFHA = new string[0];
            string[] listPFNo = new string[0];
            int[] listQtrint = new int[0];
            int[] listPIDint = new int[0];
            string[] lispfname = new string[0];
            bool showException = false;
            if (Session["listPe"] != null)
                listPe = Session["listPe"] as string[];

            if (Session["listQtrint"] != null)
                listQtrint = Session["listQtrint"] as int[];

            if (Session["listPFNo"] != null)
                listPFNo = Session["listPFNo"] as string[];

            if (Session["listPIDint"] != null)
                listPIDint = Session["listPIDint"] as int[];

            if (Session["listFHA"] != null)
                listFHA = Session["listFHA"] as string[];

            if (Session["lispfname"] != null)
                lispfname = Session["lispfname"] as string[];
            if (Session["showException"] != null)
                showException = (bool)Session["showException"];
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectDetail Lender");
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForAccountExecutive(userName, HUDRole.LenderAccountRepresentative.ToString(), Querydate);
            if (listPe != null && listQtrint != null)
            {
                ApplyFilter(reportModel, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);
            }
            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cells, "LenderAccountRepresentative", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderProjectDetail(reportModel.ReportHeaderModel, cellsSearch, "LenderAccountRepresentative", listPe, listQtrint, listPFNo, lispfname, listPIDint, listFHA);
                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var ErrorModel = new ReportGridModel();
                ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                ErrorModel.ADRperQuarter = TotalADRError.ToString();
                ErrorModel.QuaterlyOperatingRevenue = TotalOpRevError.ToString();
                ErrorModel.RowWiseError = Convert.ToInt32(TotalColumErrors);

                reportModel.ReportGridModelList.Insert(0, ErrorModel);
                ExcelAndPrintHelper.SetDataValuesForProjectDetail(reportModel, cells, "LenderAccountRepresentative");
            }

            // workbook.Worksheets[0].Select();
            var userSelection = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            ExcelAndPrintHelper.HideColumns(workbook, userSelection);
            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "Project Detailt For Lender " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Project Detailt For Lender " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            //Session["listPe"] = null;
            //Session["listQtrint"] = null;
            //Session["listPFNo"] = null;
            return View();

        }

        public ActionResult ExportProjectDetailReportForWorkloadManager(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjectDetail WFM");
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUser(userName, HUDRole.SuperUser.ToString());

            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "WFM");
                ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "WFM");
            }
            //GetResponse(workbook, "Project Detailt For SuperUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Project Detailt For SuperUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportProjectDetailSuperUserHighLevel(string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "ProjHighLevel SuperUser");

            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Header Details";


            var reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUserHighLevel(userName, HUDRole.SuperUser.ToString(), "ASC", Querydate);

            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderProjectDetailSuperUserHighLevel(reportModel.ReportHeaderModel, cells, "SuperUser");
                var cellsSearch = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.GetHeaderProjectDetailSuperUserHighLevel(reportModel.ReportHeaderModel, cellsSearch, "SuperUser");
                ExcelAndPrintHelper.SetDataValuesForProjectDetailSuperUserHighLevel(reportModel, cells, "SuperUser");
            }

            workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);
            //GetResponse(workbook, "ProjectDetailHighLevel For SuperUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "ProjectDetailHighLevel For SuperUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }

        public ActionResult ExportMissingProjectSuperUser(int year)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Missing Project For SuperUser");
            var reportModel = missingProjectReportMgr.GetMissingProjectReport(HUDRole.SuperUser, UserPrincipal.Current.UserName, year);

            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "SuperUser");
                ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "SuperUser");
            }
            //GetResponse(workbook, "Missing Project For SuperUser " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Missing Project For SuperUser " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();
        }

        public ActionResult ExportMissingProjectSuperUserWLM(int year)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Missing Project For WLM");
            var reportModel = missingProjectReportMgr.GetSuperUserMissingProjectReportWithWLM(UserPrincipal.Current.UserName, year);

            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "SuperUserWLM");
                ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "SuperUserWLM");
            }
            //GetResponse(workbook, "Missing Project For WLM " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Missing Project For WLM " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();
        }

        public ActionResult ExportMissingProjectWLM(int year, string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Missing Project For WLM");
            var reportModel = missingProjectReportMgr.GetWorkloadManagerMissingProjectReport(userName, year);

            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "WLM");
                ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "WLM");
            }

            //GetResponse(workbook, "Missing Project For WLM " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Missing Project For WLM " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();
        }

        public ActionResult ExportMissingProjectAE(int year, string userName)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Missing Project For AE");
            var reportModel = missingProjectReportMgr.GetAccountExecutiveMissingProjectReport(userName, year);

            if (reportModel != null)
            {
                ExcelAndPrintHelper.GetHeaderValuesForMissingProjectReport(cells, "AE");
                ExcelAndPrintHelper.SetDataValuesForMissingProjectReport(reportModel, cells, "AE");
            }

            //GetResponse(workbook, "Missing Project For AE " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Missing Project For AE " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();
        }

        public ActionResult ExportPortfolioSummary(int? page, string sort, string sortDir, string quarter)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Portfolio Summary");
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "page", page }, { "sort", sort }, { "sortdir", sortDir }, { "quarterToken", quarter } };

            routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_PORTFOLIO_SUMMARY_LINK_DICT];
            page = (int?)routeValuesDict["page"];
            sort = (string)routeValuesDict["sort"];
            sortDir = (string)routeValuesDict["sortdir"];
            quarter = (string)routeValuesDict["quarterToken"];

            var model = new PortfolioSummaryModel { YearQrtModel = new YearQuarterModel(4, quarter) };
            string yearQrtToken = string.IsNullOrEmpty(quarter)
                ? model.YearQrtModel.SelectedQrtToken
                : quarter;
            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            const int pageSize = 10;
            var portfolios = portfolioMgr.GetPortfolios(page ?? 1, pageSize, string.IsNullOrEmpty(sort) ? "Portfolio_Name" : sort,
                 sortOrder, yearQrtToken);

            if (portfolios != null)
            {
                ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "PortfolioSummary");
                ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(portfolios.Entities.ToList(), cells, "PortfolioSummary");
            }

            //GetResponse(workbook, "Portfolio Summary " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Portfolio Summary " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();
        }

        public ActionResult ExportPortfolioDetail(int? page, string sort, string sortDir, string portfolioNum, string quarter)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Portfolio Detail");
            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            const int pageSize = 10;
            var portfolioDetail = portfolioMgr.GetPortfolioDetail(page ?? 1, pageSize, string.IsNullOrEmpty(sort) ? "ProjectName" : sort, sortOrder, portfolioNum, quarter);

            if (portfolioDetail != null)
            {
                ExcelAndPrintHelper.GetHeaderValueForProjectReport(cells, "PortfolioDetail");
                ExcelAndPrintHelper.SetDataValuestoExcelForProjectReport(portfolioDetail.Entities.ToList(), cells, "PortfolioDetail");
            }

            //GetResponse(workbook, "Portfolio Detail " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Portfolio Detail " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();
        }

        public ActionResult ExportProjectReportSuperUser(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetSuperUserProjectSummary(userName, minUploadDate, maxUploadDate);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();

            GetProjectSummarySpreadSheet(reportModel, null, "Workload Manager", ReportType.ProjectReport);

            return View();
        }

        public ActionResult ExportProjectReportWLM(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_PROJECTS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetWLMProjectSummary(userName, wlmId, minUploadDate, maxUploadDate);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();

            GetProjectSummarySpreadSheet(reportModel, null, "Account Executive", ReportType.ProjectReport);

            return View();
        }

        public ActionResult ExportProjectReportAE(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_PROJECTS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];

            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetAEProjectSummary(userName, wlmId, aeId, minUploadDate, maxUploadDate);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();

            GetProjectSummarySpreadSheet(reportModel, null, "Lender", ReportType.ProjectReport);

            return View();
        }

        public ActionResult ExportProjectReportDetail(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_PROJECTS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];
            var lenderId = (string)routeValuesDict["lenderId"];
            var reportFilter = (string)routeValuesDict["reportFilter"];

            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel projectReportDetail = projectReportsMngr.GetProjectLenderDetail(userName, wlmId, aeId, lenderId, minUploadDate, maxUploadDate);
            projectReportDetail.MinUploadDate = minUploadDate.ToShortDateString();
            projectReportDetail.MaxUploadDate = maxUploadDate.ToShortDateString();

            GetProjectDetailSpreadSheet(projectReportDetail, null, ReportType.ProjectReport);

            return View();
        }

        public ActionResult ExportLargeLoanReportSuperUser(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetSuperUserLargeLoanSummary(userName, minUploadDate, maxUploadDate);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();

            GetProjectSummarySpreadSheet(reportModel, null, "Workload Manager", ReportType.LargeLoanReport);

            return View();
        }


        public ActionResult ExportLargeLoanReportWLM(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];

            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetWLMLargeLoanSummary(userName, wlmId, minUploadDate, maxUploadDate);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();

            GetProjectSummarySpreadSheet(reportModel, null, "Account Executive", ReportType.LargeLoanReport);

            return View();
        }

        public ActionResult ExportLargeLoanReportAE(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];

            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetAELargeLoanSummary(userName, wlmId, aeId, minUploadDate, maxUploadDate);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();

            GetProjectSummarySpreadSheet(reportModel, null, "Lender", ReportType.LargeLoanReport);

            return View();
        }

        public ActionResult ExportLargeLoanReportDetail(DateTime minUploadDate, DateTime maxUploadDate)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];
            var lenderId = (string)routeValuesDict["lenderId"];
            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel projectReportDetail = projectReportsMngr.GetLargeLoanLenderDetail(userName, wlmId, aeId, lenderId, minUploadDate, maxUploadDate);
            projectReportDetail.MinUploadDate = minUploadDate.ToShortDateString();
            projectReportDetail.MaxUploadDate = maxUploadDate.ToShortDateString();

            GetProjectDetailSpreadSheet(projectReportDetail, null, ReportType.LargeLoanReport);

            return View();

        }

        public ActionResult ExportRatioExceptionsReportSuperUser(DateTime minUploadDate, DateTime maxUploadDate,
            bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable,
            bool isAvgPaymentPeriod)
        {
            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetSuperUserRatioExceptionSummary(userName, minUploadDate, maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();
            List<String> selectedRatioExceptions = ExcelAndPrintHelper.GetSelectedRatioExceptions(isDebtCoverageRatio, isWorkingCapital,
                isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            GetProjectSummarySpreadSheet(reportModel, selectedRatioExceptions, "Workload Manager", ReportType.RatioExceptionsReport);

            return View();
        }

        public ActionResult ExportRatioExceptionsReportWLM(DateTime minUploadDate, DateTime maxUploadDate,
            bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable,
            bool isAvgPaymentPeriod)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];

            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetWLMRatioExceptionSummary(userName, wlmId, minUploadDate, maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();
            List<String> selectedRatioExceptions = ExcelAndPrintHelper.GetSelectedRatioExceptions(isDebtCoverageRatio, isWorkingCapital,
                isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            GetProjectSummarySpreadSheet(reportModel, selectedRatioExceptions, "Account Executive", ReportType.RatioExceptionsReport);

            return View();
        }

        public ActionResult ExportRatioExceptionsReportAE(DateTime minUploadDate, DateTime maxUploadDate,
            bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable,
            bool isAvgPaymentPeriod)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];

            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel reportModel = projectReportsMngr.GetAERatioExceptionSummary(userName, wlmId, aeId, minUploadDate, maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
            reportModel.MinUploadDate = minUploadDate.ToShortDateString();
            reportModel.MaxUploadDate = maxUploadDate.ToShortDateString();
            List<String> selectedRatioExceptions = ExcelAndPrintHelper.GetSelectedRatioExceptions(isDebtCoverageRatio, isWorkingCapital,
                isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            GetProjectSummarySpreadSheet(reportModel, selectedRatioExceptions, "Lender", ReportType.RatioExceptionsReport);

            return View();
        }

        public ActionResult ExportRatioExceptionsReportDetail(DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            var routeValuesDict = (RouteValueDictionary)Session[SessionHelper.SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT];
            var lenderId = (string)routeValuesDict["lenderId"];
            var wlmId = (string)routeValuesDict["wlmId"];
            var aeId = (string)routeValuesDict["aeId"];
            var userName = UserPrincipal.Current.UserName;

            ProjectReportModel projectReportDetail = projectReportsMngr.GetRatioExceptionLenderDetail(userName, wlmId, aeId, lenderId, minUploadDate, maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
            projectReportDetail.MinUploadDate = minUploadDate.ToShortDateString();
            projectReportDetail.MaxUploadDate = maxUploadDate.ToShortDateString();
            List<String> selectedRatioExceptions = ExcelAndPrintHelper.GetSelectedRatioExceptions(isDebtCoverageRatio, isWorkingCapital,
                isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            GetProjectDetailSpreadSheet(projectReportDetail, selectedRatioExceptions, ReportType.RatioExceptionsReport);

            return View();
        }

        /// <summary>
        /// Export to Excel on from user management report
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public ActionResult ExportUsersReport(string searchText, int? page, string sort, string sortdir)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Users Report");
            ExcelAndPrintHelper.GetHeaderValuesForUsersReport(cells);
            string sortString = string.IsNullOrEmpty(sort) ? "UserName" : sort;
            List<UserViewModel> results = accountMgr.GetDataAdminManageUser(searchText, 0, sortString, string.IsNullOrEmpty(sortdir)
                    ? EnumUtils.Parse<SqlOrderByDirecton>("ASC")
                    : EnumUtils.Parse<SqlOrderByDirecton>(sortdir)).ToList();

            ExcelAndPrintHelper.SetDataValuesForUsersReport(results, cells);
            //GetResponse(workbook, "Users Report " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Users Report " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();
        }

        public ActionResult ExportPAMReport(string aeIds, string isouIds, DateTime? fromDate, DateTime? toDate, string status, string projectAction, int? page, string sort, string sortdir, string wlmIds , int? hasOpened, int? daysOpened)
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "PAM Report");

            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Search Criteria";

            int wlmId = 0, aeId = 0, isouId = 0;
            if (RoleManager.IsWorkloadManagerRole(UserPrincipal.Current.UserName))
            {
                wlmId = pamReportMgr.GetWlmId(UserPrincipal.Current.UserId);
            }
            else if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
            {
                aeId = pamReportMgr.GetAeId(UserPrincipal.Current.UserId);
                aeIds = aeId.ToString(CultureInfo.InvariantCulture);
            }

            else if (RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName))
            {
                //isouId = pamReportMgr.GetIsouId(UserPrincipal.Current.UserId);
                isouId = UserPrincipal.Current.UserId;
                isouIds = isouId.ToString(CultureInfo.InvariantCulture);
            }

            int? requestStatus = string.IsNullOrWhiteSpace(status) || status == "1,0" || status == "0,1" ? (int?)null : int.Parse(status);
            // int? requestProjectAction = string.IsNullOrWhiteSpace(projectAction) ? (int?)null : projectAction.Length > 3 ? (int?)null : int.Parse(projectAction == "0,1" || projectAction == "1,0" ? "10" : projectAction == "0,2" || projectAction == "2,0" ? "20" : projectAction == "1,2" || projectAction == "2,1" ? "12" : projectAction);
            string requestProjectAction = projectAction;

            var sortOrder = string.IsNullOrEmpty(sortdir)
                ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                : EnumUtils.Parse<SqlOrderByDirecton>(sortdir);
            string sortString = string.IsNullOrEmpty(sort) ? "IsProjectActionOpen,ProjectActionSubmitDate" : sort;

            //var viewModel = pamReportMgr.GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds, fromDate, toDate, wlmId,
            //	aeId, isouId, requestStatus, requestProjectAction, 0, sort, sortOrder, wlmIds);
            var viewModel = pamReportMgr.GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds, fromDate, toDate, wlmId,
                aeId, isouId, requestStatus, requestProjectAction, 0, sort, sortOrder, wlmIds
                , "", "", "", hasOpened, daysOpened);//karri:D#610

            //get the search criteria details, ae names, wlm names
            pamReportMgr.GetSearchCriteriaDetails(ref viewModel, aeIds, isouIds, wlmIds, requestStatus, requestProjectAction, wlmId, isouId, aeId, fromDate, toDate);

            ExcelAndPrintHelper.GetHeaderValuesForPAMReport(cells);
            ExcelAndPrintHelper.SetDataValuesForPAMReport(viewModel.PAMReportGridlList, cells);

            var cellsSearch = workbook.Worksheets[1].Cells;
            //write the header and values of the search criteria details
            ExcelAndPrintHelper.SetPAMSearchCriteriaDetails(viewModel, cellsSearch);
            workbook.Worksheets[1].PageSetup.Orientation = PageOrientation.Landscape;
            workbook.Worksheets[0].PageSetup.Orientation = PageOrientation.Landscape;

            //to set the default sheet to be active : need to check more documentation, as of now ActiveSheet, ActiveWorkSheet readonly methods
            // workbook.Worksheets[0].MoveBefore(workbook.Worksheets[1]).Select();
            workbook.Worksheets[0].Select();
            // workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);

            //GetResponse(workbook, "PAM Report " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "PAM Report " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }


        //User story 1904            

        public ActionResult ExportLenderPAMReport(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string projectAction, int? page, string sort, string sortdir)
        {
            //Get Lender id and Role name
            int lenderId = UserPrincipal.Current.LenderId ?? default(int);
            var roleName = UserPrincipal.Current.UserRole;

            //Set workbook and add work sheet
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Lender PAM Report");
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Search Criteria";

            //   Gets status , project action and page properties
            int? requestStatus = string.IsNullOrWhiteSpace(status) || status == "1,0" || status == "0,1" ? (int?)null : int.Parse(status);
            string requestProjectAction = projectAction;
            var sortOrder = string.IsNullOrEmpty(sortdir)
                ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                : EnumUtils.Parse<SqlOrderByDirecton>(sortdir);
            string sortString = string.IsNullOrEmpty(sort) ? "IsProjectActionOpen,ProjectActionSubmitDate" : sort;

            // Gets view model data for search event 
            var viewModel = lenderPamReportMgr.GetLenderPAMSummary(lamIds, bamIds, larIds, roleIds, fromDate, toDate,
                 requestStatus, requestProjectAction, 0, sort, sortOrder);

            // Gets data for search criteria tab in excel export
            lenderPamReportMgr.GetSearchCriteriaDetails(ref viewModel, lamIds, bamIds, larIds, roleIds, requestStatus, requestProjectAction, fromDate, toDate, roleName, lenderId);

            viewModel.LenderName = lenderPamReportMgr.GetLenderName((int)UserPrincipal.Current.LenderId);
            //Gets and sets data for Report tab
            ExcelAndPrintHelper.SetHeaderValuesForLenderPAMReport(cells);
            ExcelAndPrintHelper.SetDataValuesForLenderPAMReport(viewModel.PAMReportGridlList, cells);

            var cellsSearch = workbook.Worksheets[1].Cells;
            // write the header and values of the search criteria details
            ExcelAndPrintHelper.SetLenderPAMSearchCriteriaLAMBAM(viewModel, cellsSearch);
            workbook.Worksheets[1].PageSetup.Orientation = PageOrientation.Landscape;
            workbook.Worksheets[0].PageSetup.Orientation = PageOrientation.Landscape;

            //to set the default sheet to be active : need to check more documentation, as of now ActiveSheet, ActiveWorkSheet readonly methods
            // workbook.Worksheets[0].MoveBefore(workbook.Worksheets[1]).Select();
            workbook.Worksheets[0].Select();
            // workbook.Worksheets[1].MoveBefore(workbook.Worksheets[0]);

            //GetResponse(workbook, "Lender PAM Report " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Lender PAM Report " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }

        public void GetResponse(IWorkbook workbook, string worksheetName)
        {
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + worksheetName + ".xlsx");
            workbook.SaveToStream(Response.OutputStream, FileFormat.OpenXMLWorkbook);
            Response.End();
        }

        public void GetProjectDetailSpreadSheet(ProjectReportModel projectReportDetail, List<String> selectedRatioExceptions, ReportType reportType)
        {
            var reportName = "Workload Mgmt Report Detail";
            var worksheetName = "WLM Detail";

            switch (reportType)
            {
                case ReportType.RatioExceptionsReport:
                    reportName = "Ratio Exceptions Report Detail";
                    worksheetName = "Ratio Exceptions Detail";
                    break;
                case ReportType.LargeLoanReport:
                    reportName = "Large Loan Report Detail";
                    worksheetName = "Large Loan Detail";
                    break;
            }

            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, worksheetName);
            int headerRows = ExcelAndPrintHelper.GetHeaderValuesForProjectDetail(projectReportDetail, selectedRatioExceptions, cells);
            ExcelAndPrintHelper.SetDataValuestoExcelForProjectDetailReport(projectReportDetail, headerRows, cells);

            //GetResponse(workbook, reportName + DateTime.Now.ToString(" MM-dd-yyyy HHmmss"));
            GetResponse(workbook, reportName + DateTime.UtcNow.ToString(" MM-dd-yyyy HHmmss"));
        }

        public void GetProjectSummarySpreadSheet(ProjectReportModel projectReportSummary, List<String> selectedRatioExceptions, string userLevel, ReportType reportType)
        {
            var reportName = "Workload Mgmt Report Summary";
            var worksheetName = "WLM Summary";

            switch (reportType)
            {
                case ReportType.RatioExceptionsReport:
                    reportName = "Ratio Exceptions Report Summary";
                    worksheetName = "Ratio Exceptions Summary";
                    break;
                case ReportType.LargeLoanReport:
                    reportName = "Large Loan Report Summary";
                    worksheetName = "Large Loan Summary";
                    break;
            }

            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, worksheetName);
            int headerRows = ExcelAndPrintHelper.GetHeaderValuesForProjectSummary(projectReportSummary, userLevel, selectedRatioExceptions, cells);
            ExcelAndPrintHelper.SetDataValuestoExcelForProjectSummaryReport(projectReportSummary, userLevel, headerRows, cells);

            //GetResponse(workbook, reportName + " For " + userLevel + DateTime.Now.ToString(" MM-dd-yyyy HHmmss"));
            GetResponse(workbook, reportName + " For " + userLevel + DateTime.UtcNow.ToString(" MM-dd-yyyy HHmmss"));
        }

        //Production
        public ActionResult ExportProdPAMReport(string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate, string status, string strpageTypeIds, string strprogramType, string strprodUserIds, string strstausIds, string Level = "High", Guid? grouptaskinstanceid = null, string Mode = "Other")
        {
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Production PAM Report");

            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Search Criteria";
            var cellsSearch = workbook.Worksheets[1].Cells;




            var pamtasks = _plmProdReportMgr.GetProductionPAMReportSummary(appType, programType, prodUserIds,
fromDate == null ? (DateTime?)null : DateTime.Parse(fromDate.ToString()),
fromDate == null ? (DateTime?)null : DateTime.Parse(toDate.ToString()), status, Level, grouptaskinstanceid, "Excel");
            ExcelAndPrintHelper.SetSearchForProdPAMReport
           (pamtasks, cellsSearch, strpageTypeIds, strprogramType, strprodUserIds, strstausIds, fromDate, toDate);

            ExcelAndPrintHelper.GetHeaderValuesForProdPAMReport(cells, "ProdPAM");
            ExcelAndPrintHelper.SetDataValuesForProdPAMReport(pamtasks.PAMReportGridProductionlList, cells, "ProdPAM");
            //GetResponse(workbook, "Production PAM Report " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Production PAM Report " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }

        public ActionResult ExportProdLenderPAMReport(string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes, string strpageTypeIds, string strprogramType)
        {

            //Get Lender id and Role name
            int lenderId = UserPrincipal.Current.LenderId ?? default(int);
            var roleName = UserPrincipal.Current.UserRole;

            //Set workbook and add work sheet
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Production Lender PAM Report");
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Search Criteria";

            //   Gets status & search fields
            // int? requestStatus = string.IsNullOrWhiteSpace(status) || status == "1,0" || status == "0,1" ? (int?)null : int.Parse(status);
            string requestProgramTypes = programTypes;
            string requestAppType = appType;

            // Gets view model data for search event 
            var viewModel = _plmProdReportMgr.GetProductionLenderPAMExcel(lamIds, bamIds, larIds, roleIds, fromDate, toDate, status, appType, programTypes);

            // Gets data for search criteria tab in excel export
            if (strprogramType != null && strprogramType.Trim().Length > 0)
                viewModel.SelectedProgramTypesList = strprogramType.Split(',').ToList();
            if (strpageTypeIds != null && strpageTypeIds.Trim().Length > 0)
                viewModel.SelectedPageTypesList = strpageTypeIds.Split(',').ToList();
            GetSearchCriteriaProdLenderPamDetails(ref viewModel, lamIds, bamIds, larIds, roleIds, status, fromDate, toDate, roleName, roleName, lenderId);

            viewModel.LenderName = lenderPamReportMgr.GetLenderName((int)UserPrincipal.Current.LenderId);

            //Gets and sets data for Report tab
            ExcelAndPrintHelper.GetHeaderValuesForProdPAMReport(cells, "LenderPAM");
            ExcelAndPrintHelper.SetDataValuesForProdPAMReport(viewModel.PAMReportGridProductionlList, cells, "LenderPAM");

            var cellsSearch = workbook.Worksheets[1].Cells;
            // write the header and values of the search criteria details          
            ExcelAndPrintHelper.SetProdLenderPAMSearchCriteriaLAMBAM(viewModel, cellsSearch);

            workbook.Worksheets[1].PageSetup.Orientation = PageOrientation.Landscape;
            workbook.Worksheets[0].PageSetup.Orientation = PageOrientation.Landscape;

            workbook.Worksheets[0].Select();

            //GetResponse(workbook, "Production Lender PAM Report " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "Production Lender PAM Report " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return View();
        }

        public ProductionPAMReportModel GetSearchCriteriaProdLenderPamDetails(ref ProductionPAMReportModel viewModel, string lamIds, string bamIds, string larIds, string roleIds, string status, DateTime? fromDate, DateTime? toDate, string roleName, string userRole, int lenderId)
        {
            viewModel.FromDate = fromDate.HasValue ? String.Format("{0:MM/dd/yyyy}", fromDate.Value) : string.Empty;
            viewModel.ToDate = toDate.HasValue ? String.Format("{0:MM/dd/yyyy}", toDate.Value) : string.Empty;

            StringBuilder sbString = new StringBuilder();
            if (status != null)
            {
                if (status.Contains('1'))
                {
                    sbString.Append("InQueue");
                }
                if (status.Contains('2'))
                {
                    if (sbString.Length > 0)
                    {
                        //sbString.Append(Environment.NewLine);
                        sbString.Append(',');
                    }
                    sbString.Append("Open");
                }
                if (status.Contains('3'))
                {
                    if (sbString.Length > 0)
                    {
                        sbString.Append(',');
                    }
                    sbString.Append("Closed");
                }
            }

            viewModel.SelectedStatusList = sbString.ToString().Split(',').ToList();


            if (userRole.Equals(HUDRole.LenderAccountRepresentative.ToString("g")))
            {
                viewModel.ReportViewName = "Lender Account Representative View";
            }
            else
            {
                if (userRole.Equals(HUDRole.LenderAccountManager.ToString("g")))
                {
                    viewModel.ReportViewName = "Lender Account Manager View";
                }
                else if (userRole.Equals(HUDRole.BackupAccountManager.ToString("g")))
                {
                    viewModel.ReportViewName = "Backup Account Manager View";
                }

                if (string.IsNullOrEmpty(lamIds))
                {

                    var lamList = (Dictionary<int, string>)_plmProdReportMgr.GetLenderUsersByRoleLenderId(lenderId, HUDRole.LenderAccountManager.ToString("g")).ToDictionary(g => g.UserID, g => g.FirstName + g.LastName);
                    viewModel.SelectedLAMNameList = lamList.Values.ToList();
                }
                else
                {
                    var userList = (Dictionary<int, string>)_plmProdReportMgr.GetUserNameByIDs(lamIds).ToDictionary(g => g.Key, g => g.Value);
                    viewModel.SelectedLAMNameList = userList.Values.ToList();

                }
                if (string.IsNullOrEmpty(bamIds))
                {
                    var bamList = (Dictionary<int, string>)_plmProdReportMgr.GetLenderUsersByRoleLenderId(lenderId, HUDRole.BackupAccountManager.ToString("g")).ToDictionary(g => g.UserID, g => g.FirstName + g.LastName);
                    viewModel.SelectedBAMNameList = bamList.Values.ToList();
                }
                else
                {
                    var userList = (Dictionary<int, string>)_plmProdReportMgr.GetUserNameByIDs(bamIds).ToDictionary(g => g.Key, g => g.Value);
                    viewModel.SelectedBAMNameList = userList.Values.ToList();
                }
                if (string.IsNullOrEmpty(larIds))
                {
                    var larList = (Dictionary<int, string>)_plmProdReportMgr.GetLenderUsersByRoleLenderId(lenderId, HUDRole.LenderAccountRepresentative.ToString("g")).ToDictionary(g => g.UserID, g => g.FirstName + g.LastName);
                    viewModel.SelectedLARNameList = larList.Values.ToList();
                }
                else
                {
                    var userList = (Dictionary<int, string>)_plmProdReportMgr.GetUserNameByIDs(larIds).ToDictionary(g => g.Key, g => g.Value);
                    viewModel.SelectedLARNameList = userList.Values.ToList();

                }
                if (string.IsNullOrEmpty(roleIds))
                {
                    var roleList = new List<string>();

                    roleList.Add("Self");
                    roleList.Add("LAM");
                    roleList.Add("BAM");
                    roleList.Add("LAR");
                    viewModel.SelectedRolesList = roleList;
                }
                else
                {
                    var roleList = new List<string>();
                    if (roleIds.Contains("0"))
                    {
                        roleList.Add("Self");
                    }
                    if (roleIds.Contains("1"))
                    {
                        roleList.Add("LAM");
                    }
                    if (roleIds.Contains("2"))
                    {
                        roleList.Add("BAM");
                    }
                    if (roleIds.Contains("3"))
                    {
                        roleList.Add("LAR");
                    }
                    viewModel.SelectedRolesList = roleList;
                }

            }
            return viewModel;
        }

        public ActionResult ExportProdPAMReportV3(HUDPamReportFiltersModel model)
        {
            ProdPAMReportStatusGridViewModel statusGridViewModel = new ProdPAMReportStatusGridViewModel();
            List<string> TotalNumberOfFYDistinctFHA = new List<string>();
            List<string> LoanOpenedDistinctFHA = new List<string>();
            List<string> TotalClosingDistinctFHA = new List<string>();
            List<string> TotalNumberOfProjectsDistinctFHA = new List<string>();
            List<string> TotalNumberOfFYNA = new List<string>();
            List<string> LoanOpenedNA = new List<string>();
            List<string> TotalClosingNA = new List<string>();
            List<string> TotalNumberOfProjectsNA = new List<string>();
            List<string> TotalNumberOf223a7DistinctFHA = new List<string>();
            List<string> TotalNumberOf223fDistinctFHA = new List<string>();
            List<string> TotalNumberOf223dDistinctFHA = new List<string>();
            List<string> TotalNumberOf223iDistinctFHA = new List<string>();
            List<string> TotalNumberOf241aDistinctFHA = new List<string>();
            List<string> TotalNumberOfNCDistinctFHA = new List<string>();
            List<string> TotalNumberOfSRDistinctFHA = new List<string>();
            List<string> TotalNumberOfIRRDistinctFHA = new List<string>();
            List<string> TotalNumberOf223a7NA = new List<string>();
            List<string> TotalNumberOf223fNA = new List<string>();
            List<string> TotalNumberOf223dNA = new List<string>();
            List<string> TotalNumberOf223iNA = new List<string>();
            List<string> TotalNumberOf241aNA = new List<string>();
            List<string> TotalNumberOfNCNA = new List<string>();
            List<string> TotalNumberOfSRNA = new List<string>();
            List<string> TotalNumberOfIRRNA = new List<string>();
            var dates = ControllerHelper.GetFYDuration();
            DateTime fisStart = dates[0];
            DateTime fisEnd = dates[1];
            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "PAM Report Status Count");
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "PAM Report Filters";
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet3"].Name = "PAM Report Results";
            var outresults = _plmProdReportMgr.GetProdHUDPAMReportResults();
            statusGridViewModel = ControllerHelper.GetPamSummary();
            TotalNumberOf223a7DistinctFHA = outresults.Where(x => x.LoanType == "Refinance 223(a)(7)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf223fDistinctFHA = outresults.Where(x => x.LoanType == "Purchase/Refinance 223(f)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf223dDistinctFHA = outresults.Where(x => x.LoanType == "OLL 223(d)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf223iDistinctFHA = outresults.Where(x => x.LoanType == "Fire Safety 223(i)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf241aDistinctFHA = outresults.Where(x => x.LoanType == "Construction 241(a)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfNCDistinctFHA = outresults.Where(x => x.LoanType == "Construction NC").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfSRDistinctFHA = outresults.Where(x => x.LoanType == "Construction SR").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfIRRDistinctFHA = outresults.Where(x => x.LoanType == "IRR").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf223a7NA = TotalNumberOf223a7DistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf223fNA = TotalNumberOf223fDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf223dNA = TotalNumberOf223dDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf223iNA = TotalNumberOf223iDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf241aNA = TotalNumberOf241aDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfNCNA = TotalNumberOfNCDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfSRNA = TotalNumberOfSRDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfIRRNA = TotalNumberOfIRRDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf223a7DistinctFHA = TotalNumberOf223a7DistinctFHA.Except(TotalNumberOf223a7NA).ToList();
            TotalNumberOf223fDistinctFHA = TotalNumberOf223fDistinctFHA.Except(TotalNumberOf223fNA).ToList();
            TotalNumberOf223dDistinctFHA = TotalNumberOf223dDistinctFHA.Except(TotalNumberOf223dNA).ToList();
            TotalNumberOf223iDistinctFHA = TotalNumberOf223iDistinctFHA.Except(TotalNumberOf223iNA).ToList();
            TotalNumberOf241aDistinctFHA = TotalNumberOf241aDistinctFHA.Except(TotalNumberOf241aNA).ToList();
            TotalNumberOfNCDistinctFHA = TotalNumberOfNCDistinctFHA.Except(TotalNumberOfNCNA).ToList();
            TotalNumberOfSRDistinctFHA = TotalNumberOfSRDistinctFHA.Except(TotalNumberOfSRNA).ToList();
            TotalNumberOfIRRDistinctFHA = TotalNumberOfIRRDistinctFHA.Except(TotalNumberOfIRRNA).ToList();
            TotalNumberOfFYDistinctFHA = outresults.Where(x => x.ProjectStartDate >= fisStart && x.ProjectStartDate <= fisEnd).Select(i => i.FhaNumber).Distinct().ToList();
            LoanOpenedDistinctFHA = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Open").Select(i => i.FhaNumber).Distinct().ToList();
            TotalClosingDistinctFHA = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Complete").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfProjectsDistinctFHA = outresults.Where(x => x.LoanType != null).Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfFYNA = TotalNumberOfFYDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            LoanOpenedNA = LoanOpenedDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalClosingNA = TotalClosingDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfProjectsNA = TotalNumberOfProjectsDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfFYDistinctFHA = TotalNumberOfFYDistinctFHA.Except(TotalNumberOfFYNA).ToList();
            LoanOpenedDistinctFHA = LoanOpenedDistinctFHA.Except(LoanOpenedNA).ToList();
            TotalClosingDistinctFHA = TotalClosingDistinctFHA.Except(TotalClosingNA).ToList();
            TotalNumberOfProjectsDistinctFHA = TotalNumberOfProjectsDistinctFHA.Except(TotalNumberOfProjectsNA).ToList();
            statusGridViewModel.TotalNumberOf223a7 = TotalNumberOf223a7DistinctFHA.Count() + outresults.Where(x => x.LoanType == "Refinance 223(a)(7)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOf223f = TotalNumberOf223fDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Purchase/Refinance 223(f)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOf223d = TotalNumberOf223dDistinctFHA.Count() + outresults.Where(x => x.LoanType == "OLL 223(d)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOf223i = TotalNumberOf223iDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Fire Safety 223(i)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOf241a = TotalNumberOf241aDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Construction 241(a)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfNC = TotalNumberOfNCDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Construction NC").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfSR = TotalNumberOfSRDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Construction SR").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfIRR = TotalNumberOfIRRDistinctFHA.Count() + outresults.Where(x => x.LoanType == "IRR").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfFY = TotalNumberOfFYDistinctFHA.Count() + outresults.Where(x => x.ProjectStartDate >= fisStart && x.ProjectStartDate <= fisEnd).Where(o => o.FhaNumber == "N/A").Count();
            statusGridViewModel.LoanOpened = LoanOpenedDistinctFHA.Count() + outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Open").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalClosing = TotalClosingDistinctFHA.Count() + outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Complete").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfProjects = TotalNumberOfProjectsDistinctFHA.Count() + outresults.Where(x => x.LoanType != null).Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalAppraiserAssigned = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerAppraiserStatus != null && x.ReviewerAppraiserStatus != "Unassigned" && x.ReviewerAppraiserStatus != "N/A").Count();
            statusGridViewModel.TotalEnvironmentalistAssigned = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerEnvironmentalistStatus != null && x.ReviewerEnvironmentalistStatus != "Unassigned" && x.ReviewerEnvironmentalistStatus != "N/A").Count();
            statusGridViewModel.TotalTitleSurveyAssigned = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerTitleSurveyStatus != null && x.ReviewerTitleSurveyStatus != "Unassigned" && x.ReviewerTitleSurveyStatus != "N/A").Count();

            var routeValues = SessionHelper.SessionGet<RouteValueDictionary>(SessionHelper.SESSION_KEY_PRODPAM_FILTERS);
            if (routeValues != null)
            {
                model = (HUDPamReportFiltersModel)routeValues["model"];
                outresults = ControllerHelper.FilterProdPAMResults(model, outresults);
            }
            if (outresults != null && statusGridViewModel != null)
            {
                ExcelAndPrintHelper.SetValuesForProdPAMStatusCount(cells, statusGridViewModel);
                var reportCellsWithFilters = workbook.Worksheets[1].Cells;
                ExcelAndPrintHelper.SetDataValuesForProdPAMReportV3Filters(model, reportCellsWithFilters);
                var reportCellsWithResult = workbook.Worksheets[2].Cells;
                ExcelAndPrintHelper.GetHeaderForProdPAMReportV3(reportCellsWithResult);
                ExcelAndPrintHelper.SetDataValuesForProdPAMReportV3(outresults, reportCellsWithResult);
            }

            //GetResponse(workbook, "HUD_Production_PAM_Report_" + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "HUD_Production_PAM_Report_" + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            return View();

        }
        public ActionResult ExportProdAdhocDataDownloadReport()
        {
            var workbook = Factory.GetWorkbook();
            var reportDate = (string)Session["ReportDate"];
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Production Data Download");
            workbook.Worksheets.Add();
            var results = adhocDataManager.GetProdDataForAdhocDataDownload();
            ExcelAndPrintHelper.GetHeaderForProdAdhocDataDownload(cells, reportDate);
            ExcelAndPrintHelper.SetDataValuesForProdAdhocDataDownload(results, cells);

            //GetResponse(workbook, "HUD_Production_Data_Download_" + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "HUD_Production_Data_Download_" + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));
            //return View();
            return View("~/Views/Production/Adhoc/Index.cshtml");

        }

        public string ExportProdLC(LoanCommitteeFiltersModel model)
        {
            var results = loanCommitteeManager.GetLCFiltersValues();
            results = ControllerHelper.FilterResultForLoanCommitteeScheduler(results, model);
            if (results != null && results.Any())
            {
                foreach (var item in results)
                {
                    if (item.ProjectType != null)
                    {
                        item.ProjectTypeName = fhaNumberRequestManager.GetProjectTypeById((int)item.ProjectType);
                    }
                }
            }
            results = results.OrderBy(x => x.SequenceId).ToList();
            if (model != null)
            {
                if (model.ProjectType != null) if (model.ProjectType != 0) model.ProjectTypeName = fhaNumberRequestManager.GetProjectTypeById((int)model.ProjectType);
            }


            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "Loan Committee Filters");
            workbook.Worksheets.Add();
            workbook.Worksheets["Sheet2"].Name = "Loan Committee Results";
            ExcelAndPrintHelper.SetValuesForProdLCFilters(model, cells);
            var reportCellsWithData = workbook.Worksheets[1].Cells;
            ExcelAndPrintHelper.GetHeaderForLoanCommitteeScheduler(reportCellsWithData);
            ExcelAndPrintHelper.SetDataValuesForLoanCommitteeScheduler(results, reportCellsWithData);
            //string filename = "Loan_Committee_Scheduler_" + DateTime.Now.ToString("MM - dd - yyyy HHmmss") + ".xlsx";
            string filename = "Loan_Committee_Scheduler_" + DateTime.UtcNow.ToString("MM - dd - yyyy HHmmss") + ".xlsx";
            var path = Server.MapPath("~/uploads/");


            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {

                    return "";
                }
            }
            workbook.SaveAs(path + filename, FileFormat.OpenXMLWorkbook);

            //GetResponse(workbook, "Loan_Committee_Scheduler_" + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            return filename;//View("~/Views/Production/LoanCommitteeScheduler/LoanCommitteeSchedulerMain.cshtml");

        }
        [DeleteFileAttribute]
        public ActionResult DownloadLCFile(string file)
        {

            string fullPath = Path.Combine(Server.MapPath("~/uploads"), file);
            return File(fullPath, "application/vnd.ms-excel", file);
        }
    }

    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();

            //convert the current filter context to file and get the file path
            string filePath = (filterContext.Result as FilePathResult).FileName;

            //delete the file after download
            System.IO.File.Delete(filePath);
        }
    }
}
