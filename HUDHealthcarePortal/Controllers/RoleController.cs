﻿using BusinessService.Interfaces;
using Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using HUDHealthcarePortal.Filters;

namespace HUDHealthcarePortal.Controllers
{
    /// <summary>
    /// This class is related to user roles
    /// </summary>
    [Authorize]
    public class RoleController : Controller
    {
        IAccountManager accountMgr;
        IUploadDataManager uploadDataManager;

        public RoleController()
            : this(new AccountManager(), new UploadDataManager())
        {

        }

        public RoleController(IAccountManager accountManager)
        {
            accountMgr = accountManager;
        }

        public RoleController(IAccountManager accountManager, IUploadDataManager uploadDataManager)
        {
            accountMgr = accountManager;
            this.uploadDataManager = uploadDataManager;
        }

        /// <summary>
        /// This method gets the roles by selected user type
        /// </summary>
        /// <param name="userTypeId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetRolesByUserType(int userTypeId)
        {
            var roleSource = (HUDRoleSource)userTypeId;
            var roles = EnumType.GetEnumListByCategory(roleSource.ToString(), typeof(HUDRole));

            const int lenderAccountManagerEnumValue = 11;
            const int backupAccountManagerEnumValue = 10;

            // Restricting LAM from creating a LAM
            if (UserPrincipal.Current.UserRole == Enum.GetName(typeof(HUDRole), lenderAccountManagerEnumValue))
            {
                roles.Remove(HUDRole.LenderAccountManager);
            }
            // Restricting BAM from creating LAM and BAM
            if (UserPrincipal.Current.UserRole == Enum.GetName(typeof(HUDRole), backupAccountManagerEnumValue))
            {
                roles.Remove(HUDRole.LenderAccountManager);
                roles.Remove(HUDRole.BackupAccountManager);
            }

            return Json(roles, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveUserRoleChanges(UpdateRoleModel roleModel)
        {
            try
            {
                var rolesSelected = roleModel.RolesToUpdate.ToList();
                var rolesInType = EnumType.GetEnumListByCategory(roleModel.RoleSourceType.ToString(), typeof(HUDRole));
                // get user roles in db
                //var currentRoles = Roles.GetRolesForUser().ToList();
                var currentRoles = UserPrincipal.Current.Roles.ToList();
                List<string> currentRolesInType = new List<string>();
                currentRolesInType.AddRange(rolesInType.Where(p => currentRoles.Contains(EnumType.Parse<HUDRole>(p.Key).ToString())).Select(p => p.Key));

                // new roles, where exist in rolesSelected but not in currntRolesInType
                List<string> newRoles = new List<string>();
                newRoles.AddRange(rolesSelected.Where(p => !currentRolesInType.Contains(p.Key)).Select(p => EnumType.Parse<HUDRole>(p.Key).ToString()));

                // roles to remove, where exist in currentRolesInType but not in rolesSelected
                List<string> rolesToRemove = new List<string>();
                rolesToRemove.AddRange(currentRolesInType.Where(p => !rolesSelected.Exists(k => k.Key == p)).Select(p => EnumType.Parse<HUDRole>(p).ToString()));

                Session[SessionHelper.SESSION_KEY_NEW_ROLES] = newRoles;
                Session[SessionHelper.SESSION_KEY_REMOVE_ROLES] = rolesToRemove;

                var result = new { Success = true };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateUserRoles(UpdateRoleModel roleModel)
        {
            try
            {
                var rolesSelected = roleModel.RolesToUpdate.ToList();
                var rolesPickedNumber = roleModel.RolesToUpdate.Count;
                var rolesPickedType = roleModel.RoleSourceType;
                if ((rolesPickedNumber >= 2) && (rolesPickedType == HUDRoleSource.ExternalUser))
                    throw new InvalidProgramException("You are allowed to select only one role for the External User.");
                if ((rolesPickedNumber >= 2) && (rolesPickedType == HUDRoleSource.InternalUser) && rolesSelected.Contains(HUDRole.ProductionUser))
                    throw new InvalidProgramException("You are not allowed to select any other role along with ProductionUser Role.");

                List<string> newRoles = new List<string>();
                newRoles.AddRange(rolesSelected.Select(p => EnumType.Parse<HUDRole>(p.Key).ToString()));

                Session[SessionHelper.SESSION_KEY_NEW_ROLES] = newRoles;

                var result =
                         new
                         {
                             Success = true,
                             IsServicer = newRoles.Contains("Servicer"),
                             IsOperator = newRoles.Contains("OperatorAccountRepresentative"),
                             IsWorkloadManager = newRoles.Contains("WorkflowManager"),
                             IsAccountExecutive = newRoles.Contains("AccountExecutive"),
                             IsAdmin = (newRoles.Contains("HUDAdmin") || newRoles.Contains("HUDDirector")),
                             IsInternalSpecialOptionUser = newRoles.Contains("InternalSpecialOptionUser"),
                             IsProdUser = (newRoles.Contains("ProductionUser")),
                             IsReviewer = (newRoles.Contains("Reviewer")),
                             IsContractInspector = (newRoles.Contains("InspectionContractor"))
                         };

                if ((rolesPickedNumber >= 2) && (rolesPickedType == HUDRoleSource.InternalUser) && (result.IsInternalSpecialOptionUser == true))
                    throw new InvalidProgramException("You are allowed to select only InternalSpecialOptionUser role for the Internal User.");


                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (InvalidProgramException ei)
            {
                //Message = "You have to select only one role for the External User."
                var result = new { Success = false, ei.Message };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetLenders()
        {
            //var lenders = accountMgr.GetInstitutionsByRoles(new string[] { "Lender" }).ToList();
            var lenders = accountMgr.GetLendersByUserRole().ToList();
            return Json(lenders, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFhasByWLMId(int wlmid)
        {
            //var fhasbywlmid = accountMgr.GetFhasByWLMId(wlmid).ToList();
            //return Json(fhasbywlmid, JsonRequestBehavior.AllowGet);

            var fhas = accountMgr.GetFhasByWLMId(wlmid).ToList()
                   .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                       p.FHANumber, p.FHADescription)));
            var result = new { Success = true, Fhas = fhas };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateUserLARLinksForWLM(IDictionary<string, string> selectedFhas, int wlmid)
        {
            try
            {
                Session[SessionHelper.SESSION_KEY_LAR_FHA_LINKS] = selectedFhas;
                Session[SessionHelper.SESSION_KEY_WORKLOADMANAGERID] = wlmid.ToString();


                var result = new { Success = true };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult GetFHAsFromLenders()
        {
            try
            {
                var selectedLenders = SessionHelper.SessionExtract<IDictionary<int, string>>(SessionHelper.SESSION_KEY_NEW_LENDERS);
                if (selectedLenders == null || selectedLenders.Count == 0)
                    throw new InvalidOperationException("You have to select at least one lender.");
                string sLenderIds = TextUtils.CommaDelimitedText(selectedLenders.Select(p => p.Key));

                // convert to key value pair to be consumed by multi selector
                var fhas = accountMgr.GetFHAsByLenders(sLenderIds).ToList()
                    .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                        p.FHANumber, p.FHADescription)));
                var result = new { Success = true, Fhas = fhas };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This method gets the available FHAs for an operator by user role, and the list of current FHAs for the Operator.
        /// </summary>
        /// <param></param>
        /// <returns>fhas, multiSelectLenderFhaPairs</returns>

        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        [HttpGet]
        public JsonResult GetSelectedAndAvailableFhasForOar()
        {
            try
            {
                string lenderIds = "-1";
                if (RoleManager.IsCurrentUserLenderAdmin() && UserPrincipal.Current.LenderId != null)
                {
                    lenderIds = UserPrincipal.Current.LenderId.Value.ToString();
                }

                // convert to key value pair to be consumed by multi selector
                var fhas = accountMgr.GetFHAsByLenders(lenderIds).ToList().OrderBy(c => c.FHADescription)
                    .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                        p.FHANumber, p.FHADescription)));

                List<string> lenderFhaNumbers = new List<string>();
                var fhaLenderPairs =
                    uploadDataManager.GetFhasByLenderIds("-1").ToList();

                IEnumerable<KeyValuePair<string, string>> multiSelectLenderFhaPairs = null;
                if (Session[SessionHelper.SESSION_KEY_MANAGING_OAR] != null)
                {
                    int operatorId = ((UserViewModel)Session[SessionHelper.SESSION_KEY_MANAGING_OAR]).UserID;
                    lenderFhaNumbers = uploadDataManager.GetAllowedFhasForUser(operatorId).ToList();
                    fhaLenderPairs = fhaLenderPairs.Where(p => lenderFhaNumbers.Contains(p.FHANumber)).OrderBy(c => c.FHADescription).ToList();
                    multiSelectLenderFhaPairs = fhaLenderPairs.Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                        p.FHANumber, p.FHADescription)));
                    fhas = fhas.Where(p => !multiSelectLenderFhaPairs.Contains(p));
                    Session[SessionHelper.SESSION_KEY_MANAGING_OAR_FHA_LIST] = multiSelectLenderFhaPairs;
                    var result = new { Success = true, Fhas = fhas, existingFHAs = multiSelectLenderFhaPairs };
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                var nullResult = new { Success = true, Fhas = fhas, existingFHAs = multiSelectLenderFhaPairs };
                return Json(nullResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This method updates FHAs for an operator based on previously existing FHAs, and intended FHAs.
        /// </summary>
        /// <param name="selectedFhas"></param>
        /// <returns></returns>

        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        [HttpPost]
        public JsonResult SaveUpdatedOarFhas(IDictionary<string, string> selectedFhas)
        {
            try
            {
                UserViewModel operatorUser = (UserViewModel)Session[SessionHelper.SESSION_KEY_MANAGING_OAR];
                IEnumerable<KeyValuePair<string, string>> existingFhas = (IEnumerable<KeyValuePair<string, string>>)Session[SessionHelper.SESSION_KEY_MANAGING_OAR_FHA_LIST];
                var success = accountMgr.UpdateOarFhas(operatorUser, existingFhas, selectedFhas);
                var result = new { Success = true };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the fhas from lam or bam or Admin.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetFhAsFromLamOrBamOrAdmin()
        {
            try
            {
                object fhas = null;
                if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName))
                {
                    fhas =
                        uploadDataManager.GetFhasByLenderIds("-1")
                            .ToList()
                            .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                                p.FHANumber, p.FHADescription)));
                }
                else
                {
                    var lendersIds = accountMgr.GetLendersByUserId(UserPrincipal.Current.UserId).ToList();
                    string sLenderIds = TextUtils.CommaDelimitedText(lendersIds);
                    // convert to key value pair to be consumed by multi selector
                    fhas = accountMgr.GetFHAsByLenders(sLenderIds).ToList()
                        .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                            p.FHANumber, p.FHADescription)));
                }

                var result = new { Success = true, Fhas = fhas };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveSelectedLenders(IDictionary<int, string> selectedLenders)
        {
            try
            {
                Session[SessionHelper.SESSION_KEY_NEW_LENDERS] = selectedLenders;

                var result = new { Success = true };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateUserLARLinks(IDictionary<string, string> selectedFhas)
        {
            try
            {
                Session[SessionHelper.SESSION_KEY_LAR_FHA_LINKS] = selectedFhas;
                //var Fhanumbs = new List<string>();

                // Fhanumbs =  selectedFhas.Keys.ToList();
                ////Fhanumbs = SessionHelper.SessionExtract<List<string>>(SessionHelper.SESSION_KEY_LAR_FHA_LINKS);

                //accountMgr.deleteuser(Fhanumbs);


                var result = new { Success = true };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveSelectedFhasForIC(IDictionary<string, string> selectedFhas)
        {
            try
            {
                Session[SessionHelper.SESSION_KEY_IC_FHA_LINKS] = selectedFhas;

                var result = new { Success = true };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult GetFhasReadyForApplication()
        {
            object fhas = null;
            try
            {
                //TODO:populated FHA # must be ready for application
                fhas =
                       uploadDataManager.GetReadyFhasForInspectionContracotr()
                           .ToList()
                           .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                               p.FHANumber, p.FHADescription)));
                var result = new { Success = true, Fhas = fhas };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This method gets the available FHAs for an Inspection Contractor by user role, 
        /// and the list of current FHAs for the Inspection Contractor.
        /// </summary>
        /// <param></param>
        /// <returns>fhas, multiSelectLenderFhaPairs</returns>

        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        [HttpGet]
        public JsonResult GetSelectedAndAvailableFhasForIC()
        {
            try
            {
                var fhas =
                        uploadDataManager.GetReadyFhasForInspectionContracotr()
                            .ToList()
                            .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                                p.FHANumber, p.FHADescription)));

                List<FhaInfoModel> inspectorFhaNumbers = new List<FhaInfoModel>();

                IEnumerable<KeyValuePair<string, string>> multiSelectLenderFhaPairs = null;
                if (Session[SessionHelper.SESSION_KEY_MANAGING_OAR] != null)
                {
                    int userId = ((UserViewModel)Session[SessionHelper.SESSION_KEY_MANAGING_OAR]).UserID;
                    inspectorFhaNumbers = uploadDataManager.GetSelectedFhasForInspector(userId).ToList();


                    multiSelectLenderFhaPairs = inspectorFhaNumbers.Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                        p.FHANumber, p.FHADescription)));
                    fhas = fhas.Where(p => !multiSelectLenderFhaPairs.Contains(p));
                    Session[SessionHelper.SESSION_KEY_IC_FHA_LINKS] = multiSelectLenderFhaPairs;
                    var result = new { Success = true, Fhas = fhas, existingFHAs = multiSelectLenderFhaPairs };
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                var nullResult = new { Success = true, Fhas = fhas, existingFHAs = multiSelectLenderFhaPairs };
                return Json(nullResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This method updates FHAs for an Inspection Contractor based on previously selected FHAs.
        /// </summary>
        /// <param name="selectedFhas"></param>
        /// <returns></returns>

        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        [HttpPost]
        public JsonResult SaveUpdatedICFhas(IDictionary<string, string> selectedICFhas)
        {
            try
            {
                UserViewModel InspContractor = (UserViewModel)Session[SessionHelper.SESSION_KEY_MANAGING_OAR];
                IEnumerable<KeyValuePair<string, string>> existingFhas = (IEnumerable<KeyValuePair<string, string>>)Session[SessionHelper.SESSION_KEY_IC_FHA_LINKS];
                var success = accountMgr.UpdateOarFhas(InspContractor, existingFhas, selectedICFhas);
                var result = new { Success = true };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //karri#354
        [HttpGet]
        public JsonResult GetServicerFHANumbersFromLamOrBamOrAdmin(string servicerUserId = "111")
        {
            try
            {
                object fhas = null;
                if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName))
                {
                    fhas =
                        uploadDataManager.GetFhasByLenderIds("-1")
                            .ToList()
                            .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                                p.FHANumber, p.FHADescription)));
                }
                else
                {
                    var lendersIds = accountMgr.GetLendersByUserId(UserPrincipal.Current.UserId).ToList();
                    string sLenderIds = TextUtils.CommaDelimitedText(lendersIds);
                    // convert to key value pair to be consumed by multi selector
                    //fhas = accountMgr.GetFHAsForServicer(lendersIds, servicerUserId).ToList()
                    //fhas = accountMgr.GetFHAsForServicer(lendersIds.ToString(), servicerUserId.ToString()).ToList()
                    //    .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                    //        p.FHANumber, p.FHADescription)));

                    fhas = accountMgr.GetFHAsForServicer(sLenderIds, servicerUserId).ToList()
                        .Select(p => new KeyValuePair<string, string>(p.FHANumber, string.Format("{0} ({1})",
                            p.FHANumber, p.FHADescription)));
                }

                var result = new { Success = true, Fhas = fhas };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteFHAs(IDictionary<string, string> selectedFhas, int Userid)
        {
            try
            {
                Session[SessionHelper.SESSION_KEY_LAR_FHA_LINKS] = selectedFhas;
                var Fhanumbs = new List<string>();

                Fhanumbs = selectedFhas.Keys.ToList();
                //Fhanumbs = SessionHelper.SessionExtract<List<string>>(SessionHelper.SESSION_KEY_LAR_FHA_LINKS);

                accountMgr.deleteuser(Fhanumbs, Userid);


                var result = new { Success = true };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var result = new { Success = false, Message = e.InnerException.ToString() };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


    }
}
