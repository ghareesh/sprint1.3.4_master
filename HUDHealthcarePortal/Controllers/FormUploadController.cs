﻿using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Data;
using System.Text;
using System.Web.Routing;
using BusinessService.Interfaces;
using Core;
using Core.Utilities;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthCarePortal.Helpers;
using HUDHealthcarePortal.Model;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using WebGrease.Css.Extensions;
using System.Configuration;
using HUDHealthcarePortal.Filters;
using System.Data.SqlClient;
using Core.ExceptionHandling.Types;
using HUDHealthcarePortal.Core.ExceptionHandling;

namespace HUDHealthcarePortal.Controllers
{
    public class FormUploadController : Controller
    {
        private readonly bool _isAeEmailAlertEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsAEEmailAlertEnabled"]);
        ILookupManager lookupMgr;
        IUploadDataManager uploadDataManager;
        IAccountManager accountMgr;
        ITaskManager taskMgr;
        IEmailManager emailMgr;
        IBackgroundJobMgr backgroundJobMgr;
        BizRuleManager bizRuleMgr = new BizRuleManager();

        public FormUploadController()
            : this(new LookupManager(), new BackgroundJobMgr(),
                new UploadDataManager(), new AccountManager(), new TaskManager(), new EmailManager())
        {

        }

        public FormUploadController(ILookupManager lookupManager, IBackgroundJobMgr backgroundManager, IUploadDataManager
            uploadManager, IAccountManager accountManager, ITaskManager taskManager, IEmailManager emailManager)
        {
            lookupMgr = lookupManager;
            uploadDataManager = uploadManager;
            backgroundJobMgr = backgroundManager;
            accountMgr = accountManager;
            taskMgr = taskManager;
            emailMgr = emailManager;
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetFormUploadDetail(bool isEditMode)
        {
            Dictionary<string, string> fhaNumberList;
            var model = (FormUploadModel)(TempData["FormUploadData"] ?? new FormUploadModel());

            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            if (model != null)
            {
                model.IsFromTask = true;
                model.NewNotes = string.Empty;
                var financialTypes = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(FinancialStatementType)));
                model.AllFinancialStatementType = financialTypes.ToList();
                model.AllOperatorOwners = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(OperatorOwner))).ToList();
                model.AllMonthsInPeriod = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(MonthsInPeriod))).ToList();
                var allLenders = lookupMgr.GetAllLenders();
                model.AllServicerNames = SelectionItemHelper.GetSelectItemsFrom(allLenders).ToList();
                model.SelectedServicerId = model.LenderID ?? 0;
                var firstOrDefault = model.AllServicerNames.FirstOrDefault(p => p.Value.Equals(model.LenderID.ToString()));
                if (firstOrDefault != null)
                    model.SelectedLenderName = model.LenderID == 0 ? string.Empty
                        : firstOrDefault.Text;

                if (model.TaskGuid != null)
                {
                    model.TaskFileName = taskMgr.GetTaskFileNameByTaskInstanceId(model.TaskGuid.Value);
                }

                if (isEditMode)
                {
                    TempData["FormAttachmentFileTypes"] = ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();
                    GetFHAsByLenderForDDL(model);
                    ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
                    // store opened by user to TaskOpenStatus column
                    if (model.TaskOpenStatus != null && !model.TaskOpenStatus.TaskOpenStatus.Exists(p => p.Key.Equals(UserPrincipal.Current.UserName)))
                    {
                        model.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    }

                    if (model.IsSubmitMode)
                    {
                        PopupHelper.ConfigPopup(TempData, 720, 350, "Submit Disclaimer", false, false, true, false, false, true, "../FormUpload/PreSubmitConfirm");
                    }
                    else
                    {
                        PopupHelper.ConfigPopup(TempData, 720, 350, "Upload Disclaimer", false, false, true, false, false, true, "../FormUpload/PreUploadConfirm");
                    }
                    var assignedByUser = accountMgr.GetUserByUsername(model.AssignedBy);
                    int lenderId;
                    if (RoleManager.IsCurrentOperatorAccountRepresentative())
                    {
                        if (UserPrincipal.Current.LenderId != null)
                        {
                           //lenderId = UserPrincipal.Current.LenderId.Value;
                             lenderId = model.LenderID.Value;
                             var lenderUsers = accountMgr.GetLenderUsersByLenderId(lenderId).ToList();
                            var lenderUsersAndId = new List<string>();
                            foreach (var item in lenderUsers)
                            {
                                string instName = string.IsNullOrEmpty(item.Lender_Name) ? "Admin" : item.Lender_Name;
                                if (instName != "Admin")
                                    lenderUsersAndId.Add(item.FirstName + " " + item.LastName + " (" + instName + ", " + EnumType.GetEnumDescription(EnumType.Parse<HUDRole>(item.RoleName)) + ")|" + item.UserID.ToString());
                            }

                            model.AvailableLenderUsers = SetDropDownListValues(lenderUsersAndId, true);
                        }
                        if (assignedByUser != null) model.SelectedLenderUserId = assignedByUser.UserID;
                        fhaNumberList = GetFhaNumbersListBasedOnUser(model.SelectedLenderUserId);
                        
                        model.AvailableFHANumbersList.Clear();
                        foreach (var fhaNumber in fhaNumberList)
                        {
                            model.AvailableFHANumbersList.Add(fhaNumber.Value);
                        }
                        return View("FormUploadOperatorView", model);
                    }
                    if (!RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName) && RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
                    {
                        // get available lenders for this AE
                        var lenderUsers = accountMgr.GetLenderUsersByAeUserId(UserPrincipal.Current.UserId);
                        var lenderUsersAndId = new List<string>();
                        foreach (var item in lenderUsers)
                        {
                            string instName = string.IsNullOrEmpty(item.Lender_Name) ? "Admin" : item.Lender_Name;
                            if (instName != "Admin")
                                lenderUsersAndId.Add(item.FirstName + " " + item.LastName + " (" + item.Lender_Name + ", " + EnumType.GetEnumDescription(EnumType.Parse<HUDRole>(item.RoleName)) + ")|" + item.UserID.ToString());
                        }

                        model.AvailableLenderUsers = SetDropDownListValues(lenderUsersAndId, true);

                        // get lender to which oar submitted
                        var tasks = taskMgr.GetTasksByTaskInstanceId(model.TaskGuid.Value);
                        var lenderUserName =
                            tasks.First(
                                p => p.SequenceId == model.SequenceId && p.AssignedTo != UserPrincipal.Current.UserName)
                                .AssignedTo;
                        int lenderUserId = accountMgr.GetUserByUsername(lenderUserName).UserID;
                        model.SelectedLenderUserId = lenderUserId;
                        fhaNumberList = GetFhaNumbersListBasedOnUser(model.SelectedLenderUserId);
                        
                        model.AvailableFHANumbersList.Clear();
                        foreach (var fhaNumber in fhaNumberList)
                        {
                            model.AvailableFHANumbersList.Add(fhaNumber.Value);
                        }
                        return View("FormUploadAeView", model);
                    }

                    lenderId = HUDConstants.ALL_SQL_IDS;

                    if (RoleManager.IsCurrentUserLenderRoles())
                        if (UserPrincipal.Current.LenderId != null) lenderId = UserPrincipal.Current.LenderId.Value;
                    var operators = accountMgr.GetOperatorsByLenderId(lenderId).ToList();
                    var operatorsAndId = new List<string>();

                    foreach (var item in operators)
                    {
                        operatorsAndId.Add(item.FirstName + " " + item.LastName + " (" + item.Lender_Name + ")|" + item.UserID.ToString());
                    }

                    model.AvailableOperators = SetDropDownListValues(operatorsAndId, true);
                    
                    model.SelectedOperatorId = operators.ToList().Count == 1 ? operators.First().UserID : assignedByUser.UserID;
                    fhaNumberList = GetFhaNumbersListBasedOnUser(model.SelectedOperatorId);
                    
                    model.AvailableFHANumbersList.Clear();
                    foreach (var fhaNumber in fhaNumberList)
                    {
                        model.AvailableFHANumbersList.Add(fhaNumber.Value);
                    }
                    return View("FormUploadLenderView", model);
                }
                return View("FormUploadReadOnly", model);
            }
            return View("FormUploadLenderView", model);
        }

        [HttpGet]
        [AccessDeniedAuthorize(
            Roles = "SuperUser,Administrator,HUDAdmin,HUDDirector,WorkflowManager,LenderAdmin,LenderAccountManager,BackupAccountManager,AccountExecutive,LenderAccountRepresentative,Servicer,InternalSpecialOptionUser,OperatorAccountRepresentative,Attorney")]
        public ActionResult Index(FormUploadModel modelInput)
        {
            // clear error message
            Session["Confirmation"] = "";

            FormUploadModel model = modelInput ?? new FormUploadModel();

            var financialTypes = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(FinancialStatementType)));
            model.AllFinancialStatementType = financialTypes.ToList();
            model.AllOperatorOwners = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(OperatorOwner))).ToList();
            model.AllMonthsInPeriod = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(MonthsInPeriod))).ToList();
            var allLenders = lookupMgr.GetAllLenders().ToList();

            if (RoleManager.IsCurrentUserLenderRoles())
            {
                var selectedLender = allLenders.FirstOrDefault(p => p.Key == UserPrincipal.Current.UserData.LenderId);
                model.AllServicerNames.Insert(0, new SelectListItem() { Text = selectedLender.Value, Value = selectedLender.Key.ToString(CultureInfo.InvariantCulture) });
                if (!RoleManager.IsCurrentOperatorAccountRepresentative())
                {
                    model.SelectedServicerId = selectedLender.Key;
                    model.SelectedLenderName = model.SelectedServicerId == 0 ? string.Empty
                        : model.AllServicerNames.FirstOrDefault(p => p.Value.Equals(model.SelectedServicerId.ToString())).Text;
                    model.AvailableFHANumbersList = uploadDataManager.GetFhasByLenderIds(UserPrincipal.Current.UserData.LenderId.Value.ToString())
                        .Select(p => p.FHANumber).ToList();
                }
            }
            else
            {
                model.AllServicerNames = SelectionItemHelper.GetSelectItemsFrom(allLenders).ToList();
                /*if (!string.IsNullOrEmpty(model.SelectedLenderName))
                {
                    model.SelectedServicerId = -1;
                    model.SelectedLenderName = string.Empty;
                }*/
            }

            if (model.IsSubmitMode)
            {
                PopupHelper.ConfigPopup(TempData, 720, 350, "Submit Disclaimer", false, false, true, false, false, true, "../FormUpload/PreSubmitConfirm");
            }
            else
            {
                PopupHelper.ConfigPopup(TempData, 720, 350, "Upload Disclaimer", false, false, true, false, false, true, "../FormUpload/PreUploadConfirm");
            }
            if ((TempData["PasswordExpires"] != null && (bool)TempData["PasswordExpires"])
                || (TempData["bShowChangePasswordMsg"] != null && (bool)TempData["bShowChangePasswordMsg"]))
            { PopupHelper.ConfigPopup(TempData, 640, 340, "Change Password", false, false, true, false, false, true, "../Account/ChangePassword"); }

            TempData["FormAttachmentFileTypes"] = ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();
            return PopulateViewModelAndNavigateBasedOnUserRole(model);
        }

        //PRC Bug 463 Please add comments below.
        [HttpGet]
        public FileResult DownloadTaskFile(Guid? taskInstanceId)
        {
            TaskFileModel taskFile = taskMgr.GetTaskFileByTaskInstanceId(taskInstanceId.Value);
            if (taskFile != null)
            {
                return File(taskFile.FileData, "text/text", taskFile.FileName);
            }
            return null;
        }

        [HttpPost]
        public ActionResult UploadForm(FormUploadModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {
            if (model.UploadFile != null)
            {
                TempData["FormAttachmentFileTypes"] = ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();
                Session["Confirmation"] = ControllerHelper.CheckFileType(model.UploadFile.FileName, (string)TempData["FormAttachmentFileTypes"]);
                if (Session["Confirmation"] != null)
                {
                    return FormValidationFailed(model);
                }
                TaskHelper.CacheFileData(TempData, model.UploadFile);
            }
            //validating date cannot be a future date
            if (!string.IsNullOrEmpty(model.PeriodEnding))
            {
                try
                {
                    DateTime dPeriodEnding = DateTime.Parse(model.PeriodEnding);
                    //DateTime dCurrentDate = DateTime.Now;
                    DateTime dCurrentDate = DateTime.UtcNow;
                    TimeSpan tDateDifference = dCurrentDate - dPeriodEnding;
                    // cannot enter future date in period ending
                    if (tDateDifference < new TimeSpan(0, 0, 0))
                    {
                        Session["Confirmation"] = "Date must not be in the future.";
                        return FormValidationFailed(model);
                    }
                    Session["Confirmation"] = "";
                }
                catch (FormatException)
                {
                    Session["Confirmation"] = "Period Ending date selected doesn't belong to Gregorian calendar year.";
                    return FormValidationFailed(model);
                }
            }
            //validating quarterly and year-end financial rules
            bool isLateSubmit = false;
            var uploadStatusFlag = bizRuleMgr.GetUploadStatusFlag(model.MonthsInPeriod, model.PeriodEnding);
            switch (uploadStatusFlag)
            {
                case UploadStatusFlag.BadData:
                    Session["Confirmation"] = "Wrong Period Ending or Months in Period.";
                    return FormValidationFailed(model);
                    break;
                case UploadStatusFlag.Late:
                    isLateSubmit = true;
                    break;
            }

            if (!taskGuid.HasValue && !model.IsFhaValid)
            {
                ModelState.AddModelError("InvalidFhaErr", "FHA Number entered is invalid.");
            }
            var displayErrors = bizRuleMgr.UploadFieldsCannotEmpty(model);
            if (!String.IsNullOrEmpty(displayErrors))
            {
                ModelState.AddModelError("FieldEmptyErr", displayErrors);
            }

            //populate FHANumbers dropdown
            GetFHAsByLenderForDDL(model);
            if (ModelState.IsValid)
            {
                model.IsReadyToSubmitUpload = true;
                ViewBag.IsReadyToSubmitUpload = true;
                if (!model.IsUploadTermConfirmed)
                    return Index(model);
                Session["Confirmation"] = null;
                var financialTypes =
                    SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(FinancialStatementType)));
                model.AllFinancialStatementType = financialTypes.ToList();
                model.AllOperatorOwners =
                    SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(OperatorOwner))).ToList();
                model.AllMonthsInPeriod =
                    SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(MonthsInPeriod))).ToList();
                var allLenders = lookupMgr.GetAllLenders();

                model.ServiceName = model.SelectedLenderName;

                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                model.DataInserted = model.ModifiedOn.Value;
                model.UserID = model.ModifiedBy.Value;
                model.IsLate = isLateSubmit;

                //Remove the leading zeros to match exactly the format when uploaded from Excel sheet which removes leading zeros from datetime.
                DateTime periodEndingDate = DateTime.ParseExact(model.PeriodEnding, "d", CultureInfo.InvariantCulture,
                    DateTimeStyles.None).Date;
                model.PeriodEnding = periodEndingDate.ToString("G").TrimStart(new char['0']);

                // insert into intermediate data table
                uploadDataManager.SaveFormUploadToIntermediateTbl(model);

                // need to call usp_HCP_Verify_PropertyID_iRems as in excel upload to fill in property id, project name, lender name
                // they were auto filled before submitting form, but property id is missing
                //uploadDataManager.UpdateLenderPropertyInfo(UserPrincipal.Current.UserId, model.LenderID.Value);  

                // calculate and update ratios
                uploadDataManager.CalcAndUpdateUploadedData();

                //Code for the Intermediate Calculations
                int resultDistribution = 1;
                int resultCalculations = 1;
                int resultCalculateErrors = 1;
                resultDistribution = uploadDataManager.DistributeQuarterly();
                if (resultDistribution >= 0)
                {
                    resultCalculations = uploadDataManager.ApplyCalculations();
                    if (resultCalculations < 0)
                    {
                        throw new DataUploadException(ExceptionCategory.ExcelUploadException, "Failed To Apply Calculations");

                    }
                    else
                    {
                        resultCalculateErrors = uploadDataManager.CalculateErrors();
                    }
                  
                    if (resultCalculateErrors < 0)
                    {
                        throw new DataUploadException(ExceptionCategory.ExcelUploadException, "Failed To Calculate Errors");

                    }

                }
                else
                {
                    throw new DataUploadException(ExceptionCategory.ExcelUploadException, "Failed To Distribute Quarterly");
                }

                // need to call usp_HCP_Verify_PropertyID_iRems as in excel upload to fill in property id, project name, lender name
                // they were auto filled before submitting form, but property id is missing
                if (model.LenderID != null)
                    uploadDataManager.UpdateLenderPropertyInfo(UserPrincipal.Current.UserId, model.LenderID.Value);

                var taskList = new List<TaskModel>();
                // save task to finish
                var task = new TaskModel();

                //var toUser = accountMgr.GetUserById(model.SubmitToUserId.Value);
                // save submit form data in task db, task table, serialize upload data
                // model operator/owner and financial types saved in intermediate is string 
                // serialized to task are integers

                task.AssignedBy = UserPrincipal.Current.UserName;
                task.AssignedTo = UserPrincipal.Current.UserName;
                // Don't keep local filename info in model
                model.BaseFileName = null;
                model.FullyQualifiedFileName = null;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(FormUploadModel), Encoding.Unicode);
                task.Notes = model.NewNotes;
                task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                task.Concurrency = concurrency;
                task.TaskStepId = (int)TaskStep.Final;
                taskList.Add(task);
                var taskFile = UpdateTaskFileModelForTask(model, task);
                try
                {
                    taskMgr.SaveTask(taskList, taskFile);
                    List<string> fha = new List<string>();
                    fha.Add(model.FHANumber);

                    // use thread pool to finish sending email so not holding up UI
                    if (_isAeEmailAlertEnabled)
                    {
                        backgroundJobMgr.SendUploadNotificationEmails(emailMgr, fha);
                    }
                    return RedirectToAction("ReportExcelUpload", "ExcelUploadView_");
                }
                catch (DbUpdateConcurrencyException)
                {
                    HandleConcurrentUpdateException(model);
                }
            }

            return FormValidationFailed(model);
        }

        //Get FHANumbers for the logged in LAR/LAM/BAM to populate in the FHANumber DDL
        private void GetFHAsByLenderForDDL(FormUploadModel model)
        {
            if (RoleManager.IsCurrentUserLenderRoles())
            {
                if (UserPrincipal.Current.LenderId != null)
                {
                    var results =
                        uploadDataManager.GetFhasByLenderIds(UserPrincipal.Current.UserData.LenderId.ToString())
                            .OrderBy(p => p.FHANumber)
                            .ToList();
                    if (RoleManager.IsCurrentOperatorAccountRepresentative())
                    {
                        var fhas = uploadDataManager.GetAllowedFhasForUser(UserPrincipal.Current.UserId);
                        results = results.Where(p => fhas.Contains(p.FHANumber)).ToList();
                    }
                    foreach (var item in results)
                    {
                        model.AvailableFHANumbersList.Add(item.FHANumber);
                    }
                }
            }
            else
            {
                model.AvailableFHANumbersList.Add(model.FHANumber);
            }
        }

        private void HandleConcurrentUpdateException(FormUploadModel model)
        {
            model.IsConcurrentUserUpdateFound = true;
        }

        [HttpPost]
        public ActionResult SubmitForm(FormUploadModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {
            bool isCurrentUserOperator = RoleManager.IsCurrentOperatorAccountRepresentative();
            bool isCurrentUserSpecialOptionUser = RoleManager.IsCurrentSpecialOptionUser();
            bool isCurrentUserLender = RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName);
            bool isCurrentUserAE = !RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName) &&
                                   RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName);
            if (model.UploadFile != null)
            {
                TempData["FormAttachmentFileTypes"] = ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();
                Session["Confirmation"] = ControllerHelper.CheckFileType(model.UploadFile.FileName, (string)TempData["FormAttachmentFileTypes"]);
                if (Session["Confirmation"] != null)
                {
                    return FormValidationFailed(model);
                }
                TaskHelper.CacheFileData(TempData, model.UploadFile);
            }

            if (!isCurrentUserOperator || isCurrentUserSpecialOptionUser) // lender or special option user submit to operator
            {
                if (model.SelectedOperatorId != null)
                {
                    int operatorUserId = model.SelectedOperatorId.Value;
                    var operatorFhas = uploadDataManager.GetAllowedFhasForUser(operatorUserId).ToList();
                    if (operatorFhas.Count < 1 || !operatorFhas.Contains(model.FHANumber))
                    {
                        Session["Confirmation"] = string.Format("Selected FHA Number:{0} is not tied to selected operator",
                            model.FHANumber);
                        return FormValidationFailed(model);
                    }
                }
            }
            if (!isCurrentUserLender && !isCurrentUserOperator) // nonlender and nonoperator submit to lender
            {
                if (model.SelectedLenderUserId != null)
                {
                    int lenderUserId = model.SelectedLenderUserId.Value;
                    var operatorFhas = uploadDataManager.GetAllowedFhaByLenderUserId(lenderUserId);
                    //var operatorFhas = uploadDataManager.GetAllowedFhaByLenderUserId(lenderUserId);
                    if (operatorFhas.Count < 1 || !operatorFhas.Contains(model.FHANumber))
                    {
                        Session["Confirmation"] = string.Format("Selected FHA Number:{0} is not tied to selected lender",
                            model.FHANumber);
                        return FormValidationFailed(model);
                    }
                }
            }

            //validating date cannot be a future date
            if (!string.IsNullOrEmpty(model.PeriodEnding))
            {
                DateTime dPeriodEnding = DateTime.Parse(model.PeriodEnding);

                //DateTime dCurrentDate = DateTime.Now;
                DateTime dCurrentDate = DateTime.UtcNow;
                TimeSpan tDateDifference = dCurrentDate - dPeriodEnding;
                // cannot enter future date in period ending
                if (tDateDifference < new TimeSpan(0, 0, 0))
                {
                    Session["Confirmation"] = "Date must not be in the future.";
                    return FormValidationFailed(model);
                }
                Session["Confirmation"] = "";
            }

            // for lenders: LAM, BAM, LAR must submit to Operator; AE submits to lenders
            if ((!isCurrentUserOperator || isCurrentUserSpecialOptionUser) && !isCurrentUserAE && (!model.SelectedOperatorId.HasValue || model.SelectedOperatorId < 1))
            {
                ModelState.AddModelError("OperatorSelectErr", "Please select an operator.");
            }
            if (ModelState.IsValid)
            {
                model.IsReadyToSubmitUpload = true;
                ViewBag.IsReadyToSubmitUpload = true;
                if (!model.IsUploadTermConfirmed)
                {
                    GetFHAsByLenderForDDL(model);

                    if (RoleManager.IsCurrentOperatorAccountRepresentative())
                    {
                        if (UserPrincipal.Current.LenderId != null)
                        {
                            //lenderId = UserPrincipal.Current.LenderId.Value;
                            var lenderId = model.LenderID.Value;
                            var lenderUsers = accountMgr.GetLenderUsersByLenderId(lenderId).ToList();
                            var lenderUsersAndId = new List<string>();
                            foreach (var item in lenderUsers)
                            {
                                string instName = string.IsNullOrEmpty(item.Lender_Name) ? "Admin" : item.Lender_Name;
                                if (instName != "Admin")
                                    lenderUsersAndId.Add(item.FirstName + " " + item.LastName + " (" + instName + ", " + EnumType.GetEnumDescription(EnumType.Parse<HUDRole>(item.RoleName)) + ")|" + item.UserID.ToString());
                            }

                            model.AvailableLenderUsers = SetDropDownListValues(lenderUsersAndId, true);
                        }
                        if (model.SubmitByUserId != null) model.SelectedLenderUserId = model.SubmitByUserId.Value;
                    }
                    return Index(model);
                }
                // reset is upload confirmed
                model.IsUploadTermConfirmed = false;
                Session["Confirmation"] = null;
                var taskList = new List<TaskModel>();
                var task = new TaskModel();
                var taskAe = new TaskModel();
                bool bNeedAeTask = false;
                if (isCurrentUserOperator)
                {
                    model.SubmitToUserId = model.SelectedLenderUserId;
                    model.SubmitByUserId = UserPrincipal.Current.UserId;
                    bNeedAeTask = true;
                }
                else if (isCurrentUserAE)
                {
                    model.SubmitByUserId = UserPrincipal.Current.UserId;
                    model.SubmitToUserId = model.SelectedLenderUserId;
                }
                else
                {
                    model.SubmitByUserId = UserPrincipal.Current.UserId;
                    model.SubmitToUserId = model.SelectedOperatorId;
                }
                if (model.SubmitToUserId != null)
                {
                    var toUser = accountMgr.GetUserById(model.SubmitToUserId.Value);
                    // save submit form data in task db, task table, serialize upload data
                    task.AssignedBy = UserPrincipal.Current.UserName;
                    task.AssignedTo = toUser.UserName;
                }
                // Don't keep local filename info in model
                model.BaseFileName = null;
                model.FullyQualifiedFileName = null;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(FormUploadModel), Encoding.Unicode);
                task.Notes = model.NewNotes;
                task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                task.Concurrency = concurrency;
                task.TaskStepId = (int)TaskStep.SubmitForm;
                taskList.Add(task);

                //Commenting as per the ticket 1543
                //if (bNeedAeTask)
                //{
                //    taskAe.DataStore1 = task.DataStore1;
                //    taskAe.Notes = task.Notes;
                //    taskAe.SequenceId = task.SequenceId;
                //    taskAe.StartTime = task.StartTime;

                //    // get AE's own timezone
                //    var aeEmail = emailMgr.GetAEEmailsByFhaNumbers(new[] { model.FHANumber }).Keys.First();
                //    var aeUser = accountMgr.GetUserByUsername(aeEmail);
                //    // if this AE does not exist in db, use eastern standard time zone, as HUD locates in DC
                //    var aeTimeZoneNameId = aeUser == null ? "3" : aeUser.SelectedTimezoneId;
                //    string aeTimeZoneName = EnumUtils.Parse<HUDTimeZone>(aeTimeZoneNameId).ToName();
                //    taskAe.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime, aeTimeZoneName);
                //    taskAe.TaskInstanceId = task.TaskInstanceId;
                //    taskAe.Concurrency = concurrency;
                //    taskAe.TaskStepId = task.TaskStepId;
                //    taskAe.AssignedBy = UserPrincipal.Current.UserName;
                //    taskAe.AssignedTo = aeEmail;
                //    taskList.Add(taskAe);
                //}

                var taskFile = UpdateTaskFileModelForTask(model, task);

                try
                {
                    taskMgr.SaveTask(taskList, taskFile);
                    // email notification for submission
                    UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                    string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);
                    backgroundJobMgr.SendSubmitNotificationEmails(emailMgr, model.FHANumber, task.AssignedBy,
                        task.AssignedTo, myTaskUrl);
                    return RedirectToAction("MyTasks", "Task");//, new { page = TempData["page"], sort = TempData["sort"], sortdir = TempData["sortdir"]});
                }
                catch (DbUpdateConcurrencyException)
                {
                    HandleConcurrentUpdateException(model);
                }
            }
            return FormValidationFailed(model);
        }


        private TaskFileModel UpdateTaskFileModelForTask(FormUploadModel model, TaskModel task)
        {
            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.UploadFile == null && TempData["UploadFile"] != null)
            {
                model.UploadFile = (HttpPostedFileBase)TempData["UploadFile"];
            }
            // Get the file data.
            TaskFileModel taskFile = new TaskFileModel();
            if (model.UploadFile != null)
            {
                taskFile.FileName = Path.GetFileName(model.UploadFile.FileName);
                taskFile.FileType = FileType.ServicerInput;
                taskFile.UploadTime = task.MyStartTime;
                BinaryReader rdr = null;
                try
                {
                    rdr = new BinaryReader(model.UploadFile.InputStream);
                    taskFile.FileData = rdr.ReadBytes(model.UploadFile.ContentLength);
                }
                finally
                {
                    // Ensure Streams do not get left to garbage collection, the data can persist.
                    if (rdr != null)
                    {
                        rdr.Close();
                        rdr.Dispose();
                        model.UploadFile.InputStream.Flush();
                    }
                    model.UploadFile.InputStream.Close();
                    model.UploadFile.InputStream.Dispose();
                }
                // Get the taskGuid for the associated task.
                taskFile.TaskInstanceId = task.TaskInstanceId;
            }
            return taskFile;
        }

        [HttpPost]
        public ActionResult SubmitUpload(string submitButton, FormUploadModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {
            switch (submitButton)
            {
                case "Submit":
                    // delegate submit to another controller action
                    return (SubmitForm(model, taskGuid, sequenceId, concurrency));
                case "Upload":
                    // call another action to perform the upload
                    return (UploadForm(model, taskGuid, sequenceId, concurrency));
                default:
                    // If they've submitted the form without a submitButton, 
                    // just return the view again.
                    return (Index(model));
            }
        }

        [HttpGet]
        public JsonResult ValidateFhaNumber(string searchText, int maxResults)
        {
            List<FhaInfoModel> fhaLenderPairsFiltered = null;
            if (RoleManager.IsCurrentUserLenderRoles())
            {
                List<FhaInfoModel> fhaLenderPairs = new List<FhaInfoModel>();
                List<string> fhas;
                if (RoleManager.IsCurrentOperatorAccountRepresentative())
                {
                    var lenderIds = accountMgr.GetLenderIdsByUser(UserPrincipal.Current.UserId);
                    fhaLenderPairs.AddRange(lenderIds.SelectMany(id => uploadDataManager.GetFhasByLenderIds(id.ToString()).ToList()));
                    fhas = uploadDataManager.GetAllowedFhasForUser(UserPrincipal.Current.UserId).ToList();
                    fhaLenderPairsFiltered = fhaLenderPairs.Where(p => fhas.Contains(p.FHANumber)).ToList();
                }
                else if (UserPrincipal.Current.UserData.LenderId != null)
                {
                    fhaLenderPairs = uploadDataManager.GetFhasByLenderIds(UserPrincipal.Current.UserData.LenderId.Value.ToString()).ToList();
                    fhaLenderPairsFiltered = fhaLenderPairs;
                }

            }
            else // if not lender allow to show all fha numbers
            {
                fhaLenderPairsFiltered = uploadDataManager.GetFhasByLenderIds(HUDConstants.ALL_SQL_IDS.ToString(CultureInfo.InvariantCulture)).ToList();
            }
            JsonResult json = null;
            if (fhaLenderPairsFiltered != null && searchText != null)
            {
                var results = fhaLenderPairsFiltered.Where(p => p.FHANumber.Contains(searchText)).OrderBy(p => p.FHANumber)
                    .Select(p => new { p.FHANumber, ProjectName = p.FHADescription, p.LenderId, LenderName = p.Lender_Name })
                    .Distinct().Take(maxResults);
                json = Json(results, JsonRequestBehavior.AllowGet);

            }
            return json;
        }

        //Populate the FHANumber DDL based on the selected operator/lar/lam/bam
        [HttpGet]
        public JsonResult GetFhaNumbers(string userid)
        {
            int userId = userid == null ? 0 : int.Parse(userid);
            var results = GetFhaNumbersListBasedOnUser(userId);
            JsonResult json = Json(results.ToList(), JsonRequestBehavior.AllowGet);
            return json;
        }

        [HttpGet]
        public JsonResult GetOperatorsLenderbyFHA(string fhanumber)
        {
            //umesh
            //int userId = userid == null ? 0 : int.Parse(userid);
            //var UserIDs = uploadDataManager.GetLenderOperatorsbyFHA(fhanumber).ToList();
            int lenderId = HUDConstants.ALL_SQL_IDS;
            if (RoleManager.IsCurrentUserLenderRoles())
                if (UserPrincipal.Current.LenderId != null) lenderId = UserPrincipal.Current.LenderId.Value;
            var operators = accountMgr.GetOperatorsByLenderId(lenderId).ToList().Where(a =>a.FhaNumber==fhanumber);
    
            var operatorsAndId = new List<string>();

            foreach (var item in operators)
            {
                //operatorsAndId.Add(item.UserID.ToString(), item.FirstName + " " + item.LastName + " (" + item.Lender_Name + ")");
                operatorsAndId.Add(item.FirstName + " " + item.LastName + " (" + item.Lender_Name + ")|" + item.UserID.ToString());
            }
          var listOps=  SetDropDownListValues(operatorsAndId, true);

       //   var results = listOps.Where(o => UserIDs.Any(c => c.Equals (( Convert.ToInt32(o.Value )))));

          JsonResult json = Json(listOps.ToList(), JsonRequestBehavior.AllowGet);
            return json;
        }

        private Dictionary<string, string> GetFhaNumbersListBasedOnUser(int? userId)
        {
            List<FhaInfoModel> fhaLenderPairsFiltered = null;

            if (RoleManager.IsCurrentUserLenderRoles())
            {
                IEnumerable<FhaInfoModel> fhaLenderPairs = new List<FhaInfoModel>();
                int lenderId = 0;
                if (RoleManager.IsCurrentOperatorAccountRepresentative())
                {
                    if (userId != null)
                    {
                        if (RoleManager.IsUserLenderRole(accountMgr.GetUserById((int)userId).UserName))
                        {
                            var lenderIds = accountMgr.GetLendersByUserId((int)userId).ToList();
                            var id = lenderIds[0];
                            if (id != null)
                            {
                                lenderId = (int)id;
                                fhaLenderPairs = uploadDataManager.GetFhasByLenderIds(lenderId.ToString()).ToList();
                            }
                        }

                    }
                }
                else
                {
                    lenderId = (int)UserPrincipal.Current.UserData.LenderId;
                    fhaLenderPairs =
                        uploadDataManager.GetFhasByLenderIds(UserPrincipal.Current.UserData.LenderId.Value.ToString()).ToList();
                }

                if (lenderId != null)
                {
                    if (userId != 0)
                    {
                        if (userId != null)
                        {
                            List<string> fhas;
                            if (RoleManager.IsCurrentOperatorAccountRepresentative())
                            {
                                fhas = uploadDataManager.GetAllowedFhasForUser(UserPrincipal.Current.UserId).ToList();
                            }
                            else
                            {
                                fhas = RoleManager.IsUserLenderRole(accountMgr.GetUserById((int)userId).UserName)
                                    ? uploadDataManager.GetAllowedFhaByLenderUserId((int)userId).ToList()
                                    : uploadDataManager.GetAllowedFhasForUser((int)userId).ToList();
                            }
                            fhaLenderPairsFiltered = fhaLenderPairs.Where(p => fhas.Contains(p.FHANumber)).ToList();
                        }

                        if (RoleManager.IsCurrentSpecialOptionUser())
                        {
                            var specialOptionUserFhas =
                                uploadDataManager.GetAllowedFhasForUser(UserPrincipal.Current.UserId);
                            if (fhaLenderPairsFiltered != null)
                                fhaLenderPairsFiltered =
                                    fhaLenderPairsFiltered.Where(p => specialOptionUserFhas.Contains(p.FHANumber)).ToList();
                        }
                    }
                    else
                    {
                        fhaLenderPairsFiltered = (List<FhaInfoModel>)fhaLenderPairs;
                    }
                }
            }
            else if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
            {
                var lenderUsers = accountMgr.GetLenderUsersByAeUserId(UserPrincipal.Current.UserId);
                foreach (var lenderUser in lenderUsers)
                {
                    var lenderFhaNumbers =
                        uploadDataManager.GetFhasByLenderIds(lenderUser.LenderID.ToString()).ToList();
                    if (lenderUser.UserID == userId && userId != 0)
                    {
                        var fhas = uploadDataManager.GetAllowedFhaByLenderUserId((int)userId);
                        fhaLenderPairsFiltered = lenderFhaNumbers.Where(p => fhas.Contains(p.FHANumber)).ToList();
                    }
                    else if (userId == 0)
                    {
                        fhaLenderPairsFiltered = lenderFhaNumbers;
                    }
                }
            }

            var results = new Dictionary<string, string>();
            if (fhaLenderPairsFiltered != null)
            {
                fhaLenderPairsFiltered = fhaLenderPairsFiltered.OrderBy(p => p.FHANumber).ToList();

                foreach (var item in fhaLenderPairsFiltered)
                {
                    results.Add(item.FHANumber, item.FHANumber);
                }
            }
            else if (RoleManager.IsHudAdmin(UserPrincipal.Current.UserName) || RoleManager.IsWorkloadManagerRole(UserPrincipal.Current.UserName))
            {
                if (userId != null)
                {
                    var fhas = uploadDataManager.GetAllowedFhasForUser((int)userId);
                    fhas = fhas.OrderBy(p => p).ToList();
                    foreach (var fhaNumber in fhas)
                    {
                        results.Add(fhaNumber, fhaNumber);
                    }
                }
            }
            return results;
        }

        [AllowAnonymous]
        public ActionResult SingleMessage(string message)
        {
            ViewBag.Message = message;
            return View();
        }

        [HttpPost]
        public ActionResult PreUploadConfirm()
        {
            return PartialView("PreUploadConfirm");
        }

        [HttpPost]
        public ActionResult PreSubmitConfirm()
        {
            return PartialView("PreSubmitConfirm");
        }

        [HttpPost]
        public ActionResult ConcurrentUpdateError()
        {
            return PartialView("ConcurrentUpdateErrorView");
        }

        private ActionResult FormValidationFailed(FormUploadModel model)
        {
            var financialTypes = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(FinancialStatementType)));
            model.AllFinancialStatementType = financialTypes.ToList();
            model.AllOperatorOwners = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(OperatorOwner))).ToList();
            model.AllMonthsInPeriod = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(MonthsInPeriod))).ToList();
            var allLenders = lookupMgr.GetAllLenders().ToList();

            if (model.IsConcurrentUserUpdateFound)
            {
                PopupHelper.ConfigPopup(TempData, 600, 250, "Concurrrent Update Found", false, false, true, false, false, true, "../FormUpload/ConcurrentUpdateError");
                ViewBag.IsConcurrentUserUpdateFound = true;
            }

            if (RoleManager.IsCurrentUserLenderRoles())
            {
                var selectedLender = allLenders.FirstOrDefault(p => p.Key == UserPrincipal.Current.UserData.LenderId);
                model.AllServicerNames.Insert(0, new SelectListItem() { Text = selectedLender.Value, Value = selectedLender.Key.ToString() });
                model.SelectedServicerId = selectedLender.Key;
                model.SelectedLenderName = selectedLender.Key == 0 ? string.Empty
                    : model.AllServicerNames.FirstOrDefault(p => p.Value.Equals(selectedLender.Key.ToString())).Text;
            }
            else
            {
                model.AllServicerNames = SelectionItemHelper.GetSelectItemsFrom(allLenders).ToList();
            }
            return PopulateViewModelAndNavigateBasedOnUserRole(model);
        }

        private IList<SelectListItem> SetDropDownListValues(List<string> listOfValues, bool sortKeys)
        {
            IList<SelectListItem> selectList = new List<SelectListItem>();

            if (sortKeys) listOfValues.Sort();

            foreach (var value in listOfValues)
            {
                var values = value.Split('|');

                selectList.Add(new SelectListItem()
                {
                    Text = values[0],
                    Value = values[1]
                });
            }

            return selectList;
        }
        
        private ActionResult PopulateViewModelAndNavigateBasedOnUserRole(FormUploadModel model)
        {
            int lenderId = HUDConstants.ALL_SQL_IDS;
            Dictionary<string, string> fhaNumberList;
            var lenderUsersAndId = new List<string>();

            if (RoleManager.IsCurrentOperatorAccountRepresentative())
            {
                List<UserInfoModel> lenderUsers = new List<UserInfoModel>();
                if (UserPrincipal.Current.LenderId != null)
                {
                    var lenderIds = accountMgr.GetLenderIdsByUser(UserPrincipal.Current.UserId);
                    lenderUsers.AddRange(lenderIds.SelectMany(id => accountMgr.GetLenderUsersByLenderId((int)id).ToList()));

                    if (lenderUsers != null)
                    {
                        foreach (var item in lenderUsers)
                        {
                            string instName = string.IsNullOrEmpty(item.Lender_Name) ? "Admin" : item.Lender_Name;
                            if (instName != "Admin")
                            {
                                lenderUsersAndId.Add(item.FirstName + " " + item.LastName + " (" + instName + ", " + EnumType.GetEnumDescription(EnumType.Parse<HUDRole>(item.RoleName)) + ")|" + item.UserID.ToString(CultureInfo.InvariantCulture));
                            }
                        }

                        model.AvailableLenderUsers = SetDropDownListValues(lenderUsersAndId, true);
                    }
                }
                fhaNumberList = GetFhaNumbersListBasedOnUser(model.SelectedLenderUserId);
                foreach (var fhaNumber in fhaNumberList)
                {
                    model.AvailableFHANumbersList.Add(fhaNumber.Value);
                }

                return View("FormUploadOperatorView", model);
            }

            if (!RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName) &&
                RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
            {
                // get available lenders for this AE
                var lenderUsers = accountMgr.GetLenderUsersByAeUserId(UserPrincipal.Current.UserId);
                foreach (var item in lenderUsers)
                {
                    lenderUsersAndId.Add(item.FirstName + " " + item.LastName + " (" + item.Lender_Name + ", " +
                            EnumType.GetEnumDescription(EnumType.Parse<HUDRole>(item.RoleName)) + ")|" + item.UserID.ToString());
                }

                model.AvailableLenderUsers = SetDropDownListValues(lenderUsersAndId, true);


                fhaNumberList = GetFhaNumbersListBasedOnUser(UserPrincipal.Current.UserId);
                foreach (var fhaNumber in fhaNumberList)
                {
                    model.AvailableFHANumbersList.Add(fhaNumber.Value);
                }
                return View("FormUploadAeView", model);
            }

            if (RoleManager.IsCurrentUserLenderRoles())
                if (UserPrincipal.Current.LenderId != null) lenderId = UserPrincipal.Current.LenderId.Value;
            //var operators = accountMgr.GetOperatorsByLenderId(lenderId).ToList();
            var UserIds = new List<int>();
            if(model.FHANumber!=null )
            {
                var operators = accountMgr.GetOperatorsByLenderId(lenderId).ToList().Where(a => a.FhaNumber == model.FHANumber);
                var operatorsAndId = new List<string>();
               

                foreach (var item in operators)
                {
                    //operatorsAndId.Add(item.UserID.ToString(), item.FirstName + " " + item.LastName + " (" + item.Lender_Name + ")");
                    operatorsAndId.Add(item.FirstName + " " + item.LastName + " (" + item.Lender_Name + ")|" + item.UserID.ToString());
                    UserIds.Add(item.UserID);
                }

                model.AvailableOperators = SetDropDownListValues(operatorsAndId, true);
            }
          
            //umesh
            var results = new Dictionary<string, string>();


            if (RoleManager.IsSuperUserRole(UserPrincipal.Current.UserName))
            {
                var operators = accountMgr.GetOperatorsByLenderId(lenderId).ToList();
                foreach (var item in operators)
                {
                   
                    UserIds.Add(item.UserID);
                }
                if (UserIds != null)
                {
                    var fhas = uploadDataManager.GetAllowedFhasForUsers(UserIds);
                    fhas = fhas.OrderBy(p => p).ToList();
                    foreach (var fhaNumber in fhas)
                    {
                        results.Add(fhaNumber, fhaNumber);
                    }
                }

                fhaNumberList = results;

            }
            else
            {
                fhaNumberList = GetFhaNumbersListBasedOnUser(model.SelectedOperatorId);
            }
    

            //fhaNumberList = GetFhaNumbersListBasedOnUser(model.SelectedOperatorId);

            foreach (var fhaNumber in fhaNumberList)
            {
                model.AvailableFHANumbersList.Add(fhaNumber.Value);
            }

            return View("FormUploadLenderView", model);
        }

        
    }
}
