﻿using BusinessService.Interfaces;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Controllers.OPAForm
{
    public class OPAReviewController : Controller
    {
        private IProjectActionFormManager projectActionFormManager;
        private IProjectActionManager projectActionManager;
        private IGroupTaskManager groupTaskManager;
        private ITaskManager taskManager;
        private IAccountManager accountManager;
        private IEmailManager emailManager;
        private IBackgroundJobMgr backgroundJobManager;



        public OPAReviewController()
            : this(new ProjectActionFormManager(), new TaskManager(), new AccountManager(), new EmailManager(), new BackgroundJobMgr(), new ProjectActionManager(), new GroupTaskManager())
        {

        }

        private OPAReviewController(IProjectActionFormManager _projectActionFormManager, ITaskManager _taskManager, IAccountManager _accountManager, IEmailManager _emailManager, IBackgroundJobMgr backgroundManager, IProjectActionManager _projectActionManager, IGroupTaskManager _groupTaskManager)
        {
            projectActionFormManager = _projectActionFormManager;
            taskManager = _taskManager;
            accountManager = _accountManager;
            emailManager = _emailManager;
            backgroundJobManager = backgroundManager;
            projectActionManager = _projectActionManager;
            groupTaskManager = _groupTaskManager;

        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetgridContent(Guid taskInstanceId, string sidx, string sord, int page, int rows)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var history = projectActionFormManager.GetOpaHistory(taskInstanceId);
            int totalrecods = history.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            if (sord.ToUpper() == "DESC")
            {

                history = history.OrderByDescending(s => s.submitDate).ToList();

                history = history.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }
            else
            {
                history = history.OrderBy(s => s.submitDate).ToList();
                history = history.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }

            var jsonData = new
            {

                total = totalpages,
                page,
                records = totalrecods,
                rows = history,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);



        }


        public JsonResult GetFileCommentHistory(int taskFileId)
        {
            List<OPAHistoryViewModel> taskFileComments = new List<OPAHistoryViewModel>();
            List<OPAHistoryViewModel> taskFileCommentList = new List<OPAHistoryViewModel>();
            OPAHistoryViewModel taskFileComment = new OPAHistoryViewModel();

            taskFileComments = projectActionFormManager.GetOpaFileCommentHistory(taskFileId);

            //Setting the date based on the Time Zone
            if (taskFileComments != null)
            {
                foreach (var item in taskFileComments)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    item.uploadDate = item.uploadDate;
                }
            }

            var jsonData = new
            {
                total = 1,
                page = 1,
                records = 1,
                rows = taskFileComments,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }

        public JsonResult GetFileUploadHistory(int taskFileId)
        {
            List<OPAHistoryViewModel> taslFileComments = new List<OPAHistoryViewModel>();

            taslFileComments = projectActionFormManager.GetOpaFileUploadHistory(taskFileId);
            //Setting the date based on the Time Zone
            if (taslFileComments != null)
            {
                foreach (var item in taslFileComments)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    item.uploadDate = item.uploadDate;

                }
            }



            var jsonData = new
            {
                total = 1,
                page = 1,
                records = 1,
                rows = taslFileComments.OrderByDescending(x => x.uploadDate).ToList(),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);


        }


        public JsonResult GetFileGridContent(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {
            // ArrayList at = new ArrayList();

            List<OPAHistoryViewModel> reviewerTask = new List<OPAHistoryViewModel>();
            var opaview = new OPAViewModel();
            reviewerTask = GetOpaHistory(taskInstanceId);
            //opaview = projectActionFormManager.GetOPAByTaskInstanceId((Guid)taskInstanceId);
            //opaview.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(opaview.FhaNumber).PropertyId;


            //var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId, opaview.PropertyId.ToString(), opaview.FhaNumber);
            //GridContent = GridContent.ToList();
            //foreach (var prevfile in GridContent)
            //{
            //    if (prevfile.id == null)
            //    {
            //        prevfile.id = "39";
            //        prevfile.parent = "39";
            //    }
            //}
            //// GridContent = GridContent.Where(x => x.id != "32" && x.id !="40" && x.id !="41").ToList();
            //GridContent = GridContent.Where(x => x.id.Split('_')[0] != "32" && x.id.Split('_')[0] != "40" && x.id.Split('_')[0] != "41").ToList();

            //GridContent = GridContent.Where(x => x.id == "39").ToList();






           // reviewerTask = reviewerTask.Where(x => x.userRole != null).ToList();
            //reviewerTask = reviewerTask.GroupBy(x => x.ReviewFileStatusId).Select(y => y.First()).ToList();
            // reviewerTask = reviewerTask.Distinct().Select(x=>x.taskFileId).ToList();

            //reviewerTask = reviewerTask.GroupBy(m => new { m.taskFileId }).Select(g => g.Last()).ToList();
            // reviewerTask = reviewerTask.Where(x => x.fileReviewStatusId != x.fileReviewStatusId).ToList();
            // reviewerTask = reviewerTask.Where(x => x.fileReviewStatusId != 1 || x.DocTypeID != null).ToList();
           // reviewerTask = reviewerTask.Where(x => x.fileReviewStatusId != 1 || x.isFileComment != true).ToList();
            // reviewerTask = reviewerTask.Where(x => x.IsOPAUpload != 1 || x.fileReviewStatusId != 1).ToList();


            //Setting the date based on the Time Zone
            foreach (var item in reviewerTask)
            {
                //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                item.uploadDate = item.uploadDate;
                item.submitDate = item.submitDate;
                // if(item.actionTaken =="")
                //item.userRole = UserPrincipal.Current.UserRole;

                if (UserPrincipal.Current.UserRole.Contains("LenderAccountRepresentative") || UserPrincipal.Current.UserRole.Contains("LenderAccountManager")
                || UserPrincipal.Current.UserRole.Contains("BackupAccountManager"))
                {

                    string statusName1 = "( New Unread )";
                    string statusName2 = "( RR Unread  )";
                    string statusName3 = "( Read       )";
                    string statusName4 = "( RAI        )";

                    if (item.actionTaken.Contains("New Unread"))
                    {
                        item.actionTaken = item.actionTaken.Replace(statusName1, "");
                    }
                    else if (item.actionTaken.Contains("RR Unread"))
                    {
                        item.actionTaken = item.actionTaken.Replace(statusName2, "");
                    }
                    else if (item.actionTaken.Contains("Read"))
                    {
                        item.actionTaken = item.actionTaken.Replace(statusName3, "");
                    }
                    else if (item.actionTaken.Contains("RAI"))
                    {
                        item.actionTaken = item.actionTaken.Replace(statusName4, "");
                    }

                }

            }

            var jsonData = new
            {
                total = 1,
                page = 1,
                records = 1,
                rows = reviewerTask,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);



        }

        public List<OPAHistoryViewModel> GetOpaHistory(Guid taskInstanceId)
        {
            return projectActionFormManager.GetOpaHistory(taskInstanceId);

        }



        [HttpPost]
        public int RenameTaskFile(string taskfileid, string NewName)
        {
            int value = 0;
            var taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            taskFile.FileName = NewName;
            if (taskFile != null)
            {
                taskManager.UpdateTaskFile(taskFile);
                value = 1;
            }

            return value;
        }



    }
}
