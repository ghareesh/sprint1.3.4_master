﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;
using BusinessService.AssetManagement;
using BusinessService;
using Model;
//using BusinessService.Interfaces.Production;
using BusinessService.Production;
using Model.Production;
using System.Net;// User Story 1901
using Repository.Interfaces;
using Repository;
using System.Configuration;
using Newtonsoft.Json.Linq;
using BusinessService.Interfaces.Production;
using BusinessService.ManagementReport;
using System.Collections;
using HUDHealthcarePortal.Repository.Interfaces;
//using HUDHealthcarePortal.Repository;

namespace HUDHealthcarePortal.Controllers.OPAForm
{
    public class OPAFormController : Controller
    {

        private IProjectActionFormManager projectActionFormManager;
        private IProjectActionManager projectActionManager;
        private IGroupTaskManager groupTaskManager;
        private ITaskManager taskManager;
        private ISubFolderStructureRepository folderManager;
        private IAccountManager accountManager;
        private IEmailManager emailManager;
        private IBackgroundJobMgr backgroundJobManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager; // User Story 1901
        private IParentChildTaskManager parentChildTaskManager;
        private IRequestAdditionalInfoFileManager requestAdditionalInfoFilesManager;
        private IReviewFileStatusManager reviewFileStatusManager;
        private IProd_RestfulWebApiDownload WebApiDownload;
        private IProd_RestfulWebApiUpdate WebApiUpdate;
        private IProd_RestfulWebApiDelete WebApiDelete;
        private IProd_RestfulWebApiTokenRequest WebApiTokenRequest;
        private IProd_RestfulWebApiDocumentUpload WebApiDocumentUpload;
        private IProd_RestfulWebApiReplace WebApiDocumentReplace;
        private IProd_GroupTasksManager prod_GroupTasksManager;
        private IOPAManager iOPAForm;
        IWebSecurityWrapper webSecurity;
        private IAppProcessManager appProcessManager;
        private string DefaultDocID = ConfigurationManager.AppSettings["DefaultDocId"].ToString();
        private ITaskFile_FolderMappingManager taskFile_FolderMappingManager;
        private IProd_NextStageManager prod_NextStageManager;
        IPAMReportManager _plmReportMgr;
        private IProd_RestfulWebApiUpdate prodRestfulWebApiUpdate;
        //#605
        private IProd_TaskXrefManager prod_TaskXrefManager;
        //#605
        private IProductionQueueManager productionQueue;
        //#605
        private readonly ILookupManager lookupMgr;
        private IFHANumberRequestManager fhaRequestManager;
        private IProd_SharepointScreenManager sharepointScreenManager;


        //
        // GET: /OPAForm/

        public OPAFormController()
            : this(
                new ProjectActionFormManager(), new TaskManager(), new AccountManager(), new EmailManager(),
                new BackgroundJobMgr(), new ProjectActionManager(), new GroupTaskManager(),
                new NonCriticalRepairsRequestManager(), new ParentChildTaskManager(),
                new RequestAdditionalInfoFilesManager(), new ReviewFileStatusManager(), new WebSecurityWrapper(), new OPAManager(),
                new Prod_RestfulWebApiDownload(), new Prod_RestfulWebApiTokenRequest(), new Prod_RestfulWebApiUpdate(),
                new Prod_RestfulWebApiDocumentUpload(), new Prod_RestfulWebApiReplace(), new Prod_RestfulWebApiDelete(), new SubFolderStructureRepository(), new Prod_GroupTasksManager(), new AppProcessManager()
                   , new TaskFile_FolderMappingManager(), new Prod_NextStageManager(), new PAMReportsManager(), new Prod_RestfulWebApiUpdate(), new Prod_TaskXrefManager()
                 , new ProductionQueueManager(), new LookupManager(), new FHANumberRequestManager(), new Prod_SharepointScreenManager())

        {
        }

        private OPAFormController(IProjectActionFormManager _projectActionFormManager, ITaskManager _taskManager,
            IAccountManager _accountManager, IEmailManager _emailManager, IBackgroundJobMgr backgroundManager,
            IProjectActionManager _projectActionManager, IGroupTaskManager _groupTaskManager,
            INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
            IParentChildTaskManager _parentChildTaskManager,
            IRequestAdditionalInfoFileManager _requestAdditionalInfoFilesManager,


            IReviewFileStatusManager _reviewFileStatusManager,
            IWebSecurityWrapper webSec,
            IOPAManager _opaManager,
            IProd_RestfulWebApiDownload _WebApiDownload,
            IProd_RestfulWebApiTokenRequest _WebApiTokenRequest,
            IProd_RestfulWebApiUpdate _WebApiUpdate,
            IProd_RestfulWebApiDocumentUpload _WebApiDocumentUpload,
            IProd_RestfulWebApiReplace _WebApiDocumentReplace,
            IProd_RestfulWebApiDelete _WebApiDelete,
            ISubFolderStructureRepository _folderManager,
            IProd_GroupTasksManager _prod_GroupTasksManager,
            IAppProcessManager _AppProcessManager, ITaskFile_FolderMappingManager _taskFile_FolderMappingManager,//harish added
              IProd_NextStageManager _prod_NextStageManager, IPAMReportManager plmReportMgr, IProd_RestfulWebApiUpdate _prodRestfulWebApiUpdate, IProd_TaskXrefManager _prodTaskXrefManager
            , IProductionQueueManager _productionQueueManager, ILookupManager _lookupMgr,
             FHANumberRequestManager _fhaRequestManager, IProd_SharepointScreenManager _sharepointScreenManager)
        {
            projectActionFormManager = _projectActionFormManager;//harish added
            taskFile_FolderMappingManager = _taskFile_FolderMappingManager;//harish added
            taskManager = _taskManager;
            accountManager = _accountManager;
            emailManager = _emailManager;
            backgroundJobManager = backgroundManager;
            projectActionManager = _projectActionManager;
            groupTaskManager = _groupTaskManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager; // User Story 1901
            parentChildTaskManager = _parentChildTaskManager;
            requestAdditionalInfoFilesManager = _requestAdditionalInfoFilesManager;
            reviewFileStatusManager = _reviewFileStatusManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;// User Story 1901
            webSecurity = webSec;
            iOPAForm = _opaManager;
            WebApiDownload = _WebApiDownload;
            WebApiTokenRequest = _WebApiTokenRequest;
            WebApiUpdate = _WebApiUpdate;
            WebApiDelete = _WebApiDelete;
            WebApiDocumentUpload = _WebApiDocumentUpload;
            WebApiDocumentReplace = _WebApiDocumentReplace;
            folderManager = _folderManager;
            prod_GroupTasksManager = _prod_GroupTasksManager;
            appProcessManager = _AppProcessManager;
            prod_NextStageManager = _prod_NextStageManager;
            _plmReportMgr = plmReportMgr;
            prodRestfulWebApiUpdate = _prodRestfulWebApiUpdate;
            prod_TaskXrefManager = _prodTaskXrefManager;
            productionQueue = _productionQueueManager;
            lookupMgr = _lookupMgr;
            fhaRequestManager = _fhaRequestManager;
            sharepointScreenManager = _sharepointScreenManager;
        }



        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        // [MvcSiteMapNode(Title = "Project Action Request Form", ParentKey = "AMId", PreservedRouteParameters = "", Key = "OPAkey")]
        [MvcSiteMapNode(Title = "Project Action Request Form", ParentKey = "AMId")]
        [SiteMapTitle("PAMReport", Target = AttributeTarget.CurrentNode)]
        public ActionResult Index()
        {
            OPAViewModel OPAprojectActionViewModel = new OPAViewModel();
            InitializeViewModel(OPAprojectActionViewModel);
            //OPAWorkProduct(OPAprojectActionViewModel.PropertyWorkProduct.TaskInstanceId);
            return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAprojectActionViewModel);
        }

        // to attach a form for AE
        public void OPAWorkProduct(Guid taskInstanceId, string fileType)
        {
            //List<OPAWorkProductModel> OPAprojectActionViewModel = iOPAForm.getAllOPAWorkProducts(taskInstanceId, fileType);
            List<OPAWorkProductModel> OPAprojectActionViewModel = iOPAForm.getAllOPAWorkProducts(taskInstanceId, fileType);
            //return PartialView("~/Views/OPAForm/_OPAUploadForAe.cshtml", OPAprojectActionViewModel);
        }

        public JsonResult GetOPAWorkProduct(string sidx, string sord, int page, int rows, Guid taskInstanceId, string fileType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var opaWorkProduct = iOPAForm.getAllOPAWorkProducts(taskInstanceId, fileType);

            //Setting the date based on the Time Zone
            if (opaWorkProduct != null)
            {
                foreach (var item in opaWorkProduct)
                {
                    //item.UploadTime = TimezoneManager.ToMyTimeFromUtc(item.UploadTime);
                    item.UploadTime = item.UploadTime;

                }
            }
            if (opaWorkProduct != null)
            {
                int totalrecods = opaWorkProduct.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                var result = opaWorkProduct.ToList().OrderBy(s => s.FileId);
                var results = result.Skip(pageIndex * pageSize).Take(pageSize);

                var jsonData = new
                {
                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = results,
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = opaWorkProduct,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult OPADeleteGroupTaskFile(string taskFileId, string taskInstanceID,
            OPAViewModel OPAProjectActionViewModel, string IsFromGroupTask, string fhaNumber, string ServicerComments,
            string addressschanged)
        {
            string GroupTaskId = string.Empty;
            GroupTaskId = Request.QueryString[0];
            // OPAProjectActionViewModel.FhaNumber = fhaNumber;
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskFileId));
            var OPAModel = new OPAViewModel();

            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");
                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFileId));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            OPAModel = groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(GroupTaskId));
            OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAModel.ProjectActionTypeId);
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
            foreach (var model in projectActionList)
            {
                OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                {
                    Text = model.ProjectActionName,
                    Value = model.ProjectActionID.ToString()
                });
            }

            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);

            var result = projectActionFormManager.GetPropertyInfo(fhaNumber);
            if (result != null)
            {
                OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                OPAModel.PropertyAddress.City = result.City ?? "";
                OPAModel.PropertyAddress.StateCode = result.State ?? "";
                OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

            }

            var IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];

            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsViewFromGroupTask = true;
            }
            else
            {
                OPAModel.IsViewFromGroupTask = false;
            }

            if (addressschanged.ToUpper() == "TRUE")
            {
                OPAModel.IsAddressChange = true;
            }
            else
            {
                OPAModel.IsAddressChange = false;
            }
            OPAModel.ServicerComments = ServicerComments;
            GetDisclaimerText(OPAModel); // User Story 1901
            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsAgreementAccepted = true; // User Story 1901
                return PartialView("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", OPAModel);
            }
            OPAModel.IsAgreementAccepted = false; // User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
        }

        [HttpPost]
        public ActionResult DeleteGroupTaskFile(string taskFileId, string taskInstanceID,
            OPAViewModel OPAProjectActionViewModel, string IsFromGroupTask, string fhaNumber, string ServicerComments,
            string addressschanged)
        {
            string GroupTaskId = string.Empty;
            GroupTaskId = Request.QueryString[0];
            // OPAProjectActionViewModel.FhaNumber = fhaNumber;
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskFileId));
            var OPAModel = new OPAViewModel();

            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");
                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFileId));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            OPAModel = groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(GroupTaskId));
            OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAModel.ProjectActionTypeId);
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
            foreach (var model in projectActionList)
            {
                OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                {
                    Text = model.ProjectActionName,
                    Value = model.ProjectActionID.ToString()
                });
            }

            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);

            var result = projectActionFormManager.GetPropertyInfo(fhaNumber);
            if (result != null)
            {
                OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                OPAModel.PropertyAddress.City = result.City ?? "";
                OPAModel.PropertyAddress.StateCode = result.State ?? "";
                OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

            }

            var IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];

            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsViewFromGroupTask = true;
            }
            else
            {
                OPAModel.IsViewFromGroupTask = false;
            }

            if (addressschanged.ToUpper() == "TRUE")
            {
                OPAModel.IsAddressChange = true;
            }
            else
            {
                OPAModel.IsAddressChange = false;
            }
            OPAModel.ServicerComments = ServicerComments;
            GetDisclaimerText(OPAModel); // User Story 1901
            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsAgreementAccepted = true; // User Story 1901
                return PartialView("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", OPAModel);
            }
            OPAModel.IsAgreementAccepted = false; // User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
        }

        [HttpPost]
        public ActionResult Facade(string submitButton, OPAViewModel OPAprojectActionViewModel)
        {
            var temp = Request.QueryString[0];
            string[] str = temp.Split(',');

            var grouptaskid = str[0];

            if (!string.IsNullOrEmpty(grouptaskid))
            {
                OPAprojectActionViewModel.GroupTaskId = Convert.ToInt32(grouptaskid);
            }

            submitButton = str[1];
            switch (submitButton)
            {
                case "Submit":
                    // delegate sending to another controller action
                    return (Submit(OPAprojectActionViewModel));
                case "Save":
                    // call another action to perform the Save
                    return Save(OPAprojectActionViewModel);
                case @"Save and Check-in":
                    // call another action to perform the cancellation
                    return (SaveCheckin(OPAprojectActionViewModel));

                default:
                    // If they've submitted the form without a submitButton, 
                    // just return the view again.
                    return (View());
            }
        }

        private ActionResult Submit1(OPAViewModel model)
        {
            var OPAModel = new OPAViewModel();
            string GroupTaskTaskInstanceId = string.Empty;
            string FHA = string.Empty;
            string disclaimer = string.Empty;

            string requestDate = string.Empty;
            GroupTaskTaskInstanceId = Request.Form["GroupTaskInstanceId"];
            requestDate = Request.Form["requestDate"];
            disclaimer = Request.Form["DisclaimerText"];
            var ServicerComments = Request.Form["ServicerComments"];
            var addressschanged = Request.Form["IsAddressChange"];

            FHA = Request.Form["ddlFHANumbers"];
            //model.PropertyId = projectActionFormManager.GetPropertyInfo(FHA).PropertyId;
            if (disclaimer.ToUpper() == "TRUE")
            {
                model.IsAgreementAccepted = true;
            }
            else
            {
                model.IsAgreementAccepted = false;
            }

            if (addressschanged.ToUpper() == "FALSE")
            {
                model.IsAddressChange = false;
            }
            else
            {
                model.IsAddressChange = true;
            }
            if (string.IsNullOrEmpty(GroupTaskTaskInstanceId))
            {
                if (ModelState.IsValid)
                {
                    OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(model.ProjectActionTypeId);
                    OPAModel.ServicerComments = model.ServicerComments;
                    OPAModel.PropertyName = model.PropertyName;
                    OPAModel.RequesterName = model.RequesterName;
                    OPAModel.ProjectActionTypeId = model.ProjectActionTypeId;
                    //  var  FHA = Request.Form["FHA"];
                    OPAModel.IsAddressChange = model.IsAddressChange;
                    //model.FhaNumber = FHA;
                    //Insert the non critical request and get the generated id, insert into view model
                    OPAModel.FhaNumber = FHA;
                    OPAModel.ServicerSubmissionDate = DateTime.UtcNow;
                    OPAModel.RequestStatus = (int)RequestStatus.Submit;
                    OPAModel.CreatedOn = DateTime.UtcNow;
                    OPAModel.ModifiedOn = DateTime.UtcNow;
                    OPAModel.CreatedBy = UserPrincipal.Current.UserId;
                    OPAModel.ModifiedBy = UserPrincipal.Current.UserId;
                    OPAModel.ProjectActionDate = DateTime.UtcNow;
                    OPAModel.RequestDate = Convert.ToDateTime(requestDate);
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAModel);
                    OPAModel.ProjectActionFormId = projectActionFormId;
                    OPAModel.SequenceId = 0;
                    OPAModel.IsAgreementAccepted = model.IsAgreementAccepted;
                    OPAModel.RequesterName = model.RequesterName;
                    var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                    if (result != null)
                    {
                        OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                        OPAModel.PropertyAddress.City = result.City ?? "";
                        OPAModel.PropertyAddress.StateCode = result.State ?? "";
                        OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

                    }
                    var taskList = new List<TaskModel>();
                    var task = new TaskModel();
                    var InstanceId = Guid.NewGuid();
                    task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(OPAModel.FhaNumber);
                    OPAModel.TaskInstanceId = InstanceId;
                    task.AssignedBy = UserPrincipal.Current.UserName;

                    task.Notes = ServicerComments ?? string.Empty;
                    task.SequenceId = 0;
                    task.StartTime = DateTime.UtcNow;
                    //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = InstanceId;

                    var tempmodel = new OPAViewModel();
                    if (model.GroupTaskId != null)
                    {
                        tempmodel =
                            groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(model.GroupTaskId));
                        task.TaskInstanceId = tempmodel.GroupTaskInstanceId.Value;
                        tempmodel.RequestStatus = (int)RequestStatus.Submit;
                        OPAModel.TaskInstanceId = tempmodel.GroupTaskInstanceId.Value;
                    }
                    task.DataStore1 = XmlHelper.Serialize(OPAModel, typeof(OPAViewModel), Encoding.Unicode);
                    //adding fhanumber and pagetype 
                    task.FHANumber = OPAModel.FhaNumber;
                    task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");

                    task.TaskStepId = (int)TaskStep.ProjectActionRequest;
                    taskList.Add(task);
                    var taskFile = new TaskFileModel();
                    try
                    {
                        taskManager.SaveTask(taskList, taskFile);
                        //Latest task id been updated on submission
                        OPAModel.TaskInstanceId = task.TaskInstanceId;
                        projectActionFormManager.UpdateProjectActionRequestForm(OPAModel);
                        OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(OPAModel);

                        // Save ReviewFileStatus
                        string assignedto = task.AssignedTo;
                        int userId = webSecurity.GetUserId(assignedto);
                        Guid taskInstanceID = task.TaskInstanceId;
                        int reviewerUserId = userId;
                        int currentUserId = UserPrincipal.Current.UserId;
                        //int IsOPAUpload = 1;
                        //int ISRAIUpload = 0;
                        //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        taskManager.SaveOPAReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        // taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);

                        if (tempmodel != null)
                            groupTaskManager.UpdateOPAGroupTaskByModel(tempmodel);
                        // projectActionFormManager.InsertAEChecklistStatus(model);

                        if (OPAModel.ProjectActionName.Equals("TPA (Light) - Preliminary approval "))
                        {
                            backgroundJobManager.SendTPALightPreliminarySubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }
                        else
                        {

                            backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }

                        if (addressschanged.ToUpper() == "FALSE")
                        {
                            backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager,
                                "The property address is not listed correctly",
                                      OPAModel.PropertyName, OPAModel.FhaNumber);
                        }
                        UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                        string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);
                        return RedirectToAction("MyTasks", "Task");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            else
            {
                var IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];
                if (ModelState.IsValid || IsViewFromGroupTask.ToUpper().Contains("TRUE"))
                {

                    OPAModel =
                        groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(model.GroupTaskId));
                    OPAModel.ProjectActionName =
                        projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAModel.ProjectActionTypeId);
                    var taskFileModel =
                        taskManager.GetAllTaskFileByTaskInstanceId(new Guid(OPAModel.GroupTaskInstanceId.ToString()));
                    OPAModel.TaskFileList = taskFileModel.ToList();
                    OPAModel.ServicerComments = model.ServicerComments;
                    OPAModel.RequesterName = model.RequesterName;

                    OPAModel.IsAddressChange = model.IsAddressChange;
                    //  var  FHA = Request.Form["FHA"];

                    //model.FhaNumber = FHA;
                    //Insert the non critical request and get the generated id, insert into view model
                    OPAModel.ServicerSubmissionDate = DateTime.UtcNow;
                    OPAModel.RequestStatus = (int)RequestStatus.Submit;
                    OPAModel.CreatedOn = DateTime.UtcNow;
                    OPAModel.ModifiedOn = DateTime.UtcNow;
                    OPAModel.CreatedBy = UserPrincipal.Current.UserId;
                    OPAModel.ModifiedBy = UserPrincipal.Current.UserId;
                    OPAModel.ProjectActionDate = DateTime.UtcNow;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAModel);
                    OPAModel.ProjectActionFormId = projectActionFormId;
                    groupTaskManager.UpdateOPAGroupTask(OPAModel);
                    var taskList = new List<TaskModel>();
                    var task = new TaskModel();
                    OPAModel.SequenceId = 0;
                    OPAModel.IsAgreementAccepted = model.IsAgreementAccepted;
                    var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                    if (result != null)
                    {
                        OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                        OPAModel.PropertyAddress.City = result.City ?? "";
                        OPAModel.PropertyAddress.StateCode = result.State ?? "";
                        OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

                    }

                    task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(OPAModel.FhaNumber);
                    OPAModel.TaskInstanceId = OPAModel.GroupTaskInstanceId.Value;
                    task.AssignedBy = UserPrincipal.Current.UserName;
                    task.DataStore1 = XmlHelper.Serialize(OPAModel, typeof(OPAViewModel), Encoding.Unicode);
                    task.Notes = ServicerComments ?? string.Empty;
                    task.SequenceId = 0;
                    task.StartTime = DateTime.UtcNow;
                    //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = OPAModel.GroupTaskInstanceId.Value;
                    //adding fhanumber and pagetype 
                    task.FHANumber = OPAModel.FhaNumber;
                    task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                    task.TaskStepId = (int)TaskStep.ProjectActionRequest;
                    taskList.Add(task);
                    var taskFile = new TaskFileModel();
                    try
                    {
                        taskManager.SaveTask(taskList, taskFile);
                        //Latest task id been updated on submission
                        OPAModel.TaskInstanceId = task.TaskInstanceId;
                        projectActionFormManager.UpdateProjectActionRequestForm(OPAModel);
                        OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(OPAModel);
                        //Review Filestatus Saving
                        string assignedto = task.AssignedTo;
                        int userId = webSecurity.GetUserId(assignedto);
                        Guid taskInstanceID = task.TaskInstanceId;
                        int reviewerUserId = userId;
                        int currentUserId = UserPrincipal.Current.UserId;

                        //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        taskManager.SaveOPAReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        // projectActionFormManager.InsertAEChecklistStatus(model);
                        //*************** Place a condition here for the TPA (light)  ********************

                        if (OPAModel.ProjectActionName.Equals("TPA (Light) - Preliminary approval "))
                        {
                            backgroundJobManager.SendTPALightPreliminarySubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }
                        else
                        {

                            backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }
                        //Send Mail on address Change
                        if (addressschanged.ToUpper() == "FALSE")
                        {
                            backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager,
                                "The property address is not listed correctly",
                                      OPAModel.PropertyName, OPAModel.FhaNumber);
                        }
                        UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                        string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);
                        return RedirectToAction("MyTasks", "Task");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);
            OPAModel.IsAddressChange = model.IsAddressChange;
            return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
        }

        [HttpPost]
        public ViewResult SubmitPopup()
        {
            TempData["RequesStatus"] = "Save";
            return View("~/Views/OPAForm/ConfirmPopup.cshtml");
        }

        private ActionResult Save(OPAViewModel OPAprojectActionViewModel)
        {
            //string disclaimer = string.Empty;
            //disclaimer = Request.Form["disclaimer"];
            // User Story 1901
            //if (disclaimer.ToUpper() == "TRUE")
            //{
            //    OPAprojectActionViewModel.IsAgreementAccepted = true;
            //}
            //else
            //{
            //    OPAprojectActionViewModel.IsAgreementAccepted = false;
            //}

            //var addressschanged = string.Empty;
            //addressschanged = Request.Form["addressschanged"];
            //if (addressschanged.ToUpper() == "TRUE")
            //{
            //    OPAprojectActionViewModel.IsAddressChange = true;
            //}
            //else
            //{
            //    OPAprojectActionViewModel.IsAddressChange = false;
            //}
            var addresschange = OPAprojectActionViewModel.IsAddressChange.ToString();
            if (addresschange == "False")
            {
                OPAprojectActionViewModel.IsAddressChange = true;
            }
            // var OPAModel = new OPAViewModel();
            //var TaskInstanceId = ((OPAViewModel)TempData.Peek("OPAFormData")).GroupTaskInstanceId;
            //AppProcessModel.TaskGuid = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskGuid;
            var taskinstanceid = Request.Form["GroupTaskInstanceId"];

            //Added by harish for group task @20032020
            if (OPAprojectActionViewModel.GroupTaskId == null)
            {
                var grouptaskid = groupTaskManager.GroupTaskbyTaskinstanceid(new Guid(taskinstanceid));
                if (grouptaskid != null)
                {
                    OPAprojectActionViewModel.GroupTaskId = grouptaskid.TaskId;
                }


            }

            // var TaskInstanceid = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;
            //string GroupTaskTaskInstanceId = string.Empty;
            //GroupTaskTaskInstanceId = Request.Form["GroupTaskInstanceId"];

            if (OPAprojectActionViewModel.GroupTaskId == null)
            {
                //var FHA = Request.Form["FHA"];
                OPAprojectActionViewModel.FhaNumber = OPAprojectActionViewModel.FhaNumber;
                var groupTaskModel = new GroupTaskModel();
                // harish commented
                //groupTaskModel.TaskInstanceId = Guid.NewGuid();
                groupTaskModel.TaskInstanceId = new Guid(taskinstanceid);
                //groupTaskModel.TaskInstanceId = OPAprojectActionViewModel.TaskInstanceId;
                //groupTaskModel.TaskInstanceId = new Guid( taskinstanceid);
                groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskModel.InUse = UserPrincipal.Current.UserId;
                groupTaskModel.FhaNumber = OPAprojectActionViewModel.FhaNumber;
                groupTaskModel.PropertyName = OPAprojectActionViewModel.PropertyName;
                groupTaskModel.RequestDate = OPAprojectActionViewModel.RequestDate;
                groupTaskModel.RequesterName = OPAprojectActionViewModel.RequesterName;
                groupTaskModel.ServicerSubmissionDate = OPAprojectActionViewModel.ServicerSubmissionDate;
                groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                groupTaskModel.ProjectActionTypeId = OPAprojectActionViewModel.ProjectActionTypeId;
                groupTaskModel.IsDisclimerAccepted = OPAprojectActionViewModel.IsAgreementAccepted;
                groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                groupTaskModel.CreatedOn = DateTime.UtcNow;
                groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                groupTaskModel.ModifiedOn = DateTime.UtcNow;
                groupTaskModel.IsAddressChanged = OPAprojectActionViewModel.IsAddressChange;
                groupTaskModel.ServicerComments = OPAprojectActionViewModel.ServicerComments;
                OPAprojectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                OPAprojectActionViewModel.IsEditMode = true;
                OPAprojectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskModel.TaskId = (int)OPAprojectActionViewModel.GroupTaskId;
                OPAprojectActionViewModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                OPAprojectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                OPAprojectActionViewModel.ProjectActionDate = groupTaskModel.ProjectActionStartDate.Value;
            }
            else
            {
                // UploadFile(OPAprojectActionViewModel);
                groupTaskManager.LockGroupTask((int)OPAprojectActionViewModel.GroupTaskId);
                OPAprojectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                OPAprojectActionViewModel.TaskInstanceId = new Guid(taskinstanceid);
                //groupTaskManager.UpdateOPAGroupTask(OPAprojectActionViewModel);
                groupTaskManager.UpdateOPAGroupTaskByModel(OPAprojectActionViewModel);
                //PopupHelper.ConfigPopup(TempData, 720, 350, "Save", false, false, true, false, false, true,
                //            "../OPAForm/ConfirmPopup");

                PopupHelper.ConfigPopup(TempData, 520, 200, "Disclaimer", false, false, true, false, false, true,
                               "../NonCriticalRequestExtension/ConfirmPopup");



            }

            //var OPAModel = new OPAProjectActionViewModel();

            //OPAModel = groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(OPAprojectActionViewModel.GroupTaskId));
            //OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionName(OPAprojectActionViewModel.ProjectActionTypeId);
            //var taskFileModel = taskManager.GetAllTaskFileByTaskInstanceId(new Guid(OPAModel.GroupTaskInstanceId.ToString()));
            //OPAModel.TaskFileList = taskFileModel.ToList();

            //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActions();
            //foreach (var model in projectActionList)
            //{
            //    OPAModel.ProjectActionTypeList.Add(new SelectListItem() { Text = model.ProjectActionName, Value = model.ProjectActionID.ToString() });
            //}


            return RedirectToAction("Index", "GroupTask");
            // return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
        }

        private ActionResult SaveCheckin(OPAViewModel OPAprojectActionViewModel)
        {
            string disclaimer = string.Empty;
            disclaimer = Request.Form["disclaimer"];
            var taskinstanceid = Request.Form["GroupTaskInstanceId"];
            var FHA = Request.Form["FHA"];
            //User Story 1901
            //if (disclaimer.ToUpper() == "TRUE")
            //{
            //    OPAprojectActionViewModel.IsAgreementAccepted = true;
            //}
            //else
            //{
            //    OPAprojectActionViewModel.IsAgreementAccepted = false;
            //}


            //var addressschanged = string.Empty;

            //addressschanged = Request.Form["addressschanged"];

            //if (addressschanged.ToUpper() == "TRUE")
            //{
            //    OPAprojectActionViewModel.IsAddressChange = true;
            //}
            //else
            //{
            //    OPAprojectActionViewModel.IsAddressChange = false;
            //}
            var addresschange = OPAprojectActionViewModel.IsAddressChange.ToString();
            if (addresschange == "False")
            {
                OPAprojectActionViewModel.IsAddressChange = true;
            }
            //Added by harish for group task @20032020
            if (OPAprojectActionViewModel.GroupTaskId == null)
            {
                var grouptaskid = groupTaskManager.GroupTaskbyTaskinstanceid(new Guid(taskinstanceid));
                if (grouptaskid != null)
                {
                    OPAprojectActionViewModel.GroupTaskId = grouptaskid.TaskId;
                }


            }

            if (OPAprojectActionViewModel.GroupTaskId == null)
            {


                //OPAprojectActionViewModel.FhaNumber = FHA;
                var groupTaskModel = new GroupTaskModel();
                // harish commented below line to save same taskinstanceid 28-12-2019

                //groupTaskModel.TaskInstanceId = Guid.NewGuid();
                groupTaskModel.TaskInstanceId = OPAprojectActionViewModel.TaskInstanceId;
                groupTaskModel.TaskInstanceId = new Guid(taskinstanceid);
                groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskModel.InUse = 0;
                groupTaskModel.FhaNumber = OPAprojectActionViewModel.FhaNumber;
                groupTaskModel.PropertyName = OPAprojectActionViewModel.PropertyName;
                groupTaskModel.RequestDate = OPAprojectActionViewModel.RequestDate;
                groupTaskModel.RequesterName = OPAprojectActionViewModel.RequesterName;
                groupTaskModel.ServicerSubmissionDate = OPAprojectActionViewModel.ServicerSubmissionDate;
                groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                groupTaskModel.ProjectActionTypeId = OPAprojectActionViewModel.ProjectActionTypeId;
                groupTaskModel.IsDisclimerAccepted = OPAprojectActionViewModel.IsAgreementAccepted;
                groupTaskModel.IsAddressChanged = OPAprojectActionViewModel.IsAddressChange;
                groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                groupTaskModel.CreatedOn = DateTime.UtcNow;
                groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                groupTaskModel.ModifiedOn = DateTime.UtcNow;
                groupTaskModel.ServicerComments = OPAprojectActionViewModel.ServicerComments;
                OPAprojectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                OPAprojectActionViewModel.IsEditMode = true;
                OPAprojectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                groupTaskModel.TaskId = (int)OPAprojectActionViewModel.GroupTaskId;
                OPAprojectActionViewModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                OPAprojectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                OPAprojectActionViewModel.ProjectActionDate = groupTaskModel.ProjectActionStartDate.Value;
            }
            else
            {
                // harish commented below line because we are uploading files in Transacess ,so we are not uploading in database
                // UploadFile(OPAprojectActionViewModel);
                groupTaskManager.UnlockGroupTask((int)OPAprojectActionViewModel.GroupTaskId);
                OPAprojectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                // groupTaskManager.UpdateOPAGroupTask(OPAprojectActionViewModel);
                OPAprojectActionViewModel.TaskInstanceId = new Guid(taskinstanceid);
                groupTaskManager.UpdateOPAGroupTaskByModel(OPAprojectActionViewModel);
                TempData["RequesStatus"] = "Checkin";
                PopupHelper.ConfigPopup(TempData, 520, 200, "Disclaimer", false, false, true, false, false, true,
                               "../NonCriticalRequestExtension/ConfirmPopup");
            }

            //var OPAModel = new OPAProjectActionViewModel();

            //OPAModel = groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(OPAprojectActionViewModel.GroupTaskId));
            //OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionName(OPAprojectActionViewModel.ProjectActionTypeId);
            //var taskFileModel = taskManager.GetAllTaskFileByTaskInstanceId(new Guid(OPAModel.GroupTaskInstanceId.ToString()));
            //OPAModel.TaskFileList = taskFileModel.ToList();

            //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActions();
            //foreach (var model in projectActionList)
            //{
            //    OPAModel.ProjectActionTypeList.Add(new SelectListItem() { Text = model.ProjectActionName, Value = model.ProjectActionID.ToString() });
            //}

            //return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
            PopupHelper.ConfigPopup(TempData, 720, 350, "Save", false, false, true, false, false, true,
                          "../OPAForm/ConfirmPopup");
            return RedirectToAction("Index", "GroupTask");
        }

        private void InitializeViewModel(OPAViewModel OPAprojectActionViewModel)
        {
            ViewBag.SaveCheckListFormURL = Url.Action("ApproveCheckListForm");
            PopulateFHANumberList(OPAprojectActionViewModel);
            OPAprojectActionViewModel.IsEditMode = false;
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3).OrderBy(a => a.ProjectActionName).ToList();
            foreach (var model in projectActionList)
            {
                if (model.ProjectActionName != "Other")
                {
                    OPAprojectActionViewModel.ProjectActionTypeList.Add(new SelectListItem()
                    {
                        Text = model.ProjectActionName,
                        Value = model.ProjectActionID.ToString()
                    });
                }
            }
            foreach (var model in projectActionList)
            {
                if (model.ProjectActionName == "Other")
                {
                    OPAprojectActionViewModel.ProjectActionTypeList.Add(new SelectListItem()
                    {
                        Text = model.ProjectActionName,
                        Value = model.ProjectActionID.ToString()
                    });
                }
            }
            OPAprojectActionViewModel.LenderId = UserPrincipal.Current.UserData.LenderId.Value;
            OPAprojectActionViewModel.LenderName =
                projectActionFormManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            OPAprojectActionViewModel.RequesterName = UserPrincipal.Current.FullName;
            OPAprojectActionViewModel.ProjectActionDate = DateTime.Today;
            OPAprojectActionViewModel.ServicerSubmissionDate = null;
            OPAprojectActionViewModel.IsAgreementAccepted = false;
            OPAprojectActionViewModel.RequestDate = DateTime.UtcNow;
            OPAprojectActionViewModel.CreatedBy = UserPrincipal.Current.UserId;
            OPAprojectActionViewModel.CreatedOn = DateTime.UtcNow;
            OPAprojectActionViewModel.ModifiedOn = DateTime.UtcNow;
            OPAprojectActionViewModel.ModifiedBy = UserPrincipal.Current.UserId;
            IList<TaskFileModel> TasFilelist = new List<TaskFileModel>();
            OPAprojectActionViewModel.TaskFileList = TasFilelist;
            OPAprojectActionViewModel.IsViewFromGroupTask = false;
            List<OPAHistoryViewModel> History = new List<OPAHistoryViewModel>();
            OPAprojectActionViewModel.OpaHistoryViewModel = History;
            OPAprojectActionViewModel.GroupTaskInstanceId = Guid.NewGuid();
            // User Story 1901
            GetDisclaimerText(OPAprojectActionViewModel);

        }

        private void PopulateFHANumberList(OPAViewModel OPAProjectActionViewModel)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                fhaNumberList =
                    projectActionFormManager.GetAllowedFhaByLenderUserId(UserPrincipal.Current.UserId);
            }

            OPAProjectActionViewModel.AvailableFHANumbersList = fhaNumberList;
        }

        public JsonResult GetPropertyInfo(string selectedFhaNumber)
        {
            var result = projectActionFormManager.GetPropertyInfo(selectedFhaNumber);
            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        //public ContactAddressViewModel GetContactAddressInfo(OPAProjectActionViewModel OPAProjectActionViewModel)
        //{
        //    return projectActionFormManager.GetContactAddressInfo(OPAProjectActionViewModel).ContactAddressViewModel;
        //}

        public string CheckIfProjectActionExistsForFhaNumber(int projectActionTypeId, string fhaNumber, Guid taskinstid)
        {
            ArrayList foldrstructure = new ArrayList();

            var taskFileInfo = taskManager.GetAllTaskFileByTaskInstanceId(taskinstid).ToList();
            var PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(fhaNumber).PropertyId;
            var ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(projectActionTypeId);
            if (taskFileInfo.Count() > 0)
            {
                RestfulWebApiUpdateModel[] obj1 = new RestfulWebApiUpdateModel[1];
                var folderNames = "/" + PropertyId + "/" + fhaNumber + "/" + "Asset Management /" + ProjectActionName + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy");
                foreach (var item in taskFileInfo)
                {
                    if (item.DocId != null)
                    {
                        item.FolderName = folderNames;
                        var docInfo = new RestfulWebApiUpdateModel()
                        {
                            docId = item.DocId,
                            documentType = item.DocTypeID,
                            propertyId = PropertyId.ToString(),
                            indexType = "1",
                            indexValue = fhaNumber,
                            documentStatus = "Active",
                            folderName = item.FolderName,
                            transactionId = projectActionTypeId.ToString()
                        };

                        obj1[0] = docInfo;
                        var tokenResult = WebApiTokenRequest.RequestToken();
                        var result1 = prodRestfulWebApiUpdate.UpdateFolderDocument(obj1, tokenResult.access_token);
                        if (result1 != null && result1.status.ToLower() == "updated successfully")
                        {
                            item.DocTypeID = item.DocTypeID;
                            string ext = Path.GetExtension(item.FileName);


                            taskManager.UpdateFolderinTaskfile(item);

                        }




                    }
                }



            }

            var result = groupTaskManager.GroupTaskbyFHAProjectAction(projectActionTypeId, fhaNumber);
            if (result != null)
            {
                return result.RequestStatus.ToString();
            }
            else
            {
                var OpaResult = projectActionFormManager.OPATaskbyFHAProjectAction(projectActionTypeId, fhaNumber);
                if (OpaResult != null)
                {
                    return OpaResult.RequestStatus.ToString();
                }
                else
                {
                    return "NA";
                }
            }
        }

        //[HttpPost]
        public virtual ActionResult UploadFile(OPAViewModel OPAProjectActionViewModel)
        {

            string disclaimer = string.Empty;
            disclaimer = Request.Form["disclaimer"];

            if (disclaimer.ToUpper() == "TRUE")
            {
                OPAProjectActionViewModel.IsAgreementAccepted = true;
            }
            else
            {
                OPAProjectActionViewModel.IsAgreementAccepted = false;
            }

            var addressschanged = string.Empty;

            addressschanged = Request.Form["addressschanged"];


            bool isUploaded = false;
            string message = "File upload failed";
            string groupTaskInstanceId = string.Empty;

            string FHA = string.Empty;
            string PropertyName = string.Empty;
            string requestDate = string.Empty;
            string RequesterName = string.Empty;
            string ServicerSubmissionDate = string.Empty;
            string GroupTaskId = string.Empty;
            string Mode = string.Empty;
            string ProjectActionTypeId = string.Empty;
            FHA = Request.Form["FHA"];
            PropertyName = Request.Form["PropertyName"];
            requestDate = Request.Form["requestDate"];
            RequesterName = Request.Form["RequesterName"];
            ServicerSubmissionDate = Request.Form["ServicerSubmissionDate"];
            GroupTaskId = Request.Form["GroupTaskId"];
            ProjectActionTypeId = Request.Form["ProjectActionTypeId"];
            var OPAModel = new OPAViewModel();
            string IsViewFromGroupTask = string.Empty;
            IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];



            //Populate OPAProjectActionViewModel

            OPAProjectActionViewModel.FhaNumber = FHA;
            //if (!string.IsNullOrEmpty(ProjectActionTypeId))
            //{
            //    OPAProjectActionViewModel.ProjectActionTypeId = Convert.ToInt32(ProjectActionTypeId);
            //}

            //OPAProjectActionViewModel.PropertyName = PropertyName;
            //OPAProjectActionViewModel.RequestDate = Convert.ToDateTime(requestDate);
            //OPAProjectActionViewModel.ServicerSubmissionDate = Convert.ToDateTime(ServicerSubmissionDate);
            //if(!string.IsNullOrEmpty(GroupTaskId))
            //{
            //    OPAProjectActionViewModel.GroupTaskId =  Convert.ToInt32(GroupTaskId);
            //}
            string data = Request.QueryString[0];
            string taskid = string.Empty;
            //taskid
            var arr = data.Split(',');
            if (arr.Length > 1)
            {
                taskid = arr[0];
            }
            else
            {
                taskid = data;
            }
            if (!string.IsNullOrEmpty(taskid))
            {
                OPAProjectActionViewModel.GroupTaskId = Convert.ToInt32(taskid);
            }

            if (ModelState.IsValid)
            {
                var groupTaskModel = new GroupTaskModel();
                if (OPAProjectActionViewModel != null)
                {

                    if (OPAProjectActionViewModel.GroupTaskId == null)
                    {
                        groupTaskModel.TaskInstanceId = Guid.NewGuid();
                        groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.FhaNumber = OPAProjectActionViewModel.FhaNumber;
                        groupTaskModel.PropertyName = OPAProjectActionViewModel.PropertyName;
                        groupTaskModel.RequestDate = OPAProjectActionViewModel.RequestDate;
                        groupTaskModel.RequesterName = OPAProjectActionViewModel.RequesterName;
                        groupTaskModel.ServicerSubmissionDate = OPAProjectActionViewModel.ServicerSubmissionDate;
                        groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                        groupTaskModel.ProjectActionTypeId = OPAProjectActionViewModel.ProjectActionTypeId;
                        groupTaskModel.IsDisclimerAccepted = OPAProjectActionViewModel.IsAgreementAccepted;
                        groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.CreatedOn = DateTime.UtcNow;
                        groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedOn = DateTime.UtcNow;
                        groupTaskModel.ServicerComments = OPAProjectActionViewModel.ServicerComments;
                        OPAProjectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                        OPAProjectActionViewModel.IsEditMode = true;
                        OPAProjectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.TaskId = (int)OPAProjectActionViewModel.GroupTaskId;
                        OPAProjectActionViewModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                        OPAProjectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                        OPAProjectActionViewModel.ProjectActionDate = groupTaskModel.ProjectActionStartDate.Value;
                    }
                    else
                    {
                        OPAModel =
                            groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
                                Convert.ToInt32(OPAProjectActionViewModel.GroupTaskId));
                        OPAModel.ServicerComments = OPAProjectActionViewModel.ServicerComments;
                        OPAModel.ServicerSubmissionDate = OPAProjectActionViewModel.ServicerSubmissionDate;
                        OPAModel.PropertyId = OPAProjectActionViewModel.PropertyId;
                        groupTaskManager.UpdateOPAGroupTask(OPAModel);

                        OPAProjectActionViewModel.GroupTaskInstanceId = OPAModel.GroupTaskInstanceId;
                        // OPAProjectActionViewModel.GroupTaskInstanceId =  new Guid(Request.QueryString[0]);
                        //OPAProjectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                    }
                    OPAProjectActionViewModel.ProjectActionName =
                        projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAProjectActionViewModel.ProjectActionTypeId);
                }
                //return View("~/Views/ProjectAction/ProjectActionUploadCheckList.cshtml", OPAProjectActionViewModel);
            }

            //From Group task

            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel =
                    groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
                        Convert.ToInt32(OPAProjectActionViewModel.GroupTaskId));
                OPAModel.ServicerComments = OPAProjectActionViewModel.ServicerComments;
                OPAModel.ServicerSubmissionDate = OPAProjectActionViewModel.ServicerSubmissionDate;

                groupTaskManager.UpdateOPAGroupTask(OPAModel);

                OPAProjectActionViewModel.ProjectActionName =
                    projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAModel.ProjectActionTypeId);

                OPAProjectActionViewModel.ProjectActionTypeId = OPAModel.ProjectActionTypeId;
                OPAProjectActionViewModel.GroupTaskInstanceId = OPAModel.GroupTaskInstanceId;
                OPAModel.IsAgreementAccepted = false; // User Story 1901

            }
            else
            {
                OPAModel.IsAgreementAccepted = true; // User Story 1901
            }
            Random randoms = new Random();
            foreach (string Filename in Request.Files)
            {
                HttpPostedFileBase myFile = Request.Files[Filename];
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;


                    var UniqueId = randoms.Next(0, 99999);
                    // Debug.WriteLine("My debug string here" + UniqueId);

                    var systemFileName = OPAProjectActionViewModel.FhaNumber + "_" +
                                        OPAProjectActionViewModel.ProjectActionTypeId + "_" +
                                        OPAProjectActionViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                        Path.GetExtension(myFile.FileName);
                    var tModel = taskManager.GetTaskFileByTaskFileName(systemFileName.ToString());
                    var taskModel = taskManager.GetTaskFileByTaskFileType(systemFileName.ToString());
                    while (taskModel != null || tModel != null)
                    {
                        UniqueId = randoms.Next(0, 99999);
                        taskModel = taskManager.GetTaskFileByTaskFileType(UniqueId.ToString());
                        //OPAProjectActionViewModel.GroupTaskInstanceId = Guid.NewGuid();

                    }




                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = true;
                            message = "File uploaded successfully!";
                        }
                        catch (Exception ex)
                        {
                            message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
                        DateTime.UtcNow.ToString(), systemFileName.ToString(),
                        new Guid(OPAProjectActionViewModel.GroupTaskInstanceId.ToString()));

                    //var taskFileModel = taskManager.GetGroupTaskFileByTaskInstanceAndFileTypeId(new Guid(groupTaskInstanceId), new Guid(chkListId));
                    //if (taskFileModel != null)
                    //{
                    //    taskManager.DeleteGroupTaskFile(new Guid(groupTaskInstanceId), new Guid(chkListId));
                    //}

                    taskManager.SaveGroupTaskFileModel(taskFile);

                    OPAModel =
                        groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
                            Convert.ToInt32(OPAProjectActionViewModel.GroupTaskId));
                    OPAModel.ProjectActionName =
                        projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAProjectActionViewModel.ProjectActionTypeId);
                    var taskFileModel =
                        taskManager.GetAllTaskFileByTaskInstanceId(
                            new Guid(OPAProjectActionViewModel.GroupTaskInstanceId.ToString()));
                    OPAModel.TaskFileList = taskFileModel.ToList();

                    IList<ProjectActionTypeViewModel> projectActionList =
                        projectActionManager.GetAllProjectActionsByPageId(3);
                    foreach (var model in projectActionList)
                    {
                        OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                        {
                            Text = model.ProjectActionName,
                            Value = model.ProjectActionID.ToString()
                        });
                    }

                }
            }

            var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
            if (result != null)
            {
                OPAModel.PropertyId = result.PropertyId;
                OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                OPAModel.PropertyAddress.City = result.City ?? "";
                OPAModel.PropertyAddress.StateCode = result.State ?? "";
                OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";
            }

            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);


            if (addressschanged.ToUpper() == "TRUE")
            {
                OPAModel.IsAddressChange = true;
            }
            else
            {
                OPAModel.IsAddressChange = false;
            }
            GetDisclaimerText(OPAModel); // User Story 1901
            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsViewFromGroupTask = true;

                return PartialView("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", OPAModel);
            }
            OPAModel.IsAgreementAccepted = false; // User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);

        }

        public JsonResult GetOpaHistoryGridContent(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            // List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();

            var GridContent = GetOpaHistory(taskInstanceId);
            ////if (taskInstanceId == null)
            ////{ return Json(""); }
            ////var GridContent = GetOpaHistory(taskInstanceId.Value);


            //Setting the date based on the Time Zone
            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    if (item.uploadDate != null)
                    {
                        //item.uploadDate = TimezoneManager.GetPreferredTimeFromUtc((DateTime)item.uploadDate);
                        item.uploadDate = (DateTime)item.uploadDate;
                    }
                    if (item.submitDate != null)
                    {
                        //item.submitDate = TimezoneManager.GetPreferredTimeFromUtc((DateTime)item.submitDate);
                        item.submitDate = (DateTime)item.submitDate;
                    }

                }

            }

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);


            var result = GridContent.ToList().OrderBy(s => s.fileId);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public string OPAuploadOpaFiles(OPAViewModel oPAWorkProductModel)
        {
            string data = Request.QueryString[0];
            string taskid = string.Empty;

            string pathForSaving = Server.MapPath("~/Uploads");
            foreach (string Filename in Request.Files)
            {
                HttpPostedFileBase myFile = Request.Files[Filename];
                //string fileName = Path.GetFileName(myFile.FileName);
                double fileSize = (myFile.ContentLength) / 1024;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    Random randoms = new Random();
                    var UniqueRandomId = randoms.Next(0, 99999);
                    string UniqueId;
                    if (oPAWorkProductModel.ButtonName.Equals("WP"))
                    {
                        UniqueId = "WP";
                    }
                    else
                    {
                        UniqueId = "FWP";
                    }

                    var systemFileName = UniqueId + "_" + Filename + "_" + UniqueRandomId + Path.GetExtension(myFile.FileName);

                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(oPAWorkProductModel.TaskInstanceId.ToString()));
                    taskManager.SaveGroupTaskFileModel(taskFile);

                }
            }
            if (oPAWorkProductModel.ButtonName.Equals("WP"))
            {
                return "Success";
            }
            else
            {
                return "Success FWP";
            }
        }


        // To upload the AE files to the TaskTable
        public string uploadOpaFiles(OPAViewModel oPAWorkProductModel)

        {
            OPAViewModel Model = projectActionFormManager.GetOPAByTaskInstanceId(oPAWorkProductModel.TaskInstanceId);
            Model.pageTypeId = oPAWorkProductModel.pageTypeId;
            var PageName = projectActionFormManager.FormNameBYPageTypeID(oPAWorkProductModel.pageTypeId);
            var result = projectActionFormManager.GetProdPropertyInfo(Model.FhaNumber);
            if (result != null)
            {
                oPAWorkProductModel.PropertyId = result.PropertyId;
                oPAWorkProductModel.FhaNumber = Model.FhaNumber;
            }

            string data = Request.QueryString[0];
            string taskid = string.Empty;
            var uploadmodel = new RestfulWebApiUploadModel()
            {
                propertyID = oPAWorkProductModel.PropertyId.ToString(),
                indexType = "1",
                indexValue = oPAWorkProductModel.FhaNumber,
                pdfConvertableValue = "false",
            };
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //karri#162
            //request = WebApiTokenRequest.RequestToken();
            bool uploadStatus = true;

            string pathForSaving = Server.MapPath("~/Uploads");
            foreach (string Filename in Request.Files)
            {

                HttpPostedFileBase myFile = Request.Files[Filename];
                //string fileName = Path.GetFileName(myFile.FileName);
                double fileSize = (myFile.ContentLength) / 1024;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    Random randoms = new Random();
                    var UniqueRandomId = randoms.Next(0, 99999);
                    string UniqueId;
                    if (oPAWorkProductModel.ButtonName.Equals("WP"))
                    {
                        UniqueId = "WP";
                    }
                    else
                    {
                        UniqueId = "FWP";
                        uploadmodel.folderNames = "Production/" + PageName + "/FirmCommitment";
                    }

                    var systemFileName = UniqueId + "_" + Filename + "_" + UniqueRandomId + Path.GetExtension(myFile.FileName);
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            //isUploaded = true;
                            //message = "File uploaded successfully!";
                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    //karri#162
                    request = WebApiTokenRequest.RequestToken();

                    WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "OPA");
                    System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {

                        var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(oPAWorkProductModel.TaskInstanceId.ToString()));
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        taskFile.DocTypeID = WebApiUploadResult.documentType;
                        taskManager.SaveGroupTaskFileModel(taskFile);
                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        uploadStatus = false;
                    }



                }
            }
            //List<OPAWorkProductModel> opaWorkProduct = new List<OPAWorkProductModel>();

            //opaWorkProduct = iOPAForm.getAllOPAWorkProducts(oPAWorkProductModel.TaskInstanceId);
            if (uploadStatus)
            {
                if (oPAWorkProductModel.ButtonName.Equals("WP"))
                {
                    return "Success";
                }
                else
                {
                    return "Success FWP";
                }
            }
            else
            {
                return "failed";
            }

        }

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetOPAFormDetail(OPAViewModel model)
        {
            if (model == null)
            {

                model = (OPAViewModel)TempData.Peek("OPAFormData");
            }
            bool hasTaskOpenByUser = TempData["hasTaskOpenByUser"] != null
                ? (bool)TempData["hasTaskOpenByUser"]
                : false;
            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
            model.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(model.ProjectActionTypeId);
            var propertyInfoModel = projectActionFormManager.GetPropertyInfo(model.FhaNumber);
            if (propertyInfoModel != null)
            {
                model.PropertyId = propertyInfoModel.PropertyId;
                model.PropertyAddress.AddressLine1 = propertyInfoModel.StreetAddress;
                model.PropertyAddress.City = propertyInfoModel.City;
                model.PropertyAddress.StateCode = propertyInfoModel.State;
                model.PropertyAddress.ZIP = propertyInfoModel.Zipcode;
            }
            model.IsRAI = false;
            if ((model.RequestStatus == 2 || model.RequestStatus == 5) && !hasTaskOpenByUser &&
                RoleManager.IsUserLenderRole(model.AssignedTo) && !model.IsPAMReport &&
                RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                var task = new TaskModel();
                var taskList = new List<TaskModel>();
                task.AssignedBy = model.AssignedBy;
                task.AssignedTo = model.AssignedTo;
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                task.TaskOpenStatus = XmlHelper.Serialize(model.TaskOpenStatus, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                task.IsReAssigned = model.IsReassigned;
                task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                //adding fhanumber and pagetype 
                task.FHANumber = model.FhaNumber;
                task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                var taskFile = new TaskFileModel();
                try
                {
                    task.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                    taskList.Add(task);
                    taskManager.SaveTask(taskList, taskFile);
                    if (!parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
                    {
                        model.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(model);
                    }

                }
                catch (Exception e)
                {
                    ErrorSignal.FromCurrentContext().Raise(e);
                    throw new InvalidOperationException(
                        string.Format("Error project action request form detail: {0}", e.InnerException),
                        e.InnerException);
                }
            }
            model.hasReviewComments = true;
            model.userName = UserPrincipal.Current.FullName;
            model.userRole = UserPrincipal.Current.UserRole;
            var isChild = false;
            if (parentChildTaskManager.IsParentTaskAvailable(model.ChildTaskInstanceId))
            {
                isChild = true;
                model.OpaHistoryViewModel =
                    projectActionFormManager.GetOpaHistoryForRequestAdditionalInfo(model.ChildTaskInstanceId);
                // harish added to set isaddresschange to true 06042020
                if (model.IsAddressChange == false)
                {
                    model.IsAddressChange = true;
                }
            }
            else
            {
                model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
            }



            //Setting the date based on the Time Zone
            if (model.OpaHistoryViewModel != null)
            {
                foreach (var item in model.OpaHistoryViewModel)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                }

            }


            model.GridVisibility = "ShowForLender";
            model.IsAgreementAccepted = true; // User Story 1901
            GetDisclaimerText(model); // User Story 1901          
            if (model.RequestStatus == 1 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) &&
                model.TaskStepId == 16)
            {
                model.IsAgreementAccepted = false; // User Story 1901
                return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
            }
            //harish added for new file request
            //harish added
            else if (model.RequestStatus == 1 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) &&
                 model.TaskStepId == 25)
            {
                model.IsAgreementAccepted = false; // User Story 1901
                return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
            }
            //harish added
            else if (model.RequestStatus == 6 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) &&
                model.TaskStepId == 16)
            {
                model.IsAgreementAccepted = false; // User Story 1901
                return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
            }
            // harish added for RAI New file request 11-02-2020
            else if (model.RequestStatus == 6 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) &&
                model.TaskStepId == 25)
            {
                model.IsAgreementAccepted = false; // User Story 1901
                return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
            }
            else if (model.RequestStatus == 1 && RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) &&
               model.TaskStepId == 16)
            {
                return View("~/Views/OPAForm/OPARequestAdditionalAEandReadOnly.cshtml", model);
            }
            else if (model.RequestStatus == 1 && model.TaskStepId == 15)
            {
                model.IsAgreementAccepted = true;

                return View("~/Views/OPAForm/OPARequestAdditionalAEandReadOnly.cshtml", model);
            }
            // harish added 07-01-2020 
            else if (model.RequestStatus == 1 && model.TaskStepId == 21)
            {
                //model.TaskStepId = 22;
                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
            }
            // harish added 09-01-2020 RIS
            else if (model.RequestStatus == 1 && model.TaskStepId == 20 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {

                model.RequestStatus = 1;
                model.IsRIS = true;

                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);



            }
            else if (model.RequestStatus == 1 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                model.GridVisibility = string.Empty;
                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
            }
            //#664 harish added this below line for Request Info from servicer 06-01-2020
            else if (model.RequestStatus == 6 && model.TaskStepId == 20 && !RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {

                model.RequestStatus = 1;
                model.IsRIS = false;

                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);



            }
            // harish added new line of code to show grid with RIS form
            //09-01-2020
            else if (model.RequestStatus == 2 && model.TaskStepId == 24 && !RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {

                model.RequestStatus = 1;
                model.TaskStepId = 20;
                model.IsRIS = false;

                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);



            }
            else if (model.RequestStatus == 6 && model.TaskStepId == 20 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {

                model.RequestStatus = 1;
                model.IsRIS = true;

                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);



            }


            else if (model.RequestStatus == 8 && model.TaskStepId == 20 || model.RequestStatus == 1 && model.TaskStepId == 20)
            {
                model.IsRAI = true;
                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
            }





            else if (model.RequestStatus == 2 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                if (isChild)
                {
                    return View("~/Views/OPAForm/OPARequestAdditionalAEandReadOnly.cshtml", model);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
                }
            }

            else if (model.RequestStatus == 5 && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
            }

            else if (RoleManager.IsReviewer(UserPrincipal.Current.UserName))
            {
                return View("~/Views/OPAForm/OPARequestReviewerandReadOnly.cshtml", model);
            }
            else
            {
                if (isChild)
                {

                    return View("~/Views/OPAForm/OPARequestAdditionalAEandReadOnly.cshtml", model);
                }
                else
                {
                    model.ShowVisibility = true;
                    return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", model);
                }
            }
        }

        public ActionResult ApproveOrDeny(OPAViewModel model, Guid TaskInstanceId, string showPopup, string TaskGuid, int requestStatus, string SequenceId,
                                                              string Concurrency, string AssignedBy, string FHANumber, Guid ProjectActionFormId, int ProjectActionTypeId,
                                                              string IschildTask, string showcomments, int PropertyId, bool isAddressChange, string propertyName, string requesterName,
                                                              DateTime servicerSubmissionDate, string projectActionName, bool isEditMode, string AEComments, string StreetAddress,
                                                              string City, string StateCode, string ZIP)
        {

            bool isChildApproved = true;
            model.ShowVisibility = true;
            if (ModelState.IsValid)
            {
                List<TaskModel> childTaskList = projectActionFormManager.GetChildTasks(model.TaskInstanceId);
                var childlist = childTaskList.Where(x => x.TaskStepId == 16 || x.TaskStepId == 15 || x.TaskStepId == 25).ToList();
                var childlist20AppType = childTaskList.Where(x => x.TaskStepId == 20).ToList();
                if (childlist.Count() > 0)
                {
                    if (showPopup != "No" && model.DenyStatus != "Yes" && model.DenyStatus == null)
                    {

                        foreach (var childList in childlist)
                        {

                            if (childList.TaskStepId == 16 || childList.TaskStepId == 15 || childList.TaskStepId == 25)
                            {
                                isChildApproved = false;
                                break;
                            }

                        }

                        if (isChildApproved == false)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            model.ShowVisibility = false;
                            if (showPopup != "No")
                            {
                                PopupHelper.ConfigPopup(TempData, 350, 450, "Confirmation", false, false, true, false, false, true,
                                    "../OPAForm/ApprovePopupForOPA");
                            }
                            return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", model);
                        }
                    }
                }
                if (childlist20AppType.Count() > 0)
                {

                    // harish added new line For RIS
                    if (showPopup != "No" && model.DenyStatus != "Yes" && model.DenyStatus == null)
                    {

                        foreach (var childList in childlist20AppType)
                        {

                            if (childList.TaskStepId == 20)
                            {
                                isChildApproved = false;
                                break;
                            }

                        }

                        if (isChildApproved == false)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            model.ShowVisibility = false;
                            if (showPopup != "No")
                            {
                                PopupHelper.ConfigPopup(TempData, 350, 450, "Confirmation", false, false, true, false, false, true,
                                    "../OPAForm/ApprovePopupForOPA");
                            }
                            return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", model);
                        }
                    }
                }



                if (model.RequestStatus != 5)
                {
                    model.RequestStatus = 2;
                }

                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;

                var taskList = new List<TaskModel>();
                var parentTask = new TaskModel();
                string assignedBy;
                string assignedTo;
                string alertTitle = String.Empty;
                var requestAdditionalInfoFilesList = new List<RequestAdditionalInfoFileModel>();
                model.GroupTaskInstanceId = model.TaskGuid;
                TempData["IsReAssigned"] = model.IsReassigned.HasValue && model.IsReassigned.Value == true
                    ? "true"
                    : "false";
                parentTask.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                if (showPopup == "No")
                {
                    model.RequestStatus = 2;
                    model.FhaNumber = FHANumber;
                    model.ProjectActionFormId = ProjectActionFormId;
                    model.ProjectActionTypeId = ProjectActionTypeId;
                }
                if (model.RequestStatus == (int)RequestStatus.RequestAdditionalInfo)
                {
                    TempData["PopupText"] = "Additional Request is Sent.";
                    alertTitle = "Additional Request Sent";
                    var childTask = new TaskModel();
                    childTask.TaskInstanceId = Guid.NewGuid();
                    childTask.SequenceId = 0;
                    childTask.AssignedBy = UserPrincipal.Current.UserName;
                    childTask.AssignedTo = model.AssignedBy;
                    childTask.StartTime = DateTime.UtcNow;
                    childTask.TaskStepId = (int)TaskStep.OPAAddtionalInformation;
                    //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                    childTask.MyStartTime = childTask.StartTime;
                    childTask.IsReAssigned = false;
                    childTask.Concurrency = model.Concurrency;
                    //adding fhanumber and pagetype      
                    childTask.FHANumber = model.FhaNumber;
                    childTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                    var childTaskOpenStatusModel = new TaskOpenStatusModel();
                    childTaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    childTask.TaskOpenStatus = XmlHelper.Serialize(childTaskOpenStatusModel,
                        typeof(TaskOpenStatusModel),
                        Encoding.Unicode);
                    taskList.Add(childTask);
                    model.RequestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                    model.RequestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                    model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskGuid;
                    model.RequestAdditionalChildTask.ChildTaskInstanceId = childTask.TaskInstanceId;
                    model.RequestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();
                    childTask.Notes = model.RequestAdditionalComment;
                    assignedBy = childTask.AssignedBy;
                    assignedTo = childTask.AssignedTo;
                    var taskFileIdList =
                        reviewFileStatusManager.GetRequestAdditionalFileIdByParentTaskInstanceId((Guid)model.TaskGuid);
                    foreach (var item in taskFileIdList)
                    {
                        var requestAdditionalInfoFiles = new RequestAdditionalInfoFileModel();
                        requestAdditionalInfoFiles.ChildTaskInstanceId = childTask.TaskInstanceId;
                        requestAdditionalInfoFiles.RequestAdditionalFileId = Guid.NewGuid();
                        requestAdditionalInfoFiles.TaskFileId = item;
                        requestAdditionalInfoFilesList.Add(requestAdditionalInfoFiles);
                    }

                    model.IsRAI = true;
                }
                else
                {
                    if (model.RequestStatus == (int)RequestStatus.Approve)
                    {
                        TempData["PopupText"] = "Project Action Request was approved.";
                        alertTitle = "Project Action Approved";
                        parentTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                    }
                    else
                    {
                        TempData["PopupText"] = "Project Action Request was denied.";
                        alertTitle = "Project Action Denied";
                        parentTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                    }
                    model.SequenceId++;
                    parentTask.SequenceId = (int)model.SequenceId;
                    parentTask.StartTime = DateTime.UtcNow;
                    //parentTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(parentTask.StartTime);
                    parentTask.MyStartTime = parentTask.StartTime;
                    parentTask.TaskInstanceId = model.TaskInstanceId;
                    parentTask.IsReAssigned = model.IsReassigned;
                    parentTask.Concurrency = model.Concurrency;
                    parentTask.AssignedTo = model.AssignedBy;
                    parentTask.AssignedBy = UserPrincipal.Current.UserName;
                    parentTask.Notes = model.AEComments;
                    //adding fhanumber and pagetype 
                    parentTask.FHANumber = model.FhaNumber;
                    parentTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                    var taskOpenStatusModel = new TaskOpenStatusModel();
                    taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    parentTask.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                        Encoding.Unicode);
                    taskList.Add(parentTask);
                    assignedBy = parentTask.AssignedBy;
                    assignedTo = parentTask.AssignedTo;
                }

                //model.Concurrency = concurrency;
                if (taskList != null && taskList.Any() && (model.IsRAI || model.IsRIS))
                {
                    taskList[0].DataStore1 = parentTask.DataStore1;
                }
                // harish changed childtasklist to childlist (09-01-2020)
                if (childlist != null && childlist.Count > 0 && !model.IsRAI)
                {
                    var childTask = new TaskModel();
                    var childtaskOpenStatusModel = new TaskOpenStatusModel();

                    foreach (TaskModel childTaskListItem in childlist)
                    {
                        childTask = new TaskModel();

                        childTask.TaskInstanceId = childTaskListItem.TaskInstanceId;
                        childTaskListItem.SequenceId++;
                        childTask.SequenceId = (int)childTaskListItem.SequenceId;
                        childTask.AssignedBy = UserPrincipal.Current.UserName;
                        childTask.AssignedTo = model.AssignedBy;
                        childTask.StartTime = DateTime.UtcNow;
                        //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                        childTask.MyStartTime = childTask.StartTime;
                        childTask.Notes = model.AEComments;
                        childTask.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                        childTask.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);//
                        childtaskOpenStatusModel = new TaskOpenStatusModel();
                        childtaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        childTask.TaskOpenStatus = XmlHelper.Serialize(childtaskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                        childTask.IsReAssigned = model.IsReassigned;
                        childTask.Concurrency = model.Concurrency;
                        childTask.FHANumber = model.FhaNumber;
                        childTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");

                        taskList.Add(childTask);
                    }
                }
                // harish added new method for RIS(09-01-2020)
                if (childlist20AppType != null && childlist20AppType.Count > 0 && !model.IsRIS)
                {
                    var childTask = new TaskModel();
                    var childtaskOpenStatusModel = new TaskOpenStatusModel();

                    foreach (TaskModel childTaskListItem in childlist20AppType)
                    {
                        childTask = new TaskModel();

                        childTask.TaskInstanceId = childTaskListItem.TaskInstanceId;
                        childTaskListItem.SequenceId++;
                        childTask.SequenceId = (int)childTaskListItem.SequenceId;
                        childTask.AssignedBy = UserPrincipal.Current.UserName;
                        childTask.AssignedTo = model.AssignedBy;
                        childTask.StartTime = DateTime.UtcNow;
                        //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                        childTask.MyStartTime = childTask.StartTime;
                        childTask.Notes = model.AEComments;
                        childTask.TaskStepId = (int)TaskStep.RISProjectActionRequestComplete;
                        childTask.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);//
                        childtaskOpenStatusModel = new TaskOpenStatusModel();
                        childtaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        childTask.TaskOpenStatus = XmlHelper.Serialize(childtaskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                        childTask.IsReAssigned = model.IsReassigned;
                        childTask.Concurrency = model.Concurrency;
                        childTask.FHANumber = model.FhaNumber;
                        childTask.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");

                        taskList.Add(childTask);
                    }
                }



                var taskFile = new List<TaskFileModel>();
                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    // harish added new Model.IsRIS in if condition (if(model.IsRAI) to if (model.IsRAI || model.IsRIS) 09-01-2020
                    if (model.IsRAI || model.IsRIS)
                    {
                        parentChildTaskManager.AddParentChildTask(model.RequestAdditionalChildTask);
                        requestAdditionalInfoFilesManager.SaveRequestAdditionalInfoFiles(requestAdditionalInfoFilesList);
                    }
                    else
                    {
                        model.MytaskId = projectActionFormManager.GetLatestTaskId(parentTask.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(model);
                        projectActionFormManager.UpdateProjectActionRequestForm(model);
                        // groupTaskManager.UpdateOPAGroupTask(model);
                        groupTaskManager.UpdateOPAGroupTaskByModel(model);
                    }

                    if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(emailManager, assignedTo,
                        assignedBy, model))
                    {
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                                true, "GenericPopup");
                            model.ProjectActionName =
                                projectActionFormManager.GetProjectActionNameforAssetmanagement(model.ProjectActionTypeId);
                            model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
                            model.IsAgreementAccepted = true; // User Story 1901
                            GetDisclaimerText(model); // User Story 1901
                            return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", model);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                }
            }
            return RedirectToAction("MyTasks", "Task");
        }

        [HttpPost]
        public ActionResult ApprovePopupForOPA(OPAViewModel model)
        {
            return PartialView("~/Views/OPAForm/ApprovePopupForOPA.cshtml", model);
        }
        /*
        public ActionResult LenderSubmit(OPAViewModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {
            var OPAModel = new OPAViewModel();
            if (ModelState.IsValid)
            {
                // var updatedconcurrency = taskManager.GetConcurrencyTimeStamp((Guid)model.TaskGuid);
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                var taskList = new List<TaskModel>();
                var task = new TaskModel();
                string alertTitle = String.Empty;
                //harish commented
                // task.AssignedTo = model.AssignedBy;
                task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(model.FhaNumber);
                task.AssignedBy = UserPrincipal.Current.UserName;
                model.RequestStatus = (int)RequestStatus.Submit;
                var taskguid = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskGuid;
                taskGuid = taskguid;
                TempData["PopupText"] = "Additional Information is Sent.";
                alertTitle = "Additional Information Sent";
                task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                model.Concurrency = concurrency;
                model.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                task.Notes = model.LenderComment;
                task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                task.IsReAssigned = model.IsReassigned;
                task.Concurrency = concurrency;
                //adding fhanumber and pagetype 
                task.FHANumber = model.FhaNumber;
                // harish added this logic to save in opa form
                model.ProjectActionName = projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
               // OPAModel.ServicerComments = model.ServicerComments;
               // OPAModel.PropertyName = model.PropertyName;
                //OPAModel.RequesterName = model.RequesterName;
               // OPAModel.ProjectActionTypeId = model.ProjectActionTypeId;
                //  var  FHA = Request.Form["FHA"];
                //OPAModel.IsAddressChange = model.IsAddressChange;
                //model.FhaNumber = FHA;
                //Insert the non critical request and get the generated id, insert into view model
                //OPAModel.FhaNumber = model.FhaNumber;
                model.ServicerSubmissionDate = DateTime.UtcNow;
                //OPAModel.RequestStatus = (int)RequestStatus.Submit;
               // OPAModel.CreatedOn = DateTime.UtcNow;
                //OPAModel.ModifiedOn = DateTime.UtcNow;
                model.CreatedBy = UserPrincipal.Current.UserId;
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ProjectActionDate = DateTime.UtcNow;
                model.RequestDate = Convert.ToDateTime(model.RequestDate);
                model.TaskInstanceId = task.TaskInstanceId;
                task.Status = FormUploadStatus.Done;
               // OPAModel.TaskInstanceId = model.TaskInstanceId;
                //OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;

                var result = projectActionFormManager.GetPropertyInfo(model.FhaNumber);
                if (result != null)
                {
                    model.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                    model.PropertyAddress.City = result.City ?? "";
                    model.PropertyAddress.StateCode = result.State ?? "";
                    model.PropertyAddress.ZIP = result.Zipcode ?? "";

                }

                task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                var taskOpenStatusModel = new TaskOpenStatusModel();
                taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(task);
                
                // Parentchild harish aded to get data from table
                model.RequestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                model.RequestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                //model.RequestAdditionalChildTask.ParentTaskInstanceId = task.TaskInstanceId;
                // model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskGuid;
                // model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskInstanceId;
                //model.RequestAdditionalChildTask.ChildTaskInstanceId = model.TaskInstanceId;
                model.RequestAdditionalChildTask.ChildTaskInstanceId = task.TaskInstanceId;
                model.RequestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();
               // OPAModel.RequestAdditionalChildTask.ParentChildTaskId = model.RequestAdditionalChildTask.ParentChildTaskId;
                model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskInstanceId;
               // OPAModel.RequestAdditionalChildTask.ParentTaskInstanceId =model.RequestAdditionalChildTask.ParentTaskInstanceId;






                var taskFile = new List<TaskFileModel>();

                // Update Status 
                // Guid ChildTaskInstanceId = model.ChildTaskInstanceId;
                Guid ChildTaskInstanceId = model.RequestAdditionalChildTask.ChildTaskInstanceId;
                var results = reviewFileStatusManager.GetTaskFileIdByChildTaskInstanceId(ChildTaskInstanceId);
                var resultset = results;
                foreach (var newresults in results)
                {
                    reviewFileStatusManager.UpdateReviewFileStatusForTaskFileId(newresults.TaskFileId);
                }

                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    model.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(model);
                    model.ProjectActionFormId = projectActionFormId;
                    // OPAModel.SequenceId = 0;
                    model.IsAgreementAccepted = model.IsAgreementAccepted;
                    //OPAModel.RequesterName = model.RequesterName;
                   // model.ServicerComments = model.LenderComment;
                    if (backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo, task.AssignedBy,
                        model))
                    {
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                                true, "GenericPopup");
                            //model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
                            model.ProjectActionName =
                                projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                            // return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {

                }
            }
            return RedirectToAction("MyTasks", "Task");
        }
        */
        //#664
        public ActionResult LenderSubmit(OPAViewModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {
            var OPAModel = new OPAViewModel();
            // harish added below code to show new file request whethe they changed or not ,if not we are showing popup ,please select file name 06042020
            model.TaskInstanceId = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;


            var FileList = (from filelist in taskManager.GetAllTaskFileByTaskinstanceidinsteadofTaskGuid(model.TaskInstanceId)
                            select new
                            {
                                filelist.FileId,
                                filelist.DocTypeID,
                                docId = filelist.DocId,
                                Name = filelist.FileName,
                                filelist.FolderStructureKey
                            }).Where(m => string.IsNullOrEmpty(m.DocTypeID));


            FileList = FileList.Where(x => x.FolderStructureKey != 40 && x.FolderStructureKey != 41).ToList();

            if (FileList.Count() > 0)
            {
                //if(AppProcessModel.pageTypeId==18)
                OPAModel = projectActionFormManager.GetOPAByTaskInstanceId(model.TaskInstanceId);
                //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
                //foreach (var pr in projectActionList)
                //{
                //    OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                //    {
                //        Text = pr.ProjectActionName,
                //        Value = pr.ProjectActionID.ToString()
                //    });
                //}
                OPAModel.IsChangeDocTypePopupDisplayed = true;

                OPAModel.IsAddressChange = model.IsAddressChange;
                var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                if (result != null)
                {
                    OPAModel.PropertyId = result.PropertyId;
                    OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                    OPAModel.PropertyAddress.StateCode = result.State;
                    OPAModel.PropertyAddress.City = result.City;
                    OPAModel.PropertyAddress.ZIP = result.Zipcode;
                }
                OPAModel.GroupTaskInstanceId = model.TaskInstanceId;
                //  AppProcessViewModel.ProjectActionTypeId;
                OPAModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(OPAModel.ProjectActionTypeId);
                GetDisclaimerText(OPAModel);
                PopulateFHANumberList(OPAModel);
                GetPropertyInfo(OPAModel.FhaNumber);
                TempData["AppProcessModel"] = OPAModel;
                if (OPAModel.pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", OPAModel);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", OPAModel);
                }
            }



            if (ModelState.IsValid)
            {
                // var updatedconcurrency = taskManager.GetConcurrencyTimeStamp((Guid)model.TaskGuid);
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                var taskList = new List<TaskModel>();
                var task = new TaskModel();
                string alertTitle = String.Empty;
                //harish commented
                // task.AssignedTo = model.AssignedBy;
                task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(model.FhaNumber);
                task.AssignedBy = UserPrincipal.Current.UserName;
                model.RequestStatus = (int)RequestStatus.Submit;
                var taskguid = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskGuid;
                taskGuid = taskguid;
                TempData["PopupText"] = "Additional Information is Sent.";
                alertTitle = "Additional Information Sent";
                task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                model.TaskGuid = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskGuid;
                model.SequenceId = ((OPAViewModel)TempData.Peek("OPAFormData")).SequenceId;
                model.SequenceId++;
                model.Concurrency = concurrency;
                //model.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                task.Notes = model.LenderComment;
                task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                task.SequenceId = model.SequenceId.Value;
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                task.IsReAssigned = model.IsReassigned;
                task.Concurrency = concurrency;
                //adding fhanumber and pagetype 
                task.FHANumber = model.FhaNumber;
                // harish added this logic to save in opa form
                OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(model.ProjectActionTypeId);
                OPAModel.ServicerComments = model.ServicerComments;
                OPAModel.PropertyName = model.PropertyName;
                OPAModel.RequesterName = model.RequesterName;
                OPAModel.ProjectActionTypeId = model.ProjectActionTypeId;
                //  var  FHA = Request.Form["FHA"];
                OPAModel.IsAddressChange = model.IsAddressChange;
                //model.FhaNumber = FHA;
                //Insert the non critical request and get the generated id, insert into view model
                OPAModel.FhaNumber = model.FhaNumber;
                OPAModel.ServicerSubmissionDate = DateTime.UtcNow;
                //OPAModel.RequestStatus = (int)RequestStatus.Submit;
                OPAModel.CreatedOn = DateTime.UtcNow;
                OPAModel.ModifiedOn = DateTime.UtcNow;
                OPAModel.CreatedBy = UserPrincipal.Current.UserId;
                OPAModel.ModifiedBy = UserPrincipal.Current.UserId;
                OPAModel.ProjectActionDate = DateTime.UtcNow;
                OPAModel.RequestDate = Convert.ToDateTime(model.RequestDate);
                OPAModel.TaskInstanceId = task.TaskInstanceId;
                // OPAModel.TaskInstanceId = model.TaskInstanceId;
                //OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;

                var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                if (result != null)
                {
                    OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                    OPAModel.PropertyAddress.City = result.City ?? "";
                    OPAModel.PropertyAddress.StateCode = result.State ?? "";
                    OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

                }


                task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                var taskOpenStatusModel = new TaskOpenStatusModel();
                taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(task);

                // Parentchild harish aded to get data from table
                model.RequestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                model.RequestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskGuid;
                // model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskInstanceId;
                //model.RequestAdditionalChildTask.ChildTaskInstanceId = model.TaskInstanceId;
                model.RequestAdditionalChildTask.ChildTaskInstanceId = task.TaskInstanceId;
                model.RequestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();
                OPAModel.RequestAdditionalChildTask.ParentChildTaskId = model.RequestAdditionalChildTask.ParentChildTaskId;
                model.RequestAdditionalChildTask.ParentTaskInstanceId = (Guid)model.TaskInstanceId;
                OPAModel.RequestAdditionalChildTask.ParentTaskInstanceId = model.RequestAdditionalChildTask.ParentTaskInstanceId;






                var taskFile = new List<TaskFileModel>();

                // Update Status 
                // Guid ChildTaskInstanceId = model.ChildTaskInstanceId;
                Guid ChildTaskInstanceId = model.RequestAdditionalChildTask.ChildTaskInstanceId;
                var results = reviewFileStatusManager.GetTaskFileIdByChildTaskInstanceId(ChildTaskInstanceId);
                var resultset = results;
                foreach (var newresults in results)
                {
                    reviewFileStatusManager.UpdateReviewFileStatusForTaskFileId(newresults.TaskFileId);
                }

                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAModel);
                    OPAModel.ProjectActionFormId = projectActionFormId;
                    // OPAModel.SequenceId = 0;
                    OPAModel.IsAgreementAccepted = model.IsAgreementAccepted;
                    //OPAModel.RequesterName = model.RequesterName;
                    // model.ServicerComments = model.LenderComment;
                    if (backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo, task.AssignedBy,
                        model))
                    {
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                                true, "GenericPopup");
                            //model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
                            model.ProjectActionName =
                                projectActionFormManager.GetProjectActionNameforAssetmanagement(model.ProjectActionTypeId);
                            // return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {

                }
            }
            return RedirectToAction("MyTasks", "Task");
        }

        // harish added new submit form
        //#664
        public ActionResult LenderSubmit1(OPAViewModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {
            if (ModelState.IsValid)
            {
                // var updatedconcurrency = taskManager.GetConcurrencyTimeStamp((Guid)model.TaskGuid);
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                var taskList = new List<TaskModel>();
                var task = new TaskModel();
                string alertTitle = String.Empty;
                task.AssignedTo = task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(model.FhaNumber);
                task.AssignedBy = UserPrincipal.Current.UserName;
                model.RequestStatus = (int)RequestStatus.Submit;

                TempData["PopupText"] = "Additional Information is Sent.";
                alertTitle = "Additional Information Sent";
                task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                model.Concurrency = concurrency;
                model.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(OPAViewModel), Encoding.Unicode);
                task.Notes = model.LenderComment;
                task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                task.IsReAssigned = model.IsReassigned;
                task.Concurrency = concurrency;
                //adding fhanumber and pagetype 
                task.FHANumber = model.FhaNumber;
                task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                var result = projectActionFormManager.GetPropertyInfo(model.FhaNumber);
                if (result != null)
                {
                    model.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                    model.PropertyAddress.City = result.City ?? "";
                    model.PropertyAddress.StateCode = result.State ?? "";
                    model.PropertyAddress.ZIP = result.Zipcode ?? "";

                }
                var taskOpenStatusModel = new TaskOpenStatusModel();
                taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(task);
                var taskFile = new List<TaskFileModel>();
                model.TaskStepId = task.TaskStepId;

                // Update Status 
                Guid ChildTaskInstanceId = model.ChildTaskInstanceId;
                var results = reviewFileStatusManager.GetTaskFileIdByChildTaskInstanceId(ChildTaskInstanceId);
                var resultset = results;
                foreach (var newresults in results)
                {
                    reviewFileStatusManager.UpdateReviewFileStatusForTaskFileId(newresults.TaskFileId);
                }

                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    //model.status = "3";
                    model.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(model);
                    model.ProjectActionFormId = projectActionFormId;
                    model.ServicerComments = model.LenderComment;
                    if (backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo, task.AssignedBy,
                        model))
                    {
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false,
                                true, "GenericPopup");
                            //model.OpaHistoryViewModel = GetOpaHistory((Guid)model.TaskGuid);
                            model.ProjectActionName =
                                projectActionFormManager.GetProjectActionNameforAssetmanagement(model.ProjectActionTypeId);
                            // return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", model);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {

                }
            }
            return RedirectToAction("MyTasks", "Task");
        }

        //[HttpPost]
        public ActionResult GenericPopup()
        {
            return PartialView("~/Views/ProjectAction/_GenericPopup.cshtml");
        }

        public List<OPAHistoryViewModel> GetOpaHistory(Guid taskInstanceId)
        {
            return projectActionFormManager.GetOpaHistory(taskInstanceId);
        }

        // #664 

        public List<OPAHistoryViewModel> GetOpaHistoryRAI(Guid taskInstanceId)
        {
            return projectActionFormManager.GetOpaHistory(taskInstanceId);
        }



        [HttpGet]
        public FileResult OPADownloadTaskFile(string taskfileid)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            reviewFileStatusManager.UpdateReviewFileStatusForFileDownload(taskFile.TaskFileId);
            if (taskFile != null)
            {
                string filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                var fileInfo = new System.IO.FileInfo(filePath);
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition",
                    String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.WriteFile(filePath);
                Response.End();
            }
            return null;
        }

        [HttpGet]
        public ActionResult DownloadTaskFile(string taskfileid)
        {

            string JsonSteing = "";

            StreamReader resultFile = null;
            Stream streamResult = null;
            // string JsonSteing = "{\"filename\":\"5091161.pdf\",\"folderName\":\"C:/em/FHA232/webtest1/\",\"documentType\":\"pdf\"}";
            RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();


            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            reviewFileStatusManager.UpdateReviewFileStatusForFileDownload(taskFile.TaskFileId);
            string filePath = "";
            FileInfo fileInfo = null;
            if (taskFile != null)
            {
                filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                fileInfo = new System.IO.FileInfo(filePath);
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition",
                String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                #region MyRegion
                if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                {
                    request = WebApiTokenRequest.RequestToken();
                    JsonSteing = "{\"docId\":" + taskFile.DocId + ",\"version\":" + taskFile.Version + "}";
                    streamResult = WebApiDownload.DownloadDocumentUsingWebApi(JsonSteing, request.access_token, taskFile.FileName);
                    return new FileStreamResult(streamResult, "application/octet-stream");
                }

                else
                {
                    filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                    fileInfo = new System.IO.FileInfo(filePath);
                    Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    Response.WriteFile(filePath);
                    Response.End();
                }
                #endregion

                ////karri replaced the above code since the above functionality is not working even after object is built
                //if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                //{
                //    request = WebApiTokenRequest.RequestToken();
                //    JsonSteing = "{\"docId\":" + taskFile.DocId + ",\"version\":" + taskFile.Version + "}";
                //    resultFile = new StreamReader(streamResult);
                //    #region Obsolete
                //    // resultFile = streamResult;
                //    //filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                //    //fileInfo = new System.IO.FileInfo(filePath);
                //    ////Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                //    ////Response.WriteFile(filePath);

                //    //#region file block
                //    //using (FileStream fs = new FileInfo(filePath).Open(FileMode.Create, FileAccess.ReadWrite))
                //    //{

                //    //    try
                //    //    {
                //    //        StreamReader rdr = new StreamReader(streamResult);

                //    //        string text = rdr.ReadToEnd();
                //    //        byte[] byteData = System.Text.Encoding.ASCII.GetBytes(text);
                //    //       // fs.Write(byteData, 0, byteData.Length);

                //    //        //Response.ClearContent();
                //    //        //Response.AddHeader("content-disposition", "attachment;filename=Contact.xls");
                //    //        //Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                //    //        //Response.Write(
                //    //        //Response.End();


                //    //        //using (StreamReader sr = new StreamReader(streamResult))
                //    //        //{
                //    //        //    string text = rdr.ReadToEnd();
                //    //        //    //byte[] byteData = System.Text.Encoding.ASCII.GetBytes(text);
                //    //        //    //fs.Write(byteData, 0, byteData.Length);

                //    //        //    string line;
                //    //        //    do
                //    //        //    {
                //    //        //        try
                //    //        //        {
                //    //        //            line = sr.ReadLine();
                //    //        //            byte[] byteData = System.Text.Encoding.ASCII.GetBytes(text);
                //    //        //            fs.Write(byteData, 0, byteData.Length);
                //    //        //        }
                //    //        //        catch (System.IO.IOException)
                //    //        //        {
                //    //        //            break;
                //    //        //        }
                //    //        //    } while (line != null);

                //    //        //}
                //    //    }
                //    //    catch (Exception ex1)
                //    //    {
                //    //        string a = ex1.Message;//karri
                //    //    }
                //    //}
                //    //#endregion 
                //    #endregion

                //};

                //Response.End();
            }




            return null;


        }

        [HttpPost]
        public virtual ActionResult ReAttachFile(OPAViewModel OPAProjectActionViewModel)
        {
            var FHA = Request.Form["FHA"];
            var ProjectActionTypeId = Request.Form["ProjectActionTypeId"];
            var TaskGuid = Request.Form["TaskGuid"];
            var lendercomment = Request.Form["lendercomment"];

            OPAProjectActionViewModel.FhaNumber = FHA;
            OPAProjectActionViewModel.ProjectActionTypeId = Convert.ToInt32(ProjectActionTypeId);
            OPAProjectActionViewModel.TaskGuid = new Guid(TaskGuid);
            foreach (string Filename in Request.Files)
            {
                HttpPostedFileBase myFile = Request.Files[Filename];
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();

                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = OPAProjectActionViewModel.FhaNumber + "_" +
                                         OPAProjectActionViewModel.ProjectActionTypeId + "_" +
                                         OPAProjectActionViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);

                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
                        DateTime.UtcNow.ToString(), systemFileName.ToString(),
                        new Guid(OPAProjectActionViewModel.TaskGuid.ToString()));
                    taskManager.SaveGroupTaskFileModel(taskFile);
                }
            }

            OPAViewModel formUploadData = new OPAViewModel();

            var task = taskManager.GetLatestTaskByTaskInstanceId(new Guid(TaskGuid));
            if (task.DataStore1.Contains("OPAViewModel"))
            {
                formUploadData =
                    XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1, Encoding.Unicode) as OPAViewModel;

                formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                formUploadData.SequenceId = task.SequenceId;
                formUploadData.AssignedBy = task.AssignedBy;
                formUploadData.AssignedTo = task.AssignedTo;
                formUploadData.IsReassigned = task.IsReAssigned;
            }
            formUploadData.OpaHistoryViewModel = GetOpaHistory((Guid)OPAProjectActionViewModel.TaskGuid);
            formUploadData.LenderComment = lendercomment;
            GetDisclaimerText(formUploadData); //User Story 1901
            formUploadData.IsAgreementAccepted = false; //User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", formUploadData);
        }

        public string OPADeleteFile(int taskfileid)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            var success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid));
                if (success == 1)
                {
                    System.IO.File.Delete(pathForSaving + "\\" + taskFile.SystemFileName);
                    if (success == 1)
                        return "Success";
                }
            }
            return "Success";
        }
        //#664
        public string DeleteFile(string taskfileid)
        {
            //Naveen 235 added foreach loop
            var taskFileIdList = taskfileid.Split(',');

            foreach (var taskfileid1 in taskFileIdList)
            {
                RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
                RestfulWebApiResultModel APIresult = new RestfulWebApiResultModel();
                RestfulWebApiUpdateModel UpdateInfo = new RestfulWebApiUpdateModel();
                TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid1));
                var success = 0;
                string pathForSaving = "";
                if (taskFile != null)
                {
                    if (UserPrincipal.Current.UserId == taskFile.CreatedBy || (UserPrincipal.Current.UserRole == "BackupAccountManager" || UserPrincipal.Current.UserRole == "LenderAccountManager" || UserPrincipal.Current.UserRole == "LenderAccountRepresentative"))
                    {

                        pathForSaving = Server.MapPath("~/Uploads");

                        success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid1));
                        if (success == 1)
                        {
                            if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                            {
                                request = WebApiTokenRequest.RequestToken();
                                //UpdateInfo.docId = taskFile.DocId;
                                //UpdateInfo.documentStatus = "Inactive";
                                //UpdateInfo.propertyId = "";
                                //UpdateInfo.documentType = "";
                                //UpdateInfo.indexType = "";
                                //UpdateInfo.indexValue = "500";

                                //APIresult = WebApiUpdate.UpdateDocument(UpdateInfo, request.access_token);
                                APIresult = WebApiDelete.DeleteDocumentUsingWebApi(taskFile.DocId, request.access_token);
                                //Naveen 235
                                //if (!string.IsNullOrEmpty(APIresult.status) && APIresult.key == "200")
                                //{
                                //    return "Success";
                                //}
                            }
                            else
                            {
                                pathForSaving = Server.MapPath("~/Uploads");


                                System.IO.File.Delete(pathForSaving + "\\" + taskFile.SystemFileName);

                                if (success == 1)
                                    return "Success";
                            }
                        }
                    }
                    else
                    {
                        return "unauthorized";
                    }
                }
            }

            return "Success";
        }
        [HttpPost]
        public ActionResult OPARemovefile(string taskFileId, string taskInstanceID, OPAViewModel OPAProjectActionViewModel,
            string lendercomment)
        {
            string GroupTaskId = string.Empty;
            //var TaskGuid = Request.Form["TaskGuid"];
            var TaskGuid = Request.QueryString[0];

            OPAViewModel formUploadData = new OPAViewModel();
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskFileId));
            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFileId));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            var task = taskManager.GetLatestTaskByTaskInstanceId(new Guid(TaskGuid));
            if (task.DataStore1.Contains("OPAViewModel"))
            {
                formUploadData =
                    XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1, Encoding.Unicode) as OPAViewModel;

                formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                formUploadData.SequenceId = task.SequenceId;
                formUploadData.AssignedBy = task.AssignedBy;
                formUploadData.AssignedTo = task.AssignedTo;
                formUploadData.IsReassigned = task.IsReAssigned;
            }
            formUploadData.OpaHistoryViewModel = GetOpaHistory((Guid)formUploadData.TaskGuid);
            formUploadData.LenderComment = lendercomment;
            GetDisclaimerText(formUploadData);
            formUploadData.IsAgreementAccepted = false;
            return PartialView("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", formUploadData);
        }


        [HttpPost]
        public ActionResult Removefile(string taskFileId, string taskInstanceID, OPAViewModel OPAProjectActionViewModel,
            string lendercomment)
        {
            string GroupTaskId = string.Empty;
            //var TaskGuid = Request.Form["TaskGuid"];
            var TaskGuid = Request.QueryString[0];

            OPAViewModel formUploadData = new OPAViewModel();
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskFileId));


            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFileId));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            var task = taskManager.GetLatestTaskByTaskInstanceId(new Guid(TaskGuid));
            if (task.DataStore1.Contains("OPAViewModel"))
            {
                formUploadData =
                    XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1, Encoding.Unicode) as OPAViewModel;

                formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                formUploadData.SequenceId = task.SequenceId;
                formUploadData.AssignedBy = task.AssignedBy;
                formUploadData.AssignedTo = task.AssignedTo;
                formUploadData.IsReassigned = task.IsReAssigned;
            }
            formUploadData.OpaHistoryViewModel = GetOpaHistory((Guid)formUploadData.TaskGuid);
            formUploadData.LenderComment = lendercomment;
            GetDisclaimerText(formUploadData);
            formUploadData.IsAgreementAccepted = false;
            return PartialView("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", formUploadData);
        }

        // User Story 1901 download
        private void GetDisclaimerText(OPAViewModel model)
        {
            model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("OPA");
        }
        //#664
        public JsonResult GetRequestAdditionalGridForLender1(string sidx, string sord, int page, int rows, int Projectactiontypeid,
            Guid taskInstanceId)
        {
            var AppProcessViewModel = new OPAViewModel();
            var requestAddtionalTask = new List<OPAHistoryViewModel>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            //var parentchildtaskmodel = new ParentChildTaskModel();


            // parentchildtaskmodel = parentChildTaskManager.GetChildTaskbytaskinstanceid(taskInstanceId);
            //var taskinstanceidRai = Taskinstanceid.ChildTaskInstanceId;

            requestAddtionalTask = GetOpaHistory(taskInstanceId);

            // requestAddtionalTask = GetOpaHistory(parentchildtaskmodel.ChildTaskInstanceId);
            //requestAddtionalTask = requestAddtionalTask.Where(x =>(x.IsOPAUpload!=1 || x.fileReviewStatusId !=1) || x.fileReviewStatusId !=1).ToList();
            //requestAddtionalTask = requestAddtionalTask.Where(x => x.IsOPAUpload != 1 && x.fileReviewStatusId != 1).ToList();
            // requestAddtionalTask = requestAddtionalTask.Where(x => x.DocTypeID != null || x.fileReviewStatusId == 1).ToList();
            // requestAddtionalTask = requestAddtionalTask.Distinct().Where(x => x.fileReviewStatusId == 1).ToList();
            //var  requestAddtionalTaskinfo = requestAddtionalTask.Where(x => x.actionTaken != "OPA Submit" && x.actionTaken != "File Upload ( New Unread )").ToList();
            //string s = "";
            //requestAddtionalTask = requestAddtionalTask.Where(x => x.actionTaken != "OPA Submit" &&x.actionTaken != "File Upload ( Read )").ToList();
            //if (requestAddtionalTask.Count > 0)
            if (requestAddtionalTask.Count > 0)
            {
                // harish added for to add file names
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                foreach (var item in requestAddtionalTask)
                {
                    if (item.fileReviewStatusId != 5 || item.fileReviewStatusId != 4)
                    {
                        //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                        //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                        item.uploadDate = item.uploadDate;
                        item.submitDate = item.submitDate;


                        //item.level = 0;
                        if (item.fileId != null)
                        {
                            var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
                            if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                            || currentUserRole == "BackupAccountManager")
                                //item.isUploadedByMe = true;
                                //item.isFileRenamed = true;
                                //item.role = UserPrincipal.Current.UserRole;
                                item.userRole = UserPrincipal.Current.UserRole;
                            item.name = UserPrincipal.Current.FullName;
                            // item.actionTaken = opahist.actionTaken;
                            //item.actionTaken = griditem.actionTaken;


                        }

                        //if (!string.IsNullOrEmpty(item.childFileName))
                        if ((item.actionTaken == "File Upload ( New Unread )" && item.userRole == "AE" && item.fileId != null) || (item.actionTaken == "File Upload ( New Unread )" && (item.userRole == "LenderAccountRepresentative" || item.userRole == "LAR") && item.fileId != null) || (item.fileReviewStatusId == 1 && (item.userRole == "LenderAccountManager" || item.userRole == "LAM") && item.fileId != null))
                        {
                            var DocTypeList = appProcessManager.GetAm_Assetmanagement(Convert.ToInt32(Projectactiontypeid));
                            if (DocTypeList.Count() == 0)
                            {
                                AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(Projectactiontypeid);
                                item.fileName = AppProcessViewModel.ProjectActionName;
                                //item.folderNodeName = AppProcessViewModel.ProjectActionName;
                                //item.actionTaken = opahist.actionTaken;
                            }

                            if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                            {

                                //harish new line of code for checking file name and document(shortnameid) 07-01-2020
                                if (DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).Any())
                                {
                                    item.fileName = item.fileName;
                                    //item.f = item.fileName;

                                }
                                else
                                {
                                    var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.shortname_id.Split('_').First()));
                                    if (matched.Any())
                                    {
                                        item.fileName = matched.First().shortname_id;
                                    }
                                    else
                                    {
                                        item.DocTypesList = GetAssetManagementDocTypesdropdownlist(DocTypeList, item.fileId);
                                    }
                                }
                                //item.DocTypesList = GetAssetManagementDocTypesdropdownlist(DocTypeList, item.fileId);
                                //item.isFileRenamed = true;
                            }

                            //item.DocTypesList = "<select data-val='true' data-val-number='The field BorrowerTypeId must be a number." +
                            //                "data-val-required='The BorrowerTypeId field is required.' id=_'ddlDocType'  name='BorrowerTypeId'>" +
                            //                "<option value=''>Select Document Type</option>" +
                            //                "<option value='1'>Profit</option>" +
                            //                "<option value='2'>Non-Profit</option></select>";

                        }

                        else
                        {

                        }
                    }
                    else if (item.fileReviewStatusId == 5)
                    {
                        item.userRole = "AE";
                        item.name = item.name;
                    }
                    else
                    {
                        item.userRole = "AE";
                        item.name = item.name;
                    }
                }


                int totalrecods = requestAddtionalTask.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                if (sord.ToUpper() == "DESC")
                {

                    requestAddtionalTask = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList();

                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    requestAddtionalTask = requestAddtionalTask.OrderBy(s => s.submitDate).ToList();
                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {

                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = requestAddtionalTask,
                };




                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {



                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = requestAddtionalTask,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }






        // harish added new method for rai 10-02-2020
        public JsonResult GetRequestAdditionalGridForLender(string sidx, string sord, int page, int rows, int Projectactiontypeid,
         Guid taskInstanceId)
        {
            var AppProcessViewModel = new OPAViewModel();
            var requestAddtionalTask = new List<OPAHistoryViewModel>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //var parentchildtaskmodel = new ParentChildTaskModel();


            //parentchildtaskmodel = parentChildTaskManager.GetChildTaskbytaskinstanceid(taskInstanceId);
            //var taskinstanceidRai = Taskinstanceid.ChildTaskInstanceId;

            requestAddtionalTask = GetOpaHistory(taskInstanceId);
            requestAddtionalTask = requestAddtionalTask.GroupBy(o => new { o.fileId }).Select(o => o.FirstOrDefault()).ToList();

            //requestAddtionalTask = GetOpaHistory(parentchildtaskmodel.ChildTaskInstanceId);

            //requestAddtionalTask = GetOpaHistory(taskInstanceId);

            if (requestAddtionalTask.Count > 0)
            {
                //foreach(var item in requestAddtionalTask)
                //{
                //    item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                //    item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                //    //if (!string.IsNullOrEmpty(item.childFileName))
                //    if(item.DocTypeID ==null)
                //   // if ((item.actionTaken == "File Upload ( New Unread )" && item.userRole == "AE" && item.fileId != null) || (item.actionTaken == "File Upload ( New Unread )" && (item.userRole == "LenderAccountRepresentative" || item.userRole == "LAR") && item.fileId != null) || (item.fileReviewStatusId == 1 && (item.userRole == "LenderAccountManager" || item.userRole == "LAM") && item.fileId != null))
                //    {
                //        var DocTypeList = appProcessManager.GetAm_Assetmanagement(Convert.ToInt32(Projectactiontypeid));
                //        if (DocTypeList.Count() == 0)
                //        {
                //            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(Projectactiontypeid);
                //            item.fileName = AppProcessViewModel.ProjectActionName;
                //            //item.folderNodeName = AppProcessViewModel.ProjectActionName;
                //            //item.actionTaken = opahist.actionTaken;
                //        }

                //        if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                //        {

                //            //harish new line of code for checking file name and document(shortnameid) 07-01-2020
                //            if (DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).Any())
                //            {
                //                item.fileName = item.fileName;
                //                //item.f = item.fileName;

                //            }
                //            else
                //            {
                //                var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.shortname_id.Split('_').First()));
                //                if (matched.Any())
                //                {
                //                    item.fileName = matched.First().shortname_id;
                //                }
                //                else
                //                {
                //                    item.DocTypesList = GetAssetManagementDocTypesdropdownlist(DocTypeList, item.fileId);
                //                }
                //            }
                //            //item.DocTypesList = GetAssetManagementDocTypesdropdownlist(DocTypeList, item.fileId);
                //            //item.isFileRenamed = true;
                //        }

                //        //item.DocTypesList = "<select data-val='true' data-val-number='The field BorrowerTypeId must be a number." +
                //        //                "data-val-required='The BorrowerTypeId field is required.' id=_'ddlDocType'  name='BorrowerTypeId'>" +
                //        //                "<option value=''>Select Document Type</option>" +
                //        //                "<option value='1'>Profit</option>" +
                //        //                "<option value='2'>Non-Profit</option></select>";

                //    }

                //    else
                //    {

                //    }









                //}











                int totalrecods = requestAddtionalTask.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                if (sord.ToUpper() == "DESC")
                {

                    requestAddtionalTask = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList();

                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    requestAddtionalTask = requestAddtionalTask.OrderBy(s => s.submitDate).ToList();
                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {

                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList(),
                };




                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {



                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = requestAddtionalTask.OrderBy(s => s.submitDate),

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }


        public string SaveToTaskFileHistory()
        {
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //request = WebApiTokenRequest.RequestToken();

            bool isUploaded = false;

            var fileId = Request.Form["fileId"];
            var childTaskInstanceId = Request.Form["childInstanceId"];
            var fhanumber = Request.Form["FhaNumber"];
            int FolderKey = 39;
            var isNewFile = false;
            var files = Request.Files;
            var taskFileModel = new TaskFileModel();
            var opaProjectActionViewModel = new OPAViewModel();
            Guid taskInstanceIdForFileTable = new Guid();
            var childTaskNewFile = new ChildTaskNewFileModel();
            var FolderInfo = projectActionFormManager.GetFolderInfoByKey(FolderKey);
            //AppProcessViewModel.pageTypeId = 3;
            if (opaProjectActionViewModel.FhaNumber == null)
            {
                taskFileModel = taskManager.GetTaskFileById(int.Parse(fileId));
                opaProjectActionViewModel = projectActionFormManager.GetOPAByTaskInstanceId(taskFileModel.TaskInstanceId);
                opaProjectActionViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(fhanumber).PropertyId;
                //opaProjectActionViewModel.PropertyId = projectActionFormManager.GetPropertyInfo(opaProjectActionViewModel.FhaNumber).PropertyId;
                opaProjectActionViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(opaProjectActionViewModel.ProjectActionTypeId);
            }

            string folderNames = string.Empty;

            if (FolderInfo != null)
            {


                folderNames = "/" + opaProjectActionViewModel.PropertyId + "/" + opaProjectActionViewModel.FhaNumber + "/" + "Asset Management /" + opaProjectActionViewModel.ProjectActionName + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy");



            }
            if (fileId != "")
            {
                taskFileModel = taskManager.GetTaskFileById(int.Parse(fileId));
                opaProjectActionViewModel = projectActionFormManager.GetOPAByTaskInstanceId(taskFileModel.TaskInstanceId);
                opaProjectActionViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(fhanumber).PropertyId;
                //opaProjectActionViewModel.PropertyId = projectActionFormManager.GetPropertyInfo(opaProjectActionViewModel.FhaNumber).PropertyId;
                opaProjectActionViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(opaProjectActionViewModel.ProjectActionTypeId);
                // var success = this.RAIUpload(opaProjectActionViewModel, Convert.ToInt32(FolderKey));
            }
            else
            {
                opaProjectActionViewModel = projectActionFormManager.GetOPAByChildTaskId(new Guid(childTaskInstanceId));

            }


            foreach (string file in files)
            {
                var expectedFileName = Request.Form["expectedFileName"];
                HttpPostedFileBase myFile = Request.Files[file];
                if (myFile != null && myFile.ContentLength != 0)
                {

                    if (string.IsNullOrEmpty(expectedFileName) || expectedFileName == "null" || expectedFileName == "NA")
                    {
                        expectedFileName = Path.GetFileName(myFile.FileName);
                        isNewFile = true;
                        taskInstanceIdForFileTable = parentChildTaskManager.GetParentTask(new Guid(childTaskInstanceId)).ParentTaskInstanceId;
                    }
                    else if (expectedFileName != null &&
                             Path.GetExtension(expectedFileName) != Path.GetExtension(myFile.FileName))
                    {
                        expectedFileName = expectedFileName.Replace(Path.GetExtension(expectedFileName),
                            Path.GetExtension(myFile.FileName));
                        taskInstanceIdForFileTable = new Guid(childTaskInstanceId);
                    }
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();
                    var UniqueId = randoms.Next(0, 99999);
                    var systemFileName = opaProjectActionViewModel.FhaNumber + "_" +
                                         opaProjectActionViewModel.ProjectActionTypeId + "_" +
                                         taskInstanceIdForFileTable + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);
                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    // RestfulWebApiUploadModel[] obj1 = new RestfulWebApiUploadModel[1];
                    var uploadmodel = new RestfulWebApiUploadModel()
                    {
                        propertyID = opaProjectActionViewModel.PropertyId.ToString(),
                        indexType = "1",
                        indexValue = opaProjectActionViewModel.FhaNumber,
                        pdfConvertableValue = "false",
                        folderKey = FolderKey,
                        folderNames = folderNames,
                        transactionType = opaProjectActionViewModel.ProjectActionTypeId.ToString(),
                        transactionStatus = "",
                        transactionDate = DateTime.UtcNow.ToString("MM-dd-yyyy")
                    };
                    // obj1[0] = uploadmodel;

                    request = WebApiTokenRequest.RequestToken();
                    WebApiUploadResult = WebApiDocumentUpload.AssetManagementUploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");

                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {

                        isUploaded = true;

                        var taskFile = PoupuLateRequestAdditionalTaskFile(myFile, fileSize, UniqueId.ToString(),
                        expectedFileName, systemFileName.ToString(),
                        taskInstanceIdForFileTable);
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        if (WebApiUploadResult.documentType != DefaultDocID)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        else if (FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductClosing) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductUnderwriting) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductLoanCommittee))
                        {
                            //taskFile.DocTypeID = DefaultDocID;
                            taskFile.FileType = FileType.WP;
                        }
                        else if (opaProjectActionViewModel.pageTypeId == 3)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }

                        taskFile.RoleName = UserPrincipal.Current.UserRole;
                        taskFile.FolderName = folderNames;
                        var childTaskFileId = taskManager.SaveGroupTaskFileModel(taskFile);
                        if (isNewFile)
                        {
                            var reviewFileStatusModel = new ReviewFileStatusModel();
                            reviewFileStatusModel.ReviewFileStatusId = Guid.NewGuid();
                            reviewFileStatusModel.ReviewerUserId = taskManager.GetReviewerUserIdByTaskInstanceId(taskFile.TaskInstanceId);
                            reviewFileStatusModel.Status = 1;
                            reviewFileStatusModel.TaskFileId = taskFile.TaskFileId;
                            reviewFileStatusModel.ModifiedOn = DateTime.UtcNow;
                            reviewFileStatusModel.ModifiedBy = UserPrincipal.Current.UserId;
                            reviewFileStatusManager.SaveReviewFileStatus(reviewFileStatusModel);
                            childTaskNewFile = new ChildTaskNewFileModel();
                            childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
                            childTaskNewFile.ChildTaskInstanceId = new Guid(childTaskInstanceId);
                            childTaskNewFile.TaskFileInstanceId = childTaskFileId;
                            projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);
                        }
                        if (fileId != "")
                        {
                            var taskFileHistoryModel = new TaskFileHistoryModel();
                            taskFileHistoryModel.TaskFileHistoryId = Guid.NewGuid();
                            taskFileHistoryModel.ChildTaskInstanceId = new Guid(childTaskInstanceId);
                            taskFileHistoryModel.ParentTaskFileId = taskFileModel.TaskFileId;
                            taskFileHistoryModel.ChildTaskFileId = childTaskFileId;
                            projectActionFormManager.SaveLenderRequestAdditionalFiles(taskFileHistoryModel);
                            var result =
                                reviewFileStatusManager.UpdateReviewFileStatusForRequestAdditionalResponse(
                                    taskFileModel.TaskFileId);

                        }
                    }


                }
            }
            return "Success";
        }

        public string DeleteFileAndChildFile(int taskfileid)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid));
            var success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");

                success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid));
                if (success == 1)
                {
                    System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
                    if (projectActionFormManager.IsFileHistoryExists(taskFile.TaskFileId))
                    {
                        success = projectActionFormManager.DeleteChildTaskFile(taskFile.TaskFileId);
                        if (success == 1)
                            return "Success";
                    }
                    return "Success";
                }
            }
            return "Failure";
        }

        private TaskFileModel PoupuLateRequestAdditionalTaskFile(HttpPostedFileBase fileStream, double fileSize, string checkListId, string expectedFileName, string systemFileName, Guid taskInstanceId)
        {
            var taskFile = new TaskFileModel();
            taskFile.FileName = expectedFileName;
            taskFile.GroupFileType = checkListId;
            taskFile.UploadTime = DateTime.UtcNow;
            // taskFile.FileName = fileStream.FileName;
            taskFile.FileSize = fileSize;
            taskFile.SystemFileName = systemFileName;
            taskFile.TaskInstanceId = taskInstanceId;
            taskFile.CreatedBy = UserPrincipal.Current.UserId;
            //BinaryReader rdr = null;
            try
            {
                //rdr = new BinaryReader(fileStream.InputStream);
                taskFile.FileData = null;//rdr.ReadBytes(fileStream.ContentLength);
            }
            finally
            {
            }
            return taskFile;
        }

        public string CheckIfFileExistsByGroupTaskId(int groupTaskId)
        {
            return groupTaskManager.CheckIfFileExistsByGroupTaskId(groupTaskId) ? "success" : "failure";
        }

        //#664
        // harish added new method
        public JsonResult GetProdLenderUploadFileGridContentOPA(string sidx, string sord, int page, int rows, int Projectactiontypeid, Guid? taskInstanceId, string PropertyId, string FhaNumber, string FormName)
        {
            int newprojectactionid = Projectactiontypeid;
            var AppProcessViewModel = new OPAViewModel();
            var opaview = new OPAViewModel();
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            AppProcessViewModel.OpaHistoryViewModel = GetOpaHistory((Guid)taskInstanceId);
            opaview = projectActionFormManager.GetOPAByTaskInstanceId((Guid)taskInstanceId);
            if (opaview != null)
            {
                newprojectactionid = projectActionFormManager.Updateprojectactiontypeid(opaview, newprojectactionid);
            }

            var opahist = new OPAHistoryViewModel();
            if (AppProcessViewModel.OpaHistoryViewModel != null)
            {
                foreach (var item in AppProcessViewModel.OpaHistoryViewModel)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    opahist.actionTaken = item.actionTaken;
                    opahist.userRole = item.userRole;

                }

            }

            var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId.Value, PropertyId, FhaNumber);
            GridContent = GridContent.Where(x => x.FolderStructureKey != 40).ToList();
            foreach (var prevfile in GridContent)
            {
                if (prevfile.id == null)
                {
                    // prevfile.id = "39";
                    prevfile.id = "39_" + prevfile.fileId;
                    prevfile.parent = "39";
                }
            }

            // GridContent = GridContent.Where(x => x.id != "32" && x.id !="40" && x.id !="41").ToList();
            GridContent = GridContent.Where(x => x.id.Split('_')[0] != "32" && x.id.Split('_')[0] != "40" && x.id.Split('_')[0] != "41").ToList();
            GridContent = GridContent.Where(x => x.id.Split('_')[0] == "39").ToList();

            // GridContent = GridContent.Where(x => x.id == "39").ToList();

            GridContent = GridContent.GroupBy(o => new { o.fileId }).Select(o => o.FirstOrDefault()).ToList();


            //GridContent = GridContent.Where(x => x.id == "39").ToList();

            if (GridContent != null)
            {
                // GridContent = GridContent.Where(x => x.PageTypeId == Convert.ToInt32(FormName) || x.PageTypeId == 3).ToList();
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;


                    //if(item.role != "AccountExecutive" ||item.role !="AE")
                    //item.level = 0;
                    if (item.fileId != null)
                    {
                        var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
                        if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                        || currentUserRole == "BackupAccountManager")
                            item.isUploadedByMe = true;
                        //item.role = UserPrincipal.Current.UserRole;
                        item.role = opahist.userRole;
                        //item.role = accountManager.get(item.CreatedBy);
                        // item.name = UserPrincipal.Current.FullName;
                        item.name = accountManager.GetFullUserNameById(item.CreatedBy);
                        item.actionTaken = opahist.actionTaken;
                        //item.actionTaken = griditem.actionTaken;


                    }

                    if (!string.IsNullOrEmpty(item.parent))
                    {
                        //var DocTypeList = appProcessManager.GetAm_Assetmanagement(Convert.ToInt32(projectactionid));
                        var DocTypeList = appProcessManager.GetAm_Assetmanagement(newprojectactionid);
                        if (DocTypeList.Count() == 0)
                        {
                            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(newprojectactionid);
                            item.fileName = AppProcessViewModel.ProjectActionName;
                            item.folderNodeName = AppProcessViewModel.ProjectActionName;
                            item.actionTaken = opahist.actionTaken;
                            if (item.fileName == "PCNA Upload")
                            {
                                item.DocTypeID = "7000937";
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    string ext = Path.GetExtension(taskfile.FileName);
                                    taskfile.FileName = item.fileName + ext;
                                    item.folderNodeName = taskfile.FileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                }
                            }
                            else if (item.fileName == "Other")
                            {
                                item.DocTypeID = "2082";
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    string ext = Path.GetExtension(taskfile.FileName);
                                    taskfile.FileName = item.fileName + ext;
                                    item.folderNodeName = taskfile.FileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                }
                            }
                            // harish adding other 5 transaction types due to corona these need to be added 27042020
                            //COVID-19/R4R Suspensions
                            else if (item.fileName == "COVID-19/R4R Suspensions")
                            {
                                item.DocTypeID = "2082";
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    string ext = Path.GetExtension(taskfile.FileName);
                                    taskfile.FileName = item.fileName + ext;
                                    item.folderNodeName = taskfile.FileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                }
                            }
                            //COVID-19/R4R Releases for Mortgage Payments
                            else if (item.fileName == "COVID-19/R4R Releases for Mortgage Payments")
                            {
                                item.DocTypeID = "2082";
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    string ext = Path.GetExtension(taskfile.FileName);
                                    taskfile.FileName = item.fileName + ext;
                                    item.folderNodeName = taskfile.FileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                }
                            }
                            //COVID-19/R4R Other Releases
                            else if (item.fileName == "COVID-19/R4R Other Releases")
                            {
                                item.DocTypeID = "2082";
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    string ext = Path.GetExtension(taskfile.FileName);
                                    taskfile.FileName = item.fileName + ext;
                                    item.folderNodeName = taskfile.FileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                }
                            }
                            //COVID-19/Escrow Releases to Pay Mortgage
                            else if (item.fileName == "COVID-19/Escrow Releases to Pay Mortgage")
                            {
                                item.DocTypeID = "2082";
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    string ext = Path.GetExtension(taskfile.FileName);
                                    taskfile.FileName = item.fileName + ext;
                                    item.folderNodeName = taskfile.FileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                }
                            }
                            //COVID-19/Other Relief
                            else if (item.fileName == "COVID-19/Other Relief")
                            {
                                item.DocTypeID = "2082";
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    string ext = Path.GetExtension(taskfile.FileName);
                                    taskfile.FileName = item.fileName + ext;
                                    item.folderNodeName = taskfile.FileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                }
                            }
                        }


                        //harish added 00042020
                        if (item.DocTypeID == "2082" && (item.fileName.ToUpper().StartsWith("OTHERS_") || item.fileName.ToUpper().StartsWith("OTHER_")))
                        {
                            var matched = item.fileName.Substring(7, item.fileName.Length - 7);
                            // var matched = item.fileName.Split('_').Last();
                            // item.fileName = matched;
                            var matchedinfo = DocTypeList.Where(x => matched.Contains(x.shortname_id));
                            if (matchedinfo.Any())
                            {
                                item.DocTypeID = matchedinfo.First().doc_type_id.ToString();
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {

                                    item.fileName = taskfile.FileName;
                                    taskfile.DocTypeID = item.DocTypeID;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);

                                }
                            }
                            else
                            {
                                var matchedfile = item.fileName.Substring(7, item.fileName.Length - 7);

                                var matchedinfodata = DocTypeList.Where(x => matchedfile.Contains(x.shortname_id));
                                if (matchedinfodata.Any())
                                {
                                    item.DocTypeID = matchedinfodata.First().doc_type_id.ToString();
                                    var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                    if (taskfile != null)
                                    {

                                        item.fileName = taskfile.FileName;
                                        taskfile.DocTypeID = item.DocTypeID;
                                        taskManager.UpdateDoctyidinTaskfile(taskfile);

                                    }
                                }
                            }

                        }
                        //EXTENSION CHANGES 
                        //if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                        //{
                        //    //harish new line of code for checking file name and document(shortnameid) 07-01-2020
                        //    item.fileName =  Path.GetFileNameWithoutExtension(item.fileName);
                        //    //if (DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).Any())
                        //    if (DocTypeList.Where(x => x.shortname_id.Contains(item.fileName)).Any())

                        //    {
                        //        // item.fileName = item.fileName;
                        //        //item.folderNodeName = item.fileName;
                        //        //var matchedinfo = DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).First();
                        //        var matchedinfo = DocTypeList.Where(x => x.shortname_id.Contains(item.fileName)).First();
                        //        item.DocTypeID = matchedinfo.doc_type_id.ToString();
                        //        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                        //        if (taskfile != null)
                        //        {
                        //            string ext = Path.GetExtension(taskfile.FileName);
                        //            item.fileName = item.fileName + ext;
                        //            item.folderNodeName = item.fileName;
                        //            taskfile.DocTypeID = item.DocTypeID;
                        //            taskfile.FileName = item.fileName;
                        //            taskManager.UpdateDoctyidinTaskfile(taskfile);
                        //        }

                        //    }

                        //    else
                        //    {
                        //        var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.shortname_id.Split('_').First()));
                        //        if (matched.Any())
                        //        {
                        //            // item.fileName = matched.First().shortname_id;
                        //            item.DocTypeID = matched.First().doc_type_id.ToString();
                        //            var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                        //            if (taskfile != null)
                        //            {
                        //                taskfile.DocTypeID = item.DocTypeID;
                        //                //taskfile.FileName = item.fileName;
                        //                //Getting file extension name from task file table and adding extension to the filename@28042020
                        //                string ext = Path.GetExtension(taskfile.FileName);
                        //                taskfile.FileName = item.fileName +ext;
                        //                item.fileName = taskfile.FileName;
                        //                item.folderNodeName = item.fileName;
                        //                taskManager.UpdateDoctyidinTaskfile(taskfile);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            item.DocTypesList = GetAssetManagementDocTypesdropdownlist(DocTypeList, item.fileId);
                        //        }
                        //    }


                        //}
                        if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                        {
                            //harish new line of code for checking file name and document(shortnameid) 07-01-2020
                            if (DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).Any())

                            {
                                item.fileName = item.fileName;
                                item.folderNodeName = item.fileName;
                                var matchedinfo = DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).First();
                                item.DocTypeID = matchedinfo.doc_type_id.ToString();
                                var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                if (taskfile != null)
                                {
                                    taskfile.DocTypeID = item.DocTypeID;
                                    taskfile.FileName = item.fileName;
                                    taskManager.UpdateDoctyidinTaskfile(taskfile);
                                }

                            }

                            else
                            {
                                var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.shortname_id.Split('_').First()));
                                if (matched.Any())
                                {
                                    // item.fileName = matched.First().shortname_id;
                                    item.DocTypeID = matched.First().doc_type_id.ToString();
                                    var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                    if (taskfile != null)
                                    {
                                        taskfile.DocTypeID = item.DocTypeID;
                                        //taskfile.FileName = item.fileName;
                                        //Getting file extension name from task file table and adding extension to the filename@28042020
                                        //string ext = Path.GetExtension(taskfile.FileName);
                                        taskfile.FileName = item.fileName;
                                        item.fileName = taskfile.FileName;
                                        item.folderNodeName = item.fileName;
                                        taskManager.UpdateDoctyidinTaskfile(taskfile);
                                    }
                                }
                                else
                                {
                                    item.DocTypesList = GetAssetManagementDocTypesdropdownlist(DocTypeList, item.fileId);
                                }
                            }


                        }


                    }

                    else
                    {
                        if (item.SubfolderSequence != null)
                        {
                            item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                        }
                    }
                }
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var result = GridContent.ToList();//.OrderBy(a => a.id);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results.OrderBy(x => x.uploadDate).ToList(),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        //#664
        private string GetDocTypesdropdownlist(IEnumerable<DocumentTypeModel> DocTypeModel, int? fileid)
        {

            StringBuilder sb = new StringBuilder();
            string defaultDocTypeId = ConfigurationManager.AppSettings["DefaultDocId"].ToString();
            sb.AppendLine("<select id='" + fileid.ToString() + "_ddlDocType'");
            sb.AppendLine("onchange='ChangrDocTypeConfirmation(fileId =" + fileid.ToString() + ")'  style='width: 200px;'>");
            sb.AppendLine("<option value='0'>Select New File Name</option>");

            foreach (DocumentTypeModel doctype in DocTypeModel)
            {
                string DocName = "";
                if (!string.IsNullOrEmpty(doctype.ShortDocTypeName))
                {
                    DocName = doctype.ShortDocTypeName + "_" + doctype.DocumentType;
                }
                else
                {
                    DocName = doctype.DocumentType;
                }
                sb.AppendLine("<option value='" + doctype.DocumentTypeId.ToString() + "' id='" + doctype.DocumentTypeId.ToString() + "'>" + DocName + "</option>");
            }
            sb.AppendLine("<option value='" + defaultDocTypeId + "'>Others</option></select> ");

            return sb.ToString();
        }
        //#664
        public JsonResult onchangedropdownvalue(int projectActionTypeId)
        {
            var OPAModel = new OPAViewModel();
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByProjecttype(projectActionTypeId);
            foreach (var item in projectActionList)
            {
                OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                {
                    Text = item.ProjectActionName,
                    Value = item.ProjectActionID.ToString()
                });
            }
            return Json(OPAModel, JsonRequestBehavior.AllowGet);
        }
        //#664
        [ValidateInput(false)]
        public ActionResult UploadPopupLoad(int FolderKey, string Model)
        {
            FolderKey = 39;



            var parentdict = GetParentModelDictionary(Model);

            ViewBag.uploadurl = "/OPAForm/SaveDropzoneJsUploadedFiles";

            if (parentdict != null)
            {
                var PagetypeID = parentdict.FirstOrDefault(x => x.Key == "pageTypeId").Value;
                if (Convert.ToInt32(PagetypeID) == 18)
                {
                    ViewBag.redirecturl = "/Amendments/Reload/GroupTaskInstanceId?=";
                }
                else
                {
                    ViewBag.redirecturl = "/OPAForm/Reload/GroupTaskInstanceId?=";
                    //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();
                }
            }

            var folderuploadModel = new FolderUploadModel { FolderKey = FolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };


            return View("~/Views/Production/_Shared/_DragDropFileUpload.cshtml", folderuploadModel);
        }
        //#664
        private Dictionary<string, object> GetParentModelDictionary(string Model)
        {
            Dictionary<string, object> mydict = new Dictionary<string, object>();
            //string[] reslts = Model.Split(new char[] { '|', '|', '|', '|' }, StringSplitOptions.RemoveEmptyEntries);
            string[] reslts = Model.Split(new char[] { '|', '|', '|', '|' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string s in reslts)
            {
                var temp = JObject.Parse(s);

                if (!mydict.ContainsKey(((Newtonsoft.Json.Linq.JValue)(temp["name"])).Value.ToString()))
                {
                    mydict.Add(((Newtonsoft.Json.Linq.JValue)(temp["name"])).Value.ToString(), ((Newtonsoft.Json.Linq.JValue)(temp["value"])).Value.ToString());
                }

            }
            return mydict;
        }

        //#664
        [ValidateInput(false)]
        public ActionResult SaveDropzoneJsUploadedFiles(FolderUploadModel fileFolderModel)
        {
            OPAViewModel AppProcessModel = new OPAViewModel();

            int userID = 0;
            string userModel = string.Empty;
            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;

            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            int projectactiontypeid = 0;
            //var projectactiontypeid = Request.Form["projectActionType"];
            string value = string.Empty;
            string GroupTaskInstanceId = string.Empty;
            int pageTypeId = 0;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();
            //Logic for check  whether a task Exists
            var parentdict = GetParentModelDictionary(ParentModel);
            bool IsTaskavilable = false;
            //harish
            string GroupTaskId = string.Empty;
            GroupTaskId = Request.Form["GroupTaskId"];
            //projectactiontypeid = (string)parentdict["ProjectActionTypeId"];






            try
            {
                if (parentdict.Count > 0)
                {

                    if (parentdict.ContainsKey("GroupTaskInstanceId") && !string.IsNullOrEmpty((string)parentdict["GroupTaskInstanceId"]))
                    {
                        GroupTaskInstanceId = (string)parentdict["GroupTaskInstanceId"];

                    }
                    if (parentdict.ContainsKey("pageTypeId") && !string.IsNullOrEmpty((string)parentdict["pageTypeId"]))
                    {

                        pageTypeId = Convert.ToInt32((string)parentdict["pageTypeId"]);

                    }
                    var grouptaskmodel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
                    if (grouptaskmodel == null)
                    {


                        //Save OpaForm

                        //AppProcessModel.ProjectActionName = parentdict["ProjectActionName"] != null ? (string)parentdict["ProjectActionName"] : "";

                        AppProcessModel.ServicerComments = "";


                        AppProcessModel.PropertyName = parentdict["PropertyName"] != null ? (string)parentdict["PropertyName"] : "";
                        AppProcessModel.RequesterName = parentdict["RequesterName"] != null ? (string)parentdict["RequesterName"] : "";
                        AppProcessModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        //  var  FHA = Request.Form["FHA"];
                        AppProcessModel.IsAddressChange = parentdict["IsAddressChange"] != null ? Convert.ToBoolean(parentdict["IsAddressChange"]) : false;
                        //AppProcessModel.ProjectActionTypeId = Convert.ToInt32( projectactiontypeid);
                        //model.FhaNumber = FHA;
                        //Insert the non critical request and get the generated id, insert into view model
                        AppProcessModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : ""; ;
                        AppProcessModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        AppProcessModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(AppProcessModel.ProjectActionTypeId);

                        AppProcessModel.ServicerSubmissionDate = DateTime.UtcNow;
                        AppProcessModel.RequestStatus = (int)RequestStatus.Draft;
                        AppProcessModel.CreatedOn = DateTime.UtcNow;
                        AppProcessModel.ModifiedOn = DateTime.UtcNow;
                        AppProcessModel.CreatedBy = UserPrincipal.Current.UserId;
                        AppProcessModel.ModifiedBy = UserPrincipal.Current.UserId;
                        AppProcessModel.ProjectActionDate = DateTime.UtcNow;
                        AppProcessModel.RequestDate = DateTime.UtcNow;
                        AppProcessModel.TaskInstanceId = new Guid(GroupTaskInstanceId);
                        AppProcessModel.pageTypeId = pageTypeId;


                        AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        var groupTaskModel = new GroupTaskModel();
                        groupTaskModel.TaskInstanceId = AppProcessModel.TaskInstanceId;
                        groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.FhaNumber = AppProcessModel.FhaNumber;
                        groupTaskModel.PropertyName = AppProcessModel.PropertyName;
                        groupTaskModel.RequestDate = AppProcessModel.RequestDate;
                        groupTaskModel.RequesterName = AppProcessModel.RequesterName;
                        groupTaskModel.ServicerSubmissionDate = AppProcessModel.ServicerSubmissionDate;
                        groupTaskModel.ServicerComments = parentdict["ServicerComments"] != null ? (string)parentdict["ServicerComments"] : "";
                        groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                        groupTaskModel.ProjectActionTypeId = AppProcessModel.ProjectActionTypeId;
                        groupTaskModel.IsDisclimerAccepted = AppProcessModel.IsAgreementAccepted;
                        groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.CreatedOn = DateTime.UtcNow;
                        groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedOn = DateTime.UtcNow;
                        groupTaskModel.IsAddressChanged = AppProcessModel.IsAddressChange;
                        groupTaskModel.ServicerComments = AppProcessModel.ServicerComments;
                        AppProcessModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);

                        var projectActionFormId = projectActionFormManager.SaveOPAForm(AppProcessModel);

                        InstanceID = groupTaskModel.TaskInstanceId;
                        AppProcessModel.GroupTaskInstanceId = InstanceID;
                        AppProcessModel.ProjectActionFormId = projectActionFormId;
                    }
                    else
                    {
                        AppProcessModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        AppProcessModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : "";
                        AppProcessModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
                        AppProcessModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        //skumar-form290 requires following information
                        AppProcessModel.pageTypeId = pageTypeId;
                        if (parentdict.ContainsKey("ModifiedBy"))
                        {
                            if (parentdict["ModifiedBy"] != null)
                            {
                                userModel = (string)parentdict["ModifiedBy"];
                                if (string.IsNullOrEmpty(userModel))
                                    userModel = (string)parentdict["CreatedBy"];
                                userID = Convert.ToInt32(userModel);
                            }
                            AppProcessModel.userId = userID;
                        }

                    }
                }
                var success = this.Upload(AppProcessModel, Convert.ToInt32(FolderKey));


                if (success)
                {

                    string assignedto = projectActionFormManager.GetAeEmailByFhaNumber(AppProcessModel.FhaNumber);
                    int userId = webSecurity.GetUserId(assignedto);
                    Guid taskInstanceID = new Guid(GroupTaskInstanceId);
                    int reviewerUserId = userId;
                    int currentUserId = UserPrincipal.Current.UserId;

                    //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                    taskManager.SaveOPAReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                    ViewBag.UploadFailed = "False";
                    TempData["UploadFailed"] = "False";
                    ViewBag.Message = "Files Uploaded Sucessfully ";

                }
                else
                {
                    ViewBag.UploadFailed = "True";
                    TempData["UploadFailed"] = "True";
                    ViewBag.Message = "Files Uploaded UnSucessfully ";

                }
                if (pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessModel);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestForm.cshtml", AppProcessModel);
                }
            }
            catch (Exception e)
            {
                //todo:skumar: redirect to error page
                return RedirectToAction("Login", "Account");
            }
        }

        // upload to api
        //#664
        [ValidateInput(false)]
        private bool Upload(OPAViewModel AppProcessViewModel, int FolderKey)
        {
            var FolderInfo = projectActionFormManager.GetFolderInfoByKey(FolderKey);
            //AppProcessViewModel.pageTypeId = 3;
            AppProcessViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(AppProcessViewModel.FhaNumber).PropertyId;
            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(AppProcessViewModel.ProjectActionTypeId);
            var PageName = projectActionFormManager.FormNameBYPageTypeID(AppProcessViewModel.pageTypeId);
            string folderNames = string.Empty;
            string formNameConfiguration = ConfigurationManager.AppSettings["FORM 290"] ?? ConfigurationSettings.AppSettings["FORM 290"];
            string amendmentConfig = ConfigurationManager.AppSettings["Amendments"] ?? ConfigurationSettings.AppSettings["Amendments290"];
            int ammendmentPageId = !string.IsNullOrEmpty(amendmentConfig) ? Convert.ToInt32(amendmentConfig) : 0;
            int form290PageId = !string.IsNullOrEmpty(formNameConfiguration) ? Convert.ToInt32(formNameConfiguration) : 0;
            if (FolderInfo != null)
            {
                foreach (Prod_SubFolderStructureModel folder in FolderInfo)
                {


                    if (AppProcessViewModel.pageTypeId == form290PageId)//skumar-form290 - folder map 
                    {
                        folderNames = "/" + AppProcessViewModel.PropertyId.ToString() + "/" + AppProcessViewModel.FhaNumber + "/Production/ExecutedClosing/WorkProduct/Form290/";
                    }
                    else if (AppProcessViewModel.pageTypeId == ammendmentPageId)//skumar-2044 amendemtns=18
                    {
                        folderNames = "/" + AppProcessViewModel.PropertyId.ToString() + "/" + AppProcessViewModel.FhaNumber + "/Production/Closing/WorkProduct/Amendments/";
                    }
                    else if (FolderInfo[0].ViewTypeId == 2)
                    {
                        folderNames = "Production/" + PageName + "/" + "WorkProduct/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                    }

                    else
                    {
                        //Consider the case where there is no SubfolderSequence
                        //folderNames = "Production/" + PageName + "/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                        //folderNames = "Asset Management /"  +AppProcessViewModel.ProjectActionName +"/"+ AppProcessViewModel.PropertyId + "/" + AppProcessViewModel.FhaNumber + "/Asset Management/OPA/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy");
                        folderNames = "/" + AppProcessViewModel.PropertyId + "/" + AppProcessViewModel.FhaNumber + "/" + "Asset Management /" + AppProcessViewModel.ProjectActionName + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy");
                    }
                }

            }
            RestfulWebApiUploadModel[] obj1 = new RestfulWebApiUploadModel[1];
            var uploadmodel = new RestfulWebApiUploadModel()
            {
                propertyID = AppProcessViewModel.PropertyId.ToString(),
                indexType = "1",
                indexValue = AppProcessViewModel.FhaNumber,
                pdfConvertableValue = "false",
                folderKey = FolderKey,
                folderNames = folderNames,
                transactionType = AppProcessViewModel.ProjectActionTypeId.ToString(),
                transactionStatus = "",
                transactionDate = DateTime.UtcNow.ToString("MM-dd-yyyy")


            };
            obj1[0] = uploadmodel;
            //karri#162
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //request = WebApiTokenRequest.RequestToken();

            bool isUploaded = false;

            foreach (string Filename in Request.Files)
            {
                // var TEMP = Request.Files[Filename];
                //HttpPostedFileBase Temp;

                HttpPostedFileBase myFile = Request.Files[Filename];
                var t = myFile.InputStream;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();

                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = AppProcessViewModel.FhaNumber + "_" +
                                         AppProcessViewModel.ProjectActionTypeId + "_" +
                                         AppProcessViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);

                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {

                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = true;

                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    //karri#162,#171
                    request = WebApiTokenRequest.RequestToken();

                    if (AppProcessViewModel.pageTypeId == 18)
                    {
                        WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, FolderInfo[0].FolderName + "_" + FolderInfo[0].SubfolderSequence);
                    }
                    else
                    {
                        WebApiUploadResult = WebApiDocumentUpload.AssetManagementUploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");
                    }


                    if (System.IO.File.Exists(Path.Combine(pathForSaving, systemFileName)))
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));

                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {

                        isUploaded = true;
                        var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(AppProcessViewModel.GroupTaskInstanceId.ToString()));
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        // harish added 1404202 
                        if (myFile.FileName.ToUpper().StartsWith("OTHERS_") || myFile.FileName.ToUpper().StartsWith("OTHER_"))
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        if (WebApiUploadResult.documentType != DefaultDocID)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        else if (FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductClosing) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductUnderwriting) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductLoanCommittee))
                        {
                            //taskFile.DocTypeID = DefaultDocID;
                            taskFile.FileType = FileType.WP;
                        }
                        else if (AppProcessViewModel.pageTypeId == 3)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }

                        taskFile.RoleName = UserPrincipal.Current.UserRole;
                        taskFile.FolderName = folderNames;
                        var taskfileid = taskManager.SaveGroupTaskFileModel(taskFile);
                        var mappingmodel = new TaskFile_FolderMappingModel();
                        mappingmodel.FolderKey = FolderKey;
                        mappingmodel.TaskFileId = taskfileid;
                        mappingmodel.CreatedOn = DateTime.UtcNow;

                        taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);

                        //Code for saving new files on RAI
                        if (AppProcessViewModel.TaskGuid != null)
                        {
                            var childTaskNewFile = new ChildTaskNewFileModel();
                            childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
                            childTaskNewFile.ChildTaskInstanceId = AppProcessViewModel.TaskGuid.Value;
                            childTaskNewFile.TaskFileInstanceId = taskfileid;
                            projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);

                        }
                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        isUploaded = false;

                    }


                }
            }
            return isUploaded;
        }

        [ValidateInput(false)]
        private bool RAIUpload(OPAViewModel AppProcessViewModel, int FolderKey)
        {
            var FolderInfo = projectActionFormManager.GetFolderInfoByKey(FolderKey);
            //AppProcessViewModel.pageTypeId = 3;
            AppProcessViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(AppProcessViewModel.FhaNumber).PropertyId;
            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(AppProcessViewModel.ProjectActionTypeId);
            var PageName = projectActionFormManager.FormNameBYPageTypeID(AppProcessViewModel.pageTypeId);
            string folderNames = string.Empty;
            string formNameConfiguration = ConfigurationManager.AppSettings["FORM 290"] ?? ConfigurationSettings.AppSettings["FORM 290"];
            string amendmentConfig = ConfigurationManager.AppSettings["Amendments"] ?? ConfigurationSettings.AppSettings["Amendments290"];
            int ammendmentPageId = !string.IsNullOrEmpty(amendmentConfig) ? Convert.ToInt32(amendmentConfig) : 0;
            int form290PageId = !string.IsNullOrEmpty(formNameConfiguration) ? Convert.ToInt32(formNameConfiguration) : 0;
            if (FolderInfo != null)
            {
                foreach (Prod_SubFolderStructureModel folder in FolderInfo)
                {


                    if (AppProcessViewModel.pageTypeId == form290PageId)//skumar-form290 - folder map 
                    {
                        folderNames = "/" + AppProcessViewModel.PropertyId.ToString() + "/" + AppProcessViewModel.FhaNumber + "/Production/ExecutedClosing/WorkProduct/Form290/";
                    }
                    else if (AppProcessViewModel.pageTypeId == ammendmentPageId)//skumar-2044 amendemtns=18
                    {
                        folderNames = "/" + AppProcessViewModel.PropertyId.ToString() + "/" + AppProcessViewModel.FhaNumber + "/Production/Closing/WorkProduct/Amendments/";
                    }
                    else if (FolderInfo[0].ViewTypeId == 2)
                    {
                        folderNames = "Production/" + PageName + "/" + "WorkProduct/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                    }

                    else
                    {
                        //Consider the case where there is no SubfolderSequence
                        //folderNames = "Production/" + PageName + "/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                        //folderNames = "Asset Management /"  +AppProcessViewModel.ProjectActionName +"/"+ AppProcessViewModel.PropertyId + "/" + AppProcessViewModel.FhaNumber + "/Asset Management/OPA/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy");
                        //folderNames = "/" + AppProcessViewModel.PropertyId + "/" + AppProcessViewModel.FhaNumber + "/" + "Asset Management /" + AppProcessViewModel.ProjectActionName + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy");
                        folderNames = "/" + AppProcessViewModel.PropertyId + "/" + AppProcessViewModel.FhaNumber + "/" + "Asset Management /" + AppProcessViewModel.ProjectActionName + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy");

                    }
                }

            }
            RestfulWebApiUploadModel[] obj1 = new RestfulWebApiUploadModel[1];

            var uploadmodel = new RestfulWebApiUploadModel()
            {
                propertyID = AppProcessViewModel.PropertyId.ToString(),
                indexType = "1",
                indexValue = AppProcessViewModel.FhaNumber,
                pdfConvertableValue = "false",
                folderKey = FolderKey,
                folderNames = folderNames,
                transactionType = AppProcessViewModel.ProjectActionTypeId.ToString(),
                transactionStatus = "",
                transactionDate = DateTime.UtcNow.ToString("MM-dd-yyyy")
            };
            obj1[0] = uploadmodel;
            //karri#162
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //request = WebApiTokenRequest.RequestToken();

            bool isUploaded = false;

            foreach (string Filename in Request.Files)
            {
                // var TEMP = Request.Files[Filename];
                //HttpPostedFileBase Temp;

                HttpPostedFileBase myFile = Request.Files[Filename];
                var t = myFile.InputStream;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();

                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = AppProcessViewModel.FhaNumber + "_" +
                                         AppProcessViewModel.ProjectActionTypeId + "_" +
                                         AppProcessViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);

                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {

                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = true;

                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    //karri#162,#171
                    request = WebApiTokenRequest.RequestToken();

                    if (AppProcessViewModel.pageTypeId == 18)
                    {
                        WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, FolderInfo[0].FolderName + "_" + FolderInfo[0].SubfolderSequence);
                    }
                    else
                    {
                        WebApiUploadResult = WebApiDocumentUpload.AssetManagementUploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");
                    }


                    if (System.IO.File.Exists(Path.Combine(pathForSaving, systemFileName)))
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));

                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {

                        isUploaded = true;
                        var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(AppProcessViewModel.GroupTaskInstanceId.ToString()));
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        // harish added 1404202 
                        if (myFile.FileName.ToUpper().StartsWith("OTHERS_") || myFile.FileName.ToUpper().StartsWith("OTHER_"))
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        if (WebApiUploadResult.documentType != DefaultDocID)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        else if (FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductClosing) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductUnderwriting) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductLoanCommittee))
                        {
                            //taskFile.DocTypeID = DefaultDocID;
                            taskFile.FileType = FileType.WP;
                        }
                        else if (AppProcessViewModel.pageTypeId == 3)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }

                        taskFile.RoleName = UserPrincipal.Current.UserRole;
                        taskFile.FolderName = folderNames;
                        var taskfileid = taskManager.SaveGroupTaskFileModel(taskFile);
                        var mappingmodel = new TaskFile_FolderMappingModel();
                        mappingmodel.FolderKey = FolderKey;
                        mappingmodel.TaskFileId = taskfileid;
                        mappingmodel.CreatedOn = DateTime.UtcNow;

                        taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);

                        //Code for saving new files on RAI
                        if (AppProcessViewModel.TaskGuid != null)
                        {
                            var childTaskNewFile = new ChildTaskNewFileModel();
                            childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
                            childTaskNewFile.ChildTaskInstanceId = AppProcessViewModel.TaskGuid.Value;
                            childTaskNewFile.TaskFileInstanceId = taskfileid;
                            projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);

                        }
                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        isUploaded = false;

                    }


                }
            }
            return isUploaded;
        }
        //#664
        [ValidateInput(false)]
        public ActionResult Reload()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.pageTypeId = 3;
            string GroupTaskInstanceId = Request.Params[0];
            var tr = Request.Params[1];
            //var fr = Request.Params[2];
            //string ProjectActionTypeId = string.Empty;
            //ProjectActionTypeId = Request.Form["ProjectActionTypeId"];
            //var projectactiontypeid = Request.QueryString[2];
            // AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
            foreach (var model in projectActionList)
            {
                AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
                {
                    Text = model.ProjectActionName,
                    Value = model.ProjectActionID.ToString()
                });
            }
            //projectActionList = (from q in  ProjectInfoModel);

            //var result = projectActionFormManager.GetProdPropertyInfo(AppProcessViewModel.FhaNumber);
            var result = projectActionFormManager.GetPropertyInfo(AppProcessViewModel.FhaNumber);
            if (result != null)
            {
                AppProcessViewModel.PropertyId = result.PropertyId;
                AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                AppProcessViewModel.PropertyAddress.StateCode = result.State;
                AppProcessViewModel.PropertyAddress.City = result.City;
                AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
            }
            AppProcessViewModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
            //  AppProcessViewModel.ProjectActionTypeId;
            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(AppProcessViewModel.ProjectActionTypeId);
            AppProcessViewModel.IsProjectActionChanged = true;

            // var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            var grouptaskmodel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            if (grouptaskmodel != null)
            {
                AppProcessViewModel.ServicerComments = grouptaskmodel.ServicerComments;
                AppProcessViewModel.pageTypeId = (int)PageType.OPA;
            }
            GetDisclaimerText(AppProcessViewModel);
            //AppProcessViewModel.pageTypeId = (int)PageType.ProductionApplication; 
            if (TempData["UploadFailed"] != null)
            {
                ViewBag.UploadFailed = TempData["UploadFailed"].ToString();
            }



            //else if (AppProcessViewModel.pageTypeId == (int)PageType.Amendments)
            //{
            //    ViewBag.ConstructionTitle = "Amendments";
            //    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessViewModel);
            //}

            // PopulateFHANumberList(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            PopulateFHANumberList(AppProcessViewModel);
            GetPropertyInfo(AppProcessViewModel.FhaNumber);
            return View("~/Views/OPAForm/OPARequestForm.cshtml", AppProcessViewModel);
            //}
        }

        //#664
        //delete functioanlity
        public void MultipleDelete(string taskFileIDs)
        {
            var taskFileIdList = taskFileIDs.Split(',');
            foreach (var taskFileId in taskFileIdList)
            {
                DeleteFile(taskFileId);
            }

        }
        //#664

        public ActionResult OPAFacade(OPAViewModel AppProcessViewModel, string submitButton, string submitButton1 = null)
        {



            //skumar- remove duplicate fha numbers
            if (!string.IsNullOrEmpty(AppProcessViewModel.FhaNumber))
            {
                var fhaNumbers = AppProcessViewModel.FhaNumber.Split(',');

                if (fhaNumbers.Length > 0)
                {
                    AppProcessViewModel.FhaNumber = fhaNumbers[0];
                }
            }
            //if (submitButton1 == "Submit")
            //{
            //    submitButton = submitButton1;
            //}
            //if (submitButton == "Submit")
            //{
            //    submitButton1 = submitButton;
            //}

            switch (submitButton)
            {
                case "Submit":

                    return (Submit(AppProcessViewModel));

                case "Save":

                    return Save(AppProcessViewModel);
                case @"Save and Check-in":

                    return (SaveCheckin(AppProcessViewModel));

                default:

                    return (SaveCheckin(AppProcessViewModel));
            }
        }

        //#664
        public ActionResult SaveDropzoneJsUploadedFiles1(FolderUploadModel fileFolderModel)
        {
            OPAViewModel AppProcessModel = new OPAViewModel();

            int userID = 0;
            string userModel = string.Empty;
            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;

            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            string value = string.Empty;
            string GroupTaskInstanceId = string.Empty;
            int pageTypeId = 0;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();

            //string disclaimer = string.Empty;
            //disclaimer = Request.Form["disclaimer"];

            //if (disclaimer.ToUpper() == "TRUE")
            //{
            //    AppProcessModel.IsAgreementAccepted = true;
            //}
            //else
            //{
            //    AppProcessModel.IsAgreementAccepted = false;
            //}

            //var addressschanged = string.Empty;

            //addressschanged = Request.Form["addressschanged"];


            bool isUploaded = false;
            string message = "File upload failed";
            string groupTaskInstanceId = string.Empty;

            string FHA = string.Empty;
            string PropertyName = string.Empty;
            string requestDate = string.Empty;
            string RequesterName = string.Empty;
            string ServicerSubmissionDate = string.Empty;
            string GroupTaskId = string.Empty;
            string Mode = string.Empty;
            string ProjectActionTypeId = string.Empty;
            FHA = Request.Form["FHA"];
            PropertyName = Request.Form["PropertyName"];
            requestDate = Request.Form["requestDate"];
            RequesterName = Request.Form["RequesterName"];
            ServicerSubmissionDate = Request.Form["ServicerSubmissionDate"];
            GroupTaskId = Request.Form["GroupTaskId"];
            ProjectActionTypeId = Request.Form["ProjectActionTypeId"];
            var OPAModel = new OPAViewModel();
            string IsViewFromGroupTask = string.Empty;
            IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];



            //Populate OPAProjectActionViewModel

            AppProcessModel.FhaNumber = FHA;
            //if (!string.IsNullOrEmpty(ProjectActionTypeId))
            //{
            //    OPAProjectActionViewModel.ProjectActionTypeId = Convert.ToInt32(ProjectActionTypeId);
            //}

            //OPAProjectActionViewModel.PropertyName = PropertyName;
            //OPAProjectActionViewModel.RequestDate = Convert.ToDateTime(requestDate);
            //OPAProjectActionViewModel.ServicerSubmissionDate = Convert.ToDateTime(ServicerSubmissionDate);
            //if(!string.IsNullOrEmpty(GroupTaskId))
            //{
            //    OPAProjectActionViewModel.GroupTaskId =  Convert.ToInt32(GroupTaskId);
            //}
            string data = Request.QueryString[0];
            string taskid = string.Empty;
            //taskid
            var arr = data.Split(',');
            if (arr.Length > 1)
            {
                taskid = arr[0];
            }
            else
            {
                taskid = data;
            }
            if (!string.IsNullOrEmpty(taskid))
            {
                AppProcessModel.GroupTaskId = Convert.ToInt32(taskid);
            }

            if (ModelState.IsValid)
            {
                var groupTaskModel = new GroupTaskModel();
                if (AppProcessModel != null)
                {

                    if (AppProcessModel.GroupTaskId == null)
                    {
                        groupTaskModel.TaskInstanceId = Guid.NewGuid();
                        groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.FhaNumber = AppProcessModel.FhaNumber;
                        groupTaskModel.PropertyName = AppProcessModel.PropertyName;
                        groupTaskModel.RequestDate = AppProcessModel.RequestDate;
                        groupTaskModel.RequesterName = AppProcessModel.RequesterName;
                        groupTaskModel.ServicerSubmissionDate = AppProcessModel.ServicerSubmissionDate;
                        groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                        groupTaskModel.ProjectActionTypeId = AppProcessModel.ProjectActionTypeId;
                        groupTaskModel.IsDisclimerAccepted = AppProcessModel.IsAgreementAccepted;
                        groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.CreatedOn = DateTime.UtcNow;
                        groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedOn = DateTime.UtcNow;
                        groupTaskModel.ServicerComments = AppProcessModel.ServicerComments;
                        AppProcessModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                        AppProcessModel.IsEditMode = true;
                        AppProcessModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.TaskId = (int)AppProcessModel.GroupTaskId;
                        AppProcessModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                        AppProcessModel.GroupTaskCreatedOn = DateTime.UtcNow;
                        AppProcessModel.ProjectActionDate = groupTaskModel.ProjectActionStartDate.Value;
                    }
                    else
                    {
                        OPAModel =
                            groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
                                Convert.ToInt32(AppProcessModel.GroupTaskId));
                        OPAModel.ServicerComments = AppProcessModel.ServicerComments;
                        OPAModel.ServicerSubmissionDate = AppProcessModel.ServicerSubmissionDate;
                        OPAModel.PropertyId = AppProcessModel.PropertyId;
                        groupTaskManager.UpdateOPAGroupTask(OPAModel);

                        AppProcessModel.GroupTaskInstanceId = OPAModel.GroupTaskInstanceId;
                        // OPAProjectActionViewModel.GroupTaskInstanceId =  new Guid(Request.QueryString[0]);
                        //OPAProjectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                    }
                    AppProcessModel.ProjectActionName =
                       appProcessManager.GetProjectActionTypebyID(AppProcessModel.ProjectActionTypeId);
                }
                //return View("~/Views/ProjectAction/ProjectActionUploadCheckList.cshtml", OPAProjectActionViewModel);
            }

            //From Group task

            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel =
                    groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
                        Convert.ToInt32(AppProcessModel.GroupTaskId));
                OPAModel.ServicerComments = AppProcessModel.ServicerComments;
                OPAModel.ServicerSubmissionDate = AppProcessModel.ServicerSubmissionDate;

                groupTaskManager.UpdateOPAGroupTask(OPAModel);

                AppProcessModel.ProjectActionName =
                   appProcessManager.GetProjectActionTypebyID(AppProcessModel.ProjectActionTypeId);

                AppProcessModel.ProjectActionTypeId = OPAModel.ProjectActionTypeId;
                AppProcessModel.GroupTaskInstanceId = OPAModel.GroupTaskInstanceId;
                OPAModel.IsAgreementAccepted = false; // User Story 1901

            }
            else
            {
                OPAModel.IsAgreementAccepted = true; // User Story 1901
            }

            // Random randoms = new Random();
            //foreach (string Filename in Request.Files)
            //{
            //    HttpPostedFileBase myFile = Request.Files[Filename];
            //    if (myFile != null && myFile.ContentLength != 0)
            //    {
            //        double fileSize = (myFile.ContentLength) / 1024;


            //        var UniqueId = randoms.Next(0, 99999);
            //        // Debug.WriteLine("My debug string here" + UniqueId);

            //        var systemFileName = OPAProjectActionViewModel.FhaNumber + "_" +
            //                            OPAProjectActionViewModel.ProjectActionTypeId + "_" +
            //                            OPAProjectActionViewModel.GroupTaskInstanceId + "_" + UniqueId +
            //                            Path.GetExtension(myFile.FileName);
            //        var tModel = taskManager.GetTaskFileByTaskFileName(systemFileName.ToString());
            //        var taskModel = taskManager.GetTaskFileByTaskFileType(systemFileName.ToString());
            //        while (taskModel != null || tModel != null)
            //        {
            //            UniqueId = randoms.Next(0, 99999);
            //            taskModel = taskManager.GetTaskFileByTaskFileType(UniqueId.ToString());
            //            //OPAProjectActionViewModel.GroupTaskInstanceId = Guid.NewGuid();

            //        }




            //        string pathForSaving = Server.MapPath("~/Uploads");
            //        if (this.CreateFolderIfNeeded(pathForSaving))
            //        {
            //            try
            //            {
            //                // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
            //                myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
            //                isUploaded = true;
            //                message = "File uploaded successfully!";
            //            }
            //            catch (Exception ex)
            //            {
            //                message = string.Format("File upload failed: {0}", ex.Message);
            //            }
            //        }
            //        var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
            //            DateTime.UtcNow.ToString(), systemFileName.ToString(),
            //            new Guid(OPAProjectActionViewModel.GroupTaskInstanceId.ToString()));

            //        //var taskFileModel = taskManager.GetGroupTaskFileByTaskInstanceAndFileTypeId(new Guid(groupTaskInstanceId), new Guid(chkListId));
            //        //if (taskFileModel != null)
            //        //{
            //        //    taskManager.DeleteGroupTaskFile(new Guid(groupTaskInstanceId), new Guid(chkListId));
            //        //}

            //        taskManager.SaveGroupTaskFileModel(taskFile);

            //        OPAModel =
            //            groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(
            //                Convert.ToInt32(OPAProjectActionViewModel.GroupTaskId));
            //        OPAModel.ProjectActionName =
            //            projectActionFormManager.GetProjectActionName(OPAProjectActionViewModel.ProjectActionTypeId);
            //        var taskFileModel =
            //            taskManager.GetAllTaskFileByTaskInstanceId(
            //                new Guid(OPAProjectActionViewModel.GroupTaskInstanceId.ToString()));
            //        OPAModel.TaskFileList = taskFileModel.ToList();

            //        IList<ProjectActionTypeViewModel> projectActionList =
            //            projectActionManager.GetAllProjectActionsByPageId(3);
            //        foreach (var model in projectActionList)
            //        {
            //            OPAModel.ProjectActionTypeList.Add(new SelectListItem()
            //            {
            //                Text = model.ProjectActionName,
            //                Value = model.ProjectActionID.ToString()
            //            });
            //        }

            //    }
            //}
            var success = this.Upload(AppProcessModel, Convert.ToInt32(FolderKey));
            if (success)
            {

                var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                if (result != null)
                {
                    OPAModel.PropertyId = result.PropertyId;
                    OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                    OPAModel.PropertyAddress.City = result.City ?? "";
                    OPAModel.PropertyAddress.StateCode = result.State ?? "";
                    OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";
                }

                OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);

            }
            else
            {
                ViewBag.UploadFailed = "True";
                TempData["UploadFailed"] = "True";

            }
            //if (addressschanged.ToUpper() == "TRUE")
            //{
            //    OPAModel.IsAddressChange = true;
            //}

            GetDisclaimerText(OPAModel); // User Story 1901
            if (IsViewFromGroupTask.ToUpper().Contains("TRUE"))
            {
                OPAModel.IsViewFromGroupTask = true;

                return PartialView("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", OPAModel);
            }
            OPAModel.IsAgreementAccepted = false; // User Story 1901
            return PartialView("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);

        }

        //#664
        // save
        private ActionResult Submit(OPAViewModel model)
        {
            var OPAModel = new OPAViewModel();
            var TaskInstanceId = Request.Form["GroupTaskInstanceId"];
            //harish added this new method to check newfilename error message
            var FileList = (from filelist in taskManager.GetAllTaskFileByTaskInstanceId(new Guid(TaskInstanceId))
                            select new
                            {
                                filelist.FileId,
                                filelist.DocTypeID,
                                docId = filelist.DocId,
                                Name = filelist.FileName
                            }).Where(m => string.IsNullOrEmpty(m.DocTypeID));


            if (FileList.Count() > 0)
            {
                //if(AppProcessModel.pageTypeId==18)
                OPAModel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(TaskInstanceId));
                IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
                foreach (var pr in projectActionList)
                {
                    OPAModel.ProjectActionTypeList.Add(new SelectListItem()
                    {
                        Text = pr.ProjectActionName,
                        Value = pr.ProjectActionID.ToString()
                    });
                }
                OPAModel.IsChangeDocTypePopupDisplayed = true;

                OPAModel.IsAddressChange = model.IsAddressChange;
                var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                if (result != null)
                {
                    OPAModel.PropertyId = result.PropertyId;
                    OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                    OPAModel.PropertyAddress.StateCode = result.State;
                    OPAModel.PropertyAddress.City = result.City;
                    OPAModel.PropertyAddress.ZIP = result.Zipcode;
                }
                OPAModel.GroupTaskInstanceId = new Guid(TaskInstanceId);
                //  AppProcessViewModel.ProjectActionTypeId;
                OPAModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(OPAModel.ProjectActionTypeId);
                GetDisclaimerText(OPAModel);
                PopulateFHANumberList(OPAModel);
                GetPropertyInfo(OPAModel.FhaNumber);
                TempData["AppProcessModel"] = OPAModel;
                if (OPAModel.pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", OPAModel);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);
                }
            }









            //string FHA = string.Empty;
            //string requestDate = string.Empty;
            //var ServicerComments = Request.Form["ServicerComments"];
            //var addressschanged = Request.Form["addressschanged"];
            //requestDate = Request.Form["requestDate"];
            //FHA = Request.Form["ddlFHANumbers"];
            //string addresschange = string.Empty;
            var addresschange = model.IsAddressChange.ToString();
            if (addresschange == "False")
            {
                model.IsAddressChange = true;
            }
            if (ModelState.IsValid)
            {
                OPAModel.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(model.ProjectActionTypeId);
                OPAModel.ServicerComments = model.ServicerComments;
                OPAModel.PropertyName = model.PropertyName;
                OPAModel.RequesterName = model.RequesterName;
                OPAModel.ProjectActionTypeId = model.ProjectActionTypeId;
                //  var  FHA = Request.Form["FHA"];
                OPAModel.IsAddressChange = model.IsAddressChange;
                //model.FhaNumber = FHA;
                //Insert the non critical request and get the generated id, insert into view model
                OPAModel.FhaNumber = model.FhaNumber;
                OPAModel.ServicerSubmissionDate = DateTime.UtcNow;
                OPAModel.RequestStatus = (int)RequestStatus.Submit;
                OPAModel.CreatedOn = DateTime.UtcNow;
                OPAModel.ModifiedOn = DateTime.UtcNow;
                OPAModel.CreatedBy = UserPrincipal.Current.UserId;
                OPAModel.ModifiedBy = UserPrincipal.Current.UserId;
                OPAModel.ProjectActionDate = DateTime.UtcNow;
                //OPAModel.RequestDate = Convert.ToDateTime(model.RequestDate);
                //OPAModel.RequestDate = DateTime.UtcNow;
                //var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAModel);
                //OPAModel.ProjectActionFormId = projectActionFormId;
                //Added to prvent saving in opa form@29042020
                if (model.RequestDate != null)
                {
                    OPAModel.RequestDate = Convert.ToDateTime(model.RequestDate);
                }
                else
                {
                    OPAModel.RequestDate = DateTime.UtcNow;
                }
                // OPAModel.RequestDate = DateTime.UtcNow;
                OPAModel.ProjectActionFormId = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(TaskInstanceId)).ProjectActionFormId;
                OPAModel.SequenceId = 0;
                OPAModel.IsAgreementAccepted = model.IsAgreementAccepted;
                OPAModel.RequesterName = model.RequesterName;
                OPAModel.IsChangeDocTypePopupDisplayed = false;
                var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                if (result != null)
                {
                    OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                    OPAModel.PropertyAddress.City = result.City ?? "";
                    OPAModel.PropertyAddress.StateCode = result.State ?? "";
                    OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

                }
                var taskList = new List<TaskModel>();
                var task = new TaskModel();
                //var InstanceId = Guid.NewGuid();
                // var InstanceId = model.GroupTaskInstanceId;
                task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(OPAModel.FhaNumber);
                OPAModel.TaskInstanceId = new Guid(TaskInstanceId);
                task.AssignedBy = UserPrincipal.Current.UserName;

                task.Notes = model.ServicerComments ?? string.Empty;
                task.SequenceId = 0;
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = OPAModel.TaskInstanceId;

                var tempmodel = new OPAViewModel();
                if (model.GroupTaskId == null)
                {
                    var grouptaskid = groupTaskManager.GroupTaskbyTaskinstanceid(OPAModel.TaskInstanceId);
                    if (grouptaskid != null)
                    {
                        model.GroupTaskId = grouptaskid.TaskId;
                    }


                }
                if (model.GroupTaskId != null)
                {
                    tempmodel =
                        groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(model.GroupTaskId));
                    task.TaskInstanceId = tempmodel.GroupTaskInstanceId.Value;
                    tempmodel.RequestStatus = (int)RequestStatus.Submit;
                    OPAModel.TaskInstanceId = tempmodel.GroupTaskInstanceId.Value;
                }
                task.DataStore1 = XmlHelper.Serialize(OPAModel, typeof(OPAViewModel), Encoding.Unicode);
                //adding fhanumber and pagetype 
                task.FHANumber = OPAModel.FhaNumber;
                task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");

                task.TaskStepId = (int)TaskStep.ProjectActionRequest;
                taskList.Add(task);

                var taskFile = new TaskFileModel();
                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    //Latest task id been updated on submission
                    OPAModel.TaskInstanceId = task.TaskInstanceId;
                    projectActionFormManager.UpdateProjectActionRequestForm(OPAModel);
                    OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    projectActionFormManager.UpdateTaskId(OPAModel);

                    // Save ReviewFileStatus
                    string assignedto = task.AssignedTo;
                    int userId = webSecurity.GetUserId(assignedto);
                    Guid taskInstanceID = task.TaskInstanceId;
                    int reviewerUserId = userId;
                    int currentUserId = UserPrincipal.Current.UserId;
                    // added by harish to differentaite the upload files 24-01-2020
                    taskManager.SaveOPAReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);

                    //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                    //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);

                    if (tempmodel != null)
                        groupTaskManager.UpdateOPAGroupTaskByModel(tempmodel);
                    // projectActionFormManager.InsertAEChecklistStatus(model);

                    if (OPAModel.ProjectActionName.Equals("TPA (Light) - Preliminary approval "))
                    {
                        backgroundJobManager.SendTPALightPreliminarySubmissionEmail(emailManager, task.AssignedTo,
                            accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                    }
                    else
                    {

                        backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo,
                            accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                    }

                    if (model.IsAddressChange == false)
                    {
                        backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager,
                            "The property address is not listed correctly",
                                  OPAModel.PropertyName, OPAModel.FhaNumber);
                    }
                    UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                    string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);
                    return RedirectToAction("MyTasks", "Task");
                }
                catch (Exception ex)
                {
                }
            }

            else
            {
                var IsViewFromGroupTask = Request.Form["IsViewFromGroupTask"];
                if (ModelState.IsValid || IsViewFromGroupTask.ToUpper().Contains("TRUE"))
                {

                    OPAModel =
                        groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(Convert.ToInt32(model.GroupTaskId));
                    OPAModel.ProjectActionName =
                        projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAModel.ProjectActionTypeId);
                    var taskFileModel =
                        taskManager.GetAllTaskFileByTaskInstanceId(new Guid(OPAModel.GroupTaskInstanceId.ToString()));
                    OPAModel.TaskFileList = taskFileModel.ToList();
                    OPAModel.ServicerComments = model.ServicerComments;
                    OPAModel.RequesterName = model.RequesterName;

                    OPAModel.IsAddressChange = model.IsAddressChange;
                    //  var  FHA = Request.Form["FHA"];

                    //model.FhaNumber = FHA;
                    //Insert the non critical request and get the generated id, insert into view model
                    OPAModel.ServicerSubmissionDate = DateTime.UtcNow;
                    OPAModel.RequestStatus = (int)RequestStatus.Submit;
                    OPAModel.CreatedOn = DateTime.UtcNow;
                    OPAModel.ModifiedOn = DateTime.UtcNow;
                    OPAModel.CreatedBy = UserPrincipal.Current.UserId;
                    OPAModel.ModifiedBy = UserPrincipal.Current.UserId;
                    OPAModel.ProjectActionDate = DateTime.UtcNow;
                    OPAModel.IsChangeDocTypePopupDisplayed = false;// harish added 06042020
                    var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAModel);
                    OPAModel.ProjectActionFormId = projectActionFormId;
                    groupTaskManager.UpdateOPAGroupTask(OPAModel);
                    var taskList = new List<TaskModel>();
                    var task = new TaskModel();
                    OPAModel.SequenceId = 0;
                    OPAModel.IsAgreementAccepted = model.IsAgreementAccepted;
                    var result = projectActionFormManager.GetPropertyInfo(OPAModel.FhaNumber);
                    if (result != null)
                    {
                        OPAModel.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                        OPAModel.PropertyAddress.City = result.City ?? "";
                        OPAModel.PropertyAddress.StateCode = result.State ?? "";
                        OPAModel.PropertyAddress.ZIP = result.Zipcode ?? "";

                    }

                    task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(OPAModel.FhaNumber);
                    OPAModel.TaskInstanceId = OPAModel.GroupTaskInstanceId.Value;
                    task.AssignedBy = UserPrincipal.Current.UserName;
                    task.DataStore1 = XmlHelper.Serialize(OPAModel, typeof(OPAViewModel), Encoding.Unicode);
                    task.Notes = model.ServicerComments ?? string.Empty;
                    task.SequenceId = 0;
                    task.StartTime = DateTime.UtcNow;
                    //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = OPAModel.GroupTaskInstanceId.Value;
                    //adding fhanumber and pagetype 
                    task.FHANumber = OPAModel.FhaNumber;
                    task.PageTypeId = projectActionFormManager.GetPageTypeIdByName("OPA");
                    task.TaskStepId = (int)TaskStep.ProjectActionRequest;
                    taskList.Add(task);
                    var taskFile = new TaskFileModel();
                    try
                    {
                        taskManager.SaveTask(taskList, taskFile);
                        //Latest task id been updated on submission
                        OPAModel.TaskInstanceId = task.TaskInstanceId;
                        projectActionFormManager.UpdateProjectActionRequestForm(OPAModel);
                        OPAModel.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        projectActionFormManager.UpdateTaskId(OPAModel);
                        //Review Filestatus Saving
                        string assignedto = task.AssignedTo;
                        int userId = webSecurity.GetUserId(assignedto);
                        Guid taskInstanceID = task.TaskInstanceId;
                        int reviewerUserId = userId;
                        int currentUserId = UserPrincipal.Current.UserId;
                        // harish added new properties for upload 24-01-2020
                        taskManager.SaveOPAReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);

                        //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        // taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        // projectActionFormManager.InsertAEChecklistStatus(model);
                        //*************** Place a condition here for the TPA (light)  ********************

                        if (OPAModel.ProjectActionName.Equals("TPA (Light) - Preliminary approval "))
                        {
                            backgroundJobManager.SendTPALightPreliminarySubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }
                        else
                        {

                            backgroundJobManager.SendOPASubmissionEmail(emailManager, task.AssignedTo,
                                accountManager.GetUserById(OPAModel.CreatedBy).UserName, OPAModel);
                        }
                        //Send Mail on address Change
                        if (model.IsAddressChange == false)
                        {
                            backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager,
                                "The property address is not listed correctly",
                                      OPAModel.PropertyName, OPAModel.FhaNumber);
                        }
                        UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                        string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);
                        return RedirectToAction("MyTasks", "Task");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            OPAModel.OpaHistoryViewModel = GetOpaHistory((Guid)OPAModel.GroupTaskInstanceId);
            OPAModel.IsAddressChange = model.IsAddressChange;
            return View("~/Views/OPAForm/OPARequestForm.cshtml", OPAModel);

            //Guid? InstanceId = Guid.NewGuid();
            //var taskList = new List<TaskModel>();
            //var task = new TaskModel();
            //ProductionTaskAssignmentModel model = new ProductionTaskAssignmentModel();
            //if (Exists)
            //{
            //    InstanceId = AppProcessModel.GroupTaskInstanceId;
            //}

            //task.AssignedBy = UserPrincipal.Current.UserName;

            //task.Notes = AppProcessModel.ServicerComments ?? string.Empty;
            //task.SequenceId = 0;
            //task.StartTime = DateTime.UtcNow;
            //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
            //task.TaskInstanceId = InstanceId.Value;
            //if (AppProcessModel.FhaNumber.Length > 9 && AppProcessModel.FhaNumber.Contains(','))
            //{
            //    task.FHANumber = AppProcessModel.FhaNumber.Substring(0, 9);
            //}
            //else
            //{
            //    task.FHANumber = AppProcessModel.FhaNumber;
            //}

            //if (AppProcessModel.pageTypeId == 3)
            //{
            //    //Prod_Assainedto Assainedto = new Prod_Assainedto();
            //    //Assainedto = projectActionFormManager.AssignedTo(AppProcessModel.pageTypeId, task.FHANumber);
            //    //task.AssignedTo = Assainedto.UserName;
            //    task.TaskStepId = 15;


            //    var currentUser = UserPrincipal.Current;

            //    model.CurrentUserId = currentUser.UserId;
            //    model.UserName = currentUser.UserName;
            //    model.Status = (int)ProductionFhaRequestStatus.Completed;
            //    //model.AssignedTo = Assainedto.UserID;
            //    model.AssignedBy = currentUser.UserId;
            //    model.AssignedToUserId = currentUser.UserId;
            //    model.AssignedToViewId = 1;
            //    model.ModifiedBy = currentUser.UserId;
            //    model.ModifiedOn = DateTime.UtcNow;
            //    model.CompletedOn = DateTime.UtcNow;
            //    model.Status = 15;
            //    model.ViewId = 1;
            //    model.IsReviewer = false;
            //    model.TaskXrefid = Guid.NewGuid();
            //    model.TaskInstanceId = task.TaskInstanceId;
            //    //if (model.TaskInstanceId == Guid.Empty)
            //    //    prod_TaskXrefManager.AssignProductionFhaRequest(model);
            //    //else productionQueue.AssignProductionFhaInsert(model);


            //}
            //else
            //{
            //    task.AssignedTo = null;
            //    task.TaskStepId = 17;
            //}

            ////task.AssignedTo = null;
            //task.PageTypeId = AppProcessModel.pageTypeId;
            ////task.PageTypeId = (int)PageType.ProductionApplication; 
            ////task.TaskStepId = (int)ProductionFhaRequestStatus.InQueue;
            ////task.TaskStepId = 17;
            //taskList.Add(task);
            //var taskFile = new TaskFileModel();
            //if (taskList.Count > 0)
            //{
            //    taskManager.SaveTask(taskList, taskFile);


            //}
            //if (AppProcessModel.pageTypeId == 17)
            //{
            //    string TaskId = projectActionFormManager.GetTaskId(AppProcessModel.pageTypeId, task.FHANumber, model.TaskInstanceId);
            //    model.TaskId = Convert.ToInt32(TaskId);
            //   // prod_TaskXrefManager.AddTaskXref(model);

            //    //var pdfgenerated = GenerateSharepointPdf(model.TaskInstanceId, task.FHANumber, model.PropertyId);
            //}
        }
        //#664
        //grid
        public JsonResult GetMyTaskProdLenderUploadFileGridContent(Guid? taskInstanceId, string PropertyId, string FhaNumber)
        {


            //var GridContent = projectActionFormManager.GetProdLenderUploadForAssetManagement(taskInstanceId.Value, PropertyId, FhaNumber);
            var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId.Value, PropertyId, FhaNumber);
            GridContent = GridContent.Where(x => x.id != "32").ToList();

            if (GridContent != null)
            {
                // GridContent = GridContent.Where(x => x.PageTypeId == Convert.ToInt32(FormName) || x.PageTypeId == 3).ToList();
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    //item.level = 0;
                    if (item.fileId != null)
                    {
                        var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
                        if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                        || currentUserRole == "BackupAccountManager")
                            item.isUploadedByMe = true;
                    }

                    //if (!string.IsNullOrEmpty(item.parent))
                    //{
                    //    var DocTypeList = appProcessManager.GetMappedDocTypesbyFolder(Convert.ToInt32(item.parent));

                    //    if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                    //    {

                    //        item.DocTypesList = GetDocTypesdropdownlist(DocTypeList, item.fileId);
                    //    }

                    //    //item.DocTypesList = "<select data-val='true' data-val-number='The field BorrowerTypeId must be a number." +
                    //    //                "data-val-required='The BorrowerTypeId field is required.' id=_'ddlDocType'  name='BorrowerTypeId'>" +
                    //    //                "<option value=''>Select Document Type</option>" +
                    //    //                "<option value='1'>Profit</option>" +
                    //    //                "<option value='2'>Non-Profit</option></select>";

                    //}

                    else
                    {
                        if (item.SubfolderSequence != null)
                        {
                            item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                        }
                    }
                }
            }

            // int pageIndex = Convert.ToInt32(page) - 1;
            //int pageSize = rows;
            //int totalrecods = GridContent.Count();
            //var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var result = GridContent.ToList();//.OrderBy(a => a.id);
                                              // var results = result.Skip(pageIndex * pageSize).Take(pageSize);
                                              //var jsonData = new
                                              //{
                                              //    total = totalpages,
                                              //    page,s
                                              //    records = totalrecods,
                                              //    rows = results,

            //};
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //#664
        //documents
        private string GetAssetManagementDocTypesdropdownlist(IEnumerable<EntityObject.Entities.HCP_task.AM_Document> DocTypeModel, int? fileid)
        {

            StringBuilder sb = new StringBuilder();
            string defaultDocTypeId = ConfigurationManager.AppSettings["DefaultDocId"].ToString();
            sb.AppendLine("<select id='" + fileid.ToString() + "_ddlDocType'");
            sb.AppendLine("onchange='ChangrDocTypeConfirmation(fileId =" + fileid.ToString() + ")'  style='width: 200px;'>");
            sb.AppendLine("<option value='0'>Select New File Name</option>");

            foreach (EntityObject.Entities.HCP_task.AM_Document doctype in DocTypeModel)
            {
                string DocName = "";
                if (!string.IsNullOrEmpty(doctype.document_shotname))
                {
                    //DocName = doctype.document_shotname + "_" + doctype.document;
                    DocName = doctype.shortname_id;
                }
                else
                {
                    DocName = doctype.shortname_id;
                }
                //sb.AppendLine("<option value='" + doctype.ta_doc_id.ToString() + "' id='" + doctype.ta_doc_id.ToString() + "'>" + DocName + "</option>");
                sb.AppendLine("<option value='" + doctype.doc_type_id.ToString() + "' id='" + doctype.doc_type_id.ToString() + "'>" + DocName + "</option>");
            }
            sb.AppendLine("<option value='" + defaultDocTypeId + "'>Others</option></select> ");

            return sb.ToString();
        }

        //#664
        // documnet functionality
        // public ActionResult changeDoumentType(string fileId, int docTypeId, Guid taskinstanceId, Guid grouptaskinstanceId, string redirectFrom)
        public ActionResult AssetManagementchangeDoumentType(string fileId, string docType, string DocName, Guid taskinstanceId, string referrer)
        {




            //if (fileId != null)
            //{

            var tokenResult = WebApiTokenRequest.RequestToken();

            var taskFileInfo = taskManager.GetTaskFileById(Int32.Parse(fileId));
            var task = taskManager.GetTasksByTaskInstanceId(taskFileInfo.TaskInstanceId).FirstOrDefault();
            var formUploadData = AssetManagementReloadApplication(taskFileInfo.TaskInstanceId);

            if (taskFileInfo != null)
            {
                if (taskFileInfo.DocId != null)
                {
                    var docInfo = new RestfulWebApiUpdateModel()
                    {
                        docId = taskFileInfo.DocId,
                        documentType = docType.ToString(),
                        propertyId = formUploadData.PropertyId.ToString(),
                        indexType = "1",
                        indexValue = formUploadData.FhaNumber,
                        documentStatus = "Active",

                    };
                    var result = prodRestfulWebApiUpdate.UpdateDocument(docInfo, tokenResult.access_token);
                    if (result != null && result.status.ToLower() == "success")
                    {
                        taskFileInfo.DocTypeID = docType.ToString();
                        string ext = Path.GetExtension(taskFileInfo.FileName);
                        if (!string.IsNullOrEmpty(DocName))
                        {
                            taskFileInfo.FileName = DocName + ext;
                        }

                        taskManager.UpdateDocType(taskFileInfo);

                    }
                }
                else
                {
                    var docInfo = new RestfulWebApiUpdateModel()
                    {
                        documentType = docType.ToString()
                    };

                    taskFileInfo.DocTypeID = docType.ToString();
                    taskManager.UpdateDocType(taskFileInfo);

                }

            }
            if (referrer == "RAI")
            {
                return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml", formUploadData);
            }
            return View("~/Views/OPAForm/OPARequestForm.cshtml", formUploadData);
        }




        //#664
        public ActionResult RAIAssetManagementchangeDoumentType(string fileId, string docType, string DocName, Guid taskinstanceId, string referrer)
        {


            var ohv = new OPAHistoryViewModel();

            //if (fileId != null)
            //{

            var tokenResult = WebApiTokenRequest.RequestToken();

            var taskFileInfo = taskManager.GetTaskFileById(Int32.Parse(fileId));
            var task = taskManager.GetTasksByTaskInstanceId(taskFileInfo.TaskInstanceId).FirstOrDefault();
            var formUploadData = AssetManagementReloadApplication(taskFileInfo.TaskInstanceId);

            if (taskFileInfo != null)
            {
                if (taskFileInfo.DocId != null)
                {
                    var docInfo = new RestfulWebApiUpdateModel()
                    {
                        docId = taskFileInfo.DocId,
                        documentType = docType.ToString(),
                        propertyId = formUploadData.PropertyId.ToString(),
                        indexType = "1",
                        indexValue = formUploadData.FhaNumber,
                        documentStatus = "Active"

                    };
                    var result = prodRestfulWebApiUpdate.UpdateDocument(docInfo, tokenResult.access_token);
                    if (result != null && result.status.ToLower() == "success")
                    {
                        taskFileInfo.DocTypeID = docType.ToString();
                        string ext = Path.GetExtension(taskFileInfo.FileName);
                        if (!string.IsNullOrEmpty(DocName))
                        {
                            taskFileInfo.FileName = DocName + ext;
                            //ohv.isFileRenamed = true;
                        }

                        taskManager.UpdateDocType(taskFileInfo);

                    }
                }
                else
                {
                    var docInfo = new RestfulWebApiUpdateModel()
                    {
                        documentType = docType.ToString()
                    };

                    taskFileInfo.DocTypeID = docType.ToString();
                    taskManager.UpdateDocType(taskFileInfo);

                }

            }
            if (referrer == "RAI")
            {
                return View("~/Views/Production/ApplicationRequest/ApplicationRequestRAIHUDandReadOnly.cshtml", formUploadData);
            }
            return View("~/Views/OPAForm/OPARequestForm.cshtml", formUploadData);
        }
        //#664
        //releod
        private OPAViewModel AssetManagementReloadApplication(Guid taskinstanceid)
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            var Groupmodel = projectActionFormManager.GetOPAByTaskInstanceId(taskinstanceid);

            AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(Groupmodel.TaskInstanceId);
            var result = projectActionFormManager.GetPropertyInfo(AppProcessViewModel.FhaNumber);
            if (result != null)
            {
                AppProcessViewModel.PropertyId = result.PropertyId;
                AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                AppProcessViewModel.PropertyAddress.StateCode = result.State;
                AppProcessViewModel.PropertyAddress.City = result.City;
                AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
            }
            AppProcessViewModel.GroupTaskInstanceId = Groupmodel.TaskInstanceId;

            if (Groupmodel != null)
            {
                AppProcessViewModel.ServicerComments = Groupmodel.ServicerComments;

            }

            GetDisclaimerText(AppProcessViewModel);
            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(AppProcessViewModel.ProjectActionTypeId);
            //AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectTypebyID(AppProcessViewModel.ProjectActionTypeId);
            AppProcessViewModel.pageTypeId = (int)PageType.OPA;


            return AppProcessViewModel;

        }

        // Upload Button after RAI request form AE
        //#664
        // [ValidateInput(false)]
        public ActionResult RAIUploadPopupLoad(int FolderKey, string Model)
        {
            FolderKey = 39;



            var parentdict = GetParentModelDictionary(Model);

            ViewBag.uploadurl = "/OPAForm/RAISaveDropzoneJsUploadedFiles";

            if (parentdict != null)
            {
                var PagetypeID = parentdict.FirstOrDefault(x => x.Key == "pageTypeId").Value;
                if (Convert.ToInt32(PagetypeID) == 18)
                {
                    ViewBag.redirecturl = "/Amendments/Reload/GroupTaskInstanceId?=";
                }
                else
                {
                    //TaskGuid?="
                    ViewBag.redirecturl = "/OPAForm/RAIReload/GroupTaskInstanceId?=";
                    //ViewBag.redirecturl = "/OPAForm/RAIReload/TaskGuid?=";
                    //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();
                }
            }

            var folderuploadModel = new FolderUploadModel { FolderKey = FolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };


            return View("~/Views/Production/_Shared/_DragDropFileUpload.cshtml", folderuploadModel);
        }

        //harish added new functionality for save dropdownupload
        //#664

        [ValidateInput(false)]
        public ActionResult RAISaveDropzoneJsUploadedFiles(FolderUploadModel fileFolderModel)
        {
            OPAViewModel AppProcessModel = new OPAViewModel();

            int userID = 0;
            string userModel = string.Empty;
            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;

            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            int projectactiontypeid = 0;
            //var projectactiontypeid = Request.Form["projectActionType"];
            string value = string.Empty;
            string GroupTaskInstanceId = string.Empty;
            string TaskGuid = string.Empty;
            TaskGuid = Request.Form["TaskGuid"];
            int pageTypeId = 0;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();
            //Logic for check  whether a task Exists
            var parentdict = GetParentModelDictionary(ParentModel);
            bool IsTaskavilable = false;
            //harish
            string GroupTaskId = string.Empty;
            GroupTaskId = Request.Form["GroupTaskId"];
            //projectactiontypeid = (string)parentdict["ProjectActionTypeId"];






            try
            {
                if (parentdict.Count > 0)
                {

                    if (parentdict.ContainsKey("TaskInstanceId") && !string.IsNullOrEmpty((string)parentdict["TaskInstanceId"]))
                    {
                        GroupTaskInstanceId = (string)parentdict["TaskInstanceId"];

                    }
                    if (parentdict.ContainsKey("pageTypeId") && !string.IsNullOrEmpty((string)parentdict["pageTypeId"]))
                    {

                        pageTypeId = Convert.ToInt32((string)parentdict["pageTypeId"]);

                    }
                    var grouptaskmodel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
                    if (grouptaskmodel == null)
                    {


                        //Save OpaForm

                        //AppProcessModel.ProjectActionName = parentdict["ProjectActionName"] != null ? (string)parentdict["ProjectActionName"] : "";

                        AppProcessModel.ServicerComments = "";


                        AppProcessModel.PropertyName = parentdict["PropertyName"] != null ? (string)parentdict["PropertyName"] : "";
                        AppProcessModel.RequesterName = parentdict["RequesterName"] != null ? (string)parentdict["RequesterName"] : "";
                        AppProcessModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        //  var  FHA = Request.Form["FHA"];
                        AppProcessModel.IsAddressChange = parentdict["IsAddressChange"] != null ? Convert.ToBoolean(parentdict["IsAddressChange"]) : false;
                        //AppProcessModel.ProjectActionTypeId = Convert.ToInt32( projectactiontypeid);
                        //model.FhaNumber = FHA;
                        //Insert the non critical request and get the generated id, insert into view model
                        AppProcessModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : ""; ;
                        AppProcessModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        AppProcessModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(AppProcessModel.ProjectActionTypeId);

                        AppProcessModel.ServicerSubmissionDate = null;
                        AppProcessModel.RequestStatus = (int)RequestStatus.Draft;
                        AppProcessModel.CreatedOn = DateTime.UtcNow;
                        AppProcessModel.ModifiedOn = DateTime.UtcNow;
                        AppProcessModel.CreatedBy = UserPrincipal.Current.UserId;
                        AppProcessModel.ModifiedBy = UserPrincipal.Current.UserId;
                        AppProcessModel.ProjectActionDate = DateTime.UtcNow;
                        AppProcessModel.RequestDate = DateTime.UtcNow;
                        AppProcessModel.TaskInstanceId = new Guid(GroupTaskInstanceId);
                        AppProcessModel.pageTypeId = pageTypeId;


                        AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;

                        var groupTaskModel = new GroupTaskModel();
                        groupTaskModel.TaskInstanceId = AppProcessModel.TaskInstanceId;
                        groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.FhaNumber = AppProcessModel.FhaNumber;
                        groupTaskModel.PropertyName = AppProcessModel.PropertyName;
                        groupTaskModel.RequestDate = AppProcessModel.RequestDate;
                        groupTaskModel.RequesterName = AppProcessModel.RequesterName;
                        groupTaskModel.ServicerSubmissionDate = AppProcessModel.ServicerSubmissionDate;
                        groupTaskModel.ServicerComments = parentdict["ServicerComments"] != null ? (string)parentdict["ServicerComments"] : "";
                        groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                        groupTaskModel.ProjectActionTypeId = AppProcessModel.ProjectActionTypeId;
                        groupTaskModel.IsDisclimerAccepted = AppProcessModel.IsAgreementAccepted;
                        groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.CreatedOn = DateTime.UtcNow;
                        groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedOn = DateTime.UtcNow;
                        groupTaskModel.IsAddressChanged = AppProcessModel.IsAddressChange;
                        groupTaskModel.ServicerComments = AppProcessModel.ServicerComments;
                        AppProcessModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                        var projectActionFormId = projectActionFormManager.SaveOPAForm(AppProcessModel);

                        InstanceID = grouptaskmodel.TaskInstanceId;
                        AppProcessModel.GroupTaskInstanceId = InstanceID;
                        AppProcessModel.ProjectActionFormId = projectActionFormId;
                    }
                    else
                    {
                        AppProcessModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        AppProcessModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : "";
                        //TaskGuid = (string)parentdict["TaskGuid"];
                        //AppProcessModel.TaskGuid = new Guid(TaskGuid);
                        AppProcessModel.TaskGuid = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskGuid;
                        AppProcessModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
                        AppProcessModel.TaskInstanceId = new Guid(GroupTaskInstanceId);
                        AppProcessModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        //skumar-form290 requires following information
                        AppProcessModel.pageTypeId = pageTypeId;
                        if (parentdict.ContainsKey("ModifiedBy"))
                        {
                            if (parentdict["ModifiedBy"] != null)
                            {
                                userModel = (string)parentdict["ModifiedBy"];
                                if (string.IsNullOrEmpty(userModel))
                                    userModel = (string)parentdict["CreatedBy"];
                                userID = Convert.ToInt32(userModel);
                            }
                            AppProcessModel.userId = userID;
                        }

                    }
                }
                var success = this.Upload(AppProcessModel, Convert.ToInt32(FolderKey));

                if (success)
                {
                    //// Save ReviewFileStatus
                    string assignedto = projectActionFormManager.GetAeEmailByFhaNumber(AppProcessModel.FhaNumber);
                    int userId = webSecurity.GetUserId(assignedto);
                    Guid taskInstanceID = AppProcessModel.TaskInstanceId;
                    int reviewerUserId = userId;
                    int currentUserId = UserPrincipal.Current.UserId;
                    taskManager.SaveOPAReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);

                    //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);

                    //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);

                    ViewBag.UploadFailed = "False";
                    TempData["UploadFailed"] = "False";
                }
                else
                {
                    ViewBag.UploadFailed = "True";
                    TempData["UploadFailed"] = "True";

                }
                if (pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessModel);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", AppProcessModel);
                }
            }
            catch (Exception e)
            {
                //todo:skumar: redirect to error page
                return RedirectToAction("Login", "Account");
            }
        }

        // #664harish added new fucntionality
        [ValidateInput(false)]
        public ActionResult RAIReload()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.pageTypeId = 3;
            var GroupTaskInstanceId = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;
            var taskstepid = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskStepId;
            //var GroupTaskInstanceId = "55dd209e-ed1f-4b3d-99d2-f882283481f0";
            // string GroupTaskInstanceId = Request.Form["TaskInstanceId"];
            //AppProcessViewModel.GroupTaskInstanceId = GroupTaskInstanceId;
            AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(GroupTaskInstanceId);
            //AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
            foreach (var model in projectActionList)
            {
                AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
                {
                    Text = model.ProjectActionName,
                    Value = model.ProjectActionID.ToString()
                });
            }



            var result = projectActionFormManager.GetPropertyInfo(AppProcessViewModel.FhaNumber);
            if (result != null)
            {
                AppProcessViewModel.PropertyId = result.PropertyId;
                AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                AppProcessViewModel.PropertyAddress.StateCode = result.State;
                AppProcessViewModel.PropertyAddress.City = result.City;
                AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
            }
            //AppProcessViewModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
            AppProcessViewModel.GroupTaskInstanceId = GroupTaskInstanceId;
            //  AppProcessViewModel.ProjectActionTypeId;
            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(AppProcessViewModel.ProjectActionTypeId);
            AppProcessViewModel.TaskStepId = taskstepid;

            //var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(new Guid(GroupTaskInstanceId));
            var grouptaskmodel = projectActionFormManager.GetOPAByTaskInstanceId(GroupTaskInstanceId);
            if (grouptaskmodel != null)
            {
                AppProcessViewModel.ServicerComments = grouptaskmodel.ServicerComments;
                AppProcessViewModel.pageTypeId = (int)PageType.OPA;
            }
            GetDisclaimerText(AppProcessViewModel);
            //AppProcessViewModel.pageTypeId = (int)PageType.ProductionApplication; 
            if (TempData["UploadFailed"] != null)
            {
                ViewBag.UploadFailed = TempData["UploadFailed"].ToString();
            }



            //else if (AppProcessViewModel.pageTypeId == (int)PageType.Amendments)
            //{
            //    ViewBag.ConstructionTitle = "Amendments";
            //    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessViewModel);
            //}

            // PopulateFHANumberList(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            PopulateFHANumberList(AppProcessViewModel);
            GetPropertyInfo(AppProcessViewModel.FhaNumber);
            return View("~/Views/OPAForm/OPARequestLenderReadOnly.cshtml", AppProcessViewModel);
            //}
        }
        //#541
        //read only grid
        public JsonResult RAIGetRequestAdditionalGridForLender(string sidx, string sord, int page, int rows,
           Guid taskInstanceId)
        {
            var requestAddtionalTask = new List<OPAHistoryViewModel>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //var parentchildtaskmodel = new ParentChildTaskModel();
            //parentchildtaskmodel = parentChildTaskManager.GetChildTaskbytaskinstanceid(taskInstanceId);
            //requestAddtionalTask = GetOpaHistory(parentchildtaskmodel.ChildTaskInstanceId);
            //requestAddtionalTask = requestAddtionalTask.Where(x => x.actionTaken != "Request Additional Submit").ToList();

            requestAddtionalTask = GetOpaHistory(taskInstanceId);

            // requestAddtionalTask = requestAddtionalTask.Where(x=>x.actionTaken !=)
            //requestAddtionalTask = requestAddtionalTask.Where(x => x.fileReviewStatusId != 1).ToList();


            if (requestAddtionalTask.Count > 0)
            {

                foreach (var item in requestAddtionalTask)
                {
                    item.fileRename = item.childFileName;
                }
                int totalrecods = requestAddtionalTask.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                if (sord.ToUpper() == "DESC")
                {

                    requestAddtionalTask = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList();

                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    requestAddtionalTask = requestAddtionalTask.OrderBy(s => s.submitDate).ToList();
                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {

                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = requestAddtionalTask,
                };




                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {



                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = requestAddtionalTask,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }



        //#541
        // delete functionality for single record
        public string RAIDeleteFile(string taskFileIDs)
        {
            //Naveen 235 added foreach loop
            var taskFileIdList = taskFileIDs.Split(',');

            foreach (var taskfileid1 in taskFileIdList)
            {
                RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
                RestfulWebApiResultModel APIresult = new RestfulWebApiResultModel();
                RestfulWebApiUpdateModel UpdateInfo = new RestfulWebApiUpdateModel();
                TaskFileModel taskFile = taskManager.GetTaskFileById(Convert.ToInt32(taskfileid1));
                var success = 0;
                string pathForSaving = "";
                if (taskFile != null)
                {
                    if (UserPrincipal.Current.UserId == taskFile.CreatedBy || (UserPrincipal.Current.UserRole == "BackupAccountManager" || UserPrincipal.Current.UserRole == "LenderAccountManager" || UserPrincipal.Current.UserRole == "LenderAccountManager"))
                    {

                        pathForSaving = Server.MapPath("~/Uploads");

                        success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskfileid1));
                        if (success == 1)
                        {
                            if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                            {
                                request = WebApiTokenRequest.RequestToken();
                                APIresult = WebApiDelete.DeleteDocumentUsingWebApi(taskFile.DocId, request.access_token);
                            }
                            else
                            {
                                pathForSaving = Server.MapPath("~/Uploads");

                                System.IO.File.Delete(pathForSaving + "\\" + taskFile.SystemFileName);

                                if (success == 1)
                                    return "Success";
                            }
                        }
                    }
                    else
                    {
                        return "unauthorized";
                    }
                }
            }

            return "Success";
        }
        //#541
        // rai delete
        //delete functioanlity
        public void DeleteRAI(string taskFileIDs)
        {

            RAIDeleteFile(taskFileIDs);


        }
        // code merges start from line number 4933 (31-12-2019)
        // siddesh 
        //Added by siddu 20120219//
        //public void MultipleDelete(string taskFileIDs)
        //{
        //    var taskFileIdList = taskFileIDs.Split(',');
        //    foreach (var taskFileId in taskFileIdList)
        //    {
        //        if (!string.IsNullOrEmpty(taskFileId))
        //        {
        //            DeleteFile(taskFileId);
        //            // return filteredTickets1;
        //        }
        //    }
        //}
        //Added by siddu 23122019//
        public JsonResult GetOPAWorkInternalProduct(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //var taskInstanceId1 = new Guid("1C0F081F-4515-4C18-AC64-BDB5AC59B023");
            //if (fileType == null)
            //    fileType = iOPAForm.UpdateOPAGroupTask1(fileType);
            //var opaWorkProduct1=    GetOpaHistory(taskInstanceId);
            // var taskInstanceId1 = new Guid("F2BD7D89-AF05-424A-8375-8F31C0E25B7C");
            var opaWorkProduct = iOPAForm.getAllOPAWorkInternalProducts(taskInstanceId);

            //Setting the date based on the Time Zone
            if (opaWorkProduct != null)
            {
                foreach (var item in opaWorkProduct)
                {
                    //item.UploadTime = TimezoneManager.ToMyTimeFromUtc(item.UploadTime);
                    item.UploadTime = item.UploadTime;

                }
            }
            if (opaWorkProduct != null)
            {
                int totalrecods = opaWorkProduct.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                var result = opaWorkProduct.ToList();
                var results = result.Skip(pageIndex * pageSize).Take(pageSize);

                var jsonData = new
                {
                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = results,
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = opaWorkProduct,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }
        //Added by siddu for internal work products @24122019//
        [ValidateInput(false)]
        public ActionResult UploadPopupLoadInternalProducts(int FolderKey, string Model)
        {
            FolderKey = 41;
            var parentdict = GetParentModelDictionary(Model);
            ViewBag.uploadurl = "/OPAForm/SaveDropzoneJsUploadedFilesInternalProducts";
            var formUploadData = new OPAViewModel();
            if (parentdict != null)
            {
                var PagetypeID = parentdict.FirstOrDefault(x => x.Key == "pageTypeId").Value;
                if (Convert.ToInt32(PagetypeID) == 18)
                {
                    ViewBag.redirecturl = "/Amendments/Reload/GroupTaskInstanceId?=";
                }
                else
                {
                    // ViewBag.redirecturl = "/OPAForm/Reload/GroupTaskInstanceId?=";
                    string redURL = "/Task/GetTaskDetail/" + parentdict.FirstOrDefault(x => x.Key == "TaskId").Value + "?fromTaskReAssignment=False&style=text-decoration%3Aunderline";
                    ViewBag.redirecturl = redURL;
                    // return RedirectToAction("GetOPAFormDetail", "OPAForm", new { @model = formUploadData });
                    //ViewBag.redirecturl = "/OPAForm/GetTaskDetail?taskInstanceId=";
                    // ViewBag.redirecturl = "/OPAForm/GetOPAFormDetail/GroupTaskInstanceId?=";

                    // GetOPAFormDetail
                    //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();
                }
            }

            var folderuploadModel = new FolderUploadModel { FolderKey = FolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };


            return View("~/Views/Production/_Shared/_DragDropFileUpload.cshtml", folderuploadModel);
        }
        //Added by siddu for internal work products @24122019//


        [ValidateInput(false)]
        public ActionResult SaveDropzoneJsUploadedFilesInternalProducts(FolderUploadModel fileFolderModel)
        {
            // FolderUploadModel fileFolderModel
            OPAViewModel OPAProjectActionViewModel = new OPAViewModel();
            int userID = 0;
            string userModel = string.Empty;
            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;
            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            string value = string.Empty;

            var OPAModel = new OPAViewModel();
            string GroupTaskInstanceId = string.Empty;
            int pageTypeId = 0;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();
            //Logic for check  whether a task Exists
            var parentdict = GetParentModelDictionary(ParentModel);
            bool IsTaskavilable = false;

            try
            {
                if (parentdict.Count > 0)
                {

                    if (parentdict.ContainsKey("TaskInstanceId") && !string.IsNullOrEmpty((string)parentdict["TaskInstanceId"]))
                    {
                        GroupTaskInstanceId = (string)parentdict["TaskInstanceId"];

                    }
                    if (parentdict.ContainsKey("pageTypeId") && !string.IsNullOrEmpty((string)parentdict["pageTypeId"]))
                    {

                        pageTypeId = Convert.ToInt32((string)parentdict["pageTypeId"]);

                    }
                    var grouptaskmodel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
                    if (grouptaskmodel == null)
                    {

                        //Save OpaForm
                        OPAProjectActionViewModel.ServicerComments = "";

                        OPAProjectActionViewModel.PropertyName = parentdict["PropertyName"] != null ? (string)parentdict["PropertyName"] : "";
                        OPAProjectActionViewModel.RequesterName = parentdict["RequesterName"] != null ? (string)parentdict["RequesterName"] : "";
                        OPAProjectActionViewModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        //  var  FHA = Request.Form["FHA"];
                        OPAProjectActionViewModel.IsAddressChange = parentdict["IsAddressChange"] != null ? Convert.ToBoolean(parentdict["IsAddressChange"]) : false;
                        //model.FhaNumber = FHA;
                        //Insert the non critical request and get the generated id, insert into view model
                        OPAProjectActionViewModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : ""; ;
                        //AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        OPAProjectActionViewModel.ServicerSubmissionDate = null;
                        OPAProjectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                        OPAProjectActionViewModel.CreatedOn = DateTime.UtcNow;
                        OPAProjectActionViewModel.ModifiedOn = DateTime.UtcNow;
                        OPAProjectActionViewModel.CreatedBy = UserPrincipal.Current.UserId;
                        OPAProjectActionViewModel.ModifiedBy = UserPrincipal.Current.UserId;
                        OPAProjectActionViewModel.ProjectActionDate = DateTime.UtcNow;
                        OPAProjectActionViewModel.RequestDate = DateTime.UtcNow;
                        OPAProjectActionViewModel.TaskInstanceId = new Guid(GroupTaskInstanceId);
                        OPAProjectActionViewModel.pageTypeId = pageTypeId;
                        OPAProjectActionViewModel.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAProjectActionViewModel.ProjectActionTypeId);
                        OPAProjectActionViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(OPAProjectActionViewModel.FhaNumber).PropertyId;
                        var groupTaskModel = new GroupTaskModel();
                        // harish commented
                        groupTaskModel.TaskInstanceId = OPAProjectActionViewModel.TaskInstanceId;
                        groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.FhaNumber = OPAProjectActionViewModel.FhaNumber;
                        groupTaskModel.PropertyName = OPAProjectActionViewModel.PropertyName;
                        groupTaskModel.RequestDate = OPAProjectActionViewModel.RequestDate;
                        groupTaskModel.RequesterName = OPAProjectActionViewModel.RequesterName;
                        groupTaskModel.ServicerSubmissionDate = OPAProjectActionViewModel.ServicerSubmissionDate;
                        groupTaskModel.ServicerComments = parentdict["ServicerComments"] != null ? (string)parentdict["ServicerComments"] : "";
                        groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                        groupTaskModel.ProjectActionTypeId = OPAProjectActionViewModel.ProjectActionTypeId;
                        groupTaskModel.IsDisclimerAccepted = OPAProjectActionViewModel.IsAgreementAccepted;
                        groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.CreatedOn = DateTime.UtcNow;
                        groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedOn = DateTime.UtcNow;
                        groupTaskModel.IsAddressChanged = OPAProjectActionViewModel.IsAddressChange;
                        groupTaskModel.ServicerComments = OPAProjectActionViewModel.ServicerComments;
                        OPAProjectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);


                        var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAProjectActionViewModel);

                        InstanceID = grouptaskmodel.TaskInstanceId;
                        OPAProjectActionViewModel.GroupTaskInstanceId = InstanceID;
                        OPAProjectActionViewModel.ProjectActionFormId = projectActionFormId;
                    }
                    else
                    {
                        OPAProjectActionViewModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        OPAProjectActionViewModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : "";
                        OPAProjectActionViewModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
                        OPAProjectActionViewModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(OPAProjectActionViewModel.FhaNumber).PropertyId;
                        //skumar-form290 requires following information
                        OPAProjectActionViewModel.pageTypeId = pageTypeId;
                        if (parentdict.ContainsKey("ModifiedBy"))
                        {
                            if (parentdict["ModifiedBy"] != null)
                            {
                                userModel = (string)parentdict["ModifiedBy"];
                                if (string.IsNullOrEmpty(userModel))
                                    userModel = (string)parentdict["CreatedBy"];
                                userID = Convert.ToInt32(userModel);
                            }
                            OPAProjectActionViewModel.userId = userID;
                        }

                    }
                }
                var success = this.UploadInternalProducts(OPAProjectActionViewModel, Convert.ToInt32(FolderKey));

                if (success)
                {
                    var nextStage = new Prod_NextStageModel();

                    nextStage.NextPgTypeId = pageTypeId;
                    nextStage.ModifiedOn = DateTime.UtcNow;
                    nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                    nextStage.FhaNumber = OPAProjectActionViewModel.FhaNumber;
                    nextStage.NextPgTaskInstanceId = OPAProjectActionViewModel.GroupTaskInstanceId;
                    nextStage.IsNew = false;
                    //prod_NextStageManager.UpdateTaskInstanceIdForNxtStage(nextStage);
                    // Venkatesh They are passing the page type id by default as production application I cahnge it dinamic
                    //AppProcessModel.pageTypeId = (int)PageType.ProductionApplication;
                    OPAProjectActionViewModel.pageTypeId = pageTypeId;
                    // Save ReviewFileStatus
                    //string assignedto = projectActionFormManager.GetAeEmailByFhaNumber(OPAProjectActionViewModel.FhaNumber);
                    //int userId = webSecurity.GetUserId(assignedto);
                    //Guid taskInstanceID = OPAProjectActionViewModel.TaskInstanceId;
                    //int reviewerUserId = userId;
                    //int currentUserId = UserPrincipal.Current.UserId;
                    //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                    ViewBag.UploadFailed = "False";
                    TempData["UploadFailed"] = "False";

                }
                else
                {
                    ViewBag.UploadFailed = "True";
                    TempData["UploadFailed"] = "True";

                }
                if (pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", OPAProjectActionViewModel);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", OPAProjectActionViewModel);
                }
            }
            catch (Exception e)
            {
                //todo:skumar: redirect to error page
                return RedirectToAction("Login", "Account");
            }
        }
        //Added by siddu for internal work products @24122019//


        [ValidateInput(false)]
        private bool UploadInternalProducts(OPAViewModel OPAProjectActionViewModel, int FolderKey)
        {
            PropertyInfoModel PropertyInfoModel = new PropertyInfoModel();
            var OPAModel = new OPAViewModel();
            OPAProjectActionViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(OPAProjectActionViewModel.FhaNumber).PropertyId;
            OPAProjectActionViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(OPAProjectActionViewModel.ProjectActionTypeId);
            string FileSectionName = "";
            var FolderInfo = projectActionFormManager.GetFolderInfoByKey(FolderKey);
            var PageName = projectActionFormManager.FormNameBYPageTypeID(OPAProjectActionViewModel.pageTypeId);
            string folderNames = string.Empty;
            string ProjectActionTypeId = string.Empty;
            ProjectActionTypeId = Request.Form["ProjectActionTypeId"];
            string GroupTaskId = string.Empty;
            // OPAProjectActionViewModel.GroupTaskId = 2010;
            GroupTaskId = Request.Form["GroupTaskId"];
            string formNameConfiguration = ConfigurationManager.AppSettings["FORM 290"] ?? ConfigurationSettings.AppSettings["FORM 290"];
            string amendmentConfig = ConfigurationManager.AppSettings["Amendments"] ?? ConfigurationSettings.AppSettings["Amendments290"];
            int ammendmentPageId = !string.IsNullOrEmpty(amendmentConfig) ? Convert.ToInt32(amendmentConfig) : 0;
            int form290PageId = !string.IsNullOrEmpty(formNameConfiguration) ? Convert.ToInt32(formNameConfiguration) : 0;
            if (FolderInfo != null)
            {
                foreach (Prod_SubFolderStructureModel folder in FolderInfo)
                {


                    if (OPAProjectActionViewModel.pageTypeId == form290PageId)//skumar-form290 - folder map 
                    {
                        folderNames = "/" + OPAProjectActionViewModel.PropertyId.ToString() + "/" + OPAProjectActionViewModel.FhaNumber + "/Production/ExecutedClosing/WorkProduct/Form290/";
                    }
                    else if (OPAProjectActionViewModel.pageTypeId == ammendmentPageId)//skumar-2044 amendemtns=18
                    {
                        folderNames = "/" + OPAProjectActionViewModel.PropertyId.ToString() + "/" + OPAProjectActionViewModel.FhaNumber + "/Production/Closing/WorkProduct/Amendments/";
                    }
                    else if (FolderInfo[0].ViewTypeId == 2)
                    {
                        folderNames = "Production/" + PageName + "/" + "WorkProduct/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                    }

                    else
                    {
                        //Consider the case where there is no SubfolderSequence
                        // folderNames = "Production/" + PageName + "/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                        //folderNames = "Asset Management /" + OPAProjectActionViewModel.ProjectActionName + "/" + OPAProjectActionViewModel.PropertyId + "/" + OPAProjectActionViewModel.FhaNumber + "/Asset Management/OPA/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy");
                        folderNames = "/" + OPAProjectActionViewModel.PropertyId + "/" + OPAProjectActionViewModel.FhaNumber + "/" + "Asset Management /" + OPAProjectActionViewModel.ProjectActionName + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy");
                    }
                }

            }
            RestfulWebApiUploadModel[] obj1 = new RestfulWebApiUploadModel[1];
            var uploadmodel = new RestfulWebApiUploadModel()
            {
                // propertyID = AppProcessViewModel.PropertyId.ToString(),
                propertyID = OPAProjectActionViewModel.PropertyId.ToString(),
                indexType = "1",
                // indexValue = AppProcessViewModel.FhaNumber,
                indexValue = OPAProjectActionViewModel.FhaNumber,
                //pdfConvertableValue = "false",
                pdfConvertableValue = "false",
                folderKey = FolderKey,
                folderNames = folderNames,
                transactionType = OPAProjectActionViewModel.ProjectActionTypeId.ToString(),
                transactionStatus = "",
                transactionDate = DateTime.UtcNow.ToString("MM-dd-yyyy")
                // folderNames = "Asset Management /" + OPAProjectActionViewModel.ProjectActionName + "/" + OPAProjectActionViewModel.PropertyId + "/" + OPAProjectActionViewModel.FhaNumber + "/Asset Management/OPA/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy")
                // folderNames = PropertyInfoModel.PropertyId.ToString() + "/" + OPAProjectActionViewModel.FhaNumber + "/Asset Management/Project Action Form/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy") + "/" + FileSectionName,
            };
            obj1[0] = uploadmodel;
            //karri#162
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //request = WebApiTokenRequest.RequestToken();

            bool isUploaded = false;

            foreach (string Filename in Request.Files)
            {
                // var TEMP = Request.Files[Filename];
                //HttpPostedFileBase Temp;

                HttpPostedFileBase myFile = Request.Files[Filename];
                var t = myFile.InputStream;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();

                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = OPAProjectActionViewModel.FhaNumber + "_" +
                                         OPAProjectActionViewModel.ProjectActionTypeId + "_" +
                                         OPAProjectActionViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);

                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {

                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = true;

                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    //karri#162,#171
                    request = WebApiTokenRequest.RequestToken();

                    if (OPAProjectActionViewModel.pageTypeId == 18)
                    {
                        WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, FolderInfo[0].FolderName + "_" + FolderInfo[0].SubfolderSequence);
                    }
                    else
                    {
                        WebApiUploadResult = WebApiDocumentUpload.AssetManagementUploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");
                    }


                    if (System.IO.File.Exists(Path.Combine(pathForSaving, systemFileName)))
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));

                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {

                        isUploaded = true;
                        var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(OPAProjectActionViewModel.GroupTaskInstanceId.ToString()));
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        //if (AppProcessViewModel.pageTypeId == 18)
                        //{
                        //string subFolder = string.Empty;
                        //subFolder = !string.IsNullOrEmpty(FolderInfo[0].SubfolderSequence) ? FolderInfo[0].SubfolderSequence.Trim() : subFolder;
                        //if (!string.IsNullOrEmpty(subFolder))
                        //{
                        //    taskFile.FileName = FolderInfo[0].FolderName + "_" + FolderInfo[0].SubfolderSequence.Trim() + "!" + myFile.FileName;
                        //}
                        //else
                        //{
                        //    taskFile.FileName = FolderInfo[0].FolderName + "_" + subFolder + "!" + myFile.FileName;
                        //}
                        //}
                        if (WebApiUploadResult.documentType != DefaultDocID)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        else if (FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductClosing) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductUnderwriting) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductLoanCommittee))
                        {
                            //taskFile.DocTypeID = DefaultDocID;
                            taskFile.FileType = FileType.WP;
                        }
                        else if (OPAProjectActionViewModel.pageTypeId == 18)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        // OPAProjectActionViewModel.ProjectActionName =
                        // projectActionFormManager.GetProjectActionName(OPAModel.ProjectActionTypeId);
                        //OPAProjectActionViewModel.ProjectActionTypeId = OPAModel.ProjectActionTypeId;


                        taskFile.RoleName = UserPrincipal.Current.UserRole;
                        taskFile.FolderName = folderNames;
                        taskFile.FolderStructureKey = 41;
                        var taskfileid = taskManager.SaveGroupTaskFileModel(taskFile);
                        var mappingmodel = new TaskFile_FolderMappingModel();
                        mappingmodel.FolderKey = FolderKey;
                        mappingmodel.TaskFileId = taskfileid;
                        mappingmodel.CreatedOn = DateTime.UtcNow;

                        taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);

                        //Code for saving new files on RAI
                        if (OPAProjectActionViewModel.TaskGuid != null)
                        {
                            var childTaskNewFile = new ChildTaskNewFileModel();
                            childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
                            childTaskNewFile.ChildTaskInstanceId = OPAProjectActionViewModel.TaskGuid.Value;
                            childTaskNewFile.TaskFileInstanceId = taskfileid;
                            projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);

                        }
                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        isUploaded = false;

                    }

                    //OPAProjectActionViewModel.ProjectActionName =
                    //      projectActionFormManager.GetProjectActionName(OPAProjectActionViewModel.ProjectActionTypeId);
                    //IList<ProjectActionTypeViewModel> projectActionList =
                    //   projectActionManager.GetAllProjectActionsByPageId(3);
                    //foreach (var model in projectActionList)
                    //{
                    //    OPAProjectActionViewModel.ProjectActionTypeList.Add(new SelectListItem()
                    //    {
                    //        Text = model.ProjectActionName,
                    //        Value = model.ProjectActionID.ToString()
                    //    });
                    //}



                }
            }
            return isUploaded;
        }
        //Added by siddu 27122019//

        [HttpPost]
        public void ExternalTaskAsignment(ProductionTaskAssignmentModel model)
        {

            model.IsReviewer = false;
            model.AssignedBy = UserPrincipal.Current.UserId;
            model.Status = (int)ProductionAppProcessStatus.InProcess;
            model.ViewId = model.AssignedToViewId;
            model.ModifiedOn = DateTime.UtcNow;
            model.ModifiedBy = UserPrincipal.Current.UserId;
            model.AssignedTo = model.AssignedToUserId;
            model.TaskXrefid = Guid.NewGuid();
            prod_TaskXrefManager.AddTaskXref(model);

            //Logic to Separate the UnderWriter
            if (model.AssignedToViewId == 1)
            {
                var task = taskManager.GetLatestTaskByTaskInstanceId(model.TaskInstanceId);
                if (task != null)
                {

                    task.AssignedTo = productionQueue.GetUserById(model.AssignedToUserId).UserName;
                    task.Notes = model.Comments;
                    taskManager.UpdateTaskAssignment(task);
                }
            }

            // taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId, IsOPAUpload, ISRAIUpload);
            taskManager.SaveReviewFileStatus(model.TaskInstanceId, model.AssignedToUserId, UserPrincipal.Current.UserId, model.AssignedToViewId);


        }

        //Added by siddu 27122019//
        public ActionResult AssignOPAToExternalReviewerAssignment(Guid taskInstanceId)
        {
            var taskAssignmentModel = new ProductionTaskAssignmentModel { TaskInstanceId = taskInstanceId };
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            if (task != null)
            {
                taskAssignmentModel.TaskId = task.TaskId;
            }
            var curentUser = UserPrincipal.Current.UserName;
            taskAssignmentModel.isWLM = RoleManager.IsProductionWLM(curentUser); //
            var IsoUsers = (from isoUsers in productionQueue.GetALLInternalSpecialOptionUsers()
                            select new
                            {
                                isoUsers.UserID,
                                Name = String.Format("{0} {1} {2} {3} {4}", isoUsers.FirstName, isoUsers.LastName, "( ", isoUsers.RoleName, " )")

                            });


            var AvailableViews = (from Views in productionQueue.GetUnAssignedViewsbyTaskInstanceId(2, taskInstanceId)
                                  select new
                                  {
                                      Views.ViewId,
                                      Name = Views.ViewName

                                  });

            ViewBag.AssignedToUserId = new SelectList(IsoUsers, "UserID", "Name");
            ViewBag.AssignedToViewId = new SelectList(AvailableViews, "ViewId", "Name");

            return View("~/Views/OPAForm/OPARequest/popup/ExternalReviewerTaskAsignment.cshtml", taskAssignmentModel);
        }
        // Ae Delete functionality

        //delete functioanlity
        public void AEMultipleDelete(string taskFileIDs)
        {
            var taskFileIdList = taskFileIDs.Split(',');
            foreach (var taskFileId in taskFileIdList)
            {
                DeleteFile(taskFileId);
            }

        }

        // ae internal wp delete functionality
        public void AEInternalMultipleDelete(string taskFileIDs)
        {
            var taskFileIdList = taskFileIDs.Split(',');
            foreach (var taskFileId in taskFileIdList)
            {
                DeleteFile(taskFileId);
            }

        }

        //Added by siddu 18122019//
        public ActionResult SendEmailToLender(Guid taskInstanceId)
        {
            //get pageid from the configurations
            //string frmR4R = ConfigurationManager.AppSettings["R4R"];//1
            //string frmNCRE = ConfigurationManager.AppSettings["NCRE"];//2
            //string frmOPA = ConfigurationManager.AppSettings["OPA"];//3
            //string frmFHA_REQUEST = ConfigurationManager.AppSettings["FHA# REQUEST"];//4
            //string frmAPPLICATION_REQUEST = ConfigurationManager.AppSettings["APPLICATION REQUEST"];//5
            //string frmCONSTRUCTION_SINGLE_STAGE = ConfigurationManager.AppSettings["CONSTRUCTION SINGLE STAGE"];//6
            //string frmCONSTRUCTION_TWO_STAGE_INITIAL = ConfigurationManager.AppSettings["CONSTRUCTION TWO STAGE INITIAL"];//7
            //string frmCONSTRUCTION_TWO_STAGE_FINAL = ConfigurationManager.AppSettings["CONSTRUCTION TWO STAGE FINAL"];//8
            //string frmCONSTRUCTION_MANAGEMENT = ConfigurationManager.AppSettings["CONSTRUCTION MANAGEMENT"];//9
            //string frmNON_CONSTRUCTION_DRAFT_CLOSING = ConfigurationManager.AppSettings["NON-CONSTRUCTION DRAFT CLOSING"];//10
            //string frmCLOSING_TWO_STAGE_INITIAL = ConfigurationManager.AppSettings["CLOSING TWO STAGE INITIAL"];//11
            //string frmCLOSING_TWO_STAGE_FINAL = ConfigurationManager.AppSettings["CLOSING TWO STAGE FINAL"];//12
            //string frmFORM_290 = ConfigurationManager.AppSettings["FORM 290"];//16
            //string frmNON_CONSTRUCTION_EXECUTED_CLOSING = ConfigurationManager.AppSettings["NON-CONSTRUCTION EXECUTED CLOSING"];//17

            //int frm_R4R = !string.IsNullOrEmpty(frmR4R) ? Convert.ToInt32(frmR4R) : 1;//1
            //int frm_NCRE = !string.IsNullOrEmpty(frmNCRE) ? Convert.ToInt32(frmNCRE) :2;//2
            //int frm_OPA = !string.IsNullOrEmpty(frmOPA) ? Convert.ToInt32(frmOPA) :3;//3
            //int frm_FHA_REQUEST = !string.IsNullOrEmpty(frmFHA_REQUEST) ? Convert.ToInt32(frmFHA_REQUEST) : 4;//4
            //int frm_APPLICATION_REQUEST = !string.IsNullOrEmpty(frmAPPLICATION_REQUEST) ? Convert.ToInt32(frmAPPLICATION_REQUEST) : 5;//5
            //int frm_CONSTRUCTION_SINGLE_STAGE = !string.IsNullOrEmpty(frmCONSTRUCTION_SINGLE_STAGE) ? Convert.ToInt32(frmCONSTRUCTION_SINGLE_STAGE) : 6;//6
            //int frm_CONSTRUCTION_TWO_STAGE_INITIAL = !string.IsNullOrEmpty(frmCONSTRUCTION_TWO_STAGE_INITIAL) ? Convert.ToInt32(frmCONSTRUCTION_TWO_STAGE_INITIAL) : 7;//7
            //int frm_CONSTRUCTION_TWO_STAGE_FINAL = !string.IsNullOrEmpty(frmCONSTRUCTION_TWO_STAGE_FINAL) ? Convert.ToInt32(frmCONSTRUCTION_TWO_STAGE_FINAL) : 8;//8
            //int frm_CONSTRUCTION_MANAGEMENT = !string.IsNullOrEmpty(frmCONSTRUCTION_MANAGEMENT) ? Convert.ToInt32(frmCONSTRUCTION_MANAGEMENT) : 9;//9
            //int frm_NON_CONSTRUCTION_DRAFT_CLOSING = !string.IsNullOrEmpty(frmNON_CONSTRUCTION_DRAFT_CLOSING) ? Convert.ToInt32(frmNON_CONSTRUCTION_DRAFT_CLOSING) : 10;//10
            //int frm_CLOSING_TWO_STAGE_INITIAL = !string.IsNullOrEmpty(frmCLOSING_TWO_STAGE_INITIAL) ? Convert.ToInt32(frmCLOSING_TWO_STAGE_INITIAL) : 11;//11
            //int frm_CLOSING_TWO_STAGE_FINAL = !string.IsNullOrEmpty(frmCLOSING_TWO_STAGE_FINAL) ? Convert.ToInt32(frmCLOSING_TWO_STAGE_FINAL) : 12;//12
            //int frm_FORM_290 = !string.IsNullOrEmpty(frmFORM_290) ? Convert.ToInt32(frmFORM_290) : 16;//16
            //int frm_NON_CONSTRUCTION_EXECUTED_CLOSING = !string.IsNullOrEmpty(frmNON_CONSTRUCTION_EXECUTED_CLOSING) ? Convert.ToInt32(frmNON_CONSTRUCTION_EXECUTED_CLOSING) : 17;//17
            bool isForm290 = false;
            var EmailToLendertModel = new Prod_EmailToLenderModel();
            var task = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
            EmailToLendertModel.SendToEmail = task.AssignedBy;
            var fhaRequest = fhaRequestManager.GetFhaRequestByFhaNumber(task.FHANumber);
            var emailTypes = fhaRequest.ProjectTypeId == (int)ProdProjectTypes.IRR ? lookupMgr.GetAllEmailTypes().Where
                                                                                 (t1 => t1.EmailTypeId == (Int32)EmailType.FIRMAP ||
                                                            t1.EmailTypeId == (Int32)EmailType.SSTGS || t1.EmailTypeId == (Int32)EmailType.IRRAP) :
                                                            lookupMgr.GetAllEmailTypes().Where
                                                                                 (t1 => t1.EmailTypeId == (Int32)EmailType.FIRMAP ||
                                                            t1.EmailTypeId == (Int32)EmailType.SSTGS);
            var productionUser = (from prodUsers in productionQueue.GetProductionUsers()
                                  select new
                                  {
                                      prodUsers.UserName,
                                      Name = String.Format("{0} {1} {2} {3} {4}", prodUsers.FirstName, prodUsers.LastName, "( ", prodUsers.RoleName, " )")

                                  }).OrderBy(o => o.Name);
            const int a = 1;
            switch (task.PageTypeId)
            {
                case 3:
                    var Email_Type_3 = (from emailType in emailTypes
                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_3, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 5:
                    var Email_Type_5 = (from emailType in emailTypes
                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_5, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 6:
                    var Email_Type_6 = (from emailType in lookupMgr.GetAllEmailTypes().Where
                                 (t1 => t1.EmailTypeId == (Int32)EmailType.FIRMAP ||
                                      t1.EmailTypeId == (Int32)EmailType.SSTGS)

                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_6, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 7:
                    var Email_Type_7 = (from emailType in lookupMgr.GetAllEmailTypes().Where
                                  (t1 => t1.EmailTypeId == (Int32)EmailType.FIRMAP ||
                                      t1.EmailTypeId == (Int32)EmailType.INISUB)

                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_7, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 8:
                    var Email_Type_8 = (from emailType in lookupMgr.GetAllEmailTypes()

                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_8, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 9:
                    var Email_Type_9 = (from emailType in lookupMgr.GetAllEmailTypes().Where
                               (t1 => t1.EmailTypeId == (Int32)EmailType.FRMCMMTMNT ||
                                  t1.EmailTypeId == (Int32)EmailType.ERSTRAPP ||
                                    t1.EmailTypeId == (Int32)EmailType.INCLDATE)

                                        select new
                                        {
                                            emailType.EmailTypeId,
                                            emailType.EmailTypeCd,
                                            emailType.EmailTypeDescription
                                        }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(Email_Type_9, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 10:
                    var emialtype10 = (from emailType in lookupMgr.GetAllEmailTypes().Where(t1 => t1.EmailTypeId == (Int32)EmailType.IRRCLSG)
                                       select new
                                       {
                                           emailType.EmailTypeId,
                                           emailType.EmailTypeCd,
                                           emailType.EmailTypeDescription
                                       }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(emialtype10, "EmailTypeCd", "EmailTypeDescription");
                    break;
                case 16: //skumar-frm260
                    var emailType16 = (from emailType in lookupMgr.GetAllEmailTypes().Where(t1 => t1.EmailTypeId == (Int32)EmailType.FRM290CLOS)
                                       select new
                                       {
                                           emailType.EmailTypeId,
                                           emailType.EmailTypeCd,
                                           emailType.EmailTypeDescription
                                       }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(emailType16, "EmailTypeCd", "EmailTypeDescription");
                    isForm290 = true;
                    break;
                case 18:
                    var emailType18 = (from emailType in lookupMgr.GetAllEmailTypes().Where(t1 => t1.EmailTypeId == (Int32)EmailType.WLMAMEND1)
                                       select new
                                       {
                                           emailType.EmailTypeId,
                                           emailType.EmailTypeCd,
                                           emailType.EmailTypeDescription
                                       }).OrderBy(o => o.EmailTypeDescription);
                    EmailToLendertModel.Emailtemplate = new SelectList(emailType18, "EmailTypeCd", "EmailTypeDescription");
                    break;
            }
            EmailToLendertModel.CCToEmailList = new SelectList(productionUser, "UserName", "Name");
            EmailToLendertModel.TaskInstanceID = taskInstanceId;
            return View("~/Views/OPAForm/OPARequest/popup/SendEmailToLender.cshtml", EmailToLendertModel);
        }
        //Added  by siddu 18122019//
        [HttpPost]
        public JsonResult EmailToLender(Guid taskInstanceID, string ToEmail, string CCEmails, string template)
        {
            List<string> Emaillist = new List<string>();
            string result = string.Empty;
            if (!string.IsNullOrEmpty(CCEmails))
            {
                Emaillist = (new List<string>(CCEmails.Split(',')));
            }
            if (template.Equals(EmailType.FRM290CLOS.ToString()))
            {
                string accountExecutive = string.Empty;
                var form290Task = productionQueue.GetForm290TaskBySingleTaskInstanceID(taskInstanceID);
                if (form290Task != null && form290Task.ClosingTaskInstanceID != null)
                {
                    accountExecutive = sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(form290Task.ClosingTaskInstanceID);
                }
                else
                {
                    accountExecutive = sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(taskInstanceID);
                }

                var Model = new Prod_EmailToLenderModel()
                {
                    CCEmails = Emaillist,
                    SendToEmail = ToEmail,
                    EmailTemplateId = template,
                    TaskInstanceID = taskInstanceID,
                    AccountExecutive = accountExecutive,
                };
                result = backgroundJobManager.SendLenderNotificationsEmail(new EmailManager(), Model);


                if (result == "true")
                {
                    var form290model = new TaskModel()
                    {
                        TaskInstanceId = taskInstanceID,
                        SequenceId = 1,
                        FHANumber = taskManager.GetLatestTaskByTaskInstanceId(taskInstanceID).FHANumber,
                        TaskStepId = (int)TaskStep.Form290Complete,
                        PageTypeId = (int)PageType.Form290,
                        AssignedBy = UserPrincipal.Current.UserName,
                        AssignedTo = UserPrincipal.Current.UserName,
                        //StartTime = DateTime.Now
                        StartTime = DateTime.UtcNow
                    };
                    productionQueue.AddTask(form290model);

                }

            }
            else
            {
                if (template.Equals(EmailType.WLMAMEND2.ToString()))
                {
                    if (Emaillist.Count > 0)
                        ToEmail = Emaillist.FirstOrDefault().ToString();
                }
                var Model = new Prod_EmailToLenderModel()
                {
                    CCEmails = Emaillist,
                    SendToEmail = ToEmail,
                    EmailTemplateId = template,
                    TaskInstanceID = taskInstanceID,
                    AccountExecutive = string.Empty,//accountExecutive,//sharepointScreenManager.GetAccountExecutiveByTaskInstanceId(taskInstanceID),
                };
                result = backgroundJobManager.SendLenderNotificationsEmail(new EmailManager(), Model);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //Added by siddu//
        public JsonResult GetProdLenderUploadFileGridContentInternalProducts(string sidx, string sord, int page, int rows, Guid? taskInstanceId, string PropertyId, string FhaNumber, string FormName)
        {
            //41
            if (taskInstanceId == null)
                taskInstanceId = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;
            var AppProcessViewModel = new OPAViewModel();
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            AppProcessViewModel.OpaHistoryViewModel = GetOpaHistory((Guid)taskInstanceId);
            var opahist = new OPAHistoryViewModel();
            if (AppProcessViewModel.OpaHistoryViewModel != null)
            {
                foreach (var item in AppProcessViewModel.OpaHistoryViewModel)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    opahist.actionTaken = item.actionTaken;
                    opahist.userRole = item.userRole;

                }

            }
            var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId.Value, PropertyId, FhaNumber);

            foreach (var intern in GridContent)
            {
                if (intern.folderNodeName == "Internal Products")
                {
                    intern.FolderStructureKey = 41;
                }
            }

            GridContent = GridContent.Where(x => x.FolderStructureKey == 41).ToList();
            foreach (var prevfile in GridContent)
            {
                if (prevfile.FolderStructureKey == 41)
                {
                    if (prevfile.id == null || prevfile.id != null)
                    {
                        prevfile.id = "41";
                    }
                }
                else if (prevfile.FolderStructureKey == 40)
                {
                    if (prevfile.id == null)
                    {
                        prevfile.id = "40";
                    }
                }
            }

            //GridContent = GridContent.Where(x => x.id == "41" || x.folderNodeName == "Internal Products").ToList();
            //foreach (var item in GridContent)
            //{
            //    if (item.id.Contains("_"))
            //        item.id = item.id.Split('_')[0];
            //}
            //GridContent = GridContent.Where(x => x.id != "32" && x.id != "39" && x.id != "40").ToList();
            GridContent = GridContent.Where(x => x.id.Split('_')[0] != "32" && x.id.Split('_')[0] != "39" && x.id.Split('_')[0] != "40").ToList();
            GridContent = GridContent.Where(x => x.FolderStructureKey == 41).ToList();
            GridContent = GridContent.GroupBy(o => new { o.fileId }).Select(o => o.FirstOrDefault()).ToList();
            foreach (var ud in GridContent)
            {
                if (ud.folderNodeName != "Internal Products")
                {
                    ud.id = "41_" + ud.fileId;
                }
            }
            // commented below line for not getting existing files in grid
            //GridContent = GridContent.Where(x => x.parent == null).ToList();
            if (GridContent != null)
            {
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                foreach (var item in GridContent)
                {
                    if (item.fileId != null)
                    {
                        item.RoleName = currentUserRole;
                    }
                    if (item.RoleName != "AccountExecutive")
                    {
                        //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                        //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                        item.uploadDate = item.uploadDate;
                        item.submitDate = item.submitDate;

                        if (item.fileId != null)
                        {
                            var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
                            if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                            || currentUserRole == "BackupAccountManager")
                                item.isUploadedByMe = true;
                            //Added by siddu 19122019//
                            item.role = "AccountExecutive";
                            //item.name = uploadedBy;
                            //item.role = opahist.userRole;
                            //item.name = UserPrincipal.Current.FullName;
                            item.name = accountManager.GetFullUserNameById(item.CreatedBy);
                            item.actionTaken = opahist.actionTaken;
                            //item.actionTaken = griditem.actionTaken;
                        }
                        else
                        {
                            if (item.SubfolderSequence != null)
                            {
                                item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                            }
                        }
                    }
                    else
                    {
                        item.role = currentUserRole;
                        item.name = accountManager.GetFullUserNameById(item.CreatedBy);
                        item.isUploadedByMe = true;
                    }


                }
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var result = GridContent.ToList();//.OrderBy(a => a.id);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results.OrderBy(x => x.uploadDate).ToList(),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        //Added by siddu 19122019//
        [ValidateInput(false)]
        public ActionResult SaveDropzoneJsUploadedFilesExternalProducts(FolderUploadModel fileFolderModel)
        {
            // FolderUploadModel fileFolderModel
            OPAViewModel OPAProjectActionViewModel = new OPAViewModel();
            int userID = 0;
            string userModel = string.Empty;
            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;
            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            string value = string.Empty;

            var OPAModel = new OPAViewModel();
            string GroupTaskInstanceId = string.Empty;
            int pageTypeId = 0;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();
            //Logic for check  whether a task Exists
            var parentdict = GetParentModelDictionary(ParentModel);
            bool IsTaskavilable = false;

            try
            {
                if (parentdict.Count > 0)
                {

                    if (parentdict.ContainsKey("TaskInstanceId") && !string.IsNullOrEmpty((string)parentdict["TaskInstanceId"]))
                    {
                        GroupTaskInstanceId = (string)parentdict["TaskInstanceId"];

                    }
                    if (parentdict.ContainsKey("pageTypeId") && !string.IsNullOrEmpty((string)parentdict["pageTypeId"]))
                    {

                        pageTypeId = Convert.ToInt32((string)parentdict["pageTypeId"]);

                    }
                    // while file uploadig first time we are saving data both opa and grouptask table .
                    var grouptaskmodel = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(GroupTaskInstanceId));
                    if (grouptaskmodel == null)
                    {

                        //Save OpaForm


                        OPAProjectActionViewModel.ServicerComments = "";

                        OPAProjectActionViewModel.PropertyName = parentdict["PropertyName"] != null ? (string)parentdict["PropertyName"] : "";
                        OPAProjectActionViewModel.RequesterName = parentdict["RequesterName"] != null ? (string)parentdict["RequesterName"] : "";
                        OPAProjectActionViewModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        //  var  FHA = Request.Form["FHA"];
                        OPAProjectActionViewModel.IsAddressChange = parentdict["IsAddressChange"] != null ? Convert.ToBoolean(parentdict["IsAddressChange"]) : false;
                        //model.FhaNumber = FHA;
                        //Insert the non critical request and get the generated id, insert into view model
                        OPAProjectActionViewModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : ""; ;
                        //AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        OPAProjectActionViewModel.ServicerSubmissionDate = null;
                        OPAProjectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                        OPAProjectActionViewModel.CreatedOn = DateTime.UtcNow;
                        OPAProjectActionViewModel.ModifiedOn = DateTime.UtcNow;
                        OPAProjectActionViewModel.CreatedBy = UserPrincipal.Current.UserId;
                        OPAProjectActionViewModel.ModifiedBy = UserPrincipal.Current.UserId;
                        OPAProjectActionViewModel.ProjectActionDate = DateTime.UtcNow;
                        OPAProjectActionViewModel.RequestDate = DateTime.UtcNow;
                        OPAProjectActionViewModel.TaskInstanceId = new Guid(GroupTaskInstanceId);
                        OPAProjectActionViewModel.pageTypeId = pageTypeId;
                        OPAProjectActionViewModel.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAProjectActionViewModel.ProjectActionTypeId);
                        OPAProjectActionViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(OPAProjectActionViewModel.FhaNumber).PropertyId;

                        var groupTaskModel = new GroupTaskModel();
                        // harish commented
                        groupTaskModel.TaskInstanceId = OPAProjectActionViewModel.TaskInstanceId;
                        groupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.FhaNumber = OPAProjectActionViewModel.FhaNumber;
                        groupTaskModel.PropertyName = OPAProjectActionViewModel.PropertyName;
                        groupTaskModel.RequestDate = OPAProjectActionViewModel.RequestDate;
                        groupTaskModel.RequesterName = OPAProjectActionViewModel.RequesterName;
                        groupTaskModel.ServicerSubmissionDate = OPAProjectActionViewModel.ServicerSubmissionDate;
                        groupTaskModel.ServicerComments = parentdict["ServicerComments"] != null ? (string)parentdict["ServicerComments"] : "";
                        groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                        groupTaskModel.ProjectActionTypeId = OPAProjectActionViewModel.ProjectActionTypeId;
                        groupTaskModel.IsDisclimerAccepted = OPAProjectActionViewModel.IsAgreementAccepted;
                        groupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.CreatedOn = DateTime.UtcNow;
                        groupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        groupTaskModel.ModifiedOn = DateTime.UtcNow;
                        groupTaskModel.IsAddressChanged = OPAProjectActionViewModel.IsAddressChange;
                        groupTaskModel.ServicerComments = OPAProjectActionViewModel.ServicerComments;
                        OPAProjectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);



                        var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAProjectActionViewModel);

                        InstanceID = groupTaskModel.TaskInstanceId;
                        OPAProjectActionViewModel.GroupTaskInstanceId = InstanceID;
                        OPAProjectActionViewModel.ProjectActionFormId = projectActionFormId;
                    }
                    else
                    {
                        OPAProjectActionViewModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        OPAProjectActionViewModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : "";
                        OPAProjectActionViewModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
                        OPAProjectActionViewModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(OPAProjectActionViewModel.FhaNumber).PropertyId;
                        //skumar-form290 requires following information
                        OPAProjectActionViewModel.pageTypeId = pageTypeId;
                        if (parentdict.ContainsKey("ModifiedBy"))
                        {
                            if (parentdict["ModifiedBy"] != null)
                            {
                                userModel = (string)parentdict["ModifiedBy"];
                                if (string.IsNullOrEmpty(userModel))
                                    userModel = (string)parentdict["CreatedBy"];
                                userID = Convert.ToInt32(userModel);
                            }
                            OPAProjectActionViewModel.userId = userID;
                        }

                    }
                }
                var success = this.UploadExternalProducts(OPAProjectActionViewModel, Convert.ToInt32(FolderKey));

                if (success)
                {
                    var nextStage = new Prod_NextStageModel();

                    nextStage.NextPgTypeId = pageTypeId;
                    nextStage.ModifiedOn = DateTime.UtcNow;
                    nextStage.ModifiedBy = UserPrincipal.Current.UserId;
                    nextStage.FhaNumber = OPAProjectActionViewModel.FhaNumber;
                    nextStage.NextPgTaskInstanceId = OPAProjectActionViewModel.GroupTaskInstanceId;
                    nextStage.IsNew = false;
                    //prod_NextStageManager.UpdateTaskInstanceIdForNxtStage(nextStage);
                    // Venkatesh They are passing the page type id by default as production application I cahnge it dinamic
                    //AppProcessModel.pageTypeId = (int)PageType.ProductionApplication;
                    OPAProjectActionViewModel.pageTypeId = pageTypeId;
                    // Save ReviewFileStatus
                    //string assignedto = projectActionFormManager.GetAeEmailByFhaNumber(OPAProjectActionViewModel.FhaNumber);
                    //int userId = webSecurity.GetUserId(assignedto);
                    //Guid taskInstanceID = OPAProjectActionViewModel.TaskInstanceId;
                    //int reviewerUserId = userId;
                    //int currentUserId = UserPrincipal.Current.UserId;
                    //taskManager.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                    ViewBag.UploadInternalFailed = "False";
                    TempData["UploadInternalFailed"] = "False";

                }
                else
                {
                    ViewBag.UploadInternalFailed = "True";
                    TempData["UploadInternalFailed"] = "True";

                }
                if (pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", OPAProjectActionViewModel);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestAEandReadOnly.cshtml", OPAProjectActionViewModel);
                }
            }
            catch (Exception e)
            {
                //todo:skumar: redirect to error page
                return RedirectToAction("Login", "Account");
            }
        }
        //Added by siddu 19122019//
        [ValidateInput(false)]
        private bool UploadExternalProducts(OPAViewModel OPAProjectActionViewModel, int FolderKey)
        {
            PropertyInfoModel PropertyInfoModel = new PropertyInfoModel();
            var OPAModel = new OPAViewModel();
            OPAProjectActionViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(OPAProjectActionViewModel.FhaNumber).PropertyId;
            OPAProjectActionViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(OPAProjectActionViewModel.ProjectActionTypeId);
            string FileSectionName = "";
            var FolderInfo = projectActionFormManager.GetFolderInfoByKey(FolderKey);
            var PageName = projectActionFormManager.FormNameBYPageTypeID(OPAProjectActionViewModel.pageTypeId);
            string folderNames = string.Empty;
            string ProjectActionTypeId = string.Empty;
            ProjectActionTypeId = Request.Form["ProjectActionTypeId"];
            string GroupTaskId = string.Empty;
            // OPAProjectActionViewModel.GroupTaskId = 2010;
            GroupTaskId = Request.Form["GroupTaskId"];
            string formNameConfiguration = ConfigurationManager.AppSettings["FORM 290"] ?? ConfigurationSettings.AppSettings["FORM 290"];
            string amendmentConfig = ConfigurationManager.AppSettings["Amendments"] ?? ConfigurationSettings.AppSettings["Amendments290"];
            int ammendmentPageId = !string.IsNullOrEmpty(amendmentConfig) ? Convert.ToInt32(amendmentConfig) : 0;
            int form290PageId = !string.IsNullOrEmpty(formNameConfiguration) ? Convert.ToInt32(formNameConfiguration) : 0;
            if (FolderInfo != null)
            {
                foreach (Prod_SubFolderStructureModel folder in FolderInfo)
                {


                    if (OPAProjectActionViewModel.pageTypeId == form290PageId)//skumar-form290 - folder map 
                    {
                        folderNames = "/" + OPAProjectActionViewModel.PropertyId.ToString() + "/" + OPAProjectActionViewModel.FhaNumber + "/Production/ExecutedClosing/WorkProduct/Form290/";
                    }
                    else if (OPAProjectActionViewModel.pageTypeId == ammendmentPageId)//skumar-2044 amendemtns=18
                    {
                        folderNames = "/" + OPAProjectActionViewModel.PropertyId.ToString() + "/" + OPAProjectActionViewModel.FhaNumber + "/Production/Closing/WorkProduct/Amendments/";
                    }
                    else if (FolderInfo[0].ViewTypeId == 2)
                    {
                        folderNames = "Production/" + PageName + "/" + "WorkProduct/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());
                    }

                    else
                    {
                        //Consider the case where there is no SubfolderSequence
                        //   folderNames = "Production/" + PageName + "/" + folder.FolderName + ((folder.SubfolderSequence == null) ? "" : "_" + folder.SubfolderSequence.Trim());

                        //folderNames = "Asset Management /" + OPAProjectActionViewModel.ProjectActionName + "/" + OPAProjectActionViewModel.PropertyId + "/" + OPAProjectActionViewModel.FhaNumber + "/Asset Management/OPA/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy");
                        folderNames = "/" + OPAProjectActionViewModel.PropertyId + "/" + OPAProjectActionViewModel.FhaNumber + "/" + "Asset Management /" + OPAProjectActionViewModel.ProjectActionName + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy");

                    }
                }

            }
            RestfulWebApiUploadModel[] obj1 = new RestfulWebApiUploadModel[1];
            var uploadmodel = new RestfulWebApiUploadModel()
            {
                // propertyID = AppProcessViewModel.PropertyId.ToString(),
                propertyID = OPAProjectActionViewModel.PropertyId.ToString(),
                indexType = "1",
                // indexValue = AppProcessViewModel.FhaNumber,
                indexValue = OPAProjectActionViewModel.FhaNumber,
                //pdfConvertableValue = "false",
                pdfConvertableValue = "false",
                folderKey = FolderKey,
                folderNames = folderNames,
                transactionType = OPAProjectActionViewModel.ProjectActionTypeId.ToString(),
                transactionStatus = "",
                transactionDate = DateTime.UtcNow.ToString("MM-dd-yyyy")
                // folderNames = "Asset Management /" + OPAProjectActionViewModel.ProjectActionName + "/" + OPAProjectActionViewModel.PropertyId + "/" + OPAProjectActionViewModel.FhaNumber + "/Asset Management/OPA/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy")
                // folderNames = PropertyInfoModel.PropertyId.ToString() + "/" + OPAProjectActionViewModel.FhaNumber + "/Asset Management/Project Action Form/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy") + "/" + FileSectionName,
            };
            obj1[0] = uploadmodel;
            //karri#162
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //request = WebApiTokenRequest.RequestToken();

            bool isUploaded = false;

            foreach (string Filename in Request.Files)
            {
                // var TEMP = Request.Files[Filename];
                //HttpPostedFileBase Temp;

                HttpPostedFileBase myFile = Request.Files[Filename];
                var t = myFile.InputStream;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();

                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = OPAProjectActionViewModel.FhaNumber + "_" +
                                         OPAProjectActionViewModel.ProjectActionTypeId + "_" +
                                         OPAProjectActionViewModel.GroupTaskInstanceId + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);

                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {

                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = true;

                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    //karri#162,#171
                    request = WebApiTokenRequest.RequestToken();

                    if (OPAProjectActionViewModel.pageTypeId == 18)
                    {
                        WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, FolderInfo[0].FolderName + "_" + FolderInfo[0].SubfolderSequence);
                    }
                    else
                    {
                        WebApiUploadResult = WebApiDocumentUpload.AssetManagementUploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");
                    }


                    if (System.IO.File.Exists(Path.Combine(pathForSaving, systemFileName)))
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));

                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {

                        isUploaded = true;
                        var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), new Guid(OPAProjectActionViewModel.GroupTaskInstanceId.ToString()));
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        //if (AppProcessViewModel.pageTypeId == 18)
                        //{
                        //string subFolder = string.Empty;
                        //subFolder = !string.IsNullOrEmpty(FolderInfo[0].SubfolderSequence) ? FolderInfo[0].SubfolderSequence.Trim() : subFolder;
                        //if (!string.IsNullOrEmpty(subFolder))
                        //{
                        //    taskFile.FileName = FolderInfo[0].FolderName + "_" + FolderInfo[0].SubfolderSequence.Trim() + "!" + myFile.FileName;
                        //}
                        //else
                        //{
                        //    taskFile.FileName = FolderInfo[0].FolderName + "_" + subFolder + "!" + myFile.FileName;
                        //}
                        //}
                        if (WebApiUploadResult.documentType != DefaultDocID)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        else if (FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductClosing) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductUnderwriting) ||
                                FolderKey == Convert.ToInt32(ProdFolderStructure.WorkProductLoanCommittee))
                        {
                            //taskFile.DocTypeID = DefaultDocID;
                            taskFile.FileType = FileType.WP;
                        }
                        else if (OPAProjectActionViewModel.pageTypeId == 18)
                        {
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                        }
                        // OPAProjectActionViewModel.ProjectActionName =
                        // projectActionFormManager.GetProjectActionName(OPAModel.ProjectActionTypeId);
                        //OPAProjectActionViewModel.ProjectActionTypeId = OPAModel.ProjectActionTypeId;


                        taskFile.RoleName = UserPrincipal.Current.UserRole;
                        taskFile.FolderName = folderNames;
                        taskFile.FolderStructureKey = 40;
                        var taskfileid = taskManager.SaveGroupTaskFileModel(taskFile);
                        var mappingmodel = new TaskFile_FolderMappingModel();
                        mappingmodel.FolderKey = FolderKey;
                        mappingmodel.TaskFileId = taskfileid;
                        mappingmodel.CreatedOn = DateTime.UtcNow;

                        taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);

                        //Code for saving new files on RAI
                        if (OPAProjectActionViewModel.TaskGuid != null)
                        {
                            var childTaskNewFile = new ChildTaskNewFileModel();
                            childTaskNewFile.ChildTaskNewFileId = Guid.NewGuid();
                            childTaskNewFile.ChildTaskInstanceId = OPAProjectActionViewModel.TaskGuid.Value;
                            childTaskNewFile.TaskFileInstanceId = taskfileid;
                            projectActionFormManager.SaveChildTaskNewFiles(childTaskNewFile);

                        }
                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        isUploaded = false;

                    }

                    //OPAProjectActionViewModel.ProjectActionName =
                    //      projectActionFormManager.GetProjectActionName(OPAProjectActionViewModel.ProjectActionTypeId);
                    //IList<ProjectActionTypeViewModel> projectActionList =
                    //   projectActionManager.GetAllProjectActionsByPageId(3);
                    //foreach (var model in projectActionList)
                    //{
                    //    OPAProjectActionViewModel.ProjectActionTypeList.Add(new SelectListItem()
                    //    {
                    //        Text = model.ProjectActionName,
                    //        Value = model.ProjectActionID.ToString()
                    //    });
                    //}



                }
            }
            return isUploaded;
        }
        //Added by siddu 19122019//
        // [HttpPost]
        [ValidateInput(false)]
        public ActionResult UploadPopupLoadExternalProducts(int FolderKey, string Model)
        {
            FolderKey = 40;
            var parentdict = GetParentModelDictionary(Model);
            ViewBag.uploadurl = "/OPAForm/SaveDropzoneJsUploadedFilesExternalProducts";
            var formUploadData = new OPAViewModel();
            if (parentdict != null)
            {
                var PagetypeID = parentdict.FirstOrDefault(x => x.Key == "pageTypeId").Value;
                if (Convert.ToInt32(PagetypeID) == 18)
                {
                    ViewBag.redirecturl = "/Amendments/Reload/GroupTaskInstanceId?=";
                }
                else
                {
                    // ViewBag.redirecturl = "/OPAForm/Reload/GroupTaskInstanceId?=";
                    string redURL = "/Task/GetTaskDetail/" + parentdict.FirstOrDefault(x => x.Key == "TaskId").Value + "?fromTaskReAssignment=False&style=text-decoration%3Aunderline";
                    ViewBag.redirecturl = redURL;
                    // return RedirectToAction("GetOPAFormDetail", "OPAForm", new { @model = formUploadData });
                    //ViewBag.redirecturl = "/OPAForm/GetTaskDetail?taskInstanceId=";
                    // ViewBag.redirecturl = "/OPAForm/GetOPAFormDetail/GroupTaskInstanceId?=";

                    // GetOPAFormDetail
                    //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();
                }
            }

            var folderuploadModel = new FolderUploadModel { FolderKey = FolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };


            return View("~/Views/Production/_Shared/_DragDropFileUpload.cshtml", folderuploadModel);
        }


        public JsonResult GetProdLenderUploadFileGridContentExternalProducts(string sidx, string sord, int page, int rows, Guid? taskInstanceId, string PropertyId, string FhaNumber, string FormName)
        {
            //40
            if (taskInstanceId == null)
                taskInstanceId = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;
            var AppProcessViewModel = new OPAViewModel();
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            AppProcessViewModel.OpaHistoryViewModel = GetOpaHistory((Guid)taskInstanceId);
            var opahist = new OPAHistoryViewModel();
            if (AppProcessViewModel.OpaHistoryViewModel != null)
            {
                foreach (var item in AppProcessViewModel.OpaHistoryViewModel)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    opahist.actionTaken = item.actionTaken;
                    opahist.userRole = item.userRole;

                }

            }
            var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId.Value, PropertyId, FhaNumber);

            foreach (var uid in GridContent)
            {
                if (uid.folderNodeName == "External Products")
                {
                    uid.FolderStructureKey = 40;
                }
            }
            GridContent = GridContent.Where(x => x.FolderStructureKey == 40).ToList();
            foreach (var prevfile in GridContent)
            {
                if (prevfile.FolderStructureKey == 40)
                {
                    if (prevfile.id == null)
                    {
                        prevfile.id = "40";
                    }
                }

                else if (prevfile.FolderStructureKey == 41)
                {
                    if (prevfile.id == null)
                    {
                        prevfile.id = "41";
                    }
                }
            }

            GridContent = GridContent.Where(x => x.id.Split('_')[0] != "32" && x.id.Split('_')[0] != "39" && x.id.Split('_')[0] != "41").ToList();
            GridContent = GridContent.Where(x => x.FolderStructureKey == 40).ToList();
            GridContent = GridContent.GroupBy(o => new { o.fileId }).Select(o => o.FirstOrDefault()).ToList();//for neglecting duplicate records
            foreach (var ud in GridContent)
            {
                if (ud.folderNodeName != "External Products")
                {
                    ud.id = "40_" + ud.fileId;
                }
            }
            if (GridContent != null)
            {
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                // var currentUserName = UserPrincipal.Current.Roles;

                foreach (var item in GridContent)
                {
                    if (item.fileId != null)
                    {
                        item.RoleName = currentUserRole;
                    }

                    if (item.RoleName != "AccountExecutive")
                    {
                        //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                        //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                        item.uploadDate = item.uploadDate;
                        item.submitDate = item.submitDate;

                        if (item.fileId != null)
                        {
                            var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;




                            if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                            || currentUserRole == "BackupAccountManager")


                                item.isUploadedByMe = true;
                            item.role = currentUserRole;

                            item.name = accountManager.GetFullUserNameById(item.CreatedBy);
                            item.actionTaken = opahist.actionTaken;
                            //item.actionTaken = griditem.actionTaken;
                        }


                        //  }

                        else
                        {
                            if (item.SubfolderSequence != null)
                            {
                                item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                            }
                        }
                    }
                    else
                    {
                        item.role = currentUserRole;
                        item.name = accountManager.GetFullUserNameById(item.CreatedBy);
                        item.isUploadedByMe = true;
                        //var AEname =
                    }

                }
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var result = GridContent.ToList();//.OrderBy(a => a.id);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results.OrderBy(x => x.uploadDate).ToList(),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }







        //Added by siddu 18122019//
        public JsonResult GetProdLenderUploadFileGridContentExternalProductsTest(string sidx, string sord, int page, int rows, Guid? taskInstanceId, string PropertyId, string FhaNumber, string FormName)
        {
            //40
            if (taskInstanceId == null)
                taskInstanceId = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;
            var AppProcessViewModel = new OPAViewModel();
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            AppProcessViewModel.OpaHistoryViewModel = GetOpaHistory((Guid)taskInstanceId);
            // var opahistlist = Apppro
            var opahist = new OPAHistoryViewModel();
            if (AppProcessViewModel.OpaHistoryViewModel != null)
            {
                foreach (var item in AppProcessViewModel.OpaHistoryViewModel)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    opahist.actionTaken = item.actionTaken;
                    opahist.userRole = item.userRole;

                }

            }
            var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId.Value, PropertyId, FhaNumber);

            foreach (var prevfile in GridContent)
            {
                if (prevfile.FolderStructureKey == 40)
                {
                    if (prevfile.id == null)
                    {
                        prevfile.id = "40_" + prevfile.fileId;
                    }
                }

                else if (prevfile.FolderStructureKey == 41)
                {
                    if (prevfile.id == null)
                    {
                        prevfile.id = "41";
                    }
                }
            }
            //GridContent = GridContent.Where(x => x.id == "40").ToList();
            GridContent = GridContent.Where(x => x.id.Split('_')[0] == "40" || x.folderNodeName == "External Products").ToList();
            //foreach(var item in GridContent)
            //{
            //    if (item.id.Contains("_"))
            //        item.id = item.id.Split('_')[0];


            //}
            GridContent = GridContent.Where(x => x.id.Split('_')[0] != "32" && x.id.Split('_')[0] != "39" && x.id.Split('_')[0] != "41").ToList();
            GridContent = GridContent.GroupBy(o => new { o.fileId }).Select(o => o.FirstOrDefault()).ToList();//for neglecting duplicate records

            if (GridContent != null)
            {
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                // var currentUserName = UserPrincipal.Current.Roles;

                foreach (var item in GridContent)
                {
                    if (item.fileId != null)
                    {
                        item.RoleName = currentUserRole;
                    }

                    if (item.RoleName != "AccountExecutive")
                    {
                        //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                        //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                        item.uploadDate = item.uploadDate;
                        item.submitDate = item.submitDate;
                        //if (!string.IsNullOrEmpty(item.id))
                        //    idname = new String[] { };
                        //    idname = item.id.Split('_');
                        //if (item.id.Contains("_"))
                        //    item.id = item.id.Split('_')[0];
                        if (item.fileId != null)
                        {
                            var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;




                            if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                            || currentUserRole == "BackupAccountManager")


                                item.isUploadedByMe = true;
                            item.role = currentUserRole;
                            // item.role = "AE";
                            //item.name = opahist.name;
                            //Added by siddu 19122019//
                            //item.role = "AE";
                            // item.role = "LAR";
                            //item.role = UserPrincipal.Current.UserRole;
                            //item.role = opahist.userRole;
                            //item.name = UserPrincipal.Current.FullName;
                            item.name = accountManager.GetFullUserNameById(item.CreatedBy);
                            item.actionTaken = opahist.actionTaken;
                            //item.actionTaken = griditem.actionTaken;
                        }


                        //  }

                        else
                        {
                            if (item.SubfolderSequence != null)
                            {
                                item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                            }
                        }
                    }
                    else
                    {
                        item.role = currentUserRole;
                        item.name = accountManager.GetFullUserNameById(item.CreatedBy);
                        item.isUploadedByMe = true;
                        //var AEname = 
                    }

                }
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var result = GridContent.ToList();//.OrderBy(a => a.id);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results.OrderBy(x => x.uploadDate).ToList(),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        // harish added new method for new request  03-1-2020
        //Code for Firm Commitment Sent
        public string FirmCommitmentSent()
        {
            //var Instanceid = new Guid(Request.QueryString[0]);
            //string Instanceid= Request.QueryString[0];
            var taskxref = new Guid(Request.QueryString[0]);
            var propertyname = Request.QueryString[1];
            var ProjectActionName = Request.QueryString[2];

            // var pagetypeid = (int)(Convert.ToInt32(Request.QueryString[2]));
            string message = "Success";
            try
            {

                var parentTask = taskManager.GetTasksByTaskInstanceId(new Guid(Request.QueryString[0])).ToList();
                if (taskManager.IsFirmCommitmentCompleted(new Guid(Request.QueryString[0])))
                {
                    return "Completed";
                }
                if (taskManager.IsFirmCommitmentExists(new Guid(Request.QueryString[0])))
                {
                    return "Exists";
                }

                //var fileTask =
                //    taskManager.GetFilesByTaskInstanceIdAndFileType(new Guid(Request.QueryString[0]), FileType.FWP);
                //if (fileTask.Count == 0)
                //{
                //    return "No File";
                //}

                var taskList = new List<TaskModel>();
                var childTask = new TaskModel();


                childTask.TaskInstanceId = Guid.NewGuid();
                childTask.SequenceId = 0;
                childTask.AssignedBy = UserPrincipal.Current.UserName;
                childTask.AssignedTo = parentTask[0].AssignedBy;
                childTask.StartTime = DateTime.UtcNow;
                childTask.TaskStepId = (int)ProductionAppProcessStatus.Request;
                //childTask.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(childTask.StartTime);
                childTask.MyStartTime = childTask.StartTime;
                childTask.IsReAssigned = false;
                childTask.Concurrency = parentTask[0].Concurrency;
                childTask.DataStore1 = parentTask[0].DataStore1;
                //adding fhanumber and pagetype      
                childTask.FHANumber = parentTask[0].FHANumber;
                childTask.PageTypeId = parentTask[0].PageTypeId;
                // var model = projectActionFormManager.GetOPAByTaskInstanceId(new Guid(Request.QueryString[0]));
                // if (childTask.TaskStepId == 20)
                // {
                // model.RequestStatus = 10;
                //parentTask;
                // }

                //childTask.PageTypeId = (int)PageType.ProductionApplication; 
                var childTaskOpenStatusModel = new TaskOpenStatusModel();
                childTaskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                childTask.TaskOpenStatus = XmlHelper.Serialize(childTaskOpenStatusModel,
                    typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(childTask);

                var requestAdditionalChildTask = new ParentChildTaskModel();
                requestAdditionalChildTask.CreatedBy = UserPrincipal.Current.UserId;
                requestAdditionalChildTask.CreatedOn = DateTime.UtcNow;
                requestAdditionalChildTask.ParentTaskInstanceId = taskxref;
                requestAdditionalChildTask.ChildTaskInstanceId = childTask.TaskInstanceId;
                requestAdditionalChildTask.ParentChildTaskId = Guid.NewGuid();

                if (taskList.Count > 0)
                {
                    taskManager.SaveTask(taskList, new List<TaskFileModel>());
                }
                childTask.PropertyName = propertyname;
                childTask.TaskName = ProjectActionName;
                parentChildTaskManager.AddParentChildTask(requestAdditionalChildTask);
                backgroundJobManager.SendRISNotificationEmail(emailManager, childTask);
                //_backgroundManager.SendFirmCommitmentNotificationEmail(_emailManager, pm[cnt]);

            }
            catch (Exception ex)
            {
                message = "Failure";
            }
            return message;
        }

        //Code for Firm Commitment Lender Reply Sent
        public string FirmCommitmentReply()
        {
            //string Instanceid= Request.[0];

            string message = "Success";
            try
            {
                var parentTask = taskManager.GetTasksByTaskInstanceId(new Guid(Request.QueryString[0])).ToList();
                var parentChildTask = parentChildTaskManager.GetParentTask(new Guid(Request.QueryString[0]));
                var xrefTask = prod_TaskXrefManager.GetProductionSubtaskById(parentChildTask.ParentTaskInstanceId);
                var fileTask =
                    taskManager.GetFilesByTaskInstanceIdAndFileType(xrefTask.TaskInstanceId, FileType.FWP);
                var IsFileExists = false;
                //var IsFileExists = true;
                if (fileTask.Any())
                {
                    foreach (var item in fileTask)
                    {
                        //if (item.CreatedBy != null && RoleManager.IsProductionUser(accountManager.GetUserById((int)item.CreatedBy).UserName))
                        if (item.CreatedBy != null && RoleManager.IsUserLenderRole(accountManager.GetUserById((int)item.CreatedBy).UserName))
                        {
                            IsFileExists = true;
                        }
                    }
                }
                if (!IsFileExists)
                {
                    return "No File";
                }


                var taskList = new List<TaskModel>();
                var task = new TaskModel();

                string alertTitle = String.Empty;

                task.AssignedTo = parentTask[0].AssignedBy;
                task.AssignedBy = UserPrincipal.Current.UserName;


                task.TaskStepId = (int)ProductionAppProcessStatus.Response;


                task.DataStore1 = null;
                task.Notes = "Firm Commitment sent";
                task.SequenceId = parentTask[0].SequenceId + 1; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = parentTask[0].TaskInstanceId;
                task.IsReAssigned = parentTask[0].IsReAssigned;
                task.Concurrency = parentTask[0].Concurrency;
                //adding fhanumber and pagetype
                task.FHANumber = parentTask[0].FHANumber;
                task.PageTypeId = parentTask[0].PageTypeId;
                //task.PageTypeId = (int)PageType.ProductionApplication;
                var taskOpenStatusModel = new TaskOpenStatusModel();
                taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel),
                    Encoding.Unicode);
                taskList.Add(task);
                var taskFile = new List<TaskFileModel>();

                if (taskList.Count > 0)
                {
                    taskManager.SaveTask(taskList, new List<TaskFileModel>());
                }



            }
            catch (Exception ex)
            {
                message = "Failure";
            }
            return message;
        }

        // harish aded new upload method for ris 04-01-2020
        [ValidateInput(false)]
        public ActionResult RISUploadPopupLoad(int FolderKey, string Model)
        {
            FolderKey = 40;



            var parentdict = GetParentModelDictionary(Model);

            ViewBag.uploadurl = "/OPAForm/RISSaveDropzoneJsUploadedFilesExternalProducts";

            if (parentdict != null)
            {
                var PagetypeID = parentdict.FirstOrDefault(x => x.Key == "pageTypeId").Value;
                if (Convert.ToInt32(PagetypeID) == 18)
                {
                    ViewBag.redirecturl = "/Amendments/Reload/GroupTaskInstanceId?=";
                }
                else
                {
                    ViewBag.redirecturl = "/OPAForm/RISReload/GroupTaskInstanceId?=";
                    //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();
                }
            }

            var folderuploadModel = new FolderUploadModel { FolderKey = FolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };


            return View("~/Views/Production/_Shared/_DragDropFileUpload.cshtml", folderuploadModel);
        }

        // harish added new ris save upload fucntionality
        [ValidateInput(false)]
        public ActionResult RISSaveDropzoneJsUploadedFilesExternalProducts(FolderUploadModel fileFolderModel)
        {
            // FolderUploadModel fileFolderModel
            OPAViewModel OPAProjectActionViewModel = new OPAViewModel();
            int userID = 0;
            string userModel = string.Empty;
            //Getting the Folder Key
            bool isSavedSuccessfully = false;
            bool IsnewInsert = false;
            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            string value = string.Empty;

            var OPAModel = new OPAViewModel();
            string GroupTaskInstanceId = string.Empty;
            var taskinstanceid = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;
            int pageTypeId = 0;
            var Taskinstaceid = string.IsNullOrEmpty(value)
                       ? ""
                       : value;
            Guid InstanceID = new Guid();
            //Logic for check  whether a task Exists
            var parentdict = GetParentModelDictionary(ParentModel);
            bool IsTaskavilable = false;

            try
            {
                if (parentdict.Count > 0)
                {

                    if (parentdict.ContainsKey("TaskGuid") && !string.IsNullOrEmpty((string)parentdict["TaskGuid"]))
                    {
                        GroupTaskInstanceId = (string)parentdict["TaskGuid"];

                    }
                    if (parentdict.ContainsKey("pageTypeId") && !string.IsNullOrEmpty((string)parentdict["pageTypeId"]))
                    {

                        pageTypeId = Convert.ToInt32((string)parentdict["pageTypeId"]);

                    }
                    // var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(new Guid(GroupTaskInstanceId));
                    var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(taskinstanceid);
                    if (grouptaskmodel == null)
                    {
                        var prodgroupTaskModel = new Prod_GroupTasksModel();
                        prodgroupTaskModel.TaskInstanceId = taskinstanceid;
                        // prodgroupTaskModel.TaskInstanceId = new Guid(GroupTaskInstanceId);
                        prodgroupTaskModel.InUse = UserPrincipal.Current.UserId;

                        prodgroupTaskModel.RequestStatus = (int)RequestStatus.RequestInfoFromLender;
                        prodgroupTaskModel.PageTypeId = 3;
                        //prodgroupTaskModel.PageTypeId = (int) PageType.ProductionApplication;
                        prodgroupTaskModel.IsDisclaimerAccepted = false;
                        prodgroupTaskModel.ModifiedOn = DateTime.UtcNow;
                        prodgroupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                        prodgroupTaskModel.CreatedOn = DateTime.UtcNow;
                        prodgroupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                        // prodgroupTaskModel.ServicerComments = parentdict["ServicerComments"] != null ? (string)parentdict["ServicerComments"] : "";
                        var ProdGroupTaskId = prod_GroupTasksManager.AddProdGroupTasks(prodgroupTaskModel);


                        //Save OpaForm

                        //AppProcessModel.ProjectActionName = parentdict["ProjectActionName"] != null ? (string)parentdict["ProjectActionName"] : "";
                        //  OPAProjectActionViewModel.ProjectActionName =
                        //projectActionFormManager.GetProjectActionName(OPAProjectActionViewModel.ProjectActionTypeId);
                        OPAProjectActionViewModel.ServicerComments = "";
                        // OPAProjectActionViewModel.GroupTaskId = 2010;

                        OPAProjectActionViewModel.PropertyName = parentdict["PropertyName"] != null ? (string)parentdict["PropertyName"] : "";
                        OPAProjectActionViewModel.RequesterName = parentdict["RequesterName"] != null ? (string)parentdict["RequesterName"] : "";
                        OPAProjectActionViewModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        //  var  FHA = Request.Form["FHA"];
                        OPAProjectActionViewModel.IsAddressChange = parentdict["IsAddressChange"] != null ? Convert.ToBoolean(parentdict["IsAddressChange"]) : false;
                        //model.FhaNumber = FHA;
                        //Insert the non critical request and get the generated id, insert into view model
                        OPAProjectActionViewModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : ""; ;
                        //AppProcessModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(AppProcessModel.FhaNumber).PropertyId;
                        OPAProjectActionViewModel.ServicerSubmissionDate = null;
                        OPAProjectActionViewModel.RequestStatus = (int)RequestStatus.RequestInfoFromLender;
                        OPAProjectActionViewModel.CreatedOn = DateTime.UtcNow;
                        OPAProjectActionViewModel.ModifiedOn = DateTime.UtcNow;
                        OPAProjectActionViewModel.CreatedBy = UserPrincipal.Current.UserId;
                        OPAProjectActionViewModel.ModifiedBy = UserPrincipal.Current.UserId;
                        OPAProjectActionViewModel.ProjectActionDate = DateTime.UtcNow;
                        OPAProjectActionViewModel.RequestDate = DateTime.UtcNow;
                        OPAProjectActionViewModel.TaskInstanceId = ProdGroupTaskId;
                        OPAProjectActionViewModel.pageTypeId = pageTypeId;
                        OPAProjectActionViewModel.ProjectActionName = projectActionFormManager.GetProjectActionNameforAssetmanagement(OPAProjectActionViewModel.ProjectActionTypeId);
                        OPAProjectActionViewModel.PropertyId = projectActionFormManager.GetAssetManagementPropertyInfo(OPAProjectActionViewModel.FhaNumber).PropertyId;
                        var projectActionFormId = projectActionFormManager.SaveOPAForm(OPAProjectActionViewModel);

                        InstanceID = ProdGroupTaskId;
                        OPAProjectActionViewModel.GroupTaskInstanceId = InstanceID;
                        OPAProjectActionViewModel.ProjectActionFormId = projectActionFormId;
                    }
                    else
                    {
                        OPAProjectActionViewModel.ProjectActionTypeId = parentdict["ProjectActionTypeId"] != null ? Convert.ToInt32(parentdict["ProjectActionTypeId"]) : 0;
                        OPAProjectActionViewModel.FhaNumber = parentdict["FhaNumber"] != null ? (string)parentdict["FhaNumber"] : "";
                        OPAProjectActionViewModel.GroupTaskInstanceId = taskinstanceid;
                        //OPAProjectActionViewModel.GroupTaskInstanceId = new Guid(GroupTaskInstanceId);
                        OPAProjectActionViewModel.PropertyId = projectActionFormManager.GetProdPropertyInfo(OPAProjectActionViewModel.FhaNumber).PropertyId;
                        //skumar-form290 requires following information
                        OPAProjectActionViewModel.pageTypeId = pageTypeId;
                        if (parentdict.ContainsKey("ModifiedBy"))
                        {
                            if (parentdict["ModifiedBy"] != null)
                            {
                                userModel = (string)parentdict["ModifiedBy"];
                                if (string.IsNullOrEmpty(userModel))
                                    userModel = (string)parentdict["CreatedBy"];
                                userID = Convert.ToInt32(userModel);
                            }
                            OPAProjectActionViewModel.userId = userID;
                        }

                    }
                }
                var success = this.UploadExternalProducts(OPAProjectActionViewModel, Convert.ToInt32(FolderKey));

                if (success)
                {


                }
                else
                {
                    ViewBag.UploadFailed = "True";
                    TempData["UploadFailed"] = "True";

                }
                if (pageTypeId == 18)
                {
                    return View("~/Views/Production/Amendments/Index.cshtml", OPAProjectActionViewModel);
                }
                else
                {
                    return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", OPAProjectActionViewModel);
                }
            }
            catch (Exception e)
            {
                //todo:skumar: redirect to error page
                return RedirectToAction("Login", "Account");
            }
        }

        //#664
        [ValidateInput(false)]
        public ActionResult RISReload()
        {
            OPAViewModel AppProcessViewModel = new OPAViewModel();
            AppProcessViewModel.pageTypeId = 3;

            var GroupTaskInstanceId = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;

            AppProcessViewModel = projectActionFormManager.GetOPAByTaskInstanceId(GroupTaskInstanceId);
            AppProcessViewModel.RequestStatus = (int)RequestStatus.RequestInfoFromLender;
            AppProcessViewModel.TaskStepId = 22;
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);
            foreach (var model in projectActionList)
            {
                AppProcessViewModel.ProjectActionTypeList.Add(new SelectListItem()
                {
                    Text = model.ProjectActionName,
                    Value = model.ProjectActionID.ToString()
                });
            }
            //projectActionList = (from q in  ProjectInfoModel);

            //var result = projectActionFormManager.GetProdPropertyInfo(AppProcessViewModel.FhaNumber);
            var result = projectActionFormManager.GetPropertyInfo(AppProcessViewModel.FhaNumber);
            if (result != null)
            {
                AppProcessViewModel.PropertyId = result.PropertyId;
                AppProcessViewModel.PropertyAddress.AddressLine1 = result.StreetAddress;
                AppProcessViewModel.PropertyAddress.StateCode = result.State;
                AppProcessViewModel.PropertyAddress.City = result.City;
                AppProcessViewModel.PropertyAddress.ZIP = result.Zipcode;
            }
            AppProcessViewModel.GroupTaskInstanceId = GroupTaskInstanceId;
            //  AppProcessViewModel.ProjectActionTypeId;
            AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(AppProcessViewModel.ProjectActionTypeId);

            var grouptaskmodel = prod_GroupTasksManager.GetGroupTaskAByTaskInstanceId(GroupTaskInstanceId);
            if (grouptaskmodel != null)
            {
                AppProcessViewModel.ServicerComments = grouptaskmodel.ServicerComments;
                AppProcessViewModel.pageTypeId = (int)PageType.OPA;
            }
            GetDisclaimerText(AppProcessViewModel);
            //AppProcessViewModel.pageTypeId = (int)PageType.ProductionApplication; 
            if (TempData["UploadFailed"] != null)
            {
                ViewBag.UploadFailed = TempData["UploadFailed"].ToString();
            }



            //else if (AppProcessViewModel.pageTypeId == (int)PageType.Amendments)
            //{
            //    ViewBag.ConstructionTitle = "Amendments";
            //    return View("~/Views/Production/Amendments/Index.cshtml", AppProcessViewModel);
            //}

            // PopulateFHANumberList(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            PopulateFHANumberList(AppProcessViewModel);
            GetPropertyInfo(AppProcessViewModel.FhaNumber);
            return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", AppProcessViewModel);
            //}
        }

        // harish added new method to get upload files
        public JsonResult RISGetOPAWorkInternalProduct(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //var taskInstanceId1 = new Guid("1C0F081F-4515-4C18-AC64-BDB5AC59B023");
            //if (fileType == null)
            //    fileType = iOPAForm.UpdateOPAGroupTask1(fileType);
            //var opaWorkProduct1=    GetOpaHistory(taskInstanceId);
            // var taskInstanceId1 = new Guid("F2BD7D89-AF05-424A-8375-8F31C0E25B7C");
            var opaWorkProduct = iOPAForm.RISgetAllOPAWorkInternalProducts(taskInstanceId);

            //Setting the date based on the Time Zone
            if (opaWorkProduct != null)
            {
                foreach (var item in opaWorkProduct)
                {
                    //item.UploadTime = TimezoneManager.ToMyTimeFromUtc(item.UploadTime);
                    item.UploadTime = item.UploadTime;

                }
            }
            if (opaWorkProduct != null)
            {
                int totalrecods = opaWorkProduct.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                var result = opaWorkProduct.ToList();
                var results = result.Skip(pageIndex * pageSize).Take(pageSize);

                var jsonData = new
                {
                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = results,
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = opaWorkProduct,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        // harish added new method to show AE uploaded grid
        //Added by siddu 18122019//
        public JsonResult GetProdLenderUploadFileGridContentExternalProductsToLender(string sidx, string sord, int page, int rows, Guid? taskInstanceId, string PropertyId, string FhaNumber, string FormName)
        {

            if (taskInstanceId == null)
                taskInstanceId = ((OPAViewModel)TempData.Peek("OPAFormData")).TaskInstanceId;
            var AppProcessViewModel = new OPAViewModel();
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            AppProcessViewModel.OpaHistoryViewModel = GetOpaHistory((Guid)taskInstanceId);
            var opahist = new OPAHistoryViewModel();
            if (AppProcessViewModel.OpaHistoryViewModel != null)
            {
                foreach (var item in AppProcessViewModel.OpaHistoryViewModel)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    opahist.actionTaken = item.actionTaken;
                    opahist.userRole = item.userRole;

                }

            }

            var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId.Value, PropertyId, FhaNumber);
            //foreach(var item in GridContent)
            //{
            //    if (item.id.Contains("_"))
            //        item.id = item.id.Split('_')[0];


            //}
            GridContent = GridContent.Where(x => x.id.Split('_')[0] != "32" && x.id.Split('_')[0] != "39" && x.id.Split('_')[0] != "41").ToList();

            if (GridContent != null)
            {
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    //if (!string.IsNullOrEmpty(item.id))
                    //    idname = new String[] { };
                    //    idname = item.id.Split('_');
                    //if (item.id.Contains("_"))
                    //    item.id = item.id.Split('_')[0];
                    if (item.fileId != null)
                    {
                        var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
                        if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                        || currentUserRole == "BackupAccountManager")
                            item.isUploadedByMe = true;
                        //Added by siddu 19122019//
                        item.role = UserPrincipal.Current.UserRole;
                        //item.role = opahist.userRole;
                        item.name = UserPrincipal.Current.FullName;
                        item.actionTaken = opahist.actionTaken;
                        //item.actionTaken = griditem.actionTaken;
                    }
                    else
                    {
                        if (item.SubfolderSequence != null)
                        {
                            item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                        }
                    }
                }
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var result = GridContent.ToList();//.OrderBy(a => a.id);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        // pending RAI
        public JsonResult GetProdPendingRAI(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            // List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();

            var GridContent = appProcessManager.GetOPAPendingRAI(taskInstanceId);

            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.CreatedOn = TimezoneManager.ToMyTimeFromUtc(item.CreatedOn);
                    item.CreatedOn = item.CreatedOn;

                }

            }
            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);



            //var results = GridContent.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = GridContent,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        // harihs added new method to get review statis
        public JsonResult GetOPAReviewersTaskStatus(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            // List<OPAHistoryViewModel> GridContent = new List<OPAHistoryViewModel>();

            var GridContent = appProcessManager.GetProdReviewersTaskStatus(taskInstanceId);

            if (GridContent != null)
            {
                foreach (var item in GridContent)
                {
                    //item.CompletedOn = TimezoneManager.ToMyTimeFromUtc(item.CompletedOn);
                    //item.CreatedOn = TimezoneManager.ToMyTimeFromUtc(item.CreatedOn);
                    item.CompletedOn = item.CompletedOn;
                    item.CreatedOn = item.CreatedOn;
                }

            }

            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);



            //var results = GridContent.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = GridContent,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }

        // harish added new method to get new file request files
        //#664
        public JsonResult GetRequestAdditionalGridForLenderForNewFileRequest(string sidx, string sord, int page, int rows, int Projectactiontypeid,
             Guid taskInstanceId)
        {
            var AppProcessViewModel = new OPAViewModel();
            var requestAddtionalTask = new List<OPAHistoryViewModel>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            //var parentchildtaskmodel = new ParentChildTaskModel();
            var childtaskinstanceid = ((OPAViewModel)TempData.Peek("OPAFormData")).ChildTaskInstanceId;
            // parentchildtaskmodel = parentChildTaskManager.GetChildTaskbytaskinstanceid(taskInstanceId);
            requestAddtionalTask = GetOpaHistory(childtaskinstanceid);
            if (requestAddtionalTask.Count > 0)
            {
                // harish added for to add file names
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                foreach (var item in requestAddtionalTask)
                {
                    if (item.fileReviewStatusId != 5 || item.fileReviewStatusId != 4)
                    {
                        //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                        //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                        item.uploadDate = item.uploadDate;
                        item.submitDate = item.submitDate;


                        //item.level = 0;
                        if (item.fileId != null)
                        {
                            var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
                            if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                            || currentUserRole == "BackupAccountManager")

                                item.userRole = UserPrincipal.Current.UserRole;
                            item.name = UserPrincipal.Current.FullName;
                            if (item.DocTypeID == null)
                            {

                                //if ((item.actionTaken == "File Upload ( New Unread )" && item.userRole == "AE" && item.fileId != null) || (item.actionTaken == "File Upload ( New Unread )" && (item.userRole == "LenderAccountRepresentative" || item.userRole == "LAR") && item.fileId != null) || (item.fileReviewStatusId == 1 && (item.userRole == "LenderAccountManager" || item.userRole == "LAM") && item.fileId != null))
                                //{
                                var DocTypeList = appProcessManager.GetAm_Assetmanagement(Convert.ToInt32(Projectactiontypeid));
                                if (DocTypeList.Count() == 0)
                                {
                                    AppProcessViewModel.ProjectActionName = appProcessManager.GetProjectActionTypebyID(Projectactiontypeid);
                                    item.fileName = AppProcessViewModel.ProjectActionName;
                                    // harish added hardcoded this pcna upload value 06042020
                                    if (item.fileName == "PCNA Upload")
                                    {
                                        item.DocTypeID = "7000937";
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            string ext = Path.GetExtension(taskfile.FileName);
                                            taskfile.FileName = item.fileName + ext;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        }
                                    }
                                    else if (item.fileName == "Other")
                                    {
                                        item.DocTypeID = "2082";
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            string ext = Path.GetExtension(taskfile.FileName);
                                            taskfile.FileName = item.fileName + ext;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        }
                                    }
                                    // harish adding other 5 transaction types due to corona these need to be added 27042020
                                    //COVID-19/R4R Suspensions
                                    else if (item.fileName == "COVID-19/R4R Suspensions")
                                    {
                                        item.DocTypeID = "2082";
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            string ext = Path.GetExtension(taskfile.FileName);
                                            taskfile.FileName = item.fileName + ext;
                                           
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        }
                                    }
                                    //COVID-19/R4R Releases for Mortgage Payments
                                    else if (item.fileName == "COVID-19/R4R Releases for Mortgage Payments")
                                    {
                                        item.DocTypeID = "2082";
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            string ext = Path.GetExtension(taskfile.FileName);
                                            taskfile.FileName = item.fileName + ext;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        }
                                    }
                                    //COVID-19/R4R Other Releases
                                    else if (item.fileName == "COVID-19/R4R Other Releases")
                                    {
                                        item.DocTypeID = "2082";
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            string ext = Path.GetExtension(taskfile.FileName);
                                            taskfile.FileName = item.fileName + ext;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        }
                                    }
                                    //COVID-19/Escrow Releases to Pay Mortgage
                                    else if (item.fileName == "COVID-19/Escrow Releases to Pay Mortgage")
                                    {
                                        item.DocTypeID = "2082";
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            string ext = Path.GetExtension(taskfile.FileName);
                                            taskfile.FileName = item.fileName + ext;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        }
                                    }
                                    //COVID-19/Other Relief
                                    else if (item.fileName == "COVID-19/Other Relief")
                                    {
                                        item.DocTypeID = "2082";
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            string ext = Path.GetExtension(taskfile.FileName);
                                            taskfile.FileName = item.fileName + ext;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                        }
                                    }


                                }


                                //harish added 14042020
                                if (item.DocTypeID == "2082" && (item.fileName.ToUpper().StartsWith("OTHERS_") || item.fileName.ToUpper().StartsWith("OTHER_")))
                                {
                                    var matched = item.fileName.Substring(7, item.fileName.Length - 7);
                                    // var matched = item.fileName.Split('_').Last();
                                    // item.fileName = matched;
                                    var matchedinfo = DocTypeList.Where(x => matched.Contains(x.shortname_id));
                                    if (matchedinfo.Any())
                                    {
                                        item.DocTypeID = matchedinfo.First().doc_type_id.ToString();
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {

                                            item.fileName = taskfile.FileName;
                                            taskfile.DocTypeID = item.DocTypeID;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);

                                            // taskManager.UpdateFileNameInTaskFile(taskfile);
                                        }
                                    }
                                    else
                                    {
                                        var matchedfile = item.fileName.Substring(7, item.fileName.Length - 7);

                                        var matchedinfodata = DocTypeList.Where(x => matchedfile.Contains(x.shortname_id));
                                        if (matchedinfodata.Any())
                                        {
                                            item.DocTypeID = matchedinfodata.First().doc_type_id.ToString();
                                            var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                            if (taskfile != null)
                                            {

                                                item.fileName = taskfile.FileName;
                                                taskfile.DocTypeID = item.DocTypeID;
                                                taskManager.UpdateDoctyidinTaskfile(taskfile);

                                                // taskManager.UpdateFileNameInTaskFile(taskfile);
                                            }
                                        }
                                    }

                                }




                                //Extension changes 
                                //if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                                //{
                                //    item.fileName = Path.GetFileNameWithoutExtension(item.fileName);
                                //    //harish new line of code for checking file name and document(shortnameid) 07-01-2020
                                //    //if (DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).Any())
                                //    if (DocTypeList.Where(x => x.shortname_id.Contains(item.fileName)).Any())
                                //    {
                                //        //item.fileName = item.fileName;
                                //        // harish added to save 03042020
                                //        //var matchedinfo = DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).First();
                                //        var matchedinfo = DocTypeList.Where(x => x.shortname_id.Contains(item.fileName)).First();
                                //        item.DocTypeID = matchedinfo.doc_type_id.ToString();
                                //        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                //        if (taskfile != null)
                                //        {
                                //            taskfile.DocTypeID = item.DocTypeID;
                                //            string ext = Path.GetExtension(taskfile.FileName);
                                //            taskfile.FileName = item.fileName + ext;
                                //            taskManager.UpdateDoctyidinTaskfile(taskfile);
                                //        }

                                //    }
                                //    else
                                //    {
                                //        var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.shortname_id.Split('_').First()));
                                //        if (matched.Any())
                                //        {
                                //            // item.fileName = matched.First().shortname_id;
                                //            // harish added 06042020 to save doctyid in taskfile table
                                //            item.DocTypeID = matched.First().doc_type_id.ToString();
                                //            var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                //            if (taskfile != null)
                                //            {
                                //                taskfile.DocTypeID = item.DocTypeID;
                                //                //taskfile.FileName = item.fileName;
                                //                //Getting file extension name from task file table and adding extension to the filename@28042020
                                //                 string ext = Path.GetExtension(taskfile.FileName);
                                //                taskfile.FileName = item.fileName + ext;
                                //                item.fileName = taskfile.FileName;
                                //                taskManager.UpdateDoctyidinTaskfile(taskfile);
                                //            }
                                //        }
                                //        else
                                //        {
                                //            item.DocTypesList = GetAssetManagementDocTypesdropdownlist(DocTypeList, item.fileId);
                                //        }
                                //    }

                                //}


                                if (DocTypeList != null && DocTypeList.Count() > 0 && item.DocTypeID == null)
                                {

                                    //harish new line of code for checking file name and document(shortnameid) 07-01-2020
                                    if (DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).Any())
                                    {
                                        item.fileName = item.fileName;
                                        // harish added to save 03042020
                                        var matchedinfo = DocTypeList.Where(x => item.fileName.Contains(x.shortname_id)).First();
                                        item.DocTypeID = matchedinfo.doc_type_id.ToString();
                                        var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                        if (taskfile != null)
                                        {
                                            taskfile.DocTypeID = item.DocTypeID;
                                            taskfile.FileName = item.fileName;
                                            taskManager.UpdateDoctyidinTaskfile(taskfile);
                                        }

                                    }
                                    else
                                    {
                                        var matched = DocTypeList.Where(x => item.fileName.Split('_').First().Contains(x.shortname_id.Split('_').First()));
                                        if (matched.Any())
                                        {
                                            // item.fileName = matched.First().shortname_id;
                                            // harish added 06042020 to save doctyid in taskfile table
                                            item.DocTypeID = matched.First().doc_type_id.ToString();
                                            var taskfile = taskManager.GetTaskFileById((int)item.fileId);
                                            if (taskfile != null)
                                            {
                                                taskfile.DocTypeID = item.DocTypeID;
                                                //taskfile.FileName = item.fileName;
                                                //Getting file extension name from task file table and adding extension to the filename@28042020
                                                // string ext = Path.GetExtension(taskfile.FileName);
                                                taskfile.FileName = item.fileName;
                                                item.fileName = taskfile.FileName;
                                                taskManager.UpdateDoctyidinTaskfile(taskfile);
                                            }
                                        }
                                        else
                                        {
                                            item.DocTypesList = GetAssetManagementDocTypesdropdownlist(DocTypeList, item.fileId);
                                        }
                                    }

                                }


                            }


                        }


                        else
                        {

                        }
                    }
                    else if (item.fileReviewStatusId == 5)
                    {
                        item.userRole = "AE";
                        item.name = item.name;
                    }
                    else
                    {
                        item.userRole = "AE";
                        item.name = item.name;
                    }
                }


                int totalrecods = requestAddtionalTask.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                if (sord.ToUpper() == "DESC")
                {

                    requestAddtionalTask = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList();

                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    requestAddtionalTask = requestAddtionalTask.OrderBy(s => s.submitDate).ToList();
                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {

                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = requestAddtionalTask,
                };




                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {



                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = requestAddtionalTask,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }
        // harish added new method for RAI 11-02-2020
        public JsonResult RAIGetRequestAdditionalGridForLender123(string sidx, string sord, int page, int rows,
         Guid taskInstanceId)
        {
            var requestAddtionalTask = new List<OPAHistoryViewModel>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var parentchildtaskmodel = new ParentChildTaskModel();
            parentchildtaskmodel = parentChildTaskManager.GetChildTaskbytaskinstanceid(taskInstanceId);
            requestAddtionalTask = GetOpaHistory(parentchildtaskmodel.ChildTaskInstanceId);
            requestAddtionalTask = requestAddtionalTask.Where(x => x.actionTaken != "Request Additional Submit").ToList();

            //requestAddtionalTask = GetOpaHistory(taskInstanceId);
            //requestAddtionalTask = requestAddtionalTask.Where(x => x.fileReviewStatusId != 1).ToList();


            if (requestAddtionalTask.Count > 0)
            {

                int totalrecods = requestAddtionalTask.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
                if (sord.ToUpper() == "DESC")
                {

                    requestAddtionalTask = requestAddtionalTask.OrderByDescending(s => s.submitDate).ToList();

                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    requestAddtionalTask = requestAddtionalTask.OrderBy(s => s.submitDate).ToList();
                    requestAddtionalTask = requestAddtionalTask.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {

                    total = totalpages,
                    page,
                    records = totalrecods,
                    rows = requestAddtionalTask,
                };




                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            else
            {



                var jsonData = new
                {
                    total = 1,
                    page = 1,
                    records = 1,
                    rows = requestAddtionalTask,

                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        // harish adding new method to acccess the files has readonly for AssetManagement Grid for both Lender and Ae
        public JsonResult GetProdLenderUploadFileGridContentOPAReadOnlyGrid(string sidx, string sord, int page, int rows, int Projectactiontypeid, Guid? taskInstanceId, string PropertyId, string FhaNumber, string FormName)
        {
            var AppProcessViewModel = new OPAViewModel();
            if (taskInstanceId == null)
            {
                taskInstanceId = Guid.Empty;
            }

            AppProcessViewModel.OpaHistoryViewModel = GetOpaHistory((Guid)taskInstanceId);


            var opahist = new OPAHistoryViewModel();
            if (AppProcessViewModel.OpaHistoryViewModel != null)
            {
                foreach (var item in AppProcessViewModel.OpaHistoryViewModel)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;
                    opahist.actionTaken = item.actionTaken;
                    opahist.userRole = item.userRole;

                }

            }

            var GridContent = projectActionFormManager.GetProdLenderUploadForAmendments(taskInstanceId.Value, PropertyId, FhaNumber);
            GridContent = GridContent.Where(x => x.FolderStructureKey != 40).ToList();
            GridContent = GridContent.Where(x => x.FolderStructureKey != 41).ToList();
            foreach (var prevfile in GridContent)
            {
                if (prevfile.id == null)
                {
                    prevfile.id = "39";
                    prevfile.parent = "39";
                }
            }

            GridContent = GridContent.Where(x => x.id.Split('_')[0] != "32" && x.id.Split('_')[0] != "40" && x.id.Split('_')[0] != "41").ToList();
            GridContent = GridContent.Where(x => x.id.Split('_')[0] == "39").ToList();

            // GridContent = GridContent.Where(x => x.id == "39").ToList();

            GridContent = GridContent.GroupBy(o => new { o.fileId }).Select(o => o.FirstOrDefault()).ToList();

            if (GridContent != null)
            {
                var currentUserId = UserPrincipal.Current.UserId;
                var currentUserRole = UserPrincipal.Current.UserRole;
                foreach (var item in GridContent)
                {
                    //item.uploadDate = TimezoneManager.ToMyTimeFromUtc(item.uploadDate);
                    //item.submitDate = TimezoneManager.ToMyTimeFromUtc(item.submitDate);
                    item.uploadDate = item.uploadDate;
                    item.submitDate = item.submitDate;

                    if (item.fileId != null)
                    {
                        var uploadedBy = (int)taskManager.GetTaskFileById((int)item.fileId).CreatedBy;
                        if (currentUserId == uploadedBy || currentUserRole == "LenderAccountManager"
                                                        || currentUserRole == "BackupAccountManager")
                            item.isUploadedByMe = true;
                        item.role = opahist.userRole;
                        item.name = accountManager.GetFullUserNameById(item.CreatedBy);
                        item.actionTaken = opahist.actionTaken;
                    }


                    else
                    {
                        if (item.SubfolderSequence != null)
                        {
                            item.folderNodeName = item.folderNodeName + "_" + item.SubfolderSequence;
                        }
                    }
                }
            }

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalrecods = GridContent.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var result = GridContent.ToList();//.OrderBy(a => a.id);
            var results = result.Skip(pageIndex * pageSize).Take(pageSize);
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results.OrderBy(x => x.uploadDate).ToList(),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


    }
}
