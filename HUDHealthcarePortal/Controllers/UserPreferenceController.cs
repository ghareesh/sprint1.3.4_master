﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessService;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using Model;

namespace HUDHealthcarePortal.Controllers
{
    public class UserPreferenceController : Controller
    {
        //
        // GET: /UserPreference/
        private IPDRUserPreferenceManager _pdrUserPreferenceManager;

        public UserPreferenceController() : this(new PDRUserPreferenceManager())
        {
            
        }

        public UserPreferenceController(IPDRUserPreferenceManager pdrUserPreferenceManager)
        {
            _pdrUserPreferenceManager = pdrUserPreferenceManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PdrUserPreference()
        {

            var allColumns = _pdrUserPreferenceManager.GetAllColumns();
            var userPreference = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);
            var userPreferenceColumns = new List<PDRColumnViewModel>();
            foreach (var column in allColumns)
            {
                bool columnVisibility = true;
               
                    var userPrefForCol = userPreference.FirstOrDefault(m => m.PDRColumnID == column.ColumnID);
                    if (userPrefForCol != null)
                    {
                        columnVisibility = userPrefForCol.Visibility;
                    }
               
                userPreferenceColumns.Add(new PDRColumnViewModel()
                {
                    ColumnID = column.ColumnID,
                    ColumnName = column.ColumnName,
                    Visibility = columnVisibility
                        
                });
            }


            return View(userPreferenceColumns.ToList());

        }
        [HttpPost]
        public void UpdateUserPreference(List<PDRColumnViewModel> PDRColumnViewModels)
        {
            int userID = UserPrincipal.Current.UserId;
            var userPreference = _pdrUserPreferenceManager.GetUserPreference(userID);
            if (userPreference.Count() > 0)
            {
                var PDRColumnIDs = userPreference.Select(m => m.PDRColumnID).Distinct();
                foreach (var model in PDRColumnViewModels.Where(m => PDRColumnIDs.Contains(m.ColumnID)))
                {
                    if (model.Visibility)
                    {
                        _pdrUserPreferenceManager.DeletePDRUserPreference(userID,model.ColumnID);
                    }
                       
                   /* else
                    {
                        var modelToUpdate = new PDRUserPreferenceViewModel()
                        {
                            PDRColumnID = model.ColumnID,
                            UserID = userID,
                            Visibility = model.Visibility
                        };
                        _pdrUserPreferenceManager.UpdatePDRUserPreference(modelToUpdate);
                    }*/
                }
                AddNewUserPreference(PDRColumnViewModels.Where(m =>!PDRColumnIDs.Contains(m.ColumnID)).ToList());
            }
            else
            {
                AddNewUserPreference(PDRColumnViewModels);

            }

                     
            
        }
         [HttpPost]
        public void SetDefaultUserPreference()
        {
            int userID = UserPrincipal.Current.UserId;
            _pdrUserPreferenceManager.SetDefaultUserPreference(userID);

             //adding default columns
           

            
        }

        public JsonResult GetPDRUserPrefrence()
        {
            var userPreference = _pdrUserPreferenceManager.GetUserPreference(UserPrincipal.Current.UserId);

            return Json(userPreference,JsonRequestBehavior.AllowGet);
        }
        private void AddNewUserPreference(List<PDRColumnViewModel> PDRColumnViewModels)
        {
            int userID = UserPrincipal.Current.UserId;
            foreach (var pdrColumnViewModel in PDRColumnViewModels.Where(m => !m.Visibility))
            {
                var model = new PDRUserPreferenceViewModel()
                {
                    PDRColumnID = pdrColumnViewModel.ColumnID,
                    UserID = userID,
                    Visibility = pdrColumnViewModel.Visibility

                };
                _pdrUserPreferenceManager.AddPDRUserPreference(model);

            }
        }
    }
}
