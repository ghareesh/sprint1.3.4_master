﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HUDHealthcarePortal.Controllers.HelpDesk
{
    public class GetDemoController : ApiController
    {
        // GET api/default2        
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new string[] { "Hello", "World" });
        }

    }
}
