﻿using System;
using System.Web.Mvc;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Core;
using HelpDeskController.Repository;
using System.Collections.Generic;
using HUDHealthcarePortal.BusinessService;
using System.IO;
using System.Web;
using HUDHealthCarePortal.Filters;
using HUDHealthcarePortal.Filters;
using BusinessService.Interfaces;
using System.Collections;
using Repository.Interfaces;
using System.Data;
using System.Text;
using System.Configuration;
using EntityObject.Entities.HCP_live;
using System.Threading.Tasks;
using BusinessService.AssetManagement;
using BusinessService.ProjectAction;
using System.Linq;
using HUDHealthcarePortal.Helpers;
using SpreadsheetGear;
using EntityObject.Entities.HCP_live.Programmability;


//karri:#149,#214
//https://www.c-sharpcorner.com/article/upload-files-in-asp-net-mvc-5/
//https://docs.microsoft.com/en-us/aspnet/core/razor-pages/upload-files?view=aspnetcore-2.2
//https://www.c-sharpcorner.com/article/asp-net-mvc-form-with-file-upload/

namespace HelpDeskController
{
    public class HelpDeskController : Controller
    {
        // private HUDHealthcarePortal.Repository.UnitOfWork _unitOfWork;
        //private IAdminManagerUsersRepository _adminManagerUsersRepository;
        ArrayList _sameGroupUsers = new ArrayList();
        IBackgroundJobMgr _backgroundManager = null;
        private IReserveForReplacementManager reserveForReplacementManager;
        private IProjectActionFormManager projectActionFormManager;
        private IUploadDataManager uploadDataManager;
        IEmailManager _emailManager = null;
        IAccountManager _accountManager = null;
        //controller constructor is called every time a action is initiated
        HDTicketManager _hdtktmngr = null;
        HDTicketLoggerMngr _hdtktnotifmngr = null;//HDNOTIFICAITONS

        Hashtable _htLoggedUserRole = null;
        int _userid;
        //ArrayList _hudHelpDeskUsers = new ArrayList();
        List<SelectListItem> _hdAdminUsersList = new List<SelectListItem>();
        //written by siddesh 28082019   #344//
        IUploadDataManager _uploadDataMgr = null;

        public HelpDeskController()
        {
            //_unitOfWork = new HUDHealthcarePortal.Repository.UnitOfWork(DBSource.Live);
            //_adminManagerUsersRepository = new HUDHealthcarePortal.Repository.AdminManagerUsersRepository(_unitOfWork);
            //NOTE:we cannot use Server.MapPath("~/UploadedHelpDeskTktFiles/") since we are still in Constructor itself 
            _hdtktmngr = new HDTicketManager(ConfigurationManager.AppSettings["HUDHelpDeskMetaFile"].ToString());
            _hdtktnotifmngr = new HDTicketLoggerMngr();//HDNOTIFICAITONS

            _userid = UserPrincipal.Current.UserId;
            uploadDataManager = new UploadDataManager();
            projectActionFormManager = new ProjectActionFormManager();

            _accountManager = new AccountManager();
            _backgroundManager = new BackgroundJobMgr();
            _emailManager = new EmailManager();
            reserveForReplacementManager = new ReserveForReplacementManager();
            //written by siddesh 28082019   #344//
            _uploadDataMgr = new UploadDataManager();
            _htLoggedUserRole = new Hashtable();
            _htLoggedUserRole["TechDesk"] = false;
            _htLoggedUserRole["HUDAdmin"] = false;
            _htLoggedUserRole["OtherUser"] = false;

            //setting of the role from the logged user
            foreach (var role in UserPrincipal.Current.Roles)
            {
                //will be setting only for Admin and HD user
                if (_htLoggedUserRole.ContainsKey(role))
                {
                    _htLoggedUserRole[role] = true;
                }
            }
            //if all the values are false, then make true for OtherUser key
            bool isAKeyFound = false;
            foreach (DictionaryEntry entry in _htLoggedUserRole)
            {
                if (Convert.ToBoolean(entry.Value))
                {
                    isAKeyFound = true;
                    break;
                }
            }
            if (!isAKeyFound)
                _htLoggedUserRole["OtherUser"] = true;

            //string 1username = _accountManager.GetFullUserNameById(3318);
        }

        //doing nothing
        public ActionResult Index()
        {
            return View();
        }

        //opens create ticket form for "other than" "HDADMIN, HelpDesk" users only
        //CreateTicket.cshtml
        //SuperUser,Administrator,HUDAdmin,HUDDirector,WorkflowManager,LenderAdmin,LenderAccountManager,BackupAccountManager,AccountExecutive,LenderAccountRepresentative,InternalSpecialOptionUser,HUDAdmin,HelpDesk
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "HUDAdmin,HUDDirector,LenderAdmin,LenderAccountManager,BackupAccountManager,LenderAccountRepresentative,AccountExecutive,WorkflowManager,Attorney,InternalSpecialOptionUser,ProductionUser,ProductionWlm,Reviewer")]
        public ActionResult CreateTicket()
        {
            //while creating a new ticket filling default values
            var hdt = new HD_TICKET_ViewModel();
            PopulatehelpdeskFHANumberList(hdt);
            DataTable dtStatus = _hdtktmngr.getStatusTbl();
            DataTable dtCategory = _hdtktmngr.getCateogryTbl();
            DataTable dtSubCategory = _hdtktmngr.getSubCategoryTbl();
            DataTable dtOnAction = _hdtktmngr.getOnActionTbl();

            foreach (DataRow dr in dtStatus.Rows)
                hdt.TicketStatusList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtCategory.Rows)
                hdt.TicketCategoryList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtSubCategory.Rows)
                hdt.TicketSubCategoryList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtOnAction.Rows)
                hdt.TicketOnActionList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });


            //karrinew
            //display the created users firstname,last name, (user email)

            //these values do not persist as constructor is initiated afresh for every action,hence better in
            //submit function
            //hdt.LAST_MODIFIED_ON = DateTime.Now;
            hdt.LAST_MODIFIED_ON = DateTime.UtcNow;
            hdt.CREATED_BY_USERDISPLAYNAME = UserPrincipal.Current.FullName + "(" + UserPrincipal.Current.UserName + ")";//EMAIL ID
            hdt.CREATED_BY = UserPrincipal.Current.UserId;//userid in number
                                                          //hdt.CREATED_ON = DateTime.Now;
            hdt.CREATED_ON = DateTime.UtcNow;

            //we first create a folder on UserID_RandomNumber, fill in files into it, later Rename folder afer creating ticket
            Random randoms = new Random();
            var UniqueId = randoms.Next(0, 99999);

            //temperory folder will be userid\tempnumber in the folder "UploadedHelpDeskTktFiles"
            string newPath1 = UserPrincipal.Current.UserId + "_" + UniqueId;
            string newPath2 = Server.MapPath("~/UploadedHelpDeskTktFiles/" + newPath1);
            hdt.HELPDESK_UPLOADS_PATH = newPath2;

            hdt.TEMPERORY_FILE_UNIQUEPATH = UniqueId;

            return View(hdt);

        }

        //submission/saving data from CreateTicket.cshtml
        [HttpPost]
        public ActionResult SubmitForm(HD_TICKET_ViewModel hdt)
        {
            //Implemented by siddesh 01102019 Start//
            if (hdt.TICKET_ID > 0)
            {
                var hdticketsGridData = _hdtktmngr.GetMyTickets(0, hdt.TICKET_ID);
                if (hdticketsGridData.First().CONVERSATION != null && hdticketsGridData.First().CONVERSATION != "")
                    hdt.CONVERSATION = hdticketsGridData.First().CONVERSATION;

            }

            //Implemented by siddesh 01102019 End//
            string s2newPath = string.Empty;
            string newpath1 = string.Empty;
            int filescount = 0;
            int ticketid = 0;
            bool isUPDATE = false;

            bool isAdminUser = false;//Note: there is no HUDAdmin functionality, HUDAdmin now will become as HelpDesk1 role
            bool isHelpDesk1 = false;
            bool isOhterUser = false;
            bool isHelpDesk2 = false;
            bool isOnlyCmntsSubmitted = false;//karri-hareesh-13092019//to send mail if only comments are submitted

            int loggedUserID = 0;//userid = 0;
            IdentifyUserRole(ref isAdminUser, ref isHelpDesk1, ref isOhterUser, ref isHelpDesk2, UserPrincipal.Current.UserId);
            //if (isAdminUser || isHelpDesk1 || isHelpDesk2)
            if (isHelpDesk1 || isHelpDesk2)
                loggedUserID = 0;
            else
                loggedUserID = UserPrincipal.Current.UserId;

            if (hdt != null && ModelState.IsValid)
            {
                try
                {
                    //Note: Identify if NEW-CREATE or UPDATE
                    //if (isHelpDesk1 || isHelpDesk2 || isAdminUser)
                    //if (isHelpDesk1 || isHelpDesk2 || (isOhterUser && hdt.TICKET_ID > 0))
                    if ((isHelpDesk1 && hdt.TICKET_ID > 0) || (isHelpDesk2 && hdt.TICKET_ID > 0) || (isOhterUser && hdt.TICKET_ID > 0))
                        isUPDATE = true;

                    //hepful for UPDATE only
                    bool isStatusChanged = false;
                    bool isAssigned = false;
                    bool isReassigned = false;
                    bool isPriorityChanged = false;
                    bool isDataChanged = false;

                    #region CREATE
                    if (!isUPDATE)//if create
                    {
                        //Note: donno why it is not populating from ui
                        hdt.STATUS = 0;

                        //Note:ignore for updats, use these only for Creates
                        hdt.CREATED_BY_USERNAME = UserPrincipal.Current.FullName;
                        hdt.CREATED_BY = UserPrincipal.Current.UserId;
                        //hdt.CREATED_ON = DateTime.Now;
                        hdt.CREATED_ON = DateTime.UtcNow;
                        //hdt.LAST_MODIFIED_ON = DateTime.Now;
                        hdt.LAST_MODIFIED_ON = DateTime.UtcNow;
                        hdt.ROLENAME = UserPrincipal.Current.UserRole;

                        //savin data into DB and get ticketid return

                        //29082019//30082019

                        string S = string.Empty;
                        if (RoleManager.IsInternalUser(UserPrincipal.Current.UserName))
                        {
                            S = "HUD Help Desk";
                        }
                        else
                        {
                            S = UserPrincipal.Current.FullName + "> (Submitter Comments)";
                        }
                        //User Comments here//dataInModel.USERCOMMENTS;
                        if (hdt.USERCOMMENTS != null && !string.IsNullOrEmpty(hdt.USERCOMMENTS))
                        {
                            StringBuilder sbCreateComments = new StringBuilder();
                            //sbCreateComments.Append("\n" + DateTime.Now.ToShortDateString() + ":" + DateTime.Now.ToShortTimeString());
                            sbCreateComments.Append("\n" + DateTime.UtcNow.ToShortDateString() + ":" + DateTime.UtcNow.ToShortTimeString());
                            sbCreateComments.Append("\n" + S + ":" + hdt.USERCOMMENTS + ".");
                            hdt.USERCOMMENTS = sbCreateComments.ToString();
                        }


                        int ticketID = _hdtktmngr.saveTicket(hdt);
                        ticketid = ticketID;

                        //files uploaindg temperory path
                        newpath1 = UserPrincipal.Current.UserId + "_" + hdt.TEMPERORY_FILE_UNIQUEPATH;
                        //hdt.TEMPERORY_FILE_UNIQUEPATH = null;

                        s2newPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + newpath1);

                        //already created  a new directory with ticketid as folder name during file uploads
                        string s3newPath = ticketID.ToString();
                        s3newPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + s3newPath);

                        //Note: if files are uploaded than folder exists for files, check the folder now
                        //rename the files by removing the prefix like File1_xxxxxx
                        if (Directory.Exists(s2newPath))
                        {
                            var files = Directory.GetFiles(s2newPath);
                            filescount = files.Length;

                            if (filescount > 0)
                            {
                                foreach (var iFile in files)
                                {
                                    //get the file name only
                                    string[] name = iFile.ToString().ToUpper().Split(new Char[] { '\\' });
                                    string fName = name[name.Length - 1];

                                    int i = fName.IndexOf('_');
                                    fName = fName.Substring(i + 1);

                                    string newLocFilePath = s2newPath + @"\" + fName;

                                    System.IO.File.Move(iFile, newLocFilePath);
                                }

                                //moing files to new ticket folder and delete the old
                                Directory.Move(s2newPath, s3newPath);


                                //display alert message with ticketid
                                ViewBag.Message = String.Format("{0}.\\n current Date and time:{1}", "Success! Your Help Desk Ticketid is :", ticketID);
                            }
                            else //if directory exists but no files exists? when we upload file and then again delete the file..than folder is left
                            {
                                Directory.Delete(s2newPath);
                            }
                        }

                        //FILLING data for mailing process for create
                        hdt.MAILNOTIFICATION_NUMBEROFFILESATTACHED = filescount.ToString();
                        hdt.TICKET_ID = ticketid;

                    }//FOR CREATE
                    #endregion

                    #region  UPDATE                    
                    else if (isUPDATE)
                    {
                        StringBuilder sbComment = new StringBuilder();
                        string S = string.Empty;
                        if (RoleManager.IsInternalUser(UserPrincipal.Current.UserName))
                        {
                            S = "HUD Help Desk";
                        }
                        else
                        {
                            S = UserPrincipal.Current.FullName;
                        }

                        if (isOhterUser)
                        {
                            //StringBuilder sbComment = new StringBuilder();
                            if (hdt.CONVERSATION != null && !string.IsNullOrEmpty(hdt.CONVERSATION) && hdt.CONVERSATION.Length > 5)
                            {
                                sbComment.Append(hdt.CONVERSATION);
                            }

                            if (hdt.USERCOMMENTS != null && !string.IsNullOrEmpty(hdt.USERCOMMENTS))
                            {
                                isDataChanged = true;
                                //sbComment.Append("\n" + DateTime.Now.ToShortDateString() + ":" + DateTime.Now.ToShortTimeString());
                                sbComment.Append("\n" + DateTime.UtcNow.ToShortDateString() + ":" + DateTime.UtcNow.ToShortTimeString());
                                //sbComment.Append("\n" + UserPrincipal.Current.FullName + " (" + UserPrincipal.Current.UserName + ") : COMMENTS: " + hdt.USERCOMMENTS + ".");
                                //29082019


                                sbComment.Append("\n" + S + " :Comments>: " + hdt.USERCOMMENTS + ".");
                            }
                        }

                        else if (isHelpDesk1 || isHelpDesk2)
                        {
                            //status
                            if (hdt.PREVIOUS_STATUS != hdt.STATUS)
                                isStatusChanged = true;
                            if (hdt.PREVIOUS_ASSIGNED_TO == null && hdt.ASSIGNED_TO != null)
                                isAssigned = true;
                            if (hdt.PREVIOUS_ASSIGNED_TO != null && hdt.ASSIGNED_TO != null && hdt.PREVIOUS_ASSIGNED_TO != hdt.ASSIGNED_TO)
                                isReassigned = true;
                            if (hdt.PREVIOUS_PRIORITY != hdt.PRIORITY)
                                isPriorityChanged = true;

                            //StringBuilder sbComment = new StringBuilder();
                            if (hdt.CONVERSATION != null && !string.IsNullOrEmpty(hdt.CONVERSATION) && hdt.CONVERSATION.Length > 5)
                            {
                                sbComment.Append(hdt.CONVERSATION);
                            }
                            if (isAssigned)
                            {
                                //hdt.ASSIGNED_DATE = DateTime.Now;
                                hdt.ASSIGNED_DATE = DateTime.UtcNow;
                                isDataChanged = true;
                                //sbComment.Append("\n" + DateTime.Now.ToShortDateString() + ":" + DateTime.Now.ToShortTimeString());
                                sbComment.Append("\n" + DateTime.UtcNow.ToShortDateString() + ":" + DateTime.UtcNow.ToShortTimeString());
                                //sbComment.Append("\n" + UserPrincipal.Current.FullName + " (" + UserPrincipal.Current.UserName + ")" + " has assigned this ticket to " + _accountManager.GetFullUserNameById((int)hdt.ASSIGNED_TO) + ".");
                                //29082019
                                //sbComment.Append("\n" + S + " has assigned this ticket to " + _accountManager.GetFullUserNameById((int)hdt.ASSIGNED_TO) + ".");
                                //karri11102019
                                if (isHelpDesk1)
                                    sbComment.Append("\n" + S + ">> has assigned this ticket to " + _accountManager.GetFullUserNameById((int)hdt.ASSIGNED_TO) + ".");
                                if (isHelpDesk2)
                                    sbComment.Append("\n" + S + ">> has assigned this ticket to " + _accountManager.GetFullUserNameById((int)hdt.ASSIGNED_TO) + ".");

                            }
                            if (isReassigned)
                            {
                                isDataChanged = true;
                                //sbComment.Append("\n" + DateTime.Now.ToShortDateString() + ":" + DateTime.Now.ToShortTimeString());
                                sbComment.Append("\n" + DateTime.UtcNow.ToShortDateString() + ":" + DateTime.UtcNow.ToShortTimeString());
                                //29082019
                                //sbComment.Append("\n" + UserPrincipal.Current.FullName + " (" + UserPrincipal.Current.UserName + ")" + " has reassigned this ticket to " + _accountManager.GetFullUserNameById((int)hdt.ASSIGNED_TO) + ".");
                                //sbComment.Append("\n" + S + " has reassigned this ticket to " + _accountManager.GetFullUserNameById((int)hdt.ASSIGNED_TO) + ".");
                                //karri11102019
                                if (isHelpDesk1)
                                    sbComment.Append("\n" + S + ">> has reassigned this ticket to " + _accountManager.GetFullUserNameById((int)hdt.ASSIGNED_TO) + ".");
                                if (isHelpDesk2)
                                    sbComment.Append("\n" + S + ">> has reassigned this ticket to " + _accountManager.GetFullUserNameById((int)hdt.ASSIGNED_TO) + ".");

                            }
                            if (isPriorityChanged)
                            {
                                isDataChanged = true;
                                //sbComment.Append("\n" + DateTime.Now.ToShortDateString() + ":" + DateTime.Now.ToShortTimeString());
                                sbComment.Append("\n" + DateTime.UtcNow.ToShortDateString() + ":" + DateTime.UtcNow.ToShortTimeString());
                                //29082019
                                //sbComment.Append("\n" + UserPrincipal.Current.FullName + " (" + UserPrincipal.Current.UserName + ")" + " has set this ticket Priority to '" + _hdtktmngr.getPriorityById((int)hdt.PRIORITY) + "'.");
                                //sbComment.Append("\n" + S + " has set this ticket Priority to '" + _hdtktmngr.getPriorityById((int)hdt.PRIORITY) + "'.");
                                //karri11102019
                                if (isHelpDesk1)
                                    sbComment.Append("\n" + S + ">> has set this ticket Priority to '" + _hdtktmngr.getPriorityById((int)hdt.PRIORITY) + "'.");
                                if (isHelpDesk2)
                                    sbComment.Append("\n" + S + ">> has set this ticket Priority to '" + _hdtktmngr.getPriorityById((int)hdt.PRIORITY) + "'.");

                            }
                            if (isStatusChanged)
                            {
                                isDataChanged = true;
                                //sbComment.Append("\n" + DateTime.Now.ToShortDateString() + ":" + DateTime.Now.ToShortTimeString());
                                sbComment.Append("\n" + DateTime.UtcNow.ToShortDateString() + ":" + DateTime.UtcNow.ToShortTimeString());
                                //29082019
                                //sbComment.Append("\n" + UserPrincipal.Current.FullName + " (" + UserPrincipal.Current.UserName + ")" + " has set this Status to '" + _hdtktmngr.getStatusById((int)hdt.STATUS) + "'.");
                                //sbComment.Append("\n" + S + " has set this Status to '" + _hdtktmngr.getStatusById((int)hdt.STATUS) + "'.");
                                //karri11102019
                                if (isHelpDesk1)
                                    sbComment.Append("\n" + S + ">> has set this Status to '" + _hdtktmngr.getStatusById((int)hdt.STATUS) + "'.");
                                if (isHelpDesk2)
                                    sbComment.Append("\n" + S + ">> has set this Status to '" + _hdtktmngr.getStatusById((int)hdt.STATUS) + "'.");

                            }
                            if (hdt.USERCOMMENTS != null && !string.IsNullOrEmpty(hdt.USERCOMMENTS))
                            {
                                isDataChanged = true;
                                //sbComment.Append("\n" + DateTime.Now.ToShortDateString() + ":" + DateTime.Now.ToShortTimeString());
                                sbComment.Append("\n" + DateTime.UtcNow.ToShortDateString() + ":" + DateTime.UtcNow.ToShortTimeString());
                                //29082019
                                //sbComment.Append("\n" + UserPrincipal.Current.FullName + " (" + UserPrincipal.Current.UserName + ") : COMMENTS: " + hdt.USERCOMMENTS + ".");
                                //sbComment.Append("\n" + S + " : COMMENTS: " + hdt.USERCOMMENTS + ".");

                                //karri11102019
                                if (isHelpDesk2)
                                    sbComment.Append("\n" + S + ">> Comments: " + hdt.USERCOMMENTS + ".");
                                else if (isHelpDesk1)
                                    sbComment.Append("\n" + S + ">> Comments: " + hdt.USERCOMMENTS + ".");
                                else
                                    sbComment.Append("\n" + S + "> Comments: " + hdt.USERCOMMENTS + ".");
                            }
                        }

                        if (isDataChanged)
                        {
                            hdt.CONVERSATION = sbComment.ToString();
                            //hdt.LAST_MODIFIED_ON = DateTime.Now;
                            hdt.LAST_MODIFIED_ON = DateTime.UtcNow;
                        }

                        //we are already in update mode
                        //1. identify In Progress and Assignement
                        //2. identify either status change or reassignment change
                        if (isOhterUser)
                            _hdtktmngr.updateTicket(hdt, isStatusChanged, isAssigned, isReassigned, isPriorityChanged, isDataChanged, true);//for HU HelpDesk1,  pass value as true
                        if (isHelpDesk1)
                            _hdtktmngr.updateTicket(hdt, isStatusChanged, isAssigned, isReassigned, isPriorityChanged, isDataChanged, true);//for HU HelpDesk1,  pass value as true
                        else if (isHelpDesk2)
                            _hdtktmngr.updateTicket(hdt, isStatusChanged, isAssigned, isReassigned, isPriorityChanged, isDataChanged, false);//for HU HelpDesk2,  pass value as false
                        else if (isAdminUser)
                            _hdtktmngr.updateTicket(hdt, isStatusChanged, isAssigned, isReassigned, isPriorityChanged, isDataChanged, false);//for HU HelpDesk2,  pass value as false
                    }
                    #endregion

                    #region MAILING AND NOTIFICATIONS

                    bool CanSendMailNotification = false;
                    if (ConfigurationManager.AppSettings["HUDMailNotificationsFlag"] != null
                       && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDMailNotificationsFlag"].ToString())
                       && Convert.ToBoolean(ConfigurationManager.AppSettings["HUDMailNotificationsFlag"].ToString()))
                    {
                        CanSendMailNotification = true;
                    }

                    ////karri#11092019//stopping mailing for only if comments only is given
                    //if(isUPDATE && isOhterUser && isDataChanged)
                    //    CanSendMailNotification = false;
                    //if((isHelpDesk1 || isHelpDesk2) && !isAssigned && !isReassigned && !isPriorityChanged && !isStatusChanged && isDataChanged)
                    //    CanSendMailNotification = false;

                    //karri-hareesh-13092019
                    //karri#11092019//stopping mailing for only if comments only is given
                    //mails to ticket creator and helpdesk team                    
                    if (isUPDATE && isOhterUser && isDataChanged)
                        isOnlyCmntsSubmitted = true;
                    //if help desk team puts a comments then mail to ticket creator and helpdesk team
                    if ((isHelpDesk1 || isHelpDesk2) && isUPDATE && !isAssigned && !isReassigned && !isPriorityChanged && !isStatusChanged && isDataChanged)
                        isOnlyCmntsSubmitted = true;

                    //filling data for mailing process for updates
                    //for CREATE Ticket notification
                    if (!isUPDATE && CanSendMailNotification)
                    {
                        //if CC is given in config
                        if (ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"] != null
                            && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString()))
                            hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();

                        //Submitter email
                        hdt.MAILNOTIFICATION_REQUESTOREMAIL = UserPrincipal.Current.UserName;

                        //karri:
                        //NOTE:MAILNOTIFICATION_REQUESTOREMAIL = who created the ticket, to whom the Ticket is intended TO
                        //NOTE:MAILNOTIFICATION_TOEMAIL= to HELPDESK, to TECH DESK, to BASE EMAIL

                        //common GROUP MAIL for HUD Help Desk
                        if (ConfigurationManager.AppSettings["HUDHelpDeskDLEmail"] != null
                            && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDHelpDeskDLEmail"].ToString()))
                            hdt.MAILNOTIFICATION_TOEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskDLEmail"].ToString();

                        //if (ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"] != null
                        //    && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString()))
                        //    hdt.MAILNOTIFICATION_TOEMAIL += "," + ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();

                        //BASE EMAIL
                        if (ConfigurationManager.AppSettings["HUDMoniterFromEmail"] != null
                            && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString()))
                            hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();

                        hdt.MAILNOTIFICATION_TICKETMODE = 1;//update
                        hdt.TICKET_ID = hdt.TICKET_ID;
                        hdt.MAILNOTIFICATION_STATUS = "OPEN";
                        _backgroundManager.SendHelpDeskNotificationEmail(_emailManager, hdt);

                        return RedirectToAction("GetMyTickets");
                    }
                    //Submitter Actions after Ticket is created Like submitting Comments Only, submitter action is limited to comments only
                    else if (isUPDATE && isOhterUser && isDataChanged && CanSendMailNotification)
                    {
                        if (hdt.ASSIGNED_TO == null || hdt.ASSIGNED_TO < 1)
                        {
                            //common GROUP MAIL for HUD Help Desk
                            if (ConfigurationManager.AppSettings["HUDHelpDeskDLEmail"] != null
                                && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDHelpDeskDLEmail"].ToString()))
                                hdt.MAILNOTIFICATION_TOEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskDLEmail"].ToString();
                        }
                        else
                        {
                            hdt.MAILNOTIFICATION_TOEMAIL = _accountManager.GetUserNameById(Convert.ToInt32(hdt.ASSIGNED_TO));
                        }

                        //if CC is given in config
                        if (ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"] != null
                            && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString()))
                            hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();

                        hdt.MAILNOTIFICATION_REQUESTOREMAIL = UserPrincipal.Current.UserName;

                        //hdt.MAILNOTIFICATION_TOEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskDLEmail"].ToString();

                        //if (ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"] != null
                        //    && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString()))
                        //    hdt.MAILNOTIFICATION_TOEMAIL += "," + ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();

                        hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();

                        hdt.MAILNOTIFICATION_TICKETMODE = 2;//update
                        hdt.MAILNOTIFICATION_SUBJECT = "Help Desk Update Notification: #" + hdt.TICKET_ID + "; User Comments Update!";
                        hdt.TICKET_ID = hdt.TICKET_ID;
                        if (hdt.STATUS.Equals(0))
                            hdt.MAILNOTIFICATION_STATUS = "OPEN";
                        else if (hdt.STATUS.Equals(1))
                            hdt.MAILNOTIFICATION_STATUS = "INPROGRESS";

                        _backgroundManager.SendHelpDeskNotificationEmail(_emailManager, hdt);

                        return RedirectToAction("GetMyTickets");
                    }


                    else if (isUPDATE && CanSendMailNotification)//11092019;karri
                    {
                        hdt.MAILNOTIFICATION_TICKETMODE = 2;//update
                        hdt.TICKET_ID = hdt.TICKET_ID;
                        //Help Desk Ticket Update Notification: #" + model.TICKET_ID.ToString() + "; For: " + model.ISSUE;
                        hdt.MAILNOTIFICATION_SUBJECT = "Help Desk Update Notification: #" + hdt.TICKET_ID + ";";

                        //if CC is given in config
                        if (ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"] != null
                            && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString()))
                            hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();


                        //if only comments are submitted by help desk team when the ticket is in Progress
                        if (isOnlyCmntsSubmitted)
                        {
                            //HDNOTIFICATIONS                            
                            HD_TICKET_NOTIFICATIONS notobj = new HD_TICKET_NOTIFICATIONS();
                            notobj.TICKET_ID = hdt.TICKET_ID;
                            notobj.TITLE = hdt.ISSUE;
                            notobj.TARGETUSERID = (int)_accountManager.GetUserId(hdt.CREATED_BY_USERNAME);
                            notobj.MESSAGE = "Your Ticket has comments from Help Desk Team!";
                            hdt.MAILNOTIFICATION_SUBJECT = "Help Desk Update Notification: #" + hdt.TICKET_ID + ";HelpDesk Comment!";
                            //notobj.LOGDATE = DateTime.Now;
                            notobj.LOGDATE = DateTime.UtcNow;
                            _hdtktnotifmngr.saveNotifiation(notobj);

                            //hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();
                            //hdt.MAILNOTIFICATION_REQUESTOREMAIL = UserPrincipal.Current.UserName;

                            hdt.MAILNOTIFICATION_REQUESTOREMAIL = hdt.CREATED_BY_USERNAME;
                            //Note:Assignee from Help Desk
                            if (hdt.ASSIGNED_TO != null && hdt.ASSIGNED_TO > 1)
                                hdt.MAILNOTIFICATION_TOEMAIL = _accountManager.GetUserNameById(Convert.ToInt32(hdt.ASSIGNED_TO));

                            hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();
                            hdt.MAILNOTIFICATION_SUBJECT += "Ticket Assigned;";

                        }

                        //only hudhelpdesk can assign the ticket, mail to hudhelpdesk, ticket owner
                        if (isAssigned)
                        {
                            //HDNOTIFICATIONS                            
                            HD_TICKET_NOTIFICATIONS notobj = new HD_TICKET_NOTIFICATIONS();
                            notobj.TICKET_ID = hdt.TICKET_ID;
                            notobj.TITLE = hdt.ISSUE;
                            notobj.TARGETUSERID = (int)_accountManager.GetUserId(hdt.CREATED_BY_USERNAME);
                            notobj.MESSAGE = "Your Ticket is assigned to HUD HelpDesk!";
                            //notobj.LOGDATE = DateTime.Now;
                            notobj.LOGDATE = DateTime.UtcNow;
                            _hdtktnotifmngr.saveNotifiation(notobj);

                            //hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();
                            //Note:Who Created the ticket
                            hdt.MAILNOTIFICATION_REQUESTOREMAIL = hdt.CREATED_BY_USERNAME;
                            //Note:Assignee from Help Desk
                            if (hdt.ASSIGNED_TO != null && hdt.ASSIGNED_TO > 1)
                                hdt.MAILNOTIFICATION_TOEMAIL = _accountManager.GetUserNameById(Convert.ToInt32(hdt.ASSIGNED_TO));

                            hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();
                            hdt.MAILNOTIFICATION_SUBJECT += "Ticket Assigned;";

                        }
                        //only hudhelpdesk can reassign the ticket
                        else if (isReassigned)
                        {
                            //HDNOTIFICATIONS                            
                            HD_TICKET_NOTIFICATIONS notobj = new HD_TICKET_NOTIFICATIONS();

                            string assigneename = _hdtktmngr.getHelpDeskUserNameByID((int)hdt.ASSIGNED_TO);

                            if (string.IsNullOrEmpty(assigneename))
                                assigneename = _accountManager.GetUserNameById((int)hdt.ASSIGNED_TO);

                            if (string.IsNullOrEmpty(assigneename))
                                notobj.MESSAGE = "Your Ticket is assigned to HUD TechTeam!";
                            else
                                notobj.MESSAGE = "Your Ticket is assigned to HUD HelpDesk!";

                            notobj.TICKET_ID = hdt.TICKET_ID;
                            notobj.TITLE = hdt.ISSUE;
                            notobj.TARGETUSERID = (int)_accountManager.GetUserId(hdt.CREATED_BY_USERNAME);
                            //notobj.MESSAGE = "Your Ticket is Re-Assigned to HUD HelpDesk!";
                            //notobj.LOGDATE = DateTime.Now;
                            notobj.LOGDATE = DateTime.UtcNow;
                            _hdtktnotifmngr.saveNotifiation(notobj);

                            //hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();

                            if (isHelpDesk1 && !assigneename.Contains("@penielsolutions.com"))
                                hdt.MAILNOTIFICATION_REQUESTOREMAIL = hdt.CREATED_BY_USERNAME;
                            else if (isHelpDesk1 && assigneename.Contains("@penielsolutions.com"))
                                hdt.MAILNOTIFICATION_REQUESTOREMAIL = UserPrincipal.Current.UserName;

                            if (isHelpDesk2)
                                hdt.MAILNOTIFICATION_REQUESTOREMAIL = UserPrincipal.Current.UserName;

                            hdt.MAILNOTIFICATION_TOEMAIL = assigneename;
                            hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();
                            hdt.MAILNOTIFICATION_SUBJECT += "Ticket Re-Assigned;";

                        }
                        //hudhelpdesk or hudadmin can change the priority
                        else if (isPriorityChanged)
                        {
                            // HDNOTIFICATIONS
                            HD_TICKET_NOTIFICATIONS notobj = new HD_TICKET_NOTIFICATIONS();

                            //hudadmin
                            if (isAdminUser)
                            {
                                notobj.MESSAGE = "Your Ticket Proirity is set to " + _hdtktmngr.getPriorityById((int)hdt.PRIORITY) + " by HUD Admin";

                                //hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();
                                hdt.MAILNOTIFICATION_REQUESTOREMAIL = UserPrincipal.Current.UserName;
                                //Note:Assignee from Help Desk
                                if (hdt.ASSIGNED_TO != null && hdt.ASSIGNED_TO > 1)
                                    hdt.MAILNOTIFICATION_TOEMAIL = _accountManager.GetUserNameById(Convert.ToInt32(hdt.ASSIGNED_TO));

                                hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();
                            }
                            else
                            {
                                notobj.MESSAGE = "Your Ticket Proirity is set to " + _hdtktmngr.getPriorityById((int)hdt.PRIORITY) + " by HUD HelpDesk";

                                //hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();
                                hdt.MAILNOTIFICATION_REQUESTOREMAIL = hdt.CREATED_BY_USERNAME;
                                //Note:Assignee from Help Desk
                                if (hdt.ASSIGNED_TO != null && hdt.ASSIGNED_TO > 1)
                                    hdt.MAILNOTIFICATION_TOEMAIL = _accountManager.GetUserNameById(Convert.ToInt32(hdt.ASSIGNED_TO));

                                hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();
                            }
                            hdt.MAILNOTIFICATION_SUBJECT += "Priority Set;";

                            // HDNOTIFICATIONS
                            notobj.TICKET_ID = hdt.TICKET_ID;
                            notobj.TITLE = hdt.ISSUE;
                            notobj.TARGETUSERID = (int)_accountManager.GetUserId(hdt.CREATED_BY_USERNAME);
                            //notobj.MESSAGE = "Your Ticket has been Re-Assigned to HUD HelpDesk!";
                            //notobj.LOGDATE = DateTime.Now;
                            notobj.LOGDATE = DateTime.UtcNow;
                            _hdtktnotifmngr.saveNotifiation(notobj);
                        }
                        //only hudhelpdesk or techteam can change the status
                        //if hudhelpdesk, mail to hudhelpdesk, ticket owner, 
                        //if techteam, mail to techteam, hudhlepdesk, hudadmin
                        else if (isStatusChanged)
                        {
                            //Added by siddu@02122019
                            if ((int)hdt.STATUS == 3 || (int)hdt.STATUS == 4 || (int)hdt.STATUS == 5 || (int)hdt.STATUS == 6)
                            {
                                if (ConfigurationManager.AppSettings["HUDHelpDeskResloveCCDLEmail"] != null
                                    && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDHelpDeskResloveCCDLEmail"].ToString()))
                                {
                                    if (!string.IsNullOrEmpty(hdt.MAILNOTIFICATION_CCEMAIL))
                                        hdt.MAILNOTIFICATION_CCEMAIL = hdt.MAILNOTIFICATION_CCEMAIL + "," + ConfigurationManager.AppSettings["HUDHelpDeskResloveCCDLEmail"].ToString();
                                    else
                                        hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskResloveCCDLEmail"].ToString();
                                }

                            }


                            // HDNOTIFICATIONS 
                            HD_TICKET_NOTIFICATIONS notobj = new HD_TICKET_NOTIFICATIONS();

                            if (isHelpDesk1)
                            {
                                notobj.MESSAGE = "Your Ticket status is set to " + _hdtktmngr.getStatusById((int)hdt.STATUS) + " by HUD HelpDesk";

                                //hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();
                                hdt.MAILNOTIFICATION_REQUESTOREMAIL = hdt.CREATED_BY_USERNAME;
                                //Note:Assignee from Help Desk
                                if (hdt.ASSIGNED_TO != null && hdt.ASSIGNED_TO > 1)
                                    hdt.MAILNOTIFICATION_TOEMAIL = _accountManager.GetUserNameById(Convert.ToInt32(hdt.ASSIGNED_TO));

                                hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();
                            }
                            else if (isHelpDesk2)
                            {
                                notobj.MESSAGE = "Your Ticket status is set to " + _hdtktmngr.getStatusById((int)hdt.STATUS) + " by HUD TechTeam";

                                //hdt.MAILNOTIFICATION_CCEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskCCDLEmail"].ToString();
                                hdt.MAILNOTIFICATION_REQUESTOREMAIL = ConfigurationManager.AppSettings["HUDTechTeamDLEmail"].ToString();
                                hdt.MAILNOTIFICATION_TOEMAIL = ConfigurationManager.AppSettings["HUDHelpDeskDLEmail"].ToString();
                                hdt.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();
                            }

                            // HDNOTIFICATIONS
                            notobj.TICKET_ID = hdt.TICKET_ID;
                            notobj.TITLE = hdt.ISSUE;
                            notobj.TARGETUSERID = (int)_accountManager.GetUserId(hdt.CREATED_BY_USERNAME);
                            //notobj.MESSAGE = "Your Ticket has been Re-Assigned to HUD HelpDesk!";
                            //notobj.LOGDATE = DateTime.Now;
                            notobj.LOGDATE = DateTime.UtcNow;
                            _hdtktnotifmngr.saveNotifiation(notobj);


                            hdt.MAILNOTIFICATION_SUBJECT += "Status Set";
                            hdt.MAILNOTIFICATION_STATUS = _hdtktmngr.getStatusById((int)hdt.STATUS);
                            hdt.MAILNOTIFICATION_SUBJECT += ";";
                        }


                        if (CanSendMailNotification)
                        {
                            //#498
                            string tktstatus = _hdtktmngr.getStatusById((int)hdt.STATUS);
                            hdt.MAILNOTIFICATION_STATUS = tktstatus;

                            ////karri;sup#187
                            //if ticket is assigned to HUD232helpdesk or techrteam, then mail notificaitaon to Tech Team, Ticket Owner(creator), 
                            //HUD Helpdesk Admin who assigned this ticket to Tech Team
                            if (isHelpDesk2)
                            {
                                //identify who is the last HUD Help Desk Assignee from the Conversation/Log dat
                                string conv = hdt.CONVERSATION;
                                string[] arrConversation = conv.Split(new string[] { "\n" }, StringSplitOptions.None);
                                ArrayList reqData = new ArrayList();
                                if (arrConversation.Length > 0)
                                {
                                    foreach (string s in arrConversation)
                                    {
                                        if (s.Contains("has reassigned this ticket to") || s.Contains("has assigned this ticket to"))
                                            reqData.Add(s);
                                    }
                                }

                                string hudhdadminusrename = string.Empty;

                                //obviously the last log will be for Tech Team only in the Conversation Log
                                if (reqData.Count >= 2)
                                {
                                    //becoz after reassigned to Tech Team, the log text is already written in conversation as the latest update in Log
                                    //so excluding this there should be atleast one HUDHDAdmin user assignment inorder to send a notificaiton, so 
                                    //total assignment and reassignment should be atleast 2
                                    string data = reqData[reqData.Count - 1 - 1].ToString();
                                    string hudhdassigneename = data.Substring(2);
                                    int iDx = data.LastIndexOf("ticket to") + 9;
                                    string givenFullNamedata = data.Substring(iDx).Trim();

                                    //get the list of hud admin users
                                    DataTable dt = _hdtktmngr.getHelpDesk1Users();
                                    //split given full name and check if it contians in the username
                                    //max will contain 3 cols for name combination
                                    bool isHudHDAdminFound = false;
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        string username = dr["Text"].ToString();
                                        string[] usernamecompo = username.Split('@');
                                        usernamecompo = usernamecompo[0].Split('.');

                                        foreach (string s in usernamecompo)
                                        {
                                            if (s.Length > 1)
                                                if (givenFullNamedata.ToUpper().Contains(s.ToUpper()))
                                                {
                                                    int hudhdadminUserId = Convert.ToInt32(dr["Value"].ToString());
                                                    string fullname = _accountManager.GetFullUserNameById(hudhdadminUserId);
                                                    if (givenFullNamedata.ToUpper().Contains(fullname.ToUpper()))
                                                    {
                                                        hudhdadminusrename = _accountManager.GetUserNameById(hudhdadminUserId);
                                                        isHudHDAdminFound = true;
                                                        break;
                                                    }
                                                }
                                        }
                                        if (isHudHDAdminFound)
                                            break;
                                    }
                                }
                                else if (reqData.Count == 1)
                                {
                                    //the HUDHDAdmin without their Self Assignment of the ticket they directly assigned to Tech Team/Helpdesk2, hence
                                    //only one record will be there in the conversation log
                                    //MAILNOTIFICATION_REQUESTOREMAIL = HUDHDADMIN User
                                    //MAILNOTIFICATION_TOEMAIL = new Target mail User
                                    hdt.MAILNOTIFICATION_REQUESTOREMAIL = string.Empty;
                                }

                                if (hudhdadminusrename != string.Empty)
                                {
                                    hdt.MAILNOTIFICATION_REQUESTOREMAIL = hudhdadminusrename;
                                }
                                //end of s#187

                            }//end of s#187

                            _backgroundManager.SendHelpDeskNotificationEmail(_emailManager, hdt);
                        }

                        return RedirectToAction("GetMyTickets");
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    string a = ex.Message;

                    //ON CREATION- after Ticket created
                    //if ticketid is created but an exception during files uploading, than delete the temp folder
                    if (ticketid > 0 && !isUPDATE)
                    {
                        if (Directory.Exists(s2newPath))
                            Directory.Delete(s2newPath);
                    }
                    //ON CREATION-  Ticket created failed
                    else if (!isUPDATE) //if problem in ticket creation but files are already uploaded then delete the folder again
                    {
                        //if ticket submission fails then delete the files folder also if any files existing
                        if (ticketid.Equals(0) && Directory.Exists(s2newPath))
                            Directory.Delete(s2newPath);
                    }
                }
            }
            else
            {
                throw new Exception("Error while submitting Help Desk ticket");
            }

            return RedirectToAction("GetMyTickets");
        }

        [HttpPost]
        public void SearchHelpDeskTicket()
        {

        }

        //displays the grid for tickets for a User and for HelpDesk admin
        [HttpPost]
        public JsonResult GetMyTickets(int _userid)
        {


            var jsonData = new
            {
                total = "",
                records = "",

            };
            return Json(jsonData);
        }

        //displays the grid for tickets       
        public ActionResult GetMyTickets()
        {


            var hdt = new HD_TICKET_ViewModel();
            //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);

            //Note:HD_TICKET_ViewModel class should have a constructur instantiating these child classes like 
            //TicketStatusList, TicketCategoryList for them to work with
            //hdt.TicketStatusList.Add(new SelectListItem() { Text = "OPEN", Value = "0" });
            //hdt.TicketStatusList.Add(new SelectListItem() { Text = "INPROGRESS", Value = "1" });
            //hdt.TicketStatusList.Add(new SelectListItem() { Text = "REJECTED", Value = "2" });
            //hdt.TicketStatusList.Add(new SelectListItem() { Text = "CLOSED", Value = "3" });


            //hdt.TicketCategoryList.Add(new SelectListItem() { Text = "Data Issue", Value = "1" });
            //hdt.TicketCategoryList.Add(new SelectListItem() { Text = "Technical", Value = "2" });
            //hdt.TicketCategoryList.Add(new SelectListItem() { Text = "Functional", Value = "3" });

            //karri;31082019
            DataTable dtStatus = _hdtktmngr.getStatusTbl();
            DataTable dtCategory = _hdtktmngr.getCateogryTbl();
            //DataTable dtSubCategory = _hdtktmngr.getSubCategoryTbl();
            DataTable dtOnAction = _hdtktmngr.getOnActionTbl();
            //DataTable dtPriority = _hdtktmngr.getPriorityTbl();

            ////Note:HD_TICKET_ViewModel class should have a constructur instantiating these child classes like 
            foreach (DataRow dr in dtStatus.Rows)
                hdt.TicketStatusList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtCategory.Rows)
                hdt.TicketCategoryList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            //foreach (DataRow dr in dtSubCategory.Rows)
            //    hdt.TicketSubCategoryList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtOnAction.Rows)
                hdt.TicketOnActionList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            //foreach (DataRow dr in dtPriority.Rows)
            //    hdt.TicketPriorityList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            //Naveen 28-08
            DataTable dtSubCategory = _hdtktmngr.getSubCategoryTbl();
            DataTable dtPriority = _hdtktmngr.getPriorityTbl();
            foreach (DataRow dr in dtSubCategory.Rows)
                hdt.TicketSubCategoryList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtPriority.Rows)
                hdt.TicketPriorityList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            //var hdticketsGridData11 = _hdtktmngr.GetMyTickets(0);
            //foreach (var i in hdticketsGridData11)
            //    hdt.TicketUsernameList.Add(new SelectListItem() { Text = i.CREATED_BY_USERNAME , Value = i.CREATED_BY_USERNAME });


            //karri;31082019;to get user names who created tickets from the displayed grid list, for normal users like lender, 
            //get the names from the displayed list
            bool isAdminUser = false;//Note: there is no HUAdmin extra functionality, HUAdmin will only have HelpDesk1 role from now
            bool isHlepDesk1 = false;
            bool isOhterUser = false;
            bool isHlepDesk2 = false;

            int loggedUserID = 0;//userid = 0;
            IdentifyUserRole(ref isAdminUser, ref isHlepDesk1, ref isOhterUser, ref isHlepDesk2, UserPrincipal.Current.UserId);

            if (isHlepDesk1 || isHlepDesk2)
                loggedUserID = 0;
            else
                loggedUserID = UserPrincipal.Current.UserId;

            if (isHlepDesk1 || isHlepDesk2)
            {

                var hdticketsGridData2 = _hdtktmngr.GetMyTickets(loggedUserID, isHlepDesk1, isHlepDesk2, UserPrincipal.Current.UserId).Select(o => new { o.CREATED_BY_USERNAME }).Distinct();

                //int page1 = 1;

                foreach (var item in hdticketsGridData2)
                    hdt.TicketUsernameList.Add(new SelectListItem() { Text = item.CREATED_BY_USERNAME, Value = item.CREATED_BY_USERNAME });

                //Added by siddesh 11102019
                if (isHlepDesk1 || isHlepDesk2)
                {

                    var hdticketsGridData5 = _hdtktmngr.GetMyTickets(loggedUserID, isHlepDesk1, isHlepDesk2, UserPrincipal.Current.UserId).Select(o => new { o.ASSIGNED_TO_USERNAME }).Distinct();

                    //int page1 = 1;

                    foreach (var item in hdticketsGridData5)
                    {
                        if (!string.IsNullOrEmpty(item.ASSIGNED_TO_USERNAME.ToString()))
                            hdt.TicketHelpdesknameList.Add(new SelectListItem() { Text = item.ASSIGNED_TO_USERNAME, Value = item.ASSIGNED_TO_USERNAME });
                    }
                }
                //foreach (var item in hdticketsGridData2)
                //    hdt.TicketHelpdesknameList.Add(new SelectListItem() { Text = item., Value = item.CREATED_BY_USERNAME });

                //foreach (var item in resultdata)
                //    hdt.TicketUsernameList.Add(new SelectListItem() { Text = item.CREATED_BY_USERNAME, Value = item.CREATED_BY_USERNAME });
                //return View("~/Views/HelpDesk/GetMyTickets.cshtml", hdticketsGridData);

                return View(hdt);
                // return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

            //for narmal users other than ticket admins, be it lenders, internal users etc
            ArrayList alGroupUsers = new ArrayList();
            ArrayList alInternalUsers = new ArrayList();
            alInternalUsers.Add("AccountExecutive");
            alInternalUsers.Add("WorkflowManager");
            alInternalUsers.Add("Attorney");
            alInternalUsers.Add("InternalSpecialOptionUser");
            alInternalUsers.Add("ProductionUser");
            alInternalUsers.Add("ProductionWlm");
            alInternalUsers.Add("Reviewer");

            if (alInternalUsers.Contains(UserPrincipal.Current.UserRole))
                alGroupUsers.Add(UserPrincipal.Current.UserId);//karri#280
            else
            {
                var model = _accountManager.GetDataAdminManageUser(null, 1, "UserName", HUDHealthcarePortal.Model.SqlOrderByDirecton.ASC);

                int userLenderId = UserPrincipal.Current.LenderId ?? 0;
                foreach (var item in model)
                {
                    if (item.LenderID.Equals(userLenderId))
                        alGroupUsers.Add(item.UserID);
                }
            }
            var hdticketsGridData = _hdtktmngr.GetMyTickets(alGroupUsers).Select(o => new { o.CREATED_BY_USERNAME }).Distinct();

            foreach (var item in hdticketsGridData)
                hdt.TicketUsernameList.Add(new SelectListItem() { Text = item.CREATED_BY_USERNAME, Value = item.CREATED_BY_USERNAME });
            ////Added by siddesh 11102019
            //var hdticketsGridData5 = _hdtktmngr.GetMyTickets(alGroupUsers).Select(o => new { o.ASSIGNED_TO_USERNAME }).Distinct();
            //foreach (var item in hdticketsGridData5)
            //    hdt.TicketHelpdesknameList.Add(new SelectListItem() { Text = item.ASSIGNED_TO_USERNAME, Value = item.ASSIGNED_TO_USERNAME });
            //foreach (var item in hdticketsGridData)
            //    hdt.TicketUsernameList.Add(new SelectListItem() { Text = item.CREATED_BY_USERNAME, Value = item.CREATED_BY_USERNAME });
            ////return View("~/Views/HelpDesk/GetMyTickets.cshtml", hdticketsGridData);
            return View(hdt);
            //return Json(jsonData2,JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetTicketsData(string status, string categorytype, string Priority, string ticketid, string UserName, string FromDate, string ToDate, string HelpDeskName)
        //{
        //    return FilterHelpDeskTicket(status, categorytype, Priority, ticketid, UserName, FromDate, ToDate, HelpDeskName);
        //}

        //Added by siddu 290120 for grid show//
        public JsonResult GetTicketsData(int page, int rows, string status, string categorytype, string Priority, string ticketid, string UserName, string FromDate, string ToDate, string HelpDeskName)
        {
            return FilterHelpDeskTicket(page, rows, status, categorytype, Priority, ticketid, UserName, FromDate, ToDate, HelpDeskName);
        }

        public void IdentifyUserRole(ref bool isAdminUser, ref bool isHelpDesk1, ref bool isOhterUser, ref bool isHelpDesk2, int userID)
        {
            //if SET Role for the logged user who is HUDAdmin matches with meta data then only hes is set as HelpDesk/HUDAdmin who can own the tickets
            foreach (DictionaryEntry entry in _htLoggedUserRole)
            {
                //karri11102019
                //if userid already exists in the Help Desk Users list in the support xml file then can mark him as Help Desk1
                //Search is only in HelpDesk type, not for TECHDESK
                string username = _hdtktmngr.getHelpDeskUserNameByID(UserPrincipal.Current.UserId);
                if (username == UserPrincipal.Current.UserName)
                {
                    isHelpDesk1 = true;//NOTE: for HUDAdmin we are setting HelpDesk1 true since we are ignore now HUDAdmin old functionality    
                    break;
                }
                else//if for TechDesk/HUDAdmin/Other Users
                {

                    if (Convert.ToBoolean(_htLoggedUserRole["HUDAdmin"].ToString()))
                    {
                        username = _hdtktmngr.getHelpDeskUserNameByID(UserPrincipal.Current.UserId);
                        if (username == UserPrincipal.Current.UserName)
                        {
                            isHelpDesk1 = true;//NOTE: for HUDAdmin we are setting HelpDesk1 true since we are ignore now HUDAdmin old functionality    
                            break;
                        }

                        isOhterUser = true;
                        break;
                    }
                    if (Convert.ToBoolean(_htLoggedUserRole["TechDesk"].ToString()))
                    {
                        isHelpDesk2 = true;
                        break;
                    }
                    if (Convert.ToBoolean(_htLoggedUserRole["OtherUser"].ToString()))
                    {
                        isOhterUser = true;
                        break;
                    }
                }

            }
        }

        /// <summary>
        ///  Implemented by Hareesh For Filtering Search Results
        /// </summary>
        /// <param name="status"></param>
        /// <param name="categorytype"></param>
        /// <returns></returns>
        //Naveen 28-08
        public JsonResult FilterHelpDeskTicket(int pages, int rows, string status, string categorytype, string Priority, string ticketid, string UserName, string FromDate, string ToDate, string HelpDeskName)
        {
            int pageIndex = Convert.ToInt32(pages) - 1;
            int pageSize = rows;
            int cType = -1;
            if (categorytype != null || !string.IsNullOrEmpty(categorytype))
                cType = Convert.ToInt32(categorytype);

            int sType = -1;
            if (status != null || !string.IsNullOrEmpty(status))
                sType = Convert.ToInt32(status);

            int priorityType = -1;
            if (Priority != null || !string.IsNullOrEmpty(Priority))
                priorityType = Convert.ToInt32(Priority);

            int tktid = -1;
            if (ticketid != null || !string.IsNullOrEmpty(ticketid))
                tktid = Convert.ToInt32(ticketid);

            //string UName;
            //if (UserName != null || !string.IsNullOrEmpty(UserName))
            //    UName = UserName;

            bool isAdminUser = false;//Note: there is no HUAdmin extra functionality, HUAdmin will only have HelpDesk1 role from now
            bool isHlepDesk1 = false;
            bool isOhterUser = false;
            bool isHlepDesk2 = false;

            int loggedUserID = 0;//userid = 0;
            IdentifyUserRole(ref isAdminUser, ref isHlepDesk1, ref isOhterUser, ref isHlepDesk2, UserPrincipal.Current.UserId);
            //if (isAdminUser || isHlepDesk1 || isHlepDesk2)
            if (isHlepDesk1 || isHlepDesk2)
                loggedUserID = 0;
            else
                loggedUserID = UserPrincipal.Current.UserId;

            //Note:this functionlaity is no more working as we are not at all setting this flag, HUdAdmin from now has HelpDesk1 role only
            #region HUDADMIN (NO MORE REQUIRED NOW)
            if (isAdminUser)
            {
                var hdticketsGridData1 = _hdtktmngr.GetMyTickets(loggedUserID);
                //Naveen 28-08
                var filteredTickets1 = FilterDataByStatusAndCategory(hdticketsGridData1, cType, sType, priorityType, tktid, UserName, FromDate, ToDate, HelpDeskName);

                //var filteredTickets1 = FilterDataByStatusAndCategory(hdticketsGridData1, cType, sType);


                var hdt1 = new HD_TICKET_ViewModel();
                foreach (var item in filteredTickets1)
                {
                    hdt1.TICKET_ID = item.TICKET_ID;
                    // Added by Harish FHANumber,PropertyId,PropertyName
                    hdt1.FHANumber = item.FHANumber;
                    hdt1.PropertyId = item.PropertyId;
                    hdt1.PropertyName = item.PropertyName;
                    hdt1.STATUS = item.STATUS;
                    hdt1.ISSUE = item.ISSUE;
                    hdt1.DESCRIPTION = item.DESCRIPTION;
                    hdt1.CATEGORY = item.CATEGORY;
                    hdt1.PRIORITY = item.PRIORITY;
                    hdt1.RECREATION_STEPS = item.RECREATION_STEPS;
                    hdt1.CONVERSATION = item.CONVERSATION;
                    hdt1.CONVERSATION = item.CONVERSATION;
                    hdt1.CREATED_ON = item.CREATED_ON;
                    hdt1.ASSIGNED_TO = item.ASSIGNED_TO;
                    //   hdt1.ASSIGNED_TO_USERNAME = assigneduseremail;
                    hdt1.ASSIGNED_DATE = item.ASSIGNED_DATE;
                    hdt1.LAST_MODIFIED_BY = item.LAST_MODIFIED_BY;
                    hdt1.LAST_MODIFIED_ON = item.LAST_MODIFIED_ON;
                    // commented by harish and naveen 19092019
                    //if (item.STATUS == 0 || item.STATUS == 1)
                    //{
                    //    TimeSpan ts = DateTime.Now - item.CREATED_ON;
                    //    item.OPENEDDAYS = ts.Days.ToString();

                    //}
                    //Added by siddu//
                    //siddu #825 @10042020
                    int ts = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.UtcNow);
                    item.OPENEDDAYS = ts.ToString();
                    //siddu #825 @10042020
                    if (item.STATUS == 2 || item.STATUS == 3 || item.STATUS == 4)
                    {
                        int ts1 = GetNumberOfWorkingDays(item.CREATED_ON, item.LAST_MODIFIED_ON.Value.Date);
                        item.OPENEDDAYS = ts1.ToString();
                    }
                    //siddu #825 @10042020
                    if (item.STATUS == 5 || item.STATUS == 6)
                    {
                        int ts1 = GetNumberOfWorkingDays(item.ASSIGNED_DATE.Value.Date, DateTime.UtcNow);
                        //item.Numberofdays = ts1;
                        //item.Numberofweeks = item.Numberofdays / 7;
                        item.OPENEDDAYS = ts1.ToString();
                    }
                }
                //Added by siddu 290120//
                int totalrecords = filteredTickets1.Count();
                var totalpages = (int)Math.Ceiling((float)totalrecords / (float)rows);
                var results = filteredTickets1.Skip(pageIndex * pageSize).Take(pageSize);
                // int page1 = 1;
                var Data1 = new
                {
                    total = totalpages,
                    pages,
                    records = totalrecords,
                    rows = results,

                };
                return Json(Data1, JsonRequestBehavior.AllowGet);
            }
            #endregion


            if (isHlepDesk1 || isHlepDesk2)
            {

                var hdticketsGridData2 = _hdtktmngr.GetMyTickets(loggedUserID, isHlepDesk1, isHlepDesk2, UserPrincipal.Current.UserId);
                //Naveen 28-08
                var filteredTickets2 = FilterDataByStatusAndCategory(hdticketsGridData2, cType, sType, priorityType, tktid, UserName, FromDate, ToDate, HelpDeskName);
                //var filteredTickets2 = FilterDataByStatusAndCategory(hdticketsGridData2, cType, sType);
                var hdt2 = new HD_TICKET_ViewModel();
                foreach (var item in filteredTickets2)
                {
                    hdt2.TICKET_ID = item.TICKET_ID;
                    //Added By Hareesh FHANumber,PropertyId,PropertyName.
                    hdt2.FHANumber = item.FHANumber;
                    hdt2.PropertyId = item.PropertyId;
                    hdt2.PropertyName = item.PropertyName;
                    hdt2.STATUS = item.STATUS;
                    hdt2.ISSUE = item.ISSUE;
                    hdt2.DESCRIPTION = item.DESCRIPTION;
                    hdt2.CATEGORY = item.CATEGORY;
                    hdt2.PRIORITY = item.PRIORITY;
                    hdt2.RECREATION_STEPS = item.RECREATION_STEPS;
                    hdt2.CONVERSATION = item.CONVERSATION;
                    hdt2.CONVERSATION = item.CONVERSATION;
                    hdt2.CREATED_ON = item.CREATED_ON;
                    hdt2.ASSIGNED_TO = item.ASSIGNED_TO;
                    // hdt2.ASSIGNED_TO_USERNAME = assigneduseremail;
                    hdt2.ASSIGNED_DATE = item.ASSIGNED_DATE;
                    hdt2.LAST_MODIFIED_BY = item.LAST_MODIFIED_BY;
                    hdt2.LAST_MODIFIED_ON = item.LAST_MODIFIED_ON;
                    //29082019
                    item.CATEGORY_TEXT = _hdtktmngr.getCategoryById((int)item.CATEGORY);
                    item.SUBCATEGORY_TEXT = _hdtktmngr.getSubCategoryById((int)item.SUBCATEGORY);
                    // commented by harish and naveen 19092019
                    //if (item.STATUS == 0 || item.STATUS == 1)
                    //{
                    //    TimeSpan ts = DateTime.Now - item.CREATED_ON;
                    //    item.OPENEDDAYS = ts.Days.ToString();

                    //}
                    //siddu #825 @10042020
                    int ts = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.UtcNow);
                    item.OPENEDDAYS = ts.ToString();
                    //TimeSpan ts = DateTime.Now - item.CREATED_ON;
                    //item.OPENEDDAYS = ts.Days.ToString();
                    //siddu #825 @10042020
                    if (item.STATUS == 2 || item.STATUS == 3 || item.STATUS == 4)
                    {
                        int ts1 = GetNumberOfWorkingDays(item.CREATED_ON, item.LAST_MODIFIED_ON.Value.Date);
                        item.OPENEDDAYS = ts1.ToString();
                    }
                    //siddu #825 @10042020
                    if (item.STATUS == 5 || item.STATUS == 6)
                    {
                        int ts1 = GetNumberOfWorkingDays(item.ASSIGNED_DATE.Value.Date, DateTime.UtcNow);
                        //item.Numberofdays = ts1;
                        //item.Numberofweeks = item.Numberofdays / 7;
                        item.OPENEDDAYS = ts1.ToString();
                    }
                }

                //Added b y siddu 290120//
                int totalrecords1 = filteredTickets2.Count();
                var totalpages1 = (int)Math.Ceiling((float)totalrecords1 / (float)rows);
                var result1 = filteredTickets2.Skip(pageIndex * pageSize).Take(pageSize);
                //int page2 = 1;
                var jsonData2 = new
                {
                    total = totalpages1,
                    pages,
                    records = totalrecords1,
                    rows = result1,

                };

                return Json(jsonData2, JsonRequestBehavior.AllowGet);
            }
            ////for normal users
            ////get all the gruoup users for this logged user
            ////lenderid is nside the function
            //var model = _accountManager.GetDataAdminManageUser(null, 1, "UserName", HUDHealthcarePortal.Model.SqlOrderByDirecton.ASC);

            ArrayList alGroupUsers = new ArrayList();

            //karri#280//internal users
            ArrayList alInternalUsers = new ArrayList();
            //alInternalUsers.Add("HUDAdmin");
            //alInternalUsers.Add("HUDDirector");
            alInternalUsers.Add("AccountExecutive");
            alInternalUsers.Add("WorkflowManager");
            alInternalUsers.Add("Attorney");
            //alInternalUsers.Add("SuperUser");
            alInternalUsers.Add("InternalSpecialOptionUser");
            alInternalUsers.Add("ProductionUser");
            alInternalUsers.Add("ProductionWlm");
            alInternalUsers.Add("Reviewer");

            //External Users
            //if (!UserPrincipal.Current.UserRole.Equals("LenderAccountRepresentative"))
            //{
            //    if (alInternalUsers.Contains(UserPrincipal.Current.UserRole))
            //        alGroupUsers.Add(UserPrincipal.Current.UserId);//karri#280
            //}
            if (alInternalUsers.Contains(UserPrincipal.Current.UserRole))
                alGroupUsers.Add(UserPrincipal.Current.UserId);//karri#280
            else
            {
                //for normal users
                //get all the gruoup users for this logged user
                //lenderid is nside the function
                var model = _accountManager.GetDataAdminManageUser(null, 1, "UserName", HUDHealthcarePortal.Model.SqlOrderByDirecton.ASC);

                int userLenderId = UserPrincipal.Current.LenderId ?? 0;
                foreach (var item in model)
                {
                    if (item.LenderID.Equals(userLenderId))
                        alGroupUsers.Add(item.UserID);
                }
            }


            //HUDUpdates
            //var hdticketsGridData = _hdtktmngr.GetMyTickets(loggedUserID);
            var hdticketsGridData = _hdtktmngr.GetMyTickets(alGroupUsers);
            //Naveen 28-08
            var filteredTickets = FilterDataByStatusAndCategory(hdticketsGridData, cType, sType, priorityType, tktid, UserName, FromDate, ToDate, HelpDeskName);

            //var filteredTickets = FilterDataByStatusAndCategory(hdticketsGridData, cType, sType);

            var hdt = new HD_TICKET_ViewModel();
            foreach (var item in filteredTickets)
            {
                hdt.TICKET_ID = item.TICKET_ID;
                // Added by Harish FHANumber,PropertyId,PropertyName
                hdt.FHANumber = item.FHANumber;
                hdt.PropertyId = item.PropertyId;
                hdt.PropertyName = item.PropertyName;
                hdt.STATUS = item.STATUS;
                hdt.ISSUE = item.ISSUE;
                hdt.DESCRIPTION = item.DESCRIPTION;
                hdt.CATEGORY = item.CATEGORY;
                hdt.PRIORITY = item.PRIORITY;
                hdt.RECREATION_STEPS = item.RECREATION_STEPS;
                hdt.CONVERSATION = item.CONVERSATION;
                hdt.CONVERSATION = item.CONVERSATION;
                hdt.CREATED_ON = item.CREATED_ON;
                hdt.ASSIGNED_TO = item.ASSIGNED_TO;
                //hdt.ASSIGNED_TO_USERNAME = assigneduseremail;
                hdt.ASSIGNED_DATE = item.ASSIGNED_DATE;
                hdt.LAST_MODIFIED_BY = item.LAST_MODIFIED_BY;
                hdt.LAST_MODIFIED_ON = item.LAST_MODIFIED_ON;
                //29082019
                item.CATEGORY_TEXT = _hdtktmngr.getCategoryById((int)item.CATEGORY);
                item.SUBCATEGORY_TEXT = _hdtktmngr.getSubCategoryById((int)item.SUBCATEGORY);
                // commented by harish and naveen 19092019
                //if (item.STATUS == 0 || item.STATUS == 1)
                //{
                //    TimeSpan ts = DateTime.Now - item.CREATED_ON;
                //    item.OPENEDDAYS = ts.Days.ToString();
                //}
                //siddu #825 @10042020
                //int ts = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.Now);
                int ts = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.UtcNow);
                item.OPENEDDAYS = ts.ToString();
                //siddu #825 @10042020
                if (item.STATUS == 2 || item.STATUS == 3 || item.STATUS == 4)
                {
                    int ts1 = GetNumberOfWorkingDays(item.CREATED_ON, item.LAST_MODIFIED_ON.Value.Date);
                    item.OPENEDDAYS = ts1.ToString();
                }
                //siddu #825 @10042020
                if (item.STATUS == 5 || item.STATUS == 6)
                {
                    //int ts1 = GetNumberOfWorkingDays(item.LAST_MODIFIED_ON.Value.Date, DateTime.Now);
                    int ts1 = GetNumberOfWorkingDays(item.ASSIGNED_DATE.Value.Date, DateTime.UtcNow);
                    //item.Numberofdays = ts1;
                    //item.Numberofweeks = item.Numberofdays / 7;
                    item.OPENEDDAYS = ts1.ToString();
                }
                //TimeSpan ts = DateTime.Now - item.CREATED_ON;
                //item.OPENEDDAYS = ts.Days.ToString();
            }
            //Added by siddu 290120//
            int totalrecords2 = filteredTickets.Count();
            var totalpages2 = (int)Math.Ceiling((float)totalrecords2 / (float)rows);
            var result2 = filteredTickets.Skip(pageIndex * pageSize).Take(pageSize);
            //int page = 1;
            var jsonData = new
            {
                total = totalpages2,
                pages,
                records = totalrecords2,
                rows = result2,

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTicketIDForDetails(int id)
        {
            bool isAdminUser = false;//Note: HUDAdmin exclusive role is removed now, he will have only HelpDesk1 role
            bool isHelpDesk1 = false;//HUD Help Desk team
            bool isOhterUser = false;
            bool isHelpDesk2 = false;//Pinieal solutions

            int loggedUserID = 0;//userid = 0;
            IdentifyUserRole(ref isAdminUser, ref isHelpDesk1, ref isOhterUser, ref isHelpDesk2, UserPrincipal.Current.UserId);
            //if (isAdminUser || isHelpDesk1 || isHelpDesk2)
            if (isHelpDesk1 || isHelpDesk2)
                loggedUserID = 0;
            else
                loggedUserID = UserPrincipal.Current.UserId;

            //HUDUpdates
            //var hdticketsGridData = _hdtktmngr.GetMyTickets(loggedUserID, id);
            var hdticketsGridData = _hdtktmngr.GetMyTickets(0, id);//forcing zero 0 to get all tickets and filter in them
                                                                   //Implemented by siddesh 01102019 Start//
                                                                   // conversation data
            string conversation_data = hdticketsGridData.First().CONVERSATION;
            if (conversation_data != null)
            {
                string[] arrConversation = conversation_data.Split(new string[] { "\n" }, StringSplitOptions.None);

                //karri11102019
                if (isOhterUser && arrConversation.Length > 0)
                    for (int i = 0; i < arrConversation.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(arrConversation[i]) && arrConversation[i].Contains(">>"))
                        {
                            int iNDX = arrConversation[i].IndexOf(">>");
                            string st1 = arrConversation[i].Substring(0, iNDX);
                            string st2 = arrConversation[i].Substring(iNDX);
                            arrConversation[i] = "Help Desk" + st2;
                        }

                        if (!string.IsNullOrEmpty(arrConversation[i]) && arrConversation[i].Contains(">> has assigned this ticket to"))
                            arrConversation[i] = "Help Desk>> has assigned this ticket";
                        if (!string.IsNullOrEmpty(arrConversation[i]) && arrConversation[i].Contains(">> has reassigned this ticket to"))
                            arrConversation[i] = "Help Desk>> has reassigned this ticket";

                        if (!string.IsNullOrEmpty(arrConversation[i])
                            && !arrConversation[i].Contains("HUD Help Desk has reassigned this ticket to PSL   Support  Team")
                            && !arrConversation[i].Contains("HUD Help Desk has assigned this ticket to PSL   Support  Team")
                            && arrConversation[i].Contains("has assigned this ticket to")
                            )
                            arrConversation[i] = "Help Desk has assigned this ticket";

                        if (!string.IsNullOrEmpty(arrConversation[i])
                            && !arrConversation[i].Contains("HUD Help Desk has reassigned this ticket to PSL   Support  Team")
                            && !arrConversation[i].Contains("HUD Help Desk has assigned this ticket to PSL   Support  Team")
                            && arrConversation[i].Contains("has reassigned this ticket to")
                            )
                            arrConversation[i] = "Help Desk has reassigned this ticket";

                        if (!string.IsNullOrEmpty(arrConversation[i])
                            && !arrConversation[i].Contains("HUD Help Desk has reassigned this ticket to PSL   Support  Team")
                            && !arrConversation[i].Contains("HUD Help Desk has assigned this ticket to PSL   Support  Team")
                            && arrConversation[i].Contains("has set this ticket Priority to")
                            )
                        {
                            int iNDX = arrConversation[i].IndexOf("has set this ticket Priority to");
                            string st1 = arrConversation[i].Substring(0, iNDX);
                            string st2 = arrConversation[i].Substring(iNDX);
                            arrConversation[i] = "Help Desk>>" + st2;
                        }

                        if (!string.IsNullOrEmpty(arrConversation[i])
                            && !arrConversation[i].Contains("HUD Help Desk has reassigned this ticket to PSL   Support  Team")
                            && !arrConversation[i].Contains("HUD Help Desk has assigned this ticket to PSL   Support  Team")
                            && arrConversation[i].Contains("has set this Status to")
                            )
                        {
                            int iNDX = arrConversation[i].IndexOf("has set this Status to");
                            string st1 = arrConversation[i].Substring(0, iNDX);
                            string st2 = arrConversation[i].Substring(iNDX);
                            arrConversation[i] = "Help Desk>>" + st2;
                        }

                        //April Edmunds : COMMENTS: Please re-upload your batchfile sheet..
                        if (!string.IsNullOrEmpty(arrConversation[i])
                            && !arrConversation[i].Contains("HUD Help Desk has reassigned this ticket to PSL   Support  Team")
                            && !arrConversation[i].Contains("HUD Help Desk has assigned this ticket to PSL   Support  Team")
                            && arrConversation[i].Contains("April Edmunds : COMMENTS:")
                            )
                        {
                            int iNDX = arrConversation[i].IndexOf("April Edmunds : COMMENTS:");
                            string st1 = arrConversation[i].Substring(0, iNDX);
                            string st2 = arrConversation[i].Substring(iNDX);
                            arrConversation[i] = "Help Desk>> Comments:" + st2;
                        }

                    }

                arrConversation = arrConversation.Reverse().ToArray();
                StringBuilder sbConversation = new StringBuilder();
                for (int intCnt = 0; intCnt <= (arrConversation.Length / 2) - 1; intCnt++)
                {
                    sbConversation.Append(arrConversation[(intCnt * 2) + 1]);
                    sbConversation.Append("\n");
                    sbConversation.Append(arrConversation[intCnt * 2]);
                    sbConversation.Append("\n");
                }
                if (arrConversation.Length % 2 != 0)
                {
                    sbConversation.Append(arrConversation[arrConversation.Length - 1]);
                }
                hdticketsGridData.First().CONVERSATION = sbConversation.ToString();
            }
            //Implemented by siddesh 01102019  End//
            string assigneduserfullname = string.Empty;
            string assigneduseremail = string.Empty;
            //get assingned user, created user details
            //Note: filteration if assigned or not assigned
            int assingneduserid = 0;
            if (hdticketsGridData.Count.Equals(1) && hdticketsGridData[0].ASSIGNED_TO != null)
            {
                assingneduserid = (int)hdticketsGridData[0].ASSIGNED_TO;
                assigneduserfullname = _accountManager.GetFullUserNameById(assingneduserid);
                assigneduseremail = _accountManager.GetUserNameById(Convert.ToInt32(assingneduserid));
            }

            var hdt = new HD_TICKET_ViewModel();

            foreach (var item in hdticketsGridData)
            {
                hdt.TICKET_ID = item.TICKET_ID;
                // Added by Harish FHANumber,PropertyId,PropertyName
                hdt.FHANumber = item.FHANumber;
                hdt.PropertyId = item.PropertyId;
                hdt.PropertyName = item.PropertyName;
                hdt.STATUS = item.STATUS;
                hdt.PREVIOUS_STATUS = item.STATUS;
                hdt.ISSUE = item.ISSUE;
                hdt.DESCRIPTION = item.DESCRIPTION;
                hdt.CATEGORY = item.CATEGORY;
                hdt.PRIORITY = item.PRIORITY;
                hdt.PREVIOUS_PRIORITY = item.PRIORITY;
                hdt.RECREATION_STEPS = item.RECREATION_STEPS;
                hdt.CONVERSATION = item.CONVERSATION;
                hdt.CREATED_ON = item.CREATED_ON;
                hdt.ASSIGNED_TO = item.ASSIGNED_TO;//userid int
                hdt.PREVIOUS_ASSIGNED_TO = item.ASSIGNED_TO;
                hdt.ASSIGNED_DATE = item.ASSIGNED_DATE;
                hdt.PREVIOUS_ASSIGNMENTDATE = item.ASSIGNED_DATE;
                hdt.LAST_MODIFIED_BY = item.LAST_MODIFIED_BY;
                hdt.LAST_MODIFIED_ON = item.LAST_MODIFIED_ON;
                hdt.CREATED_BY_USERFIRSTNAME = item.CREATED_BY_USERFIRSTNAME;
                hdt.CREATED_BY_USERLASTNAME = item.CREATED_BY_USERLASTNAME;
                hdt.CREATED_BY_USERDISPLAYNAME = item.CREATED_BY_USERFIRSTNAME + " " + item.CREATED_BY_USERLASTNAME + "(" + item.ROLENAME + ")";
                hdt.CREATED_BY_USERNAME = item.CREATED_BY_USERNAME;
                hdt.ASSIGNED_TO_USERDISPLAYNAME = assigneduserfullname;//need to hide the assigned users email(as it is yitsol, if pslsolutions, then ok)
                hdt.ONACTION = item.ONACTION;
                hdt.ROLENAME = item.ROLENAME;
                hdt.ASSIGNED_TO_USERNAME = assigneduseremail;
                hdt.ASSIGNED_TO_USERDISPLAYNAME = assigneduserfullname;
                hdt.SUBCATEGORY = item.SUBCATEGORY;//Added by siddu @18052020
                hdt.CREATED_BY = item.CREATED_BY;
            }


            //NOTE: to fill Hud Tech Team details in the Assignee drop down list, we extract details here
            //HUD TECH TEAM MEMBER DETAILS EXTRACTION
            int iTechTeamUserid = 0;
            string sTechTeamUserName = string.Empty;
            SelectListItem obj = null;
            bool isTechTeamFoundInMetaData = false;

            DataTable dtTechTeamUser = _hdtktmngr.getTechDeskUsers();

            if (dtTechTeamUser != null && dtTechTeamUser.Rows.Count > 0)
            {
                iTechTeamUserid = Convert.ToInt32(dtTechTeamUser.Rows[0]["Value"].ToString());
                sTechTeamUserName = dtTechTeamUser.Rows[0]["Text"].ToString();

                if (!string.IsNullOrEmpty(_accountManager.GetUserNameById(iTechTeamUserid))
                    && sTechTeamUserName.Equals(_accountManager.GetUserNameById(iTechTeamUserid)))
                {
                    obj = new SelectListItem();
                    obj.Text = _accountManager.GetFullUserNameById(iTechTeamUserid);
                    obj.Value = iTechTeamUserid.ToString();
                    _hdAdminUsersList.Add(obj);

                    //this key is used to stop editing by HelpDesk1 if the ticket is assigned to TechTeam
                    ViewData["TechDesk"] = obj.Value;

                    obj = null;
                    isTechTeamFoundInMetaData = true;
                }
                else
                    isTechTeamFoundInMetaData = false;
            }
            else
                isTechTeamFoundInMetaData = false;


            if (!isTechTeamFoundInMetaData)
            {
                //get all the users whose role is TechDesk
                //get all the users whose role is HelpDesk from Meta Data File xml, combine all and display             
                var modelHudHelpDeskUsers = _accountManager.GetDataAdminManageUser(null, 1, "UserName", HUDHealthcarePortal.Model.SqlOrderByDirecton.ASC);
                //SelectListItem obj = null;
                foreach (var item in modelHudHelpDeskUsers)
                {
                    if (item.RoleName != null && item.RoleName.Equals("TechDesk"))
                    {
                        obj = new SelectListItem();
                        //obj.Text = item.FirstName + "" + item.LastName + "(" + item.UserName + ")";
                        obj.Text = item.FirstName + " " + item.MiddleName + " " + item.LastName;
                        obj.Value = item.UserID.ToString();
                        _hdAdminUsersList.Add(obj);

                        //this key is used to stop editing by HelpDesk1 if the ticket is assigned to TechTeam
                        ViewData["TechDesk"] = obj.Value;

                        obj = null;
                    }

                }
            }

            //add from medta data xml file
            DataTable dt = _hdtktmngr.getHelpDesk1Users();
            foreach (DataRow dr in dt.Rows)
            {
                obj = new SelectListItem();
                //for each HelpDesk users, get his full name

                //#479
                if (dt.Columns.Contains("DispName")
                    && dr["DispName"] != null && !string.IsNullOrEmpty(dr["DispName"].ToString()))
                    obj.Text = dr["DispName"].ToString();
                else
                    obj.Text = _accountManager.GetFullUserNameById(Convert.ToInt32(dr["Value"].ToString()));

                obj.Value = dr["Value"].ToString();
                _hdAdminUsersList.Add(obj);
                obj = null;
            }

            foreach (var hudHDAdminUser in _hdAdminUsersList)
                hdt.TicketAsigneeList.Add(hudHDAdminUser);

            DataTable dtStatus = _hdtktmngr.getStatusTbl();
            DataTable dtCategory = _hdtktmngr.getCateogryTbl();
            DataTable dtSubCategory = _hdtktmngr.getSubCategoryTbl();
            DataTable dtOnAction = _hdtktmngr.getOnActionTbl();
            DataTable dtPriority = _hdtktmngr.getPriorityTbl();

            ////Note:HD_TICKET_ViewModel class should have a constructur instantiating these child classes like 
            foreach (DataRow dr in dtStatus.Rows)
                hdt.TicketStatusList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtCategory.Rows)
                hdt.TicketCategoryList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtSubCategory.Rows)
                hdt.TicketSubCategoryList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtOnAction.Rows)
                hdt.TicketOnActionList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            foreach (DataRow dr in dtPriority.Rows)
                hdt.TicketPriorityList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });

            //get the files from the folder where ticet id is created
            string filepath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + hdt.TICKET_ID.ToString());
            //get all the files here
            if (Directory.Exists(filepath))
            {
                var files = Directory.GetFiles(filepath);

                foreach (var iFile in files)
                {
                    if (string.IsNullOrEmpty(hdt.File1))
                    {
                        string[] name = iFile.ToString().ToUpper().Split(new Char[] { '\\' });
                        hdt.File1 = name[name.Length - 1];
                        continue;
                    }

                    if (string.IsNullOrEmpty(hdt.File2))
                    {
                        string[] name = iFile.ToString().ToUpper().Split(new Char[] { '\\' });
                        hdt.File2 = name[name.Length - 1];
                        continue;
                    }

                    if (string.IsNullOrEmpty(hdt.File3))
                    {
                        string[] name = iFile.ToString().ToUpper().Split(new Char[] { '\\' });
                        hdt.File3 = name[name.Length - 1];
                        continue;
                    }

                    if (string.IsNullOrEmpty(hdt.File4))
                    {
                        string[] name = iFile.ToString().ToUpper().Split(new Char[] { '\\' });
                        hdt.File4 = name[name.Length - 1];
                        continue;
                    }
                    if (string.IsNullOrEmpty(hdt.File5))
                    {
                        string[] name = iFile.ToString().ToUpper().Split(new Char[] { '\\' });
                        hdt.File5 = name[name.Length - 1];
                        continue;
                    }
                    if (string.IsNullOrEmpty(hdt.File6))
                    {
                        string[] name = iFile.ToString().ToUpper().Split(new Char[] { '\\' });
                        hdt.File6 = name[name.Length - 1];
                        continue;
                    }
                }
            }

            //Note: these values if to be persisting has to bind to hidden control on the POST page for reusing
            //save the ticket status and assigned date in the global variable
            hdt.PREVIOUS_STATUS = hdt.STATUS;
            if (!string.IsNullOrEmpty(hdt.ASSIGNED_DATE.ToString()))
                hdt.PREVIOUS_ASSIGNMENTDATE = Convert.ToDateTime(hdt.ASSIGNED_DATE);
            if (!hdt.ASSIGNED_TO.Equals(null) && !hdt.ASSIGNED_TO.Equals(0))
                hdt.PREVIOUS_ASSIGNED_TO = Convert.ToInt32(hdt.ASSIGNED_TO);

            ViewData["PreviousAssignee"] = Convert.ToInt32(hdt.ASSIGNED_TO);

            //set the model property for the coming view to set the control properties
            //if (isHelpDesk1)
            //    hdt.IS_HELPDESK_USER = true;

            //for ticket owner and his group the view is diferent
            if (isOhterUser)
                //return PartialView("HudUser_ViewHDTktDetails", hdt);
                return View("HudUser_ViewHDTktDetails", hdt);

            //for HUD HelpDesk seperate view
            if (isAdminUser)
                return View("HudAdmin_HDTktDetails", hdt);//only read only

            //for HUD HelpDesk Level2 seperate view
            if (isHelpDesk1)
                return View("HudHelpDesk1_ViewHDTktDetails", hdt);

            if (isHelpDesk2)
                return View("HudHelpDesk2_ViewHDTktDetails", hdt);

            //return View();
            return View("HudAdmin_HDTktDetails", hdt); //only read only
        }
        // #283Hareesh
        //uploaidng files

        [HttpPost]
        public JsonResult UploadFile(string filepathUniqueID, string UploadedFileSection, string ticketid = null)
        {
            bool isFileFound = true;
            string newPath = string.Empty;
            try
            {
                foreach (string Filename in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[Filename];

                    //hudenhancements3
                    //before creating ticket we create userid_uniquenumber folder and append File1_Filename.pdf,
                    if (ticketid == null)
                    {
                        newPath = UserPrincipal.Current.UserId + "_" + filepathUniqueID;

                        newPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + newPath);
                    }
                    //after creating ticket ticketid  folder and remove filesection text from File1_Filename.pdf,
                    else if (ticketid != null && ticketid.Length > 0)
                    {
                        newPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + ticketid);
                    }

                    if (file.ContentLength > 0 && this.CreateFolderIfNeeded(newPath))
                    {
                        string _FileName = Path.GetFileName(file.FileName);
                        string _path = string.Empty;

                        //before creating ticket we create userid_uniquenumber folder and append File1_Filename.pdf,
                        if (ticketid == null)
                            _path = Path.Combine(newPath, UploadedFileSection + "_" + _FileName);

                        //after creating ticket ticketid  folder and remove filesection text from File1_Filename.pdf,
                        if (ticketid != null && ticketid.Length > 0)
                            _path = Path.Combine(newPath, _FileName);

                        file.SaveAs(_path);
                    }
                }

                ViewBag.Message = "File Uploaded Successfully!!";
                return Json(new { returnvalue = isFileFound }, JsonRequestBehavior.AllowGet);
            }
            catch
            {

                ViewBag.Message = "File upload failed!!";
                return Json(new { returnvalue = isFileFound }, JsonRequestBehavior.AllowGet);
            }
        }

        /*
        //uploaidng files
        [HttpPost]
        public ActionResult UploadFile(string filepathUniqueID, string UploadedFileSection)
        {
            string newPath = string.Empty;
            try
            {
                foreach (string Filename in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[Filename];

                    newPath = UserPrincipal.Current.UserId + "_" + filepathUniqueID;

                    newPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + newPath);

                    if (file.ContentLength > 0 && this.CreateFolderIfNeeded(newPath))
                    {
                        string _FileName = Path.GetFileName(file.FileName);
                        string _path = Path.Combine(newPath, UploadedFileSection + "_" + _FileName);
                        file.SaveAs(_path);
                    }
                }

                ViewBag.Message = "File Uploaded Successfully!!";
                return View();
            }
            catch
            {

                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }
        */
        /*
        [HttpPost]
        public ActionResult DeleteFile(string filepathUniqueID, string FileSectionName)
        {
            string newPath = string.Empty;

            try
            {

                newPath = UserPrincipal.Current.UserId + "_" + filepathUniqueID;

                newPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + newPath);

                var files = Directory.GetFiles(newPath);

                foreach (var iFile in files)
                {
                    if (iFile.ToString().ToUpper().Contains(FileSectionName.ToUpper()))
                    {
                        System.IO.File.Delete(iFile);

                    }

                }

                ViewBag.Message = "File delted Successfully!!";
                return View();
            }
            catch (Exception ex)
            {

                ViewBag.Message = "File deletion failed!!";
                return View();
            }

        }
        */

        // #283 Hareesh
        //enhancements3
        [HttpPost]
        public JsonResult DeleteFile(string filepathUniqueID, string FileSectionName, string ticketid = null, string deletefilename = null)
        {
            bool isFileFound = true;
            string newPath = string.Empty;

            try
            {

                newPath = UserPrincipal.Current.UserId + "_" + filepathUniqueID;

                if (ticketid == null && deletefilename == null)
                    newPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + newPath);

                if (ticketid != null && ticketid.Length > 0 && deletefilename != null && deletefilename.Length > 0)
                {
                    //string a = @"c:\\temp\\file1_abce.pdf";
                    //int i1 = a.LastIndexOf("\\");
                    //a = a.Substring(i1 + 1);
                    deletefilename = deletefilename.Substring(deletefilename.LastIndexOf("\\") + 1);
                    newPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + ticketid + "/" + deletefilename);

                    if (System.IO.File.Exists(newPath))
                        System.IO.File.Delete(newPath);
                }

                if (ticketid == null && deletefilename == null)
                {
                    var files = Directory.GetFiles(newPath);

                    foreach (var iFile in files)
                    {
                        if (iFile.ToString().ToUpper().Contains(FileSectionName.ToUpper()))
                        {
                            if (ticketid == null)
                                System.IO.File.Delete(iFile);

                        }

                    }
                }

                ViewBag.Message = "File delted Successfully!!";
                return Json(new { returnvalue = isFileFound }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                ViewBag.Message = "File deletion failed!!";
                return Json(new { returnvalue = isFileFound }, JsonRequestBehavior.AllowGet);
            }

        }




        //createsa folder if not existing during attachments upload
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        [HttpGet]
        public FileResult DownloadTaskFile(string taskInstanceId, int ticketid)
        {
            //Note:taskInstanceId contains only the file name but not path, we have to build the path
            string newpath1 = string.Empty;

            string folder = ticketid.ToString();
            newpath1 = Server.MapPath("~/UploadedHelpDeskTktFiles/" + folder + "/" + taskInstanceId);

            string filePath = newpath1;
            var fileInfo = new System.IO.FileInfo(filePath);
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition",
                String.Format("attachment;filename=\"{0}\"", taskInstanceId));
            Response.AddHeader("Content-Length", fileInfo.Length.ToString());
            Response.WriteFile(filePath);
            Response.End();
            return null;
        }

        //UploadR4RFiles
        public ActionResult UploadR4RFiles()
        {
            return View("R4RUploadsView");
        }

        [HttpPost]
        public ActionResult R4RUploadFile(string UploadedFileSection)
        {
            string newPath = string.Empty;
            try
            {
                foreach (string Filename in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[Filename];

                    string srvrpath = @"c:\r4rUploadFiles";


                    if (file.ContentLength > 0)
                    {

                        file.SaveAs(srvrpath + @"\" + UploadedFileSection + "_" + file.FileName);
                    }
                }

                ViewBag.Message = "File Uploaded Successfully!!";
                return View();
            }
            catch
            {

                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }

        //Naveen 28-08
        private List<HD_TICKET_ViewModel> FilterDataByStatusAndCategory(List<HD_TICKET_ViewModel> obj, int CategoryType, int statusType, int priorityType, int tktid, string UName, string FromDate, string ToDate, string HelpDeskName)
        {
            var filteredTickets = obj.ToList();
            //select all of category types and all of status types
            if (CategoryType == -1 && statusType == -1 && priorityType == -1 && tktid == -1 && string.IsNullOrEmpty(UName) && (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate)) && string.IsNullOrEmpty(HelpDeskName))
            {
                return obj;
            }
            //select only status types
            if (/*CategoryType == -1 &&*/ statusType != -1 /*&& priorityType == -1 && tktid == -1*/)
            {
                filteredTickets = filteredTickets.FindAll(x => x.STATUS == statusType);

                //return filteredTickets;
            }
            if (CategoryType != -1/* && statusType != -1 && priorityType == -1 && tktid == -1*/)
            {
                filteredTickets = filteredTickets.FindAll(x => x.CATEGORY == CategoryType);
                //return filteredTickets12;
            }
            if (/*CategoryType != -1 && statusType != -1 &&*/ priorityType != -1/* && tktid == -1*/)
            {
                filteredTickets = filteredTickets.FindAll(x => x.PRIORITY == priorityType);
                //return filteredTickets13;
            }
            if (/*CategoryType != -1 && statusType != -1 && priorityType != -1 && */tktid != -1)
            {
                filteredTickets = filteredTickets.FindAll(x => /*x => x.STATUS == statusType && x.CATEGORY == CategoryType && x.PRIORITY == priorityType && */x.TICKET_ID == tktid);
                //return filteredTickets13;
            }
            if (!string.IsNullOrEmpty(UName))
            {
                filteredTickets = filteredTickets.FindAll(x => x.CREATED_BY_USERNAME == UName);
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                filteredTickets = filteredTickets.FindAll(x => x.ASSIGNED_DATE >= Convert.ToDateTime(FromDate) && x.ASSIGNED_DATE <= Convert.ToDateTime(ToDate));
            }

            //Added by siddesh 14102019//
            if (!string.IsNullOrEmpty(HelpDeskName))
            {
                filteredTickets = filteredTickets.FindAll(x => x.ASSIGNED_TO_USERNAME == HelpDeskName);
            }
            ////if both category and status is given
            //var filteredTickets = obj.FindAll(x => x.CATEGORY == CategoryType && x.STATUS == statusType && x.PRIORITY == priorityType);
            return filteredTickets;
        }

        //HDNOTIFICATIONS
        public async Task<JsonResult> getMyHDNotifications()
        {
            ArrayList notids = new ArrayList();

            var data1 = await _hdtktnotifmngr.getMyUnReadNotifications(UserPrincipal.Current.UserId);
            StringBuilder sbNotifictions = new StringBuilder();

            sbNotifictions.Append("Hello " + UserPrincipal.Current.FullName + "! Your Help Desk Notifications below.\n");

            bool isNotificationsFound = false;
            foreach (var item in data1)
            {
                //sbNotifictions.Append("\nTicket ID:" + item.TICKET_ID + ": " + item.MESSAGE);
                sbNotifictions.Append("\nTitle:" + item.TITLE.Trim() + "; QueryID:" + item.TICKET_ID + "; Notification:" + item.MESSAGE);
                notids.Add(item.NOTIFICATIONID);
            }

            if (notids.Count > 0)
            {
                _hdtktnotifmngr.updateNotificationStatus(notids);
                isNotificationsFound = true;
            }

            //string myData = "hello hareesh";
            if (isNotificationsFound)
                return Json(new { returnvalue = true, data = sbNotifictions.ToString() }, JsonRequestBehavior.AllowGet);

            return Json(new { returnvalue = true, data = "" }, JsonRequestBehavior.AllowGet);

        }

        //HDNOTIFICATIONS     //29082019
        //[AccessDeniedAuthorize(Roles = "LenderAdmin,LenderAccountManager,BackupAccountManager,LenderAccountRepresentative,AccountExecutive,WorkflowManager,Attorney,InternalSpecialOptionUser,ProductionUser,ProductionWlm,Reviewer")]
        public ActionResult MyNotifications()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetMyNotifications()
        {
            //int pageIndex = Convert.ToInt32(page) - 1;
            //int pageSize = rows;
            // Naveen & Harish changes 18092019
            bool isAdminUser = false;
            bool isHelpDesk1 = false;
            bool isOhterUser = false;
            bool isHelpDesk2 = false;
            IdentifyUserRole(ref isAdminUser, ref isHelpDesk1, ref isOhterUser, ref isHelpDesk2, UserPrincipal.Current.UserId);

            var hdnotifications = _hdtktnotifmngr.getMyNotifications(UserPrincipal.Current.UserId, isHelpDesk1);
            //int totalrecods = hdnotifications.Count();
            //var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            //var results = hdnotifications.Skip(pageIndex * pageSize).Take(pageSize);
            int page1 = 1;
            var jsonData2 = new
            {
                total = 1,
                page1,
                records = 10,
                rows = hdnotifications.OrderByDescending(x => x.LOGDATE),

            };

            return Json(jsonData2, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "HUDAdmin")]
        public ActionResult GetHelpDeskReports()
        {

            var hdt = new HD_TICKET_ViewModel();
            //IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActionsByPageId(3);

            //Note:HD_TICKET_ViewModel class should have a constructur instantiating these child classes like 
            //TicketStatusList, TicketCategoryList for them to work with
            hdt.TicketStatusList.Add(new SelectListItem() { Text = "OPEN", Value = "0" });
            hdt.TicketStatusList.Add(new SelectListItem() { Text = "INPROGRESS", Value = "1" });
            hdt.TicketStatusList.Add(new SelectListItem() { Text = "REJECTED", Value = "2" });
            hdt.TicketStatusList.Add(new SelectListItem() { Text = "CLOSED", Value = "3" });
            hdt.TicketStatusList.Add(new SelectListItem() { Text = "RESOLVED", Value = "4" });
            //Added by siddu @02122019
            hdt.TicketStatusList.Add(new SelectListItem() { Text = "REQUIRED ADDITIONAL INFORMATION", Value = "5" });
            //Added by siddu @02122019
            hdt.TicketStatusList.Add(new SelectListItem() { Text = "DEVELOPMENT", Value = "6" });

            hdt.TicketCategoryList.Add(new SelectListItem() { Text = "Data Issue", Value = "1" });
            hdt.TicketCategoryList.Add(new SelectListItem() { Text = "Technical", Value = "2" });
            hdt.TicketCategoryList.Add(new SelectListItem() { Text = "Functional", Value = "3" });
            string sdate = @"8/01/2019";
            hdt.REPORT_START_DATE = Convert.ToDateTime(sdate);
            //hdt.REPORT_END_DATE = DateTime.Now;
            hdt.REPORT_END_DATE = DateTime.UtcNow;


            //Added by siddesh 14102019//
            bool isAdminUser = false;//Note: there is no HUAdmin extra functionality, HUAdmin will only have HelpDesk1 role from now
            bool isHlepDesk1 = false;
            bool isOhterUser = false;
            bool isHlepDesk2 = false;

            int loggedUserID = 0;//userid = 0;
            IdentifyUserRole(ref isAdminUser, ref isHlepDesk1, ref isOhterUser, ref isHlepDesk2, UserPrincipal.Current.UserId);

            if (isHlepDesk1 || isHlepDesk2)
                loggedUserID = 0;
            else
                loggedUserID = UserPrincipal.Current.UserId;
            //Added by siddesh 11102019
            if (isHlepDesk1 || isHlepDesk2)
            {

                var hdticketsGridData5 = _hdtktmngr.GetMyTickets(loggedUserID, isHlepDesk1, isHlepDesk2, UserPrincipal.Current.UserId).Select(o => new { o.ASSIGNED_TO_USERNAME }).Distinct();

                //int page1 = 1;

                foreach (var item in hdticketsGridData5)
                {
                    if (!string.IsNullOrEmpty(item.ASSIGNED_TO_USERNAME.ToString()))
                        hdt.TicketHelpdesknameList.Add(new SelectListItem() { Text = item.ASSIGNED_TO_USERNAME, Value = item.ASSIGNED_TO_USERNAME });
                }

            }

            //return View("~/Views/HelpDesk/GetMyTickets.cshtml", hdticketsGridData);
            return View(hdt);
            //eturn View();
        }

        // added hareesh
        /*
        public JsonResult GetHelpdeskReportsView()
        {

            var model = _hdtktmngr.GetAllReportsTickets();

            List<HD_TICKET_ViewModel> lstData = new List<HD_TICKET_ViewModel>();

            foreach (var item in model)
            {
                var hdt = new HD_TICKET_ViewModel();
                hdt.TICKET_ID = item.TICKET_ID;
                // Added By Harish FHANumber,PropertyId,PropertyName
                hdt.FHANumber = item.FHANumber;
                hdt.PropertyId = item.PropertyId;
                hdt.PropertyName = item.PropertyName;
                hdt.STATUS = item.STATUS;
                hdt.ISSUE = item.ISSUE;
                hdt.DESCRIPTION = item.DESCRIPTION;
                hdt.CATEGORY = item.CATEGORY;
                hdt.PRIORITY = item.PRIORITY;
                hdt.RECREATION_STEPS = item.RECREATION_STEPS;
                hdt.CONVERSATION = item.CONVERSATION;
                hdt.CONVERSATION = item.CONVERSATION;
                hdt.CREATED_ON = item.CREATED_ON;
                hdt.ASSIGNED_TO = item.ASSIGNED_TO;
                hdt.ASSIGNED_DATE = item.ASSIGNED_DATE;
                hdt.LAST_MODIFIED_BY = item.LAST_MODIFIED_BY;
                hdt.LAST_MODIFIED_ON = item.LAST_MODIFIED_ON;

                lstData.Add(hdt);
            }
            //  IEnumerable <HD_TICKET_ViewModel> = (from q in model select

            //  List<HD_TICKET_ViewModel> htvm =  new HD_TICKET_ViewModel
            //  {
            //      TICKET_ID = model.TICKET_ID
            //,
            //      STATUS = a.STATUS
            //,
            //      ISSUE = a.ISSUE
            //,
            //      DESCRIPTION = a.DESCRIPTION
            //,
            //      CATEGORY = a.CATEGORY
            ////,
            ////     SUBCATEGORY = a.SUBCATEGORY
            //,
            //      PRIORITY = a.PRIORITY
            //,
            //      RECREATION_STEPS = a.RECREATION_STEPS
            //,
            //      CONVERSATION = a.CONVERSATION
            //,
            //      CREATED_BY = a.CREATED_BY
            //,
            //      CREATED_ON = a.CREATED_ON
            //,
            //      ASSIGNED_TO = a.ASSIGNED_TO
            //,
            //      ASSIGNED_DATE = a.ASSIGNED_DATE
            //,
            //      LAST_MODIFIED_BY = a.LAST_MODIFIED_BY
            //,
            //      LAST_MODIFIED_ON = a.LAST_MODIFIED_ON,

            //      //CREATED_BY_USERNAME = a.CREATED_BY_USERNAME
            //  }).ToList();


            int page2 = 1;
            var jsonData2 = new
            {
                total = 1,
                page2,
                records = 10,
                rows = lstData,

            };

            return Json(jsonData2, JsonRequestBehavior.AllowGet);

            //return View();
            //return Json(hdt, JsonRequestBehavior.AllowGet);
            //return View("",model);
        }
        */
        //written by siddesh  29082019 #344//
        public JsonResult GetHelpdeskReportsViews(string status = null, string categorytype = null, string FromDate = null, string ToDate = null, string HelpDeskName = null)
        {
            if (status == "Select Status Type")
                status = null;
            TempData["Status"] = status;
            TempData["FromDate"] = FromDate;
            TempData["ToDate"] = ToDate;
            TempData["HelpDeskName"] = HelpDeskName;

            return FilterHelpDeskReportTicket(status, FromDate, ToDate, HelpDeskName);
        }
        //written by siddesh  29082019 #344//
        public JsonResult FilterHelpDeskReportTicket(string Status = null, string FromDate = null, string ToDate = null, string HelpDeskName = null)
        {
            //int cType = -1;
            //if (categorytype != null || !string.IsNullOrEmpty(categorytype))
            //    cType = Convert.ToInt32(categorytype);

            //int sType = -1;
            //if (status != null || !string.IsNullOrEmpty(status))
            //    sType = Convert.ToInt32(status);

            //bool isAdminUser = false;
            //IdentifyUserRole1(isAdminUser, UserPrincipal.Current.UserId);
            //if (isAdminUser)
            //{
            var hdticketsGridData1 = _hdtktmngr.GetAllReportsTickets();

            var filteredTickets1 = HelpdeskReportsFilterDataByStatusAndCategory(hdticketsGridData1, Status, FromDate, ToDate, HelpDeskName);
            // var hdt1 = new HD_TICKET();
            var hdt1 = new usp_HCP_GetTicketDetails_Latest();
            foreach (var item in filteredTickets1)
            {
                hdt1.TICKET_ID = item.TICKET_ID;
                // added by Harish FHANumber,PropertyId,PropertyName.
                hdt1.STATUS = item.STATUS;
                hdt1.ISSUE = item.ISSUE;
                hdt1.DESCRIPTION = item.DESCRIPTION;
                hdt1.CATEGORY = item.CATEGORY;
                hdt1.PRIORITY = item.PRIORITY;
                hdt1.RECREATION_STEPS = item.RECREATION_STEPS;
                hdt1.CONVERSATION = item.CONVERSATION;
                hdt1.CONVERSATION = item.CONVERSATION;
                hdt1.CREATED_ON = item.CREATED_ON;
                hdt1.ASSIGNED_TO = item.ASSIGNED_TO;
                hdt1.ASSIGNED_DATE = item.ASSIGNED_DATE;
                hdt1.LAST_MODIFIED_BY = item.LAST_MODIFIED_BY;
                hdt1.LAST_MODIFIED_ON = item.LAST_MODIFIED_ON;
                hdt1.Helpdesk_name = item.Helpdesk_name;
                //siddu #825 @10042020
                //item.Numberofdays = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.Now);
                item.Numberofdays = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.UtcNow);
                item.Numberofweeks = item.Numberofdays / 7;
                //siddu #825 @10042020
                if (item.STATUS == "Rejected" || item.STATUS == "Closed" || item.STATUS == "Resolved")
                {
                    int ts1 = GetNumberOfWorkingDays(item.CREATED_ON, item.LAST_MODIFIED_ON.Value.Date);
                    item.Numberofdays = ts1;
                    item.Numberofweeks = item.Numberofdays / 7;

                }
                //siddu #825 @10042020
                if (item.STATUS == "REQUIRED ADDITIONAL INFORMATION" || item.STATUS == "DEVELOPMENT")
                {
                    //int ts1 = GetNumberOfWorkingDays(item.LAST_MODIFIED_ON.Value.Date, DateTime.Now);
                    int ts1 = GetNumberOfWorkingDays(item.ASSIGNED_DATE.Value.Date, DateTime.UtcNow);
                    item.Numberofdays = ts1;
                    item.Numberofweeks = item.Numberofdays / 7;

                }

            }

            //var data = GetTicketsData(string status, string categorytype);
            int page1 = 1;
            var jsonData = new
            {
                total = 1,
                page1,
                records = 10,
                rows = filteredTickets1.OrderByDescending(x => x.TICKET_ID),

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        //written by siddesh  29082019 #344//
        private List<usp_HCP_GetTicketDetails_Latest> HelpdeskReportsFilterDataByStatusAndCategory(List<usp_HCP_GetTicketDetails_Latest> obj, string Status, string FromDate, string ToDate, string HelpDeskName)
        {
            // var filteredTickets = obj.ToList();
            if (!string.IsNullOrEmpty(FromDate))
            {
                obj = obj.FindAll(x => x.CREATED_ON >= Convert.ToDateTime(FromDate) && x.CREATED_ON <= Convert.ToDateTime(ToDate).AddDays(1));
            }
            //Added by siddesh 15102019//
            if (!string.IsNullOrEmpty(HelpDeskName))
            {
                obj = obj.FindAll(x => x.ASSIGNED_TO_USERNAME == HelpDeskName);
            }

            //select only status types
            //if ( !string.IsNullOrEmpty(Status))
            //{
            //     var filteredTickets1 = obj.FindAll(x => x.STATUS.ToLower() == Status.ToLower());
            //    return filteredTickets1;
            //}

            var statuslist = Status != null ? Status.Split(',') : new string[0];
            List<usp_HCP_GetTicketDetails_Latest> statuses = new List<usp_HCP_GetTicketDetails_Latest>();
            if (Status != null && statuslist.Any())
            {
                foreach (string item in statuslist)
                {
                    string statusname = "";
                    if (item == "0")
                    {
                        statusname = "OPEN";
                    }
                    if (item == "1")
                    {
                        statusname = "INPROGRESS";
                    }
                    if (item == "2")
                    {
                        statusname = "REJECTED";
                    }
                    if (item == "3")
                    {
                        statusname = "CLOSED";
                    }
                    if (item == "4")
                    {
                        statusname = "RESOLVED";
                    }
                    //Added by siddu@02122019
                    if (item == "5")
                    {
                        statusname = "REQUIRED ADDITIONAL INFORMATION";
                    }
                    //Added by siddu@02122019
                    if (item == "6")
                    {
                        statusname = "DEVELOPMENT";
                    }
                    if (!string.IsNullOrEmpty(Status))
                    {
                        var filteredTickets1 = obj.FindAll(x => x.STATUS.ToLower() == statusname.ToLower());
                        statuses.AddRange(filteredTickets1);
                        // return filteredTickets1;

                    }
                }

                return statuses;
            }


            var filteredTickets = obj;
            return filteredTickets;
        }
        //written by siddesh//
        public void IdentifyUserRole1(bool isAdminUser, int userID)
        {
            foreach (DictionaryEntry entry in _htLoggedUserRole)
            {
                if (Convert.ToBoolean(_htLoggedUserRole["HUDAdmin"].ToString()))
                    isAdminUser = true;
            }
        }
        //written by siddesh  29082019 #344//
        public JsonResult GetHelpdeskReportsView(string Status = null, string FromDate = null, string ToDate = null, string HelpDeskName = null)
        {
            TempData["Status"] = Status;
            TempData["FromDate"] = FromDate;
            TempData["ToDate"] = ToDate;

            var model = _hdtktmngr.GetAllReportsTickets();
            List<usp_HCP_GetTicketDetails_Latest> lstData = new List<usp_HCP_GetTicketDetails_Latest>();
            foreach (var item in model)
            {
                var hdt = new usp_HCP_GetTicketDetails_Latest();
                //Added ticket _id by siddesh//
                hdt.TICKET_ID = item.TICKET_ID;
                //siddu #825 @10042020
                //int ts = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.Now);
                int ts = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.UtcNow);
                hdt.Numberofdays = ts;
                hdt.Numberofweeks = hdt.Numberofdays / 7;
                hdt.STATUS = item.STATUS;
                hdt.CREATED_ON = item.CREATED_ON;
                hdt.LAST_MODIFIED_ON = item.LAST_MODIFIED_ON;
                hdt.ASSIGNED_TO = item.ASSIGNED_TO;
                hdt.ASSIGNED_TO_USERNAME = item.ASSIGNED_TO_USERNAME;
                hdt.Helpdesk_name = item.Helpdesk_name;
                //siddu #825 @10042020
                if (item.STATUS == "Rejected" || item.STATUS == "Closed" || item.STATUS == "Resolved")
                {
                    int ts1 = GetNumberOfWorkingDays(item.CREATED_ON, item.LAST_MODIFIED_ON.Value.Date);
                    item.Numberofdays = ts1;
                    item.Numberofweeks = item.Numberofdays / 7;
                }
                //siddu #825 @10042020
                if (item.STATUS == "REQUIRED ADDITIONAL INFORMATION" || item.STATUS == "DEVELOPMENT")
                {
                    //int ts1 = GetNumberOfWorkingDays(item.LAST_MODIFIED_ON.Value.Date, DateTime.Now);
                    int ts1 = GetNumberOfWorkingDays(item.ASSIGNED_DATE.Value.Date, DateTime.UtcNow);
                    item.Numberofdays = ts1;
                    item.Numberofweeks = item.Numberofdays / 7;
                }
                lstData.Add(hdt);
            }
            int page2 = 1;
            var jsonData2 = new
            {
                total = 1,
                page2,
                records = 10,
                rows = lstData.OrderByDescending(x => x.TICKET_ID),

            };

            return Json(jsonData2, JsonRequestBehavior.AllowGet);
            //return View();
            //return Json(hdt, JsonRequestBehavior.AllowGet);
            //return View("",model);
        }

        //private List<usp_HCP_GetTicketDetails_Latest> HelpdeskReportsFilterDataByStatusAndCategory(List<usp_HCP_GetTicketDetails_Latest> obj, int CategoryType, string Status, string FromDate, string ToDate)
        //{
        //    ////select all of category types and all of status types
        //    //if (CategoryType == -1 && statusType == -1)
        //    //{
        //    //    return obj;
        //    //}
        //    ////select only status types
        //    // if (CategoryType == -1 && statusType != -1)
        //    //{
        //        var filteredTickets1 = obj.FindAll(x => x.STATUS == Status);
        //        return filteredTickets1;
        //    //}
        //    ////select only category types
        //    //else if (CategoryType != -1 && statusType == -1)
        //    //{
        //    //    var filteredTickets2 = obj.FindAll(x => x.CATEGORY == CategoryType);
        //    //    return filteredTickets2;
        //    //}
        //    //if both category and status is given
        //    //var filteredTickets = obj.FindAll(x => x.CATEGORY == CategoryType && x.STATUS == statusType);
        //    //return filteredTickets;

        public JsonResult GetHelpdeskPropertyInfo(string selectedFhaNumber)
        {
            var result = reserveForReplacementManager.GetPropertyInfo(selectedFhaNumber);
            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        public void PopulatehelpdeskFHANumberList(HD_TICKET_ViewModel model)
        {
            var fhaNumberList = new List<string>();

            bool isIntUser = false;
            bool isExtUser = false;
            int userLenderId = 0;

            if (_hdtktmngr.getInternalUserIDByRoleName(UserPrincipal.Current.UserRole) > 0)
                isIntUser = true;
            else
            {
                userLenderId = UserPrincipal.Current.LenderId ?? 0;
                if (userLenderId > 0)
                    isExtUser = true;
            }

            if (!isIntUser && !isExtUser)
                throw new Exception("Not an Internal or Not an External user logged in!");


            //fhaNumberList =
            //    projectActionFormManager.GetAllowedFhaByLender().ToList();
            if (isExtUser)
            {
                fhaNumberList = projectActionFormManager.GetAllowedFhaByLenderdetails(UserPrincipal.Current.UserId);
            }
            else if (isIntUser)
            {
                //Note: show full fha numbers
                fhaNumberList = projectActionFormManager.GetAllowedFhaByLender().ToList();
            }


            model.AvailableFHANumbersList = fhaNumberList;
        }

        //written by siddesh  29082019 #344//
        public ActionResult ExportUploadStatusHelpdeskreports(string Status = null, string FromDate = null, string ToDate = null, string HelpDeskName = null)
        {
            Status = TempData["Status"] == null ? null : TempData["Status"].ToString();
            FromDate = TempData["FromDate"] == null ? null : TempData["FromDate"].ToString();
            ToDate = TempData["ToDate"] == null ? null : TempData["ToDate"].ToString();
            HelpDeskName = TempData["HelpDeskName"] == null ? null : TempData["HelpDeskName"].ToString();

            TempData.Keep();

            var workbook = Factory.GetWorkbook();
            var cells = ExcelAndPrintHelper.GetExcelWorksheetRange(workbook, "HelpDesk Reports");
            var model = new HD_TICKET_ViewModel();
            // if user is AE or WLM, filter out inaccessible lenders from all lenders
            bool isUserAeOrWlm = RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName);
            //List<int> lendersForAeOrWlm = null;

            //if (isUserAeOrWlm)
            //{
            //    lendersForAeOrWlm = accountMgr.GetLenderIdsByAeOrWlmForQtr(UserPrincipal.Current.UserName).ToList();
            //}

            //int lenderId = int.Parse(strlenderId);
            var statuses = _uploadDataMgr.GetUploadStatusDetailByhelpdeskreport(Status, FromDate, ToDate, HelpDeskName);
            var statusesFiltered = new List<usp_HCP_GetTicketDetails_Latest>();
            //select only status types
            //written by siddesh  19092019 #365//
            if (!string.IsNullOrEmpty(Status))
            {
                // statusesFiltered = statuses.ToList().FindAll(x => x.STATUS.ToLower() == Status.ToLower());

                var statuslist = Status != null ? Status.Split(',') : new string[0];
                if (Status != null && statuslist.Any())
                {
                    foreach (string item in statuslist)
                    {
                        string statusname = "";
                        if (item == "0")
                        {
                            statusname = "OPEN";
                        }
                        if (item == "1")
                        {
                            statusname = "INPROGRESS";
                        }
                        if (item == "2")
                        {
                            statusname = "REJECTED";
                        }
                        if (item == "3")
                        {
                            statusname = "CLOSED";
                        }
                        if (item == "4")
                        {
                            statusname = "RESOLVED";
                        }
                        //Added by siddu@02122019
                        if (item == "5")
                        {
                            statusname = "REQUIRED ADDITIONAL INFORMATION";
                        }
                        //Added by siddu@02122019
                        if (item == "6")
                        {
                            statusname = "DEVELOPMENT";
                        }
                        if (!string.IsNullOrEmpty(Status))
                        {
                            var filteredTickets1 = statuses.ToList().FindAll(x => x.STATUS.ToLower() == statusname.ToLower());
                            statusesFiltered.AddRange(filteredTickets1);
                            // return filteredTickets1;

                        }
                    }

                    // return statusesFiltered;
                }
            }


            if (!string.IsNullOrEmpty(FromDate))
            {

                if (statusesFiltered.Any())
                {
                    statusesFiltered = statusesFiltered.ToList().FindAll(x => x.CREATED_ON >= Convert.ToDateTime(FromDate) && x.CREATED_ON <= Convert.ToDateTime(ToDate).AddDays(1));
                }

                else
                {
                    //  statusesFiltered = statuses.ToList().FindAll(x => x.STATUS.ToLower()==Status.ToLower());
                    statusesFiltered = statuses.ToList().FindAll(x => x.CREATED_ON >= Convert.ToDateTime(FromDate) && x.CREATED_ON <= Convert.ToDateTime(ToDate).AddDays(1));

                }



            }

            // if (statusesFiltered.Any())
            //{
            //    //statusesFiltered = statuses.ToList().FindAll(x => x.STATUS.ToLower() == Status.ToLower());
            //   statusesFiltered = statusesFiltered.ToList().FindAll(x => x.CREATED_ON >= Convert.ToDateTime(FromDate) && x.CREATED_ON <= Convert.ToDateTime(ToDate).AddDays(1));


            //}
            // if(!string.IsNullOrEmpty(Status))
            //{
            //    statusesFiltered = statuses.ToList().FindAll(x => x.STATUS.ToLower() == Status.ToLower());
            //}

            //Added by siddesh 15102019//
            if (!string.IsNullOrEmpty(HelpDeskName))
            {
                if (statusesFiltered.Any())
                {
                    statusesFiltered = statusesFiltered.FindAll(x => x.Helpdesk_name == HelpDeskName);
                }
                else
                {
                    statusesFiltered = statuses.ToList().FindAll(x => x.Helpdesk_name == HelpDeskName);
                }

            }
            //Added by siddesh 15102019//
            if (Status == null && FromDate == null && ToDate == null && string.IsNullOrEmpty(HelpDeskName))
            {
                statusesFiltered = statuses.ToList();
                //return statusesFiltered;
            }
            //siddu #825 @10042020
            foreach (var item in statusesFiltered)
            {

                //item.Numberofdays = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.Now);
                item.Numberofdays = GetNumberOfWorkingDays(item.CREATED_ON, DateTime.UtcNow);
                item.Numberofweeks = item.Numberofdays / 7;
                if (item.STATUS == "Rejected" || item.STATUS == "Closed" || item.STATUS == "Resolved")
                {
                    int ts1 = GetNumberOfWorkingDays(item.CREATED_ON, item.LAST_MODIFIED_ON.Value.Date);
                    item.Numberofdays = ts1;
                    item.Numberofweeks = item.Numberofdays / 7;
                }
                if (item.STATUS == "REQUIRED ADDITIONAL INFORMATION" || item.STATUS == "DEVELOPMENT")
                {
                    //int ts1 = GetNumberOfWorkingDays(item.LAST_MODIFIED_ON.Value.Date, DateTime.Now);
                    int ts1 = GetNumberOfWorkingDays(item.ASSIGNED_DATE.Value.Date, DateTime.UtcNow);
                    item.Numberofdays = ts1;
                    item.Numberofweeks = item.Numberofdays / 7;
                }

            }

            ExcelAndPrintHelper.GetHeaderValuesForHelpdeskreports(cells);
            ExcelAndPrintHelper.SetDataValuesForHelpdeskreports(statusesFiltered, cells);
            //  ExcelAndPrintHelper.SetDataValuesForUpload(statusesFiltered, cells);
            //GetResponse(workbook, "HelpDesk Reports " + DateTime.Now.ToString("MM-dd-yyyy HHmmss"));
            GetResponse(workbook, "HelpDesk Reports " + DateTime.UtcNow.ToString("MM-dd-yyyy HHmmss"));

            return PartialView("_Helpdeskreports", model);
        }
        //written by siddesh  29082019 #344//
        public void GetResponse(IWorkbook workbook, string worksheetName)
        {
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + worksheetName + ".xlsx");
            workbook.SaveToStream(Response.OutputStream, FileFormat.OpenXMLWorkbook);
            Response.End();
        }

        //karri;31082019
        [HttpPost]
        public JsonResult SendExternalMail(string mailid, int attachfiles = 0, string ticketid = null)
        {
            bool isFileFound = true;
            string newPath = string.Empty;
            try
            {
                //get ticket details
                var tiktDetail = _hdtktmngr.GetMyTickets(0, Convert.ToInt32(ticketid));

                string attachmentsPath = Server.MapPath("~/UploadedHelpDeskTktFiles/" + ticketid);
                int filescount = 0;

                if (Directory.Exists(attachmentsPath))
                {
                    var files = Directory.GetFiles(attachmentsPath);
                    filescount = files.Length;
                }

                foreach (var item in tiktDetail)
                {
                    item.EXTERNALMAILID = mailid;
                    if (attachfiles.Equals(0))
                        item.ATTACHFILES_EXTERNALMAILID = false;
                    else
                        item.ATTACHFILES_EXTERNALMAILID = true;
                    if (filescount > 0)
                        item.HELPDESK_UPLOADS_PATH = attachmentsPath;
                }

                HD_TICKET_ViewModel modelTktDetail = new HD_TICKET_ViewModel();
                foreach (var item in tiktDetail)
                {
                    modelTktDetail.FHANumber = (item.FHANumber == null || item.FHANumber == string.Empty) ? "N/A" : item.FHANumber;
                    modelTktDetail.PropertyId = item.PropertyId == null ? 000000 : item.PropertyId;
                    modelTktDetail.PropertyName = (item.PropertyName == null || item.PropertyName == string.Empty) ? "N/A" : item.PropertyName;
                    modelTktDetail.ISSUE = item.ISSUE;
                    modelTktDetail.CATEGORY_TEXT = _hdtktmngr.getCategoryById((int)item.CATEGORY);
                    modelTktDetail.SUBCATEGORY_TEXT = _hdtktmngr.getSubCategoryById((int)item.SUBCATEGORY);
                    modelTktDetail.ONACTION_TEXT = _hdtktmngr.getOnActionById((int)item.ONACTION);
                    modelTktDetail.CREATED_BY_USERNAME = item.CREATED_BY_USERNAME;
                    modelTktDetail.CREATED_ON = item.CREATED_ON;
                    modelTktDetail.ASSIGNED_DATE_TEXT = (item.ASSIGNED_DATE == null) ? "N/A" : item.ASSIGNED_DATE.ToString();
                    modelTktDetail.DESCRIPTION = item.DESCRIPTION;
                    modelTktDetail.RECREATION_STEPS = item.RECREATION_STEPS;
                    modelTktDetail.STATUS_TEXT = _hdtktmngr.getStatusById((int)item.STATUS);
                    modelTktDetail.HELPDESK_UPLOADS_PATH = item.HELPDESK_UPLOADS_PATH;
                }

                bool CanSendMailNotification = false;
                if (ConfigurationManager.AppSettings["HUDMailNotificationsFlag"] != null
                   && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDMailNotificationsFlag"].ToString())
                   && Convert.ToBoolean(ConfigurationManager.AppSettings["HUDMailNotificationsFlag"].ToString()))
                {
                    CanSendMailNotification = true;
                }

                if (ConfigurationManager.AppSettings["HUDMoniterFromEmail"] != null
                    && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["HUDMoniterFromEmail"])
                    && CanSendMailNotification)
                {
                    modelTktDetail.MAILNOTIFICATION_FROMBASEEMAIL = ConfigurationManager.AppSettings["HUDMoniterFromEmail"].ToString();

                    _backgroundManager.SendExternalHelpDeskNotificationEmail(_emailManager, modelTktDetail);

                }
                //get attachment folders

                ViewBag.Message = "Mail Sent Successfully!!";
                return Json(new { returnvalue = isFileFound }, JsonRequestBehavior.AllowGet);
            }
            catch
            {

                ViewBag.Message = "Mail Sent failed!!";
                return Json(new { returnvalue = isFileFound }, JsonRequestBehavior.AllowGet);
            }
        }
        //siddu #825 @10042020
        public int GetNumberOfWorkingDays(DateTime Fromdate, DateTime Todate)
        {

            int days = 0;
            var holidays = _hdtktmngr.GetHolidays();
            while (Fromdate <= Todate)
            {
                if (Fromdate.DayOfWeek != DayOfWeek.Saturday && Fromdate.DayOfWeek != DayOfWeek.Sunday && !holidays.Contains(Fromdate.Date))
                {
                    ++days;
                }
                Fromdate = Fromdate.AddDays(1);
            }
            return days;

        }



    }//end of HelpDeskController controller

}