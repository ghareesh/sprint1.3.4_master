﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HUDHealthcarePortal.Controllers.HelpDesk
{
    public class abcClass
    {
        public string Name { get; set; }
        public string SirName { get; set; }
    }

    public class UpdateDemoController : ApiController
    {
        public HttpResponseMessage Get(int id, [FromBody] abcClass value)
        {
            return Request.CreateResponse(HttpStatusCode.OK, value);
        }
    }
}
