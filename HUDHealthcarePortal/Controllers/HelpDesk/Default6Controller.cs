﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HUDHealthcarePortal.Controllers.HelpDesk
{
    public class TestDemo1Controller : ApiController
    {
        // GET api/default6
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/default6/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/default6
        public HttpResponseMessage Post([FromBody] abcClass value)
        {
            return Request.CreateResponse(HttpStatusCode.OK, value);
        }

        // PUT api/default6/5
        public HttpResponseMessage Put(int id, [FromBody] abcClass value)
        {
            return Request.CreateResponse(HttpStatusCode.OK, value);
        }

        // DELETE api/default6/5
        public void Delete(int id)
        {
        }
    }
}
