﻿using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.Xsl;

namespace HUDHealthcarePortal.Framework
{
    public class XsltView : IView
    {
        private readonly string _path;

        public XsltView(string path)
        {
            _path = path;
        }

        public void Render(ViewContext viewContext, TextWriter writer)
        {
            var xsltFile = viewContext.HttpContext.Server.MapPath(_path);
            if (viewContext.ViewData.Model == null || ((List<CommentModel>)viewContext.ViewData.Model).Count == 0)
                return;
            StringBuilder sbComments = new StringBuilder();
            var commentsModel = (List<CommentModel>)viewContext.ViewData.Model;
            sbComments.Append("<root>");
            foreach(CommentModel comment in commentsModel)
            {
                sbComments.Append(comment.Comments.ToString());
            }
            sbComments.Append("</root>");
            //var xmlTree = XDocument.Parse(viewContext.ViewData.Model.ToString());
            var xmlTree = XDocument.Parse(sbComments.ToString());
            var xslt = new XslCompiledTransform();

            XsltArgumentList arguments = new XsltArgumentList();
            arguments.AddExtensionObject("xsltUtils:XsltUtils", new XsltUtilHelper());

            xslt.Load(xsltFile);
            xslt.Transform(xmlTree.CreateReader(), arguments, writer);
        }
    }
}
