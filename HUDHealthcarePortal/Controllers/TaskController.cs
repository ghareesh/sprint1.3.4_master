﻿using System;
using System.Text;
using BusinessService.Interfaces;
using Core.Utilities;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Model;
using System.Collections;
using System.Collections.Generic;
using System.Collections;

namespace HUDHealthCarePortal.Controllers
{
    public class TaskController : Controller
    {
        ITaskManager taskMgr;
        private IProjectActionFormManager projectActionFormManager;
        private IAccountManager accountManager;
        private IParentChildTaskManager parentChildTaskManager;
        private ITaskReAssignmentManager taskReAssignmentManager;

        public TaskController()
            : this(new TaskManager(), new ProjectActionFormManager(), new ParentChildTaskManager(), new TaskReAssignmentManager())
        {
        }

        public TaskController(ITaskManager taskManager, IProjectActionFormManager _projectActionFormManager, IParentChildTaskManager _parentChildTaskManager, ITaskReAssignmentManager _taskReAssignmentManager)
        {
            taskMgr = taskManager;
            projectActionFormManager = _projectActionFormManager;
            parentChildTaskManager = _parentChildTaskManager;
            taskReAssignmentManager = _taskReAssignmentManager;
        }
        //
        // get my tasks
        [HttpGet]
        public ActionResult MyTasks(int? page, string sort, string sortDir, string lookupFHANumber = null)
        {
            //karri#205
            IList<String> UserFHAList = taskMgr.getFHAList(UserPrincipal.Current.UserName);
            if (lookupFHANumber != null && !string.IsNullOrEmpty(lookupFHANumber))
                taskMgr.setLookUpValues(lookupFHANumber);
            //Note:Search Filter is based on the values set in taskMgr.setLookUpValues(lookupFHANumber);

            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            var model = taskMgr.GetTasksByUserName(UserPrincipal.Current.UserName, page ?? 1,
                string.IsNullOrEmpty(sort) ? "MyStartTime" : sort,
                string.IsNullOrEmpty(sortDir)
                    ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                    : EnumUtils.Parse<SqlOrderByDirecton>(sortDir));

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir}
            };
            Session[SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT] = routeValuesDict;

            var myTasks = model.Entities.ToList();
            foreach(var item in myTasks)
            {
                var task = taskMgr.GetTaskById(item.TaskId);
                // harish added taskstepid =25 to get Rai Request 07042020
                if (task.TaskStepId != 25)
                {
                    var formUploadData = new OPAViewModel();
                    // enable below line due to data issue ,i have commented but after checking we know that it was data issue -09042020
                    formUploadData = projectActionFormManager.GetOPAByTaskId(task.TaskId);
                    // above line commented because of getting different transaction type so for opa we are getting results by using taskinstanceid 07062020
                    //formUploadData = projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    if (formUploadData != null)
                        // item.TaskName = projectActionFormManager.GetProjectActionName(formUploadData.ProjectActionTypeId); //commented by harish 11032020
                        item.TaskName = projectActionFormManager.GetProjectActionNameforAssetmanagement(formUploadData.ProjectActionTypeId);
                    else
                    {
                        formUploadData = projectActionFormManager.GetOPAByChildTaskId(task.TaskInstanceId);
                        if (formUploadData != null)
                            // item.TaskName = projectActionFormManager.GetProjectActionName(formUploadData.ProjectActionTypeId); //commented by harish 11032020
                            item.TaskName = projectActionFormManager.GetProjectActionNameforAssetmanagement(formUploadData.ProjectActionTypeId);
                    }
                }
                // harish added taskstepid =25 to get Rai Request 07042020 below condition check ParentChildTask Table 
                else
                {
                    var formUploadData = new OPAViewModel();
                    formUploadData = projectActionFormManager.GetOPAByChildTaskId(task.TaskInstanceId);
                    if (formUploadData != null)
                        // item.TaskName = projectActionFormManager.GetProjectActionName(formUploadData.ProjectActionTypeId); //commented by harish 11032020
                        item.TaskName = projectActionFormManager.GetProjectActionNameforAssetmanagement(formUploadData.ProjectActionTypeId);
                }
                if (parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
                    item.IsChild = true;
                if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
                {
                    if (item.Status == FormUploadStatus.AssignedToOther)
                    {
                        if (RoleManager.IsUserOperatorAccountRepresentative(item.AssignedBy) 
                            && RoleManager.IsUserLenderRole(item.AssignedTo))
                        {
                            item.Status = FormUploadStatus.AssignedByOther;
                        }
                    } 
                }
                
            }

            ////karri#205         
            ViewBag.FHAList = UserFHAList;
            @ViewData["BindingFHANumber"] = lookupFHANumber;

            var results = myTasks.GroupBy(s => s.ParentchildId).SelectMany(gr => gr).ToList();
            model.Entities = results; 
         
            return View("TaskView", model);
        }

        [HttpGet]        
        public ActionResult GetTaskDetail(int id, bool fromTaskReAssignment, string reAssignedTo)
        {
            var task = taskMgr.GetTaskById(id);
            // based on task step, decide if is edit mode or readonly mode
            bool isEditMode = true;

            if (task.TaskStepId == (int)TaskStep.Final||task.TaskStepId == (int)TaskStep.NonCriticalRepairComplete || 
                task.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete || 
                task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete ||
                task.TaskStepId == (int)TaskStep.R4RComplete)
                isEditMode = false;

            if(task.AssignedTo.ToLower() != UserPrincipal.Current.UserName.ToLower())
                    isEditMode = false;

            if (!string.IsNullOrEmpty(reAssignedTo) && task.AssignedBy.ToLower() != reAssignedTo.ToLower() && 
                (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) || 
                RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName)))
            {
                isEditMode = true;
            }

            // TO DO: for general tasks, need add logic to unwrap based on task types
            // now only have upload form
            // deserialize submited form data
            //if (task.DataStore1.Contains("ProjectActionViewModel"))
            //{
            //    var formUploadData = XmlHelper.Deserialize(typeof(ProjectActionViewModel), task.DataStore1, Encoding.Unicode) as ProjectActionViewModel;
            //    if (formUploadData != null)
            //    {
            //        formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus) ? new TaskOpenStatusModel() :
            //            XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as TaskOpenStatusModel;

            //        formUploadData.TaskGuid = task.TaskInstanceId;
            //        formUploadData.Concurrency = taskMgr.GetConcurrencyTimeStamp(task.TaskInstanceId);
            //        formUploadData.SequenceId = task.SequenceId;
            //        formUploadData.AssignedBy = task.AssignedBy;
            //        formUploadData.AssignedTo = task.AssignedTo;
            //        formUploadData.IsReassigned = task.IsReAssigned;
                  
            //            if (task.AssignedTo.ToLower() == UserPrincipal.Current.UserName.ToLower() &&
            //                RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
            //            {
            //                formUploadData.IsEditMode = true;
            //            }
            //            else
            //            {
            //                formUploadData.IsEditMode = false;
            //            }

            //            if (!string.IsNullOrEmpty(reAssignedTo)  && task.AssignedBy.ToLower() != reAssignedTo.ToLower() &&
            //            RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
            //            {
            //                formUploadData.IsEditMode = true;
            //            }
                 
            //        // save task opened in task db
            //        if (formUploadData.TaskOpenStatus != null)
            //        {
            //            formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
            //            task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
            //            taskMgr.UpdateTask(task);
            //        }

            //        TempData["ProjectActionFormData"] = formUploadData;
            //    }
            //    return RedirectToAction("GetProjectActionRequestFormDetail", "ProjectActionRequest", new { @model = formUploadData });
            //}
            if ((task.PageTypeId == projectActionFormManager.GetPageTypeIdByName("OPA")))
            {
                bool hasTaskOpenByUser = false;
                var formUploadData = new OPAViewModel();
                var isChildTask = false;
                if (parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
                {
                    formUploadData = projectActionFormManager.GetOPAByChildTaskId(task.TaskInstanceId);
                    formUploadData.ChildTaskInstanceId = task.TaskInstanceId;
                    isChildTask = true;
                }
                else
                {
                    formUploadData = projectActionFormManager.GetOPAByTaskId(task.TaskId);
                }
                formUploadData.IschildTask = isChildTask;
                formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                        ? new TaskOpenStatusModel()
                        : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                            TaskOpenStatusModel;

                    formUploadData.TaskGuid = task.TaskInstanceId;
                    formUploadData.Concurrency = taskMgr.GetConcurrencyTimeStamp(task.TaskInstanceId);
                    formUploadData.SequenceId = task.SequenceId;
                    formUploadData.AssignedBy = task.AssignedBy;
                    formUploadData.AssignedTo = task.AssignedTo;
                    formUploadData.IsReassigned = task.IsReAssigned;
                    formUploadData.TaskId = id;
                formUploadData.PropertyId =
                    projectActionFormManager.GetPropertyInfo(formUploadData.FhaNumber).PropertyId;
                formUploadData.TaskStepId = task.TaskStepId;
                    if (task.AssignedTo.ToLower() == UserPrincipal.Current.UserName.ToLower() &&
                        RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) && !isChildTask)
                    {
                        formUploadData.IsEditMode = true;
                    }
                    else
                    {
                        formUploadData.IsEditMode = false;
                    }

                    if (!string.IsNullOrEmpty(reAssignedTo) && task.AssignedBy.ToLower() != reAssignedTo.ToLower() &&
                    RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
                    {
                        formUploadData.IsEditMode = true;
                    }

                    hasTaskOpenByUser = formUploadData.TaskOpenStatus.HasTaskOpenByUser(UserPrincipal.Current.UserName);
                    // save task opened in task db
                    if (formUploadData.TaskOpenStatus != null)
                    {
                        formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus,
                        typeof(TaskOpenStatusModel), Encoding.Unicode);
                        taskMgr.UpdateTask(task);
                    }

                //Logic for reassign of parent task after a child request is created
                    if (parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
                    {
                        var parenttask = parentChildTaskManager.GetParentTask(task.TaskInstanceId);
                        if (parenttask != null)
                        {
                            var newreassignedbyAe = string.Empty;

                            newreassignedbyAe = taskReAssignmentManager.GetReassignedAE(parenttask.ParentTaskInstanceId);
                            if (newreassignedbyAe != string.Empty)
                                formUploadData.AssignedBy = newreassignedbyAe;
                        }
                    }
                if (formUploadData.ServicerSubmissionDate != null)
                {
                    //formUploadData.ServicerSubmissionDate = TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
                    formUploadData.ServicerSubmissionDate = (DateTime)formUploadData.ServicerSubmissionDate;
                }

            
                    TempData["OPAFormData"] = formUploadData;
                    TempData["hasTaskOpenByUser"] = hasTaskOpenByUser;
                

                return RedirectToAction("GetOPAFormDetail", "OPAForm", new { @model = formUploadData});
            }
            else if ((task.PageTypeId == projectActionFormManager.GetPageTypeIdByName("APPLICATION REQUEST")))
            {
                bool hasTaskOpenByUser = false;
                var formUploadData = new OPAViewModel();
                var isChildTask = false;
                if (parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
                {
                    formUploadData = projectActionFormManager.GetProdOPAByChildTaskId(task.TaskInstanceId);
                    formUploadData.ChildTaskInstanceId = task.TaskInstanceId;
                    isChildTask = true;
                }
                else
                {
                    formUploadData = projectActionFormManager.GetOPAByTaskId(task.TaskId);
                }
                formUploadData.IschildTask = isChildTask;
                formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                        ? new TaskOpenStatusModel()
                        : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                            TaskOpenStatusModel;

                formUploadData.TaskGuid = task.TaskInstanceId;
                formUploadData.Concurrency = taskMgr.GetConcurrencyTimeStamp(task.TaskInstanceId);
                formUploadData.SequenceId = task.SequenceId;
                formUploadData.AssignedBy = task.AssignedBy;
                formUploadData.AssignedTo = task.AssignedTo;
                formUploadData.IsReassigned = task.IsReAssigned;
                formUploadData.TaskId = id;
                formUploadData.PropertyId =
                    projectActionFormManager.GetPropertyInfo(formUploadData.FhaNumber).PropertyId;
                formUploadData.TaskStepId = task.TaskStepId;
                if (task.AssignedTo.ToLower() == UserPrincipal.Current.UserName.ToLower() &&
                    RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) && !isChildTask)
                {
                    formUploadData.IsEditMode = true;
                }
                else
                {
                    formUploadData.IsEditMode = false;
                }

                if (!string.IsNullOrEmpty(reAssignedTo) && task.AssignedBy.ToLower() != reAssignedTo.ToLower() &&
                RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
                {
                    formUploadData.IsEditMode = true;
                }

                hasTaskOpenByUser = formUploadData.TaskOpenStatus.HasTaskOpenByUser(UserPrincipal.Current.UserName);
                // save task opened in task db
                if (formUploadData.TaskOpenStatus != null)
                {
                    formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus,
                        typeof(TaskOpenStatusModel), Encoding.Unicode);
                    taskMgr.UpdateTask(task);
                }

                //Logic for reassign of parent task after a child request is created
                if (parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
                {
                    var parenttask = parentChildTaskManager.GetParentTask(task.TaskInstanceId);
                    if (parenttask != null)
                    {
                        var newreassignedbyAe = string.Empty;

                        newreassignedbyAe = taskReAssignmentManager.GetReassignedAE(parenttask.ParentTaskInstanceId);
                        if (newreassignedbyAe != string.Empty)
                            formUploadData.AssignedBy = newreassignedbyAe;
                    }
                }

                //formUploadData.ServicerSubmissionDate =
                //    TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
                formUploadData.ServicerSubmissionDate =
                   (DateTime)formUploadData.ServicerSubmissionDate;
                TempData["OPAFormData"] = formUploadData;
                TempData["hasTaskOpenByUser"] = hasTaskOpenByUser;


                return RedirectToAction("GetOPAFormDetail", "OPAForm", new { @model = formUploadData });
            }

            else if (task.DataStore1.Contains("NonCriticalRequestExtensionModel"))
            {
                var formUploadData = XmlHelper.Deserialize(typeof(NonCriticalRequestExtensionModel), task.DataStore1, Encoding.Unicode) as NonCriticalRequestExtensionModel;
                if (formUploadData != null)
                {
                    formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus) ? new TaskOpenStatusModel() :
                        XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as TaskOpenStatusModel;
                    // formUploadData.ServicerRemarks = task.Notes;
                    //TempData["FormUploadData"] = formUploadData;
                    formUploadData.TaskGuid = task.TaskInstanceId;
                    formUploadData.Concurrency = taskMgr.GetConcurrencyTimeStamp(task.TaskInstanceId);
                    formUploadData.SequenceId = task.SequenceId;
                    formUploadData.AssignedBy = task.AssignedBy;
                    formUploadData.AssignedTo = task.AssignedTo;
                    formUploadData.IsReassigned = task.IsReAssigned;

                     // save task opened in task db
                    if (formUploadData.TaskOpenStatus != null)
                    {
                        formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                        taskMgr.UpdateTask(task);
                    }

                    TempData["NonCriticalRequestExtensionModel"] = formUploadData;
                }
                if (fromTaskReAssignment)
                {
                    return RedirectToAction("GetNonCriticalRequestExtensionFormDetailReAssigned", "NonCriticalRequestExtension", new { @IsEditMode = isEditMode });
                }
                else 
                {
                    return RedirectToAction("GetNonCriticalRequestExtensionFormDetail", "NonCriticalRequestExtension", new { @IsEditMode = isEditMode });
                }
            }else if (task.DataStore1.Contains("NonCriticalRepairsViewModel"))
            {
                var formUploadData = XmlHelper.Deserialize(typeof(NonCriticalRepairsViewModel), task.DataStore1, Encoding.Unicode) as NonCriticalRepairsViewModel;
                if (formUploadData != null)
                {
                    formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus) ? new TaskOpenStatusModel() :
                        XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as TaskOpenStatusModel;
                   // formUploadData.ServicerRemarks = task.Notes;
                    //TempData["FormUploadData"] = formUploadData;
                    formUploadData.TaskGuid = task.TaskInstanceId;
                    formUploadData.Concurrency = taskMgr.GetConcurrencyTimeStamp(task.TaskInstanceId);
                    formUploadData.SequenceId = task.SequenceId;
                    formUploadData.AssignedBy = task.AssignedBy;
                    formUploadData.AssignedTo = task.AssignedTo;
                    formUploadData.IsReassigned = task.IsReAssigned;
                    // save task opened in task db
                    if (formUploadData.TaskOpenStatus != null)
                    {
                        formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                        taskMgr.UpdateTask(task);
                    }

                    TempData["NonCriticalRepairsViewModel"] = formUploadData;
                }
                return RedirectToAction("GetNonCriticalRepairsRequestFormDetail", "NonCriticalRepairsRequest", new { @IsEditMode = isEditMode });
            }
            else if (task.DataStore1.Contains("ReserveForReplacementFormModel"))
            {
                var formUploadData = XmlHelper.Deserialize(typeof(ReserveForReplacementFormModel), task.DataStore1, Encoding.Unicode) as ReserveForReplacementFormModel;
                if (formUploadData != null)
                {
                    formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus) ? new TaskOpenStatusModel() :
                        XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as TaskOpenStatusModel;
                    formUploadData.IsReAssigend = task.IsReAssigned;  
                    formUploadData.TaskGuid = task.TaskInstanceId;
                    formUploadData.Concurrency = taskMgr.GetConcurrencyTimeStamp(task.TaskInstanceId);
                    formUploadData.SequenceId = task.SequenceId;
                    formUploadData.AssignedBy = task.AssignedBy;
                    formUploadData.AssignedTo = task.AssignedTo;
                    formUploadData.TaskId = task.TaskId;

                    // save task opened in task db
                    if (formUploadData.TaskOpenStatus != null)
                    {
                        formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                        taskMgr.UpdateTask(task);
                    }

                    TempData["R4RFormData"] = formUploadData;
                }
                return RedirectToAction("GetReserveForReplacementFormDetail", "ReserveForReplacement", new { @IsEditMode = isEditMode });
            }
            else
            {
                var formUploadData = XmlHelper.Deserialize(typeof(FormUploadModel), task.DataStore1, Encoding.Unicode) as FormUploadModel;
                if (formUploadData != null)
                {
                    formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus) ? new TaskOpenStatusModel() :
                        XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as TaskOpenStatusModel;
                    formUploadData.Notes = task.Notes;
                    //TempData["FormUploadData"] = formUploadData;
                    formUploadData.TaskGuid = task.TaskInstanceId;
                    formUploadData.Concurrency = taskMgr.GetConcurrencyTimeStamp(task.TaskInstanceId);
                    formUploadData.SequenceId = task.SequenceId;
                    formUploadData.AssignedBy = task.AssignedBy;
                    formUploadData.AssignedTo = task.AssignedTo;
                    formUploadData.IsFromTask = true;

                    // save task opened in task db
                    if (formUploadData.TaskOpenStatus != null)
                    {
                        formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                        taskMgr.UpdateTask(task);
                    }

                    TempData["FormUploadData"] = formUploadData;
                }
                return RedirectToAction("GetFormUploadDetail", "FormUpload", new { @IsEditMode = isEditMode });
            }
        }

        [HttpPost]
        public ActionResult TaskNotesDetail(Guid id)
        {
            var taskInstanceId = id;
            var model = taskMgr.GetTasksByTaskInstanceId(taskInstanceId);
            var taskModels = model as IList<TaskModel> ?? model.ToList();
            if (model != null && taskModels.ToList().Any())
            {
                if (taskModels.First().DataStore1.Contains("ReserveForReplacementFormModel"))
                {
                    foreach (var item in model.ToList())
                    {
                        if (item.SequenceId == 2)
                            taskModels.Remove(item);
                    }
                    model = taskModels;
                }
            }
            return PartialView("TaskNotesDetail", model);
        }
        public JsonResult GetMyTasks()
        {
            var currentUser = UserPrincipal.Current.UserName;
            var myTasks = taskMgr.GetTasksByUserName(currentUser, 1,
                "MyStartTime" ,
                 EnumUtils.Parse<SqlOrderByDirecton>("DESC"));
            var jsonData = (from mytask in myTasks.Entities
                            select new
                            {
                                mytask.AssignedBy,
                                mytask.AssignedTo,
                                mytask.FHANumber,
                                mytask.ItemName

                            }).Take(3);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}
