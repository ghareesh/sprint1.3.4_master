﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;

namespace HUDHealthcarePortal.Controllers
{
    [Authorize]
    public class CommentController : Controller
    {
        ICommentManager commentMgr;
        IEmailManager emailMgr;
        IAccountManager accountMgr;

        public CommentController() : this(new CommentManager(), new EmailManager(), new AccountManager())
        {

        }

        public CommentController(ICommentManager commentManager, IEmailManager emailManager, IAccountManager accountManager)
        {
            commentMgr = commentManager;
            emailMgr = emailManager;
            accountMgr = accountManager;
        }

        //
        // GET: /Comment/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetToEmailRecipients(Guid? nodeId, int ldiID)
        {
            // get the uploader to email list
            var uploader = commentMgr.GetUploaderByLdiID(ldiID);
            string uploaderName = uploader.FirstName + " " + uploader.LastName;
            if (nodeId == null)  // new comment, only need to get uploader's email
            {
                //var json = Json(new { FirstName = uploader.FirstName, LastName = uploader.LastName, Email = uploader.UserName }, JsonRequestBehavior.AllowGet);
                var json = Json(uploaderName, JsonRequestBehavior.AllowGet);
                return json;
            }
            else
            {
                // this is a reply to someone else's comment
                var comments = commentMgr.GetCommentsByLdiID(ldiID);
                var commentModelRoot = FindRootNodeIdFromParentNodeId(nodeId, comments);
                
                Dictionary<string, string> authors = new Dictionary<string, string>();
                authors.Add(uploader.UserName, uploaderName);
                FindAllAuthors(commentModelRoot, authors);
                var toEmails = authors.Select(p => p.Value).ToList();                
                
                var json = Json(TextUtils.TokenDelimitedText(toEmails, ","), JsonRequestBehavior.AllowGet);
                return json;
            }
        }

        [HttpGet]
        public ActionResult AddComment(Guid? parentNodeId, int ldiID, string subject, string comment, string addedEmailRecipients)
        {
            try
            {
                var addedEmails = TextUtils.SplitStringToArrayString(addedEmailRecipients);
                var commentModelNew = new CommentModel();
                commentModelNew.LDI_ID = ldiID;
                commentModelNew.Comment = comment;
                commentModelNew.Subject = subject;
                commentModelNew.Comments = XmlHelper.Serialize(commentModelNew, typeof(CommentModel), Encoding.Unicode);
                if (parentNodeId == null)
                {
                    commentMgr.AddCommentForProject(commentModelNew);
                    var commentModelFromDB = commentMgr.GetCommentsByLdiID(ldiID);

                    SendCommentEmails(commentModelNew, commentModelFromDB, "placeHolderUploader",
                        commentModelNew.Subject, addedEmails, ldiID);
                    return PartialView("~/Xslt/ExcelUploadView_/CommentsTemplate.xslt", commentModelFromDB);
                }
                else
                {
                    // this is a reply to someone else's comment
                    var comments = commentMgr.GetCommentsByLdiID(ldiID);
                    var commentModelRoot = FindRootNodeIdFromParentNodeId(parentNodeId, comments);
                    var replyToNode = FindByParentNodeId(parentNodeId, commentModelRoot);
                    if (replyToNode == null)
                        throw new NullReferenceException(string.Format("parentNodeId {0}, LDI_ID {1} cannot find reply to comment", parentNodeId, ldiID));
                    // append comment to child comments of parent node
                    replyToNode.ChildComments.Add(commentModelNew);
                    commentModelRoot.Comments = XmlHelper.Serialize(commentModelRoot, typeof(CommentModel), Encoding.Unicode);
                    commentMgr.UpdateCommentForProject(commentModelRoot);
                    var commentModelFromDB = commentMgr.GetCommentsByLdiID(ldiID);

                    // send comment emails to all comment authors within this "Subject"
                    SendCommentEmails(commentModelRoot, commentModelFromDB, replyToNode.Username, 
                        commentModelRoot.Subject, addedEmails, ldiID);

                    return PartialView("~/Xslt/ExcelUploadView_/CommentsTemplate.xslt", commentModelFromDB);
                }
            }
            catch(Exception ex)
            {
                throw new InvalidOperationException("Error save comments", ex.InnerException);
            }
        }

        #region helper
        private CommentModel FindRootNodeIdFromParentNodeId(Guid? parentNodeId, IEnumerable<CommentModel> comments)
        {
            CommentModel result = null;
            foreach (CommentModel comment in comments)
            {
                if (FindByParentNodeId(parentNodeId, comment) != null)
                {
                    result = comment;
                    break;
                }
            }
            return result;
        }

        private CommentModel FindByParentNodeId(Guid? parentNodeId, CommentModel root)
        {
            if (root.NodeId.Equals(parentNodeId))
                return root;
            CommentModel result = null;
            foreach(var item in root.ChildComments)
            {
                result = FindByParentNodeId(parentNodeId, item);
                if (result != null)
                    break;
            }
            return result;
        }

        private void FindAllAuthors(CommentModel root, Dictionary<string, string> authors)
        {
            if (authors == null)
                throw new ArgumentNullException("need to initialized authors as new Dictionary<string, string>");
            if (!authors.ContainsKey(root.Username))
            {
                var user = accountMgr.GetUserByUsername(root.Username);
                authors.Add(root.Username, user.FirstName + " " + user.LastName);
            }
            foreach (var item in root.ChildComments)
            {
                FindAllAuthors(item, authors);
            }            
        }

        private bool SendCommentEmails(CommentModel rootComment, IEnumerable<CommentModel> commentModelFromDB, 
            string replyToUser, string subject, List<string> addedEmails, int ldiID)
        {
            var xsltFile = Server.MapPath("~/Xslt/ExcelUploadView_/CommentsTemplate.xslt");
            StringBuilder sbComments = new StringBuilder();
            var commentsModel = (List<CommentModel>)commentModelFromDB;
            sbComments.Append("<root>");
            foreach (CommentModel comment in commentsModel)
            {
                sbComments.Append(comment.Comments);
            }
            sbComments.Append("</root>");
            var xmlTree = XDocument.Parse(sbComments.ToString());
            var xslt = new XslCompiledTransform();

            XsltArgumentList arguments = new XsltArgumentList();
            arguments.AddExtensionObject("xsltUtils:XsltUtils", new XsltUtilHelper());

            xslt.Load(xsltFile);
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            settings.CloseOutput = false;
            xslt.Transform(xmlTree.CreateReader(), arguments, XmlWriter.Create(sb, settings));

            Dictionary<string,string> authors = new Dictionary<string,string>();
            FindAllAuthors(rootComment, authors);

            // added additional emails to the list
            addedEmails.ForEach(email => { if (!authors.ContainsKey(email)) authors.Add(email, email); });
            // find the person who uploaded this entry (ldi id)
            // add the uploader to email list
            var uploader = commentMgr.GetUploaderByLdiID(ldiID);
            if (!authors.ContainsKey(uploader.UserName))
                authors.Add(uploader.UserName, uploader.FirstName + " " + uploader.LastName);
            var htmlEmailContent = sb.ToString();
            if (replyToUser == "placeHolderUploader")
                replyToUser = uploader.UserName;
            
            return Request.UrlReferrer != null && emailMgr.SendCommentEmail(authors, replyToUser, subject, htmlEmailContent, Request.UrlReferrer.ToString());
        }
        #endregion helper
    }
}
