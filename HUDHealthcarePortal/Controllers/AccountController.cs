﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BusinessService.Interfaces;
using BusinessService.ProjectAction;
using Core.Utilities;
using Elmah;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthCarePortal.Filters;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Model.ProjectAction;
using Model;
using WebGrease.Css.Extensions;
using RootModel = HUDHealthcarePortal.Model;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Helpers;
using Core;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthCarePortal.Helpers;
using HUDHealthcarePortal.Core.ExceptionHandling;
using System.Configuration;
using System.Web.SessionState;
using BusinessService.Interfaces.UserLogin;
using BusinessService.UserLogin;
using BusinessService;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using System.IO;
using System.Data.OleDb;
using System.Data;
using LinqToExcel;
using HUDHealthcarePortal.CustomModels;
using System.Data.SqlClient;
using HUDHealthcarePortal.Lenderprocess;

namespace HUDHealthCarePortal.Controllers
{
	/// <summary>
	/// This class is related to managing all the user accounts
	/// </summary>
	[Authorize]
	[InitializeSimpleMembership]
	public class AccountController : Controller
	{
		IAccountManager accountMgr;
		ILookupManager lookupMgr;
		IWebSecurityWrapper webSecurity;
		IReviewerTitlesManager reviewerTitl;
		private IQuestionManager questionMgr;
		private IProjectActionManager projectActionMgr;
		private ITaskReAssignmentManager taskRAManager;
		private IUserLoginManager ulMgr;
		private IProjectActionFormManager projectActionFormMgr;
        private ILenderMgr _lenderMgr;//189
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        public AccountController()
			: this(new AccountManager(), new LookupManager(), new WebSecurityWrapper(), new QuestionManager(),
			new ProjectActionManager(), new UserLoginManager(), new ReviewerTitlesManager()
                  , new TaskReAssignmentManager(), new ProjectActionFormManager(), new LenderManager())
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AccountController"/> class.
		/// </summary>
		/// <param name="accountManager">The account manager.</param>
		/// <param name="lookupManager">The lookup manager.</param>
		/// <param name="webSec">The web sec.</param>
		public AccountController(IAccountManager accountManager, ILookupManager lookupManager, IWebSecurityWrapper webSec,
			IQuestionManager QuestMgr, IProjectActionManager projectActionManager, IUserLoginManager ulManager,
			IReviewerTitlesManager ReviewerTitlesManager, ITaskReAssignmentManager taskReAssignmentManager
            , IProjectActionFormManager projectActionFormManager, ILenderMgr lendermanager)
		{
			accountMgr = accountManager;
			lookupMgr = lookupManager;
			webSecurity = webSec;
			questionMgr = QuestMgr;
			projectActionMgr = projectActionManager;
			ulMgr = ulManager;
			reviewerTitl = ReviewerTitlesManager;
			taskRAManager = taskReAssignmentManager;
			projectActionFormMgr = projectActionFormManager;
            _lenderMgr = lendermanager;

        }

		/// <summary>
		/// Logins the specified return URL.
		/// </summary>
		/// <param name="returnUrl">The return URL.</param>
		/// <returns></returns>
		[AllowAnonymous]
		public ActionResult Login(string returnUrl)
		{
			LogOff();
			ViewBag.ReturnUrl = "";
			var model = new LoginModel();
			return View(model);
		}

		/// <summary>
		/// Logins the specified model.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <param name="returnUrl">The return URL.</param>
		/// <returns></returns>
		[HttpPost]
		[AllowAnonymous]
		[HandleAntiForgeryError]
		[ValidateAntiForgeryToken]
		public ActionResult Login(LoginModel model, string returnUrl)
		{
			if (!ModelState.IsValid)
				return View(model);
			model.UserName = model.UserName.ToLower();
			//User is inactive so throw an error.
			int userId = accountMgr.GetUserId(model.UserName.ToLower());
			if (userId == 0)
			{
				ModelState.AddModelError("", " Invalid User Name and Password.");
				return View(model);
			}

			bool isUserExist = webSecurity.UserExists(model.UserName);
			if (isUserExist && accountMgr.IsUserInactive(userId))
			{
				ModelState.AddModelError("", "Your account has been inactivated. Please contact the administrator.");
				return View(model);
			}

			if (webSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
			{
				SessionIDManager manager = new SessionIDManager();
				string newSessionId = manager.CreateSessionID(System.Web.HttpContext.Current);
				Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", newSessionId));
				System.Web.HttpContext.Current.Session["SessionId"] = newSessionId;

				if (UserPrincipal.Current.Roles == null)
				{
					UserPrincipal.Current.Roles = Roles.GetRolesForUser(model.UserName);
				}
				if (UserPrincipal.Current.Roles.ToList().Count > 1)
				{
					return RedirectToLocal(string.IsNullOrEmpty(returnUrl) ? "/Account/LoginTransition" : returnUrl);
				}

				//erPrincipal.Current.UserRole = UserPrincipal.Current.Roles[0];
				string[] Role = Roles.GetRolesForUser(model.UserName);
				UserPrincipal.Current.UserRole = Role[0];

				UserLoginModel ulmodel = new UserLoginModel();
				ulmodel.Login_Time = DateTime.UtcNow;
				//ulmodel.RoleName = UserPrincipal.Current.UserRole;
				Session["UserRole"] = UserPrincipal.Current.UserRole;
				ulmodel.RoleName = Role[0];
				ulmodel.Session_Id = newSessionId;
				ulmodel.CreatedOn = DateTime.UtcNow;
				ulmodel.ModifiedOn = DateTime.UtcNow;
				ulmodel.CreatedBy = userId;
				ulmodel.ModifiedBy = userId;
				ulmodel.User_id = userId;
				var successguid = ulMgr.SaveUserLogin(ulmodel);
				return PerformRedirectionBasedOnAuthentication(model.UserName, returnUrl);
				Role = null;
			}

			if (webSecurity.IsAccountLockedOut(model.UserName, AccountManager.MaxLoginTrials, AccountManager.LockAccountInSeconds))
				return RedirectToAction("SingleMessage", new { message = string.Format("Your account is locked out after {0} failed logins, please contact administrator to unlock your account.", AccountManager.MaxLoginTrials + 1) });

			if (isUserExist && !webSecurity.IsConfirmed(model.UserName))
				return RedirectToAction("SingleMessage", new { message = "Please complete registration first. You should have received an email to complete registration." });

			// If we got this far, something failed, redisplay form
			ModelState.AddModelError("", "The user name or password provided is incorrect.");
			return View(model);
		}

		/// <summary>
		/// Performs the redirection based on authentication for a logged in user role.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="returnUrl">The return URL.</param>
		/// <returns></returns>
		private ActionResult PerformRedirectionBasedOnAuthentication(string userName, string returnUrl)
		{
			FormsAuthentication.SetAuthCookie(userName, false);
			Session["HcpMenu"] = null;
			var passwordChangedDate = webSecurity.GetPasswordChangedDate(userName);
			var passwordToChangeDate = passwordChangedDate.AddDays(AccountManager.ChangePasswordInDays);
			if ((DateTime.UtcNow - passwordChangedDate).TotalDays <= AccountManager.ChangePasswordInDays
				&& (passwordToChangeDate - DateTime.UtcNow).TotalDays <= AccountManager.DaysToAskChangePassword)
			{

				TempData["bShowChangePasswordMsg"] = true;
				TempData["ExpireInDays"] = (int)(passwordToChangeDate - DateTime.UtcNow).TotalDays + 1;
				PopupHelper.ConfigPopup(TempData, 640, 340, "Change Password", false, false, true, false, true, false,
					"../Account/ChangePassword");
			}
			else if ((DateTime.UtcNow - passwordChangedDate).TotalDays > AccountManager.ChangePasswordInDays)
			{
				TempData["PasswordExpires"] = true;
				PopupHelper.ConfigPopup(TempData, 640, 340, "Change Password", false, false, true, false, false, true,
				   "../Account/ChangePassword");
			}
			if (RoleManager.IsUserLamBamLar(userName))
				//return RedirectToLocal(string.IsNullOrEmpty(returnUrl) ? "/LenderPAMReport/Index" : returnUrl);
				return RedirectToLocal(string.IsNullOrEmpty(returnUrl) ? "/Home/Landing" : returnUrl);
			if (RoleManager.IsReviewer(userName))
				return RedirectToLocal(string.IsNullOrEmpty(returnUrl) ? "/ProductionMyTask/Index" : returnUrl);

			if (RoleManager.IsSuperUserRole(userName) || RoleManager.IsHudUser(userName) || RoleManager.IsInternalSpecialOptionUser(userName))
				return RedirectToLocal(string.IsNullOrEmpty(returnUrl) ? "/Home/Landing" : returnUrl);
			if (RoleManager.IsInspectionContractor(userName))
				return RedirectToLocal(string.IsNullOrEmpty(returnUrl) ? "/ProductionMyTask/Index" : returnUrl);

			return RedirectToLocal(string.IsNullOrEmpty(returnUrl) ? "/Home/Landing" : returnUrl);
		}


		/// <summary>
		/// Logs the off.
		/// </summary>
		/// <returns></returns>
		public ActionResult LogOff()
		{
			//Log Off History logic

			if (System.Web.HttpContext.Current != null)
			{
				UserLoginModel ulmodel = new UserLoginModel();
				ulmodel.User_id = UserPrincipal.Current.UserId;
				ulmodel.Logout_Time = DateTime.UtcNow;
				ulmodel.Session_Id = System.Web.HttpContext.Current.Session.SessionID;
				ulmodel.ModifiedBy = UserPrincipal.Current.UserId;
				ulMgr.UpdatUserLogin(ulmodel);
			}
			// this corresponds to form authenticate in log in
			FormsAuthentication.SignOut();
			if (Session != null)
			{
				Session.RemoveAll();
				Session.Abandon();
			}

			if (Request != null && Request.Cookies != null)
				foreach (var cookie in Request.Cookies.AllKeys)
				{
					Request.Cookies.Remove(cookie);
				}
			if (Response != null && Response.Cookies != null)
				foreach (var cookie in Response.Cookies.AllKeys)
				{
					Response.Cookies.Remove(cookie);
				}

			// Invalidate the Cache on the Client Side
			if (Response != null)
			{
				Response.Cache.SetCacheability(HttpCacheability.NoCache);
				Response.Cache.SetNoStore();
			}
			UserPrincipal.RemoveUserDataCache(UserPrincipal.Current.UserName);
			webSecurity.Logout();

			return RedirectToAction("Login", "Account");
		}

		[AllowAnonymous]
		public ActionResult SingleMessage(string message)
		{
			ViewBag.Message = message;
			return View();
		}

		[AllowAnonymous]
		public ActionResult SingleMessageWithButton(string message, string strController, string strAction, string strButton)
		{
			var html = HtmlHelpers.ButtonAction(ControllerContext, strController, strAction, strButton);
			ViewBag.Message = message;
			ViewBag.HtmlRaw = html;
			//Elmah.ErrorLog.GetDefault(this.ControllerContext.HttpContext.ApplicationInstance.Context).Log(new Elmah.Error(new Exception(html.ToString())));
			return View("SingleMessage");
		}

		[HttpGet]
		public JsonResult UnlockUser(int userId)
		{
			bool result;
			try
			{
				accountMgr.UnlockUser(userId);
				result = true;
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				throw new InvalidOperationException(string.Format("Unlock user id: {0} failed.", userId), ex.InnerException);
			}
			return Json(new { result }, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser,ProductionUser,ProductionWlm")]
		public ActionResult ListUsers(string searchText, int? page, string sort, string sortdir)
		{

			ViewBag.ExcelExportAction = "ExportUsersReport";
			ViewBag.ExcelActionParam = new { searchText = searchText, page = page, sort = sort, sortdir = sortdir };
			var pageNum = page ?? 1;
			string sortString = string.IsNullOrEmpty(sort) ? "UserName" : sort;
			var sortOrder = string.IsNullOrEmpty(sortdir)
				? EnumUtils.Parse<SqlOrderByDirecton>("ASC")
				: EnumUtils.Parse<SqlOrderByDirecton>(sortdir);

			var model = accountMgr.GetDataAdminManageUser(searchText, pageNum, sortString, sortOrder);

			var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
				{
					{"searchText", searchText},
					{"page", pageNum},
					{"sort", sortString},
					{"sortdir", sortOrder}
				};
			Session[SessionHelper.SESSION_KEY_MANGE_USER_REPORT_LINK_DICT] = routeValuesDict;

			return View(model);
		}

		[HttpGet]
		public ActionResult ChangePasswordFromMenu()
		{
			return View("ChangePassword");
		}

		/// <summary>
		/// Signature feature
		/// </summary>
		/// <returns></returns>
		public ActionResult ChangeSignature()
		{
			int userId = UserPrincipal.Current.UserId;
			HUDSignatureModel objHUDSignatureModel = new HUDSignatureModel();
			SignatureModel objSignatureModel = new SignatureModel();
			//Prod_FormAmendmentTaskModel objProd_FormAmendmentTaskModel = new Prod_FormAmendmentTaskModel();
			objHUDSignatureModel.UserId = userId;
			objHUDSignatureModel = projectActionFormMgr.GetHUDSignatureByUserId(userId);
			objSignatureModel.CurrentSignature = objHUDSignatureModel.Signature;
			objSignatureModel.CreatedBy = userId;
			objSignatureModel.AgentSignatureFileName = userId.ToString() + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";

			return View(objSignatureModel);
		}


	[HttpPost]
	public ActionResult VerifySignature(string pUserId)
	{
			bool result = false;
			HUDSignatureModel objHUDSignatureModel = new HUDSignatureModel();
			objHUDSignatureModel = projectActionFormMgr.GetHUDSignatureByUserId(Convert.ToInt32(pUserId));

			if (objHUDSignatureModel != null && objHUDSignatureModel.Id > 0 && objHUDSignatureModel.Signature != null)
				result = true;			
			return Json(new { result }, JsonRequestBehavior.AllowGet);
		}
	

		/// <summary>
		/// save signature for users
		/// </summary>
		/// <param name="imageData"></param>
		/// <param name="userId"></param>
		/// <param name="userSigFile"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult SaveSignature(string imageData, string userId, string userSigFile)
		{
			int retSigId = 0;
			HUDSignatureModel objHUDSignatureModel = new HUDSignatureModel();
			HUDSignatureModel existingHUDSignatureModel = new HUDSignatureModel();
			byte[] data = null;
			if (string.IsNullOrEmpty(imageData))
			{
				objHUDSignatureModel = projectActionFormMgr.GetHUDSignatureByUserId(Convert.ToInt32(userId));
				data = objHUDSignatureModel.Signature;
			}
			else
			{
				data = Convert.FromBase64String(imageData);
			}
			objHUDSignatureModel.Signature = data;
			objHUDSignatureModel.UserId = Convert.ToInt32(userId);

			//string sigPathForSaving1 = Server.MapPath("~/Templates/Sig/Hud");
			//string fileNameWitPath1 = sigPathForSaving1 + objHUDSignatureModel.UserId.ToString() + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";
			//string sigPathForSaving = Server.MapPath("~/Templates/Sig/");
			//string fileNameWitPath = sigPathForSaving + userSigFile;

			//using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
			//{
			//	using (BinaryWriter bw = new BinaryWriter(fs))
			//	{
			//		bw.Write(data);
			//		bw.Close();
			//	}
			//}

			existingHUDSignatureModel = projectActionFormMgr.GetHUDSignatureByUserId(Convert.ToInt32(userId));
			if (existingHUDSignatureModel != null && existingHUDSignatureModel.Signature == null)
			{
				retSigId = projectActionFormMgr.AddNewSignature(objHUDSignatureModel);
			}
			else
			{
				projectActionFormMgr.UpdateHUDSignatureByUserId(objHUDSignatureModel);
			}
			return null;
		}

		public ActionResult ChangePassword()
        {
            return PartialView("_ChangePasswordPartial");
        }

        public ActionResult ChangePasswordNew(bool passwordExpired = false)
        {
            ViewBag.passwordExpired = passwordExpired;
            return PartialView("_ChangePasswordNew");
        }
        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        [HttpPost]
        public ActionResult ChangePassword(LocalPasswordModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (accountMgr.IsPasswordInHistory(UserPrincipal.Current.UserName, model.NewPassword))
                    {
                        model.IsChangePasswordSuccess = false;
                    }
                    else
                    {
                        model.IsChangePasswordSuccess = webSecurity.ChangePassword(UserPrincipal.Current.UserName, model.OldPassword, model.NewPassword);
                        TempData.Clear();
                        TempData["ChangePasswordSuccess"] = model.IsChangePasswordSuccess;
                        result = model.IsChangePasswordSuccess;
                    }

                    // FIFO password history pipe
                    if (model.IsChangePasswordSuccess)
                    {
                        accountMgr.SavePasswordHist(UserPrincipal.Current.UserName, model.NewPassword);
                    }
                }
                catch (MembershipPasswordException e)
                {
                    ModelState.AddModelError("ChangePasswordErr", e.Message);
                    throw new InvalidOperationException(string.Format("Error happened when changing password"), e.InnerException);
                }
            }
            if (result)
                return Json(new { result }, JsonRequestBehavior.AllowGet);
            return PartialView("_ChangePasswordPartial", model);
        }

        /// <summary>
        /// Registers this instance.
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Register()
        {
            var model = new UserViewModel();
            PopulateTimeZoneList(model);

            var newRoles = SessionHelper.SessionGet<List<string>>(SessionHelper.SESSION_KEY_NEW_ROLES);
            if (newRoles.Contains("LenderAccountRepresentative")
                || newRoles.Contains("LenderAccountManager")
                || newRoles.Contains("BackupAccountManager"))
            {
                if (!RoleManager.IsCurrentUserLenderAdmin())
                {
                    var lenders = accountMgr.GetInstitutionsByRoles(newRoles.ToArray());
                    model.AllLenders.Add(new SelectListItem() { Text = "Select Lender", Value = "-999" });
                    lenders.ToList().ForEach(p => model.AllLenders.Add(new SelectListItem() { Text = p.Value, Value = p.Key.ToString() }));
                }
                else
                {
                    model.SelectedLenderId = UserPrincipal.Current.LenderId;
                }
            }
            //if (newRoles.Contains("InternalSpecialOptionUser"))
            //{
            //    PopulateWorkloadManagerList(model);
            //}
            PopulateAllStateList(model);
            return View(model);
        }

        private void PopulateAllStateList(UserViewModel model)
        {
            accountMgr.GetAllStates()
                .ToList()
                .ForEach(p => model.AllStates.Add(new SelectListItem() { Text = p.Value, Value = p.Key }));
        }

        private static void PopulateTimeZoneList(UserViewModel model)
        {
            var timezones = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(HUDTimeZone)));
            model.AllTimezones = timezones.ToList();
        }

        /// <summary>
        /// Registers the workload manager.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult RegisterWorkloadManager()
        {
            var model = new UserViewModel();
            PopulateTimeZoneList(model);
            PopulateAllStateList(model);
            PopulateWorkloadManagerList(model);
            return View("~/Views/Account/Register.cshtml", model);
        }

        private void PopulateWorkloadManagerList(UserViewModel model)
        {
            model.AllWorkloadManagers.Add(new SelectListItem() { Text = "Select Workload Manager", Value = "-999" });
            var workloadManagers = accountMgr.GetWorkloadManagers();
            if (workloadManagers != null)
                foreach (var workloadManager in workloadManagers.ToList().Where(p => !p.Value.Trim().Equals("NOT ASSIGNED", StringComparison.OrdinalIgnoreCase)))
                {
                    model.AllWorkloadManagers.Add(new SelectListItem()
                    {
                        Text = workloadManager.Value.ToString(CultureInfo.InvariantCulture),
                        Value = workloadManager.Key.ToString(CultureInfo.InvariantCulture)
                    });
                }
        }

        /// <summary>
        /// Registers the account executive.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult RegisterAccountExecutive()
        {
            var model = new UserViewModel();
            PopulateTimeZoneList(model);
            PopulateAllStateList(model);
            PopulateAccountExecutiveList(model);
            return View("~/Views/Account/Register.cshtml", model);
        }

        private void PopulateAccountExecutiveList(UserViewModel model)
        {
            model.AllAccountExecutives.Add(new SelectListItem() { Text = "Select Account Executive", Value = "-999" });
            var accountExecutives = accountMgr.GetAccountExecutives();
            if (accountExecutives != null)
                foreach (var accountExecutive in accountExecutives.ToList().Where(p => !p.Value.Trim().Equals("NOT ASSIGNED", StringComparison.OrdinalIgnoreCase)))
                {
                    model.AllAccountExecutives.Add(new SelectListItem()
                    {
                        Text = accountExecutive.Value.ToString(CultureInfo.InvariantCulture),
                        Value = accountExecutive.Key.ToString(CultureInfo.InvariantCulture)
                    });
                }
        }

        //populate the lender dropdown
        private void PopulateLenderDropDown(UserViewModel model)
        {
            var newRoles = SessionHelper.SessionGet<List<string>>(SessionHelper.SESSION_KEY_NEW_ROLES);
            var lenders = accountMgr.GetInstitutionsByRoles(newRoles.ToArray());
            model.AllLenders.Add(new SelectListItem() { Text = "Select Lender", Value = "-999" });
            lenders.ToList().ForEach(p => model.AllLenders.Add(new SelectListItem() { Text = p.Value, Value = p.Key.ToString() }));
        }

        /// <summary>
        /// Registers the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserViewModel model)
        {
            model.ConfirmEmailAddress = model.ConfirmEmailAddress.ToLower();
            model.EmailAddress = model.EmailAddress.ToLower();
            var SelectedRoles = SessionHelper.SessionGet<List<string>>(SessionHelper.SESSION_KEY_NEW_ROLES);
            if (webSecurity.UserExists(model.UserName))
            {
                ModelState.AddModelError("UserExistsErr", "User with same email address already exists.");
            }

            if (model.SelectedLenderId != null && model.SelectedLenderId == -999)
            {
                ModelState.AddModelError("SelectLenderError", "Please select Lender.");
                //lender dropdown not populated, on validation
                PopulateLenderDropDown(model);
            }

            //if ((model.SelectedWorkloadManagerId != null && model.SelectedWorkloadManagerId == -999) ||
            //    (SelectedRoles.Contains("InternalSpecialOptionUser") &&
            //    (model.SelectedWorkloadManagerId == null || model.SelectedWorkloadManagerId == -999)))
            //{
            //    ModelState.AddModelError("SelectWorkloadManagerError", "Please select Workload Manager.");
            //}

            if (model.SelectedAccountExecutiveId != null && model.SelectedAccountExecutiveId == -999)
            {
                ModelState.AddModelError("SelectAccountExecutiveError", "Please select Account Executive.");
            }

            if (ModelState.IsValid)
            {
                // if login user is lender admin, take lender admin's lender id as selected lender id (invisible)
                if (RoleManager.IsCurrentUserLenderAdmin())
                {
                    var newRoles = SessionHelper.SessionGet<List<string>>(SessionHelper.SESSION_KEY_NEW_ROLES);
                    if (newRoles.Contains("LenderAccountRepresentative") || newRoles.Contains("LenderAccountManager") || newRoles.Contains("BackupAccountManager"))
                        model.SelectedLenderId = UserPrincipal.Current.LenderId;
                }
                // Attempt to register the user
                try
                {
                    // randomly generate strong password, 9 characters long
                    model.Password = RandomPassword.Generate(9);
                    string userToken = accountMgr.CreateUser(model);
                    int userId = webSecurity.GetUserId(model.UserName);
                    accountMgr.InitForceChangePassword(userId);
                    // save encrypted password history
                    accountMgr.SavePasswordHist(model.UserName, model.Password);

                    //Code to restrict sending Email to the configured Role
                    var Isblock = "FALSE";
                    var BlockRole = string.Empty;
                    string confirmLink = string.Empty;

                    if (ConfigurationManager.AppSettings["BlocUser"] != null)
                    {
                        Isblock = ConfigurationManager.AppSettings["BlocUser"].ToString();
                    }

                    if (Isblock.ToUpper() == "TRUE")
                    {
                        BlockRole = ConfigurationManager.AppSettings["BlockRole"].ToString();

                        if (SelectedRoles.Contains(BlockRole))
                        {
                            accountMgr.ActivateOrInactivateUser(userId, true);
                        }
                        else
                        {
                            var emailMgr = new EmailManager();
                            confirmLink = Url.Action("AgreementConfirm", "Account", new { id = userToken }, Request.Url.Scheme);
                            emailMgr.SendRegisterUserEmail(model, confirmLink);
                        }

                    }
                    else
                    {
                        var emailMgr = new EmailManager();
                        confirmLink = Url.Action("AgreementConfirm", "Account", new { id = userToken }, Request.Url.Scheme);
                        emailMgr.SendRegisterUserEmail(model, confirmLink);
                    }

                    return RedirectToAction("ListUsers");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ExceptionManager.WriteToEventLog(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State), "Application", EventLogEntryType.Error);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ExceptionManager.WriteToEventLog(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage), "Application", EventLogEntryType.Error);
                        }
                    }
                    throw;
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException(string.Format("Error from creating user: {0}", e.InnerException), e.InnerException);
                }
            }

            PopulateAllStateList(model);
            PopulateTimeZoneList(model);

            if (model.SelectedWorkloadManagerId != null)
            {
                PopulateWorkloadManagerList(model);
            }

            if (model.SelectedAccountExecutiveId != null)
            {
                PopulateAccountExecutiveList(model);
            }

            return View(model);
        }

        /// <summary>
        /// Registers the confirm.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult RegisterConfirm()
        {
            if (!Url.RequestContext.RouteData.Values.ContainsKey("id"))
                return RedirectToAction("SingleMessage", new { message = "Your are not allowed to access this page." });
            string userToken = Url.RequestContext.RouteData.Values["id"].ToString();
            var model = new SecurityQuestionViewModel();
            lookupMgr.GetAllSecurityQuestions().ToList()
                .ForEach(p => model.QuestionList.Add(new SelectListItem() { Text = p.SecurityQuestionDescription, Value = p.SecurityQuestionID.ToString() }));
            // if user token empty, then it is from first register confirm when user hasn't entered answers yet
            if (string.IsNullOrEmpty(userToken))
            {
                ModelState.AddModelError("", "User tried to access register confirmation page without token");
                return RedirectToAction("SingleMessage", new { message = "Your are not allowed to access this page." });
            }
            return View(model);
        }

        /// <summary>
        /// Registers the confirm.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterConfirm(SecurityQuestionViewModel model)
        {
            lookupMgr.GetAllSecurityQuestions().ToList()
                .ForEach(p => model.QuestionList.Add(new SelectListItem() { Text = p.SecurityQuestionDescription, Value = p.SecurityQuestionID.ToString() }));
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //karri:inorder to make this function generic ie while a new user is created and is asked for submission of his password hint data this function is called.
            //it saves the user submitted password hint data but this application do not have functionality for Updates or Delete+Insert, hence multiple rows gets created.
            //how to fetch the appropriate rows is described in UserSecQuestionsRepository.cs.
            //now if the call is from Users View/Update/Create Password Hints we catch in model.IsViewOrResttingPwdHints and validate and return appropriate view.
            if (model.IsViewOrResttingPwdHints)
            {
                var userIdVRPStngs = webSecurity.GetUserId(model.UserName);
                if (userIdVRPStngs <= 0)
                    return RedirectToAction("SingleMessage", new { message = "Your url is wrong." });

                // save security question answers
                accountMgr.SaveUserSecQuestions(userIdVRPStngs, model);
                ViewBag.Status = "Password Hints Saved!";

                return View("ViewOrResttingPwdHints", model);

            }

            string userToken = Url.RequestContext.RouteData.Values["id"].ToString();
            // get user name from user token
            var userId = accountMgr.GetUserIdByToken(userToken);
            if (!userId.HasValue)
                return RedirectToAction("SingleMessage", new { message = "Your url is wrong." });
            var user = accountMgr.GetUserById(userId.Value);
            if (webSecurity.IsConfirmed(user.UserName))
                return RedirectToAction("SingleMessage", new { message = "You finished registration confirmation already." });
            // model.Username = user.UserName;
            if (webSecurity.ConfirmAccount(userToken))
            {
                // save security question answers
                accountMgr.SaveUserSecQuestions(userId.Value, model);
                //return RedirectToAction("SingleMessage", new { message = "Registration complete!" });
                return RedirectToAction("SingleMessageWithButton", new
                {
                    message = "Registration complete!",
                    strController = "Account",
                    strAction = "Login",
                    strButton = "Go to login"
                });
            }
            //return RedirectToAction("SingleMessage", new { message = "Registration failed." });
            return RedirectToAction("SingleMessageWithButton", new
            {
                message = "Registration failed.",
                strController = "Account",
                strAction = "Login",
                strButton = "Go to login"
            });
        }

        /// <summary>
        /// Registers the user step1.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RegisterUserStep1(UserViewModel model)
        {
            return View(model);
        }

        /// <summary>
        /// Registers the user step1.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        public ActionResult RegisterUserStep1()
        {
            var model = new UserViewModel();
            var rolesSource = EnumType.GetEnumList(typeof(HUDRoleSource));
            var superRole = rolesSource.First(p => EnumType.ValueToEnum<HUDRoleSource>(p.EnumValue.ToString()) == HUDRoleSource.SuperUser);
            var internalRole = rolesSource.First(p => EnumType.ValueToEnum<HUDRoleSource>(p.EnumValue.ToString()) == HUDRoleSource.InternalUser);
            if (!accountMgr.GetHUDRoles().Contains(HUDRole.SuperUser))
                rolesSource.Remove(superRole);
            if (!accountMgr.GetHUDRoles().Contains(HUDRole.SuperUser) && !accountMgr.GetHUDRoles().Contains(HUDRole.HUDAdmin))
                rolesSource.Remove(internalRole);

            rolesSource.ForEach(p => model.AllRoleSources.Add(new SelectListItem() { Text = p.Value, Value = p.Key }));

            return View(model);
        }


        /// <summary>
        /// Registers the user step2.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RegisterUserStep2()
        {
            return RedirectToAction("Register");
        }

        /// <summary>
        /// Registers the servicer.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RegisterServicerIsou()
        {
            var model = new UserViewModel();
            var newRoles = SessionHelper.SessionGet<List<string>>(SessionHelper.SESSION_KEY_NEW_ROLES);
            if (newRoles.Contains("InternalSpecialOptionUser"))
            {
                PopulateWorkloadManagerList(model);
            }
            return View(model);
        }

        /// <summary>
        /// Registers the servicer.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RegisterServicer()
        {
            var model = new UserViewModel();
            var newRoles = SessionHelper.SessionGet<List<string>>(SessionHelper.SESSION_KEY_NEW_ROLES);
            return View();
        }

        /// <summary>
        /// Registers the operator1.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RegisterOperator1()
        {
            return View();
        }

        /// <summary>
        /// Registers the operator2.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RegisterOperator2()
        {
            return View();
        }

        /// <summary>
        /// Selects the fhas within lenders.
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectFhasWithinLenders()
        {
            return View();
        }

        /// <summary>
        /// Selects the fhas for Inspection Contractors.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RegisterInspectionContractor()
        {
            return View();
        }


        /// <summary>
        /// Agreements the confirm.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult AgreementConfirm()
        {
            LogOff();
            return View();
        }

        /// <summary>
        /// Shows the agreement.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ShowAgreement()
        {
            string agreemntFilePath = Server.MapPath(Url.Content("~/Content/AppFiles/RulesOfBehavior.docx"));
            return File(agreemntFilePath, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }

        /// <summary>
        /// Forgets the password.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgetPassword()
        {
            return View();
        }

        /// <summary>
        /// Forgets the password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgetPassword(string userName)
        {
            var userId = webSecurity.GetUserId(userName);
            if (userId < 0)
            {
                ViewBag.Message = "You are not registered in the Hhcp Portal.";
                return View();
            }
            var model = accountMgr.GetSecQuestionsByUserId(userId);
            model.IsAnswerStep = false;
            model.UserName = userName;
            lookupMgr.GetAllSecurityQuestions().ToList()
                .ForEach(p => model.QuestionList.Add(new SelectListItem() { Text = p.SecurityQuestionDescription, Value = p.SecurityQuestionID.ToString() }));
            //naveen 253
            //return View("RetrievePassword", model);
            return RetrievePassword(model);
        }

        /// <summary>
        /// Retrieves the password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RetrievePassword(SecurityQuestionViewModel model)
        {
            //karri
            if (model.IsViewOrResttingPwdHints)
            {
                return RegisterConfirm(model);

            }

            model.IsAnswerStep = false;
            lookupMgr.GetAllSecurityQuestions().ToList()
                .ForEach(p => model.QuestionList.Add(new SelectListItem() { Text = p.SecurityQuestionDescription, Value = p.SecurityQuestionID.ToString() }));
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            //naveen 253
            //if (model.FirstAnswer.Equals(model.UserAnswer1)
            //    && model.SecondAnswer.Equals(model.UserAnswer2)
            //    && model.ThirdAnswer.Equals(model.UserAnswer3))
            //{
            // reset password token expires in 2 days
                string resetPasswordToken = webSecurity.GeneratePasswordResetToken(model.UserName, 2880);
                // ReSharper disable once PossibleNullReferenceException
                string resetLink = Url.RouteUrl("ResetPwdRoute", new { controller = "Account", action = "ResetPassword", id = resetPasswordToken, usr = model.UserName }, Request.Url.Scheme);
                var emailMgr = new EmailManager();
                emailMgr.SendResetPasswordEmail(model.UserName, resetLink);
                return RedirectToAction("SingleMessageWithButton", new
                {
                    message = "An email was sent to your account to reset your password.",
                    strController = "Account",
                    strAction = "Login",
                    strButton = "Go to login"
                });
            //}
            ViewBag.Message = "One or more answers are incorrect.";
            return View(model);
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            if (!Url.RequestContext.RouteData.Values.ContainsKey("id"))
                return RedirectToAction("SingleMessageWithButton", new
                {
                    message = "Your are not allowed to access this page.",
                    strController = "Account",
                    strAction = "Login",
                    strButton = "Go to login"
                });
            return View();
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(NewPasswordModel model)
        {
            ViewBag.Message = string.Empty;
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            string resetPwdToken = Url.RequestContext.RouteData.Values["id"].ToString();
            string userName = Url.RequestContext.RouteData.Values["usr"].ToString();
            if (accountMgr.IsPasswordInHistory(userName, model.Password))
            {
                ViewBag.Message = "Cannot use past 8 passwords in history.";
                return View(model);
            }

            if (webSecurity.ResetPassword(resetPwdToken, model.Password))
            {
                // save FIFO password in history
                accountMgr.SavePasswordHist(userName, model.Password);
                return RedirectToAction("SingleMessageWithButton", new
                {
                    message = "Password reset.",
                    strController = "Account",
                    strAction = "Login",
                    strButton = "Go to login"
                });
            }
            return RedirectToAction("SingleMessageWithButton", new
            {
                message = "Reset password failed.",
                strController = "Account",
                strAction = "Login",
                strButton = "Go to login"
            });
        }

        /// <summary>
        /// Gets the role institutions.
        /// </summary>
        /// <param name="sRole">The s role.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Empty role</exception>
        [HttpGet]
        public ActionResult GetRoleInstitutions(string sRole)
        {
            if (String.IsNullOrEmpty(sRole))
            {
                throw new ArgumentNullException("Empty role");
            }
            var results = accountMgr.GetInstitutionsByRoles(new[] { sRole });
            return Json(results.Select(p => new { id = p.Key, name = p.Value }).OrderBy(p => p.name).ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// to make breadcrumb display properly on second level
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DoNothing()
        {
            return new EmptyResult();
        }

        /// <summary>
        /// Searches the last name of the by first or.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="maxResults">The maximum results.</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult SearchByFirstOrLastName(String searchText, int maxResults)
        {
            List<UserViewModel> dataSet = accountMgr.SearchByFirstOrLastName(searchText, maxResults).ToList();
            var results = dataSet.Where(
                p =>
                    (p.FirstName.StartsWith(searchText, true, CultureInfo.InvariantCulture) ||
                     p.LastName.StartsWith(searchText, true, CultureInfo.InvariantCulture)))
                .Take(maxResults)
                .OrderBy(p => p.FirstName)
                .Select(p => new { FirstName = p.FirstName, MiddleName = p.MiddleName, LastName = p.LastName });
            var json = Json(results, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// Searches the by first or last name page sort.
        /// </summary>
        /// <param name="searchTerm">The search term.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNum">The page number.</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult SearchByFirstOrLastNamePageSort(string searchTerm, int pageSize, int pageNum)
        {

            //List<UserViewModel> dataSet = new List<UserViewModel>();
            var dataSet = accountMgr.SearchByFirstOrLastNamePageSort(searchTerm, "LastName", SqlOrderByDirecton.ASC, pageSize, pageNum);
            var results = dataSet.Entities
                .Select(p => new
                {
                    id = p.UserName,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    MiddleName = p.MiddleName,
                    Email = p.UserName,
                    Organization = p.Organization,
                    Total = dataSet.TotalRows
                });
            var json = Json(results, JsonRequestBehavior.AllowGet);
            return json;
        }


        /// <summary>
        /// Gets the last name of the user by first and.
        /// we are not using this method anywhere obsolete
        /// </summary>
        /// <param name="firstAndLastName">First name of the and last.</param>
        /// <param name="sortdir">First name of the and last.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetUserByFirstAndLastName(String firstAndLastName, int page, string sort, SqlOrderByDirecton sortdir)
        {
            List<UserViewModel> dataSet = new List<UserViewModel>();
            string[] name = firstAndLastName.Split(new Char[] { ',' });
            dataSet = accountMgr.GetAllUsers(firstAndLastName, page, sort, sortdir).ToList();
            var results = dataSet.Where(p => (p.FirstName == name[0] && p.LastName == name[1]));
            return PartialView("ListUsers", results);
        }

        [HttpGet]
        public ActionResult SearchUsersByText(String searchText, int? page, string sort, string sortdir)
        {
            ViewBag.ExcelExportAction = "ExportUsersReport";
            ViewBag.ExcelActionParam = new { searchText = searchText, page = page, sort = sort, sortdir = sortdir };

            int pageNum = page ?? 1;
            string sortString = string.IsNullOrEmpty(sort) ? "UserName" : sort;
            var sortOrder = string.IsNullOrEmpty(sortdir)
                ? EnumUtils.Parse<SqlOrderByDirecton>("ASC")
                : EnumUtils.Parse<SqlOrderByDirecton>(sortdir);

            var results = accountMgr.GetDataAdminManageUser(searchText, pageNum, sortString, sortOrder).ToList();
            return PartialView("_ListUsers", results);
        }

        /// <summary>
        /// Goes to manageOperatorFhas view.
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        [HttpGet]
        public ActionResult ManageOperatorFhas()
        {
            return View();
        }

        /// <summary>
        /// Goes to ManageInspectorFhas view.
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        [HttpGet]
        public ActionResult ManageInspectorFhas()
        {
            return View();
        }

        /// <summary>
        /// Forwards to edit user view.
        /// </summary>
        /// <param></param>
        /// <returns>Edit User View</returns>

        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        [HttpGet]
        public ActionResult ForwardToEditUserProfileView()
        {
            UserViewModel user = (UserViewModel)Session[SessionHelper.SESSION_KEY_MANAGING_OAR];
            return GoToEditUserProfileView(user.UserID);
        }

        /// <summary>
        /// Goes to edit user view.
        /// </summary>
        /// <param name="id">User ID.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GoToEditUserProfileView(int id)
        {
            var results = accountMgr.GetUserById(id);

            var timezones = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(HUDTimeZone)));
            results.AllTimezones = timezones.ToList();

            results.AllStates.Add(new SelectListItem() { Text = "Select State", Value = "Select" });
            accountMgr.GetAllStates().ToList().ForEach(p => results.AllStates.Add(new SelectListItem() { Text = p.Value, Value = p.Key }));
            string[] userRoles = Roles.GetRolesForUser(results.UserName);
            //Convert list to comma separated string
            results.SelectedRoles = userRoles == null ? string.Empty : String.Join(WebUiConstants.Comma, userRoles);
            if (userRoles != null && (userRoles.Contains("OperatorAccountRepresentative") || userRoles.Contains("InternalSpecialOptionUser") || userRoles.Contains("InspectionContractor")))
            {
                Session[SessionHelper.SESSION_KEY_MANAGING_OAR] = results;
            }
            if (userRoles.Contains("InternalSpecialOptionUser"))
            {
                PopulateWorkloadManagerList(results);
                results.SelectedWorkloadManagerId = taskRAManager.GetWorkloadManagerIdFromInternalSpecialOptionUserId(id);
            }

            List<ReviewerTitlesModel> titles = new List<ReviewerTitlesModel>();
            if (results.SelectedRoles == HUDRole.Reviewer.ToString("g"))
            {
                titles = reviewerTitl.GetReviewerTitles(2);
            }
            else
            {
                titles = reviewerTitl.GetReviewerTitles(1);
            }
            titles.ToList().ForEach(p => results.AllTitles.Add(new SelectListItem() { Text = p.Reviewer_Name, Value = p.Reviewer_Id.ToString() }));

            ControllerHelper.SetMangeUserSessionRouteValuesToTempData(TempData);
            return View("EditUserProfile", results);
        }


        /// <summary>
        /// Saves the edit user.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveEditUserProfile(UserViewModel model)
        {
            accountMgr.UpdateUser(model);
            return RedirectToAction("ListUsers");
        }

        /// <summary>
        /// Gets the workload manager detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetWorkloadManagerDetail(int id)
        {
            var result = accountMgr.GetWorkloadManagerDetail(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the account executive detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult GetAccountExecutiveDetail(int id)
        {
            var result = accountMgr.GetAccountExecutiveDetail(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Helpers

        /// <summary>
        /// Redirects to local.
        /// </summary>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        public ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// 
        /// </summary>
        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        /// <summary>
        /// Errors the code to string.
        /// </summary>
        /// <param name="createStatus">The create status.</param>
        /// <returns></returns>
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        /// <summary>
        /// Posts from Login Transition page to the user selected role landing page.
        /// </summary>
        /// <param name="userProfile">The user profile.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        //#380
        [HttpPost]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,AccountExecutive,WorkflowManager,InternalSpecialOptionUser,HUDDirector,Attorney,Servicer,ProductionUser,ProductionWlm")]
        public ActionResult LoginTransition(UserProfile userProfile, string returnUrl)
        {
            UserPrincipal.Current.UserRole = userProfile.UserRole.ToString();
            //login logic

            var userid = webSecurity.GetUserId(userProfile.UserName);

            UserLoginModel ulmodel = new UserLoginModel();
            ulmodel.Login_Time = DateTime.UtcNow;
            ulmodel.RoleName = UserPrincipal.Current.UserRole;
            Session[SessionHelper.SESSION_KEY_LOGINUSERROLE] = UserPrincipal.Current.UserRole;
            Session["UserRole"] = UserPrincipal.Current.UserRole;   // dual role changes
            ulmodel.Session_Id = HttpContext.Session.SessionID;
            ulmodel.CreatedOn = DateTime.UtcNow;
            ulmodel.ModifiedOn = DateTime.UtcNow;
            ulmodel.CreatedBy = userid;
            ulmodel.ModifiedBy = userid;
            ulmodel.User_id = userid;


            var successguid = ulMgr.SaveUserLogin(ulmodel);
            return PerformRedirectionBasedOnAuthentication(UserPrincipal.Current.UserName, returnUrl);
        }

        /// <summary>
        /// Provides the Login Transistion View with available roles for an authenticated user.
        /// </summary>
        /// <returns></returns>
        //#380
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,AccountExecutive,WorkflowManager,InternalSpecialOptionUser,HUDDirector,Attorney,Servicer,ProductionUser,ProductionWlm")]
        public ActionResult LoginTransition()
        {
            var model = new UserProfile { UserName = UserPrincipal.Current.UserName };
            var roles = Roles.GetRolesForUser(model.UserName).ToList();

            foreach (string role in roles)
            {
                model.AvailableUserRoles.Add(new SelectListItem() { Text = EnumUtils.Parse<HUDRole>(role).ToName(), Value = role });
            }
            return View(model);
        }

        /// <summary>
        /// Method to make the user active/inactive
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isUserInactive"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ActivateOrInactivateUser(int userId, bool isUserInactive)
        {
            bool result;
            try
            {
                accountMgr.ActivateOrInactivateUser(userId, isUserInactive);
                result = true;
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                throw new InvalidOperationException(string.Format("Unlock user id: {0} failed.", userId), ex.InnerException);
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoginDetail(int id)
        {
            var userid = id;

            var model = ulMgr.GetLoginHistory(id);
            return PartialView("LoginDetail", model);
        }

        //Project action Questions Data Entry Form related functions are below


        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        public ActionResult DataEntryFormForQuestionsIndex()
        {
            return View("~/Views/Account/DataEntryFormForQuestions.cshtml");
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        public ActionResult QuestionDataEntryIndex()
        {
            //IList<ProjectActionViewModel> projectActionList = projectActionMgr.GetAllProjectActions();
            var model = new QuestionDataEntryViewModel();
            /*if (projectActionList != null)
            {
                projectActionList.ForEach(
                    p =>
                        model.AllProjectActions.Add(new SelectListItem()
                        {
                            Text = p.ProjectActionName,
                            Value = p.ProjectActionID.ToString()
                        }));

            }*/
            return View("~/Views/Account/QuestionDataEntryIndex.cshtml", model);
        }

        public ActionResult TestNGGrid()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllFormTypes()
        {
            IList<FormTypeModel> projectActionList = accountMgr.GetAllFormTypesForProjectAction();
            return Json(projectActionList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllProjectActions(int pageTypeId)
        {
            IList<ProjectActionTypeViewModel> projectActionList = projectActionMgr.GetAllProjectActionsByPageId(pageTypeId);
            return Json(projectActionList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllQuestions(int projectActionTypeId)
        {
            var allQuestions = questionMgr.GetAllQuestions(projectActionTypeId);
            return Json(allQuestions, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string AddQuestion(QuestionViewModel questionViewModel)
        {
            questionViewModel.CreatedBy = UserPrincipal.Current.UserId;
            questionViewModel.CreatedOn = DateTime.Now;
            questionViewModel.ModifiedBy = UserPrincipal.Current.UserId;
            questionViewModel.ModifiedOn = DateTime.Now;
            questionMgr.AddQuestion(questionViewModel);
            return "Question added";
        }

        [HttpPost]
        public JsonResult GetQuestionById(QuestionViewModel questionViewModel)
        {
            var question = questionMgr.GetQuestion(questionViewModel.CheckListId);
            return Json(question, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public string UpdateQuestion(QuestionViewModel questionViewModel)
        {
            questionViewModel.ModifiedBy = UserPrincipal.Current.UserId;
            questionViewModel.ModifiedOn = DateTime.Now;
            questionMgr.UpdateQuestion(questionViewModel);
            return "Question Updated";
        }

        public JsonResult GetPasswordExpireInfo()
        {
            var userName = UserPrincipal.Current.UserName;
            PasswordExpiredModel passwordExpiredModelsword = new PasswordExpiredModel();
            if (userName != null)
            {
                var passwordChangedDate = webSecurity.GetPasswordChangedDate(userName);
                var passwordToChangeDate = passwordChangedDate.AddDays(AccountManager.ChangePasswordInDays);
                if ((DateTime.UtcNow - passwordChangedDate).TotalDays <= AccountManager.ChangePasswordInDays
                    && (passwordToChangeDate - DateTime.UtcNow).TotalDays <= AccountManager.DaysToAskChangePassword)
                {

                    TempData["bShowChangePasswordMsg"] = true;
                    passwordExpiredModelsword.ShowPasswordExpiredMsg = true;
                    TempData["ExpireInDays"] = (int)(passwordToChangeDate - DateTime.UtcNow).TotalDays + 1;
                    passwordExpiredModelsword.ExpireInDays = (int)(passwordToChangeDate - DateTime.UtcNow).TotalDays + 1;
                    PopupHelper.ConfigPopup(TempData, 640, 340, "Change Password", false, false, true, false, true, false,
                        "../Account/ChangePassword");
                }
                else if ((DateTime.UtcNow - passwordChangedDate).TotalDays > AccountManager.ChangePasswordInDays)
                {
                    TempData["PasswordExpires"] = true;
                    passwordExpiredModelsword.IsPasswordExpired = true;
                    PopupHelper.ConfigPopup(TempData, 640, 340, "Change Password", false, false, true, false, false, true,
                       "../Account/ChangePassword");
                }
            }
            return Json(passwordExpiredModelsword, JsonRequestBehavior.AllowGet);


        }

        //karri
        [HttpGet]
        public ActionResult ViewChangePasswordHints()
        {
            ViewBag.Status = string.Empty;
            var userId = webSecurity.GetUserId(UserPrincipal.Current.UserName);
            var qmodel = accountMgr.GetSecQuestionsByUserId(userId);
            qmodel.IsAnswerStep = true;
            qmodel.IsViewOrResttingPwdHints = true;
            qmodel.UserName = UserPrincipal.Current.UserName;
            lookupMgr.GetAllSecurityQuestions().ToList()
                .ForEach(p => qmodel.QuestionList.Add(new SelectListItem() { Text = p.SecurityQuestionDescription, Value = p.SecurityQuestionID.ToString() }));
            return View("ViewOrResttingPwdHints", qmodel);

        }

        //naveen//called from menu:Administration:Loans Data(Upload)
        [HttpGet]
        public ActionResult UploadLaonasData11()
        {
            //if necessary pass the moel data to the view
            return View("ExceltoDb");

        }

        //naveen;after file upload from the view
        [HttpPost]
        public ActionResult ExceltoDb(HttpPostedFileBase File)
        {
            try
            {
                exceltodb db = new exceltodb();
                if (File != null)
                {
                    //if (File.ContentType == "application/vnd.ms-excel" || File.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    //{
                        string filename = File.FileName;

                        if (filename.EndsWith(".xlsx"))
                        {
                            string targetpath = Server.MapPath("~/Content/DetailFormatInExcel/");
                            File.SaveAs(targetpath + filename);
                            string pathToExcelFile = targetpath + filename;

                            OleDbConnectionStringBuilder sbConnection = new OleDbConnectionStringBuilder();
                            String strExtendedProperties = String.Empty;
                            sbConnection.DataSource = pathToExcelFile;
                            if (Path.GetExtension(pathToExcelFile).Equals(".xls"))//for 97-03 Excel file
                            {
                                sbConnection.Provider = "Microsoft.Jet.OLEDB.4.0";
                                strExtendedProperties = "Excel 8.0;HDR=Yes;IMEX=1";//HDR=ColumnHeader,IMEX=InterMixed
                            }
                            else if (Path.GetExtension(pathToExcelFile).Equals(".xlsx"))  //for 2007 Excel file
                            {
                                sbConnection.Provider = "Microsoft.ACE.OLEDB.12.0";
                                strExtendedProperties = "Excel 12.0;HDR=Yes;IMEX=1";
                            }
                            sbConnection.Add("Extended Properties", strExtendedProperties);
                            List<string> listSheet = new List<string>();
                            using (OleDbConnection conn = new OleDbConnection(sbConnection.ToString()))
                            {
                                conn.Open();
                                DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                foreach (DataRow drSheet in dtSheet.Rows)
                                {
                                    if (drSheet["TABLE_NAME"].ToString().Contains("$"))//checks whether row contains '_xlnm#_FilterDatabase' or sheet name(i.e. sheet name always ends with $ sign)
                                    {
                                        listSheet.Add(drSheet["TABLE_NAME"].ToString());
                                    }
                                }
                            }

                            var sheet = listSheet.First().Replace("$", "");

                            string sheetName = "Loans";


                            if (sheet == sheetName)
                            {
                                var excelFile = new ExcelQueryFactory(pathToExcelFile);
                                var empDetails = from a in excelFile.Worksheet<Loans_82817_New>(sheetName) select a;

                                foreach (var a in empDetails)
                                {

                                    var InsertExcelData = new Loans_82817_New();

                                    InsertExcelData.Portfolio_Number = a.Portfolio_Number;
                                    InsertExcelData.Portfolio_Name = a.Portfolio_Name;
                                    InsertExcelData.Master_Lease_Number = a.Master_Lease_Number;
                                    InsertExcelData.Credit_Review = a.Credit_Review;
                                    InsertExcelData.property_id = a.property_id;
                                    InsertExcelData.fha_number_with_suffix = a.fha_number_with_suffix;
                                    InsertExcelData.Medicare_Provider_Number = a.Medicare_Provider_Number;
                                    InsertExcelData.soa_code = a.soa_code;
                                    InsertExcelData.primary_loan_code = a.primary_loan_code;
                                    InsertExcelData.soa_numeric_name = a.soa_numeric_name;
                                    InsertExcelData.initial_endorsement_date = a.initial_endorsement_date;
                                    InsertExcelData.amortized_unpaid_principal_bal = a.amortized_unpaid_principal_bal;
                                    InsertExcelData.property_name_text = a.property_name_text;
                                    InsertExcelData.address_line1_text = a.address_line1_text;
                                    InsertExcelData.address_line2_text = a.address_line2_text;
                                    InsertExcelData.city_name_text = a.city_name_text;
                                    InsertExcelData.state_code = a.state_code;
                                    InsertExcelData.zip_code = a.zip_code;
                                    InsertExcelData.troubled_code = a.troubled_code;
                                    InsertExcelData.is_active_dec_case_ind = a.is_active_dec_case_ind;
                                    InsertExcelData.reac_last_inspection_score = a.reac_last_inspection_score;
                                    InsertExcelData.NCRE_Balance = a.NCRE_Balance;
                                    InsertExcelData.Account_Executive = a.Account_Executive;
                                    InsertExcelData.HUD_Account_Executive_Telephone_Number = a.HUD_Account_Executive_Telephone_Number;
                                    InsertExcelData.HUD_Account_Executive_Email_Address = a.HUD_Account_Executive_Email_Address;
                                    InsertExcelData.Workload_Manager = a.Workload_Manager;
                                    InsertExcelData.HUD_Workload_Manager_Telephone_Number = a.HUD_Workload_Manager_Telephone_Number;
                                    InsertExcelData.HUD_Workload_Manager_Email_Address = a.HUD_Workload_Manager_Email_Address;
                                    InsertExcelData.Lender_Mortgagee_ID = a.Lender_Mortgagee_ID;
                                    InsertExcelData.Lender = a.Lender;
                                    InsertExcelData.Lender_Contact_Name = a.Lender_Contact_Name;
                                    InsertExcelData.Lender_Contact_Title = a.Lender_Contact_Title;
                                    InsertExcelData.Lender_Contact_Telephone_Number = a.Lender_Contact_Telephone_Number;
                                    InsertExcelData.Lender_Contact_Email_Address = a.Lender_Contact_Email_Address;
                                    InsertExcelData.Lender_Contact_Street_Address_1 = a.Lender_Contact_Street_Address_1;
                                    InsertExcelData.Lender_Contact_Street_Address_2 = a.Lender_Contact_Street_Address_2;
                                    InsertExcelData.Lender_Contact_City = a.Lender_Contact_City;
                                    InsertExcelData.Lender_Contact_State = a.Lender_Contact_State;
                                    InsertExcelData.Lender_Contact_Zip_Code = a.Lender_Contact_Zip_Code;
                                    InsertExcelData.Lender_Contact_Zip_Code___4 = a.Lender_Contact_Zip_Code___4;
                                    InsertExcelData.Servicer_Mortgagee_ID = a.Servicer_Mortgagee_ID;
                                    InsertExcelData.Servicer = a.Servicer;
                                    InsertExcelData.Servicer_Contact_Name = a.Servicer_Contact_Name;
                                    InsertExcelData.Servicer_Contact_Title = a.Servicer_Contact_Title;
                                    InsertExcelData.Servicer_Contact_Telephone_Number = a.Servicer_Contact_Telephone_Number;
                                    InsertExcelData.Servicer_Contact_Email_Address = a.Servicer_Contact_Email_Address;
                                    InsertExcelData.Servicer_Contact_Street_Address_1 = a.Servicer_Contact_Street_Address_1;
                                    InsertExcelData.Servicer_Contact_Street_Address_2 = a.Servicer_Contact_Street_Address_2;
                                    InsertExcelData.Servicer_Contact_City = a.Servicer_Contact_City;
                                    InsertExcelData.Servicer_Contact_State = a.Servicer_Contact_State;
                                    InsertExcelData.Servicer_Contact_Zip_Code = a.Servicer_Contact_Zip_Code;
                                    InsertExcelData.Servicer_Contact_Zip___4 = a.Servicer_Contact_Zip___4;
                                    InsertExcelData.owner_organization_name = a.owner_organization_name;
                                    InsertExcelData.owner_address_line1 = a.owner_address_line1;
                                    InsertExcelData.owner_address_line2 = a.owner_address_line2;
                                    InsertExcelData.owner_city_name = a.owner_city_name;
                                    InsertExcelData.owner_state_code = a.owner_state_code;
                                    InsertExcelData.owner_zip_code = a.owner_zip_code;
                                    InsertExcelData.owner_main_phone_number_text = a.owner_main_phone_number_text;
                                    InsertExcelData.owner_email_text = a.owner_email_text;
                                    InsertExcelData.owner_contact_indv_full_name = a.owner_contact_indv_full_name;
                                    InsertExcelData.owner_contact_indv_title_text = a.owner_contact_indv_title_text;
                                    InsertExcelData.owner_contact_address_line1 = a.owner_contact_address_line1;
                                    InsertExcelData.owner_contact_address_line2 = a.owner_contact_address_line2;
                                    InsertExcelData.owner_contact_city_name = a.owner_contact_city_name;
                                    InsertExcelData.owner_contact_state_code = a.owner_contact_state_code;
                                    InsertExcelData.owner_contact_zip_code = a.owner_contact_zip_code;
                                    InsertExcelData.owner_contact_main_phone_num = a.owner_contact_main_phone_num;
                                    InsertExcelData.owner_contact_email_text = a.owner_contact_email_text;
                                    InsertExcelData.mgmt_agent_org_name = a.mgmt_agent_org_name;
                                    InsertExcelData.mgmt_agent_address_line1 = a.mgmt_agent_address_line1;
                                    InsertExcelData.mgmt_agent_address_line2 = a.mgmt_agent_address_line2;
                                    InsertExcelData.mgmt_agent_city_name = a.mgmt_agent_city_name;
                                    InsertExcelData.mgmt_agent_state_code = a.mgmt_agent_state_code;
                                    InsertExcelData.mgmt_agent_zip_code = a.mgmt_agent_zip_code;
                                    InsertExcelData.mgmt_agent_main_phone_number = a.mgmt_agent_main_phone_number;
                                    InsertExcelData.mgmt_agent_email_text = a.mgmt_agent_email_text;
                                    InsertExcelData.mgmt_contact_full_name = a.mgmt_contact_full_name;
                                    InsertExcelData.mgmt_contact_indv_title_text = a.mgmt_contact_indv_title_text;
                                    InsertExcelData.mgmt_contact_address_line1 = a.mgmt_contact_address_line1;
                                    InsertExcelData.mgmt_contact_address_line2 = a.mgmt_contact_address_line2;
                                    InsertExcelData.mgmt_contact_city_name = a.mgmt_contact_city_name;
                                    InsertExcelData.mgmt_contact_state_code = a.mgmt_contact_state_code;
                                    InsertExcelData.mgmt_contact_zip_code = a.mgmt_contact_zip_code;
                                    InsertExcelData.mgmt_contact_main_phn_nbr = a.mgmt_contact_main_phn_nbr;
                                    InsertExcelData.mgmt_contact_email_text = a.mgmt_contact_email_text;
                                    InsertExcelData.is_nursing_home_ind = a.is_nursing_home_ind;
                                    InsertExcelData.mddr_in_default_status_name = a.mddr_in_default_status_name;

                                    db.Loans_82817_New.Add(InsertExcelData);


                                }
                                db.SaveChanges();
                                ViewBag.Message = string.Format("Data uploaded successfully");
                            }
                            else
                            {
                                ViewBag.Message = string.Format("Sheet name should be Loans in excel file uploaded and it should be in first place");
                            }
                        }
                    //}
                    //else
                    //{
                    //    ViewBag.Message = string.Format("Please Upload excel file");
                    //}
                }
            }
            catch (Exception)
            {
                ViewBag.Message = string.Format("Something went worng!");
            }
            return View();
        }

        //karri#63//this is '232PortfolioExcel' view rendering function, 
        //'Upload232PortFolio' is the bulk excel data submission action method
        [HttpGet]
        public ActionResult Upload232Portfolio()
        {
            //if necessary pass the moel data to the view
            return View("232PortfolioExcel");

        }

        //karri#63
        [HttpPost]
        public ActionResult Upload232PortFolio(HttpPostedFileBase File)
        {
            SqlConnection con = null;
            SqlBulkCopy sqlBulkCopy = null;
            SqlCommand truncate = null;
            SqlTransaction transaction = null;
            
            OleDbConnection connExcel = null;
            OleDbCommand cmdExcel = null;
            OleDbDataAdapter odaExcel = null;

            try
            {
                //Mapping of columns; xml file with mappings will be placed in a path and will ask
                // the user to select Profile (Table name) and corresponding  columns will be displayed
                //to select by the user to push into DB
                //??Validation is required for a file after uploading to find the right file in place or not
                if (ConfigurationManager.AppSettings["BulkUploadMapingColumnsPath"].Equals(null)
                    || string.IsNullOrEmpty(ConfigurationManager.AppSettings["BulkUploadMapingColumnsPath"].ToString())
                    || !System.IO.File.Exists(ConfigurationManager.AppSettings["BulkUploadMapingColumnsPath"].ToString() + "BulkUploadColMappings.xml"))
                    throw new Exception("Bulk File Column mappings file not found or null");

                string colmappingfile = ConfigurationManager.AppSettings["BulkUploadMapingColumnsPath"].ToString() + "BulkUploadColMappings.xml";
                DataSet dsmapping = new DataSet("Mappings");
                dsmapping.ReadXml(colmappingfile);

                if(dsmapping.Tables.Count.Equals(0))
                    throw new Exception("Bulk File Column mappings file not found or null");
                                
                string filePath = string.Empty;
                exceltodb db = new exceltodb();
                if (File != null)
                {
                    //folder if not exists creates a new else keeps the existing
                    string path = Server.MapPath("~/232PortFolioUploads/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    //saving the file i the prescribed path
                    filePath = path + Path.GetFileName(File.FileName);
                    string extension = Path.GetExtension(File.FileName);
                    File.SaveAs(filePath);

                    //connection objects for excel file, string is in web.config
                    string conString = string.Empty;
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03.
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx": //Excel 07 and above.
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }

                    //open th excel file, read the file and push the data into temp data table
                    DataTable dt = new DataTable();
                    conString = string.Format(conString, filePath);

                    using (connExcel = new OleDbConnection(conString))
                    {
                        using (cmdExcel = new OleDbCommand())
                        {
                            using (odaExcel = new OleDbDataAdapter())
                            {
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string Loans = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcel.CommandText = "SELECT * From [" + Loans + "]";
                                odaExcel.SelectCommand = cmdExcel;
                                odaExcel.Fill(dt);
                                connExcel.Close();

                            }
                        }
                    }


                    //Mapping of columns
                    //inserting into DB 
                    string destTbleName = dsmapping.Tables[0].TableName + "$";

                    conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                    using (con = new SqlConnection(conString))
                    {
                        //we have to open connection here for linking the transactions
                        con.Open();

                        using (transaction = con.BeginTransaction())
                        {
                            truncate = new SqlCommand("DELETE FROM " + destTbleName, con, transaction);
                            truncate.ExecuteNonQuery();                                                      

                            using (sqlBulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, transaction))
                            {
                                sqlBulkCopy.DestinationTableName = destTbleName;

                                foreach (DataRow dr in dsmapping.Tables[0].Rows)
                                {
                                    string srcColName = dr["SourceColName"].ToString();
                                    string destColName = dr["DestiColName"].ToString();
                                    sqlBulkCopy.ColumnMappings.Add(srcColName, destColName);
                                                                       
                                }

                                sqlBulkCopy.WriteToServer(dt);
                            }

                            transaction.Commit();
                            //naveen 22/may/2019
                            SqlCommand ExecJob = new SqlCommand();
                            ExecJob.CommandType = CommandType.StoredProcedure;
                            ExecJob.CommandText = "msdb.dbo.sp_start_job";
                            ExecJob.Parameters.AddWithValue("@job_name", "j_run_excel_upload");
                            ExecJob.Connection = con;
                            ExecJob.ExecuteNonQuery();


                            con.Close();

                            ViewBag.Message = "Data Uploaded Successfully";
                        }
                        
                    }

                }
            }
            catch(SqlException e)
            {
                if(e.Number == 11)
                {
                    ViewBag.Message = string.Format("Already another job running, Please wait for sometime.");
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();

                ViewBag.Message = string.Format("Something went worng!" + ex.Message + "; " + ex.Source + "; " + ex.StackTrace);
            }
            finally
            {
                if (connExcel != null && connExcel.State.Equals(ConnectionState.Open))
                    connExcel.Close();

                if(con != null && con.State.Equals(ConnectionState.Open))
                    con.Close();
            }
            return View("232PortfolioExcel");
        }

        //naveen:#189
        [HttpGet]
        public ActionResult LenderCreation()
        {
            return View();
        }

        //naveen:#189
        [HttpPost]
        public ActionResult LenderCreation(Lenderdata obj)
        {
            _lenderMgr.Addlender(obj);
            ViewBag.message = "Lender created successfully.";
            ModelState.Clear();
            return View();
        }

        // naveen
        [HttpPost]
        public JsonResult checkUserEmailId(UserViewModel model, string email)
        {

            //return Json(accountMgr.checkUserEmailId(email), JsonRequestBehavior.AllowGet);

            if (webSecurity.UserExists(email))
            {
                model.IsUserExist = true;
                model.UserName = email;
                //accountMgr.CreateUser(model);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult CreateSpecialoptionuser(UserViewModel model, string email)
        //{

        //    if (webSecurity.UserExists(email))
        //    {
        //        model.IsUserExist = true;
        //        model.UserName = email;
        //        accountMgr.CreateUser(model);
        //        //accountMgr.UpdateUser(model);
        //    }
        //    //return View("~/Views/Account/ListUsers.cshtml", model);
        //    return RedirectToAction("ListUsers");

        //    //return View(model);
        //}


        [HttpPost]
        public JsonResult CreateSpecialoptionuser(UserViewModel model, string email)
        {

            if (webSecurity.UserExists(email))
            {
                model.IsUserExist = true;
                model.UserName = email;
                accountMgr.CreateUser(model);
                //accountMgr.UpdateUser(model);
            }
            //return View("~/Views/Account/ListUsers.cshtml", model);
            return Json(new { redirectTo = Url.Action("ListUsers", "Account"), }, JsonRequestBehavior.AllowGet);
            // return View(model);
            //return View(model);
        }







        public ActionResult EmailPopUp()
        {
            var model = new UserViewModel();

            return PartialView("_CheckUser_email", model);
        }


        //karri#354
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,LenderAccountManager,BackupAccountManager,SuperUser")]
        public ActionResult UnRegisterServicer()
        {
            string sortString = "UserName";
            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>("ASC");

            //Naveen#543
            int? lenderid = UserPrincipal.Current.LenderId;
            var UserListModel = accountMgr.GetServicer(lenderid);
            var model1 = new UserViewModel();

            foreach (var item in UserListModel)
            {
                //hdt.TicketStatusList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });
                model1.AllServicerUsers.Add(new SelectListItem() { Text = item.UserName, Value = item.UserID.ToString() });
            }

            //var model = new UserViewModel();
            //var rolesSource = EnumType.GetEnumList(typeof(HUDRoleSource));
            //var superRole = rolesSource.First(p => EnumType.ValueToEnum<HUDRoleSource>(p.EnumValue.ToString()) == HUDRoleSource.SuperUser);
            //var internalRole = rolesSource.First(p => EnumType.ValueToEnum<HUDRoleSource>(p.EnumValue.ToString()) == HUDRoleSource.InternalUser);
            //if (!accountMgr.GetHUDRoles().Contains(HUDRole.SuperUser))
            //    rolesSource.Remove(superRole);
            //if (!accountMgr.GetHUDRoles().Contains(HUDRole.SuperUser) && !accountMgr.GetHUDRoles().Contains(HUDRole.HUDAdmin))
            //    rolesSource.Remove(internalRole);

            //rolesSource.ForEach(p => model.AllRoleSources.Add(new SelectListItem() { Text = p.Value, Value = p.Key }));

            return View("UnRegisterServicer", model1);
        }

    }//end of Account controller



        }


