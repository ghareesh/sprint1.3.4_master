﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BusinessService.ProjectAction;
using Core.Utilities;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using Model.ProjectAction;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;
using BusinessService.AssetManagement;// User Story 1901
using System.Collections;

namespace HUDHealthcarePortal.Controllers.ProjectAction
{
    public class GroupTaskController : Controller
    {
        //
        // GET: /GroupTask/
        private IGroupTaskManager _groupTaskManager;
        private IProjectActionFormManager _projectActionFormManager;
        private ITaskManager _taskManager;
        private INonCriticalRepairsRequestManager _nonCriticalRepairsManager;// User Story 1901

        public GroupTaskController()
            : this(new GroupTaskManager(), new ProjectActionFormManager(), new TaskManager(), new NonCriticalRepairsRequestManager())
        {

        }

        private GroupTaskController(GroupTaskManager groupTaskManager, ProjectActionFormManager projectActionFormManager, TaskManager taskManager, NonCriticalRepairsRequestManager nonCriticalRepairsRequestManager)
        {
            _groupTaskManager = groupTaskManager;
            _projectActionFormManager = projectActionFormManager;
            _taskManager = taskManager;
            _nonCriticalRepairsManager = nonCriticalRepairsRequestManager;//User Story 1901
        }

        public ActionResult Index(int? page, string sort, string sortDir, string lookupFHANumber = null)
        {
            if (UserPrincipal.Current.LenderId != null)
            {
                var userLenderId = (int)UserPrincipal.Current.LenderId;

                //karri#205
                IList<String> UserFHAList = _groupTaskManager.getFHAList(userLenderId);
                if (lookupFHANumber != null && !string.IsNullOrEmpty(lookupFHANumber))
                    _groupTaskManager.setLookUpValues(lookupFHANumber);

                var model = _groupTaskManager.GetGroupTasksForLender(userLenderId, page ?? 1,
                    string.IsNullOrEmpty(sort) ? "ModifiedOn" : sort,
                    string.IsNullOrEmpty(sortDir)
                    ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                    : EnumUtils.Parse<SqlOrderByDirecton>(sortDir));

                var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
                {
                    {"page", page},
                    {"sort", sort},
                    {"sortdir", sortDir}
                };
                Session[SessionHelper.SESSION_KEY_GROUP_TASKS_LINK_DICT] = routeValuesDict;
                //restricting only to draft mode
                // model.Entities = model.Entities.Where(a => a.RequestStatus == 6);

                ////karri#205               
                ViewBag.FHAList = UserFHAList;
                @ViewData["BindingFHANumber"] = lookupFHANumber;

                return View("~/Views/ProjectAction/GroupTaskView.cshtml", model);
            }
            return View();
        }

        //karri#205
        private List<GroupTaskModel> getMyFilteredTasks(IEnumerable<GroupTaskModel> rawData, string srchFHANumber)
        {
            if (srchFHANumber != null && !string.IsNullOrEmpty(srchFHANumber))
                return rawData.Where(m => m.FhaNumber == srchFHANumber).ToList();
            else
                return rawData.ToList();
        }

        public ActionResult GetGroupTaskDetail(int taskId)
        {
            //Commeted for Project Action(OPA)
            //var projectActionViewModel = _groupTaskManager.GetProjectActionViewModelFromGroupTask(taskId);

            //projectActionViewModel.ContactAddressViewModel = _projectActionFormManager.GetContactAddressInfo(projectActionViewModel).ContactAddressViewModel;
            //projectActionViewModel.ProjectActionName =
            //    _projectActionFormManager.GetProjectActionName(projectActionViewModel.ProjectActionTypeId);
            //projectActionViewModel.IsViewFromGroupTask = true;
            //return View("~/Views/ProjectAction/ProjectActionRequestFormAEandReadOnlyView.cshtml", projectActionViewModel);
            var model = _groupTaskManager.GetOPAProjectActionViewModelFromGroupTask(taskId);
            var task = _taskManager.GetLatestTaskByTaskInstanceId((Guid)model.GroupTaskInstanceId);
            if (task != null)
                if (task.DataStore1.Contains("OPAViewModel"))
                {
                    var formUploadData = XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1, Encoding.Unicode) as OPAViewModel;
                    if (formUploadData != null)
                    {
                        model.IsAddressChange = formUploadData.IsAddressChange;
                        model.IsAgreementAccepted = formUploadData.IsAgreementAccepted;
                        //model.ServicerSubmissionDate = TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
                        model.ServicerSubmissionDate = (DateTime)formUploadData.ServicerSubmissionDate;
                    }
                }

            var result = _projectActionFormManager.GetPropertyInfo(model.FhaNumber);
            if (result != null)
            {
                model.PropertyId = result.PropertyId;
                model.PropertyAddress.AddressLine1 = result.StreetAddress ?? "";
                model.PropertyAddress.City = result.City ?? "";
                model.PropertyAddress.StateCode = result.State ?? "";
                model.PropertyAddress.ZIP = result.Zipcode ?? "";

            }

            //  var taskFileModel = _taskManager.GetAllTaskFileByTaskInstanceId(new Guid(model.GroupTaskInstanceId.ToString()));
            // model.TaskFileList = taskFileModel.ToList();

            //model.ProjectActionName =
            //    _projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);

            model.ProjectActionName =
                _projectActionFormManager.GetProjectActionNameforAssetmanagement(model.ProjectActionTypeId);
            model.IsViewFromGroupTask = true;
            //if(model.RequestStatus==6)
            //{
            //    model.ServicerSubmissionDate = TimezoneManager.GetPreferredTimeFromUtc(DateTime.UtcNow);
            //}
            //else
            //{

            //}

            GetDisclaimerText(model);//User Story 1901
            model.IsAgreementAccepted = false;//User Story 1901
            model.OpaHistoryViewModel = _projectActionFormManager.GetOpaHistory((Guid)model.GroupTaskInstanceId);

            return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", model);
        }

        // User Story 1901 
        private void GetDisclaimerText(OPAViewModel model)
        {

            model.DisclaimerText = _nonCriticalRepairsManager.GetDisclaimerMsgByPageType("OPA");


        }


        public ActionResult UnlockGroupTask(int taskId)
        {
            if (RoleManager.IsLenderAdmin(UserPrincipal.Current.UserName))
            {
                var userLenderId = UserPrincipal.Current.LenderId;
                if (userLenderId != null)
                {

                    ControllerHelper.SetGroupTaskSessionRouteValuesToTempData(TempData);
                    var groupTasks = _groupTaskManager.GetGroupTasksForLender((int)userLenderId, TempData["page"] == null ? 1 : (int)TempData["page"],
                        string.IsNullOrEmpty(TempData["sort"] == null ? "" : (string)TempData["sort"]) ? "ModifiedOn" : (string)TempData["sort"],
                        string.IsNullOrEmpty(TempData["sortDir"] == null ? "" : (string)TempData["sortDir"])
                    ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                    : EnumUtils.Parse<SqlOrderByDirecton>((string)TempData["sortDir"]));
                    foreach (var model in groupTasks.Entities)
                    {
                        if (model.TaskId == taskId)
                        {
                            model.InUse = 0;
                            model.UserNameInUse = "";
                            model.UserRole = "";
                            _groupTaskManager.UnlockGroupTask(taskId);
                        }
                    }
                    return View("~/Views/ProjectAction/GroupTaskView.cshtml", groupTasks);
                }

            }

            return null;
        }
    }
}
