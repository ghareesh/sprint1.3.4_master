﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Windows.Forms;
using BusinessService.Interfaces;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using BusinessService.ProjectAction;
using HUDHealthCarePortal.Helpers;
using Model.ProjectAction;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using WebGrease.Css.Extensions;


namespace HUDHealthcarePortal.Controllers.ProjectAction
{
    public class ProjectActionRequestController : Controller
    {
        private IProjectActionFormManager projectActionFormManager;
        private IProjectActionManager projectActionManager;
        private IGroupTaskManager groupTaskManager;
        private ITaskManager taskManager;
        private IAccountManager accountManager;
        private IEmailManager emailManager;
        private IBackgroundJobMgr backgroundJobManager;

        public ProjectActionRequestController()
            : this(new ProjectActionFormManager(), new TaskManager(), new AccountManager(), new EmailManager(), new BackgroundJobMgr(), new ProjectActionManager(), new GroupTaskManager())
        {

        }

        private ProjectActionRequestController(IProjectActionFormManager _projectActionFormManager, ITaskManager _taskManager, IAccountManager _accountManager, IEmailManager _emailManager, IBackgroundJobMgr backgroundManager, IProjectActionManager _projectActionManager, IGroupTaskManager _groupTaskManager)
        {
            projectActionFormManager = _projectActionFormManager;
            taskManager = _taskManager;
            accountManager = _accountManager;
            emailManager = _emailManager;
            backgroundJobManager = backgroundManager;
            projectActionManager = _projectActionManager;
            groupTaskManager = _groupTaskManager;
        }

        public ActionResult Index()
        {
            ProjectActionViewModel projectActionViewModel = new ProjectActionViewModel();
            InitializeViewModel(projectActionViewModel);
            //return View("~/Views/ProjectAction/ProjectActionRequestForm.cshtml", projectActionViewModel);
            return View("~/Views/ProjectAction/ProjectActionRequestForm.cshtml", projectActionViewModel);
        }

        private void InitializeViewModel(ProjectActionViewModel projectActionViewModel)
        {
            ViewBag.SaveCheckListFormURL = Url.Action("ApproveCheckListForm");
            PopulateFHANumberList(projectActionViewModel);
            IList<ProjectActionTypeViewModel> projectActionList = projectActionManager.GetAllProjectActions();
            foreach (var model in projectActionList)
            {
                projectActionViewModel.ProjectActionTypeList.Add(new SelectListItem() { Text = model.ProjectActionName, Value = model.ProjectActionID.ToString() });
            }
            projectActionViewModel.LenderId = UserPrincipal.Current.UserData.LenderId.Value;
            projectActionViewModel.LenderName = projectActionFormManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            projectActionViewModel.RequesterName = UserPrincipal.Current.FullName;
            projectActionViewModel.ProjectActionDate = DateTime.Today;
            projectActionViewModel.ServicerSubmissionDate = DateTime.Today;
            projectActionViewModel.IsAgreementAccepted = false;
            projectActionViewModel.RequestDate = null;
            projectActionViewModel.CreatedBy = UserPrincipal.Current.UserId;
            projectActionViewModel.CreatedOn = DateTime.UtcNow;
            projectActionViewModel.ModifiedOn = DateTime.UtcNow;
            projectActionViewModel.ModifiedBy = UserPrincipal.Current.UserId;
        }

        private void PopulateFHANumberList(ProjectActionViewModel projectActionViewModel)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                fhaNumberList =
                    projectActionFormManager.GetAllowedFhaByLenderUserId(UserPrincipal.Current.UserId);
            }

            projectActionViewModel.AvailableFHANumbersList = fhaNumberList;
        }

        private void PopulateCheckList(ProjectActionViewModel projectActionViewModel, bool submitted = false, Guid taskInstanceID = new Guid())
        {
            var checkList = new List<QuestionViewModel>();
            var checklistWithSection = new Dictionary<string, IList<QuestionViewModel>>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) || RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) || (projectActionViewModel.IsPAMReport) || (projectActionViewModel.IsEditMode))
            {
                checklistWithSection = (Dictionary<string, IList<QuestionViewModel>>) projectActionFormManager.GetCheckListByActionType(projectActionViewModel.ProjectActionFormId, projectActionViewModel.ProjectActionTypeId, submitted, taskInstanceID);
            }

            projectActionViewModel.Questions = checkList;
            projectActionViewModel.QuestionsWithSections = checklistWithSection;
        }

        public ActionResult GetPropertyInfo(string selectedFhaNumber, ProjectActionViewModel projectActionViewModel)
        {
            var result = projectActionFormManager.GetPropertyInfo(selectedFhaNumber);
            if (projectActionViewModel == null)
            {
                projectActionViewModel = new ProjectActionViewModel();
                InitializeViewModel(projectActionViewModel);
            }
            projectActionViewModel.FhaNumber = selectedFhaNumber;
            projectActionViewModel.PropertyName = result.PropertyName;
            projectActionViewModel.PropertyId = result.PropertyId;
            projectActionViewModel.ContactAddressViewModel = GetContactAddressInfo(projectActionViewModel);
            return PartialView("~/Views/ProjectAction/ProjectActionRequestForm.cshtml", projectActionViewModel);
        }

        public ContactAddressViewModel GetContactAddressInfo(ProjectActionViewModel projectActionViewModel)
        {
            return projectActionFormManager.GetContactAddressInfo(projectActionViewModel).ContactAddressViewModel;
        }

        public bool CheckIfProjectActionExistsForFhaNumber(int projectActionTypeId, string fhaNumber)
        {
            return groupTaskManager.CheckIfProjectActionExistsForFhaNumber(projectActionTypeId, fhaNumber);
        }


        public ActionResult GetProjectActionRequestPage(ProjectActionViewModel projectActionViewModel)
        {

            var projActViewModel = groupTaskManager.GetProjectActionViewModelFromGroupTask(projectActionViewModel.GroupTaskId.Value);

            var results = projectActionFormManager.GetPropertyInfo(projActViewModel.FhaNumber);
            projectActionViewModel.PropertyId = results.PropertyId;

            if (UserPrincipal.Current.LenderId != null)
                projActViewModel.LenderId = (int)UserPrincipal.Current.LenderId;

            projActViewModel.LenderName = projectActionFormManager.GetLenderName(projectActionViewModel.LenderId);
            projActViewModel.ProjectActionName = projectActionFormManager.GetProjectActionName(projActViewModel.ProjectActionTypeId);

            projActViewModel.ContactAddressViewModel = projectActionFormManager.GetContactAddressInfo(projectActionViewModel).ContactAddressViewModel;

            return View("~/Views/ProjectAction/ProjectActionRequestFormAEandReadOnlyView.cshtml", projActViewModel);
        }

        public ActionResult SubmitForm(ProjectActionViewModel projectActionViewModel, string prevBtn,
            string nextBtn)
        {
            if (nextBtn != null)
            {
                PopulateCheckList(projectActionViewModel);
                ViewBag.GoProjectActionFormURL = Url.Action("GetProjectActionRequestPage");
                ViewBag.GoToCheckListForm = Url.Action("GoToChecklistForm");
                if (ModelState.IsValid)
                {
                    var groupTaskModel = new GroupTaskModel();
                    if (projectActionViewModel != null)
                    {
                        if (!projectActionViewModel.IsEditMode)
                        {
                        groupTaskModel.TaskInstanceId = Guid.NewGuid();
                        groupTaskModel.RequestStatus = (int) RequestStatus.Draft;
                        groupTaskModel.InUse = UserPrincipal.Current.UserId;
                        groupTaskModel.FhaNumber = projectActionViewModel.FhaNumber;
                        groupTaskModel.PropertyName = projectActionViewModel.PropertyName;
                        groupTaskModel.RequestDate = projectActionViewModel.RequestDate;
                        groupTaskModel.RequesterName = projectActionViewModel.RequesterName;
                        groupTaskModel.ServicerSubmissionDate = projectActionViewModel.ServicerSubmissionDate;
                        groupTaskModel.ProjectActionStartDate = DateTime.UtcNow;
                        groupTaskModel.ProjectActionTypeId = projectActionViewModel.ProjectActionTypeId;
                        groupTaskModel.IsDisclimerAccepted = projectActionViewModel.IsAgreementAccepted;
                        groupTaskModel.CreatedBy = projectActionViewModel.CreatedBy;
                        groupTaskModel.CreatedOn = projectActionViewModel.CreatedOn;
                        groupTaskModel.ModifiedBy = projectActionViewModel.ModifiedBy;
                        groupTaskModel.ModifiedOn = projectActionViewModel.ModifiedOn;
                        groupTaskModel.ServicerComments = projectActionViewModel.ServicerComments;
                        projectActionViewModel.GroupTaskId = groupTaskManager.AddNewGroupTask(groupTaskModel);
                            projectActionViewModel.IsEditMode = true;
                            projectActionViewModel.RequestStatus = (int)RequestStatus.Draft;
                        groupTaskModel.TaskId = (int) projectActionViewModel.GroupTaskId;
                        projectActionViewModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                        projectActionViewModel.GroupTaskCreatedOn = DateTime.UtcNow;
                        projectActionViewModel.ProjectActionDate = groupTaskModel.ProjectActionStartDate.Value;
                        }
                        projectActionViewModel.ProjectActionName = projectActionFormManager.GetProjectActionName(projectActionViewModel.ProjectActionTypeId);
                    }
                    return View("~/Views/ProjectAction/ProjectActionUploadCheckListAccordion.cshtml", projectActionViewModel);
                }
            }
            return View("~/Views/ProjectAction/ProjectActionRequestForm.cshtml", projectActionViewModel);
        }


        public ActionResult ApproveCheckListForm(ProjectActionViewModel projectActionViewModel)
        {
            var successFlag = true;
            var messageText = string.Empty; 
            try{
                projectActionFormManager.UpdateAECheckList(projectActionViewModel);
              }
              catch (Exception e)
              {
                    ErrorSignal.FromCurrentContext().Raise(e);
                    successFlag = false;
                  messageText = "Error Occurred, please contact adminstrator";
              }

            return Json(new { success = successFlag, message = messageText }, JsonRequestBehavior.AllowGet);
  
        }


        /// <summary>
        /// Navigate to the part 2 screen - based on Lender/AE
        /// </summary>
        /// <param name="projectActionViewModel"></param>
        /// <param name="prevBtn"></param>
        /// <param name="nextBtn"></param>
        /// <returns></returns>
        public ActionResult GoToChecklistForm(ProjectActionViewModel projectActionViewModel, string prevBtn, string nextBtn)
        {
            if (nextBtn != null)
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    projectActionViewModel.IsLoggedInUserLender = true;
                }
                ViewBag.GoProjectActionFormURL = Url.Action("GetProjectActionRequestPage");
                ViewBag.GoToCheckListForm = Url.Action("GoToChecklistForm");
                var taskinstanceId = projectActionViewModel.RequestStatus == (int) RequestStatus.Draft   ? projectActionViewModel.GroupTaskInstanceId.Value : projectActionViewModel.TaskGuid.HasValue  ? projectActionViewModel.TaskGuid.Value : Guid.Empty;

                PopulateCheckList(projectActionViewModel, true, taskinstanceId);
                projectActionViewModel.IsDisplayApproveBtn =
                    projectActionFormManager.IsApprovalStatusTrueForAllQuestions(
                        projectActionViewModel.ProjectActionFormId); 

                if (projectActionViewModel.IsPAMReport)
                {
                    var routeValues = (RouteValueDictionary) Session["RouteValues"];
                    ViewData["wlmIds"] = routeValues["wlmIds"];
                    ViewData["aeIds"] = routeValues["aeIds"];
                    ViewData["fromDate"] = routeValues["fromDate"];
                    ViewData["toDate"] = routeValues["toDate"];
                    ViewData["status"] = routeValues["status"];
                    ViewData["projectAction"] = routeValues["projectAction"];
                    ViewData["page"] = routeValues["page"];
                    ViewData["sort"] = routeValues["sort"];
                    ViewData["sortdir"] = routeValues["sortdir"];
                    Session["RouteValues"] = null;
                }

               var  prjActionApprovedOrDenied = projectActionFormManager.IsProjectActionApprovedOrDenied(
                            projectActionViewModel.ProjectActionFormId);
                if (prjActionApprovedOrDenied)
                {
                    projectActionViewModel.IsEditMode = false;
                }

                if (projectActionViewModel.IsEditMode && !RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
                {
                    //return View("~/Views/ProjectAction/ProjectActionUploadCheckList.cshtml", projectActionViewModel);
                    return View("~/Views/ProjectAction/ProjectActionUploadCheckListAccordion.cshtml", projectActionViewModel);
                }
                if (projectActionViewModel.IsViewFromGroupTask)
                {
                    ControllerHelper.SetGroupTaskSessionRouteValuesToTempData(TempData);
                }
                
            }

            return View("~/Views/ProjectAction/ProjectActionUploadCheckList_AE_ReadOnly.cshtml", projectActionViewModel);
        }

        /// <summary>
        /// Uploads the file.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult UploadFile()
        {
            HttpPostedFileBase myFile = Request.Files[0];
            bool isUploaded = false;
            string message = "File upload failed";

            if (myFile != null && myFile.ContentLength != 0)
            {
                var fhaNumber = Request.Form["FhaNumber"];
                var chkListId = Request.Form["CheckListId"];
                var groupTaskInstanceId = Request.Form["GroupTaskInstanceId"];
                var projectActionTypeId = Request.Form["ProjectActionTypeId"];
                var groupTaskCreatedOn = Request.Form["GroupTaskCreatedOn"];

                var systemFileName = fhaNumber + "_" + projectActionTypeId + "_" + groupTaskInstanceId + "_" + chkListId + Path.GetExtension(myFile.FileName);
                double fileSize = (myFile.ContentLength) / 1024;
                string pathForSaving = Server.MapPath("~/Uploads");
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                        isUploaded = true;
                        message = "File uploaded successfully!";
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }

                var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile,fileSize, chkListId, groupTaskCreatedOn, systemFileName, new Guid(groupTaskInstanceId));
                var taskFileModel = taskManager.GetGroupTaskFileByTaskInstanceAndFileTypeId(new Guid(groupTaskInstanceId), new Guid(chkListId));
                if (taskFileModel != null)
                {
                    taskManager.DeleteGroupTaskFile(new Guid(groupTaskInstanceId), new Guid(chkListId));
                }
                taskManager.SaveGroupTaskFileModel(taskFile);
            }
            return Json(new { isUploaded = isUploaded, message = message }, "text/html");
        }

        /// <summary>
        /// Creates the folder if needed.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        [HttpGet]
        public FileResult DownloadTaskFile(string taskInstanceId, string taskFileType)
        {
            TaskFileModel taskFile = taskManager.GetGroupTaskFileByTaskInstanceAndFileTypeId(new Guid(taskInstanceId), new Guid(taskFileType));

            if (taskFile != null)
            {
                string filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                var fileInfo = new System.IO.FileInfo(filePath);
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition",
                    String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.WriteFile(filePath);
                Response.End();
            }
            // file download from db code
            //if (taskFile != null)
            //{
            //    return File(taskFile.FileData, "text/text", taskFile.FileName);
            //}
            return null;
        }

        [HttpGet]
        public JsonResult DeleteGroupTaskFile(string taskInstanceId, string taskFileType)
        {
            TaskFileModel taskFile = taskManager.GetGroupTaskFileByTaskInstanceAndFileTypeId(new Guid(taskInstanceId), new Guid(taskFileType));
            int success = 0;
            if (taskFile != null)
            {
                string pathForSaving = Server.MapPath("~/Uploads");
                success = taskManager.DeleteGroupTaskFile(new Guid(taskInstanceId), new Guid(taskFileType));
                System.IO.File.Delete(pathForSaving + taskFile.SystemFileName);
            }

            if (success == 1)
            {
                return Json(new { success = true, CheckListId = taskFileType }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FinalSubmitForm(ProjectActionViewModel model, Guid? groupTaskGuid)
        {
            if (ModelState.IsValid)
            {
                //Insert the non critical request and get the generated id, insert into view model
                model.ServicerSubmissionDate = DateTime.Today;
                model.RequestStatus = (int)RequestStatus.Submit;
                model.ModifiedOn = DateTime.UtcNow;
                model.ModifiedBy = UserPrincipal.Current.UserId;  
                var projectActionFormId = projectActionFormManager.SaveProjectActionRequestForm(model);
                model.ProjectActionFormId = projectActionFormId;
                groupTaskManager.UpdateGroupTask(model);
                var taskList = new List<TaskModel>();
                var task = new TaskModel();

                task.AssignedTo = projectActionFormManager.GetAeEmailByFhaNumber(model.FhaNumber);

                task.AssignedBy = UserPrincipal.Current.UserName;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(ProjectActionViewModel), Encoding.Unicode);
                task.Notes = model.ServicerComments;
                task.SequenceId = 0;
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = model.GroupTaskInstanceId.Value;

                task.TaskStepId = (int)TaskStep.ProjectActionRequest;
                taskList.Add(task);
                var taskFile = new TaskFileModel();
                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    //Latest task id been updated on submission
                    model.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    projectActionFormManager.UpdateTaskId(model);
                    projectActionFormManager.InsertAEChecklistStatus(model);
                    backgroundJobManager.SendProjectActionSubmissionEmail(emailManager, task.AssignedTo, accountManager.GetUserById(model.CreatedBy).UserName, model);
                    UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                    string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);
                    return RedirectToAction("MyTasks", "Task");
                }
                catch (Exception)
                {

                }

            }
            return View("~/Views/ProjectAction/ProjectActionRequestForm.cshtml", model);
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetProjectActionRequestFormDetail(ProjectActionViewModel model)
        {
            if (model == null)
            {
                model = (ProjectActionViewModel)TempData["ProjectActionFormData"];
            }
            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
            model.ProjectActionName = projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
            model.ContactAddressViewModel = GetContactAddressInfo(model);
            if (model.SequenceId.HasValue && model.SequenceId == 1 && RoleManager.IsUserLenderRole(model.AssignedTo) && !model.IsPAMReport && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                var task = new TaskModel();
                var taskList = new List<TaskModel>();
                task.AssignedBy = model.AssignedBy;
                task.AssignedTo = model.AssignedTo;
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                task.TaskOpenStatus = XmlHelper.Serialize(model.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                var taskFile = new TaskFileModel();
                try
                {
                    task.DataStore1 = XmlHelper.Serialize(model, typeof(ProjectActionViewModel), Encoding.Unicode);
                    taskList.Add(task);
                    taskManager.SaveTask(taskList, taskFile);
                    model.MytaskId  = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    projectActionFormManager.UpdateTaskId(model);
                }
                catch (Exception e)
                {
                    ErrorSignal.FromCurrentContext().Raise(e);
                    throw new InvalidOperationException(
                        string.Format("Error project action request form detail: {0}", e.InnerException), e.InnerException);
                }
            } 
            return View("~/Views/ProjectAction/ProjectActionRequestFormAEandReadOnlyView.cshtml", model);
        }

        public ActionResult ApproveOrDeny(ProjectActionViewModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                model.RequesterName = UserPrincipal.Current.UserName;

                var taskList = new List<TaskModel>();
                var task = new TaskModel();

                string alertTitle = String.Empty;

                task.AssignedTo = model.AssignedBy;
                task.AssignedBy = UserPrincipal.Current.UserName;
                TempData["IsReAssigned"] = model.IsReassigned.HasValue && model.IsReassigned.Value == true ? "true": "false" ;
                if (model.RequestStatus == (int)RequestStatus.Approve)
                {
                    TempData["PopupText"] = "Project Action Request was approved.";
                    alertTitle = "Project Action Approved";
                    task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                }
                else
                {
                    TempData["PopupText"] = "Project Action Request was denied.";
                    alertTitle = "Project Action Denied";
                    task.TaskStepId = (int)TaskStep.ProjectActionRequestComplete;
                }

                task.DataStore1 = XmlHelper.Serialize(model, typeof(ProjectActionViewModel), Encoding.Unicode);
                task.Notes = model.AEComments;
                task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                task.IsReAssigned = model.IsReassigned;
                task.Concurrency = concurrency;

                var taskOpenStatusModel = new TaskOpenStatusModel();
                taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                taskList.Add(task);
                var taskFile = new List<TaskFileModel>();

                try
                {
                    taskManager.SaveTask(taskList, taskFile);
                    model.MytaskId = projectActionFormManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    projectActionFormManager.UpdateTaskId(model);
                    projectActionFormManager.UpdateProjectActionRequestForm(model);
                    groupTaskManager.UpdateGroupTask(model);
                    if (backgroundJobManager.SendProjectActionSubmissionResponseEmail(emailManager, task.AssignedTo, task.AssignedBy, model))
                    {
                        if (!model.IsApprovedPopupDisplayedForAe)
                        {
                            model.IsApprovedPopupDisplayedForAe = true;
                            PopupHelper.ConfigPopup(TempData, 380, 220, alertTitle, false, false, true, false, false, true, "GenericPopup");

                            model.ProjectActionName = projectActionFormManager.GetProjectActionName(model.ProjectActionTypeId);
                            PopulateCheckList(model, true, model.TaskGuid ?? Guid.Empty);

                            return View("~/Views/ProjectAction/ProjectActionUploadCheckList_AE_ReadOnly.cshtml", model);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {

                }
            }

            return RedirectToAction("MyTasks", "Task");
        }

        public ActionResult GenericPopup()
        {
            return PartialView("~/Views/ProjectAction/_GenericPopup.cshtml");
        }

        [HttpGet]
        public JsonResult CheckoutProjectActionForm(int groupTaskId, byte[] concurrency)
        {
            try
            {
                var groupTaskModel = groupTaskManager.GetGroupTaskById(groupTaskId);
                groupTaskModel.InUse = UserPrincipal.Current.UserId;
                groupTaskModel.Concurrency = concurrency;
                var projectActionViewModel = groupTaskManager.GetProjectActionViewModelFromGroupTask(groupTaskId);
                var results = projectActionFormManager.GetPropertyInfo(groupTaskModel.FhaNumber);
                projectActionViewModel.PropertyId = results.PropertyId;
                projectActionViewModel.LenderName = projectActionFormManager.GetLenderName(projectActionViewModel.LenderId);
                projectActionViewModel.ProjectActionName = projectActionFormManager.GetProjectActionName(groupTaskModel.ProjectActionTypeId);

                projectActionViewModel.IsEditMode = true;
                projectActionViewModel.GroupTaskId = groupTaskId;
                projectActionViewModel.GroupTaskInstanceId = groupTaskModel.TaskInstanceId;
                projectActionViewModel.ContactAddressViewModel = GetContactAddressInfo(projectActionViewModel);
                groupTaskManager.UpdateGroupTaskForCheckout(groupTaskModel);
            }
            catch (DbUpdateConcurrencyException)
            {
                return Json("concurrency", JsonRequestBehavior.AllowGet);
            }
           
            return Json("success", JsonRequestBehavior.AllowGet); 
        }

        public ActionResult CheckInProjectAction(int taskId)
        {
            groupTaskManager.UnlockGroupTask(taskId);
            return RedirectToAction("Index", "GroupTask");
        }
    }
}
