﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using Core.Utilities;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Helpers;
using Model;
using MvcSiteMapProvider;
using System.Collections;

namespace HUDHealthcarePortal.Controllers.TaskReassignment
{
    public class TaskReAssignmentController : Controller
    {
        //
        // GET: /TaskReassignment/
        private IPAMReportManager _pamReportManager;
        private ITaskReAssignmentManager _taskReAssignmentManager;
        private ITaskManager _taskMgr;
        private IBackgroundJobMgr _backgroundJobManager;
        private IEmailManager _emailManager;

        public TaskReAssignmentController()
            : this(new PAMReportsManager(), new TaskReAssignmentManager(), new TaskManager(),
                new BackgroundJobMgr(), new EmailManager())
        {

        }

        public TaskReAssignmentController(IPAMReportManager pamReportManager, ITaskReAssignmentManager taskReAssignmentManager,
            ITaskManager taskMgr, IBackgroundJobMgr backgroundJobManager, IEmailManager emailManager)
        {
            _pamReportManager = pamReportManager;
            _taskReAssignmentManager = taskReAssignmentManager;
            _taskMgr = taskMgr;
            _backgroundJobManager = backgroundJobManager;
            _emailManager = emailManager;
        }

        public ActionResult Index()
        {
            var viewModel = new TaskReAssignmentViewModel();
            var wlmlist = new List<KeyValuePair<int, string>>();
            wlmlist = _pamReportManager.GetAllWorkLoadManagers().ToList();
            viewModel.WLMListItems = new MultiSelectList(wlmlist, "Key", "Value");

            viewModel.AEListItems = new List<SelectListItem>();
            if (HUDHealthcarePortal.BusinessService.RoleManager.IsWorkloadManagerRole(UserPrincipal.Current.UserName))
            {
                var wlmid = _pamReportManager.GetWlmId(UserPrincipal.Current.UserId);
                viewModel.defaultwlm = wlmid.ToString();
                var aes = _taskReAssignmentManager.GetRegisteredAEsByWLMId(wlmid.ToString());

                var rows = new List<SelectListItem>(aes.Count + 1);
                //rows.Add(new SelectListItem() { Text = "Select AE", Value = "" });
                foreach (Model.HUDManagerModel m in aes)
                {
                    rows.Add(new SelectListItem() { Text = m.Manager_Name, Value = m.Manager_ID.ToString() });
                }
                viewModel.AEListItems = rows;

                var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "wlmIds", wlmid.ToString() } };
                Session[SessionHelper.SESSION_KEY_TASK_REASSIGNMENT_DICT] = routeValuesDict;
            }

            //var isous = _taskReAssignmentManager.GetISOUIdsByWLMId();

            viewModel.ReassignAEListItems = new List<SelectListItem>();
            viewModel.ReassignISOUListItems = new List<SelectListItem>();

            var pamprojectActionTypeDict = (Dictionary<int, string>)_pamReportManager.GetPamProjectionTypes().ToDictionary(g => g.Key, g => g.Value);
            var keys = pamprojectActionTypeDict.Keys.ToArray();
            viewModel.ProjectAction = new MultiSelectList(pamprojectActionTypeDict, "Key", "Value", keys);

            var taskAgeList = _taskReAssignmentManager.GetAllTaskAgeIntervals().ToList();
            viewModel.TaskAgeList = new List<SelectListItem>();
            foreach (var taskAge in taskAgeList)
            {
                viewModel.TaskAgeList.Add(new SelectListItem() { Text = taskAge.Value, Value = taskAge.Key });
            }
            return View("~/Views/TaskReassignment/TaskReassignment.cshtml", viewModel);
        }

        public JsonResult GetAEsBasedOnWLMIds(string ids)
        {
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "wlmIds", ids } };
            Session[SessionHelper.SESSION_KEY_TASK_REASSIGNMENT_DICT] = routeValuesDict;
            //naveen 06/06/2019 added .OrderBy(x=> x.Manager_Name).ToList()
            var aes = _taskReAssignmentManager.GetRegisteredAEsByWLMId(ids).OrderBy(x => x.Manager_Name).ToList();

            var rows = new List<SelectListItem>(aes.Count + 1);
            rows.Add(new SelectListItem() { Text = "Select AE", Value = "" });
            foreach (Model.HUDManagerModel m in aes)
            {
                rows.Add(new SelectListItem() { Text = m.Manager_Name, Value = m.Manager_ID.ToString() });
            }
            return Json(new { rows }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetISOUsBasedOnWLMIds(string ids)
        {
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "wlmIds", ids } };
            Session[SessionHelper.SESSION_KEY_TASK_REASSIGNMENT_DICT] = routeValuesDict;
            var isous = _taskReAssignmentManager.GetISOUIdsByWLMId(ids);
            var rows = new List<SelectListItem>(isous.Count + 1);
            rows.Add(new SelectListItem() { Text = "Select ISOU", Value = "" });
            foreach (Model.HUDManagerModel m in isous)
            {
                rows.Add(new SelectListItem() { Text = m.Manager_Name, Value = m.ISOU_ID.ToString() });
            }
            return Json(new { rows }, JsonRequestBehavior.AllowGet);
        }

        [MvcSiteMapNode(Title = "Temporary Task Reassignment", PreservedRouteParameters = "page,sort,sortDir", ParentKey = "AdminId")]
        public ActionResult SearchTasksForAE(string aeId, string fromDate, string toDate, string taskAge,
            string projectAction, string wlmids, string selectedTaskIds, bool isSelectAll, int? page, string sort,
            string sortdir, string retainsort)
        {
            string aeIds = "";
            if (string.IsNullOrEmpty(aeId))
            {
                var aes = _pamReportManager.GetSubordinateAEsByWLMId(wlmids);

                int count = 0;
                foreach (var hudManagerModel in aes)
                {
                    count++;
                    aeIds += hudManagerModel.Manager_ID;
                    if (count < aes.Count)
                    {
                        aeIds += ",";
                    }
                }
            }
            else
            {
                aeIds = aeId;
            }
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(
                SessionHelper.SESSION_KEY_TASK_REASSIGNMENT_DICT);

            //karri:#67,#68:this was set to default 4 which is to be debugged; TBD

            SqlOrderByDirecton sortDir = SqlOrderByDirecton.DESC;

            if (routeValues != null)
            {
                if (routeValues.Keys.Contains("page"))
                {
                    if (page != null)
                    {
                        routeValues["page"] = page;
                    }
                    else
                    {
                        page = routeValues["page"] != null ? (int)routeValues["page"] : 1;
                    }
                }
                else
                {
                    page = page ?? 1;
                    routeValues.Add("page", page);
                }
                if (routeValues.Keys.Contains("sort"))
                {
                    if (sort != null)
                    {
                        routeValues["sort"] = sort;
                    }
                    else
                    {
                        sort = routeValues["sort"] != null ? routeValues["sort"] as string : "TaskAge";
                    }
                }
                else
                {
                    sort = sort ?? "TaskAge";
                    routeValues.Add("sort", sort);
                }
                if (routeValues.Keys.Contains("sortDir"))
                {
                    if (sortdir != null)
                    {
                        sortDir = EnumUtils.Parse<SqlOrderByDirecton>(sortdir);
                    }
                    else
                    {
                        sortDir = routeValues["sortDir"] != null
                            ? EnumUtils.Parse<SqlOrderByDirecton>(routeValues["sortDir"].ToString())
                            : EnumUtils.Parse<SqlOrderByDirecton>("DESC");
                    }
                }
                else
                {
                    sortDir = sortdir == null ? SqlOrderByDirecton.DESC : EnumUtils.Parse<SqlOrderByDirecton>(sortdir);
                    routeValues.Add("sortDir", sortDir);
                }
            }
            var taskReassignmentModel = new TaskReAssignmentViewModel();
            taskReassignmentModel.TaskDetailPerAeList =
            _taskReAssignmentManager.GetTaskDetailsForAE(aeIds, fromDate, toDate, taskAge, projectAction, page, sort, sortDir);
            bool flag = false;
            if (routeValues != null)
            {
                if (routeValues.Keys.Contains("selectedTaskIds"))
                {
                    if (routeValues["selectedTaskIds"] != null)
                    {
                        if (!string.IsNullOrEmpty(selectedTaskIds))
                        {
                            var str = selectedTaskIds.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            if (str.Any())
                            {
                                foreach (var item in str)
                                {
                                    var selectedStr = item.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    if (selectedStr.Any() && selectedStr.Length == 2)
                                    {
                                        if (routeValues["selectedTaskIds"].ToString().Contains(selectedStr[0]))
                                        {
                                            if (!routeValues["selectedTaskIds"].ToString().Contains(item))
                                            {
                                                flag = true;
                                                if (Convert.ToBoolean(selectedStr[1]))
                                                {
                                                    routeValues["selectedTaskIds"] = routeValues["selectedTaskIds"].ToString()
                                                        .Replace(selectedStr[0] + "_false", selectedStr[0] + "_true");
                                                }
                                                else
                                                {
                                                    routeValues["selectedTaskIds"] = routeValues["selectedTaskIds"].ToString()
                                                        .Replace(selectedStr[0] + "_true", selectedStr[0] + "_false");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (flag)
                            {
                                selectedTaskIds = routeValues["selectedTaskIds"].ToString();
                            }
                        }
                    }
                }
            }

            foreach (var item in taskReassignmentModel.TaskDetailPerAeList.Entities)
            {
                if (!string.IsNullOrEmpty(selectedTaskIds) && !isSelectAll)
                {
                    var str = selectedTaskIds.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (str.Any())
                    {
                        foreach (var id in str)
                        {
                            var selectedStr = id.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            if (selectedStr.Any() && selectedStr.Length == 2)
                            {
                                if (item.TaskInstanceId == Guid.Parse(selectedStr[0]))
                                {
                                    item.IsSelected = Convert.ToBoolean(selectedStr[1]);
                                    if (item.IsSelected)
                                    {
                                        TempData["IsSelected"] = true;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (isSelectAll)
                {
                    TempData["IsSelectAll"] = true;
                    item.IsSelected = true;
                    selectedTaskIds += item.TaskInstanceId + "_true";
                    var taskDetailList = taskReassignmentModel.TaskDetailPerAeList.Entities.ToList();
                    if (item != taskDetailList[taskDetailList.Count - 1])
                    {
                        selectedTaskIds += ",";
                    }
                }
            }
            foreach (var item in taskReassignmentModel.TaskDetailPerAeList.Entities)
            {
                if (!string.IsNullOrEmpty(retainsort) && !isSelectAll)
                {
                    var str = retainsort.Split(',');
                    string value = Array.Find(str,
                                   element => element.StartsWith(item.TaskInstanceId.ToString(), StringComparison.Ordinal));
                    if (!string.IsNullOrEmpty(value))
                    {
                        var selectedStr = value.Split('_');
                        if (selectedStr[1].ToUpper() == "TRUE")
                        {

                            item.IsSelected = true;
                            TempData["IsSelected"] = true;
                        }
                        else
                        {
                            item.IsSelected = false;
                            TempData["IsSelected"] = true;
                        }
                    }
                }
            }

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"wlmIds", wlmids},
                {"aeIds", aeIds},
                {"fromDate", fromDate},
                {"toDate", toDate},
                {"taskAge", taskAge},
                {"projectAction", projectAction},
                {"reassignAeId", ""},
                {"reassignISOUId", ""},
                {"selectedTaskIds", selectedTaskIds},
                {"isSelectAll", isSelectAll},
                {"page", page},
                {"sort", sort},
                {"sortDir", sortDir}
            };
            sortdir = sortDir.ToString();
            Session[SessionHelper.SESSION_KEY_TASK_REASSIGNMENT_DICT] = routeValuesDict;
            TempData["page"] = page;
            TempData["sort"] = sort;
            TempData["sortdir"] = sortdir;
            return PartialView("~/Views/TaskReassignment/_TaskDetailPerAE.cshtml", taskReassignmentModel);
        }

        public JsonResult DisplayConfirmationPopup(string reassignAeId, string reassignISOUId, string selectedTaskIds)
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(selectedTaskIds)) isValid = false;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(
                SessionHelper.SESSION_KEY_TASK_REASSIGNMENT_DICT);
            var aeId = "";
            var page = 1;
            if (routeValues != null)
            {
                if (routeValues.Keys.Contains("aeIds"))
                {
                    aeId = routeValues["aeIds"] as string;
                }
                if (routeValues.Keys.Contains("reassignAeId"))
                {
                    routeValues["reassignAeId"] = reassignAeId;
                }
                if (routeValues.Keys.Contains("reassignISOUId"))
                {
                    routeValues["reassignISOUId"] = reassignISOUId;
                }
                if (routeValues.Keys.Contains("selectedTaskIds") && !(bool)routeValues["isSelectAll"])
                {
                    if (!string.IsNullOrEmpty(selectedTaskIds) && !routeValues["selectedTaskIds"].ToString().Contains(selectedTaskIds))
                    {
                        routeValues["selectedTaskIds"] += "," + selectedTaskIds;
                    }
                }
                if (routeValues.Keys.Contains("page"))
                {
                    page = (int)routeValues["page"];
                }
                TempData["aeId"] = _taskReAssignmentManager.GetAENameById(aeId);
                if (!string.IsNullOrEmpty(reassignAeId))
                    TempData["reassignAeId"] = _taskReAssignmentManager.GetAENameById(reassignAeId);
                else if (!string.IsNullOrEmpty(reassignISOUId))
                    TempData["reassignAeId"] = _taskReAssignmentManager.GetISOUNameById(Convert.ToInt32(reassignISOUId));
                TempData["page"] = page;
            }
            PopupHelper.ConfigPopup(TempData, 320, 200, "Confirmation", false, false, true, false, false, true,
                "../TaskReassignment/TaskReAssignmentConfirmationPopUp");
            return Json(isValid, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DisplayValidationErrorPopup(string reassignAeId, string reassignISOUId, string selectedTaskIds)
        {
            if (!string.IsNullOrEmpty(reassignAeId) || string.IsNullOrEmpty(selectedTaskIds))
                return Json(false, JsonRequestBehavior.AllowGet);
            String[] tasks = selectedTaskIds.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<string> taskIds = new List<string>();
            foreach (string item in tasks)
            {
                taskIds.Add(item);
            }
            List<string> invalidFhas = _taskReAssignmentManager.GetInvalidTaskFHAsForIsou(Convert.ToInt32(reassignISOUId), taskIds);
            bool isInValid = false;
            //if (invalidFhas.Count > 0)
            //{
            //    isInValid = true;
            //    string isouName = _taskReAssignmentManager.GetISOUNameById(Convert.ToInt32(reassignISOUId));
            //    TempData["ErrorMsg"] = isouName + " is not associated with " + invalidFhas[0] +
            //        ". The task is unable to reassign to this user.";
            //}
            //else
            //{
            //    Boolean isAllNcre = _taskReAssignmentManager.IsAllTaskNCRE(taskIds);
            //    if (!isAllNcre)
            //    {
            //        isInValid = true;
            //        string isouName = _taskReAssignmentManager.GetISOUNameById(Convert.ToInt32(reassignISOUId));
            //        TempData["ErrorMsg"] = "Only Non Critical Request or Non Critical Request Extention tasks can be reassigned to Internal Special Option User.";
            //    }
            //}
            PopupHelper.ConfigPopup(TempData, 320, 200, "Validation Error", false, false, true, false, false, true,
                        "../TaskReassignment/TaskReAssignmentValidationErrorPopUp");
            return Json(isInValid, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ReassignAE(int? page)
        {
            string wlmIds = null;
            string aeId = null;
            string reassignAeId = null;
            string reassignISOUId = null;
            string selectedTaskIds = null;
            string fromDate = null;
            string toDate = null;
            string taskAge = null;
            string projectAction = null;
            string sort = "";
            var sortDir = SqlOrderByDirecton.DESC;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(
                SessionHelper.SESSION_KEY_TASK_REASSIGNMENT_DICT);
            if (routeValues != null)
            {
                if (routeValues.Keys.Contains("wlmIds"))
                {
                    wlmIds = (string)routeValues["wlmIds"];
                }
                if (routeValues.Keys.Contains("aeIds"))
                {
                    aeId = (string)routeValues["aeIds"];
                }
                if (routeValues.Keys.Contains("reassignAeId"))
                {
                    reassignAeId = (string)routeValues["reassignAeId"];
                }
                if (routeValues.Keys.Contains("reassignISOUId"))
                {
                    reassignISOUId = (string)routeValues["reassignISOUId"];
                }
                if (routeValues.Keys.Contains("selectedTaskIds"))
                {
                    selectedTaskIds = (string)routeValues["selectedTaskIds"];
                }
                if (routeValues.Keys.Contains("fromDate"))
                {
                    fromDate = (routeValues["fromDate"]) as string;
                }
                if (routeValues.Keys.Contains("toDate"))
                {
                    toDate = routeValues["toDate"] as string;
                }
                if (routeValues.Keys.Contains("taskAge"))
                {
                    taskAge = (string)routeValues["taskAge"];
                }
                if (routeValues.Keys.Contains("projectAction"))
                {
                    projectAction = (string)routeValues["projectAction"];
                }
                if (routeValues.Keys.Contains("page"))
                {
                    page = (int)routeValues["page"];
                }
                if (routeValues.Keys.Contains("sort"))
                {
                    sort = (string)routeValues["sort"];
                }
                if (routeValues.Keys.Contains("sortDir"))
                {
                    sortDir = (SqlOrderByDirecton)routeValues["sortDir"];
                }
            }
            var taskReassignmentModel = new TaskReAssignmentViewModel();
            var taskReassignmentForEmail = new TaskReAssignmentViewModel();
            taskReassignmentForEmail.TaskDetailPerAeList = _taskReAssignmentManager.GetTaskDetailsForAE(aeId, fromDate,
                toDate, taskAge, projectAction, (int)page, sort, sortDir);

            taskReassignmentModel.TaskDetailPerAeList = _taskReAssignmentManager.ReassignAE(aeId, reassignAeId, reassignISOUId,
                selectedTaskIds, fromDate, toDate, taskAge, projectAction, (int)page, sort, sortDir);
            if (aeId != null)
            {
                taskReassignmentForEmail.SelectedAEEmail = _taskReAssignmentManager.GetAccountExecutiveDetail(int.Parse(aeId)).EmailAddress;
            }
            if (reassignAeId != null)
            {
                taskReassignmentForEmail.SelectedReassignAEEmail = _taskReAssignmentManager.GetAccountExecutiveDetail(int.Parse(reassignAeId)).EmailAddress;
                taskReassignmentForEmail.SelectedReassignAE = _taskReAssignmentManager.GetAENameById(reassignAeId);
            }
            else if (reassignISOUId != null)
            {
                taskReassignmentForEmail.SelectedReassignAEEmail = _taskReAssignmentManager.GetISOUEmailById(Convert.ToInt32(reassignISOUId));
                taskReassignmentForEmail.SelectedReassignAE = _taskReAssignmentManager.GetISOUNameById(Convert.ToInt32(reassignISOUId));
            }
            taskReassignmentForEmail.SelectedAE = _taskReAssignmentManager.GetAENameById(aeId);
            var splitStr = selectedTaskIds.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var taskDetailList = new List<TaskDetailPerAeModel>();
            foreach (var item in taskReassignmentForEmail.TaskDetailPerAeList.Entities)
            {
                if (splitStr != null && splitStr.Any())
                {
                    foreach (var str in splitStr)
                    {
                        if (str != null)
                        {
                            var selectedStr = str.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            if (selectedStr != null && selectedStr.Any() && selectedStr.Length == 2)
                            {
                                if (Convert.ToBoolean(selectedStr[1]))
                                {
                                    if (item.TaskInstanceId == Guid.Parse(selectedStr[0]))
                                    {
                                        taskDetailList.Add(item);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            taskReassignmentForEmail.TaskDetailPerAeList.Entities = taskDetailList;
            taskReassignmentForEmail.SelectedWLMEmail =
                _taskReAssignmentManager.GetWorkloadManagerEmailFromAeId(int.Parse(aeId));
            if (reassignAeId != null) taskReassignmentForEmail.SelectedReassignedWLMEmail = _taskReAssignmentManager.
                GetWorkloadManagerEmailFromAeId(int.Parse(reassignAeId));
            else if (reassignISOUId != null) taskReassignmentForEmail.SelectedReassignedWLMEmail = _taskReAssignmentManager.
                GetWorkloadManagerEmailFromInternalSpecialOptionUserId(int.Parse(reassignISOUId));
            _backgroundJobManager.SendTaskReassignmentEmail(_emailManager, taskReassignmentForEmail);
            return PartialView("~/Views/TaskReassignment/_TaskDetailPerAE.cshtml", taskReassignmentModel);
        }

        [HttpPost]
        public ActionResult TaskReAssignmentConfirmationPopUp()
        {
            return PartialView("~/Views/TaskReassignment/TaskReAssignmentConfirmation.cshtml");
        }

        [HttpPost]
        public ActionResult TaskReAssignmentValidationErrorPopUp()
        {
            return PartialView("~/Views/TaskReassignment/TaskReAssignmentValidationError.cshtml");
        }

        [HttpGet]
        public ActionResult MyReAssignmentTasks(int? page, string sort, string sortDir, string lookupFHANumber = null)
        {
            //karri#205
            IList<String> UserFHAList = _taskReAssignmentManager.getFHAList(UserPrincipal.Current.UserName);
            if (lookupFHANumber != null && !string.IsNullOrEmpty(lookupFHANumber))
                _taskReAssignmentManager.setLookUpValues(lookupFHANumber);

            var sortOrder = EnumUtils.Parse<SqlOrderByDirecton>(sortDir);
            var model = _taskReAssignmentManager.GetReAssignedTasksByUserName(UserPrincipal.Current.UserName, page ?? 1,
                string.IsNullOrEmpty(sort) ? "MyStartTime" : sort,
                string.IsNullOrEmpty(sortDir)
                    ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                    : EnumUtils.Parse<SqlOrderByDirecton>(sortDir));

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"page", page},
                {"sort", sort},
                {"sortdir", sortDir}
            };
            Session[SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT] = routeValuesDict;

            var myTasks = model.Entities.ToList();
            foreach (var item in myTasks)
            {
                if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) ||
                    RoleManager.IsAccountExecutiveOrWorkloadManagerRole(UserPrincipal.Current.UserName))
                {
                    if (item.Status == FormUploadStatus.AssignedToOther)
                    {
                        if (RoleManager.IsUserOperatorAccountRepresentative(item.AssignedBy)
                            && RoleManager.IsUserLenderRole(item.AssignedTo))
                        {
                            item.Status = FormUploadStatus.AssignedByOther;
                        }
                    }
                }
            }

            ////karri#205        
            ViewBag.FHAList = UserFHAList;
            @ViewData["BindingFHANumber"] = lookupFHANumber;

            model.Entities = myTasks;
            return View("~/Views/TaskReassignment/TaskReassignmentView.cshtml", model);
        }
    }
}
