﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Model;
using MvcSiteMapProvider;
using System.Linq;
using Core;
using MvcSiteMapProvider.Web.Mvc.Filters;

namespace HUDHealthcarePortal.Controllers.ManagementReport
{
    public class ProjectReportController : Controller
    {
        private IProjectReportsManager _ProjectReportsManager;

        public ProjectReportController() : this(new ProjectReportsManager())
        {

        }

        public ProjectReportController(IProjectReportsManager projectReportManager)
        {
            _ProjectReportsManager = projectReportManager;
        }

        #region Indexes

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        public ActionResult Index()
        {
            return IndexProject();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        public ActionResult IndexProject()
        {
            return IndexReport(ReportType.ProjectReport);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        public ActionResult IndexLargeLoan()
        {
            return IndexReport(ReportType.LargeLoanReport);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        public ActionResult IndexRatioExceptions()
        {
            return IndexReport(ReportType.RatioExceptionsReport);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title="Temp Report Title", PreservedRouteParameters = "reportType,wlmId,aeId,isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod,minUploadDate,maxUploadDate, partial", ParentKey = "FinancialAnalysisKey", Key = "IndexReportId")]
        [SiteMapTitle("Key", Target = AttributeTarget.CurrentNode)]
        [SiteMapTitle("Title", Target = AttributeTarget.CurrentNode)]
        public ActionResult IndexReport(ReportType reportType, string wlmId = "", string aeId = "", bool isDebtCoverageRatio = false, bool isWorkingCapital = false, bool isDaysCashOnHand = false, bool isDaysInAcctReceivable = false, bool isAvgPaymentPeriod = false, string minUploadDate = "", string maxUploadDate = "", bool partial = false)
        {
            var userName = UserPrincipal.Current.UserName;
            var userRoleText = "SuperUser";
 
            ProjectReportModel reportModel = new ProjectReportModel(reportType);

            if (String.IsNullOrEmpty(minUploadDate))
            {
                if (!String.IsNullOrEmpty(Request.QueryString["minUploadDate"]))
                {
                    minUploadDate = Request.QueryString["minUploadDate"];
                }
                else
                {
                    //DateTime dminUploadDate = DateTime.Now.AddDays(-90);
                    DateTime dminUploadDate = DateTime.UtcNow.AddDays(-90);
                    minUploadDate = dminUploadDate.ToShortDateString();   
                }
            }
            if (String.IsNullOrEmpty(maxUploadDate))
            {
                if (!String.IsNullOrEmpty(Request.QueryString["maxUploadDate"]))
                {
                    maxUploadDate = Request.QueryString["maxUploadDate"];
                }
                else
                {
                    //DateTime dmaxUploadDate = DateTime.Now;
                    DateTime dmaxUploadDate = DateTime.UtcNow;
                    maxUploadDate = dmaxUploadDate.ToShortDateString();
                }
            }

            if (RoleManager.IsHudAdmin(userName) || RoleManager.IsHudDirector(userName))
            {
                ViewBag.ReportLevel = ReportLevel.SuperUserLevel;

                switch (reportType)
                {
                    case ReportType.ProjectReport:
                        reportModel = _ProjectReportsManager.GetSuperUserProjectSummary(UserPrincipal.Current.UserName, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));
                        break;
                    case ReportType.LargeLoanReport:
                        reportModel = _ProjectReportsManager.GetSuperUserLargeLoanSummary(UserPrincipal.Current.UserName, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));
                        break;
                    case ReportType.RatioExceptionsReport:
                        reportModel = _ProjectReportsManager.GetSuperUserRatioExceptionSummary(UserPrincipal.Current.UserName, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
                        break;
                }
            }
            else if (RoleManager.IsWorkloadManagerRole(userName))
            {
                userRoleText = "WLM";
                wlmId = _ProjectReportsManager.GetWlmId(UserPrincipal.Current.UserName);
                SetWLMSettings(reportType, wlmId, userName);

                switch (reportType)
                {
                    case ReportType.ProjectReport:
                        reportModel = _ProjectReportsManager.GetWLMProjectSummary(UserPrincipal.Current.UserName, wlmId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));
                        break;
                    case ReportType.LargeLoanReport:
                        reportModel = _ProjectReportsManager.GetWLMLargeLoanSummary(UserPrincipal.Current.UserName, wlmId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));
                        break;
                    case ReportType.RatioExceptionsReport:
                        reportModel = _ProjectReportsManager.GetWLMRatioExceptionSummary(UserPrincipal.Current.UserName, wlmId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
                        break;
                }
            }
            else if (RoleManager.IsAccountExecutiveRole(userName))
            {
                userRoleText = "AE";
                aeId = _ProjectReportsManager.GetAeId(UserPrincipal.Current.UserName);
                SetAESettings(reportType, null, aeId, userName);

                switch (reportType)
                {
                    case ReportType.ProjectReport:
                        reportModel = _ProjectReportsManager.GetAEProjectSummary(UserPrincipal.Current.UserName, null, aeId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));
                        break;
                    case ReportType.LargeLoanReport:
                        reportModel = _ProjectReportsManager.GetAELargeLoanSummary(UserPrincipal.Current.UserName, null, aeId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));
                        break;
                    case ReportType.RatioExceptionsReport:
                        reportModel = _ProjectReportsManager.GetAERatioExceptionSummary(UserPrincipal.Current.UserName, null, aeId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
                        break;
                }
            }
            else
            {
                return View("~/Views/Home/Index.cshtml");
            }

            if (reportType == ReportType.RatioExceptionsReport)
            {
                SetUpRatioExceptionsCriterias(ref reportModel, new bool[] { isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod });
            }

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

            ViewBag.Title = userRoleText + reportType.ToString("g");
            ViewBag.ReportType = reportType;

            SetPrintExcelParameters(reportType, userRoleText, minUploadDate, maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

        #endregion

        #region Project Report Summary
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager")]
        [MvcSiteMapNode(Title = "Projects Assigned for WLM", PreservedRouteParameters = "userName,wlmId,minUploadDate,maxUploadDate, partial", ParentKey = "ProjectReportId", Key = "WLMProjectReportId")]
        public ActionResult GetWLMProjectReportSummary(string userName, string wlmId, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            SetWLMSettings(ReportType.ProjectReport, wlmId, userName);
            SetPrintExcelParameters(ReportType.ProjectReport, "WLM", minUploadDate, maxUploadDate);

            ProjectReportModel reportModel = _ProjectReportsManager.GetWLMProjectSummary(userName, wlmId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

            if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager")]
        [MvcSiteMapNode(Title = "Projects Assigned for AE", PreservedRouteParameters = "userName,wlmId,minUploadDate,maxUploadDate, partial", ParentKey = "ProjectReportId", Key = "AEProjectReportWLMId")]
        public ActionResult GetAEProjectReportSummaryWLM(string userName, string wlmId, string aeId, string minUploadDate,
            string maxUploadDate, bool partial = false)
        {

            return GetAEProjectReportSummary(userName, wlmId, aeId, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned for AE", PreservedRouteParameters = "userName,wlmId,aeId,minUploadDate,maxUploadDate, partial", ParentKey = "WLMProjectReportId", Key = "AEProjectReportId")]
        public ActionResult GetAEProjectReportSummary(string userName, string wlmId, string aeId, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            SetAESettings(ReportType.ProjectReport, wlmId, aeId, userName);
            SetPrintExcelParameters(ReportType.ProjectReport, "AE", minUploadDate, maxUploadDate);
            
            ProjectReportModel reportModel = _ProjectReportsManager.GetAEProjectSummary(userName, wlmId, aeId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

            if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail", PreservedRouteParameters = "userName,aeId,wlmId,lenderId,projectReportVisibility,minUploadDate,maxUploadDate, partial", ParentKey = "AEProjectReportId", Key = "LenderProjectReportId")]
        public ActionResult GetProjectReportLenderDetail(string userName, string wlmId, string aeId, string lenderId, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            SetDetailsSettings(ReportType.ProjectReport, wlmId, aeId, lenderId, userName);
            SetPrintExcelParameters(ReportType.ProjectReport, "Detail", minUploadDate, maxUploadDate);

            ProjectReportModel reportModel = _ProjectReportsManager.GetProjectLenderDetail(userName, wlmId, aeId, lenderId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));

            SetUpProjectReportVisibilityOptions(ref reportModel, projectReportVisibility);

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

            if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(
            Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail", PreservedRouteParameters = "userName,aeId,wlmId,lenderId,projectReportVisibility,minUploadDate,maxUploadDate, partial", ParentKey = "AEProjectReportWLMId", Key = "LenderProjectReportWLMId")]
        public ActionResult GetProjectReportLenderDetailWLM(string userName, string wlmId, string aeId, string lenderId, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetProjectReportLenderDetail(userName, wlmId, aeId, lenderId, projectReportVisibility, minUploadDate, maxUploadDate, partial);

        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail", PreservedRouteParameters = "userName,aeId,wlmId,lenderId,projectReportVisibility,minUploadDate,maxUploadDate, partial", ParentKey = "ProjectReportId", Key = "LenderProjectReportDetailId")]
        public ActionResult GetProjectReportLenderDetailAE(string userName, string wlmId, string aeId, string lenderId, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetProjectReportLenderDetail(userName, wlmId, aeId, lenderId, projectReportVisibility, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail for Admin", PreservedRouteParameters = "userName,lenderId,projectReportVisibility,minUploadDate,maxUploadDate, partial", ParentKey = "ProjectReportId", Key = "SuperUserProjectReportDetailId")]
        public ActionResult GetSuperUserProjectReportDetail(string userName, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetProjectReportLenderDetail(userName, "", "", "", projectReportVisibility, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(
            Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail for WLM", PreservedRouteParameters = "userName,wlmId,projectReportVisibility,minUploadDate,maxUploadDate, partial", ParentKey = "WLMProjectReportId", Key = "WLMProjectReportDetailId")]
        public ActionResult GetWLMProjectReportDetail(string userName, string wlmId, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetProjectReportLenderDetail(userName, wlmId, "", "", projectReportVisibility, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail for AE", PreservedRouteParameters = "userName,aeId,wlmId,lenderId,projectReportVisibility,minUploadDate,maxUploadDate, partial", ParentKey = "AEProjectReportId", Key = "AEProjectReportDetailId")]
        public ActionResult GetAEProjectReportDetail(string userName, string wlmId, string aeId, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetProjectReportLenderDetail(userName, wlmId, aeId, "", projectReportVisibility, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(
            Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail for WLM",
            PreservedRouteParameters = "userName,wlmId,aeId,projectReportVisibility,minUploadDate,maxUploadDate, partial",
            ParentKey = "ProjectReportId", Key = "WLMProjectReportDetailWLMId")]
        public ActionResult GetWLMProjectReportDetailWLM(string userName, string wlmId, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetProjectReportLenderDetail(userName, wlmId, "", "", projectReportVisibility, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(
            Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail for AE",
            PreservedRouteParameters = "userName,wlmId,aeId,projectReportVisibility,minUploadDate,maxUploadDate, partial",
            ParentKey = "AEProjectReportWLMId", Key = "AEProjectReportDetailWLMId")]
        public ActionResult GetAEProjectReportDetailWLM(string userName, string wlmId, string aeId, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetProjectReportLenderDetail(userName, wlmId, aeId, "", projectReportVisibility, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(
            Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Projects Assigned Detail for AE", PreservedRouteParameters = "userName,aeId,wlmId,lenderId,projectReportVisibility,minUploadDate,maxUploadDate, partial", ParentKey = "ProjectReportId", Key = "AELenderProjectReportAEId")]
        public ActionResult GetAEProjectReportDetailAE(string userName, string wlmId, string aeId, string lenderId, string projectReportVisibility, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetProjectReportLenderDetail(userName, wlmId, aeId, "", projectReportVisibility, minUploadDate, maxUploadDate, partial);
        }


        #endregion

        #region Large Loan Report Details
        
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager")]
        [MvcSiteMapNode(Title = "Large Loan Report for WLM", PreservedRouteParameters = "wlmId,minUploadDate,maxUploadDate, partial", ParentKey = "LargeLoanReportId", Key = "WLMLargeLoanReportId")]
        public ActionResult GetWLMLargeLoanReportSummary(string userName, string wlmId, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            SetWLMSettings(ReportType.LargeLoanReport, wlmId, userName);
            SetPrintExcelParameters(ReportType.LargeLoanReport, "WLM", minUploadDate, maxUploadDate);

            ProjectReportModel reportModel = _ProjectReportsManager.GetWLMLargeLoanSummary(userName, wlmId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

            if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Report for AE", PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial", ParentKey = "LargeLoanReportId", Key = "AELargeLoanReportWLMId")]
        public ActionResult GetAELargeLoanReportSummaryWLM(string userName, string wlmId, string aeId, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetAELargeLoanReportSummary(userName, wlmId, aeId, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Report for AE", PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial", ParentKey = "WLMLargeLoanReportId", Key = "AELargeLoanReportId")]
        public ActionResult GetAELargeLoanReportSummary(string userName, string wlmId, string aeId, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            SetAESettings(ReportType.LargeLoanReport, wlmId, aeId, userName);
            SetPrintExcelParameters(ReportType.LargeLoanReport, "AE", minUploadDate, maxUploadDate);

            ProjectReportModel reportModel = _ProjectReportsManager.GetAELargeLoanSummary(userName, wlmId, aeId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

            if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Detail",
            PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial",
            ParentKey = "AELargeLoanReportWLMId", Key = "LargeLoanReportDetailWLMId")]
        public ActionResult GetLargeLoanLenderReportDetailWLM(string userName, string wlmId, string aeId, string lenderId, string minUploadDate,
            string maxUploadDate, bool partial = false)
        {
            return GetLargeLoanLenderReportDetail(userName, wlmId, aeId, lenderId, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Detail",
            PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial",
            ParentKey = "LargeLoanReportId", Key = "LargeLoanReportDetailAEId")]
        public ActionResult GetLargeLoanLenderReportDetailAE(string userName, string wlmId, string aeId, string lenderId, string minUploadDate,
            string maxUploadDate, bool partial = false)
        {
            return GetLargeLoanLenderReportDetail(userName, wlmId, aeId, lenderId, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager")]
        [MvcSiteMapNode(Title = "Large Loan Detail for Admin",
            PreservedRouteParameters = "wlmId,minUploadDate,maxUploadDate, partial", ParentKey = "LargeLoanReportId",
            Key = "SuperUserLargeLoanReportDetailId")]
        public ActionResult GetSuperUserLargeLoanReportDetail(string userName, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            return GetLargeLoanLenderReportDetail(userName, "", "", "", minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Detail for WLM",
            PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial",
            ParentKey = "WLMLargeLoanReportId", Key = "WLMLargeLoanReportDetailId")]
        public ActionResult GetWLMLargeLoanReportDetail(string userName, string wlmId, string minUploadDate,
            string maxUploadDate, bool partial = false)
        {
            return GetLargeLoanLenderReportDetail(userName, wlmId, "", "", minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Detail for WLM",
            PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial",
            ParentKey = "LargeLoanReportId", Key = "WLMLargeLoanReportDetailWLMId")]
        public ActionResult GetWLMLargeLoanReportDetailWLM(string userName, string wlmId, string minUploadDate,
            string maxUploadDate, bool partial = false)
        {
            return GetLargeLoanLenderReportDetail(userName, wlmId, "", "", minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(
            Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Detail for AE",
            PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial",
            ParentKey = "AELargeLoanReportWLMId", Key = "AELargeLoanReportDetailWLMId")]
        public ActionResult GetAELargeLoanReportDetailWLM(string userName, string wlmId, string aeId, string minUploadDate,
            string maxUploadDate, bool partial = false)
        {
            return GetLargeLoanLenderReportDetail(userName, wlmId, aeId, "", minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(
            Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Detail for AE",
            PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial",
            ParentKey = "LargeLoanReportId", Key = "AELargeLoanReportDetailAEId")]
        public ActionResult GetAELargeLoanReportDetailAE(string userName, string aeId, string minUploadDate,
            string maxUploadDate, bool partial = false)
        {
            return GetLargeLoanLenderReportDetail(userName, "", aeId, "", minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(
            Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Detail for AE",
            PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, partial",
            ParentKey = "AELargeLoanReportId", Key = "AELargeLoanReportDetailId")]
        public ActionResult GetAELargeLoanReportDetail(string userName, string wlmId, string aeId, string minUploadDate,
            string maxUploadDate, bool partial = false)
        {
            return GetLargeLoanLenderReportDetail(userName, wlmId, aeId, "", minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Large Loan Detail", PreservedRouteParameters = "aeId,wlmId,lenderId,minUploadDate,maxUploadDate, partial", ParentKey = "AELargeLoanReportId", Key = "LenderLargeLoanReportId")]
        public ActionResult GetLargeLoanLenderReportDetail(string userName, string wlmId, string aeId, string lenderId, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            SetDetailsSettings(ReportType.LargeLoanReport, wlmId, aeId, lenderId, userName);
            SetPrintExcelParameters(ReportType.LargeLoanReport, "Detail", minUploadDate, maxUploadDate);

            ProjectReportModel reportModel = _ProjectReportsManager.GetLargeLoanLenderDetail(userName, wlmId, aeId, lenderId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate));

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

           if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

        #endregion

        #region Ratio Exception Report Details

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager")]
        [MvcSiteMapNode(Title = "Ratio Exceptions for WLM", PreservedRouteParameters = "wlmId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, partial", ParentKey = "RatioExceptionsReportId", Key = "WLMRatioExceptionsReportId")]
        public ActionResult GetWLMRatioExceptionReportSummary(string userName, string wlmId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            SetWLMSettings(ReportType.RatioExceptionsReport, wlmId, userName);
            SetPrintExcelParameters(ReportType.RatioExceptionsReport, "WLM", minUploadDate, maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            ProjectReportModel reportModel = _ProjectReportsManager.GetWLMRatioExceptionSummary(userName, wlmId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            SetUpRatioExceptionsCriterias(ref reportModel, new bool[] { isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod });

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

            if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Ratio Exceptions for AE", PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, partial", ParentKey = "RatioExceptionsReportId", Key = "AERatioExceptionsReportWLMId")]
        public ActionResult GetAERatioExceptionReportSummaryWLM(string userName, string wlmId, string aeId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
        {

            return GetAERatioExceptionReportSummary(userName, wlmId, aeId, isDebtCoverageRatio, isWorkingCapital,
                isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
        [MvcSiteMapNode(Title = "Ratio Exceptions for AE", PreservedRouteParameters = "wlmId, aeId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, partial", ParentKey = "WLMRatioExceptionsReportId", Key = "AERatioExceptionsReportId")]
        public ActionResult GetAERatioExceptionReportSummary(string userName, string wlmId, string aeId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
        {
            SetAESettings(ReportType.RatioExceptionsReport, wlmId, aeId, userName);
            SetPrintExcelParameters(ReportType.RatioExceptionsReport, "AE", minUploadDate, maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            ProjectReportModel reportModel = _ProjectReportsManager.GetAERatioExceptionSummary(userName, wlmId, aeId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            SetUpRatioExceptionsCriterias(ref reportModel, new bool[] { isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod });

            SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

            if (partial)
            {
                return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
            }

            return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
        }

            [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail", PreservedRouteParameters = "aeId,wlmId,lenderId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial", ParentKey = "AERatioExceptionsReportWLMId", Key = "LenderRatioExceptionsReportWLMId")]
            public ActionResult GetRatioExceptionLenderReportDetailWLM(string userName, string wlmId, string aeId, string lenderId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
            {
                return GetRatioExceptionLenderReportDetail(userName, wlmId, aeId, lenderId, isDebtCoverageRatio,
                    isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate,
                    maxUploadDate, partial);
            }

            [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail", PreservedRouteParameters = "aeId,wlmId,lenderId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial", ParentKey = "RatioExceptionsReportId", Key = "LenderRatioExceptionsReportAEId")]
            public ActionResult GetRatioExceptionLenderReportDetailAE(string userName, string aeId, string lenderId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
            {
                return GetRatioExceptionLenderReportDetail(userName, "", aeId, lenderId, isDebtCoverageRatio,
                    isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate,
                    maxUploadDate, partial);
            }

            [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail for Admin",
                PreservedRouteParameters =
                    "wlmId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, partial",
                ParentKey = "RatioExceptionsReportId", Key = "SuperUserRatioExceptionsReportDetailId")]
            public ActionResult GetSuperUserRatioExceptionReportDetail(string userName, bool isDebtCoverageRatio,
                bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod,
                string minUploadDate, string maxUploadDate, bool partial = false)
            {
                return GetRatioExceptionLenderReportDetail(userName, "", "", "", isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial);
            }

            [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(
                Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail for WLM",
                PreservedRouteParameters =
                    "wlmId, aeId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, partial",
                ParentKey = "WLMRatioExceptionsReportId", Key = "WLMRatioExceptionsReportDetailId")]
            public ActionResult GetWLMRatioExceptionReportDetail(string userName, string wlmId,
                bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable,
                bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
            {
                return GetRatioExceptionLenderReportDetail(userName, wlmId, "", "", isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial);
            }

            [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(
                Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail for WLM",
                PreservedRouteParameters =
                    "wlmId, aeId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, partial",
                ParentKey = "RatioExceptionsReportId", Key = "WLMRatioExceptionsReportDetailWLMId")]
            public ActionResult GetWLMRatioExceptionReportDetailWLM(string userName, string wlmId,
                bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable,
                bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
            {
                return GetRatioExceptionLenderReportDetail(userName, wlmId, "", "", isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial);
            }

            [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(
                Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail for AE",
                PreservedRouteParameters =
                    "aeId,wlmId,lenderId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial",
                ParentKey = "AERatioExceptionsReportWLMId", Key = "AERatioExceptionsReportDetailWLMId")]
            public ActionResult GetAERatioExceptionReportDetailWLM(string userName, string wlmId, string aeId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand,
                bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
            {
                return GetRatioExceptionLenderReportDetail(userName, wlmId, aeId, "", isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial);
            }

            [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(
                Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail for AE",
                PreservedRouteParameters =
                    "aeId,wlmId,lenderId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial",
                ParentKey = "RatioExceptionsReportId", Key = "AERatioExceptionsReportDetailAEId")]
            public ActionResult GetAERatioExceptionReportDetailAE(string userName, string aeId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand,
                bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
            {
                return GetRatioExceptionLenderReportDetail(userName, "", aeId, "", isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial);
            }

            [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(
                Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail for AE",
                PreservedRouteParameters =
                    "aeId,wlmId,lenderId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial",
                ParentKey = "AERatioExceptionsReportId", Key = "AERatioExceptionsReportDetailId")]
            public ActionResult GetAERatioExceptionReportDetail(string userName, string wlmId, string aeId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand,
                bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
            {
                return GetRatioExceptionLenderReportDetail(userName, wlmId, aeId, "", isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial);
            }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
            [AccessDeniedAuthorize(Roles = "Administrator, HUDAdmin, SuperUser, HUDDirector, WorkflowManager, AccountExecutive")]
            [MvcSiteMapNode(Title = "Ratio Exceptions Detail", PreservedRouteParameters = "aeId,wlmId,lenderId,minUploadDate,maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod, minUploadDate, maxUploadDate, partial", ParentKey = "AERatioExceptionsReportId", Key = "LenderRatioExceptionsReportId")]
            public ActionResult GetRatioExceptionLenderReportDetail(string userName, string wlmId, string aeId, string lenderId, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod, string minUploadDate, string maxUploadDate, bool partial = false)
            {
                SetDetailsSettings(ReportType.RatioExceptionsReport, wlmId, aeId, lenderId, userName);
                SetPrintExcelParameters(ReportType.RatioExceptionsReport, "Detail", minUploadDate, maxUploadDate, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

                ProjectReportModel reportModel = _ProjectReportsManager.GetRatioExceptionLenderDetail(userName, wlmId, aeId, lenderId, Convert.ToDateTime(minUploadDate), Convert.ToDateTime(maxUploadDate), isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

                reportModel.IsDebtCoverageRatioException = isDebtCoverageRatio;

                SetUpRatioExceptionsCriterias(ref reportModel, new bool[] { isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod });

                SetUploadDates(ref reportModel, minUploadDate, maxUploadDate);

                if (partial)
                {
                    return PartialView("~/Views/ManagementReport/ProjectReport/_Report.cshtml", reportModel);
                }

                return View("~/Views/ManagementReport/ProjectReport/Report.cshtml", reportModel);
            }

        #endregion
        
        #region Shared Helper Methods

        private void SetWLMSettings(ReportType reportType, string wlmId, string userName)
        {
            var routeValuesDict = new RouteValueDictionary()
            {
                {"wlmId", wlmId}
            };

            SetRouteValueDictionary(reportType, routeValuesDict);

            ViewBag.ReportLevel = ReportLevel.WLMUserLevel;
            ViewBag.WorkloadManager = userName;
        }

        private void SetAESettings(ReportType reportType, string wlmId, string aeId, string userName)
        {
            var routeValuesDict = new RouteValueDictionary()
            {
                {"wlmId", wlmId},
                {"aeId", aeId}
            };

            SetRouteValueDictionary(reportType, routeValuesDict);

            ViewBag.ReportLevel = ReportLevel.AEUserLevel;
            ViewBag.WorkloadManager = ViewBag.WorkloadManager;
            ViewBag.AccountExecutive = userName;
        }

        private void SetDetailsSettings(ReportType reportType, string wlmId, string aeId, string lenderId, string userName)
        {
            var routeValuesDict = new RouteValueDictionary()
            {
                {"lenderId", lenderId},
                {"wlmId", wlmId},
                {"aeId", aeId}
            };

            SetRouteValueDictionary(reportType, routeValuesDict);

            ViewBag.ReportLevel = ReportLevel.LenderDetailLevel;
            ViewBag.WorkloadManager = ViewBag.WorkloadManager;
            ViewBag.AccountExecutive = ViewBag.AccountExecutive;
            ViewBag.LenderName = userName;
        }

        private void SetPrintExcelParameters(ReportType reportType, string userRoleText, string minUploadDate, string maxUploadDate, bool isDebtCoverageRatio = false, bool isWorkingCapital = false, bool isDaysCashOnHand = false, bool isDaysInAcctReceivable = false, bool isAvgPaymentPeriod = false)
        {
            ViewBag.ExcelExportAction = "Export" + reportType.ToString("g") + userRoleText;
            ViewBag.PrintAction = "Print" + reportType.ToString("g") + userRoleText;

            if (reportType == ReportType.ProjectReport || reportType == ReportType.LargeLoanReport)
            {
                ViewBag.ExcelActionParam = new { minUploadDate = minUploadDate, maxUploadDate = maxUploadDate };
            }
            else if(reportType == ReportType.RatioExceptionsReport)
            {
                ViewBag.ExcelActionParam = new
                {
                    minUploadDate = minUploadDate,
                    maxUploadDate = maxUploadDate,
                    isDebtCoverageRatio = isDebtCoverageRatio,
                    isWorkingCapital = isWorkingCapital,
                    isDaysCashOnHand = isDaysCashOnHand,
                    isDaysInAcctReceivable = isDaysInAcctReceivable,
                    isAvgPaymentPeriod = isAvgPaymentPeriod
                };
            }
        }

        private void SetRouteValueDictionary(ReportType reportType, RouteValueDictionary routeValuesDict)
        {
            switch (reportType)
            {
                case ReportType.ProjectReport:
                    Session[SessionHelper.SESSION_KEY_PROJECTS_REPORT_LINK_DICT] = routeValuesDict;
                    break;
                case ReportType.LargeLoanReport:
                    Session[SessionHelper.SESSION_KEY_LARGE_LOAN_REPORT_LINK_DICT] = routeValuesDict;
                    break;
                case ReportType.RatioExceptionsReport:
                    Session[SessionHelper.SESSION_KEY_RATIO_EXCEPTIONS_REPORT_LINK_DICT] = routeValuesDict;
                    break;
            }
        }

        private void SetUpRatioExceptionsCriterias(ref ProjectReportModel reportModel, bool[] selected)
        {
            var ratioExceptionsReportValues = Enum.GetValues(typeof (RatioExceptionsReportFilter)).Cast<RatioExceptionsReportFilter>();

            foreach (var enumItem in ratioExceptionsReportValues)
            {
                reportModel.RatioExceptionsCriteriaCheckBoxList.Add(new ColumnCheckBoxModel()
                {
                    ColumnName = EnumType.GetEnumDescription(enumItem),
                    Id = Convert.ToInt16(enumItem),
                    Selected = selected[Convert.ToInt16(enumItem)]
                });
            }

            reportModel.IsDebtCoverageRatioException = selected[Convert.ToInt16(RatioExceptionsReportFilter.DebtCoverageRatio)];
            reportModel.IsAvgPaymentPeriodException = selected[Convert.ToInt16(RatioExceptionsReportFilter.AveragePaymentPeriod)];
            reportModel.IsDaysCashOnHandException = selected[Convert.ToInt16(RatioExceptionsReportFilter.DaysCashOnHand)];
            reportModel.IsDaysInAcctReceivableException = selected[Convert.ToInt16(RatioExceptionsReportFilter.DaysInAccountsReceivable)];
            reportModel.IsWorkingCapitalScoreException = selected[Convert.ToInt16(RatioExceptionsReportFilter.WorkingCapital)];
        }

        private void SetUpProjectReportVisibilityOptions(ref ProjectReportModel reportModel, string projectReportVisibility = "")
        {
            var projectVisibilityOptions = Enum.GetValues(typeof (ProjectReportVisibility));

            if (projectReportVisibility == null || Enum.Parse(typeof(ProjectReportVisibility), projectReportVisibility) == null)
            {
                projectReportVisibility = ProjectReportVisibility.ProjectsAssigned.ToString("g");
            }

            foreach (var enumItem in projectVisibilityOptions)
            {
                reportModel.ProjectVisibilitySelectList.Add(new SelectListItem() { Selected = enumItem.ToString() == projectReportVisibility, Text = EnumType.GetEnumDescription((ProjectReportVisibility)enumItem), Value = enumItem.ToString() });
            }

            reportModel.SelectedProjectVisibilityId = (int)(Enum.Parse(typeof(ProjectReportVisibility), projectReportVisibility));
        }

        private void SetUploadDates(ref ProjectReportModel reportModel, string minUploadDate, string maxUploadDate)
        {
            reportModel.MinUploadDate = minUploadDate;
            reportModel.MaxUploadDate = maxUploadDate;
        }
        
        #endregion
    }
}
