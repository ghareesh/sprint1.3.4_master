﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using BusinessService.ProjectAction;
using Core;
using Core.Utilities;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Controllers.OPAForm;
using HUDHealthcarePortal.Controllers.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Microsoft.Ajax.Utilities;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using System.Configuration;
using Model.Production;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using System.IO;

namespace HUDHealthcarePortal.Controllers.ManagementReport
{
    public class PAMReportController : Controller
    {

        IPAMReportManager _plmReportMgr;
        private ITaskManager _taskManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager;
        private IProjectActionFormManager projectActionManager;
        private ITaskReAssignmentManager _taskReAssignmentManager;
        private IProductionPAMReportManager plmreport;
        private IProd_RestfulWebApiTokenRequest webApiTokenRequest;// harish added to download files from Transacess Server
        private IProd_RestfulWebApiDownload WebApiDownload;// harish added to download files from Transacess Server
        public PAMReportController()
            : this(new PAMReportsManager(), new TaskManager(), new NonCriticalRepairsRequestManager(), new ProjectActionFormManager(), new TaskReAssignmentManager(), 
                  new ProductionPAMReportManager() ,new Prod_RestfulWebApiTokenRequest(), new Prod_RestfulWebApiDownload())
        {

        }
        public PAMReportController(IPAMReportManager plmReportMgr, ITaskManager taskManager, INonCriticalRepairsRequestManager _nonCriticalRepairsManager, IProjectActionFormManager _projectActionFormManager, ITaskReAssignmentManager taskReAssignmentManager, 
            IProductionPAMReportManager _productionpPamReportManager , IProd_RestfulWebApiTokenRequest _prod_WebApiTokenRequest, IProd_RestfulWebApiDownload _WebApiDownload)
        {
            _plmReportMgr = plmReportMgr;
            _taskManager = taskManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;
            projectActionManager = _projectActionFormManager;
            _taskReAssignmentManager = taskReAssignmentManager;
            plmreport = _productionpPamReportManager;//naresh-new
            webApiTokenRequest = _prod_WebApiTokenRequest;
            WebApiDownload = _WebApiDownload;
        }

        /// <summary>
        /// Sets up the View Model on initial page load
        /// </summary>
        /// <returns></returns>
        private PAMReportModel InitializeViewModel(string wlmIds, string aeIds, string isouIds, string fromDate, string toDate, string status, string projectAction, int? page, string sort, string sortdir
                                                    , string proprname, string fhanumber, string assignto, int? actionopen, int? daysopen)//karri:D#610
        {
            var viewModel = new PAMReportModel(ReportType.PAMReport);
            var userName = UserPrincipal.Current.UserName;

            int wlmId = 0, aeId = 0, isouId = 0;
            var isDefaultSearch = false;
            var aEListItems = new List<HUDManagerModel>();
            var isouListItems = new List<HUDManagerModel>();
            var wlmlist = new List<KeyValuePair<int, string>>();

            if (string.IsNullOrEmpty(aeIds))
            {
                aeIds = string.Empty;
                isDefaultSearch = true;
            }

            if (string.IsNullOrEmpty(isouIds))
            {
                isouIds = string.Empty;
                isDefaultSearch = true;
            }
            if (RoleManager.IsWorkloadManagerRole(userName))
            {
                wlmId = _plmReportMgr.GetWlmId(UserPrincipal.Current.UserId);

            }
            //else if (RoleManager.IsAccountExecutiveRole(userName))
            //{
            //    aeId = _plmReportMgr.GetAeId(UserPrincipal.Current.UserId);
            //    aeIds = aeId.ToString(CultureInfo.InvariantCulture);
            //    isouIds = "0";

            //}
            //else if (RoleManager.IsInternalSpecialOptionUser(userName))
            //{
            //    // isouId = _plmReportMgr.GetIsouId(UserPrincipal.Current.UserId);
            //    isouId = UserPrincipal.Current.UserId;
            //    isouIds = isouId.ToString(CultureInfo.InvariantCulture);
            //}
            //26012020 Assetmanagement Pam report Multiselect for AE and workloadmanager start
            wlmlist = _plmReportMgr.GetAllWorkLoadManagers().ToList();
            //26012020 Assetmanagement Pam report Multiselect for AE and workloadmanager ending
            if (RoleManager.IsHudAdmin(userName) || RoleManager.IsHudDirector(userName))
            {
                //26012020 Assetmanagement Pam report Multiselect for AE and workloadmanager start
                //  wlmlist = _plmReportMgr.GetAllWorkLoadManagers().ToList();//comment by naresh
                //26012020 Assetmanagement Pam report Multiselect for AE and workloadmanager ending
            }
            else if (wlmId > 0)
            {
                aEListItems = _plmReportMgr.GetSubordinateAEsByWLMId(wlmId.ToString()) as List<HUDManagerModel>;
                if (string.IsNullOrEmpty(aeIds))
                {
                    if (aEListItems != null)
                        aeIds = aEListItems.Aggregate(aeIds, (current, item) => current + ("," + item.Manager_ID));
                    else
                        aeIds = "0";
                }

                isouListItems = _taskReAssignmentManager.GetISOUIdsByWLMId(wlmId.ToString()) as List<HUDManagerModel>;

                if (string.IsNullOrEmpty(isouIds))
                {
                    if (isouListItems != null && isouListItems.Count() > 0)
                        isouIds = isouListItems.Aggregate(isouIds, (current, item) => current + ("," + item.ISOU_ID));
                    else
                        isouIds = "0";
                }
            }
            //Added by siddu//
            // string requestProjectAction = projectAction;
            var routeValues =
                SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(
                    SessionHelper.SESSION_KEY_PAM_REPORT_LINK_DICT);
            //var fromUTCDate = fromDate == null ? (DateTime?)null : TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(fromDate));
            //var toUTCDate = toDate == null ? (DateTime?)null : TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(toDate));
            //ended by siddu//
            int pageNum = page ?? 1;
            string sortString = string.IsNullOrEmpty(sort) ? "ProjectActionDaysToComplete" : sort;
            var sortOrder = string.IsNullOrEmpty(sortdir)
                ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                : EnumUtils.Parse<SqlOrderByDirecton>(sortdir);

            int? reqStatus = string.IsNullOrWhiteSpace(status) || status == "1,0" || status == "0,1" ? (int?)null : int.Parse(status);
            //  int? reqProjectAction = string.IsNullOrWhiteSpace(projectAction) ? (int?)null : projectAction.Length > 3 ? (int?)null : int.Parse(projectAction == "0,1" || projectAction == "1,0" ? "10" : projectAction == "0,2" || projectAction == "2,0" ? "20" : projectAction == "1,2" || projectAction == "2,1" ? "12" : projectAction);
            string reqProjectAction = projectAction;
            //if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate) && isDefaultSearch)
            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate) && isDefaultSearch)
            //  if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                ViewBag.ExcelActionParam =
                    new { wlmIds = wlmIds, aeIds = aeIds, fromDate = fromDate, toDate = toDate, status = reqStatus, projectAction = projectAction, page = pageNum, sort = sortString, sortdir = sortOrder, isouIds = isouIds };
                viewModel = _plmReportMgr.GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds, null, null, wlmId, aeId, isouId,
                    reqStatus, reqProjectAction, pageNum, sortString, sortOrder, wlmIds
                    , proprname, fhanumber, assignto, actionopen, daysopen);//karri:D#610
                                                                            //    ViewBag.ExcelActionParam =
                                                                            //new { wlmIds = wlmIds, aeIds = aeIds, isouIds = isouIds, fromDate = fromUTCDate, toDate = toUTCDate, status = reqStatus, projectAction = projectAction, page = pageNum, sort = sortString, sortdir = sortOrder };

                //    karri: D#610;modified
                //     viewModel = _plmReportMgr.GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds, fromUTCDate,
                //     toUTCDate, wlmId, aeId, isouId, reqStatus, reqProjectAction, pageNum, sortString, sortOrder, wlmIds
                //     , proprname, fhanumber, assignto, actionopen, daysopen);

                SetViewModelStatus(status, viewModel);
                SetViewModelProjectAction(projectAction, viewModel);
                viewModel.WLMListItems = new MultiSelectList(wlmlist, "Key", "Value");
                viewModel.AEListItems = new MultiSelectList(aEListItems, "Manager_Id", "Manager_Name");
                viewModel.ISOUListItems = new MultiSelectList(isouListItems, "ISOU_ID", "Manager_Name");
            }
            else
            {
                ViewBag.ExcelActionParam = new { wlmIds = wlmIds, aeIds = aeIds, fromDate = fromDate, toDate = toDate, status = reqStatus, projectAction = projectAction, page = pageNum, sort = sortString, sortdir = sortOrder, isouIds = isouIds };
                viewModel = _plmReportMgr.GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds,
                    (fromDate == null ? (DateTime?)null : DateTime.Parse(fromDate)),
                    (toDate == null ? (DateTime?)null : DateTime.Parse(toDate)), wlmId, aeId, isouId,
                    reqStatus, reqProjectAction, pageNum, sortString, sortOrder, wlmIds
                    , "", "", "", actionopen, daysopen);//karri:D#610

            }
            if (RoleManager.IsHudAdmin(userName) || RoleManager.IsHudDirector(userName))
            {
                if (!string.IsNullOrEmpty(wlmIds))
                {
                    IList selectedValues = new List<int>();
                    var wlmStr = wlmIds.Split(',');
                    foreach (var item in wlmStr)
                    {
                        foreach (var pair in wlmlist)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if (pair.Key == int.Parse(item))
                                {
                                    selectedValues.Add(pair.Key);
                                }
                            }
                        }
                    }
                    viewModel.WLMListItems = new MultiSelectList(wlmlist, "Key", "Value", selectedValues);
                }
            }
            if (RoleManager.IsWorkloadManagerRole(userName) || RoleManager.IsHudAdmin(userName))
            {
                if (wlmId > 0)
                {
                    wlmIds = wlmId.ToString();
                }
                if (!string.IsNullOrEmpty(aeIds))
                {
                    aEListItems = _plmReportMgr.GetSubordinateAEsByWLMId(wlmIds) as List<HUDManagerModel>;
                    IList selectedValues = new List<int>();
                    var aeStr = aeIds.Split(',');
                    foreach (var item in aeStr)
                    {
                        if (aEListItems != null)
                            foreach (var pair in aEListItems)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    if (pair.Manager_ID == int.Parse(item))
                                    {
                                        selectedValues.Add(pair.Manager_ID);
                                    }
                                }
                            }
                    }
                    viewModel.AEListItems = new MultiSelectList(aEListItems, "Manager_Id", "Manager_Name", selectedValues);
                }
                else
                {
                    viewModel.AEListItems = new MultiSelectList(aEListItems, "Manager_Id", "Manager_Name");
                }
                if (!string.IsNullOrEmpty(isouIds))
                {
                    isouListItems = _taskReAssignmentManager.GetISOUIdsByWLMId(wlmIds) as List<HUDManagerModel>;
                    IList selectedValues = new List<int>();
                    var isouStr = isouIds.Split(',');
                    foreach (var item in isouStr)
                    {
                        if (isouListItems != null)
                            foreach (var pair in isouListItems)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    if (pair.ISOU_ID == int.Parse(item))
                                    {
                                        selectedValues.Add(pair.ISOU_ID);
                                    }
                                }
                            }
                    }
                    viewModel.ISOUListItems = new MultiSelectList(isouListItems, "ISOU_ID", "Manager_Name", selectedValues);
                }
                else
                {
                    viewModel.ISOUListItems = new MultiSelectList(isouListItems, "ISOU_ID", "Manager_Name");
                }
                //  }

                SetViewModelStatus(status, viewModel);
                SetViewModelProjectAction(projectAction, viewModel);

                viewModel.FromDate = fromDate;
                viewModel.ToDate = toDate;
            }
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
                {
                    {"wlmIds", wlmIds},
                    {"aeIds", aeIds},
                    {"isouIds", isouIds},
                    {"fromDate", fromDate},
                    {"toDate", toDate},
                    {"status", reqStatus},
                    {"projectAction", projectAction},
                    {"page", pageNum},
                   {"sort", sortString},
                  {"sortdir", sortOrder}
                };
            Session[SessionHelper.SESSION_KEY_PAM_REPORT_LINK_DICT] = routeValuesDict;
            return viewModel;
        }

        private static void SetViewModelStatus(string status, PAMReportModel viewModel)
        {
            if (status == "1")
            {
                viewModel.Status = new MultiSelectList(new Dictionary<int, string> { { 1, "Open" }, { 0, "Closed" } },
                    "Key", "Value", new[] { 1 });
            }
            else if (status == "0")
            {
                viewModel.Status = new MultiSelectList(new Dictionary<int, string> { { 1, "Open" }, { 0, "Closed" } },
                    "Key", "Value", new[] { 0 });
            }
            else if (status == "1,0" || status == "0,1")
            {
                viewModel.Status = new MultiSelectList(new Dictionary<int, string> { { 1, "Open" }, { 0, "Closed" } },
                    "Key", "Value", new[] { 0, 1 });
            }
            else
            {
                viewModel.Status = new MultiSelectList(new Dictionary<int, string> { { 1, "Open" }, { 0, "Closed" } }, "Key", "Value",
                    new[] { 0, 1 });
            }
        }

        private void SetViewModelProjectAction(string projectAction, PAMReportModel viewModel)
        {
            var pamprojectActionTypeDict = (Dictionary<int, string>)_plmReportMgr.GetPamProjectionTypes().OrderBy(x => x.Value).ToDictionary(g => g.Key, g => g.Value);


            var keys = pamprojectActionTypeDict.Keys.ToArray();

            if (!string.IsNullOrEmpty(projectAction))
            {
                var prArray = projectAction.Split(',').Select(h => Int32.Parse(h)).ToArray();
                viewModel.ProjectAction = new MultiSelectList(pamprojectActionTypeDict,
                   "Key", "Value", prArray);
            }
            else
            {
                viewModel.ProjectAction = new MultiSelectList(pamprojectActionTypeDict,
                  "Key", "Value", keys);
            }

        }

        [MvcSiteMapNode(Title = "Project Action Management Report", ParentKey = "AMId", PreservedRouteParameters = "wlmIds,aeIds,isouIds,fromDate,toDate,status,projectAction,page,sort,sortdir")]
        [SiteMapTitle("PAMReport", Target = AttributeTarget.CurrentNode)]
        public ActionResult Index(string wlmIds, string aeIds, string isouIds, string fromDate, string toDate, string status, string projectAction, int? page, string sort, string sortdir
                            , string proprname, string fhanumber, string assignto, int? actionopen, int? daysopen)//karri:D#610
        {
            ViewBag.GetAesBasedOnWLMIdsUrl = Url.Action("GetAEsBasedOnWLMIds");
            ViewBag.GetIsousBasedOnWLMIdsUrl = Url.Action("GetISOUsBasedOnWLMIds");
            ViewBag.ExcelExportAction = "ExportPAMReport";
            ViewBag.PrintAction = "PrintPAMReport";
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            //naresh add 28012020
            var SavedFilter = plmreport.GetAssetSavedFilter();
            //if (string.IsNullOrEmpty(wlmIds))
            //{
            //    wlmIds = SavedFilter.wlmUsers;
            //}
            //if (string.IsNullOrEmpty(aeIds))
            //{
            //    aeIds = SavedFilter.aeUsers;
            //}
            //if (string.IsNullOrEmpty(isouIds))
            //{
            //    isouIds = SavedFilter.isoUsers;
            //}
            //if (string.IsNullOrEmpty(projectAction))
            //{
            //    projectAction = SavedFilter.projectAction;
            //}
            //naresh add 28012020
            //ViewBag.selectedvalues = SavedFilter.propertyname;
            var viewModel = InitializeViewModel(wlmIds, aeIds, isouIds, fromDate, toDate, status, projectAction, page, sort, sortdir
                                                     , proprname, fhanumber, assignto, actionopen, daysopen);

            //karri:D#610
            var gridData = viewModel.PAMReportGridlList.ToList();
            ArrayList alPropNames = new ArrayList();
            ArrayList alFhaNums = new ArrayList();
            ArrayList alAssignTos = new ArrayList();
            ArrayList alHasOpened = new ArrayList();
            alHasOpened.Add(true);
            alHasOpened.Add(false);
            ArrayList alalProjActComDays = new ArrayList();
            foreach (var item in gridData)
            {
                if (item.PropertyName != null && item.PropertyName.Length > 0 && !alPropNames.Contains(item.PropertyName))
                    alPropNames.Add(item.PropertyName);

                if (item.FHANumber != null && item.FHANumber.Length > 0 && !alFhaNums.Contains(item.FHANumber))
                    alFhaNums.Add(item.FHANumber);

                if (item.ReAssignedTo != null && item.ReAssignedTo.Length > 0 && !alAssignTos.Contains(item.ReAssignedTo))
                    alAssignTos.Add(item.ReAssignedTo);

                if (item.ProjectActionDaysToComplete > 0 && !alalProjActComDays.Contains(item.ProjectActionDaysToComplete))
                    alalProjActComDays.Add(item.ProjectActionDaysToComplete);
            }

            alPropNames.Sort();
            alFhaNums.Sort();
            alAssignTos.Sort();
            alalProjActComDays.Sort();

            for (int i = 0; i < alPropNames.Count; i++)
            {
                // hdt.TicketStatusList.Add(new SelectListItem() { Text = dr["Text"].ToString(), Value = dr["Value"].ToString() });
                viewModel.PropNames.Add(new SelectListItem() { Text = (string)alPropNames[i], Value = (string)alPropNames[i] });
            }
            for (int i = 0; i < alFhaNums.Count; i++)
            {
                viewModel.FHANumbersList.Add(new SelectListItem() { Text = (string)alFhaNums[i], Value = (string)alFhaNums[i] });
            }
            for (int i = 0; i < alAssignTos.Count; i++)
            {
                viewModel.AssignedToList.Add(new SelectListItem() { Text = (string)alAssignTos[i], Value = (string)alAssignTos[i] });
            }
            for (int i = 0; i < alalProjActComDays.Count; i++)
            {
                viewModel.ProjActionDaysToCompList.Add(new SelectListItem() { Text = (string)alalProjActComDays[i].ToString(), Value = (string)alalProjActComDays[i].ToString() });
            }
            for (int i = 0; i < alHasOpened.Count; i++)
            {
                viewModel.HasOpenedList.Add(new SelectListItem() { Text = (string)alHasOpened[i].ToString(), Value = (string)alHasOpened[i].ToString() });
            }

            //end/ //naresh 28012020
            if (SavedFilter != null)
            {
                viewModel.AsstMgmtRptFilters = SavedFilter;
                ViewData["hidlstWlms"] = SavedFilter.wlmUsers;
                Session["ids"] = SavedFilter.wlmUsers;
                ViewData["hidlstAes"] = SavedFilter.aeUsers;
                ViewData["hidlstIsous"] = SavedFilter.isoUsers;
                ViewData["hidlstStatus"] = SavedFilter.Status;
                ViewData["hidlstProjectAction"] = SavedFilter.projectAction;
            }



            return View("~/Views/ManagementReport/PAMReports/PAMReport.cshtml", viewModel);

        }


        public JsonResult GetISOUsBasedOnWLMIds(string ids)
        {
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "wlmIds", ids } };
            Session[SessionHelper.SESSION_KEY_TASK_REASSIGNMENT_DICT] = routeValuesDict;
            var isous = _taskReAssignmentManager.GetISOUIdsByWLMId(ids);
            //var rows = new List<SelectListItem>(isous.Count + 1);
            List<object> rows = new List<object>(isous.Count);
            //rows.Add(new SelectListItem() { Text = "Select ISOU", Value = "" });
            foreach (Model.HUDManagerModel m in isous)
            {
                rows.Add(new { ISOU_ID = m.ISOU_ID, Manager_Name = m.Manager_Name });
            }
            return Json(new { rows }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Search for non critical and R4R requests
        /// </summary>
        /// <param name="aeIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="status"></param>
        /// <param name="projectAction"></param>
        /// <param name="page"></param>
        /// <param name="sort"></param>
        /// <param name="sortdir"></param>
        /// <param name="wlmIds"></param>
        /// <returns></returns>
        public ActionResult SearchForRequests(string isouIds, string aeIds, string fromDate, string toDate, string status, string projectAction, int? page, string sort, string sortdir, string wlmIds
            , string proprname = null, string fhanumber = null, string assignto = null, int? actionopen = null, int? daysopen = null)
        {

            var userName = UserPrincipal.Current.UserName;
            ViewBag.ExcelExportAction = "ExportPAMReport";
            ViewBag.PrintAction = "PrintPAMReport";


            int wlmId = 0, aeId = 0, isouId = 0;
            if (RoleManager.IsWorkloadManagerRole(userName))
            {
                wlmId = _plmReportMgr.GetWlmId(UserPrincipal.Current.UserId);
                wlmIds = wlmId.ToString();
                var aEListItems = _plmReportMgr.GetSubordinateAEsByWLMId(wlmId.ToString()) as List<HUDManagerModel>;
                var isouListItems = _plmReportMgr.GetSubordinateISOUsByWLMId(wlmId.ToString()) as List<HUDManagerModel>;
                if (string.IsNullOrEmpty(aeIds))
                {
                    if (aEListItems == null || aEListItems.Count == 0)
                        aeIds = "0";
                    else if (aEListItems != null)
                        aeIds = aEListItems.Aggregate(aeIds, (current, item) => current + ("," + item.Manager_ID));
                }
                if (string.IsNullOrEmpty(isouIds))
                {
                    if (isouListItems == null || isouListItems.Count == 0)
                        isouIds = "0";
                    else if (isouListItems != null && isouListItems.Count() > 0)
                        isouIds = isouListItems.Aggregate(isouIds, (current, item) => current + ("," + item.ISOU_ID));
                }

            }
            //commented by siddu it is displaying no records in grid//
            //else if (RoleManager.IsAccountExecutiveRole(userName))
            //{
            //    aeId = _plmReportMgr.GetAeId(UserPrincipal.Current.UserId);
            //    aeIds = aeId.ToString(CultureInfo.InvariantCulture);
            //}

            //else if (RoleManager.IsInternalSpecialOptionUser(userName))
            //{
            //    // isouId = _plmReportMgr.GetIsouId(UserPrincipal.Current.UserId);
            //    isouId = UserPrincipal.Current.UserId;
            //    isouIds = isouId.ToString(CultureInfo.InvariantCulture);
            //}

            int? requestStatus = string.IsNullOrWhiteSpace(status) || status == "1,0" || status == "0,1" ? (int?)null : int.Parse(status);
            //   int? requestProjectAction = string.IsNullOrWhiteSpace(projectAction) ? (int?)null : projectAction.Length > 3 ? (int?)null : int.Parse(projectAction == "0,1" || projectAction == "1,0" ? "10" : projectAction == "0,2" || projectAction == "2,0" ? "20" : projectAction == "1,2" || projectAction == "2,1" ? "12" : projectAction);
            string requestProjectAction = projectAction;
            var routeValues =
                SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(
                    SessionHelper.SESSION_KEY_PAM_REPORT_LINK_DICT);

            int pageNum = page ?? 1;
            string sortString = string.IsNullOrEmpty(sort) ? "IsProjectActionOpen,ProjectActionSubmitDate" : sort;
            var sortOrder = string.IsNullOrEmpty(sortdir)
                ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                : EnumUtils.Parse<SqlOrderByDirecton>(sortdir);
            //var fromUTCDate = fromDate == null ? (DateTime?)null : TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(fromDate));
            //var toUTCDate = toDate == null ? (DateTime?)null : TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(toDate));
            var fromUTCDate = fromDate == null ? (DateTime?)null : Convert.ToDateTime(fromDate);
            var toUTCDate = toDate == null ? (DateTime?)null : Convert.ToDateTime(toDate);
            if (string.IsNullOrEmpty(wlmIds) && string.IsNullOrEmpty(aeIds) && string.IsNullOrEmpty(isouIds))
            {
                aeIds = ""; isouIds = "";
            }
            else
            {
                if (string.IsNullOrEmpty(aeIds))
                {
                    aeIds = "0";
                }
                if (string.IsNullOrEmpty(isouIds))
                {
                    isouIds = "0";
                }
            }

            ViewBag.ExcelActionParam =
             new { wlmIds = wlmIds, aeIds = aeIds, isouIds = isouIds, fromDate = fromUTCDate, toDate = toUTCDate, status = requestStatus, projectAction = projectAction, page = pageNum, sort = sortString, sortdir = sortOrder };

            //karri:D#610;modified
            var viewModel = _plmReportMgr.GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds, fromUTCDate,
            toUTCDate, wlmId, aeId, isouId, requestStatus, requestProjectAction, pageNum, sortString, sortOrder, wlmIds
            , proprname, fhanumber, assignto, actionopen, daysopen);

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"wlmIds", wlmIds},
                {"aeIds", aeIds},
                {"isouIds", isouIds},
                {"fromDate", fromUTCDate},
                {"toDate", toUTCDate},
                {"status", requestStatus},
                {"projectAction", projectAction},
                {"page", pageNum},
                {"sort", sortString},
                {"sortdir", sortOrder}
            };
            Session[SessionHelper.SESSION_KEY_PAM_REPORT_LINK_DICT] = routeValuesDict;

            return PartialView("~/Views/ManagementReport/PAMReports/_PAMReport.cshtml", viewModel);
        }

        [HttpPost]
        public ActionResult CommentsDetail(Guid id, string type)
        {

            var projectActionName = EnumType.EnumToValue(EnumType.Parse<ProjectActionName>(type));

            if (projectActionName == "R4R")
            {
                var model = _plmReportMgr.GetR4RRequest(id);
                return PartialView("~/Views/ManagementReport/PAMReports/CommentsDetail.cshtml", model);
            }

            else if (projectActionName == "NCRExtension")
            {
                // var model = new NonCriticalRequestExtensionModel();
                var ServicerNotes = string.Empty;
                var model = _plmReportMgr.GetNCRExtensionRequest(id);

                ITaskManager taskMgr = new TaskManager();
                Guid? TaskInstanceid;
                var newtask = taskMgr.GetTaskById((int)model.TaskId);


                TaskInstanceid = newtask.TaskInstanceId;
                var TaskInstancelist = taskMgr.GetTasksByTaskInstanceId((Guid)TaskInstanceid);

                if (TaskInstancelist != null && TaskInstancelist.ToList().Count > 1)
                {
                    var result = TaskInstancelist.OrderByDescending(a => a.TaskId);


                    ServicerNotes = result.ToList()[1].Notes;
                }
                else if (TaskInstancelist != null && TaskInstancelist.ToList().Count == 1)
                {
                    var result = TaskInstancelist.OrderByDescending(a => a.TaskId);
                    ServicerNotes = result.ToList()[0].Notes;
                }



                if (!string.IsNullOrEmpty(model.Comments))
                {
                    model.Comments = model.Comments + "," + ServicerNotes;
                }
                else
                {
                    model.Comments = ServicerNotes;
                }




                return PartialView("~/Views/ManagementReport/PAMReports/CommentsDetailNCRExt.cshtml", model);
            }

            else if (projectActionName == "NCR")
            {
                var model = _plmReportMgr.GetNCRRequest(id);
                var AENotes = string.Empty;
                ITaskManager taskMgr = new TaskManager();
                Guid? TaskInstanceid;
                var newtask = taskMgr.GetTaskById((int)model.TaskId);
                int SequenceID = 0;


                TaskInstanceid = newtask.TaskInstanceId;
                var TaskInstancelist = taskMgr.GetTasksByTaskInstanceId((Guid)TaskInstanceid);

                if (TaskInstancelist != null && TaskInstancelist.ToList().Count > 1)
                {
                    var result = TaskInstancelist.OrderByDescending(a => a.TaskId);

                    SequenceID = result.ToList()[0].SequenceId;
                    AENotes = result.ToList()[0].Notes;
                }
                else if (TaskInstancelist != null && TaskInstancelist.ToList().Count == 1)
                {
                    var result = TaskInstancelist.OrderByDescending(a => a.TaskId);
                    SequenceID = result.ToList()[0].SequenceId;
                    AENotes = result.ToList()[0].Notes;
                }



                if (SequenceID > 0)
                {
                    if (model.ServicerRemarks == null)
                    {
                        model.ServicerRemarks = "";
                    }
                    model.ServicerRemarks = model.ServicerRemarks + "," + AENotes;
                }
                else
                {
                    //model.ServicerRemarks = AENotes;
                }
                return PartialView("~/Views/ManagementReport/PAMReports/CommentsDetailNCR.cshtml", model);
            }
            else
            {
                var model = _plmReportMgr.GetOPARequestId(id);
                var AENotes = string.Empty;
                ITaskManager taskMgr = new TaskManager();
                Guid? TaskInstanceid;
                var newtask = taskMgr.GetTaskById((int)model.MytaskId);
                int SequenceID = 0;


                TaskInstanceid = newtask.TaskInstanceId;
                var TaskInstancelist = taskMgr.GetTasksByTaskInstanceId((Guid)TaskInstanceid);
                var comments = string.Empty;

                if (TaskInstancelist != null && TaskInstancelist.ToList().Count > 0)
                {
                    var result = TaskInstancelist.OrderByDescending(a => a.TaskId);

                    foreach (var task in result)
                    {
                        comments = comments + "~" + ((task.Notes ?? string.Empty) + "|" + task.SequenceId.ToString());
                    }

                }

                model.ServicerComments = !string.IsNullOrEmpty(comments) ? comments.Substring(1, comments.Length - 1) : string.Empty;

                return PartialView("~/Views/ManagementReport/PAMReports/CommentsOPA.cshtml", model);
            }


            return null;
        }

        public JsonResult GetAEsBasedOnWLMIds(string ids)
        {

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary() { { "wlmIds", ids } };
            Session[SessionHelper.SESSION_KEY_PAM_REPORT_LINK_DICT] = routeValuesDict;
            if (ids == null)//29082020
            {
                if (Session["ids"] != null)
                {
                    ids = Session["ids"].ToString();
                }
            }
            var aes = _plmReportMgr.GetSubordinateAEsByWLMId(ids);

            List<object> rows = new List<object>(aes.Count);
            foreach (Model.HUDManagerModel m in aes)
            {
                rows.Add(new { Manager_Id = m.Manager_ID, Manager_Name = m.Manager_Name });
            }
            return Json(new { rows }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Report Form", ParentKey = "PamReportId", PreservedRouteParameters = "id,type,aeIds,fromDate,toDate,status,projectAction,page,sort,sortdir")]
        [SiteMapTitle("ReportForm", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetForm(Guid id, string type)
        {
            var projectActionName = EnumType.EnumToValue(EnumType.Parse<ProjectActionName>(type));
            ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
            ISiteMapNode parentNode = null;
            if (SiteMaps.Current.CurrentNode != null) parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_PAM_REPORT_LINK_DICT);
            Session["RouteValues"] = routeValues;
            if (parentNode != null && routeValues != null)
            {
                if (!parentNode.RouteValues.Keys.Contains("wlmIds"))
                {
                    parentNode.RouteValues["wlmIds"] = routeValues["wlmIds"];
                    ViewData["wlmIds"] = routeValues["wlmIds"];
                }
                if (!parentNode.RouteValues.Keys.Contains("aeIds"))
                {
                    parentNode.RouteValues["aeIds"] = routeValues["aeIds"];
                    ViewData["aeIds"] = routeValues["aeIds"];
                }
                if (!parentNode.RouteValues.Keys.Contains("fromDate"))
                {
                    parentNode.RouteValues["fromDate"] = routeValues["fromDate"];
                    ViewData["fromDate"] = routeValues["fromDate"];
                }
                if (!parentNode.RouteValues.Keys.Contains("toDate"))
                {
                    parentNode.RouteValues["toDate"] = routeValues["toDate"];
                    ViewData["toDate"] = routeValues["toDate"];
                }
                if (!parentNode.RouteValues.Keys.Contains("status"))
                {
                    parentNode.RouteValues["status"] = routeValues["status"];
                    ViewData["status"] = routeValues["status"];
                }
                if (!parentNode.RouteValues.Keys.Contains("projectAction"))
                {
                    parentNode.RouteValues["projectAction"] = routeValues["projectAction"];
                    ViewData["projectAction"] = routeValues["projectAction"];
                }
                if (!parentNode.RouteValues.Keys.Contains("page"))
                {
                    parentNode.RouteValues["page"] = routeValues["page"];
                    ViewData["page"] = routeValues["page"];
                }
                if (!parentNode.RouteValues.Keys.Contains("sort"))
                {
                    parentNode.RouteValues["sort"] = routeValues["sort"];
                    ViewData["sort"] = routeValues["sort"];
                }
                if (!parentNode.RouteValues.Keys.Contains("sortdir"))
                {
                    parentNode.RouteValues["sortdir"] = routeValues["sortdir"];
                    ViewData["sortdir"] = routeValues["sortdir"];
                }
            }

            switch (projectActionName)
            {
                case "R4R":
                    var r4rModel = _plmReportMgr.GetR4RRequest(id);
                    if (r4rModel.TaskId != null)
                    {
                        var task = _taskManager.GetTaskById((int)r4rModel.TaskId);
                        if (task.DataStore1.Contains("ReserveForReplacementFormModel"))
                        {
                            var formData =
                                XmlHelper.Deserialize(typeof(ReserveForReplacementFormModel), task.DataStore1,
                                    Encoding.Unicode) as ReserveForReplacementFormModel;
                            if (formData != null)
                            {
                                formData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                                    ? new TaskOpenStatusModel()
                                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus,
                                        Encoding.Unicode) as TaskOpenStatusModel;
                                formData.TaskGuid = task.TaskInstanceId;
                                formData.Concurrency = _taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                                formData.SequenceId = task.SequenceId;
                                formData.AssignedBy = task.AssignedBy;
                                formData.AssignedTo = task.AssignedTo;
                                if (formData.TaskOpenStatus != null)
                                    formData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);

                                formData.IsDisplayAcceptPopup = false;
                                formData.IsAgreementAccepted = true;// User Strory 1901
                                GetDisclaimerTextDescriptionR4R(formData);// User Story 1901
                                formData.IsPAMReport = true;

                            }

                            return View("~/Views/AssetManagement/ReserveForReplacementFormReadOnlyView.cshtml", formData);
                        }
                    }

                    break;
                case "NCR":
                    var ncrModel = _plmReportMgr.GetNCRRequest(id);
                    if (ncrModel.TaskId != null)
                    {
                        var task = _taskManager.GetTaskById((int)ncrModel.TaskId);
                        if (task.DataStore1.Contains("NonCriticalRepairsViewModel"))
                        {
                            var formData =
                                XmlHelper.Deserialize(typeof(NonCriticalRepairsViewModel), task.DataStore1,
                                    Encoding.Unicode) as NonCriticalRepairsViewModel;
                            if (formData != null)
                            {
                                formData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                                    ? new TaskOpenStatusModel()
                                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus,
                                        Encoding.Unicode) as TaskOpenStatusModel;
                                formData.TaskGuid = task.TaskInstanceId;
                                formData.Concurrency = _taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                                formData.SequenceId = task.SequenceId;
                                formData.AssignedBy = task.AssignedBy;
                                formData.AssignedTo = task.AssignedTo;
                                if (formData.TaskOpenStatus != null)
                                    formData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);

                                formData.IsDisplayAcceptPopup = false;
                                formData.IsAgreementAccepted = false;
                                formData.ManageMode = "ReadOnly";
                                // harish added to set isfinaldraw as false because ,when it was true ,the model.FinalInspectionReport doesn't contains null values ,for old task its enabled true but FinalInspectionReport contains null value
                                if (formData.FinalInspectionReport == null)
                                {
                                    formData.IsFinalDraw = false;
                                }
                                formData.IsPAMReport = true;
                                formData.RulesCheckList =
                                    nonCriticalRepairsManager.GetNonCrticalRulesCheckList(FormType.Ncr);
                                formData.ReferalReasons =
                                    nonCriticalRepairsManager.GetNonCrticalReferReasons(formData.NonCriticalRequestId);
                            }
                            formData.IsAgreementAccepted = true;// User Story 1901
                            formData.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("NCRE"); // User Story 1901
                            return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", formData);
                        }
                    }
                    break;
                case "NCRExtension":
                    var ncrExModel = _plmReportMgr.GetNCRExtensionRequest(id);
                    if (ncrExModel.TaskId != null)
                    {
                        var task = _taskManager.GetTaskById((int)ncrExModel.TaskId);
                        if (task.DataStore1.Contains("NonCriticalRequestExtensionModel"))
                        {
                            var formData =
                                XmlHelper.Deserialize(typeof(NonCriticalRequestExtensionModel), task.DataStore1,
                                    Encoding.Unicode) as NonCriticalRequestExtensionModel;
                            if (formData != null)
                            {
                                formData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                                    ? new TaskOpenStatusModel()
                                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus,
                                        Encoding.Unicode) as TaskOpenStatusModel;
                                formData.TaskGuid = task.TaskInstanceId;
                                formData.Concurrency = _taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                                formData.SequenceId = task.SequenceId;
                                formData.AssignedBy = task.AssignedBy;
                                formData.AssignedTo = task.AssignedTo;
                                formData.ManageMode = "ReadOnly";
                               

                                if (formData.TaskOpenStatus != null)
                                    formData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);

                                formData.IsDisplayAcceptPopup = false;
                                formData.IsPAMReport = true;
                            }

                            return View("~/Views/AssetManagement/NCRRequestExtensionAEandReadOnly.cshtml", formData);
                        }
                    }
                    break;
                default:
                    {
                        var proejectActiobViewModel = _plmReportMgr.GetOPARequestId(id);
                        //get info for child task
                        if (proejectActiobViewModel == null)
                        {
                            var task = _taskManager.GetTasksByTaskInstanceId(id).FirstOrDefault();
                            if (task != null && task.DataStore1.Contains("OPAViewModel"))
                            {
                                var formData =
                                    XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1,
                                        Encoding.Unicode) as OPAViewModel;
                                if (formData != null)
                                {
                                    formData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                                        ? new TaskOpenStatusModel()
                                        : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus,
                                            Encoding.Unicode) as TaskOpenStatusModel;
                                    formData.TaskGuid = task.TaskInstanceId;
                                    formData.Concurrency = _taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                                    formData.SequenceId = task.SequenceId;
                                    formData.AssignedBy = task.AssignedBy;
                                    formData.AssignedTo = task.AssignedTo;
                                    formData.PropertyId =
                                        projectActionManager.GetPropertyInfo(formData.FhaNumber).PropertyId;
                                    //formData.ServicerSubmissionDate = TimezoneManager.GetPreferredTimeFromUtc((DateTime)formData.ServicerSubmissionDate);
                                    if (formData.TaskOpenStatus != null)
                                        formData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);

                                    ViewBag.TaskType = "Child";
                                    formData.IsPAMReport = true;
                                    formData.IsEditMode = false;
                                    var ctlr = new OPAFormController();
                                    ctlr.GetOPAFormDetail(formData);

                                }
                                formData.GridVisibility = "PAMChild";
                                formData.IsAgreementAccepted = true;// User Story 1901
                                formData.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("OPA"); // User Story 1901
                                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", formData);

                            }
                        }
                        if (proejectActiobViewModel.MytaskId != null)
                        {
                            var task = _taskManager.GetTaskById((int)proejectActiobViewModel.MytaskId);
                            if (task.DataStore1.Contains("OPAViewModel"))
                            {
                                var formData =
                                    XmlHelper.Deserialize(typeof(OPAViewModel), task.DataStore1,
                                        Encoding.Unicode) as OPAViewModel;
                                if (formData != null)
                                {
                                    formData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                                        ? new TaskOpenStatusModel()
                                        : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus,
                                            Encoding.Unicode) as TaskOpenStatusModel;
                                    formData.TaskGuid = task.TaskInstanceId;
                                    formData.Concurrency = _taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                                    formData.SequenceId = task.SequenceId;
                                    formData.AssignedBy = task.AssignedBy;
                                    formData.AssignedTo = task.AssignedTo;
                                    formData.PropertyId =
                                       projectActionManager.GetPropertyInfo(formData.FhaNumber).PropertyId;
                                    //formData.ServicerSubmissionDate = TimezoneManager.GetPreferredTimeFromUtc((DateTime)formData.ServicerSubmissionDate);
                                    if (formData.TaskOpenStatus != null)
                                        formData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);


                                    ViewBag.TaskType = "Parent";
                                    formData.IsPAMReport = true;
                                    formData.IsEditMode = false;
                                    var ctlr = new OPAFormController();
                                    ctlr.GetOPAFormDetail(formData);

                                }
                                formData.GridVisibility = "PAMParent";
                                formData.columnVisibility = 1;
                                formData.IsAgreementAccepted = true;// User Story 1901
                                formData.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("OPA"); // User Story 1901
                                return View("~/Views/OPAForm/OPARequestFormReadOnly.cshtml", formData);

                            }

                        }
                        break;
                    }
            }
            return null;
        }


        //[HttpGet]
        //public FileResult DownloadTaskFile(Guid? taskInstanceId, FileType taskFileType)
        //{
        //    TaskFileModel taskFile = _taskManager.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId.Value, taskFileType);
        //    if (taskFile != null)
        //    {
        //        return File(taskFile.FileData, "text/text", taskFile.FileName);
        //    }
        //    return null;
        //}
        //#872 harish added this below method to download files from Pam Report both external user and Internal user (HUD )
        [HttpGet]
        public FileResult DownloadTaskFile(Guid? taskInstanceId, FileType taskFileType)
        {
            //harish commented below line to download file from  database  08052020 
            //TaskFileModel taskFile = _taskManager.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId.Value, taskFileType);
            //if (taskFile != null)
            //{
            //    return File(taskFile.FileData, "text/text", taskFile.FileName);
            //}
            //return null;
            //harish  below line to download file from both database and Transacess server too 08052020 
            TaskFileModel taskFile = _taskManager.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId.Value, taskFileType);
            if (taskFile != null)
            {
                if (taskFile.DocId == null && taskFile.API_upload_status == null)
                {
                    return File(taskFile.FileData, "text/text", taskFile.FileName);
                }
                else
                {
                    string JsonSteing = "";
                    RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
                    string filePath = "";
                    FileInfo fileInfo = null;
                    filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                    fileInfo = new System.IO.FileInfo(filePath);
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                    if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                    {
                        request = webApiTokenRequest.RequestToken();
                        JsonSteing = "{\"docId\":" + taskFile.DocId + ",\"version\":" + taskFile.Version + "}";
                        Stream streamResult = WebApiDownload.DownloadDocumentUsingWebApi(JsonSteing, request.access_token, taskFile.FileName);
                        return new FileStreamResult(streamResult, "application/octet-stream");
                    }

                    else
                    {
                        filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                        fileInfo = new System.IO.FileInfo(filePath);
                        Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                        Response.WriteFile(filePath);
                    }
                    Response.End();

                }
            }

            return null;
        }
        // User Story 1901
        private void GetDisclaimerTextDescriptionR4R(ReserveForReplacementFormModel model)
        {

            model.DisclaimerText = _plmReportMgr.GetDisclaimerMsgByPageType("R4R");


        }

        //naresh add 29012020
        [HttpPost]
        public string SaveAssetPamReportFilterDetails(string daysopen, string _minUploadDate, string _maxUploadDate, string wlmUsers = null, string aeUsers = null, string isoUsers = null, string status = null, string projectAction = null, string fhanumber = null, string assignto = null, string actionopen = null, string proprname = null)
        {
            var isInfoUpdate = "Fail";
            HUDAssetpamreportfiltermodel hudassetpamreport = new HUDAssetpamreportfiltermodel();
            hudassetpamreport.daysopen = daysopen;
            hudassetpamreport.fromdate = _minUploadDate;
            hudassetpamreport.todate = _maxUploadDate;
            hudassetpamreport.wlmUsers = wlmUsers;
            hudassetpamreport.aeUsers = aeUsers;
            hudassetpamreport.isoUsers = isoUsers;
            hudassetpamreport.Status = status;
            hudassetpamreport.projectAction = projectAction;
            hudassetpamreport.fhanumber = fhanumber;
            hudassetpamreport.AssignedTo = assignto;
            hudassetpamreport.Actionopen = actionopen;
            hudassetpamreport.propertyname = proprname;
            try
            {
                isInfoUpdate = plmreport.SaveAssetPamReportFilterDetails(hudassetpamreport);

            }
            catch (Exception ex)
            {
                isInfoUpdate = "Fail";
            }
            return isInfoUpdate;
        }

        //Added by siddu//
        [HttpGet]
        public ActionResult clear(string isouIds, string aeIds, string fromDate, string toDate, string status, string projectAction, int? page, string sort, string sortdir, string wlmIds
                  , string proprname = null, string fhanumber = null, string assignto = null, int? actionopen = null, int? daysopen = null)
        {
            //var SavedFilter = plmreport.GetAssetSavedFilter();
            int wlmId = 0, aeId = 0, isouId = 0;
            int? requestStatus = string.IsNullOrWhiteSpace(status) || status == "1,0" || status == "0,1" ? (int?)null : int.Parse(status);
            //   int? requestProjectAction = string.IsNullOrWhiteSpace(projectAction) ? (int?)null : projectAction.Length > 3 ? (int?)null : int.Parse(projectAction == "0,1" || projectAction == "1,0" ? "10" : projectAction == "0,2" || projectAction == "2,0" ? "20" : projectAction == "1,2" || projectAction == "2,1" ? "12" : projectAction);
            string requestProjectAction = projectAction;
            var routeValues =
                SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(
                    SessionHelper.SESSION_KEY_PAM_REPORT_LINK_DICT);
            int pageNum = page ?? 1;
            string sortString = string.IsNullOrEmpty(sort) ? "IsProjectActionOpen,ProjectActionSubmitDate" : sort;
            var sortOrder = string.IsNullOrEmpty(sortdir)
                ? EnumUtils.Parse<SqlOrderByDirecton>("DESC")
                : EnumUtils.Parse<SqlOrderByDirecton>(sortdir);
            var fromUTCDate = fromDate == null ? (DateTime?)null : TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(fromDate));
            var toUTCDate = toDate == null ? (DateTime?)null : TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(toDate));
            var viewModel = _plmReportMgr.GetPAMSummary(UserPrincipal.Current.UserName, isouIds, aeIds, fromUTCDate,
           toUTCDate, wlmId, aeId, isouId, requestStatus, requestProjectAction, pageNum, sortString, sortOrder, wlmIds
           , proprname, fhanumber, assignto, actionopen, daysopen);

            return PartialView("~/Views/ManagementReport/PAMReports/_PAMReport.cshtml", viewModel);
        }
    }
}
