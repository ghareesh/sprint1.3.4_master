﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Model.Production;
using System.Collections;
using Model;


namespace HUDHealthcarePortal.Controllers.ManagementReport
{
    public class ProdLenderPAMController : Controller
    {
        private IProductionPAMReportManager _plmProdReportMgr;
        private ITaskManager _taskManager;
        private ILenderPAMReportManager _lenderPamReportManager;

        public ProdLenderPAMController()
            : this(new LenderPAMReportManager(), new ProductionPAMReportManager(), new TaskManager())
        {

        }

        public ProdLenderPAMController(ILenderPAMReportManager lenderPamReportManager, IProductionPAMReportManager PAMProdReportManager, ITaskManager taskManager)
        {
            _lenderPamReportManager = lenderPamReportManager;
            _plmProdReportMgr = PAMProdReportManager;
            _taskManager = taskManager;
        }

        private ProductionPAMReportModel InitializeViewModel(string roleIds, string larIds, string bamIds, string lamIds, string fromDate, string toDate,
           string status, string projectAction, string isApplicationOrTaskLevel)
        {
            var viewModel = new ProductionPAMReportModel(ReportType.ProductionLenderPAMReport);
            var userName = UserPrincipal.Current.UserName;
            var userId = UserPrincipal.Current.UserId;
            var lenderUserFullName = UserPrincipal.Current.FullName;
            var lenderId = UserPrincipal.Current.LenderId ?? default(int);


            var lamListItems = new List<KeyValuePair<int, string>>();
            var bamListItems = new List<KeyValuePair<int, string>>();
            var larListItems = new List<KeyValuePair<int, string>>();

            //Check role list and Get lenders accordingly for Multiselect list
            if (string.IsNullOrEmpty(roleIds))
            {
                viewModel.LAMListItems = new MultiSelectList(lamListItems, "Key", "Value");
                viewModel.BAMListItems = new MultiSelectList(bamListItems, "Key", "Value");
                viewModel.LARListItems = new MultiSelectList(larListItems, "Key", "Value");
            }
            else
            {
                if (roleIds.Contains("1") && !string.IsNullOrEmpty(lamIds))
                {
                    var roleName = HUDRole.LenderAccountManager.ToString("g");
                    lamListItems = GetLendersWithRoleLenderId(lenderId, roleName);


                    IList selectedValues = new List<int>();
                    var lamStr = lamIds.Split(',');
                    foreach (var item in lamStr)
                    {
                        foreach (var pair in lamListItems)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if (pair.Key == int.Parse(item))
                                {
                                    selectedValues.Add(pair.Key);
                                }
                            }
                        }
                    }
                    viewModel.LAMListItems = new MultiSelectList(lamListItems, "Key", "Value", selectedValues);
                }
                else
                {
                    viewModel.LAMListItems = new MultiSelectList(lamListItems, "Key", "Value");
                }

                if (roleIds.Contains("3") && !string.IsNullOrEmpty(larIds))
                {
                    larListItems = GetLendersWithRoleLenderId(lenderId, HUDRole.LenderAccountRepresentative.ToString("g"));

                    IList selectedValues = new List<int>();
                    var larStr = larIds.Split(',');
                    foreach (var item in larStr)
                    {
                        foreach (var pair in larListItems)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if (pair.Key == int.Parse(item))
                                {
                                    selectedValues.Add(pair.Key);
                                }
                            }
                        }
                    }
                    viewModel.LARListItems = new MultiSelectList(larListItems, "Key", "Value", selectedValues);
                }
                else
                {
                    viewModel.LARListItems = new MultiSelectList(larListItems, "Key", "Value");
                }
                if (roleIds.Contains("2") && !string.IsNullOrEmpty(bamIds))
                {
                    bamListItems = GetLendersWithRoleLenderId(lenderId, HUDRole.BackupAccountManager.ToString("g"));

                    IList selectedValues = new List<int>();
                    var bamStr = bamIds.Split(',');
                    foreach (var item in bamStr)
                    {
                        foreach (var pair in bamListItems)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if (pair.Key == int.Parse(item))
                                {
                                    selectedValues.Add(pair.Key);
                                }
                            }
                        }
                    }
                    viewModel.BAMListItems = new MultiSelectList(bamListItems, "Key", "Value", selectedValues);
                }
                else
                {
                    viewModel.BAMListItems = new MultiSelectList(bamListItems, "Key", "Value");
                }
            }
            var prodPageTypes = _plmProdReportMgr.GetProductionPageTypes();
            var prodPageTypesKeys = prodPageTypes.ToDictionary(p => p.PageTypeId).Keys.ToArray();
            //var prodPageTypesKeys = prodPageTypes.ToArray();

            var prodProjectTypes = _plmProdReportMgr.GetProjectTypeModels();
            var prodProjectTypesKeys = prodProjectTypes.ToDictionary(p => p.ProjectTypeId).Keys.ToArray();

            viewModel.LenderName = _lenderPamReportManager.GetLenderName(lenderId);
            viewModel.LenderUserFullName = lenderUserFullName;
            SetViewModelRoleList(roleIds, viewModel);
            setViewModelReportLevel(viewModel);

            viewModel.ProductionPageTypes = new MultiSelectList(prodPageTypes, "PageTypeId", "PageTypeDescription", prodPageTypesKeys);
            viewModel.PamStatusList = new MultiSelectList(new Dictionary<int, string> { { 1, "In Queue" }, { 2, "Open" }, { 3, "Closed" } }, "Key", "Value",
                    new[] { 1, 2, 3 });
            viewModel.ProductionProjecTypeList = new MultiSelectList(prodProjectTypes, "ProjectTypeId", "ProjectTypeName", prodProjectTypesKeys);

            return viewModel;
        }
        //Gets lenders list on role change event
        public JsonResult GetLendersByRoleLenderId(string value)
        {
            var roleName = string.Empty;

            var lenderId = UserPrincipal.Current.LenderId ?? default(int);
            List<object> LAMList = new List<object>();
            List<object> BAMList = new List<object>();
            List<object> LARList = new List<object>();

            //Gets Lam list if Lam is checked
            if (value.Contains("1"))
            {
                IList selectedValues = new List<int>();
                roleName = HUDRole.LenderAccountManager.ToString("g");
                var lenders = _lenderPamReportManager.GetLenderUsersByRoleLenderId(lenderId, roleName).ToList();

                LAMList = new List<object>(lenders.Count);
                foreach (Model.UserInfoModel m in lenders)
                {
                    LAMList.Add(new { Key = m.UserID, Value = m.FirstName + " " + m.LastName });
                }

            }
            //Gets Bam list if Bam is checked
            if (value.Contains("2"))
            {
                roleName = HUDRole.BackupAccountManager.ToString("g");
                var lenders = _lenderPamReportManager.GetLenderUsersByRoleLenderId(lenderId, roleName).ToList();

                BAMList = new List<object>(lenders.Count);
                foreach (Model.UserInfoModel m in lenders)
                {
                    BAMList.Add(new { Key = m.UserID, Value = m.FirstName + " " + m.LastName });
                }
            }
            //Gets Lar list if Lar is checked
            if (value.Contains("3"))
            {
                roleName = HUDRole.LenderAccountRepresentative.ToString("g");
                var lenders = _lenderPamReportManager.GetLenderUsersByRoleLenderId(lenderId, roleName).ToList();

                LARList = new List<object>(lenders.Count);
                foreach (Model.UserInfoModel m in lenders)
                {
                    LARList.Add(new { Key = m.UserID, Value = m.FirstName + " " + m.LastName });
                }
            }

            var data = new { LARList = LARList, LAMList = LAMList, BAMList = BAMList };

            return Json(new { data }, JsonRequestBehavior.AllowGet);
        }


        //Sets view model data for Role
        private void SetViewModelRoleList(string roleList, ProductionPAMReportModel viewModel)
        {
            var bamText = string.Empty;
            var lamText = string.Empty;

            if (UserPrincipal.Current.UserRole.Equals(HUDRole.BackupAccountManager.ToString("g")))
            {
                bamText = "Other BAMs";
                lamText = "LAMs";
            }
            else if (UserPrincipal.Current.UserRole.Equals(HUDRole.LenderAccountManager.ToString("g")))
            {
                bamText = "BAMs";
                lamText = "Other LAMs";
            }

            if (!string.IsNullOrEmpty(roleList))
            {
                var rlArray = roleList.Split(',').Select(h => Int32.Parse(h)).ToArray();
                viewModel.RoleListItems = new MultiSelectList(new Dictionary<int, string> { { 0, "Self" }, { 1, lamText }, { 2, bamText }, { 3, "LARs" } }, "Key", "Value",
                       rlArray);
            }
            else
            {
                viewModel.RoleListItems = new MultiSelectList(new Dictionary<int, string> { { 0, "Self" }, { 1, lamText }, { 2, bamText }, { 3, "LARs" } }, "Key", "Value",
                        null);
            }

        }

        //Sets view model data for Report name (used in Excel export)
        private void setViewModelReportLevel(ProductionPAMReportModel viewModel)
        {

            if (UserPrincipal.Current.UserRole.Equals(HUDRole.BackupAccountManager.ToString("g")))
            {
                viewModel.ReportLevel = ReportLevel.BAMUserLevel;
            }
            else if (UserPrincipal.Current.UserRole.Equals(HUDRole.LenderAccountManager.ToString("g")))
            {
                viewModel.ReportLevel = ReportLevel.LAMUserLevel;
            }
            else if (UserPrincipal.Current.UserRole.Equals(HUDRole.LenderAccountRepresentative.ToString("g")))
            {
                viewModel.ReportLevel = ReportLevel.LARUserLevel;
            }
        }


        public ActionResult Index(string roleIds = null, string larIds = null, string bamIds = null, string lamIds = null, string fromDate = null, string toDate = null,
            string status = null, string projectAction = null, string isApplicationOrTaskLevel = null)
        {
            ViewBag.GetLendersByRoleLenderIdUrl = Url.Action("GetLendersByRoleLenderId");
            ViewBag.ExcelExportAction = "ExportProdPAMReport";
            ViewBag.PrintAction = "PrintProdPAMReport";
            ViewBag.GetProjectTypesByAppTypeAction = Url.Action("GetProjectTypesByAppType");
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            var viewModel = InitializeViewModel(roleIds, larIds, bamIds, lamIds, fromDate, toDate, status, projectAction,
                isApplicationOrTaskLevel);
            return View("~/Views/ManagementReport/PAMReports/ProdLenderPAMReport.cshtml", viewModel);

        }

        //Gets Lender list for selected role and user lender id 1
        private List<KeyValuePair<int, string>> GetLendersWithRoleLenderId(int lenderId, string roleName)
        {
            var lenders = _lenderPamReportManager.GetLenderUsersByRoleLenderId(lenderId, roleName).ToList();

            List<KeyValuePair<int, string>> lendersList = new List<KeyValuePair<int, string>>(lenders.Count);
            foreach (Model.UserInfoModel m in lenders)
            {
                lendersList.Add(new KeyValuePair<int, string>(m.UserID, m.FirstName + " " + m.LastName));

            }
            return lendersList;

        }


        public JsonResult GetLenderPAMSubGridChildTasks(string sidx, string sord, int page, int rows, Guid taskInstanceId)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var currentUser = UserPrincipal.Current;


            var productionTasks = _plmProdReportMgr.GetLenderPAMSubGridChildTasks(taskInstanceId);

            if (productionTasks != null)
            {
                foreach (var item in productionTasks.PAMReportGridProductionlList)
                {
                    item.StartDate = TimezoneManager.ToMyTimeFromUtc(item.StartDate);
                    item.EndDate = TimezoneManager.ToMyTimeFromUtc(item.EndDate);
                }

            }

            int totalrecods = productionTasks.PAMReportGridProductionlList.Count;
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);

            var results = productionTasks.PAMReportGridProductionlList.Skip(pageIndex * pageSize).Take(pageSize);
            var outresults = results.GroupBy(s => s.Groupid).SelectMany(gr => gr).ToList();
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = outresults,

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLenderPAMTasks(string sidx, string sord, int page, int rows, string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate, string status, string appType, string programTypes)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var currentUser = UserPrincipal.Current;
            //format date
            //DateTime? fromUTCDate = null;
            // DateTime? toUTCDate = null;
            //if(fromDate!=null)
            //{
            //    fromUTCDate =  TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(fromDate));
            //}

            //if (toDate != null)
            //{
            //   toUTCDate = TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(toDate));
            //}


            var productionTasks = _plmProdReportMgr.GetProductionLenderPAMReportSummary(lamIds, bamIds, larIds, roleIds, fromDate, toDate, status, appType, programTypes);

            if (productionTasks != null)
            {
                foreach (var item in productionTasks.PAMReportGridProductionlList)
                {
                    //item.StartDate = TimezoneManager.ToMyTimeFromUtc(item.StartDate);
                    //item.EndDate = TimezoneManager.ToMyTimeFromUtc(item.EndDate);
                    item.StartDate = item.StartDate;
                    item.EndDate =item.EndDate;
                }

            }

            int totalrecods = productionTasks.PAMReportGridProductionlList.Count;
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var olistproductionTasks = productionTasks.PAMReportGridProductionlList.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.Groupid).SelectMany(gr => gr).OrderBy(o => o.OrderBy).ToList();
            var results = olistproductionTasks.Skip(pageIndex * pageSize).Take(pageSize);
            var outresults = results.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.Groupid).SelectMany(gr => gr).OrderBy(o => o.OrderBy).ToList();
            //var results = productionTasks.PAMReportGridProductionlList.Skip(pageIndex * pageSize).Take(pageSize);
            //var outresults = results.GroupBy(s => s.Groupid).SelectMany(gr => gr).ToList();
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = outresults,

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }



    }
}
