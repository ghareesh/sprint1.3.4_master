﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Model.Production;
using HUDHealthcarePortal.Helpers;

namespace HUDHealthcarePortal.Controllers.ManagementReport
{
    public class ProdPAMController : Controller
    {
        private IProductionPAMReportManager _plmProdReportMgr;
        private ITaskManager _taskManager;

        public ProdPAMController() : this(new ProductionPAMReportManager(), new TaskManager())
        {

        }

        public ProdPAMController(IProductionPAMReportManager PAMProdReportManager, ITaskManager taskManager)
        {
            _plmProdReportMgr = PAMProdReportManager;
            _taskManager = taskManager;
        }

        private ProductionPAMReportModel InitializeViewModel(string prodUserIds, string fromDate, string toDate,
            string status, string projectAction, string isApplicationOrTaskLevel)
        {
            var viewModel = new ProductionPAMReportModel(ReportType.HudProductionReport);
            var userName = UserPrincipal.Current.UserName;
            string prodWLMName = string.Empty;

            var isDefaultSearch = false;
            var prodUsers = _plmProdReportMgr.GetProductionUsers().ToList();
            ;

            if (RoleManager.IsProductionWLM(userName))
            {
                prodWLMName = UserPrincipal.Current.FullName;
            }

            var prodPageTypes = _plmProdReportMgr.GetProductionPageTypes();
            var prodPamStatuses = _plmProdReportMgr.GetPAMStatusList();
            var prodProjectTypes = _plmProdReportMgr.GetProjectTypeModels();
            viewModel.ProductionUsers = new MultiSelectList(prodUsers, "UserID", "Name");
            viewModel.ProductionPageTypes = new MultiSelectList(prodPageTypes, "PageTypeId", "PageTypeDescription");
            viewModel.PamStatusList = new MultiSelectList(prodPamStatuses, "PamStatusId", "StatusDescription");
            viewModel.ProductionProjecTypeList = new MultiSelectList(prodProjectTypes, "ProjectTypeId",
                "ProjectTypeName");

            return viewModel;
        }

        public ActionResult Index(string prodUserIds = null, string fromDate = null, string toDate = null,
            string status = null, string projectAction = null, string isApplicationOrTaskLevel = null)
        {
            ViewBag.ExcelExportAction = "ExportProdPAMReport";
            ViewBag.PrintAction = "PrintProdPAMReport";
            ViewBag.GetProjectTypesByAppTypeAction = Url.Action("GetProjectTypesByAppType");
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            var viewModel = InitializeViewModel(prodUserIds, fromDate, toDate, status, projectAction,
                isApplicationOrTaskLevel);
            //return View("~/Views/ManagementReport/PAMReports/ProdPAMReport.cshtml", viewModel);
            return View("~/Views/ManagementReport/PAMReports/ProdPAMNewReport.cshtml");

        }


        /// <summary>
        /// Prod pam report
        /// </summary>
        /// <param name="prodUserIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="status"></param>
        /// <param name="projectAction"></param>
        /// <param name="isApplicationOrTaskLevel"></param>
        /// <returns></returns>
        public ActionResult PAMIndex(string prodUserIds = null, string fromDate = null, string toDate = null,
            string status = null, string projectAction = null, string isApplicationOrTaskLevel = null)
        {
            ProdPAMReportStatusGridViewModel statusGridViewModel = new ProdPAMReportStatusGridViewModel();
            List<string> TotalNumberOfFYDistinctFHA = new List<string>();
            List<string> LoanOpenedDistinctFHA = new List<string>();
            List<string> TotalClosingDistinctFHA = new List<string>();
            List<string> TotalNumberOfProjectsDistinctFHA = new List<string>();
            List<string> TotalNumberOfFYNA = new List<string>();
            List<string> LoanOpenedNA = new List<string>();
            List<string> TotalClosingNA = new List<string>();
            List<string> TotalNumberOfProjectsNA = new List<string>();
            List<string> TotalNumberOf223a7DistinctFHA = new List<string>();
            List<string> TotalNumberOf223fDistinctFHA = new List<string>();
            List<string> TotalNumberOf223dDistinctFHA = new List<string>();
            List<string> TotalNumberOf223iDistinctFHA = new List<string>();
            List<string> TotalNumberOf241aDistinctFHA = new List<string>();
            List<string> TotalNumberOfNCDistinctFHA = new List<string>();
            List<string> TotalNumberOfSRDistinctFHA = new List<string>();
            List<string> TotalNumberOfIRRDistinctFHA = new List<string>();
            List<string> TotalNumberOf223a7NA = new List<string>();
            List<string> TotalNumberOf223fNA = new List<string>();
            List<string> TotalNumberOf223dNA = new List<string>();
            List<string> TotalNumberOf223iNA = new List<string>();
            List<string> TotalNumberOf241aNA = new List<string>();
            List<string> TotalNumberOfNCNA = new List<string>();
            List<string> TotalNumberOfSRNA = new List<string>();
            List<string> TotalNumberOfIRRNA = new List<string>();

            var dates = ControllerHelper.GetFYDuration();
            DateTime fisStart = dates[0];
            DateTime fisEnd = dates[1];
            //DateTime fisStart = new DateTime(DateTime.Now.Year - 1, 09, 30);
            //DateTime fisEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            ViewBag.ExcelExportAction = "ExportProdPAMReport";
            ViewBag.PrintAction = "PrintProdPAMReport";
            ViewBag.GetProjectTypesByAppTypeAction = Url.Action("GetProjectTypesByAppType");
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            //NM-- startz
            //IList<PAMReportV3ViewModel> outresults = _plmProdReportMgr.GetProdHUDPAMReportResults();
            IList<PAMReportV3ViewModel> outresults = GetPAMRepository().ToList();

            var hudPamReportFilters = getValues(outresults);
            //NM --end
            /* NM -- old code 
             * var hudPamReportFilters = getValues(outresults);
             * var outresults = _plmProdReportMgr.GetProdHUDPAMReportResults();
             */
            statusGridViewModel = ControllerHelper.GetPamSummary();
            statusGridViewModel.hudPamReportFilters = hudPamReportFilters;


            //statusGridViewModel.TotalNumberOf223a7 = outresults.Where(x => x.LoanType == "Refinance 223(a)(7)").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalNumberOf223f = outresults.Where(x => x.LoanType == "Purchase/Refinance 223(f)").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalNumberOf223d = outresults.Where(x => x.LoanType == "OLL 223(d)").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalNumberOf223i = outresults.Where(x => x.LoanType == "Fire Safety 223(i)").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalNumberOf241a = outresults.Where(x => x.LoanType == "Construction 241(a)").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalNumberOfNC = outresults.Where(x => x.LoanType == "Construction NC").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalNumberOfSR = outresults.Where(x => x.LoanType == "Construction SR").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalNumberOfIRR = outresults.Where(x => x.LoanType == "IRR").Select(i => i.FhaNumber).Distinct().Count();
            ////statusGridViewModel.TotalNumberOfFY = outresults.Where(x => x.ProjectStartDate >= fisStart && x.ProjectStartDate <= fisEnd).Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.LoanOpened = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Open").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalClosing = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Complete").Select(i => i.FhaNumber).Distinct().Count();
            //statusGridViewModel.TotalNumberOfProjects = outresults.Where(x => x.LoanType != null).Select(i => i.FhaNumber).Distinct().Count();

            //statusGridViewModel.TotalNumberOfFY = outresults.Where(x => x.ProjectStartDate >= fisStart && x.ProjectStartDate <= fisEnd).Select(i => i.FhaNumber).Distinct().Count() +
            //	outresults.Where(x => x.ProjectStartDate >= fisStart && x.ProjectStartDate <= fisEnd).Where(o=>o.FhaNumber == "N/A").Count();
            //statusGridViewModel.LoanOpened = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Open").Select(i => i.FhaNumber).Distinct().Count() +
            //	outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Open").Where(x=>x.FhaNumber == "N/A").Count();
            //statusGridViewModel.TotalClosing = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Complete").Select(i => i.FhaNumber).Distinct().Count() +
            //	outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Complete").Where(x => x.FhaNumber == "N/A").Count();
            //statusGridViewModel.TotalNumberOfProjects = outresults.Where(x => x.LoanType != null).Select(i => i.FhaNumber).Distinct().Count() +
            //	outresults.Where(x => x.LoanType != null).Where(x => x.FhaNumber == "N/A").Count(); 


            TotalNumberOf223a7DistinctFHA = outresults.Where(x => x.LoanType == "Refinance 223(a)(7)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf223fDistinctFHA = outresults.Where(x => x.LoanType == "Purchase/Refinance 223(f)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf223dDistinctFHA = outresults.Where(x => x.LoanType == "OLL 223(d)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf223iDistinctFHA = outresults.Where(x => x.LoanType == "Fire Safety 223(i)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOf241aDistinctFHA = outresults.Where(x => x.LoanType == "Construction 241(a)").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfNCDistinctFHA = outresults.Where(x => x.LoanType == "Construction NC").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfSRDistinctFHA = outresults.Where(x => x.LoanType == "Construction SR").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfIRRDistinctFHA = outresults.Where(x => x.LoanType == "IRR").Select(i => i.FhaNumber).Distinct().ToList();

            TotalNumberOf223a7NA = TotalNumberOf223a7DistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf223fNA = TotalNumberOf223fDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf223dNA = TotalNumberOf223dDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf223iNA = TotalNumberOf223iDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf241aNA = TotalNumberOf241aDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfNCNA = TotalNumberOfNCDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfSRNA = TotalNumberOfSRDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfIRRNA = TotalNumberOfIRRDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOf223a7DistinctFHA = TotalNumberOf223a7DistinctFHA.Except(TotalNumberOf223a7NA).ToList();
            TotalNumberOf223fDistinctFHA = TotalNumberOf223fDistinctFHA.Except(TotalNumberOf223fNA).ToList();
            TotalNumberOf223dDistinctFHA = TotalNumberOf223dDistinctFHA.Except(TotalNumberOf223dNA).ToList();
            TotalNumberOf223iDistinctFHA = TotalNumberOf223iDistinctFHA.Except(TotalNumberOf223iNA).ToList();
            TotalNumberOf241aDistinctFHA = TotalNumberOf241aDistinctFHA.Except(TotalNumberOf241aNA).ToList();
            TotalNumberOfNCDistinctFHA = TotalNumberOfNCDistinctFHA.Except(TotalNumberOfNCNA).ToList();
            TotalNumberOfSRDistinctFHA = TotalNumberOfSRDistinctFHA.Except(TotalNumberOfSRNA).ToList();
            TotalNumberOfIRRDistinctFHA = TotalNumberOfIRRDistinctFHA.Except(TotalNumberOfIRRNA).ToList();
            TotalNumberOfFYDistinctFHA = outresults.Where(x => x.ProjectStartDate >= fisStart && x.ProjectStartDate <= fisEnd).Select(i => i.FhaNumber).Distinct().ToList();
            LoanOpenedDistinctFHA = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Open").Select(i => i.FhaNumber).Distinct().ToList();
            TotalClosingDistinctFHA = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Complete").Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfProjectsDistinctFHA = outresults.Where(x => x.LoanType != null).Select(i => i.FhaNumber).Distinct().ToList();
            TotalNumberOfFYNA = TotalNumberOfFYDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            LoanOpenedNA = LoanOpenedDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalClosingNA = TotalClosingDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfProjectsNA = TotalNumberOfProjectsDistinctFHA.Where(x => x.Contains("N/A")).ToList();
            TotalNumberOfFYDistinctFHA = TotalNumberOfFYDistinctFHA.Except(TotalNumberOfFYNA).ToList();
            LoanOpenedDistinctFHA = LoanOpenedDistinctFHA.Except(LoanOpenedNA).ToList();
            TotalClosingDistinctFHA = TotalClosingDistinctFHA.Except(TotalClosingNA).ToList();
            TotalNumberOfProjectsDistinctFHA = TotalNumberOfProjectsDistinctFHA.Except(TotalNumberOfProjectsNA).ToList();

            statusGridViewModel.TotalNumberOf223a7 = TotalNumberOf223a7DistinctFHA.Count() + outresults.Where(x => x.LoanType == "Refinance 223(a)(7)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOf223f = TotalNumberOf223fDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Purchase/Refinance 223(f)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOf223d = TotalNumberOf223dDistinctFHA.Count() + outresults.Where(x => x.LoanType == "OLL 223(d)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOf223i = TotalNumberOf223iDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Fire Safety 223(i)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOf241a = TotalNumberOf241aDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Construction 241(a)").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfNC = TotalNumberOfNCDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Construction NC").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfSR = TotalNumberOfSRDistinctFHA.Count() + outresults.Where(x => x.LoanType == "Construction SR").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfIRR = TotalNumberOfIRRDistinctFHA.Count() + outresults.Where(x => x.LoanType == "IRR").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfFY = TotalNumberOfFYDistinctFHA.Count() + outresults.Where(x => x.ProjectStartDate >= fisStart && x.ProjectStartDate <= fisEnd).Where(o => o.FhaNumber == "N/A").Count();
            statusGridViewModel.LoanOpened = LoanOpenedDistinctFHA.Count() + outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Open").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalClosing = TotalClosingDistinctFHA.Count() + outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Complete").Where(x => x.FhaNumber == "N/A").Count();
            statusGridViewModel.TotalNumberOfProjects = TotalNumberOfProjectsDistinctFHA.Count() + outresults.Where(x => x.LoanType != null).Where(x => x.FhaNumber == "N/A").Count();

            statusGridViewModel.TotalAppraiserAssigned = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerAppraiserStatus != null && x.ReviewerAppraiserStatus != "Unassigned" && x.ReviewerAppraiserStatus != "N/A").Count();
            statusGridViewModel.TotalEnvironmentalistAssigned = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerEnvironmentalistStatus != null && x.ReviewerEnvironmentalistStatus != "Unassigned" && x.ReviewerEnvironmentalistStatus != "N/A").Count();
            statusGridViewModel.TotalTitleSurveyAssigned = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerTitleSurveyStatus != null && x.ReviewerTitleSurveyStatus != "Unassigned" && x.ReviewerTitleSurveyStatus != "N/A").Count();
            return View("~/Views/ManagementReport/PAMReports/PAMReportV3.cshtml", statusGridViewModel);

        }



        public JsonResult GetPAMTasks(string sidx, string sord, int page, int rows, string appType, string programType, string prodUserIds, DateTime? fromDate, DateTime? toDate, string status, string Level = "High", Guid? grouptaskinstanceid = null)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var currentUser = UserPrincipal.Current;
            //format date
            //DateTime? fromUTCDate = null;
            // DateTime? toUTCDate = null;
            //if(fromDate!=null)
            //{
            //    fromUTCDate =  TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(fromDate));
            //}

            //if (toDate != null)
            //{
            //   toUTCDate = TimezoneManager.GetUtcTimeFromPreferred(Convert.ToDateTime(toDate));
            //}
            if (status != null && status.Contains("18"))
            {
                status = status.Replace("18", "18,17");
            }

            if (status != null && status.Contains("19"))
            {
                status = status.Replace("19", "19,15");
            }


            var productionTasks = _plmProdReportMgr.GetProductionPAMReportSummary(appType, programType, prodUserIds,
fromDate == null ? (DateTime?)null : DateTime.Parse(fromDate.ToString()),
toDate == null ? (DateTime?)null : DateTime.Parse(toDate.ToString()), status, Level, grouptaskinstanceid);

            if (productionTasks != null)
            {
                foreach (var item in productionTasks.PAMReportGridProductionlList)
                {
                    //item.StartDate = TimezoneManager.ToMyTimeFromUtc(item.StartDate);
                    //item.EndDate = TimezoneManager.ToMyTimeFromUtc(item.EndDate);
                    item.StartDate = item.StartDate;
                    item.EndDate = item.EndDate;
                }

            }

            int totalrecods = productionTasks.PAMReportGridProductionlList.Count;
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);







            if (Level != "High")
            {

                // var underWriter = productionTasks.PAMReportGridProductionlList.Where(a => a.viewname == "UnderWriter" && a.RAI == null).ToList().Select(m=>m.);
                var temp = productionTasks.PAMReportGridProductionlList.Where(a => a.viewname == "Underwriter/Closer" && a.RAI == null).ToList();
                productionTasks.PAMReportGridProductionlList = productionTasks.PAMReportGridProductionlList.Except(temp).ToList();
                // productionTasks.PAMReportGridProductionlList = productionTasks.PAMReportGridProductionlList.Where(a => a.viewname == "UnderWriter" && a.RAI ==null).ToList();
            }

            var results = productionTasks.PAMReportGridProductionlList.Skip(pageIndex * pageSize).Take(pageSize);
            var outresults = results.GroupBy(s => s.Groupid).SelectMany(gr => gr).ToList();
            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = outresults,

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPAMReportResults()
        {
            //var outresults = new List<FHANumberRequestViewModel>(){
            //    new FHANumberRequestViewModel(){ FHANumber ="123-45678" },
            //    new FHANumberRequestViewModel(){ FHANumber ="345-67890" }
            //};

            var outresults = _plmProdReportMgr.GetAllFHARequests();
            var jsonData = new
            {
                total = outresults.Count,
                page = 1,
                records = outresults.Count,
                rows = outresults,

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// pam report - sub tasks
        /// </summary>
        /// <param name="pTaskInstanceId"></param>
        /// <param name="pFHANumber"></param>
        /// <returns></returns>
        public JsonResult GetPAMSubReportResultsV3(Guid pTaskInstanceId, string pFHANumber)
        {//skumar-2838
            var outresults = _plmProdReportMgr.GetProdHUDPAMSubReportResults(pTaskInstanceId, pFHANumber);
            var olisNullProjectStage = outresults.Where(o => o.ProjectStage == null).ToList();
            outresults = outresults.Except(olisNullProjectStage).ToList();


            var jsonData = (from data in outresults
                            select new
                            {
                                data.FhaNumber,
                                data.ProjectName,
                                data.LoanType,
                                data.ProjectStartDate,
                                data.ProjectStage,
                                data.ProjectStatus,
                                data.UnderWriterAssigned,
                                data.ReviewerAppraiserStatus,
                                data.ReviewerEnvironmentalistStatus,
                                data.ReviewerTitleSurveyStatus,
                                data.TotalDays,
                                data.LoanAmount,
                                data.CurrentTaskInstanceId,
                                data.AppraiserStatus,
                                data.EnvironmentalistStatus,
                                data.TitleSurveyStatus,
                                data.Status,
                                data.IsUnderWriterAssigned,
                                data.OrderByPageTypeId
                                //AssignedBy = productionQueue.GetUserById(data.AssignedBy).FirstName + " " + productionQueue.GetUserById(data.AssignedBy).LastName,
                                //AssignedTo = data.AssignedTo != null ? productionQueue.GetUserById(data.AssignedTo).FirstName + " " + productionQueue.GetUserById(data.AssignedTo).LastName : "",
                                //Status = EnumType.GetEnumDescription(EnumType.Parse<ProductionFhaRequestStatus>(data.Status.ToString())),
                                //FhaType = EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(data.ViewId.ToString()))

                            }).OrderBy(m => m.OrderByPageTypeId);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Pam popup summary for status
        /// </summary>
        /// <param name="pFilterType"></param>
        /// <returns></returns>
        public PartialViewResult PamStatusPopup(string pFilterType = null)
        {
            int status = 0;
            var outresults = _plmProdReportMgr.GetProdHUDPAMReportResults();
            var dates = ControllerHelper.GetFYDuration();
            DateTime fisStart = dates[0];
            DateTime fisEnd = dates[1];
            List<PAMReportV3ViewModel> olistAssignedFHAPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
            List<PAMReportV3ViewModel> olistAssignedAPTasksPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
            List<PAMReportV3ViewModel> olistAssignedNCTasksPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
            List<PAMReportV3ViewModel> olistAssignedForm290PAMReportV3ViewModel = new List<PAMReportV3ViewModel>();
            List<PAMReportV3ViewModel> olistAssignedAmendmentTasksPAMReportV3ViewModel = new List<PAMReportV3ViewModel>();

            status = ControllerHelper.ExtractIntFromString(pFilterType);
            //olistAssignedForm290PAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedForm290(status).ToList();


            //fha number process
            if (pFilterType == "FHANumberComplete19")
            {
                olistAssignedFHAPAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedFHARequests(status).ToList();
                olistAssignedFHAPAMReportV3ViewModel = olistAssignedFHAPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
                outresults = olistAssignedFHAPAMReportV3ViewModel.ToList();
            }
            if (pFilterType == "FHANumberInProcess18")
            {
                olistAssignedFHAPAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedFHARequests(status).ToList();
                //outresults = olistAssignedFHAPAMReportV3ViewModel.Where(m => m.FhaNumber != null).GroupBy(item => item.FhaNumber).Select(grouping => grouping.FirstOrDefault()).ToList();
                outresults = olistAssignedFHAPAMReportV3ViewModel.ToList();
            }
            if (pFilterType == "FHANumberInQueue")
            {
                olistAssignedFHAPAMReportV3ViewModel = ControllerHelper.ProdPamGetUnAssignedFHARequests().ToList();
                outresults = olistAssignedFHAPAMReportV3ViewModel.ToList();
            }

            //application process
            if (pFilterType == "AwaitingLenderApplicationSubmission19")//use FHANumberComplete19
            {
                int inprocessStatus = status - 1;
                List<string> fhaNums = new List<string>();
                var olistTotalFHACompleted = ControllerHelper.ProdPamGetAssignedFHARequests(status).ToList();

                var olistAppInQ = ControllerHelper.ProdPamGetUnAssignedAPTasks().ToList();
                var olistAppInProcess = ControllerHelper.ProdPamGetAssignedAPTasks(inprocessStatus).ToList();
                var olistAppIssued = ControllerHelper.ProdPamGetAssignedAPTasks(status).ToList();

                foreach (var item in olistAppInQ)
                {
                    fhaNums.Add(item.FhaNumber);
                }
                foreach (var item in olistAppInProcess)
                {
                    fhaNums.Add(item.FhaNumber);
                }
                foreach (var item in olistAppIssued)
                {
                    fhaNums.Add(item.FhaNumber);
                }

                foreach (var fhanum in fhaNums)
                {
                    PAMReportV3ViewModel objPAMReportV3ViewModel = olistTotalFHACompleted.Where(o => o.FhaNumber == fhanum).FirstOrDefault();
                    olistTotalFHACompleted.Remove(objPAMReportV3ViewModel);
                }

                //outresults = olistTotalFHACompleted.Where(m => m.FhaNumber != null).GroupBy(item => item.FhaNumber).Select(grouping => grouping.FirstOrDefault()).ToList();
                olistTotalFHACompleted = olistTotalFHACompleted.Where(o => o.FhaNumber != null).ToList();
                outresults = olistTotalFHACompleted.ToList();

            }
            if (pFilterType == "ApplicationInQueue")
            {
                olistAssignedAPTasksPAMReportV3ViewModel = ControllerHelper.ProdPamGetUnAssignedAPTasks().ToList();
                //if underwriter in process from AP Q list, remove and add to AP inprocess list
                IList<PAMReportV3ViewModel> olistUwInProcessFromUnassignedAP = new List<PAMReportV3ViewModel>();
                olistUwInProcessFromUnassignedAP = ControllerHelper.GetUwInProcessFromUnassignedAP(olistAssignedAPTasksPAMReportV3ViewModel);

                foreach (var obj in olistUwInProcessFromUnassignedAP)
                {
                    olistAssignedAPTasksPAMReportV3ViewModel.Remove(obj);
                }
                olistAssignedAPTasksPAMReportV3ViewModel = olistAssignedAPTasksPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
                outresults = olistAssignedAPTasksPAMReportV3ViewModel.ToList();
            }

            if (pFilterType == "ApplicationFirmIssued19")
            {
                olistAssignedAPTasksPAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedAPTasks(status).ToList();
                olistAssignedAPTasksPAMReportV3ViewModel = olistAssignedAPTasksPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
                outresults = olistAssignedAPTasksPAMReportV3ViewModel.ToList();
            }
            if (pFilterType == "ApplicationInProcess18")
            {
                IList<PAMReportV3ViewModel> olistAssignedAPUnassigned = new List<PAMReportV3ViewModel>();
                olistAssignedAPTasksPAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedAPTasks(status).ToList();
                olistAssignedAPUnassigned = ControllerHelper.ProdPamGetUnAssignedAPTasks().ToList();

                //if underwriter in process from AP Q list, remove and add to AP inprocess list
                IList<PAMReportV3ViewModel> olistUwInProcessFromUnassignedAP = new List<PAMReportV3ViewModel>();
                olistUwInProcessFromUnassignedAP = ControllerHelper.GetUwInProcessFromUnassignedAP(olistAssignedAPUnassigned);

                foreach (var obj in olistUwInProcessFromUnassignedAP)
                {
                    olistAssignedAPTasksPAMReportV3ViewModel.Add(obj);
                }
                olistAssignedAPTasksPAMReportV3ViewModel = olistAssignedAPTasksPAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
                outresults = olistAssignedAPTasksPAMReportV3ViewModel.ToList();
            }

            if (pFilterType == "290 Assigned")
            {
                //olistAssignedForm290PAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedForm290((int)TaskStep.InProcess).Where(i => i.Status == "In Process")
                //												.GroupBy(item => item.FhaNumber)
                //												.Select(grouping => grouping.FirstOrDefault()).ToList();

                olistAssignedForm290PAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedForm290((int)TaskStep.InProcess).GroupBy(item => item.FhaNumber)
                                                                               .Select(grouping => grouping.FirstOrDefault()).ToList();
                olistAssignedForm290PAMReportV3ViewModel = olistAssignedForm290PAMReportV3ViewModel.Where(i => i.Status == "In Process" || i.Status == "Form290 In Process").ToList();
                olistAssignedForm290PAMReportV3ViewModel = olistAssignedForm290PAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
                outresults = olistAssignedForm290PAMReportV3ViewModel.ToList();
            }
            if (pFilterType == "290 Complete")
            {
                //olistAssignedForm290PAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedForm290((int)TaskStep.Complete).ToList();
                olistAssignedForm290PAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedForm290((int)TaskStep.Complete).GroupBy(item => item.FhaNumber)
                                                                               .Select(grouping => grouping.FirstOrDefault()).ToList();
                olistAssignedForm290PAMReportV3ViewModel = olistAssignedForm290PAMReportV3ViewModel.Where(i => i.Status == "Form290 Request Completed").ToList();
                olistAssignedForm290PAMReportV3ViewModel = olistAssignedForm290PAMReportV3ViewModel.Where(o => o.FhaNumber != null).ToList();
                outresults = olistAssignedForm290PAMReportV3ViewModel.ToList();
            }


            if (pFilterType == "Closing - Executed Documents Uploaded")
                outresults = ControllerHelper.ProdPamGetExecutedDocs().ToList();


            //Initial Closing Process (non construction draft closing)
            if (pFilterType == "AwaitingLenderClosingSubmission19") //todo
            {
                //statusGridViewModel.TotalWaitingClosing = statusGridViewModel.TotalApplicationComplete - statusGridViewModel.TotalClosingInprocess - statusGridViewModel.TotalClosingComplete - statusGridViewModel.TotalClosingInqueue;
                int inprocessStatus = status - 1;
                List<string> fhaNums = new List<string>();

                var olistTotalAPCompleted = ControllerHelper.ProdPamGetAssignedAPTasks(status).ToList();

                var olistNCInQ = ControllerHelper.ProdPamGetUnAssignedNCDraftClosingTasks().ToList();
                var olistNCInProcess = ControllerHelper.ProdPamGetAssignedNCDraftClosingTasks(inprocessStatus).ToList();
                var olistNCIssued = ControllerHelper.ProdPamGetAssignedNCDraftClosingTasks(status).ToList();


                foreach (var item in olistNCInQ)
                {
                    fhaNums.Add(item.FhaNumber);
                }
                foreach (var item in olistNCInProcess)
                {
                    fhaNums.Add(item.FhaNumber);
                }
                foreach (var item in olistNCIssued)
                {
                    fhaNums.Add(item.FhaNumber);
                }

                foreach (var fhanum in fhaNums)
                {
                    PAMReportV3ViewModel objPAMReportV3ViewModel = olistTotalAPCompleted.Where(o => o.FhaNumber == fhanum).FirstOrDefault();
                    olistTotalAPCompleted.Remove(objPAMReportV3ViewModel);
                }
                outresults = olistTotalAPCompleted.ToList();
                //outresults = ControllerHelper.ProdPamGetAssignedNCDraftClosingTasks((int)TaskStep.Complete).ToList();
            }
            if (pFilterType == "ClosingInQueue")
            {
                olistAssignedNCTasksPAMReportV3ViewModel = ControllerHelper.ProdPamGetUnAssignedNCDraftClosingTasks().ToList();
                //if underwriter in process from NC Q list, remove and add to NC inprocess list
                IList<PAMReportV3ViewModel> olistUwInProcessFromUnassignedNC = new List<PAMReportV3ViewModel>();
                olistUwInProcessFromUnassignedNC = ControllerHelper.GetUwInProcessFromUnassignedNC(olistAssignedNCTasksPAMReportV3ViewModel);

                foreach (var obj in olistUwInProcessFromUnassignedNC)
                {
                    olistAssignedNCTasksPAMReportV3ViewModel.Remove(obj);
                }
                outresults = olistAssignedNCTasksPAMReportV3ViewModel.ToList();
                //outresults = ControllerHelper.ProdPamGetUnAssignedNCDraftClosingTasks().ToList();
            }
            if (pFilterType == "ClosingInProcess18")
            {
                IList<PAMReportV3ViewModel> olistAssignedNCUnassigned = new List<PAMReportV3ViewModel>();
                olistAssignedNCTasksPAMReportV3ViewModel = ControllerHelper.ProdPamGetAssignedNCDraftClosingTasks(status).ToList();
                olistAssignedNCUnassigned = ControllerHelper.ProdPamGetUnAssignedNCDraftClosingTasks().ToList();

                //if underwriter in process from NC Q list,  add to NC inprocess list
                IList<PAMReportV3ViewModel> olistUwInProcessFromUnassignedNC = new List<PAMReportV3ViewModel>();
                olistUwInProcessFromUnassignedNC = ControllerHelper.GetUwInProcessFromUnassignedNC(olistAssignedNCUnassigned);

                foreach (var obj in olistUwInProcessFromUnassignedNC)
                {
                    olistAssignedNCTasksPAMReportV3ViewModel.Add(obj);
                }
                outresults = olistAssignedNCTasksPAMReportV3ViewModel.ToList();
                //outresults = ControllerHelper.ProdPamGetAssignedNCDraftClosingTasks((int)TaskStep.InProcess).ToList();
            }

            if (pFilterType == "ClosingComplete19")
                outresults = ControllerHelper.ProdPamGetAssignedNCDraftClosingTasks((int)TaskStep.Complete).ToList();

            //changes before.
            if (pFilterType == "Purchase/Refinance 223(f)" || pFilterType == "Refinance 223(a)(7)" ||
                pFilterType == "OLL 223(d)" || pFilterType == "Fire Safety 223(i)" || pFilterType == "Construction 241(a)" ||
                pFilterType == "Construction NC" || pFilterType == "Construction SR" || pFilterType == "IRR")
            {
                outresults = outresults.Where(x => x.LoanType == pFilterType).ToList();
                outresults.ToList().ForEach(y => y.ProjectName = $"{y.ProjectName}-{y.LoanType}");
            }
            if (pFilterType == "LoansOpened")
            {
                outresults = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Open").ToList();
                outresults.ToList().ForEach(y => y.ProjectName = $"{y.ProjectName}-{y.LoanType}");
            }
            if (pFilterType == "LoansCompleted")
            {
                outresults = outresults.Where(x => x.LoanType != null).Where(x => x.Status == "Complete").ToList();
                outresults.ToList().ForEach(y => y.ProjectName = $"{y.ProjectName}-{y.LoanType}");
            }
            if (pFilterType == "LoansFY")
            {
                outresults = outresults.Where(x => x.ProjectStartDate >= fisStart && x.ProjectStartDate <= fisEnd).ToList();
                outresults.ToList().ForEach(y => y.ProjectName = $"{y.ProjectName}-{y.LoanType}");
            }
            if (pFilterType == "TotLoans")
                outresults.ToList().ForEach(y => y.ProjectName = $"{y.ProjectName}-{y.LoanType}");

            //Technical review			
            if (pFilterType == "Appraiser Assigned")
            {
                outresults = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerAppraiserStatus != "N/A" && x.ReviewerAppraiserStatus != "Unassigned" && x.ReviewerAppraiserStatus != null).ToList();
                outresults.ToList().ForEach(y => y.ProjectName = $"{y.ProjectName}-{y.LoanType}");
            }
            if (pFilterType == "Environmentalist Assigned")
            {
                outresults = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerEnvironmentalistStatus != "N/A" && x.ReviewerEnvironmentalistStatus != "Unassigned" && x.ReviewerEnvironmentalistStatus != null).ToList();
                outresults.ToList().ForEach(y => y.ProjectName = $"{y.ProjectName}-{y.LoanType}");
            }
            if (pFilterType == "Title and Survey Assigned")
            {
                outresults = outresults.Where(x => x.ProjectStatus == "In-Process" && x.ReviewerTitleSurveyStatus != "N/A" && x.ReviewerTitleSurveyStatus != null && x.ReviewerTitleSurveyStatus != "Unassigned").ToList();
                outresults.ToList().ForEach(y => y.ProjectName = $"{y.ProjectName}-{y.LoanType}");
            }

            //amendment
            if (pFilterType == "AmendmentInqueue")
                outresults = ControllerHelper.ProdPamGetUnAssignedAmendmentTasks().GroupBy(item => item.FhaNumber).Select(grouping => grouping.FirstOrDefault()).ToList();
            if (pFilterType == "AmendmentInProcess18")
                outresults = ControllerHelper.ProdPamGetAssignedAmendmentClosingTasks((int)TaskStep.InProcess).GroupBy(item => item.FhaNumber).Select(grouping => grouping.FirstOrDefault()).ToList();
            if (pFilterType == "AmendmentComplete19")
                outresults = ControllerHelper.ProdPamGetAssignedAmendmentClosingTasks((int)TaskStep.Complete).GroupBy(item => item.FhaNumber).Select(grouping => grouping.FirstOrDefault()).ToList();
            return PartialView("~/Views/ManagementReport/PAMReports/_ProdPamStatusPopup.cshtml", outresults);
        }

        public JsonResult GetPAMReportResultsV3(ProdPAMReportStatusGridViewModel model, string selectedFhaNumber = null, string selectedProjectNames = null, string selectedProjectStages = null, string selectedProjectAll = null)
        {
            //@*#701 karri:prodpamfil*@//added extra 2 params for project stage, first one when single, or multiple values selected, last one for ALL type
            List<string> ProjTypes = new List<string>();
            if (selectedProjectAll != null && !selectedProjectAll.Equals("All") && selectedProjectStages != null)
            {
                String[] spearator = { "," };
                String[] strlist = selectedProjectStages.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                foreach (string var in strlist)
                {
                    ProjTypes.Add(var);
                }
                //ProjTypes.Add("Application");
            }
            string isAllowed = Request.Params["LoanType"];
            //var outresults = new List<FHANumberRequestViewModel>(){
            //    new FHANumberRequestViewModel(){ FHANumber ="123-45678" },
            //    new FHANumberRequestViewModel(){ FHANumber ="345-67890" }
            //};
            //var SavedFilter = _plmProdReportMgr.GetSavedFilter();
            if (selectedFhaNumber != null)
            {
                model.hudPamReportFilters.FHANumber = selectedFhaNumber;
            }
            if (selectedProjectNames != null)
            {
                model.hudPamReportFilters.ProjectName = selectedProjectNames;
            }
            if (model.hudPamReportFilters == null)
            {
                model.hudPamReportFilters = _plmProdReportMgr.GetSavedFilter();

            }
            var outresults = _plmProdReportMgr.GetProdHUDPAMReportResults();
            //var outresults = GetPAMRepository();
            //outresults = outresults.Where(o => o.FHARequestStatus != (int)RequestStatus.Draft).ToList();



            if (model.hudPamReportFilters != null)
            {
                //use for popups
                //ProdPAMReportRefinance223a7ViewModel objProdPAMReportRefinance223a7ViewModel = new ProdPAMReportRefinance223a7ViewModel();
                outresults = ControllerHelper.FilterProdPAMResults(model.hudPamReportFilters, outresults, ProjTypes);
                var routeValues =
                SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(
                    SessionHelper.SESSION_KEY_PRODPAM_FILTERS);
                var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
                {
                    {"model", model.hudPamReportFilters}
                };
                Session[SessionHelper.SESSION_KEY_PRODPAM_FILTERS] = routeValuesDict;
            }

            outresults.Where(w => w.ReviewerAppraiserStatus == null).ToList().ForEach(y => y.ReviewerAppraiserStatus = $"N/A");
            outresults.Where(w => w.ReviewerEnvironmentalistStatus == null).ToList().ForEach(y => y.ReviewerEnvironmentalistStatus = $"N/A");
            outresults.Where(w => w.ReviewerTitleSurveyStatus == null).ToList().ForEach(y => y.ReviewerTitleSurveyStatus = $"N/A");
            var jsonData = new
            {
                total = outresults.Count,
                page = 1,
                records = outresults.Count,
                // rows = outresults,
                //#701 Added by siddu//
                rows = outresults.OrderByDescending(x => x.ProjectStartDate).ToList(),


            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPAMGridOneResults(string TaskInstanceId)
        {
            var outresults = new List<PAMReportTestGrid1ViewModel>();

            if (TaskInstanceId != null)
            {
                outresults.Add(_plmProdReportMgr.GetProdPAMGridOneResults(new Guid(TaskInstanceId)));
            }

            var jsonData = new
            {
                total = 1,
                page = 1,
                records = 1,
                rows = outresults,

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPAMGridTwoResults(string TaskInstanceId)
        {
            //var outresults = new List<PAMReportTestGrid2ViewModel>(){
            //    new PAMReportTestGrid2ViewModel() {
            //        ProjectStage = "Closing",
            //        ProjectStatus = "Unassigned",
            //        AppraiserStatus = "In-Process",
            //        EnvironmentalistStatus = "Completed",
            //        TitleSurveyStatus = "Unassigned",
            //        IsContractorAssigned = true
            //    }
            //};

            var outresults = new List<PAMReportTestGrid2ViewModel>();

            if (TaskInstanceId != null)
            {
                outresults.Add(_plmProdReportMgr.GetProdPAMGridTwoResults(new Guid(TaskInstanceId)));
            }

            var jsonData = new
            {
                total = 1,
                page = 1,
                records = 1,
                rows = outresults,

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjectTypesByAppType(string value)
        {
            List<object> projectTypeObjs = new List<object>();
            var proejctTypes = _plmProdReportMgr.GetProjectTypesByAppType(value);
            projectTypeObjs = new List<object>(proejctTypes.Count);
            foreach (var m in proejctTypes)
            {
                projectTypeObjs.Add(new { Key = m.ProjectTypeId, Value = m.ProjectTypeName });
            }
            return Json(new { data = projectTypeObjs }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult SearchForRequests(string aeIds, string fromDate, string toDate, string status,
        //    string projectAction, int? page, string sort, string sortdir, string wlmIds)
        //{
        //    return PartialView("~/Views/ManagementReport/PAMReports/_PAMReport.cshtml", viewModel);
        //}

        public ActionResult HUDPamReportFilters(ProdPAMReportStatusGridViewModel test)
        {
            List<SelectListItem> selListItem = new List<SelectListItem>() { new SelectListItem { Text = "Test 1" } };
            HUDPamReportFiltersModel HUDPamReportFilters = new HUDPamReportFiltersModel();
            var outresults = _plmProdReportMgr.GetProdHUDPAMReportResults();

            //HUDPamReportFilters.AllFHANumber = outresults.GroupBy(x => x.FhaNumber).Select(i => new SelectListItem() { Text = i.First().FhaNumber.ToString() }).Distinct().ToList();
            var lstFHA = outresults.GroupBy(x => x.FhaNumber).Select(i => i.First().FhaNumber.ToString()).Distinct().ToList();
            test.TotalAvailableFHANumbersList = lstFHA;
            // test.TotalAvailableFHANumbersList = HUDPamReportFilters.AllFHANumber;
            var lstProjectNames = outresults.GroupBy(x => x.ProjectName).Select(i => i.First().ProjectName.ToString()).Distinct().ToList();
            test.TotalAvailableProjectNamesList = lstProjectNames;
            //HUDPamReportFilters.AllProjectName = outresults.GroupBy(x => x.ProjectName).Select(i => new SelectListItem() { Text = i.First().ProjectName.ToString() }).Distinct().ToList();
            HUDPamReportFilters.AllLoanType = outresults.GroupBy(x => x.LoanType).Select(i => new SelectListItem() { Text = i.First().LoanType.ToString() }).Distinct().ToList();
            HUDPamReportFilters.AllStatus = outresults.GroupBy(x => x.Status).Select(i => new SelectListItem() { Text = i.First().Status.ToString() }).Distinct().ToList();
            HUDPamReportFilters.AllFHANumberRequest = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString() }).Distinct().ToList();
            HUDPamReportFilters.AllCorporateCreditReview = selListItem;
            HUDPamReportFilters.AllPortfolioNumberAssignment = selListItem;
            //HUDPamReportFilters.AllMasterLeaseNumberAssignment = selListItem;
            HUDPamReportFilters.AllAppraiser = outresults.GroupBy(x => x.AppraiserStatus).Select(i => new SelectListItem() { Text = i.First().AppraiserStatus.ToString() }).Distinct().ToList();
            HUDPamReportFilters.AllUnderwriter = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString() }).Distinct().ToList();
            HUDPamReportFilters.AllEnvironmentalist = outresults.GroupBy(x => x.EnvironmentalistStatus).Select(i => new SelectListItem() { Text = i.First().EnvironmentalistStatus.ToString() }).Distinct().ToList();
            HUDPamReportFilters.AllTitle_Survey = outresults.GroupBy(x => x.TitleSurveyStatus).Select(i => new SelectListItem() { Text = i.First().TitleSurveyStatus.ToString() }).Distinct().ToList();
            HUDPamReportFilters.AllExecutedDocuments = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString() }).Distinct().ToList();
            HUDPamReportFilters.ToDate = null;
            HUDPamReportFilters.FromDate = null;
            test.hudPamReportFilters = HUDPamReportFilters;
            //HUDPamReportFilters = getValues();
            //AppProcessViewModel.IsIRRequest = false;
            //AppProcessViewModel.Isotherloantype = false;
            //InitializeViewModel(AppProcessViewModel, AppProcessViewModel.IsIRRequest);
            //AppProcessViewModel.AvailableFHANumbersList = prod_NextStageManager.GetFhasforNxtStage((int)PageType.ExecutedClosing, (int)UserPrincipal.Current.LenderId);
            //ViewBag.ConstructionTitle = "Non-Construction Executed Closing";
            //AppProcessViewModel.pageTypeId = (int)PageType.ExecutedClosing;
            //Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            //AppProcessViewModel.TransAccessStatus = checkResult.status;
            return View("~/Views/ManagementReport/PAMReports/HUDPamReportFilters.cshtml", test);
        }

        [HttpPost]
        public string SaveHUDPamReportFilterDetails(ProdPAMReportStatusGridViewModel model, string selectedFhaNumber = null, string selectedProjectNames = null, string selectedProjectStages = null)
        {
            var isInfoUpdate = "Fail";
            if (selectedFhaNumber != null)
            {
                model.hudPamReportFilters.FHANumber = selectedFhaNumber;
            }
            if (selectedProjectNames != null)
            {
                model.hudPamReportFilters.ProjectName = selectedProjectNames;
            }
            //#701 Added by siddu for project stage saving @030220//
            if (selectedProjectStages != null)
            {
                model.hudPamReportFilters.ProjectStage = selectedProjectStages;
            }
            try
            {
                isInfoUpdate = _plmProdReportMgr.SaveHUDPamFilters(model.hudPamReportFilters);
            }
            catch (Exception ex)
            {
                isInfoUpdate = "Fail";
            }
            return isInfoUpdate;
        }

        public HUDPamReportFiltersModel getValues(IList<PAMReportV3ViewModel> outresults)
        {
            HUDPamReportFiltersModel HUDPamReportFilters = new HUDPamReportFiltersModel();
            var SavedFilter = _plmProdReportMgr.GetSavedFilter();

            // NM -- old code var outresults = _plmProdReportMgr.GetProdHUDPAMReportResults();
            // NM -- start
            if (outresults == null)
            {
                outresults = _plmProdReportMgr.GetProdHUDPAMReportResults();
            }
            //outresults = outresults.Where(o => o.FHARequestStatus != (int)RequestStatus.Draft).ToList();

            List<SelectListItem> selListItem = new List<SelectListItem>() { new SelectListItem { Text = "Test 1" } };

            if (SavedFilter == null)
            {
                //karri#83. added sorting filter

                // HUDPamReportFilters.AllFHANumber = outresults.GroupBy(x => x.FhaNumber).Select(i => new SelectListItem() { Text = i.First().FhaNumber.ToString() }).Distinct().OrderBy(e => e.Text).ToList();
                var lstFHA = outresults.OrderBy(x => x.FhaNumber).GroupBy(x => x.FhaNumber).Select(i => i.First().FhaNumber.ToString()).Distinct().ToList();
                //hd.TotalAvailableFHANumbersList = lstFHA;
                HUDPamReportFilters.TotalAvailableFHANumbersListItem = lstFHA;
                var lstProjectNames = outresults.OrderBy(x => x.ProjectName).GroupBy(x => x.ProjectName).Select(i => i.First().ProjectName.ToString()).Distinct().ToList();
                HUDPamReportFilters.TotalAvailableProjectNamesListItem = lstProjectNames;
                //HUDPamReportFilters.AllProjectName = outresults.GroupBy(x => x.ProjectName).Select(i => new SelectListItem() { Text = i.First().ProjectName.ToString() }).Distinct().OrderBy(e => e.Text).ToList();

                HUDPamReportFilters.AllLoanType = outresults.GroupBy(x => x.LoanType).Select(i => new SelectListItem() { Text = i.First().LoanType.ToString() }).Distinct().ToList();
               // HUDPamReportFilters.AllStatus = outresults.GroupBy(x => x.Status).Select(i => new SelectListItem() { Text = i.First().Status.ToString() }).Distinct().ToList();
                HUDPamReportFilters.AllFHANumberRequest = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString() }).Distinct().ToList();
                HUDPamReportFilters.AllCorporateCreditReview = outresults.GroupBy(x => x.CreditReviewStatus).Select(i => new SelectListItem() { Text = i.First().CreditReviewStatus.ToString() }).Distinct().ToList();
                HUDPamReportFilters.AllPortfolioNumberAssignment = outresults.GroupBy(x => x.PortfolioStatus).Select(i => new SelectListItem() { Text = i.First().PortfolioStatus.ToString() }).Distinct().ToList();
                //HUDPamReportFilters.AllMasterLeaseNumberAssignment = selListItem;
                HUDPamReportFilters.AllAppraiser = outresults.GroupBy(x => x.AppraiserStatus).Select(i => new SelectListItem() { Text = i.First().AppraiserStatus.ToString() }).Distinct().ToList();
                HUDPamReportFilters.AllUnderwriter = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString() }).Distinct().ToList();
                HUDPamReportFilters.AllEnvironmentalist = outresults.GroupBy(x => x.EnvironmentalistStatus).Select(i => new SelectListItem() { Text = i.First().EnvironmentalistStatus.ToString() }).Distinct().ToList();
                HUDPamReportFilters.AllTitle_Survey = outresults.GroupBy(x => x.TitleSurveyStatus).Select(i => new SelectListItem() { Text = i.First().TitleSurveyStatus.ToString() }).Distinct().ToList();
                HUDPamReportFilters.AllExecutedDocuments = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString() }).Distinct().ToList();
                HUDPamReportFilters.AllProjectStage = outresults.GroupBy(x => x.ProjectStage).Select(i => new SelectListItem() { Text = i.First().ProjectStage }).Distinct().ToList();
                HUDPamReportFilters.AllProjectStage = HUDPamReportFilters.AllProjectStage.Where(o => o.Text != null).ToList();
                // #701 Added by siddu//
                HUDPamReportFilters.AllWLMGTM = outresults.GroupBy(x => x.WLMGTM).Select(i => new SelectListItem() { Text = i.First().WLMGTM }).Distinct().ToList();
                HUDPamReportFilters.Allcloser = outresults.GroupBy(x => x.closer).Select(i => new SelectListItem() { Text = i.First().closer }).Distinct().ToList();
            }
            else if (SavedFilter != null)
            {
                HUDPamReportFilters.FromDate = SavedFilter.FromDate;
                HUDPamReportFilters.ToDate = SavedFilter.ToDate;

                ////karri#83. added sorting filter
                //HUDPamReportFilters.AllFHANumber = outresults.GroupBy(x => x.FhaNumber).Select(i => new SelectListItem() { Text = i.First().FhaNumber.ToString(), Selected = (i.First().FhaNumber == SavedFilter.FHANumber) }).ToList();
                //HUDPamReportFilters.AllProjectName = outresults.GroupBy(x => x.ProjectName).Select(i => new SelectListItem() { Text = i.First().ProjectName.ToString(), Selected = (i.First().ProjectName == SavedFilter.ProjectName) }).Distinct().ToList();

                //karri#83:sorting project names and fha numbers
                //HUDPamReportFilters.AllFHANumber = outresults.GroupBy(x => x.FhaNumber).Select(i => new SelectListItem() { Text = i.First().FhaNumber.ToString(), Selected = (i.First().FhaNumber == SavedFilter.FHANumber) }).OrderBy(e => e.Text).ToList();
                var lstFHA = outresults.OrderBy(x => x.FhaNumber).GroupBy(x => x.FhaNumber).Select(i => i.First().FhaNumber.ToString()).Distinct().ToList();
                HUDPamReportFilters.TotalAvailableFHANumbersListItem = lstFHA;
                var lstProjectNames = outresults.OrderBy(x => x.ProjectName).GroupBy(x => x.ProjectName).Select(i => i.First().ProjectName.ToString()).Distinct().ToList();
                HUDPamReportFilters.TotalAvailableProjectNamesListItem = lstProjectNames;
                //HUDPamReportFilters.AllProjectName = outresults.GroupBy(x => x.ProjectName).Select(i => new SelectListItem() { Text = i.First().ProjectName.ToString(), Selected = (i.First().ProjectName == SavedFilter.ProjectName) }).Distinct().OrderBy(e => e.Text).ToList();

                HUDPamReportFilters.AllLoanType = outresults.GroupBy(x => x.LoanType).Select(i => new SelectListItem() { Text = i.First().LoanType.ToString(), Selected = (i.First().LoanType == SavedFilter.LoanType) }).Distinct().ToList();
               // HUDPamReportFilters.AllStatus = outresults.GroupBy(x => x.Status).Select(i => new SelectListItem() { Text = i.First().Status.ToString(), Selected = (i.First().Status == SavedFilter.Status) }).Distinct().ToList();
                HUDPamReportFilters.AllFHANumberRequest = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString(), Selected = (i.First().ProjectStatus == SavedFilter.FHANumberRequest) }).Distinct().ToList();
                HUDPamReportFilters.AllCorporateCreditReview = outresults.GroupBy(x => x.CreditReviewStatus).Select(i => new SelectListItem() { Text = i.First().CreditReviewStatus.ToString(), Selected = (i.First().CreditReviewStatus == SavedFilter.CorporateCreditReview) }).Distinct().ToList();
                HUDPamReportFilters.AllPortfolioNumberAssignment = outresults.GroupBy(x => x.PortfolioStatus).Select(i => new SelectListItem() { Text = i.First().PortfolioStatus.ToString(), Selected = (i.First().PortfolioStatus == SavedFilter.PortfolioNumberAssignment) }).Distinct().ToList();
                //HUDPamReportFilters.AllMasterLeaseNumberAssignment = selListItem;
                HUDPamReportFilters.AllAppraiser = outresults.GroupBy(x => x.AppraiserStatus).Select(i => new SelectListItem() { Text = i.First().AppraiserStatus.ToString(), Selected = (i.First().AppraiserStatus == SavedFilter.Appraiser) }).Distinct().ToList();
                HUDPamReportFilters.AllUnderwriter = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString(), Selected = (i.First().ProjectStatus == SavedFilter.Underwriter) }).Distinct().ToList();
                HUDPamReportFilters.AllEnvironmentalist = outresults.GroupBy(x => x.EnvironmentalistStatus).Select(i => new SelectListItem() { Text = i.First().EnvironmentalistStatus.ToString(), Selected = (i.First().EnvironmentalistStatus == SavedFilter.Environmentalist) }).Distinct().ToList();
                HUDPamReportFilters.AllTitle_Survey = outresults.GroupBy(x => x.TitleSurveyStatus).Select(i => new SelectListItem() { Text = i.First().TitleSurveyStatus.ToString(), Selected = (i.First().TitleSurveyStatus == SavedFilter.Title_Survey) }).Distinct().ToList();
                HUDPamReportFilters.AllExecutedDocuments = outresults.GroupBy(x => x.ProjectStatus).Select(i => new SelectListItem() { Text = i.First().ProjectStatus.ToString(), Selected = (i.First().ProjectStatus == SavedFilter.ExecutedDocuments) }).Distinct().ToList();
                HUDPamReportFilters.AllProjectStage = outresults.GroupBy(x => x.ProjectStage).Select(i => new SelectListItem() { Text = i.First().ProjectStage, Selected = (i.First().ProjectStage == SavedFilter.ProjectStage) }).Distinct().ToList();
                HUDPamReportFilters.AllProjectStage = HUDPamReportFilters.AllProjectStage.Where(o => o.Text != null).ToList();
                //#701 Added by siddu//
                HUDPamReportFilters.AllWLMGTM = outresults.GroupBy(x => x.WLMGTM).Select(i => new SelectListItem() { Text = i.First().WLMGTM }).Distinct().ToList();
                HUDPamReportFilters.Allcloser = outresults.GroupBy(x => x.closer).Select(i => new SelectListItem() { Text = i.First().closer }).Distinct().ToList();

            }
            return HUDPamReportFilters;
        }

        //karri,nagaraj;Note:this is not across the sessions, only per user
        private IList<PAMReportV3ViewModel> GetPAMRepository()
        {
            if (TempData["usp_HCP_Prod_GetProdHUDPAMReportV3"] != null)
                return (IList<PAMReportV3ViewModel>)TempData.Peek("usp_HCP_Prod_GetProdHUDPAMReportV3");

            //default/else
            var outresults_into_cache = _plmProdReportMgr.GetProdHUDPAMReportResults();
            TempData["usp_HCP_Prod_GetProdHUDPAMReportV3"] = outresults_into_cache;
            return outresults_into_cache;
        }

    }
}
