﻿using System;
using System.Web;
using System.Web.Mvc;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using Model;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Builder;

namespace HUDHealthcarePortal.Controllers.ManagementReport
{
    public class MissingProjectReportController : Controller
    {
        private IMissingProjectReportManager _missingProjectReportManager;

        public MissingProjectReportController()
            : this(new MissingProjectReportManager())
        {

        }

        public MissingProjectReportController(IMissingProjectReportManager missingProjectReportManager)
        {
            _missingProjectReportManager = missingProjectReportManager;
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult Index()
        {
            var userName = UserPrincipal.Current.UserName;
            ReportModel reportModel;
            //var year = DateTime.Now.Year;
            var year = DateTime.UtcNow.Year;

            //var reportyear = EnumType.GetEnumFromDescription(year.ToString(), typeof (ReportYear));

            //var enumval = EnumType.EnumToValue(EnumType.Parse<ReportingPeriod>(reportyear.ToString()));
            ISiteMapNode parentNode = null;
            if (SiteMaps.Current.CurrentNode != null) parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_MISSING_PROJECT_LINK_DICT);
            if (parentNode != null && !parentNode.RouteValues.Keys.Contains("reportYear") && routeValues != null)
            {
                ReportYear yearVal = (ReportYear) routeValues["reportYear"];
                year = int.Parse(EnumType.GetEnumDescription(yearVal));
            }
            if (RoleManager.IsSuperUserRole(userName))
            {
                ViewBag.ExcelExportAction = "ExportMissingProjectSuperUser";
                ViewBag.PrintAction = "PrintMissingProjectSuperUser";
                ViewBag.ExcelActionParam = new {year};
                reportModel = _missingProjectReportManager.GetMissingProjectReport(HUDRole.SuperUser, userName, year);
                return View("~/Views/ManagementReport/MissingProject/SuperUserReport.cshtml", reportModel);
            }
            if (RoleManager.IsWorkloadManagerRole(userName))
            {
                ViewBag.ExcelExportAction = "ExportMissingProjectWLM";
                ViewBag.PrintAction = "PrintMissingProjectWLM";
                ViewBag.ExcelActionParam = new { year,userName };
                reportModel = _missingProjectReportManager.GetMissingProjectReport(HUDRole.WorkflowManager, userName, year);
                return View("~/Views/ManagementReport/MissingProject/WLMReport.cshtml", reportModel);
            }
            if (RoleManager.IsAccountExecutiveRole(userName))
            {
                ViewBag.ExcelExportAction = "ExportMissingProjectAE";
                ViewBag.PrintAction = "PrintMissingProjectAE";
                ViewBag.ExcelActionParam = new { year,userName };
                reportModel = _missingProjectReportManager.GetMissingProjectReport(HUDRole.AccountExecutive, userName, year);
                return View("~/Views/ManagementReport/MissingProject/AEReport.cshtml", reportModel);
            }
            
            return new EmptyResult();
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult GetSuperUserMissingProjectForYear(int year)
        {
            ViewBag.ExcelExportAction = "ExportMissingProjectSuperUser";
            ViewBag.PrintAction = "PrintMissingProjectSuperUser";
            ViewBag.ExcelActionParam = new { year };
            var userName = UserPrincipal.Current.UserName;
            
            var reportModel = _missingProjectReportManager.GetMissingProjectReport(HUDRole.SuperUser, userName, year);
            return PartialView("~/Views/ManagementReport/MissingProject/_SuperUserReport.cshtml", reportModel);
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult GetSuperUserWLMMissingProjectForYear(int year)
        {
            ViewBag.ExcelExportAction = "ExportMissingProjectSuperUserWLM";
            ViewBag.PrintAction = "PrintMissingProjectSuperUserWLM";
            ViewBag.ExcelActionParam = new { year };
            var userName = UserPrincipal.Current.UserName;
            
            var reportModel = _missingProjectReportManager.GetSuperUserMissingProjectReportWithWLM(userName, year);
            return PartialView("~/Views/ManagementReport/MissingProject/_SuperUserReport_WLM.cshtml", reportModel);
           
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult GetWLMMissingProjectReportForYear(int year, string userName)
        {
            ViewBag.ExcelExportAction = "ExportMissingProjectWLM";
            ViewBag.PrintAction = "PrintMissingProjectWLM";
            ViewBag.ExcelActionParam = new { year, userName };
            var reportModel = _missingProjectReportManager.GetWorkloadManagerMissingProjectReport(userName, year);
            return PartialView("~/Views/ManagementReport/MissingProject/_WLMReport.cshtml", reportModel);
           
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult GetAEMissingProjectReportForYear(int year, string userName)
        {
            ViewBag.ExcelExportAction = "ExportMissingProjectAE";
            ViewBag.PrintAction = "PrintMissingProjectAE";
            ViewBag.ExcelActionParam = new { year,userName };
            var reportModel = _missingProjectReportManager.GetAccountExecutiveMissingProjectReport(userName, year);
                return PartialView("~/Views/ManagementReport/MissingProject/_AEReport.cshtml", reportModel);
        }

       [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        [MvcSiteMapNode(Title = "Missing Project Report with WLM", PreservedRouteParameters = "reportYear", ParentKey = "MissingProjectReportId", Key = "SuperUserWLMMissingProjectReportId")]
        public ActionResult SuperUserWLMMissingProjectReportDetail(ReportYear reportYear)
       {
            ViewBag.ExcelExportAction = "ExportMissingProjectSuperUserWLM";
            ViewBag.PrintAction = "PrintMissingProjectSuperUserWLM";
            
            var year = EnumType.GetEnumDescription(reportYear);
            ViewBag.ExcelActionParam = new { year };
            var reportModel = _missingProjectReportManager.GetSuperUserMissingProjectReportWithWLM(UserPrincipal.Current.UserName,int.Parse(year));
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"reportYear", reportYear}
            };
            Session[SessionHelper.SESSION_KEY_MISSING_PROJECT_LINK_DICT] = routeValuesDict;
            return View("~/Views/ManagementReport/MissingProject/SuperUserReport_WLM.cshtml", reportModel);

        }

        [HttpGet]
       //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        [MvcSiteMapNode(Title = "WLM Missing Project Report", PreservedRouteParameters = "wlmId,reportYear", ParentKey = "SuperUserWLMMissingProjectReportId", Key = "WLMMissingProjectReportId")]
       public ActionResult WLMMissingProjectReportDetail(string wlmId, ReportYear reportYear)
        {
            ViewBag.ExcelExportAction = "ExportMissingProjectWLM";
            ViewBag.PrintAction = "PrintMissingProjectWLM";
            var year = EnumType.GetEnumDescription(reportYear);
            var userName = wlmId;
            ViewBag.ExcelActionParam = new { year,userName };
            ControllerHelper.PopulateParentNodeRouteValuesForMissingProject();
            var reportModel = _missingProjectReportManager.GetWorkloadManagerMissingProjectReport(wlmId,
                int.Parse(year));
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"wlmId", wlmId},
                {"reportYear", reportYear}
            };
            Session[SessionHelper.SESSION_KEY_MISSING_PROJECT_LINK_DICT] = routeValuesDict;
            return View("~/Views/ManagementReport/MissingProject/WLMReport.cshtml", reportModel);
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        [MvcSiteMapNode(Title = "AE Missing Project Report", PreservedRouteParameters = "aeId,reportYear", ParentKey = "WLMMissingProjectReportId", Key = "AEMissingProjectReportId")]
        public ActionResult AEMissingProjectReportDetail(string aeId, ReportYear reportYear)
        {
            return MissingProjectReportForAE(aeId, reportYear);
        }

        private ActionResult MissingProjectReportForAE(string aeId, ReportYear reportYear)
        {
            ViewBag.ExcelExportAction = "ExportMissingProjectAE";
            ViewBag.PrintAction = "PrintMissingProjectAE";
            var year = EnumType.GetEnumDescription(reportYear);
            var userName = aeId;
            ViewBag.ExcelActionParam = new { year, userName };
            ControllerHelper.PopulateParentNodeRouteValuesForMissingProject();
            var reportModel = _missingProjectReportManager.GetAccountExecutiveMissingProjectReport(aeId, int.Parse(year));
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"aeId", aeId},
                {"reportYear", reportYear}
            };
            Session[SessionHelper.SESSION_KEY_MISSING_PROJECT_LINK_DICT] = routeValuesDict;
            return View("~/Views/ManagementReport/MissingProject/AEReport.cshtml", reportModel);
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        [MvcSiteMapNode(Title = "AE Missing Project Report", PreservedRouteParameters = "aeId,reportYear", ParentKey = "MissingProjectReportId", Key = "MissingProjectReportIdForAE")]
        public ActionResult MissingProjectReportDetailForAE(string aeId, ReportYear reportYear)
        {
            return MissingProjectReportForAE(aeId, reportYear);
        }
    }
}
