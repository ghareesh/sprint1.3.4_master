﻿using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using BusinessService.Interfaces;
using Core;
using Elmah;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Common;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Filters;
using SpreadsheetGear;
using HUDHealthcarePortal;
using Core.ExceptionHandling.Types;
using HUDHealthcarePortal.Core.ExceptionHandling;
using System.Diagnostics;
using EntityObject.Entities.HCP_live;

namespace HUDHealthCarePortal.Controllers
{
    [Authorize]
    public class ExcelUploadController : Controller
    {
        public static string fhaExistngMessage1 = "The following FHA number(s) with specified period ending date already exist: ";
        public static string fhaExistngMessage2 = "<br/><br/> If you select Continue, the existing value(s) will be overwritten. Do you still want to continue?";
        private readonly bool _isAeEmailAlertEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsAEEmailAlertEnabled"]);
        private IUploadDataManager _uploadDataManager;
        private IEmailManager _emailManager;
        private IBackgroundJobMgr _backgroundJobManager;
        private static object syncRoot = new object();
        public bool agreed = false;
        private BizRuleManager bizRuleMgr = new BizRuleManager();
        public string Querydate = "";
        // userid to rows copied dictionary
        private static IDictionary<int, long> ProcessStatus { get; set; }

        public ExcelUploadController()
            : this(new UploadDataManager(), new EmailManager(), new BackgroundJobMgr())
        {

        }

        public ExcelUploadController(IUploadDataManager uploadDataManager, IEmailManager emailManager,
            IBackgroundJobMgr backgroundMgr)
        {
            _uploadDataManager = uploadDataManager;
            _emailManager = emailManager;
            _backgroundJobManager = backgroundMgr;
            if (ProcessStatus == null)
            {
                ProcessStatus = new Dictionary<int, long>();
            }
            Querydate = ConfigurationManager.AppSettings["Querydate"].ToString();
            if (string.IsNullOrEmpty(Querydate)) Querydate = "5/19/2016";
        }

        /// <summary>
        /// Shows the agreement.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AccessDeniedAuthorize(
            Roles =
                "BackupAccountManager,HUDAdmin,AccountExecutive,Attorney,HUDDirector,WorkflowManager,LenderAccountManager,LenderAccountRepresentative,Administrator,SuperUser"
            )]
        public ActionResult Batch_File_Upload_Template()
        {
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "Batch_File_Upload_Template.xlsx");
            Response.WriteFile(Server.MapPath("../Content/AppFiles/Batch_File_Upload_Template.xlsx"));
            Response.End();
            return View();
        }

        [HttpGet]
        [AccessDeniedAuthorize(
            Roles =
                "BackupAccountManager,HUDAdmin,AccountExecutive,Attorney,HUDDirector,WorkflowManager,LenderAccountManager,LenderAccountRepresentative,Administrator,SuperUser,Servicer"
            )]
        public ActionResult ExcelUploadView()
        {
            PopupHelper.ConfigPopup(TempData, 720, 350, "Disclaimer", false, false, true, false, false, true,
                "../ExcelUpload/PreUploadConfirm");
            if (TempData["PasswordExpires"] != null && (bool)TempData["PasswordExpires"])
                PopupHelper.ConfigPopup(TempData, 640, 340, "Change Password", false, false, true, false, false, true,
                    "../Account/ChangePassword");
            return View();
        }

        /// <summary>
        /// to make breadcrumb display properly on second level
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (RoleManager.IsCurrentOperatorAccountRepresentative())
                return RedirectToAction("Index", "FormUpload");
            return RedirectToAction("ExcelUploadView");
        }

        [HttpPost]
        public ActionResult PreUploadConfirm()
        {
            return PartialView("PreUploadConfirm");
        }

        [HttpPost]
        public ActionResult Importexcel()
        {
            OleDbConnection excelConnection = null;
            SqlConnection sqlIntermediateDBConnection = null;
            Session["Confirmation"] = null;
            if (Request.Files["FileUpload1"].ContentLength <= 0)
            {
                Session["Confirmation"] = "You must attach a file to upload, then click the UPLOAD button.";
                return RedirectToAction("ExcelUploadView", "ExcelUpload");
            }
            string extension = Path.GetExtension(Request.Files["FileUpload1"].FileName);
            if (extension != ".xlsx" && extension != ".xls")
            {
                Session["Confirmation"] = "The file is not in the expected format. Please make sure the file you are uploading is in excel format (.xls or .xlsx).";
                return RedirectToAction("ExcelUploadView", "ExcelUpload");
            }

            try
            {
                lock (syncRoot)
                {
                    string sPathUploadingFileLocation = Request.Files["FileUpload1"].FileName;
                    string sFileName = Path.GetFileName(sPathUploadingFileLocation);
                    string sPathStoring = ConfigurationManager.AppSettings["UploadExcelBackup"];
                    //sPathStoring = sPathStoring + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" +
                    //    UserPrincipal.Current.UserId + "_" + sFileName;
                    sPathStoring = sPathStoring + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + "_" +
                       UserPrincipal.Current.UserId + "_" + sFileName;
                    if (!ProcessStatus.ContainsKey(UserPrincipal.Current.UserId))
                        ProcessStatus.Add(UserPrincipal.Current.UserId, 0);
                    else
                        ProcessStatus[UserPrincipal.Current.UserId] = 0;
                    long iLenderID = 0;
                    int iUserID;
                    if (System.IO.File.Exists(sPathStoring)) System.IO.File.Delete(sPathStoring);
                    Request.Files["FileUpload1"].SaveAs(sPathStoring);
                    string sqlConnectionString = ConfigurationManager.ConnectionStrings[WebUiConstants.IntermediateConnectionName]
                            .ConnectionString;
                    sqlIntermediateDBConnection = new SqlConnection(sqlConnectionString);
                    string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sPathStoring +
                        ";Extended Properties=Excel 12.0;Persist Security Info=True";

                    //Create Connection to Excel work book
                    excelConnection = new OleDbConnection(excelConnectionString);
                    string templateWorksheetName = "Batch File Upload Template";
                    excelConnection.Open();
                    DataTable dbSchema = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dbSchema == null || dbSchema.Rows.Count < 1)
                    {
                        Session["Confirmation"] = "Error: Could not find data in the spreadsheet.  Please make sure the file you are using is from the template at the link to Batch File Upload Template.";
                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                    }
                    string compareWorksheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();
                    //tool will add single quotes if the sheet name has spaces.
                    string singlequote = templateWorksheetName.IndexOf(" ", 0) >= 0 ? "'" : "";
                    var myRowsList = dbSchema.Rows;
                    if (!(from row in dbSchema.AsEnumerable()
                          select (row["TABLE_NAME"])).ToList().Contains(singlequote + templateWorksheetName + "$" + singlequote))
                    {
                        Session["Confirmation"] = "The file does not have the expected worksheet tab name: \"" +
                            templateWorksheetName + "\". Please verify with the template from the link on this page.";
                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                    }
                    //OleDbCommand cmd = new OleDbCommand(
                    //    "Select [FHA Number],[Servicer ID],[Period Ending],[Months In Period],[Total Operating Revenues],[Operating Expenses],[FHA Insured Principal & Interest Payment],[FHA Mortgage Insurance Premium (MIP)], [Actual Number of Resident Days] from [" + templateWorksheetName + "$]", excelConnection);

                    //karri;Hareesh;Naveen:#121;
                    OleDbCommand cmd = new OleDbCommand(
                        "Select * from [" + templateWorksheetName + "$]", excelConnection);

                    OleDbDataAdapter adp = new OleDbDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    DataTable dtWithData = new DataTable();
                    adp.SelectCommand = cmd;
                    adp.FillSchema(dtWithData, SchemaType.Source);
                    adp.Fill(dtWithData);

                    // validate fha number/Servicer ID pair, if conflict with db data, stop uploading with error message
                    LenderFhaCol lenderFhaPairs = new LenderFhaCol();
                    IList<string> uploadFhaNumberList = new List<string>();
                    int idx = 1;

                    foreach (DataRow row in dtWithData.Rows)
                    {
                        idx++;
                        string sLenderIDEmptyNull = Convert.ToString(row["Servicer ID"]);
                        if (string.IsNullOrEmpty(sLenderIDEmptyNull))
                        {
                            if (string.IsNullOrEmpty(Convert.ToString(row["FHA Number"])) &&
                                string.IsNullOrEmpty(Convert.ToString(row["Period Ending"])) &&
                                string.IsNullOrEmpty(Convert.ToString(row["Months In Period"])) &&
                                string.IsNullOrEmpty(Convert.ToString(row["Total Operating Revenues"])) &&
                                string.IsNullOrEmpty(Convert.ToString(row["Operating Expenses"])) &&
                                string.IsNullOrEmpty(Convert.ToString(row["FHA Insured Principal & Interest Payment"])) &&
                                string.IsNullOrEmpty(Convert.ToString(row["FHA                 Mortgage Insurance Premium (MIP)"])) &&
                                string.IsNullOrEmpty(Convert.ToString(row["Actual Number Of Resident days"])))
                            {
                                continue;
                            }
                            Session["Confirmation"] = _uploadDataManager.GetErrorMsgForEmptyFields(idx, "Servicer ID");
                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                        }
                        string sFHANumberEmptyNull = Convert.ToString(row["FHA Number"]);
                        //Added by siddu does not allow spaces between string @22/01/20 //
                        sFHANumberEmptyNull = sFHANumberEmptyNull.TrimEnd();
                        if (string.IsNullOrEmpty(sFHANumberEmptyNull))
                        {
                            Session["Confirmation"] = _uploadDataManager.GetErrorMsgForEmptyFields(idx, "FHA Number");
                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                        }
                        if (!(new Regex(@"\d{3}-\d{5}")).Match(sFHANumberEmptyNull).Success || (sFHANumberEmptyNull.Length != 9))
                        {
                            Session["Confirmation"] = _uploadDataManager.GetErrorMsgForInvalidFields(idx, "FHA Number", "###-#####");
                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                        }
                        int intoutput;
                        if (!int.TryParse(sLenderIDEmptyNull, out intoutput))
                        {
                            Session["Confirmation"] = _uploadDataManager.GetErrorMsgForInvalidFields(idx, "Servicer ID", "Number");
                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                        }
                        if (!_uploadDataManager.IsValidLender(intoutput))
                        {
                            Session["Confirmation"] = "Servicer ID is not valid at row# " + idx;
                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                        }
                        uploadFhaNumberList.Add(((string)row["FHA Number"]).Trim());
                        lenderFhaPairs.LenderFhaPairs.Add(new LenderFhaPairModel(int.Parse(row["Servicer ID"].ToString()),
                            (string)row["FHA Number"]));
                    }
                    List<string> violationFhas = new List<string>();
                    if (RoleManager.IsCurrentSpecialOptionUser())
                    {
                        var allowedFhaNumbers = _uploadDataManager.GetAllowedFhasForUser(UserPrincipal.Current.UserId).ToList();
                        foreach (var fhaNumber in uploadFhaNumberList)
                        {
                            if (!allowedFhaNumbers.Contains(fhaNumber))
                            {
                                violationFhas.Add(fhaNumber);
                            }
                        }
                        if (violationFhas.Any())
                        {
                            Session["Confirmation"] = string.Format("Following FHA Numbers cannot be serviced by you: {0}{1}",
                                Environment.NewLine, TextUtils.CommaDelimitedText(violationFhas));
                            return RedirectToAction("ExcelUploadView", "ExcelUpload");
                        }
                    }
                    string lenderFhaPairsXml = XmlHelper.Serialize(lenderFhaPairs, typeof(LenderFhaCol), Encoding.Unicode);
                    DataTable tblErrFhas = new DataTable();
                    string sConn_Live_db = ConfigurationManager.ConnectionStrings[WebUiConstants.LiveConnectionName].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(sConn_Live_db))
                    using (SqlCommand cmdVal = new SqlCommand("usp_HCP_Validate_LenderFhaPair", conn))
                    {
                        cmdVal.CommandType = CommandType.StoredProcedure;
                        cmdVal.Parameters.Add(new SqlParameter("@LenderFhaPairs", SqlDbType.Xml));
                        cmdVal.Parameters["@LenderFhaPairs"].Value = lenderFhaPairsXml;
                        SqlDataAdapter da = new SqlDataAdapter(cmdVal);
                        da.Fill(tblErrFhas);
                    }
                    if (tblErrFhas.Rows.Count > 0)
                    {
                        foreach (DataRow row in tblErrFhas.Rows)
                        {
                            violationFhas.Add(row[0].ToString());
                        }
                        Session["Confirmation"] = string.Format("Following FHA Numbers don't match corresponding Lenders: {0}{1}",
                            Environment.NewLine, TextUtils.CommaDelimitedText(violationFhas));
                        return RedirectToAction("ExcelUploadView", "ExcelUpload");
                    }

                    dtWithData.Columns.Add("UserID");
                    dtWithData.Columns.Add("ModifiedOn");
                    dtWithData.Columns.Add("ModifiedBy");
                    dtWithData.Columns.Add("IsLate");
                    string sMonthsinPeriodNull, sTotalRevenues, sTotalExpenses, sFHAInsuredPrincipalPayment,
                        sMortgageInsurancePremium, sActualNumberOfResidentDays;
                    dt = dtWithData.Clone();
                    string sFHANumberEmpty;
                    string sLenderIDEmpty;
                    int i = 1;
                    Double doubleOutputTest = 0.0;
                    int intOutputTest = 0;
                    string currentField = "";

                    foreach (DataRow dRowData in dtWithData.Rows)
                    {
                        i++;
                        sLenderIDEmpty = Convert.ToString(dRowData["Servicer ID"]);
                        if (string.IsNullOrEmpty(sLenderIDEmpty)) continue;
                        sFHANumberEmpty = Convert.ToString(dRowData["FHA Number"]);
                        if (!string.IsNullOrEmpty(sFHANumberEmpty) && (!string.IsNullOrEmpty(sLenderIDEmpty)))
                        {
                            //Validating Period Ending in right format
                            try
                            {
                                var dPeriodEnding = Convert.ToDateTime(dRowData["Period Ending"]);
                            }
                            catch (Exception)
                            {
                                Session["Confirmation"] = "Period Ending is not in correct format: MM/DD/YYYY or is not a valid date.";
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            currentField = "Months in Period";
                            sMonthsinPeriodNull = Convert.ToString(dRowData[currentField]);
                            if (string.IsNullOrEmpty(sMonthsinPeriodNull))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForEmptyFields(i, currentField);
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            if (!int.TryParse(sMonthsinPeriodNull, out intOutputTest))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            currentField = "Total Operating Revenues";
                            sTotalRevenues = Convert.ToString(dRowData[currentField]);
                            if (string.IsNullOrEmpty(sTotalRevenues))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForEmptyFields(i, currentField);
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            if (!Double.TryParse(sTotalRevenues, out doubleOutputTest))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            currentField = "Operating Expenses";
                            sTotalExpenses = Convert.ToString(dRowData[currentField]);
                            if (string.IsNullOrEmpty(sTotalExpenses))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForEmptyFields(i, currentField);
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            if (!Double.TryParse(sTotalExpenses, out doubleOutputTest))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            currentField = "FHA Insured Principal & Interest Payment";
                            sFHAInsuredPrincipalPayment = Convert.ToString(dRowData[currentField]);
                            if (string.IsNullOrEmpty(sFHAInsuredPrincipalPayment))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForEmptyFields(i, currentField);
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            if (!Double.TryParse(sFHAInsuredPrincipalPayment, out doubleOutputTest))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            currentField = "FHA                 Mortgage Insurance Premium (MIP)";
                            sMortgageInsurancePremium = Convert.ToString(dRowData[currentField]);
                            if (string.IsNullOrEmpty(sMortgageInsurancePremium))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForEmptyFields(i, currentField);
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            if (!Double.TryParse(sMortgageInsurancePremium, out doubleOutputTest))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }

                            // Actual Number Of Resident Day
                            currentField = "Actual Number Of Resident Days";
                            sActualNumberOfResidentDays = Convert.ToString(dRowData[currentField]).Trim();
                            if (string.IsNullOrEmpty(sActualNumberOfResidentDays))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForEmptyFields(i, currentField);
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            if (!Double.TryParse(sActualNumberOfResidentDays, out doubleOutputTest))
                            {
                                Session["Confirmation"] = _uploadDataManager.GetErrorMsgForInvalidFields(i, currentField, "numbers (0-9)");
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            dt.Rows.Add(Convert.ToString(dRowData["FHA Number"]),
                                Convert.ToString(dRowData["Servicer ID"]),
                                Convert.ToDateTime(dRowData["Period Ending"]),
                                Convert.ToString(dRowData["Months In Period"]),
                                sTotalRevenues,
                                sTotalExpenses,
                                sFHAInsuredPrincipalPayment,
                                sMortgageInsurancePremium,
                                sActualNumberOfResidentDays);
                        }
                        else
                        {
                            break;
                        }
                    }

                    //Error message to verify that Excel has data.
                    if (dt.Rows.Count > 0)
                    {
                        DateTime dPeriodEnding;
                        string sFHANumber, sPeriodEnding;
                        int iDuplicateFHANumber = 0, iMonthIn = 0, iRow = 0, iSpace;
                        DateTime dCurrentDate;
                        TimeSpan tDateDifference;
                        List<string> uploadItems = new List<string>();
                        // for Operator account rep, restrict fha's allowed to upload
                        if (RoleManager.IsCurrentOperatorAccountRepresentative())
                        {
                            var allowedFhas = _uploadDataManager.GetAllowedFhasForUser(UserPrincipal.Current.UserId).ToList();
                            List<string> fhasToUpload = new List<string>();
                            foreach (DataRow dRow in dt.Rows)
                            {
                                fhasToUpload.Add(Convert.ToString(dRow["FHA Number"]));
                            }
                            var violatingFhas = fhasToUpload.Where(p => !allowedFhas.Contains(p));
                            if (violatingFhas != null && violatingFhas.Count() > 0)
                            {
                                Session["Confirmation"] =
                                    string.Format("You are not allowed to upload FHA's: {0} that are not included in allowed FHA's: {1}.",
                                        TextUtils.CommaDelimitedText(violatingFhas), TextUtils.CommaDelimitedText(allowedFhas));
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                        }


                        List<Int64> lenderIds = new List<Int64>();

                        //karri:new implementation for Support#236
                        foreach (DataRow dRow in dt.Rows)
                        {
                            iRow++;
                            iLenderID = Convert.ToInt64(dRow["Servicer ID"]);
                            lenderIds.Add(iLenderID);
                            if (RoleManager.IsCurrentUserLenderRoles() && UserPrincipal.Current.LenderId != iLenderID)
                            {
                                Session["Confirmation"] = "You are not allowed to upload a file for another lender.";
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            sPeriodEnding = Convert.ToString(dRow["Period Ending"]);
                            dPeriodEnding = Convert.ToDateTime(sPeriodEnding);
                            iSpace = sPeriodEnding.IndexOf(" ");
                            if (iSpace > 0) sPeriodEnding = sPeriodEnding.Substring(0, iSpace);
                            //#OS-support ticket #2403 -#731 Added trimend to avoid spaces between number and special character ("")
                            sFHANumber = Convert.ToString(dRow["FHA Number"]).TrimEnd();
                            iMonthIn = Convert.ToInt32(dRow["Months In Period"]);
                            uploadItems.Add(sFHANumber + "_" + iLenderID + "_" + dPeriodEnding);
                            if (!(iMonthIn == 3 || iMonthIn == 6 || iMonthIn == 9 || iMonthIn == 12))
                            {
                                Session["Confirmation"] = "Incorrect Months In Period of (" + iMonthIn + ") for FHA#-{" +
                                    sFHANumber + "} at row-" + iRow;
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            //validating quarterly and year-end financial rules
                            bool isLateSubmit = false;
                            var uploadStatusFlag = bizRuleMgr.GetUploadStatusFlag(dRow["Months In Period"].ToString(),
                                dRow["Period Ending"].ToString());
                            switch (uploadStatusFlag)
                            {
                                case UploadStatusFlag.BadData:
                                    Session["Confirmation"] = "Wrong Period Ending or Months in Period.";
                                    return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                case UploadStatusFlag.Late:
                                    isLateSubmit = true;
                                    break;
                            }
                            //dCurrentDate = DateTime.Now;
                            dCurrentDate = DateTime.UtcNow;
                            tDateDifference = dCurrentDate - dPeriodEnding;
                            // cannot enter future date in period ending
                            if (tDateDifference < new TimeSpan(0, 0, 0))
                            {
                                Session["Confirmation"] = "Date must not be in the future.";
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            dRow["IsLate"] = isLateSubmit;
                            foreach (DataRow dRowFHANumber in dt.Rows)
                            {
                                if ((sFHANumber == Convert.ToString(dRowFHANumber["FHA Number"])) &&
                                    (dPeriodEnding == Convert.ToDateTime(dRowFHANumber["Period Ending"])) &&
                                    (iMonthIn == Convert.ToInt32(dRowFHANumber["Months In Period"])))
                                {
                                    iDuplicateFHANumber++;
                                }
                                //Duplicate rows exists
                                if ((iDuplicateFHANumber > 1))
                                {
                                    Session["Confirmation"] = "Excel upload file contains duplicate FHA Number of " + sFHANumber;
                                    return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                }
                            }
                            iDuplicateFHANumber = 0;
                            dRow["UserID"] = UserPrincipal.Current.UserId;
                            dRow["ModifiedOn"] = DateTime.UtcNow;
                            dRow["ModifiedBy"] = UserPrincipal.Current.UserId;
                            //#584 Harish
                            // harish added new 
                            // DateTime finalPeriodEndingperrecord = DateTime.Today;
                            //int monthrecord = dPeriodEnding.Month;
                            // harish added #584
                            var lenderrowdata = _uploadDataManager.lenderrowdata((int)iLenderID, sFHANumber);

                            int id = lenderrowdata == null ? 0 : lenderrowdata.Id;

                            int finanlMonthIn = 0;
                            DateTime finalPeriodEnding = DateTime.Today;
                            int dbFinalMonth = 12;
                            if (lenderrowdata == null)
                            {
                                finanlMonthIn = iMonthIn == 12 ? 0 : 12 - iMonthIn;
                                finalPeriodEnding = dPeriodEnding.AddMonths(finanlMonthIn);
                                var model = new LenderFiscalYearDetails();
                                model.FHANumber = sFHANumber;
                                model.LenderId = (int)iLenderID;
                                model.Q1 = 3;
                                model.Q2 = 6;
                                model.Q3 = 9;
                                model.Q4 = 12;
                                model.FiscalYearEndMonth = finalPeriodEnding.Month;
                                var fisicalyearinsert = _uploadDataManager.CreateLenderFisicalyear(model);
                                // insert


                                //finalPeriodEnding = dPeriodEnding.AddMonths(finanlMonthIn);
                            }
                            else
                            {
                                finanlMonthIn = iMonthIn == 12 ? 0 : 12 - iMonthIn;
                                finalPeriodEnding = dPeriodEnding.AddMonths(finanlMonthIn);
                                var model = new LenderFiscalYearDetails();
                                model.FHANumber = sFHANumber;
                                model.LenderId = (int)iLenderID;
                                model.Id = id;
                                model.Q1 = 3;
                                model.Q2 = 6;
                                model.Q3 = 9;
                                model.Q4 = 12;
                                model.FiscalYearEndMonth = finalPeriodEnding.Month;
                                var fisicalyearupdate = _uploadDataManager.UpdateLenderFisicalyear(model);

                                // update
                            }

                            //check for bad fiscal year 
                            bool existInSpreadSheet, existInIntermidiate;
                            Boolean isFYGood = _uploadDataManager.CheckLenderFiscalYear(iLenderID, sFHANumber, finalPeriodEnding, dbFinalMonth,
                                out existInSpreadSheet, out existInIntermidiate, Querydate);
                            if (!isFYGood)
                            {
                                Session["Confirmation"] = "Incorrect period ending of (" + sPeriodEnding +
                                    ") or Months in Period of (" + iMonthIn + ") for FHA#-(" + sFHANumber + ") at row-" + iRow;
                                return RedirectToAction("ExcelUploadView", "ExcelUpload");
                            }
                            //for 1st time upload, if not existing in spreadsheet and intermediate table, it has to be regular FY
                            if (!existInSpreadSheet && !existInIntermidiate)
                            {
                                if (dPeriodEnding.Month != iMonthIn)
                                {
                                    Session["Confirmation"] = "Invalid Period Ending & Months in Period Combination for FHA#-" + sFHANumber;
                                    return RedirectToAction("ExcelUploadView", "ExcelUpload");
                                }
                            }
                        }


                        string checkDuplicate = Request.Form["hdnCheckDuplicate"];
                        if (checkDuplicate == "true")
                        {
                            List<string> fhas = _uploadDataManager.GetExistingFHAs(uploadItems);
                            if (fhas != null && fhas.Count > 0)
                            {

                                // return Content(fhaExistngMessage1 + string.Join(", ", fhas) + fhaExistngMessage2);
                                // Session["Confirmation"] = "The following FHA number(s) with specified period ending date already exist: " + string.Join(", ", fhas) + "<br/><br/> If you select Continue, the existing value(s) will be overwritten. Do you still want to continue?";
                                return Content("The following FHA number(s) with specified period ending date already exist: " + string.Join(", ", fhas) + "<br/><br/> If you select Continue, the existing value(s) will be overwritten. Do you still want to continue?");
                            }

                        }

                        //Save data to Intermidiate table
                        sqlIntermediateDBConnection.Open();
                        using (SqlBulkCopy sqlBulk = new SqlBulkCopy(sqlIntermediateDBConnection))
                        {
                            // Set up the column mappings by ordinal.
                            SqlBulkCopyColumnMapping column0 = new SqlBulkCopyColumnMapping(0, 2);
                            sqlBulk.ColumnMappings.Add(column0);

                            //Servicer ID
                            SqlBulkCopyColumnMapping column1 = new SqlBulkCopyColumnMapping(1, 3);
                            sqlBulk.ColumnMappings.Add(column1);
                            //Period Ending
                            SqlBulkCopyColumnMapping column2 = new SqlBulkCopyColumnMapping(2, 5);
                            sqlBulk.ColumnMappings.Add(column2);

                            //Months In Period
                            SqlBulkCopyColumnMapping column3 = new SqlBulkCopyColumnMapping(3, 6);
                            sqlBulk.ColumnMappings.Add(column3);

                            //Total Revenues
                            SqlBulkCopyColumnMapping column4 = new SqlBulkCopyColumnMapping(4, 15);
                            sqlBulk.ColumnMappings.Add(column4);

                            // Total Expenses
                            SqlBulkCopyColumnMapping column5 = new SqlBulkCopyColumnMapping(5, 19);
                            sqlBulk.ColumnMappings.Add(column5);

                            //FHA Insured Principal & Interest Payment
                            SqlBulkCopyColumnMapping column6 = new SqlBulkCopyColumnMapping(6, 51);
                            sqlBulk.ColumnMappings.Add(column6);

                            //FHA Mortgage Insurance Premium
                            SqlBulkCopyColumnMapping column7 = new SqlBulkCopyColumnMapping(7, 24);
                            sqlBulk.ColumnMappings.Add(column7);

                            // Actual Number Of Resident Days
                            SqlBulkCopyColumnMapping column8 = new SqlBulkCopyColumnMapping(8, 47);
                            sqlBulk.ColumnMappings.Add(column8);

                            SqlBulkCopyColumnMapping column9 = new SqlBulkCopyColumnMapping(9, 42);
                            sqlBulk.ColumnMappings.Add(column9);

                            SqlBulkCopyColumnMapping column10 = new SqlBulkCopyColumnMapping(10, 44);
                            sqlBulk.ColumnMappings.Add(column10);

                            SqlBulkCopyColumnMapping column11 = new SqlBulkCopyColumnMapping(11, 40);
                            sqlBulk.ColumnMappings.Add(column11);

                            SqlBulkCopyColumnMapping column12 = new SqlBulkCopyColumnMapping(12, 46);
                            sqlBulk.ColumnMappings.Add(column12);

                            //Give your Destination table name
                            sqlBulk.DestinationTableName = "Lender_DataUpload_Intermediate";
                            sqlBulk.WriteToServerAsync(dt);
                        }
                        foreach (var lid in lenderIds.Distinct())
                        {
                            //Gets the Servicer ID
                            iLenderID = lid;
                            string sConnection_HCP_Live_db =
                                ConfigurationManager.ConnectionStrings[WebUiConstants.LiveConnectionName].ConnectionString;
                            SqlCommand command1 = new SqlCommand();
                            iUserID = UserPrincipal.Current.UserId;
                            using (var conn = new SqlConnection(sConnection_HCP_Live_db))
                            {
                                conn.Open();
                                command1.Connection = conn;
                                command1.CommandText = "usp_HCP_Verify_PropertyID_iRems";
                                command1.CommandType = CommandType.StoredProcedure;

                                // Add the input parameter and set its properties.
                                SqlParameter parameter = new SqlParameter();

                                //Servicer ID/Lender ID
                                parameter.ParameterName = "@LenderID";
                                parameter.SqlDbType = SqlDbType.Int;
                                parameter.Direction = ParameterDirection.Input;

                                //Add the parameter to the Parameters collection - User Name.
                                command1.Parameters.AddWithValue("LenderID", iLenderID);

                                //User ID
                                parameter.ParameterName = "@UserID";
                                parameter.SqlDbType = SqlDbType.Int;
                                parameter.Direction = ParameterDirection.Input;

                                // Add the parameter to the Parameters collection - Password.
                                command1.Parameters.AddWithValue("UserID", iUserID);
                                command1.ExecuteNonQuery();
                            }
                        }
                        _uploadDataManager.CalcAndUpdateUploadedData();
                        excelConnection.Close();
                        int resultDistribution = 1, resultCalculations = 1, resultCalculateErrors = 1;
                        resultDistribution = _uploadDataManager.DistributeQuarterly();
                        if (resultDistribution > 0)
                        {
                            resultCalculations = _uploadDataManager.ApplyCalculations(Querydate);
                            if (resultCalculations < 0)
                            {
                                throw new DataUploadException(ExceptionCategory.ExcelUploadException, "Failed To Apply Calculations");
                            }
                            else
                            {
                                resultCalculateErrors = _uploadDataManager.CalculateErrors();
                            }
                            if (resultCalculateErrors < 0)
                            {
                                throw new DataUploadException(ExceptionCategory.ExcelUploadException, "Failed To Calculate Errors");
                            }
                        }
                        else
                        {
                            throw new DataUploadException(ExceptionCategory.ExcelUploadException, "Failed To Distribute Quarterly");
                        }
                        Session["Confirmation"] = "File Uploaded Successfully (" + sFileName + ")";
                        Dictionary<string, string> dictfhaNumbers = new Dictionary<string, string>();
                        foreach (DataRow item in dt.Rows)
                        {
                            // 0: fha number column
                            var fha = item[0].ToString();
                            if (!string.IsNullOrEmpty(fha) && !dictfhaNumbers.ContainsKey(fha)) dictfhaNumbers.Add(fha, fha);
                        }
                        // use thread pool to finish sending email so not holding up UI
                        if (_isAeEmailAlertEnabled)
                        {
                            _backgroundJobManager.SendUploadNotificationEmails(_emailManager, dictfhaNumbers.Keys);
                        }
                    }
                    else
                    {
                        Session["Confirmation"] = "Please make sure the excel file you are uploading has data in it.";
                    }
                }
            }
            catch (OleDbException dbEx)
            {
                Session["Confirmation"] = "Excel file does not have correct format to upload, OleDbException: " + dbEx.Message;
                ErrorSignal.FromCurrentContext().Raise(dbEx);
            }
            catch (HUDPortalException dUpEx)
            {
                Session["Confirmation"] = dUpEx.Message;
                ErrorSignal.FromCurrentContext().Raise(dUpEx);
            }
            catch (Exception ex)
            {
                Session["Confirmation"] = "System error occurred. Please contact HHCP help desk with your upload file.";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                if (excelConnection != null)
                {
                    excelConnection.Close();
                    excelConnection.Dispose();
                }
                if (sqlIntermediateDBConnection != null)
                {
                    sqlIntermediateDBConnection.Close();
                    sqlIntermediateDBConnection.Dispose();
                }
            }
            return RedirectToAction("ExcelUploadView", "ExcelUpload");
        }

        //[HttpGet]
        public ContentResult GetCurrentProgress(string userId)
        {
            ControllerContext.HttpContext.Response.AddHeader("cache-control", "no-cache");
            //var currentProgress = longRunningClass.GetStatus(id).ToString();
            string currentProgress = "1";
            int UserId = int.Parse(userId);
            lock (syncRoot)
            {
                if (ProcessStatus.ContainsKey(UserId))
                    currentProgress = ProcessStatus[UserId].ToString();
            }
            return Content(currentProgress);
        }

        private void sqlBulk_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            lock (syncRoot)
            {
                if (ProcessStatus.ContainsKey(UserPrincipal.Current.UserId))
                    ProcessStatus[UserPrincipal.Current.UserId] = e.RowsCopied;
            }
        }

        private void DropIndexBeforeInsert(SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText =
                "ALTER INDEX [PK_Lender_DataUpload_Intermediate] ON [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate] DISABLE";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            cmd.ExecuteNonQuery();
        }

        private void RecreateIndexAfterInsert(SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText =
                "ALTER INDEX [PK_Lender_DataUpload_Intermediate] ON [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate] REBUILD";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            cmd.ExecuteNonQuery();
        }
    }
}
