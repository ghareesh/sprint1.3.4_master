﻿using System;
using System.Web;
using System.Web.Mvc;
using BusinessService.Interfaces;
using BusinessService.ManagementReport;
using Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using Model;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Builder;
using BusinessService.ProjectDetailReport;
using MvcSiteMapProvider.Web.Mvc.Filters;
using System.Configuration;
using System.Linq;
using System.Collections.Generic;
namespace HUDHealthcarePortal.Controllers
{
    public class ProjectDetailReportController : Controller
    {
        private IProjectDetailReportManager _projectDetailReportManager;
        public string Querydate = "";

        public ProjectDetailReportController()
            : this(new ProjectDetailReportManager())
        {
            Querydate = ConfigurationManager.AppSettings["Querydate"].ToString();
        }


        public ProjectDetailReportController(IProjectDetailReportManager projectDetailReportManager)
        {
            _projectDetailReportManager = projectDetailReportManager;
            Querydate = ConfigurationManager.AppSettings["Querydate"].ToString();
        }

        #region ProjectDetailedReport
        [HttpGet]
        //  [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        //[MvcSiteMapNode(Title = "Project Detail Report", ParentKey = "ProjectDetailReportId", PreservedRouteParameters = "reportType,wlmId,aeId,minUploadDate,maxUploadDate,partial")]
        //[SiteMapTitle("Project Detail Report", Target = AttributeTarget.CurrentNode)]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive,Lender Account Representative,Lender Account Manager,Backup Account Manager")]
        public ActionResult Index()
        {
            Session["AdminProjdetail"] = null;
            ClearSession();
            var username = UserPrincipal.Current.UserName;
            //var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT);

            if (RoleManager.IsSuperUserRole(username))
            {
                return this.RedirectToAction("SuperUserProjectReportHighLevel", new { userName = username });
            }
            if (RoleManager.IsWorkloadManagerRole(username))
            {
                //return this.RedirectToAction("GetProjectDetailReportForWorkloadManager", new { userName = username });//karri
                return this.RedirectToAction("SuperUserProjectReportHighLevel", new { userName = username });// harish commented (all will access admin view)
            }
            if (RoleManager.IsAccountExecutiveRole(username))
            {
                //return this.RedirectToAction("GetProjectDetailReportForAccountExecutive", new { userName = username });//karri
                return this.RedirectToAction("SuperUserProjectReportHighLevel", new { userName = username });// harish commented (all will access admin view)
            }

            if (RoleManager.IsUserLenderRole(username))
            {
                return this.RedirectToAction("LenderProjectDetailReport", new { userName = username });
            }
            
            if (RoleManager.IsInternalSpecialOptionUser(username))
            {
                 //return this.RedirectToAction("GetProjectDetailReportForInternalSpecialOptionUser", new { userName = username });//karri
                return this.RedirectToAction("SuperUserProjectReportHighLevel", new { userName = username });// harish commented (all will access admin view)
            }

            return new EmptyResult();
        }
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        public ActionResult Facade(string username, int IsWLM)
        {
            ClearSession();

            if (IsWLM == 1)
            {
                return this.RedirectToAction("GetProjectDetailReportForWorkloadManager", new { userName = username });
            }
            else if (IsWLM == 0)
            {
                return this.RedirectToAction("GetProjectDetailReportForAccountExecutive", new { userName = username });
            }

            return new EmptyResult();
        }



        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        public ActionResult GetProjectDetailReportForSuperUser(string userName)
        {
            ClearSession();
            ViewBag.ExcelExportAction = "ExportProjectDetailSuperUser";
            ViewBag.PrintAction = "PrintProjectDetailSuperUser";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUser(userName, HUDRole.SuperUser.ToString(), Querydate);

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"username", userName}
            };
            // Session["AdminProjdetail"] = reportModel;
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/SuperUserProjectReport.cshtml", reportModel);

        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        //[MvcSiteMapNode(Title = "Project Detail Report ", ParentKey = "MyDetailReportId", PreservedRouteParameters = "", Key = "AllprojDetailLevelKey")]
        public ActionResult GetALLProjectDetailReportForSuperUser()
        {
            //  Session["AdminProjdetail"] = null;
            var username = UserPrincipal.Current.UserName;
            ViewBag.ExcelExportAction = "ExportProjectDetailSuperUser";
            ViewBag.PrintAction = "PrintProjectDetailSuperUser";
            ViewBag.ExcelActionParam = new { username };
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            ViewBag.GetMIPBasedOnPEUrl = Url.Action("GetMIPBasedOnPE");
            ViewBag.GenericSearchUrl = Url.Action("GetFilteredList");

            ViewBag.GetPEBasedOnPFNOUrl = Url.Action("GetPEBasedOnPFNO");
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUser(username, HUDRole.SuperUser.ToString(), Querydate);

            SetPEandQuarterList(reportModel);
            // Session["AdminProjdetail"] = reportModel;

            return View("~/Views/ProjectDetailReport/SuperUserProjectReport.cshtml", reportModel);
        }


        public JsonResult GetFilteredList(string PEList, string QtrList, string PFNumList, string Role, string username, string PIDList, string FHAList, string PFNameList)
        {

            string[] listPe = new string[0];
            string[] listQtr = new string[0];
            string[] listPID = new string[0];
            string[] listFHA = new string[0];
            string[] listPFNo = new string[0];
            int[] listQtrint = new int[0];
            int[] listPIDint = new int[0];
            string[] lispfname = new string[0];

            if (QtrList != null)
            {
                listQtr = QtrList.Split(',');
                listQtrint = listQtr.Select(int.Parse).ToArray();
            }

            if (PIDList != null)
            {
                listPID = PIDList.Split(',');
                listPIDint = listPID.Select(int.Parse).ToArray();
            }

            if (PFNameList != null)
            {
                lispfname = PFNameList.Split(',');

            }

            if (FHAList != null)
            {
                listFHA = FHAList.Split(',');

            }

            if (PEList != null)
            {
                listPe = PEList.Split(',');

            }

            if (PFNumList != null)
            {
                listPFNo = PFNumList.Split(',');
                listPFNo = listPFNo.Select(x => x.Replace("Unassigned Portfolio Number", "")).ToArray();
            }

            var reportmodel = new List<ReportGridModel>();

            if (Session["FilterResult"] != null)
            {
                reportmodel  = Session["FilterResult"] as List<ReportGridModel>;

                reportmodel.ToList().ForEach(x =>
                {
                    if (x.PortfolioNumber == "0")
                    {
                        x.PortfolioNumber = "";
                        //Do Something
                    }

                    if (x.PortfolioName == "0")
                    {
                        x.PortfolioName = "";
                        //Do Something
                    }
                });

                //Pf naumber filteration
                if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
                }
                else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
                }

                else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
                }
                else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).ToList();
                }

                else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
                }

                else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
                }

                else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
                }

               //End of PF Number


               //FHA Number Filteration
                else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
                }

                else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
                }

                else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
                }

                //End  of Fha Combination

                else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
                }

                else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).ToList();
                }

                else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPIDint.Contains(s.PropertyID.Value)).ToList();
                }

                else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listFHA.Contains(s.FhaNumber)).ToList();
                }

                else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPe.Contains(s.PeriodEnding)).ToList();
                }

                else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
                }

                    //New Condition
                else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length == 0)
                {
                    reportmodel = reportmodel.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
                }

                //New Condition

                else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
                }

                //New Condition

                //New Condition
                else  if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
                }
                else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
                {
                    reportmodel = reportmodel.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
                }
        


            }
            reportmodel.OrderByDescending(x => DateTime.Parse(x.PeriodEnding)).ToList();


            var PFName = (from a in reportmodel

                        select new 
                        {
                           
                            PortfolioName = a.PortfolioName
                   }
              ).ToList().Distinct();

            var PID = (from a in reportmodel

                          select new 
                          {

                              PropertyID = a.PropertyID
                          }
             ).ToList().Distinct();

            var FHA = (from a in reportmodel

                       select new 
                       {

                           FhaNumber = a.FhaNumber
                       }
            ).ToList().Distinct();

            var PE = (from a in reportmodel

                       select new 
                       {

                           PeriodEnding = a.PeriodEnding
                       }
            ).ToList().Distinct();

            var MIP = (from a in reportmodel

                      select new 
                      {

                          MonthsInPeriod = a.MonthsInPeriod
                      }
            ).ToList().Distinct();




            var data = new { PFName = PFName, PID = PID, FHA = FHA, PE = PE, MIP = MIP };



            return Json(new { data }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMIPBasedOnPE(string PEList, string PFNumList)
        {
            string[] listPe = new string[0];
            string[] listPFNo = new string[0];
            List<SelectListItem> temp = new List<SelectListItem>();

            if (Session["FilterResult"] != null)
            {
                var list = Session["FilterResult"] as List<ReportGridModel>;


                if (PEList != null)
                {
                    listPe = PEList.Split(',');

                }

                if (PFNumList != null)
                {
                    listPFNo = PFNumList.Split(',');

                }
                if (listPFNo.Length > 0 && listPe.Length > 0)
                {
                    var multilist = new MultiSelectList(list.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList().Select(p => new KeyValuePair<int, int>(p.MonthsInPeriod, p.MonthsInPeriod)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");
                    temp = multilist.ToList();
                }
                else if (listPFNo.Length > 0 && listPe.Length == 0)
                {
                    var multilist = new MultiSelectList(list.Where(s => listPFNo.Contains(s.PortfolioNumber)).ToList().Select(p => new KeyValuePair<int, int>(p.MonthsInPeriod, p.MonthsInPeriod)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");
                    temp = multilist.ToList();
                }

                else if (listPFNo.Length == 0 && listPe.Length > 0)
                {
                    var multilist = new MultiSelectList(list.Where(s => listPe.Contains(s.PeriodEnding)).ToList().Select(p => new KeyValuePair<int, int>(p.MonthsInPeriod, p.MonthsInPeriod)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");
                    temp = multilist.ToList();
                }
                //temp = list.ToList().Where(s => listPe.Contains(s.Value)).Distinct() .ToList();
            }

            return Json(new { temp }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPEBasedOnPFNO(string PFNumList)
        {
            string[] listPN = new string[0];
            List<SelectListItem> temp = new List<SelectListItem>();

            if (Session["PFNOPEListItems"] != null)
            {
                var list = Session["PFNOPEListItems"] as MultiSelectList;



                if (PFNumList != null)
                {
                    listPN = PFNumList.Split(',');

                }
                temp = list.ToList().Where(s => listPN.Contains(s.Value)).Distinct().ToList();
            }

            return Json(new { temp }, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public void ClearSession()
        {
            Session["listPe"] = null;
            Session["listQtrint"] = null;
            Session["username"] = null;
            Session["SearchRole"] = null;
            Session["listPFNo"] = null;
            
            Session["listPIDint"] = null;
            Session["listFHA"] = null;
            Session["lispfname"] = null;
        }
        public ActionResult SearchForRequests(string PEList, string QtrList, string PFNumList, string Role, string username, string PIDList, string FHAList, string PFNameList, bool IsShowExceptionsOnly=true)
        {
            if (username != null)
            {
                username = Server.UrlDecode(username);
                username = username.Replace("%40", "@");
            }
            string[] listPe = new string[0];
            string[] listQtr = new string[0];
            string[] listPID = new string[0];
            string[] listFHA = new string[0];
            string[] listPFNo = new string[0];
            int[] listQtrint = new int[0];
            int[] listPIDint = new int[0];
            string[] lispfname = new string[0];
            bool showException = false;
            if (IsShowExceptionsOnly)
            {
                showException = IsShowExceptionsOnly;
            }
           
            
            if (QtrList != null)
            {
                listQtr = QtrList.Split(',');
                listQtrint = listQtr.Select(int.Parse).ToArray();
            }

            if (PIDList != null)
            {
                listPID = PIDList.Split(',');
                listPIDint = listPID.Select(int.Parse).ToArray();
            }

            if (PFNameList != null)
            {
                lispfname = PFNameList.Split(',');
               
            }

            if (FHAList != null)
            {
                listFHA = FHAList.Split(',');

            }

            if (PEList != null)
            {
                listPe = PEList.Split(',');

            }

            if (PFNumList != null)
            {
                listPFNo = PFNumList.Split(',');
                listPFNo = listPFNo.Select(x => x.Replace("Unassigned Portfolio Number", "")).ToArray();
            }


            //Carry Forward parameters for Excel Export
            if (Request.QueryString.Count == 0)
            {
                Session["listPe"] = listPe;
                Session["listQtrint"] = listQtrint;
                Session["username"] = username;
                Session["SearchRole"] = Role;
                Session["listPFNo"] = listPFNo;
                Session["listPIDint"] = listPIDint;
                Session["listFHA"] = listFHA;
                Session["lispfname"] = lispfname;
                Session["showException"] = showException;
            }
            else
            {
                listPe = Session["listPe"] as string[];
                listQtrint = Session["listQtrint"] as int[];
                username = Session["username"] as string;
                Role = Session["SearchRole"] as string;
                listPFNo = Session["listPFNo"] as string[];
                listPIDint = Session["listPIDint"] as int[];
                listFHA = Session["listFHA"] as string[];

                lispfname = Session["lispfname"] as string[];
                showException = (bool)Session["showException"];
            }



            switch (Role)
            {
                case "Admin":

                    var reportmodel = _projectDetailReportManager.GetProjectDetailReportForSuperUser(UserPrincipal.Current.UserName, HUDRole.SuperUser.ToString(), Querydate);

                    ApplyFilter(reportmodel, listPe, listQtrint, listPFNo, listPIDint, listFHA,showException);
                    return PartialView("~/Views/ProjectDetailReport/_SuperUserProjectReport.cshtml", reportmodel);
                    break;
                case "WLM":
                    var reportmodelWLM = _projectDetailReportManager.GetProjectDetailReportForWorkloadManager(username, HUDRole.WorkflowManager.ToString(), Querydate);

                    ApplyFilter(reportmodelWLM, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);
                    return PartialView("~/Views/ProjectDetailReport/_SuperUserProjectReport.cshtml", reportmodelWLM);
                    break;
                case "AE":
                    var reportmodelAE = _projectDetailReportManager.GetProjectDetailReportForWorkloadManager(username, HUDRole.AccountExecutive.ToString(), Querydate);

                    ApplyFilter(reportmodelAE, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);
                    return PartialView("~/Views/ProjectDetailReport/_SuperUserProjectReport.cshtml", reportmodelAE);
                    break;

                case "Lender":
                    var reportmodelLender = _projectDetailReportManager.GetProjectDetailReportForWorkloadManager(username, HUDRole.LenderAccountRepresentative.ToString(), Querydate);

                    ApplyFilter(reportmodelLender, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);
                    return PartialView("~/Views/ProjectDetailReport/_SuperUserProjectReport.cshtml", reportmodelLender);
                    break;
                case "specialoptionuser":
                    var reportmodelISU = _projectDetailReportManager.GetProjectDetailReportForWorkloadManager(username, HUDRole.InternalSpecialOptionUser.ToString(), Querydate);

                    ApplyFilter(reportmodelISU, listPe, listQtrint, listPFNo, listPIDint, listFHA, showException);
                    return PartialView("~/Views/ProjectDetailReport/_SuperUserProjectReport.cshtml", reportmodelISU);
                    break;

            }
            return View();
            // return PartialView("~/Views/ProjectDetailReport/_SuperUserProjectReport.cshtml", model);
        }

        //Non Acction Method
        [NonAction]
        public ReportModel ApplyFilter(ReportModel reportmodel, string[] listPe, int[] listQtrint, string[] listPFNo, int[] listPIDint, string[] listFHA,bool isShowExceptionOnly)
         {


            reportmodel.ReportGridModelList.ToList().ForEach(x =>
            {
                if (x.PortfolioNumber == "0")
                {
                    x.PortfolioNumber = "";
                    //Do Something
                }

                if (x.PortfolioName == "0")
                {
                    x.PortfolioName = "";
                    //Do Something
                }
            });

            //exception report only checked
            if (isShowExceptionOnly)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(m => m.IsProjectError == 1 && m.RowWiseError > 0).ToList();
            }


            //Pf naumber filteration
            if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }
            else  if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
            }
            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).ToList();
            }

            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

           //End of PF Number


           //FHA Number Filteration
            else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            //End  of Fha Combination

            else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listFHA.Contains(s.FhaNumber)).ToList();
            }

            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            else if (listPe.Length == 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length == 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }

            //New Condition
            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length == 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

             //New Condition

            else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length > 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPFNo.Contains(s.PortfolioNumber)).Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
            }

            //New Condition
           else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
                
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }

            //New Condition

           else if (listPe.Length == 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
           {
               reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).ToList();
           }

            //New Condition

            else if (listPe.Length > 0 && listQtrint.Length == 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).ToList();
            }
            else if (listPe.Length > 0 && listQtrint.Length > 0 && listPFNo.Length == 0 && listPIDint.Length > 0 && listFHA.Length > 0)
            {
                reportmodel.ReportGridModelList = reportmodel.ReportGridModelList.Where(s => listPIDint.Contains(s.PropertyID.Value)).Where(s => listFHA.Contains(s.FhaNumber)).Where(s => listPe.Contains(s.PeriodEnding)).Where(s => listQtrint.Contains(s.MonthsInPeriod)).ToList();
            }


            var ProjectCnt = reportmodel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();
            var ErrorCnt = (from od in reportmodel.ReportGridModelList where od.IsProjectError == 1 select od.FhaNumber).Distinct().Count();
            reportmodel.ReportHeaderModel.TotalProperties = ProjectCnt;
            reportmodel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;
            var TotalNoiError = (from od in reportmodel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
            var TotalDSCRError = (from od in reportmodel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
            var TotalADRError = (from od in reportmodel.ReportGridModelList where od.IsADRError == 1 select od).Count();
            var TotalOpRevError = (from od in reportmodel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
            var TotalColumErrors = reportmodel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

            reportmodel.ReportHeaderModel.TotalProperties = ProjectCnt;
            reportmodel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;
            reportmodel.ReportHeaderModel.TotalDSCRError = TotalDSCRError;
            reportmodel.ReportHeaderModel.TotalNoiError = TotalNoiError;
            reportmodel.ReportHeaderModel.TotalOpRevError = TotalOpRevError;
            reportmodel.ReportHeaderModel.TotalADRError = TotalADRError;
            reportmodel.ReportHeaderModel.TotalColError = TotalColumErrors;


            reportmodel.ReportGridModelList.OrderByDescending(a => a.DataInserted);
            //ViewBag.ProjectCnt = ProjectCnt;
            //ViewBag.ErrorCnt = ErrorCnt;
            //Session["umesh"] = ProjectCnt;
            return reportmodel;
        }
        [NonAction]
        public ReportModel SetPEandQuarterList(ReportModel reportModel)
        {
            Session["PEQtrListItems"] = null;
            Session["PFNOPEListItems"] = null;
            Session["FilterResult"] = null;
            Session["PROPIDListItems"] = null;
            Session["FHANOListItems"] = null;
           
            reportModel.ReportGridModelList.ToList().ForEach(x =>
            {
                if (x.PortfolioNumber == "0")
                {
                    x.PortfolioNumber = "Unassigned Portfolio Number";
                    //Do Something
                }

                if (x.PortfolioName == "0")
                {
                    x.PortfolioName = "Unassigned Portfolio Name";
                    //Do Something
                }
            });

            reportModel.PeriodEndingListItems = new MultiSelectList(reportModel.ReportGridModelList.Select(p => new KeyValuePair<string, string>(p.PeriodEnding, p.PeriodEnding)).Distinct().OrderByDescending(x => DateTime.Parse(x.Value)).ToList(), "Key", "Value");
            reportModel.QuarterListItems = new MultiSelectList(reportModel.ReportGridModelList.Select(p => new KeyValuePair<int, int>(p.MonthsInPeriod, p.MonthsInPeriod)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");

            reportModel.PortFolioNumberListItems = new MultiSelectList(reportModel.ReportGridModelList.Select(p => new KeyValuePair<string, string>(p.PortfolioName, p.PortfolioNumber)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");

            reportModel.PortFolioNameListItems = new MultiSelectList(reportModel.ReportGridModelList.Select(p => new KeyValuePair<string, string>(p.PortfolioNumber, p.PortfolioName)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");


            reportModel.PEQtrListItems = new MultiSelectList(reportModel.ReportGridModelList.Select(p => new KeyValuePair<string, string>(p.PeriodEnding, p.MonthsInPeriod.ToString())).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");
            reportModel.PFNOPEListItems = new MultiSelectList(reportModel.ReportGridModelList.Select(p => new KeyValuePair<string, string>(p.PortfolioNumber, p.PeriodEnding)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");

            reportModel.PROPIDListItems = new MultiSelectList(reportModel.ReportGridModelList.Select(p => new KeyValuePair<int, int>(p.PropertyID.Value, p.PropertyID.Value)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");
            reportModel.FHANOListItems = new MultiSelectList(reportModel.ReportGridModelList.Select(p => new KeyValuePair<string, string>(p.FhaNumber, p.FhaNumber)).Distinct().OrderBy(p => p.Value).ToList(), "Key", "Value");

            //var FilterResult = from a in reportModel.ReportGridModelList select new { a.PortfolioNumber, a.PeriodEnding, a.MonthsInPeriod };

            Session["PEQtrListItems"] = reportModel.PEQtrListItems;
            Session["PFNOPEListItems"] = reportModel.PFNOPEListItems;
            Session["PROPIDListItems"] = reportModel.PROPIDListItems;
            Session["FHANOListItems"] = reportModel.FHANOListItems;
            Session["FilterResult"] = reportModel.ReportGridModelList;

            reportModel.ReportGridModelList.ToList().ForEach(x =>
            {
                //x.DataInserted = TimezoneManager.GetPreferredTimeFromUtc(x.DataInserted);
                x.DataInserted = x.DataInserted;
                if (x.PortfolioNumber == "Unassigned Portfolio Number")
                {
                    x.PortfolioNumber = "";
                    //Do Something
                }

                if (x.PortfolioName == "Unassigned Portfolio Name")
                {
                    x.PortfolioName = "";
                    //Do Something
                }
            });

        
            return reportModel;
        }

        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "Project Detail Report HighLevel", ParentKey = "MyDetailReportId", PreservedRouteParameters = "", Key = "HighLevelKey")]

        public ActionResult SuperUserProjectReportHighLevel(string userName)
        {
            string orderby = Request.QueryString["sortdir"];
            var reportModel = new ReportModel(ReportType.ProjectDetailReport);
            if (string.IsNullOrEmpty(orderby))
            {
                reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUserHighLevel(userName, HUDRole.SuperUser.ToString(), "ASC", Querydate);

            }
            else
            {
                if (orderby.ToUpper() == "ASC")
                {
                    reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUserHighLevel(userName, HUDRole.SuperUser.ToString(), "DESC", Querydate);
                }
                else if (orderby.ToUpper() == "DESC")
                {
                    reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUserHighLevel(userName, HUDRole.SuperUser.ToString(), "ASC", Querydate);
                }
            }


            ViewBag.ExcelExportAction = "ExportProjectDetailSuperUserHighLevel";
            ViewBag.PrintAction = "PrintProjectDetailSuperUserHighLevel";
            ViewBag.ExcelActionParam = new { userName };

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AdminId", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/SuperUserProjectReportHighLevel.cshtml", reportModel);
        }



        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "Exception  Report HighLevel", ParentKey = "ProjectErrorReportId", PreservedRouteParameters = "reportType,wlmId,aeId,minUploadDate,maxUploadDate,partial", Key = "HighLevelErrorKey")]
       // [SiteMapTitle("Key", Target = AttributeTarget.CurrentNode)]
       // [SiteMapTitle("Title", Target = AttributeTarget.CurrentNode)]
        public ActionResult SuperUserProjectReportHighLevelError(string userName)
        {

            ViewBag.ExcelExportAction = "ExportProjectDetailSuperUserHighLevel";
            ViewBag.PrintAction = "PrintProjectDetailSuperUserHighLevel";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForSuperUserHighLevel(userName, HUDRole.SuperUser.ToString(), "ASC", Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AdminId", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/SuperUserProjectReportHighLevel.cshtml", reportModel);
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        //[MvcSiteMapNode(Title = "Project Detail Report WLM Level", ParentKey = "MyDetailReportId", PreservedRouteParameters = "wlmId", Key = "HighLevelKeyWLM")]
        public ActionResult GetProjectDetailReportForWorkloadManager(string userName)
        {
            Session["WlmProjdetail"] = null;
            ViewBag.ExcelExportAction = "ExportProjectDetailWLM";
            ViewBag.PrintAction = "ExportProjectDetailWLM";
            ViewBag.ExcelActionParam = new { userName };
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            ViewBag.GetMIPBasedOnPEUrl = Url.Action("GetMIPBasedOnPE");
            ViewBag.GetPEBasedOnPFNOUrl = Url.Action("GetPEBasedOnPFNO");
            ViewBag.GenericSearchUrl = Url.Action("GetFilteredList");
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForWorkloadManager(userName, HUDRole.WorkflowManager.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"wlmId", userName}
            };

            SetPEandQuarterList(reportModel);
            Session["WlmProjdetail"] = reportModel;
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT] = routeValuesDict;
            //reportModel.ReportGridModelList.Where(m => m.IsQtrlyDSCRError == 1);
            return View("~/Views/ProjectDetailReport/ProjectDetaiReport _WLM.cshtml", reportModel);
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "Project Detail Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "wlmId", Key = "HighLevelKeyAE")]
        public ActionResult GetProjectDetailReportForAccountExecutive(string userName)
        {
            Session["AEProjdetail"] = null;
            ViewBag.ExcelExportAction = "ExportProjectDetailAE";
            ViewBag.PrintAction = "ExportProjectDetailAE";
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            ViewBag.GetMIPBasedOnPEUrl = Url.Action("GetMIPBasedOnPE");
            ViewBag.GenericSearchUrl = Url.Action("GetFilteredList");
            ViewBag.GetPEBasedOnPFNOUrl = Url.Action("GetPEBasedOnPFNO");
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForAccountExecutive(userName, HUDRole.AccountExecutive.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AEID", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT] = routeValuesDict;
            SetPEandQuarterList(reportModel);
            Session["AEProjdetail"] = reportModel;
            return View("~/Views/ProjectDetailReport/ProjectDetaiReport _AE.cshtml", reportModel);
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive,Lender Account Representative,Lender Account Manager,Backup Account Manager")]
       // [MvcSiteMapNode(Title = "Project Detail Report Lender Level", ParentKey = "MyDetailReportId", PreservedRouteParameters = "wlmId", Key = "HighLevelKeyLender")]
        public ActionResult LenderProjectDetailReport(string userName)
        {
            ViewBag.ExcelExportAction = "ExportProjectDetailLender";
            ViewBag.PrintAction = "ExportLenderProjectDetailReport";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForAccountExecutive(userName, HUDRole.LenderAccountRepresentative.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AEID", userName}
            };
            SetPEandQuarterList(reportModel);
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            ViewBag.GetMIPBasedOnPEUrl = Url.Action("GetMIPBasedOnPE");
            ViewBag.GenericSearchUrl = Url.Action("GetFilteredList");
            ViewBag.GetPEBasedOnPFNOUrl = Url.Action("GetPEBasedOnPFNO");
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/ProjectDetaiReport _Lender.cshtml", reportModel);
        }
        #endregion

        #region ProjectExceptionReport
        [HttpGet]
        // [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive,Lender Account Representative,Lender Account Manager,Backup Account Manager")]
        public ActionResult IndexError()
        {
            var username = UserPrincipal.Current.UserName;
            //var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            //var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_PROJECT_DETAIL_ERROR_REPORTLINK_DICT);

            if (RoleManager.IsSuperUserRole(username))
            {
                return this.RedirectToAction("SuperUserProjectReportHighLevelError", new { userName = username });
            }
            if (RoleManager.IsWorkloadManagerRole(username))
            {
                return this.RedirectToAction("GetErrorReportForWorkloadManager", new { userName = username });
            }
            if (RoleManager.IsAccountExecutiveRole(username))
            {
                return this.RedirectToAction("GetErrorReportForAccountExecutive", new { userName = username });
            }

            if (RoleManager.IsInternalSpecialOptionUser(username))
            {
                return this.RedirectToAction("GetErrorReportForInternalSpecialOptionUser", new { userName = username });
            }
            return new EmptyResult();
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "Project Error Report ", ParentKey = "ProjectErrorReportId", PreservedRouteParameters = "", Key = "AllProjErrorKey")]
        public ActionResult GetALLErrorReportForSuperUser()
        {
            var username = UserPrincipal.Current.UserName;
            ViewBag.ExcelExportAction = "ExportProjectErrorSuperUser";
            ViewBag.PrintAction = "PrintProjectErrorSuperUser";
            ViewBag.ExcelActionParam = new { username };
            var reportModel = _projectDetailReportManager.GetErrorReportForSuperUser(username, HUDRole.SuperUser.ToString(), Querydate);

            reportModel.ReportGridModelList.ToList().ForEach(x =>
            {
                //x.DataInserted = TimezoneManager.GetPreferredTimeFromUtc(x.DataInserted);
                x.DataInserted = x.DataInserted;
                if (x.PortfolioNumber == "0")
                {
                    x.PortfolioNumber = "";                  
                }

                if (x.PortfolioName == "0")
                {
                    x.PortfolioName = "";
                }
            });
            return View("~/Views/ProjectDetailReport/SuperUserErrorReport.cshtml", reportModel);
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "Exception Report WLM Level", ParentKey = "ProjectErrorReportId", PreservedRouteParameters = "wlmId", Key = "ErrorKeyWLM")]
        public ActionResult GetErrorReportForWorkloadManager(string userName)
        {
            ViewBag.ExcelExportAction = "ExportProjectErrorWLM";
            ViewBag.PrintAction = "ExportErrorDetailWLM";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetErrorReportForWorkloadManager(userName, HUDRole.WorkflowManager.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"wlmIdEx", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAIL_ERROR_REPORTLINK_DICT] = routeValuesDict;
           // SetPEandQuarterList(reportModel);
            reportModel.ReportGridModelList.ToList().ForEach(x =>
            {
                //x.DataInserted = TimezoneManager.GetPreferredTimeFromUtc(x.DataInserted);
                x.DataInserted = x.DataInserted;
                if (x.PortfolioNumber == "0")
                {
                    x.PortfolioNumber = "";
                }

                if (x.PortfolioName == "0")
                {
                    x.PortfolioName = "";
                }
            });
            return View("~/Views/ProjectDetailReport/ErrorReport _WLM.cshtml", reportModel);
        }
        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,AccountExecutive")]
        public ActionResult FacadeError(string username, int IsWLM)
        {

            if (IsWLM == 1)
            {
                return this.RedirectToAction("GetErrorReportForWorkloadManager", new { userName = username });
            }
            else if (IsWLM == 0)
            {
                return this.RedirectToAction("GetErrorReportForAccountExecutive", new { userName = username });
            }

            return new EmptyResult();
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive,InternalSpecialOptionUser")]
        //[MvcSiteMapNode(Title = "Exception Report AE Level", ParentKey = "ProjectErrorReportId", PreservedRouteParameters = "AEID", Key = "ErrorKeyAE")]
        public ActionResult GetErrorReportForAccountExecutive(string userName)
        {
            ViewBag.ExcelExportAction = "ExportProjectErrorAE";
            ViewBag.PrintAction = "ExportErrorReportAE";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetErrorReportForAccountExecutive(userName, HUDRole.AccountExecutive.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AEID", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAIL_ERROR_REPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/ErrorReport _AE.cshtml", reportModel);
        }
        #endregion

        #region ProjectPTReport
        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive,Lender Account Representative,Lender Account Manager,Backup Account Manager")]
        public ActionResult IndexPT()
        {
            var username = UserPrincipal.Current.UserName;
            //var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            var routeValues = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT);

            if (RoleManager.IsSuperUserRole(username))
            {
                return this.RedirectToAction("GetPTReportSuperUser", new { userName = username });
            }
            if (RoleManager.IsWorkloadManagerRole(username))
            {
                return this.RedirectToAction("GetPTReportWorkloadManager", new { userName = username });
            }
            if (RoleManager.IsAccountExecutiveRole(username))
            {
                return this.RedirectToAction("GetPTReportAccountExecutive", new { userName = username });
            }

            if (RoleManager.IsInternalSpecialOptionUser(username))
            {
                return this.RedirectToAction("GetPTReportInternalSpecialOptionUser", new { userName = username });
            }

            return new EmptyResult();
        }

        [HttpGet]
        // [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        //[MvcSiteMapNode(Title = "Point and Time Report ", ParentKey = "MyDetailReportId", PreservedRouteParameters = "", Key = "PTReportKey")]
        public ActionResult GetPTReportSuperUser(string userName)
        {
            ViewBag.ExcelExportAction = "ExportPTReportSuperUser";
            ViewBag.PrintAction = "PrintPTReportSuperUser";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetPTReportForSuperUser(userName, HUDRole.SuperUser.ToString(), Querydate);

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"username", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_PTREPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/GetPTReportSuperUser.cshtml", reportModel);

        }

        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "Point and Time Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "", Key = "PTReportWLMKey")]
        public ActionResult GetPTReportWorkloadManager(string userName)
        {
            ViewBag.ExcelExportAction = "ExportPTReportWLM";
            ViewBag.PrintAction = "PrintPTReportWLM";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetPTReportForWorkloadManager(userName, HUDRole.WorkflowManager.ToString(), Querydate);

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"username", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_WLM_PTREPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/GetPTReportWLM.cshtml", reportModel);

        }

        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "Point and Time Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "", Key = "PTReportAEMKey")]
        public ActionResult GetPTReportAccountExecutive(string userName)
        {
            ViewBag.ExcelExportAction = "ExportPTReportAE";
            ViewBag.PrintAction = "PrintPTReportWLM";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetPTReportForWorkloadManager(userName, HUDRole.AccountExecutive.ToString(), Querydate);

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"username", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_AE_PTREPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/GetPTReportAE.cshtml", reportModel);

        }







        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        //[MvcSiteMapNode(Title = "Point and Time Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "", Key = "PTReportISUMKey")]
        public ActionResult GetPTReportInternalSpecialOptionUser(string userName)
        {
            ViewBag.ExcelExportAction = "ExportPTReportInternalSpecialOptionUser";
            ViewBag.PrintAction = "PrintPTReportWLM";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetPTReportForInternalSpecialOptionUser(userName, HUDRole.InternalSpecialOptionUser.ToString(), Querydate);

            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"username", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_AE_PTREPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/GetPTReportAE.cshtml", reportModel);

        }



        #endregion


        public ActionResult IndexCurrentQuarter()
        {
            var username = UserPrincipal.Current.UserName;
            //var parentNode = SiteMaps.Current.CurrentNode.ParentNode;
            if (RoleManager.IsAccountExecutiveRole(username))
            {
                return this.RedirectToAction("GetCurrentQuarterReportForAccountExecutive", new { userName = username });
            }

            if (RoleManager.IsWorkloadManagerRole(username))
            {
                return this.RedirectToAction("GetCurrentQuarterReportForWLM", new { userName = username });
            }

            if (RoleManager.IsSuperUserRole(username))
            {
                return this.RedirectToAction("GetCurrentQuarterReportForSuperUser", new { userName = username });
            }

            if (RoleManager.IsInternalSpecialOptionUser(username))
            {
                return this.RedirectToAction("GetCurrentQuarterReportForInternalSpecialOptionUser", new { userName = username });
            }

            return new EmptyResult();
        }


        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "AE Current Quarter Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "ProjectAECurQrtReportId", Key = "CurrQrtsKeyAE")]
        public ActionResult GetCurrentQuarterReportForAccountExecutive(string userName)
        {
            ViewBag.ExcelExportAction = "ExportProjectAECurQrt";
            ViewBag.PrintAction = "PrintProjectCurQrtrAE";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetCurrentQuarterReportForAccountExecutive(userName, HUDRole.AccountExecutive.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AEID", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_CURQRTR_AE_REPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/CurQuarterReport _AE.cshtml", reportModel);
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
       // [MvcSiteMapNode(Title = "AE Current Quarter Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "ProjectAECurQrtReportId", Key = "CurrQrtsKeyISU")]
        public ActionResult GetCurrentQuarterReportForInternalSpecialOptionUser(string userName)
        {
            ViewBag.ExcelExportAction = "ExportProjectInternalSpecialOptionUserCurQrt";
            ViewBag.PrintAction = "PrintProjectCurQrtrISU";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetCurrentQuarterReportForInternalSpecialOptionUser(userName, HUDRole.InternalSpecialOptionUser.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AEID", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_CURQRTR_AE_REPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/CurQuarterReport _AE.cshtml", reportModel);
        }

        [HttpGet]
        //[AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,WorkflowManager,AccountExecutive")]
        [MvcSiteMapNode(Title = "AE Current Quarter Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "ProjectWLMCurQrtReportId", Key = "CurrQrtsKeyWLM")]
        public ActionResult GetCurrentQuarterReportForWLM(string userName)
        {
            ViewBag.ExcelExportAction = "ExportProjectWLMCurQrt";
            ViewBag.PrintAction = "PrintProjectCurQrtrAE";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetCurrentQuarterReportForAccountExecutive(userName, HUDRole.WorkflowManager.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"WLMID", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_CURQRTR_WLM_REPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/CurQuarterReports_WLM.cshtml", reportModel);
        }

       // [MvcSiteMapNode(Title = "AE Current Quarter Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "ProjectAdminCurQrtReportId", Key = "CurrQrtsKeyAdmin")]
        public ActionResult GetCurrentQuarterReportForSuperUser(string userName)
        {
            ViewBag.ExcelExportAction = "ExportProjectSuperUserCurQrt";
            ViewBag.PrintAction = "PrintProjectCurQrtrAE";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetCurrentQuarterReportForAccountExecutive(userName, HUDRole.SuperUser.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AdminiID", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_CURQRTR_ADMIN_REPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/CurQuarterReports_SuperUser.cshtml", reportModel);
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "InternalSpecialOptionUser")]
       // [MvcSiteMapNode(Title = "Project Detail Report", ParentKey = "MyDetailReportId", PreservedRouteParameters = "wlmId", Key = "HighLevelKeyISU")]
        public ActionResult GetProjectDetailReportForInternalSpecialOptionUser(string userName)
        {
            Session["AEProjdetail"] = null;
            ViewBag.ExcelExportAction = "ExportProjectDetailInternalSpecialOptionUser";
            ViewBag.PrintAction = "ExportProjectDetailInternalSpecialOptionUser";
            ViewBag.SearchRequestsUrl = Url.Action("SearchForRequests");
            ViewBag.GetMIPBasedOnPEUrl = Url.Action("GetMIPBasedOnPE");
            ViewBag.GenericSearchUrl = Url.Action("GetFilteredList");
            ViewBag.GetPEBasedOnPFNOUrl = Url.Action("GetPEBasedOnPFNO");
            ViewBag.ExcelActionParam = new { userName };
            ViewBag.Role = "ISOU";
            var reportModel = _projectDetailReportManager.GetProjectDetailReportForInternalSpecialOptionUser(userName, HUDRole.InternalSpecialOptionUser.ToString(), Querydate);
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AEID", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAILREPORTLINK_DICT] = routeValuesDict;
            SetPEandQuarterList(reportModel);
            Session["AEProjdetail"] = reportModel;
            return View("~/Views/ProjectDetailReport/ProjectDetaiReport _AE.cshtml", reportModel);
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "InternalSpecialOptionUser")]
       // [MvcSiteMapNode(Title = "Exception Report AE Level", ParentKey = "ProjectErrorReportId", PreservedRouteParameters = "AEID", Key = "ErrorKeyISU")]
        public ActionResult GetErrorReportForInternalSpecialOptionUser(string userName)
        {
            ViewBag.ExcelExportAction = "ExportProjectErrorInternalSpecialOptionUser";
            ViewBag.PrintAction = "ExportProjectErrorInternalSpecialOptionUser";
            ViewBag.ExcelActionParam = new { userName };
            var reportModel = _projectDetailReportManager.GetErrorReportForInternalSpecialOptionUser(userName, HUDRole.InternalSpecialOptionUser.ToString());
            var routeValuesDict = new System.Web.Routing.RouteValueDictionary()
            {
                {"AEID", userName}
            };
            Session[SessionHelper.SESSION_KEY_PROJECT_DETAIL_ERROR_REPORTLINK_DICT] = routeValuesDict;
            return View("~/Views/ProjectDetailReport/ErrorReport _AE.cshtml", reportModel);
        }



    }
}
