﻿using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using BusinessService.Interfaces.InternalExternalTask;
using BusinessService.Interfaces.Production;
using BusinessService.InternalExternalTask;
using BusinessService.Production;
using Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Helpers;
using HUDHealthcarePortal.Model;
using Model.InternalExternalTask;
using Model.Production;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Controllers.AssetManagement
{
    public class InternalExternalTaskController : Controller
    {        
        private IInternalExternalTaskManager internalExternalTaskManger;
        private IProd_RestfulWebApiTokenRequest WebApiTokenRequest;
        private IProd_RestfulWebApiDocumentUpload WebApiDocumentUpload;      
        private IProd_RestfulWebApiDownload webApiDownload;
        private ITaskManager taskManager;
        private IAccountManager accountManager;
        public InternalExternalTaskController():this(new InternalExternalTaskManager(),
                                                     new Prod_RestfulWebApiTokenRequest(),
                                                     new Prod_RestfulWebApiDocumentUpload(),
                                                      new Prod_RestfulWebApiDownload (),
                                                      new TaskManager() , new AccountManager() )
        {

        }


        private InternalExternalTaskController(IInternalExternalTaskManager _internalExternalTaskManger, 
                                               IProd_RestfulWebApiTokenRequest _prod_RestfulWebApiTokenRequest,
                                               IProd_RestfulWebApiDocumentUpload _prod_RestfulWebApiDocumentUpload,
                                               IProd_RestfulWebApiDownload _prod_RestfulWebApiDownload,
                                               ITaskManager _taskManager, IAccountManager _accountManager
                                               )
        {
            internalExternalTaskManger = _internalExternalTaskManger;
            WebApiTokenRequest = _prod_RestfulWebApiTokenRequest;
            WebApiDocumentUpload = _prod_RestfulWebApiDocumentUpload;
            webApiDownload = _prod_RestfulWebApiDownload;
            taskManager = _taskManager;
            accountManager = _accountManager;
        }
        //
        // GET: /InternalExternalTask/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateTask()
        {

            populatelookUps();
            var internalExternalTask = new InternalExternalTaskModel();          

            return View(internalExternalTask);
        }
        [HttpPost]
        public ActionResult CreateTask(InternalExternalTaskModel model,string submit)
        {
            if (model != null && ModelState.IsValid)
            {
                if(model.UserTypeID ==(int)HUDRoleSource.InternalUser &&  model.AssignedToUserName != null && model.AssignedToUserName != model.SelectedUserName)
                {
                    populatelookUps();
                    ModelState.AddModelError(string.Empty, "Please select from users list to assign user!");
                    return View(model);
                }
                switch (submit)
                {
                    case "Submit":
                        model.StatusID = (int)InternalExternalTaskStatus.Submitted;
                        break;
                    case "Save":
                        {
                            model.StatusID = (int)InternalExternalTaskStatus.Draft;
                            model.InUse = UserPrincipal.Current.UserId;
                            break;
                        }
                    case "Save & Check-In" :
                        {
                            model.StatusID = (int)InternalExternalTaskStatus.Draft;
                            break;
                        }
                    default: break;
                    
                }
                if(SaveOrUpdateInternalExternalTask(model))
                {
                    return RedirectToAction("Index","InternalExternalGroupTask");
                }
                else
                {
                    populatelookUps();
                    ModelState.AddModelError(string.Empty, "Unable to Save Internal External Task Please try again!");
                    return View(model);
                }
            }
            populatelookUps();
            ModelState.AddModelError("Error",  errorMessage: @"Invalid Value Please try Again");
            return View(model);
        }
        private bool SaveOrUpdateInternalExternalTask(InternalExternalTaskModel model)
        {
            if(model != null)
            {   
                if(model.UserTypeID == (int)HUDRoleSource.InternalUser)
                {
                    
                    var user = accountManager.GetUserByUsername(model.AssignedToUserName);
                    if (user != null)
                    {
                        model.AssignedToUserID = user.UserID;
                        model.LenderID = null;
                    }
                      
                    else return false;
                }
                else // assigning External User
                {
                    model.AssignedToUserID = null;
                }
                if(model.TaskInstanceID != Guid.Empty)
                {
                    //update Internal External Task Table
                    var result = internalExternalTaskManger.UpdateNoiTask(model);

                    if (result)
                    {
                        if (model.StatusID == (int)InternalExternalTaskStatus.Submitted)
                        {
                            //Save task info in Task table
                            this.SubmitTask(model);

                        }
                        return true;
                    }
                    else return result;
                }
                else
                {
                    //Insert new record in Internal External Task Table
                    var newTask= internalExternalTaskManger.AddInternalExternalTask(model);
                    if (newTask != Guid.Empty)
                    {  
                        if(model.StatusID == (int)InternalExternalTaskStatus.Submitted)
                        {
                            model.TaskInstanceID = newTask;
                            this.SubmitTask(model);
                        }
                        return true;
                    }
                       

                }
            }
            return false;
        }
        private void SubmitTask(InternalExternalTaskModel model)
        {
            if(model != null)
            {
                var intExternalTask = internalExternalTaskManger.FindByTaskInstanceID(model.TaskInstanceID);
                if(intExternalTask !=null)
                {
                    var task = new TaskModel()
                    {
                        TaskInstanceId = model.TaskInstanceID,
                        SequenceId = 0,
                        AssignedBy = accountManager.GetUserById(intExternalTask.CreatedBy).UserName,
                        AssignedTo = model.UserTypeID == (int)HUDRoleSource.InternalUser ? model.SelectedUserName : "InLenderQueue",
                        StartTime = DateTime.UtcNow,
                        //MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(DateTime.UtcNow),
                        MyStartTime = DateTime.UtcNow,
                        Notes = model.AdditionalComments,
                        //TODO: Task step Id can be changed based on the logic
                        // currently I just reused the existing status 
                        // chage if the logic is different from the existing task step.
                        TaskStepId = (int)TaskStep.UnderReview,
                        PageTypeId = model.PageTypeID,
                        FHANumber = model.FHANumber
                    };
                    taskManager.SaveInternalExternalTask(task);
                }              
               
            }          
        }
        private void populatelookUps()
        {
            var pageType = new[]
           {
                 new {PageTypeID=(int)PageType.NotSpecified,PageType="Select Task Type"},
                 new {PageTypeID=(int)PageType.NOI,PageType=EnumType.GetEnumDescription(EnumType.Parse<PageType>(PageType.NOI.ToString()))},
                 //new {PageTypeID=(int)PageType.REAC,PageType = EnumType.GetEnumDescription(EnumType.Parse<PageType>(PageType.REAC.ToString()))},
                 //new {PageTypeID=(int)PageType.OTHER,PageType =EnumType.GetEnumDescription(EnumType.Parse<PageType>(PageType.OTHER.ToString()))}

            }.ToList();
            var userType = new[]
            {
                new {UserTypeID=(int)HUDRoleSource.NotSpecified,UserType="Select user type"},
                new {UserTypeID=(int)HUDRoleSource.InternalUser,UserType=EnumType.GetEnumDescription(EnumType.Parse<HUDRoleSource>(HUDRoleSource.InternalUser.ToString()))},
                 new {UserTypeID=(int)HUDRoleSource.ExternalUser,UserType=EnumType.GetEnumDescription(EnumType.Parse<HUDRoleSource>(HUDRoleSource.ExternalUser.ToString()))},
            }.ToList();
            ViewBag.PageTypeID = new SelectList(pageType, "PageTypeID", "PageType", 0);
            ViewBag.UserTypeID = new SelectList(userType, "UserTypeID", "UserType", 0);
            ViewBag.AvailableFHANumbersList = internalExternalTaskManger.GetAvailableFHANumbers();
            ViewBag.CommentVerbage = GetCommentVerbage();
            var userName = UserPrincipal.Current.UserName;
            ViewBag.IsLender = RoleManager.IsUserLenderRole(userName);
        }
        public JsonResult GetPropertyInfo(string fhaNumber)
        {
            AdditionalPropertyInfo propertyInfo = internalExternalTaskManger.GetPropertyInfoWithAEandWLM(fhaNumber);

            return Json(propertyInfo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUsersInRole(int roleId)
        {
            var roleName = (HUDRole)roleId;
            var users = RoleManager.GetUsersByRole(roleName.ToString());
            return Json(users.ToList(), JsonRequestBehavior.AllowGet);
        }
        private List<KeyValuePair<int, string>> GetCommentVerbage()
        {
            
           
               string noiVerbage = "Please find attached a Notice of Imposition sent to ORCF by CMS for the Section 232 facility named in the subject line. " +
                            " This project is in your portfolio, and it requires your immediate attention. " +
                           " Please review the attached notice and follow up with the servicing Lender as outlined in the CMS Imposition Review Punch List below." +
                           " Please use the CMS Imposition Review Punch List, the AE Reference Guide and the slides for CMS Training on Notices of Imposition to follow up with the servicing Lender concerning the impending impositions outlined in the notice." +
                           "Should you need assistance, please contact CMSSurveys@hud.gov.Please consult with your WLM if the Lender does not provide you a response within 5 business days." +
                            "Once the Lender submits them through the portal, you MUST inform the CMSSurveys@hud.gov mailbox of the Substantial Compliance date and provide a copy of the documentation received verifying that the facility has come back into Substantial Compliance.";
            string reacVerbage = "This project has been identified as possessing a REAC score below 60.  Please review and process per the 232 Healthcare Handbook.";
            var verbageKeyValue = new List<KeyValuePair<int, string>>();


            verbageKeyValue.Add( new KeyValuePair<int, string>((int)PageType.NOI, noiVerbage));
            verbageKeyValue.Add(new KeyValuePair<int, string> ((int) PageType.REAC, reacVerbage ));
         

            return verbageKeyValue;
        }
        [ValidateInput(false)]
        public ActionResult InternalExternalUpload(int uploadType, string Model)
        {
            
            ViewBag.uploadurl = "/InternalExternalTask/SaveDropzoneJsUploadedInternalExternal";
            string taskxrefId = string.Empty;
            
            //ViewBag.redirecturl = "/ProductionMyTask/GetTaskDetail?taskInstanceId=";
            //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();

            var folderuploadModel = new FolderUploadModel { FolderKey = uploadType, CreatedBy = UserPrincipal.Current.UserId, ParentModel = Model };
            return View("~/Views/InternalExternalTask/_DragDropFileUpload_InternExternal.cshtml", folderuploadModel);
        }

        [ValidateInput(false)]
        public bool SaveDropzoneJsUploadedInternalExternal(FolderUploadModel fileFolderModel)
        {
            bool isSavedSuccessfully = false;           
            var FolderKey = string.Empty;
            var ParentModel = string.Empty;
            Guid taskInstID = Guid.Empty;
            int pageTypeId;          
            if (Request.Form != null && Request.Form.Count > 0)
            {
                FolderKey = Request.Form["FolderKey"];
                ParentModel = Request.Form["ParentModel"];
                ParentModel = HttpUtility.HtmlDecode(ParentModel);
            }
            Guid updatedTaskInstanceID = Guid.Empty;
            var intExtModel = GetParentModelDictionary(ParentModel);
            if (intExtModel.Count > 0)
            {
               
                if (intExtModel.ContainsKey("PageTypeID") && !string.IsNullOrEmpty((string)intExtModel["PageTypeID"]) && intExtModel["FHANumber"] != null)
                {
                    pageTypeId = Convert.ToInt32((string)intExtModel["PageTypeID"]);
                    var fhaNumber = (string)intExtModel["FHANumber"];
                    InternalExternalTaskModel oldTask;
                    if (intExtModel.ContainsKey("TaskInstanceID") && !string.IsNullOrEmpty((string)intExtModel["TaskInstanceID"]))
                    {
                        taskInstID = Guid.Parse((string)intExtModel["TaskInstanceID"]);
                    }
                    if (taskInstID != Guid.Empty)
                    {
                        oldTask = internalExternalTaskManger.FindByTaskInstanceID(taskInstID);
                    }
                    else
                    {
                        oldTask = internalExternalTaskManger.GetNotCompletedTask(pageTypeId, fhaNumber);
                    }                   
                    // If task doesn't exist create new Internal External Task
                    if (oldTask == null)
                    {
                        var usertypeID = Convert.ToInt32((string)intExtModel["UserTypeID"]);
                        var assignedUserName = (string)intExtModel["AssignedToUserName"];
                        int assignedUserID = 0;
                        if (assignedUserName != null)
                        {
                            if(usertypeID == (int)HUDRoleSource.InternalUser)
                            {
                                var user = accountManager.GetUserByUsername(assignedUserName);
                                if(null != user)
                                {
                                    assignedUserID = user.UserID;
                                }
                            }
                        }
                        var internalExternalTaskModel = new InternalExternalTaskModel()
                        {
                            PageTypeID = pageTypeId,
                            FHANumber = fhaNumber,
                            //SurveyDate = intExtModel["SurveyDate"]!=null?Convert.ToDateTime((string)intExtModel["SurveyDate"]):null,
                            UserTypeID = usertypeID,
                            LenderID = (usertypeID == (int)(HUDRoleSource.ExternalUser))? Convert.ToInt32((string)intExtModel["LenderID"]):0,
                            CreatedDate = DateTime.UtcNow,
                            CreatedBy = UserPrincipal.Current.UserId,
                            AssignedToUserID = assignedUserID,
                            AdditionalComments = (string)intExtModel["AdditionalComments"],
                            StatusID = (int)InternalExternalTaskStatus.Draft,
                        };
                        if (intExtModel["SurveyDate"] != null && !string.IsNullOrEmpty((string)intExtModel["SurveyDate"]))
                            internalExternalTaskModel.SurveyDate = Convert.ToDateTime((string)intExtModel["SurveyDate"]);
                        var savedTaskInstanceID =internalExternalTaskManger.AddInternalExternalTask(internalExternalTaskModel);
                       if(savedTaskInstanceID != null)
                        {
                            updatedTaskInstanceID = savedTaskInstanceID;
                            internalExternalTaskModel.TaskInstanceID = savedTaskInstanceID;
                            var propertInfo = new AdditionalPropertyInfo()
                            {
                                PropertyId = Convert.ToInt32((string)intExtModel["PropertyInfo.PropertyId"]),
                                PropertyName = (string)intExtModel["PropertyInfo.PropertyName"]
                            };
                            internalExternalTaskModel.PropertyInfo = propertInfo;
                            isSavedSuccessfully = this.UploadDoc(internalExternalTaskModel, Int32.Parse(FolderKey));
                        }

                    }
                    else
                    {
                        var taskInstanceID = oldTask.TaskInstanceID;
                        updatedTaskInstanceID = taskInstanceID;
                        var propertInfo = new AdditionalPropertyInfo()
                        {
                            PropertyId = Convert.ToInt32((string)intExtModel["PropertyInfo.PropertyId"]),
                            PropertyName = (string)intExtModel["PropertyInfo.PropertyName"]
                        };
                        oldTask.PropertyInfo = propertInfo;
                        isSavedSuccessfully = this.UploadDoc(oldTask, Int32.Parse(FolderKey));

                    }


                }
            }
            Session["TaskInstanceID"] = updatedTaskInstanceID;
            return isSavedSuccessfully;  

           
        }
        private Dictionary<string, object> GetParentModelDictionary(string Model)
        {
            Dictionary<string, object> mydict = new Dictionary<string, object>();
            string[] reslts = Model.Split(new char[] { '|', '|', '|', '|' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string s in reslts)
            {
                var temp = JObject.Parse(s);

                if (!mydict.ContainsKey(((Newtonsoft.Json.Linq.JValue)(temp["name"])).Value.ToString()))
                {
                    mydict.Add(((Newtonsoft.Json.Linq.JValue)(temp["name"])).Value.ToString(), ((Newtonsoft.Json.Linq.JValue)(temp["value"])).Value.ToString());
                }

            }
            return mydict;
        }
        private bool UploadDoc(InternalExternalTaskModel intExternalModel ,int uploadType)
        {

            var uploadmodel = new RestfulWebApiUploadModel()
            {
                propertyID = intExternalModel.PropertyInfo.PropertyId.ToString(),
                indexType = "1",
                indexValue = intExternalModel.FHANumber,
                pdfConvertableValue = "false"
            };
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            //karri#162
            //request = WebApiTokenRequest.RequestToken();

            bool isUploaded = false;


            foreach (string Filename in Request.Files)
            {
                HttpPostedFileBase myFile = Request.Files[Filename];
                var t = myFile.InputStream;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    double fileSize = (myFile.ContentLength) / 1024;
                    Random randoms = new Random();

                    var UniqueId = randoms.Next(0, 99999);

                    var systemFileName = intExternalModel.FHANumber + "_" +
                                         intExternalModel.PageTypeID + "_" +
                                         intExternalModel.TaskInstanceID + "_" + UniqueId +
                                         Path.GetExtension(myFile.FileName);

                    string pathForSaving = Server.MapPath("~/Uploads");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        try
                        {

                            // myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            isUploaded = true;

                        }
                        catch (Exception ex)
                        {
                            //message = string.Format("File upload failed: {0}", ex.Message);
                        }
                    }
                    //karri#162
                    request = WebApiTokenRequest.RequestToken();

                    WebApiUploadResult = WebApiDocumentUpload.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");
                    System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));

                    if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                    {
                        var taskFile = ControllerHelper.PopulateTaskFileForInternalExternal(myFile.FileName, fileSize, ((FileType)uploadType).ToString(), DateTime.UtcNow.ToString(), systemFileName.ToString(), intExternalModel.TaskInstanceID);
                        taskFile.DocId = WebApiUploadResult.docId;
                        taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                        taskFile.API_upload_status = WebApiUploadResult.status;
                        taskFile.FileType = (FileType)uploadType;
                        taskManager.SaveGroupTaskFileModel(taskFile);
                    }
                    else
                    {
                        Exception ex = new Exception(WebApiUploadResult.message);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                        isUploaded = false;
                    }
                }
            }
            return isUploaded;
            }
            private bool CreateFolderIfNeeded(string path)
            {
                bool result = true;
                if (!Directory.Exists(path))
                {
                    try
                    {
                        Directory.CreateDirectory(path);
                    }
                    catch (Exception)
                    {
                        /*TODO: You must process this exception.*/
                        result = false;
                    }
                }
                return result;
            }
        public JsonResult GetUploadedDocuments(string sidx, string sord, int page, int rows,Guid taskInstanceID,FileType uploadType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
           // string userName="";
            var uploadedFilesByInstanceID = taskInstanceID==Guid.Empty ? new List<TaskFileModel>(): taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceID).ToList();
            var uploadedFiles = (from data in uploadedFilesByInstanceID
                                 //.Where(m=>m.FileType == uploadType)
                                 //let userName = internalExternalTaskManger.GetUserInfoById(data.CreatedBy).UserName
                                 let userName = internalExternalTaskManger.GetUserNameById(data.CreatedBy) //skumar-form290 - Name
                                 select new
                                 {
                                     TaskFileID = data.TaskFileId,
                                     TaskInstanceID = data.TaskInstanceId,
                                     data.FileType,
                                     //userName = internalExternalTaskManger.GetUserInfoById(data.CreatedBy).UserName,
                                     Name= userName,
                                     Role = RoleManager.GetUserRoles(userName).FirstOrDefault(),
                                     data.FileName,
                                     data.FileSize,
                                     data.UploadTime
                                 });
            int totalrecods = uploadedFiles.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var results = uploadedFiles.Skip(pageIndex * pageSize).Take(pageSize);



            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetTaskInstanceID()
        {
            var updatedTaskInstanceID = Session["TaskInstanceID"];
            return Json(updatedTaskInstanceID, JsonRequestBehavior.AllowGet);
        }

        }
}
