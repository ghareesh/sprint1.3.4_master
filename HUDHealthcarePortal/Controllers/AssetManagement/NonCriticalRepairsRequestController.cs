﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using HUDHealthCarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using System.IO;
using Model.Production;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using Model;
using Model.AssetManagement;
using BusinessService.ProjectAction;

namespace HUDHealthcarePortal.Controllers.AssetManagement
{
    public class NonCriticalRepairsRequestController : Controller
    {
        private IUploadDataManager uploadDataManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager;
        private IAccountManager accountManager;
        private ITaskManager taskManager;
        private IBackgroundJobMgr backgroundJobManager;
        private IEmailManager emailManager;
        private IRFORRANDNCR_GroupTasksManager RFORRANDNCR_GroupTasksManager;
        //Added by siddu @060120//
        private IProd_RestfulWebApiTokenRequest webApiTokenRequest;
        private IProd_RestfulWebApiDocumentUpload uploadApiManager;
        private IProd_RestfulWebApiDownload WebApiDownload;
        private IProd_RestfulWebApiDelete WebApiDelete;
        private IProjectActionFormManager _projectActionFormManager;
        public NonCriticalRepairsRequestController()
            : this(new UploadDataManager(), new NonCriticalRepairsRequestManager(),
                new AccountManager(), new TaskManager(), new BackgroundJobMgr(), new EmailManager(), new Prod_RestfulWebApiTokenRequest(), new Prod_RestfulWebApiDocumentUpload(), new Prod_RestfulWebApiDownload(), new Prod_RestfulWebApiDelete(),
                new RFORRANDNCR_GroupTasksManager(), new ProjectActionFormManager())
        {

        }

        private NonCriticalRepairsRequestController(IUploadDataManager uploadManager, INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
            IAccountManager _accountManager, ITaskManager _taskManager, IBackgroundJobMgr backgroundManager, IEmailManager _emailManager, IProd_RestfulWebApiTokenRequest _prod_WebApiTokenRequest, IProd_RestfulWebApiDocumentUpload _prod_RestfulWebApiDocumentUpload
            , IProd_RestfulWebApiDownload _WebApiDownload, IProd_RestfulWebApiDelete _WebApiDelete, IRFORRANDNCR_GroupTasksManager _IRFORRANDNCR_GroupTasksManager, IProjectActionFormManager projectActionFormManager)
        {
            uploadDataManager = uploadManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;
            accountManager = _accountManager;
            taskManager = _taskManager;
            backgroundJobManager = backgroundManager;
            emailManager = _emailManager;
            webApiTokenRequest = _prod_WebApiTokenRequest;
            uploadApiManager = _prod_RestfulWebApiDocumentUpload;
            WebApiDownload = _WebApiDownload;
            WebApiDelete = _WebApiDelete;
            RFORRANDNCR_GroupTasksManager = _IRFORRANDNCR_GroupTasksManager;
            _projectActionFormManager = projectActionFormManager;

        }


        [HttpGet]
        [AccessDeniedAuthorize(
            Roles =
                "LenderAccountManager,BackupAccountManager,LenderAccountRepresentative")]
        [MvcSiteMapNode(Title = "Non-Critical Repair Request Escrow Form", ParentKey = "AMId")]
        [SiteMapTitle("NonCritical", Target = AttributeTarget.CurrentNode)]
        public ActionResult Index()
        {
            var model = new NonCriticalRepairsViewModel();
            PopulateFHANumberList(model);
            GetDisclaimerTextNCR(model);
            model.LenderId = UserPrincipal.Current.UserData.LenderId.Value;
            model.LenderName = nonCriticalRepairsManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            model.SubmittedDate = DateTime.UtcNow;
            model.CreatedBy = UserPrincipal.Current.UserId;
            model.CreatedOn = DateTime.UtcNow;
            model.ModifiedOn = DateTime.UtcNow;
            model.SubmitByUserId = UserPrincipal.Current.UserId;
            // model.RulesCheckList = nonCriticalRepairsManager.GetNonCrticalRulesCheckList(FormType.Ncr);
            //model.RulesCheckList = nonCriticalRepairsManager.GetNonCrticalReferReasons( );

            TempData["FormAttachmentFileTypes"] =
                       ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();

            return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
        }

        public JsonResult GetPropertyInfo(string selectedFhaNumber)
        {
            var resultOne = nonCriticalRepairsManager.GetNonCriticalPropertyInfo(selectedFhaNumber);
            JsonResult json = null;
            if (resultOne != null)
            {
                var resultTwo = nonCriticalRepairsManager.GetNonCriticalStatusAndDrawInfo(resultOne, selectedFhaNumber);
                if (resultTwo != null)
                {
                    json = Json(resultTwo, JsonRequestBehavior.AllowGet);
                    if (TempData["NCREValid"] == null)
                    {
                        TempData["NCREValid"] = resultTwo.IsNCREValidForFHANumber;
                    }

                }
            }
            return json;
        }

        public ActionResult SubmitForm(NonCriticalRepairsViewModel model, int? sequenceId, byte[] concurrency, bool isAgreementAccepted)
        {

            IList<TaskFileModel> taskFile = new List<TaskFileModel>();
            if (ModelState.IsValid)
            {
                model.IsRequestAutoApproved = false;
                model.IsAgreementAccepted = isAgreementAccepted;

                //User Story 1901 
                //if (!model.IsAgreementAccepted)
                //{
                //    model.IsAgreementAccepted = true;
                //    PopulateFHANumberList(model);
                //    PopupHelper.ConfigPopup(TempData, 720, 300, "Disclaimer", false, false, true, false, false, true,
                //        "../NonCriticalRepairsRequest/NonCriticalCertifyPopUp");
                //    return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
                //}

                //check conditions for auto approval
                model.IsFile92117Uploaded = model.FileName92117 != null;
                model.IsFile92464Uploaded = model.FileName92464 != null;
                model.IsFilePCNAUploaded = model.FilePCNA != null;
                if (UserPrincipal.Current.LenderId != null) model.LenderId = (int)UserPrincipal.Current.LenderId;
                model.IsFileReqExtnUploaded = (model.IsExtensionAttachmentRequired && model.FileDefaultRequestExtension != null);
                //Auto approval only on Lender delegate - User Story 1810
                /*if (model.IsScopeOfWorkChanged || model.IsThisAnAdvance || !model.IsFile92117Uploaded || !model.IsFile92464Uploaded ||
                    (model.NonCriticalAccountBalance != 0 && model.PaymentAmountRequested > model.NCRECurrentBalance) ||
                    ((DateTime.Now - (DateTime)model.ClosingDate).Days > 365 && !model.IsFileReqExtnUploaded) ||
                    (model.NonCriticalAccountBalance < 1000000) ||
                    (model.TroubledCode == "T") || (model.PaymentAmountRequested > 50000) ||
                    (!model.IsNCREAmountValid || model.NonCriticalAccountBalance == 0))
                {
                    model.IsRequestAutoApproved = false;
                }*/
                model.SubmitByUserId = UserPrincipal.Current.UserId;
                model.SubmitToUserId = nonCriticalRepairsManager.GetAeUserIdByFhaNumber(model.FHANumber);
                model.CreatedBy = UserPrincipal.Current.UserId;
                model.CreatedOn = DateTime.UtcNow;
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;

                if (model.IsLenderDelegate == true)
                {
                    model.IsRequestAutoApproved = true;
                }

                if (model.IsScopeOfWorkChanged || model.IsThisAnAdvance || model.IsFinalDraw)
                {
                    model.IsRequestAutoApproved = false;
                }

                if (model.IsRequestAutoApproved)
                {
                    model.RequestStatus = (int)RequestStatus.AutoApprove;
                    TempData["RequestStatus"] = 4;
                }
                else
                {
                    model.RequestStatus = (int)RequestStatus.Submit;
                    TempData["RequestStatus"] = 1;
                }

                //if (model.IsLenderDelegate == false && (model.File9250 == null || model.File9250A == null))
                if (model.IsLenderDelegate == false && (model.FileName92464 == null) || model.FileName92117 == null)
                {
                    Session["Confirmation"] = "Please attach the required Evidential Information.";
                    PopulateFHANumberList(model);
                    model.IsAgreementAccepted = false;
                    return View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                }
                //if (model.IsSuspension == false && (model.File9250 == null || model.File9250A == null))
                if (model.IsAgreementAccepted == false && (model.FileName92117 == null) || model.FileName92117 == null)
                {
                    Session["Confirmation"] = "Please attach the required Evidential Information.";
                    PopulateFHANumberList(model);
                    model.IsAgreementAccepted = false;
                    return View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                }

                model.NonCriticalRequestId = Guid.NewGuid();
                //Insert the non critical request and get the generated id, insert into view model
                var nonCriticalRequestId = nonCriticalRepairsManager.SaveNonCriticalRepairsRequest(model);

                var propertyDetailsTxt = "";
                if (!string.IsNullOrEmpty(model.FHANumber))
                {

                    IReserveForReplacementManager reserveForReplacementManager = new ReserveForReplacementManager();
                    var propertyInfo = reserveForReplacementManager.GetPropertyInfo(model.FHANumber);
                    if (model.PropertyAddress != null)
                    {
                        if (model.PropertyAddress.AddressLine1 != null &&
                            model.PropertyAddress.AddressLine1 != propertyInfo.StreetAddress)
                        {
                            propertyDetailsTxt += "<p>Street Address: " + model.PropertyAddress.AddressLine1 + "</p>";
                        }
                        if (model.PropertyAddress.City != null &&
                            model.PropertyAddress.City != propertyInfo.City)
                        {
                            propertyDetailsTxt += "<p>City: " + model.PropertyAddress.City + "</p>";
                        }
                        if (model.PropertyAddress.StateCode != null &&
                            model.PropertyAddress.StateCode != propertyInfo.State)
                        {
                            propertyDetailsTxt += "<p>State: " + model.PropertyAddress.StateCode + "</p>";
                        }
                        if (model.PropertyAddress.ZIP != null &&
                            model.PropertyAddress.ZIP != propertyInfo.Zipcode)
                        {
                            propertyDetailsTxt += "<p>Zip: " + model.PropertyAddress.ZIP + "</p>";
                        }

                        if (!string.IsNullOrEmpty(propertyDetailsTxt))
                        {
                            model.IsUpdateiREMS = true;
                        }
                    }
                }

                var taskList = new List<TaskModel>();
                var task = new TaskModel();

                if (model.SubmitToUserId != 0)
                {
                    var toUser = accountManager.GetUserById(model.SubmitToUserId.Value);
                    task.AssignedTo = toUser.UserName;
                }
                else
                {
                    task.AssignedTo = nonCriticalRepairsManager.GetAeEmailByFhaNumber(model.FHANumber);
                }

                task.AssignedBy = UserPrincipal.Current.UserName;
                task.FHANumber = model.FHANumber;
                task.Notes = model.ServicerRemarks;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRepairsViewModel), Encoding.Unicode);
                task.SequenceId = 0;
                task.StartTime = DateTime.UtcNow;
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = model.TaskInstanceId ?? Guid.NewGuid();
                task.IsReAssigned = false;
                if (model.IsRequestAutoApproved)
                {
                    task.TaskStepId = (int)TaskStep.NonCriticalRepairComplete;
                }
                else
                {
                    task.TaskStepId = (int)TaskStep.NonCriticalRequest;
                }

                taskList.Add(task);

                // taskFile = UpdateTaskFileModelForTask(model, task);

                try
                {
                    if (model.IsUpdateiREMS)
                    {
                        //email notification to update iREMS
                        backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager, propertyDetailsTxt,
                            model.PropertyName, model.FHANumber);
                    }

                    // taskManager.SaveTask(taskList, taskFile);
                    taskManager.NcreSaveTask(taskList);
                    //Latest task id been updated on submission
                    model.TaskId = nonCriticalRepairsManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    nonCriticalRepairsManager.UpdateTaskId(model);
                    RFORRANDNCR_GroupTasksManager.DeleteGroupTask(model.TaskInstanceId.Value, model.FHANumber);

                    // email notification for auto approve

                    backgroundJobManager.SendNonCriticalRequestApprovalNotificationEmail(emailManager, task.AssignedTo, accountManager.GetUserById(model.SubmitByUserId.Value).UserName, model);


                    //return to mytasks
                    UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                    string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);



                    if (!model.IsDisplayAcceptPopup)
                    {
                        model.IsDisplayAcceptPopup = true;
                        PopulateFHANumberList(model);
                        PopupHelper.ConfigPopup(TempData, 720, 200, "Disclaimer", false, false, true, false, false, true,
                            "../NonCriticalRepairsRequest/NonCriticalConfirmationPopUp");
                        return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
                    }

                }
                catch
                {

                }
            }
            return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
        }


        [HttpPost]
        public ActionResult NonCriticalCertifyPopUp()
        {
            return PartialView("~/Views/AssetManagement/NonCriticalCertifyPopUp.cshtml");
        }


        [HttpPost]
        public ActionResult NonCriticalDisclaimerPopUp()
        {
            return PartialView("~/Views/AssetManagement/NonCriticalDisclaimerPopUp.cshtml");
        }

        [HttpPost]
        public ActionResult NonCriticalConfirmationPopUp()
        {
            return PartialView("~/Views/AssetManagement/NonCriticalConfirmationPopUp.cshtml");
        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetNonCriticalRepairsRequestFormDetail(bool isEditMode)
        {
            //Dictionary<string, string> fhaNumberList;
            var model = (NonCriticalRepairsViewModel)(TempData["NonCriticalRepairsViewModel"] ?? new NonCriticalRepairsViewModel());
            //  var model = CreateTempModel();
            model.RulesCheckList = nonCriticalRepairsManager.GetNonCrticalRulesCheckList(FormType.Ncr);
            model.ReferalReasons = nonCriticalRepairsManager.GetNonCrticalReferReasons(model.NonCriticalRequestId);

            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
            if (model != null)
            {
                model.IsTransactionLedger = nonCriticalRepairsManager.GetTransactionLedgerStatus(model.FHANumber);
                model.ManageMode = "ReadOnly";
                model.IsDisplayAcceptPopup = false;
                model.IsAgreementAccepted = false;
                if (isEditMode)
                {
                    if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) ||
                        RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName))
                    {
                        model.ManageMode = "Edit";
                        TempData["FormAttachmentFileTypes"] =
                            ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();
                        PopulateFHANumberList(model);
                        // store opened by user to TaskOpenStatus column
                        if (model.TaskOpenStatus != null &&
                            !model.TaskOpenStatus.TaskOpenStatus.Exists(
                                p => p.Key.Equals(UserPrincipal.Current.UserName)))
                        {
                            model.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        }
                        model.IsAgreementAccepted = true;
                        GetDisclaimerTextNCR(model);
                        return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", model);
                    }
                }
                else if (model.SequenceId.HasValue
                    && model.SequenceId < 2
                    && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName)
                    && !model.IsPAMReport
                    && RoleManager.IsUserLenderRole(model.AssignedTo))
                {
                    var task = new TaskModel();
                    var taskList = new List<TaskModel>();
                    task.AssignedBy = model.AssignedBy;
                    task.AssignedTo = model.AssignedTo;
                    task.StartTime = DateTime.UtcNow;
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                    task.TaskStepId = (int)TaskStep.NonCriticalRepairComplete;
                    task.TaskOpenStatus = XmlHelper.Serialize(model.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                    task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                    task.IsReAssigned = model.IsReassigned;
                    var taskFile = new TaskFileModel();
                    try
                    {
                        task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRepairsViewModel), Encoding.Unicode);
                        taskList.Add(task);
                        taskManager.SaveTask(taskList, taskFile);
                        model.TaskId = nonCriticalRepairsManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        nonCriticalRepairsManager.UpdateTaskId(model);
                    }
                    catch (Exception e)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                        throw new InvalidOperationException(
                            string.Format("Error Non critical form detail: {0}", e.InnerException), e.InnerException);
                    }
                }
            }
            model.IsAgreementAccepted = true;
            GetDisclaimerTextNCR(model);
            return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", model);
        }

        private void HandleConcurrentUpdateException(NonCriticalRepairsViewModel model)
        {
            model.IsConcurrentUserUpdateFound = true;
        }

        /// <summary>
        /// Approving the Non critcal request, AE only can do this now
        /// </summary>
        /// <param name="model"></param>
        /// <param name="taskGuid"></param>
        /// <param name="sequenceId"></param>
        /// <param name="concurrency"></param>
        /// <returns></returns>
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult ApproveOrDisApprove(NonCriticalRepairsViewModel model, int? sequenceId, byte[] concurrency)
        {

            if (ModelState.IsValid)
            {
                if (model.Decision == (int)RequestStatus.Approve)
                    TempData["RequestStatus"] = (int)RequestStatus.Approve;
                else if (model.Decision == (int)RequestStatus.ApproveWithChanges)
                    TempData["RequestStatus"] = (int)RequestStatus.ApproveWithChanges;
                else if (model.Decision == (int)RequestStatus.Deny)
                    TempData["RequestStatus"] = (int)RequestStatus.Deny;
                model.RequestStatus = model.Decision;

                if (model.RequestStatus == (int)RequestStatus.Approve ||
                    model.RequestStatus == (int)RequestStatus.ApproveWithChanges ||
                    model.RequestStatus == (int)RequestStatus.AutoApprove)
                {
                    model.IsTransactionLedger = true;
                }
                //if (!model.IsDisplayAcceptPopup)
                //{
                //    model.IsDisplayAcceptPopup = true;
                //  //  PopulateFHANumberNumberDrawList(model);
                //    PopupHelper.ConfigPopup(TempData, 720, 200, "Disclaimer", false, false, true, false, false, true,
                //        "../NonCriticalRepairsRequest/NonCriticalDisclaimerPopUp");
                //    ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
                //    return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", model);
                //}
                //added to retain pagination parameters
                ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
                TempData["routeparams"] = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
                //update to database
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                model.ApprovedDate = DateTime.UtcNow;

                //Approve is true, approval changes is false, we are setting differnt statuses
                //update the model
                nonCriticalRepairsManager.UpdateNonCriticalRepairsRequest(model);
                model.RulesCheckList = nonCriticalRepairsManager.GetNonCrticalRulesCheckList(FormType.Ncr);
                model.ReferalReasons = nonCriticalRepairsManager.GetNonCrticalReferReasons(model.NonCriticalRequestId);
                if (model.NonCriticalRequestId != Guid.Empty)
                {
                    var taskList = new List<TaskModel>();
                    // save task to finish
                    var task = new TaskModel();
                    var toEmail = model.AssignedBy;
                    task.AssignedBy = UserPrincipal.Current.UserName;
                    task.AssignedTo = model.AssignedBy;

                    task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRepairsViewModel), Encoding.Unicode);
                    task.Notes = model.Reason;
                    task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                                                                                      //task.StartTime = DateTime.UtcNow;
                                                                                      //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.StartTime = DateTime.UtcNow;
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                    task.Concurrency = concurrency;
                    task.TaskStepId = (int)TaskStep.NonCriticalRepairComplete;
                    var taskOpenStatusModel = new TaskOpenStatusModel();
                    taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                    task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                    task.IsReAssigned = model.IsReassigned;
                    taskList.Add(task);
                    //var taskFile = UpdateTaskFileModelForTask(model, task);
                    try
                    {
                        taskManager.NcreSaveTask(taskList);
                        //Latest task id been updated on Approve
                        model.TaskId = nonCriticalRepairsManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        nonCriticalRepairsManager.UpdateTaskId(model);
                        backgroundJobManager.SendNonCriticalRequestApprovalNotificationEmail(emailManager, task.AssignedBy, accountManager.GetUserById(model.SubmitByUserId.Value).UserName, model);
                        model.IsDisplayAcceptPopup = false;
                        model.IsAgreementAccepted = true;
                        GetDisclaimerTextNCR(model);
                        model.ManageMode = "ReadOnly";
                        //return View("~/Views/AssetManagement/NonCriticalRepairsAEandReadonly.cshtml", model);
                        return RedirectToAction("MyTasks", "Task");
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        HandleConcurrentUpdateException(model);
                    }
                }

            }

            model.IsAgreementAccepted = true;
            GetDisclaimerTextNCR(model);
            return View("~/Views/AssetManagement/NonCriticalRepairsRequestView.cshtml", model);
        }

        [HttpGet]
        public FileResult DownloadTaskFile1(Guid? taskInstanceId, FileType taskFileType)
        {
            TaskFileModel taskFile = taskManager.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId.Value, taskFileType);
            if (taskFile != null)
            {
                return File(taskFile.FileData, "text/text", taskFile.FileName);
            }
            return null;
        }

        private IList<TaskFileModel> UpdateTaskFileModelForTask(NonCriticalRepairsViewModel model, TaskModel task)
        {
            IList<TaskFileModel> listTaskModel = new List<TaskFileModel>();

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileName92464 == null && TempData["File92464"] != null)
            {
                model.File92464 = (HttpPostedFileBase)TempData["File92464"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileName92117 == null && TempData["FileName92117"] != null)
            {
                model.File92117 = (HttpPostedFileBase)TempData["FileName92117"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileScopeCertification == null && TempData["FileScopeCertification"] != null)
            {
                model.FileScopeCertification = (HttpPostedFileBase)TempData["FileScopeCertification"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileContract == null && TempData["FileContract"] != null)
            {
                model.FileContract = (HttpPostedFileBase)TempData["FileContract"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileDefaultRequestExtension == null && TempData["FileDefaultRequestExtension"] != null)
            {
                model.FileDefaultRequestExtension = (HttpPostedFileBase)TempData["FileDefaultRequestExtension"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.FileClearTitle == null && TempData["FileClearTitle"] != null)
            {
                model.FileClearTitle = (HttpPostedFileBase)TempData["FileClearTitle"];
            }

            // Get the file data.
            if (model.FileName92117 != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.File92117, FileType.BorrowerCertification92117, task, listTaskModel);
            }

            if (model.FilePCNA != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FilePCNA, FileType.PCNA, task, listTaskModel);
            }

            if (model.FileName92464 != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.File92464, FileType.ApprovalAdvance92464, task, listTaskModel);
            }

            if (model.FileScopeCertification != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FileScopeCertification, FileType.ScopeCertification, task, listTaskModel);
            }

            if (model.FileClearTitle != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FileClearTitle, FileType.ClearTitle, task, listTaskModel);
            }

            if (model.FileContract != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FileContract, FileType.FileContract, task, listTaskModel);
            }

            if (model.FileDefaultRequestExtension != null)
            {
                ControllerHelper.PopulateTaskFileModel(model.FileDefaultRequestExtension, FileType.DefaultRequestExtension, task, listTaskModel);
            }
            return listTaskModel;
        }


        private void PopulateFHANumberList(NonCriticalRepairsViewModel model)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                fhaNumberList =
                    nonCriticalRepairsManager.GetAllowedNonCriticalFhaByLenderUserId(
                        UserPrincipal.Current.UserId);
            }

            model.AvailableFHANumbersList = fhaNumberList;


        }

        public ActionResult GetTransactionLedger(int propertyId, int lenderId, string fhaNumber)
        {
            var model = nonCriticalRepairsManager.GetNonCrticalNonCrticalTransactions(propertyId, lenderId, fhaNumber);
            return PartialView("~/Views/AssetManagement/TransactionView.cshtml", model);
        }

        private void GetDisclaimerTextNCR(NonCriticalRepairsViewModel model)
        {

            model.DisclaimerText = nonCriticalRepairsManager.GetDisclaimerMsgByPageType("NCRE");


        }

        //Added by siddu @061020//
        [HttpPost]
        public string UploadFile(AttachFilesViewModel attachFilesViewModel, NonCriticalRepairsViewModel model, string UploadedFileSection)
        {
            // var TaskInstanceId = model.TaskGuid;
            string isUploaded = "";
            try
            {
                // model.ServicerRemarks = DateTime.UtcNow;
                PropertyInfoModel PropertyInfoModel = new PropertyInfoModel();
                var documentmodel = new AM_R4RAndNCREDocumentModel();//#868
                PropertyInfoModel = _projectActionFormManager.GetAssetManagementPropertyInfo(attachFilesViewModel.FHANumber);
                model.PropertyId = PropertyInfoModel.PropertyId;
                model.PropertyName = PropertyInfoModel.PropertyName;
                string FileSectionName = "";
                if (UploadedFileSection == "File92464")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.ApprovalAdvance92464.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;

                }
                else if (UploadedFileSection == "File92117")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.BorrowerCertification92117.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;
                }
                else if (UploadedFileSection == "FinalInspectionReport")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.ClearTitle.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;
                }
                else if (UploadedFileSection == "FileContractFileName")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.FileContract.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;
                }
                else if (UploadedFileSection == "FileScopeCertificationFileName")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.ScopeCertification.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;
                }
                model.IsRequestAutoApproved = false;
                //check conditions for auto approval
                model.IsFile92117Uploaded = model.File92117 != null;
                model.IsFile92464Uploaded = model.File92464 != null;
                model.IsFilePCNAUploaded = model.FilePCNA != null;
                model.ActionTypeId = 111;
                if (UserPrincipal.Current.LenderId != null) model.LenderId = (int)UserPrincipal.Current.LenderId;
                model.IsFileReqExtnUploaded = (model.IsExtensionAttachmentRequired && model.FileDefaultRequestExtension != null);

                var uploadmodel = new RestfulWebApiUploadModel()
                {
                    //propertyID = _projectActionFormManager.GetProdPropertyInfo(attachFilesViewModel.FHANumber).PropertyId.ToString(),
                    propertyID = model.PropertyId.ToString(),
                    indexType = "1",
                    indexValue = attachFilesViewModel.FHANumber,
                    pdfConvertableValue = "false",
                    folderKey = 0,
                    folderNames = "/" + PropertyInfoModel.PropertyId.ToString() + "/" + attachFilesViewModel.FHANumber + "/" + "Asset Management /" + "NCRE" + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy"),//#868
                    transactionType = model.ActionTypeId.ToString(),//#868
                    transactionStatus = "",//#868
                    transactionDate = DateTime.UtcNow.ToString("MM-dd-yyyy"),//#868
                    documentType = documentmodel.TransactionDocumentId.ToString()//#868
                                                                                 // folderNames = model.PropertyId.ToString() + "/" + model.FHANumber + "/Asset Management/NonCriticalRepairsRequest/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy") + "/" + FileSectionName,
                };
                var WebApiUploadResult = new RestfulWebApiResultModel();
                var request = new RestfulWebApiTokenResultModel();
                foreach (string Filename in Request.Files)
                {
                    HttpPostedFileBase myFile = Request.Files[Filename];
                    if (myFile != null && myFile.ContentLength != 0)
                    {
                        double fileSize = (myFile.ContentLength) / 1024;
                        Random randoms = new Random();
                        var UniqueId = randoms.Next(0, 99999);

                        var systemFileName = model.FHANumber + "_" +
                                             model.TaskInstanceId + "_" + UniqueId +
                                             Path.GetExtension(myFile.FileName);
                        string pathForSaving = Server.MapPath("~/Uploads");
                        if (this.CreateFolderIfNeeded(pathForSaving))
                        {
                            try
                            {
                                myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        request = webApiTokenRequest.RequestToken();
                        WebApiUploadResult = uploadApiManager.AssetManagementR4RAndNCREUploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");


                        if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                        {
                            System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));// commented because for safety purpose we are not deleting files from physical folder
                            /* Creting entry in Group Task and R4R Tables*/
                            //var grouptaskmodel = nonCriticalRepairsManager.GetLatestTaskId(model.TaskInstanceId.Value);
                            var grouptaskmodel = RFORRANDNCR_GroupTasksManager.GetGroupTaskAByTaskInstanceId(model.TaskInstanceId.Value);
                            if (grouptaskmodel == null)
                            {
                                var prodgroupTaskModel = new RFORRANDNCR_GroupTaskModel();
                                //prodgroupTaskModel.TaskInstanceId = Guid.NewGuid();
                                prodgroupTaskModel.TaskInstanceId = model.TaskInstanceId.Value;
                                prodgroupTaskModel.FHANumber = model.FHANumber;
                                prodgroupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                                prodgroupTaskModel.ModifiedOn = DateTime.UtcNow;
                                prodgroupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                                prodgroupTaskModel.CreatedOn = DateTime.UtcNow;
                                prodgroupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                                prodgroupTaskModel.ServicerComments = model.ServicerRemarks;
                                //prodgroupTaskModel.ServicerComments = parentdict["ServicerComments"] != null ? (string)parentdict["ServicerComments"] : "";
                                // var ProdGroupTaskId = nonCriticalRepairsManager.(prodgroupTaskModel);
                                //  model.TaskGuid = ProdGroupTaskId;
                                var ProdGroupTaskId = RFORRANDNCR_GroupTasksManager.AddR4RGroupTasks(prodgroupTaskModel);
                                model.TaskInstanceId = ProdGroupTaskId;
                            }
                            else
                            {
                                
                            }
                            /*End*/
                            var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
                         DateTime.UtcNow.ToString(), systemFileName, model.TaskInstanceId.Value);
                            taskFile.GroupFileType = FileSectionName;
                            taskFile.DocId = WebApiUploadResult.docId;
                            taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                            taskFile.API_upload_status = WebApiUploadResult.status;
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                            taskManager.SaveGroupTaskFileModel(taskFile);
                            isUploaded = "Success";
                        }

                        else
                        {
                            Exception ex = new Exception(WebApiUploadResult.message);
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                            if (!string.IsNullOrEmpty(WebApiUploadResult.message))
                            {
                                isUploaded = WebApiUploadResult.message;
                            }
                            else
                            {
                                isUploaded = "failed";
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                //throw;
            }

            return isUploaded;
        }
        //Added by siddu @060120//
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return true;
        }
        //Added by siddu @071020//
        public string DeleteFile(string FHANumber, Guid TaskInstanceId, string FileSectionName)
        {
            string APIStatus = "";
            try
            {
                RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
                RestfulWebApiResultModel APIresult = new RestfulWebApiResultModel();
                RestfulWebApiUpdateModel UpdateInfo = new RestfulWebApiUpdateModel();
                if (FileSectionName == "File92464")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.ApprovalAdvance92464.ToString()).ToString();
                }
                else if (FileSectionName == "File92117")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.BorrowerCertification92117.ToString()).ToString();
                }
                else if (FileSectionName == "FinalInspectionReport")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.ClearTitle.ToString()).ToString();
                }
                else if (FileSectionName == "FileContractFileName")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.FileContract.ToString()).ToString();
                }
                else if (FileSectionName == "FileScopeCertificationFileName")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.ScopeCertification.ToString()).ToString();
                }
                TaskFileModel taskFile = taskManager.GetTaskFileByTaskInstanceAndFileType(TaskInstanceId, FileSectionName);
                var success = 0;
                if (taskFile != null)
                {
                    //if (UserPrincipal.Current.UserId == taskFile.CreatedBy || (UserPrincipal.Current.UserRole == "BackupAccountManager" || UserPrincipal.Current.UserRole == "LenderAccountManager"))
                    //{

                    if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                    {
                        request = webApiTokenRequest.RequestToken();
                        APIresult = WebApiDelete.DeleteDocumentUsingWebApi(taskFile.DocId, request.access_token);
                        if (!string.IsNullOrEmpty(APIresult.status) && APIresult.key == "200")
                        {
                            success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFile.FileId));
                            if (success == 1)
                                APIStatus = "Success";
                        }
                    }
                    //else
                    //{              
                    //    return "unauthorized";
                    //}                                     
                }
            }
            catch (Exception ex)
            {
                APIStatus = "failed";
            }
            return APIStatus;
        }


        //Added by siddu 100120//

        [HttpGet]
        public FileResult DownloadTaskFile(Guid? taskInstanceId, FileType taskFileType)
        {

            TaskFileModel taskFile = taskManager.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId.Value, taskFileType);
            if (taskFile != null)
            {
                if (taskFile.DocId == null && taskFile.API_upload_status == null)
                {
                    return File(taskFile.FileData, "text/text", taskFile.FileName);
                }
                else
                {
                    string JsonSteing = "";
                    RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
                    //reviewFileStatusManager.UpdateReviewFileStatusForFileDownload(taskFile.TaskFileId);
                    string filePath = "";
                    FileInfo fileInfo = null;
                    filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                    fileInfo = new System.IO.FileInfo(filePath);
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                    if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                    {
                        request = webApiTokenRequest.RequestToken();
                        JsonSteing = "{\"docId\":" + taskFile.DocId + ",\"version\":" + taskFile.Version + "}";
                        Stream streamResult = WebApiDownload.DownloadDocumentUsingWebApi(JsonSteing, request.access_token, taskFile.FileName);
                        return new FileStreamResult(streamResult, "application/octet-stream");
                    }

                    else
                    {
                        filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                        fileInfo = new System.IO.FileInfo(filePath);
                        Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                        Response.WriteFile(filePath);
                    }
                    Response.End();

                }
            }

            return null;
        }


    }
}
