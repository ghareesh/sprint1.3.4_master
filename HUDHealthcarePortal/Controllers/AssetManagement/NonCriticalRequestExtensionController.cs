﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using HUDHealthCarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using System.IO;

namespace HUDHealthcarePortal.Controllers.AssetManagement
{
    public class NonCriticalRequestExtensionController : Controller
    {
        private IUploadDataManager uploadDataManager;
        private INonCriticalRepairsRequestManager nonCriticalRepairsManager;
        private INonCriticalRequestExtensionManager nonCriticalRequestExtensionManager;
        private IAccountManager accountManager;
        private ITaskManager taskManager;
        private IBackgroundJobMgr backgroundJobManager;
        private IEmailManager emailManager;


        public NonCriticalRequestExtensionController()
            : this(new UploadDataManager(), new NonCriticalRepairsRequestManager(),
                new NonCriticalRequestExtensionManager(),
                new AccountManager(), new TaskManager(), new BackgroundJobMgr(), new EmailManager())
        {

        }

        private NonCriticalRequestExtensionController(IUploadDataManager uploadManager,
            INonCriticalRepairsRequestManager _nonCriticalRepairsManager,
            NonCriticalRequestExtensionManager _nonCriticalRequestExtensionManager,
            IAccountManager _accountManager, ITaskManager _taskManager, IBackgroundJobMgr backgroundManager,
            IEmailManager _emailManager)
        {
            uploadDataManager = uploadManager;
            nonCriticalRepairsManager = _nonCriticalRepairsManager;
            accountManager = _accountManager;
            taskManager = _taskManager;
            backgroundJobManager = backgroundManager;
            emailManager = _emailManager;
            nonCriticalRequestExtensionManager = _nonCriticalRequestExtensionManager;
        }

        [MvcSiteMapNode(Title = "Non-Critical Repair Request Escrow Extension Form", ParentKey = "AMId")]
        [SiteMapTitle("NonCriticalExtension", Target = AttributeTarget.CurrentNode)]
        public ActionResult Index()
        {
            var model = new NonCriticalRequestExtensionModel();
            model.ViewName = "NCRMenuView";
            model.LenderName = nonCriticalRepairsManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            PopulateFHANumberNumberDrawList(model);
            return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
        }

        public ActionResult GetNCRExtensionForm(string fhaNumber)
        {
            var model = new NonCriticalRequestExtensionModel();
            var result = nonCriticalRequestExtensionManager.GetNonCriticalPropertyInfo(fhaNumber);
            model.PropertyID = result.PropertyId; 
            model.FHANumber = fhaNumber;
            model.ClosingDate = Convert.ToDateTime(result.ClosingDate);
            if (string.IsNullOrEmpty(result.ExtensionApprovalDate))
            {
                model.EndDate = Convert.ToDateTime(result.ClosingDate).AddMonths(18);
                model.IsDefaultExention = true;
            }
            else
            {
                model.EndDate = Convert.ToDateTime(result.EndingDate).AddMonths(6);
                model.IsDefaultExention = false;
            }
            model.IsAnyPendingExtensionRequestExist = result.IsAnyPendingExtensionRequestExist; 
            model.PropertyName = result.PropertyName;
            model.ViewName = "NCRView";
            model.LenderName = nonCriticalRepairsManager.GetLenderName(UserPrincipal.Current.UserData.LenderId.Value);
            return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
        }

        public JsonResult GetPropertyInfo(string selectedFhaNumber)
        {
            var result = nonCriticalRequestExtensionManager.GetNonCriticalPropertyInfo(selectedFhaNumber);          
            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        private void PopulateFHANumberNumberDrawList(NonCriticalRequestExtensionModel model)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                fhaNumberList =
                    nonCriticalRequestExtensionManager.GetAllowedNonCriticalFhaByLenderUserId(
                        UserPrincipal.Current.UserId);
            }

            model.AvailableFHANumbersList = fhaNumberList;

        }


        public ActionResult SubmitForm(NonCriticalRequestExtensionModel model, Guid? taskGuid, int? sequenceId,
            byte[] concurrency)
        {
            if (ModelState.IsValid)
            {
                var comments = model.Comments;
                model.Comments = null;
                model.LenderID = UserPrincipal.Current.UserData.LenderId.Value;
                model.ExtensionPeriod = 6;
                model.CreatedBy = UserPrincipal.Current.UserId;
                model.CreatedOn = DateTime.UtcNow;
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                
                model.ExtensionRequestStatus = (int) RequestExtensionStatus.Submit; //submitted
                model.NonCriticalRequestExtensionId = Guid.NewGuid();
                var nonCriticalRequestExtensionId = nonCriticalRequestExtensionManager.SaveNonCriticalRequestExtension(model); var taskList = new List<TaskModel>();
                var task = new TaskModel();
                var submitToUserId = nonCriticalRepairsManager.GetAeUserIdByFhaNumber(model.FHANumber);
                if (submitToUserId != 0)
                {
                    var toUser = accountManager.GetUserById(submitToUserId);
                    task.AssignedTo = toUser.UserName;
                }
                else
                {
                    task.AssignedTo = nonCriticalRepairsManager.GetAeEmailByFhaNumber(model.FHANumber);
                }

                var propertyDetailsTxt = "";
                if (!string.IsNullOrEmpty(model.FHANumber))
                {

                    IReserveForReplacementManager reserveForReplacementManager = new ReserveForReplacementManager();
                    var propertyInfo = reserveForReplacementManager.GetPropertyInfo(model.FHANumber);
                    if (model.PropertyAddress != null)
                    {
                        if (model.PropertyAddress.AddressLine1 != null &&
                            model.PropertyAddress.AddressLine1 != propertyInfo.StreetAddress)
                        {
                            propertyDetailsTxt += "<p>Street Address: " + model.PropertyAddress.AddressLine1 + "</p>";
                        }
                        if (model.PropertyAddress.City != null &&
                            model.PropertyAddress.City != propertyInfo.City)
                        {
                            propertyDetailsTxt += "<p>City: " + model.PropertyAddress.City + "</p>";
                        }
                        if (model.PropertyAddress.StateCode != null &&
                            model.PropertyAddress.StateCode != propertyInfo.State)
                        {
                            propertyDetailsTxt += "<p>State: " + model.PropertyAddress.StateCode + "</p>";
                        }
                        if (model.PropertyAddress.ZIP != null &&
                            model.PropertyAddress.ZIP != propertyInfo.Zipcode)
                        {
                            propertyDetailsTxt += "<p>Zip: " + model.PropertyAddress.ZIP + "</p>";
                        }

                        if (!string.IsNullOrEmpty(propertyDetailsTxt))
                        {
                            model.IsUpdateiREMS = true;
                        }
                    }
                }

                task.AssignedBy = UserPrincipal.Current.UserName;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRequestExtensionModel), Encoding.Unicode);
                task.Notes = comments;
                task.SequenceId = 0;
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = Guid.NewGuid();
                task.TaskStepId = (int)TaskStep.NonCriticalRequestExtension;
                task.IsReAssigned = model.IsReassigned;
                  taskList.Add(task);

                try
                {
                    if (model.IsUpdateiREMS)
                    {
                        //email notification to update iREMS
                        backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager, propertyDetailsTxt,
                            model.PropertyName, model.FHANumber);
                    }

                    TaskFileModel taskFile = new TaskFileModel();
                    taskManager.SaveTask(taskList, taskFile);
                    //Latest task id been updated on submission
                    model.TaskId = nonCriticalRequestExtensionManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    nonCriticalRequestExtensionManager.UpdateTaskId(model);

                    // email notification for submission
                    model.Comments = comments;
                    backgroundJobManager.SendNonCriticalRequestExtensionApprovalNotificationEmail(emailManager, task.AssignedTo, task.AssignedBy, model);
                    UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                    string myTaskUrl = u.AbsoluteAction("MyTasks", "Task", null);

                    if (!model.IsDisplayAcceptPopup)
                    {
                        TempData["RequestExtensionStatus"] = 1;
                        model.IsDisplayAcceptPopup = true;
                        PopulateFHANumberNumberDrawList(model);
                        PopupHelper.ConfigPopup(TempData, 520, 200, "Disclaimer", false, false, true, false, false, true,
                            "../NonCriticalRequestExtension/ConfirmPopup");
                        model.ViewName = string.Empty;
                        model.ManageMode = "ReadOnly";
                        return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
                    }
                }
                catch
                {
                }
            }
            model.ViewName = string.Empty;
            model.ManageMode = "ReadOnly";
            return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
        }

        [HttpPost]
        public ActionResult ConfirmPopup()
        {
            return PartialView("~/Views/AssetManagement/ConfirmPopup.cshtml");
        }


        [HttpGet]
        public ActionResult GetNonCriticalRequestExtensionFormDetail(bool isEditMode)
        {
            return GetNonCriticalRequestExtensionFormDetailHelper(isEditMode);
        }

        public ActionResult GetNonCriticalRequestExtensionFormDetailReAssigned(bool isEditMode)
        {
            return GetNonCriticalRequestExtensionFormDetailHelper(isEditMode);
        }

        private ActionResult GetNonCriticalRequestExtensionFormDetailHelper(bool isEditMode)
        {
            //Dictionary<string, string> fhaNumberList;
            var model = (NonCriticalRequestExtensionModel)(TempData["NonCriticalRequestExtensionModel"] ?? new NonCriticalRequestExtensionModel());

            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
            if (model != null)
            {
                model.ManageMode = "ReadOnly";
                model.ViewName = "";
                model.IsDisplayAcceptPopup = false;

                if (isEditMode)
                {
                    if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) || RoleManager.IsInternalSpecialOptionUser(UserPrincipal.Current.UserName))
                    {
                        model.ManageMode = "Edit";
                        model.ViewName = "AEView";
                        TempData["FormAttachmentFileTypes"] =
                            ConfigurationManager.AppSettings["FormAttachmentFileTypes"].ToString();

                        PopulateFHANumberNumberDrawList(model);

                        // store opened by user to TaskOpenStatus column
                        if (model.TaskOpenStatus != null &&
                            !model.TaskOpenStatus.TaskOpenStatus.Exists(
                                p => p.Key.Equals(UserPrincipal.Current.UserName)))
                        {
                            model.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);

                        }
                        return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
                    }
                    else
                    {
                       
                    }
                }
                else if( model.SequenceId.HasValue
                    && model.SequenceId < 2 
                    && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName)
                    && !model.IsPAMReport
                    && RoleManager.IsUserLenderRole(model.AssignedTo))
                    {
                        var task = new TaskModel();
                        var taskList = new List<TaskModel>();
                        task.AssignedBy = model.AssignedBy;
                        task.AssignedTo = model.AssignedTo;
                        task.IsReAssigned = model.IsReassigned;
                        task.StartTime = DateTime.UtcNow;
                    //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                        task.TaskStepId = (int)TaskStep.NonCriticalRequestExtensionComplete;
                        task.TaskOpenStatus = XmlHelper.Serialize(model.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                        task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                        var taskFile = new TaskFileModel();
                        try
                        {
                            task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRequestExtensionModel), Encoding.Unicode);
                            taskList.Add(task);
                            taskManager.SaveTask(taskList, taskFile);
                            model.TaskId = nonCriticalRepairsManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                            nonCriticalRequestExtensionManager.UpdateTaskId(model);
                        }
                        catch (Exception e)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                            throw new InvalidOperationException(
                                string.Format("Error Non critical form detail: {0}", e.InnerException), e.InnerException);
                        }
                    }
                }
            
            return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
        }

        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult Approve(NonCriticalRequestExtensionModel model, Guid? taskGuid, int? sequenceId, byte[] concurrency)
        {

            if (ModelState.IsValid)
            { 
                if (!model.IsDisplayAcceptPopup)
                {
                    model.ViewName = "AEView";
                    if (model.IsApprove == true)
                        TempData["RequestExtensionStatus"] = (int)RequestExtensionStatus.Approve;
                    else
                        TempData["RequestExtensionStatus"] = (int)RequestExtensionStatus.Deny;

                    model.IsDisplayAcceptPopup = true;
                    //  PopulateFHANumberNumberDrawList(model);
                    PopupHelper.ConfigPopup(TempData, 520, 200, "Disclaimer", false, false, true, false, false, true,
                        "../NonCriticalRequestExtension/ConfirmPopUp");
                    ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
                    return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
                }
                //added to retain pagination parameters
                ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
                TempData["routeparams"] = SessionHelper.SessionGet<System.Web.Routing.RouteValueDictionary>(SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
                //update to database
                model.ModifiedBy = UserPrincipal.Current.UserId;
                model.ModifiedOn = DateTime.UtcNow;
                //Approve is true, approval changes is false, we are setting differnt statuses
                if (model.IsApprove == true)
                    model.ExtensionRequestStatus = (int)RequestExtensionStatus.Approve;
                else
                    model.ExtensionRequestStatus = (int)RequestExtensionStatus.Deny;
                model.ExtensionPeriod = 6;
                
                //update the model
                nonCriticalRequestExtensionManager.UpdateNonCriticalRequestExtension(model);

                var taskList = new List<TaskModel>();
                // save task to finish
                var task = new TaskModel();
                var toEmail = model.AssignedBy;
                task.AssignedBy = UserPrincipal.Current.UserName;
                task.AssignedTo = model.AssignedBy;
                task.IsReAssigned = model.IsReassigned;
                task.DataStore1 = XmlHelper.Serialize(model, typeof(NonCriticalRequestExtensionModel), Encoding.Unicode);
                task.Notes = model.Comments;
                task.SequenceId = sequenceId.HasValue ? sequenceId.Value + 1 : 0; // incrementing from 0
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                task.TaskInstanceId = taskGuid ?? Guid.NewGuid();
                task.Concurrency = concurrency;
                task.TaskStepId = (int)TaskStep.NonCriticalRequestExtensionComplete;
                var taskOpenStatusModel = new TaskOpenStatusModel();
                taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);
                taskList.Add(task);
                try
                {
                    taskManager.SaveTask(taskList, new TaskFileModel());
                    //Latest task id been updated on Approve
                    model.TaskId = nonCriticalRequestExtensionManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    nonCriticalRequestExtensionManager.UpdateTaskId(model);

                    backgroundJobManager.SendNonCriticalRequestExtensionApprovalNotificationEmail(emailManager, task.AssignedBy, task.AssignedTo, model);
                    
                    model.IsDisplayAcceptPopup = false;
                    model.ManageMode = "ReadOnly";
                    return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
                }
                catch (DbUpdateConcurrencyException)
                {
                    HandleConcurrentUpdateException(model);
                }


            }

            return View("~/Views/AssetManagement/NonCriticalRequestExtension.cshtml", model);
        }

        private void HandleConcurrentUpdateException(NonCriticalRequestExtensionModel model)
        {
            model.IsConcurrentUserUpdateFound = true;
        }

    }
}
