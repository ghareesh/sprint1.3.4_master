﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Xml.Linq;
using BusinessService.AssetManagement;
using BusinessService.Interfaces;
using Core;
using Elmah;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.ExceptionHandling;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Helpers;
using HUDHealthCarePortal.Helpers;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using Model.AssetManagement;
using Rotativa;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Model.Production;
using BusinessService.ProjectAction;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using Model.ProjectAction;
using Model;

namespace HUDHealthcarePortal.Controllers.AssetManagement
{
    public class ReserveForReplacementController : Controller
    {
        private IUploadDataManager uploadDataManager;
        private IReserveForReplacementManager reserveForReplacementManager;
        private IAccountManager accountManager;
        private ITaskManager taskManager;
        private IBackgroundJobMgr backgroundJobManager;
        private IEmailManager emailManager;
        BizRuleManager bizRuleManager = new BizRuleManager();
        private const int NoOfDaysLimitForPreviousR4RRequest = 90;
        private IProjectActionFormManager _projectActionFormManager;
        private IProd_RestfulWebApiTokenRequest webApiTokenRequest;
        private IProd_RestfulWebApiDocumentUpload uploadApiManager;
        private IRFORRANDNCR_GroupTasksManager RFORRANDNCR_GroupTasksManager;
        private IProd_RestfulWebApiDownload WebApiDownload;
        private IProd_RestfulWebApiDelete WebApiDelete;
        private string DefaultDocID = ConfigurationManager.AppSettings["DefaultDocId"].ToString();
        private ITaskFile_FolderMappingManager taskFile_FolderMappingManager;
        private IProd_TaskXrefManager prod_TaskXrefManager;
        public ReserveForReplacementController()
            : this(new UploadDataManager(), new ReserveForReplacementManager(), new AccountManager(), new TaskManager()
                  , new BackgroundJobMgr(), new EmailManager(), new ProjectActionFormManager(), new Prod_RestfulWebApiTokenRequest()
                  , new Prod_RestfulWebApiDocumentUpload(), new RFORRANDNCR_GroupTasksManager(), new Prod_RestfulWebApiDownload()
                  , new TaskFile_FolderMappingManager(), new Prod_TaskXrefManager(), new Prod_RestfulWebApiDelete())
        {
        }

        private ReserveForReplacementController(IUploadDataManager uploadManager, IReserveForReplacementManager r4rManager,
            IAccountManager _accountManager, ITaskManager _taskManager, IBackgroundJobMgr backgroundManager, IEmailManager _emailManager,
            IProjectActionFormManager projectActionFormManager, IProd_RestfulWebApiTokenRequest _prod_WebApiTokenRequest, IProd_RestfulWebApiDocumentUpload _prod_RestfulWebApiDocumentUpload
            , IRFORRANDNCR_GroupTasksManager _IRFORRANDNCR_GroupTasksManager, IProd_RestfulWebApiDownload _WebApiDownload
            , ITaskFile_FolderMappingManager _taskFile_FolderMappingManager, IProd_TaskXrefManager _prodTaskXrefManager, IProd_RestfulWebApiDelete _WebApiDelete)
        {
            uploadDataManager = uploadManager;
            reserveForReplacementManager = r4rManager;
            accountManager = _accountManager;
            taskManager = _taskManager;
            backgroundJobManager = backgroundManager;
            emailManager = _emailManager;
            _projectActionFormManager = projectActionFormManager;
            webApiTokenRequest = _prod_WebApiTokenRequest;
            uploadApiManager = _prod_RestfulWebApiDocumentUpload;
            RFORRANDNCR_GroupTasksManager = _IRFORRANDNCR_GroupTasksManager;
            WebApiDownload = _WebApiDownload;
            taskFile_FolderMappingManager = _taskFile_FolderMappingManager;
            prod_TaskXrefManager = _prodTaskXrefManager;
            WebApiDelete = _WebApiDelete;
        }

        [HttpGet]
        [AccessDeniedAuthorize(
            Roles = "LenderAccountManager,BackupAccountManager,LenderAccountRepresentative,Servicer")]
        [MvcSiteMapNode(Title = "Reserve For Replacement Submission Form", ParentKey = "AMId")]
        [SiteMapTitle("ReserveForReplacement", Target = AttributeTarget.CurrentNode)]
        public ActionResult Index()
        {
            var model = new ReserveForReplacementFormModel();
            GetDisclaimerText(model);
            model.IsAgreementAccepted = false;
            model.IsFhaNumberMissing = false;
            model.IsSuspension = false;
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                var lenderID = (int)UserPrincipal.Current.LenderId;
                model.ServicerSubmissionDate = DateTime.Today;
                model.IsLoggedInUserLender = true;
                model.IsCommentEntered = true;
                FHAListExceptSubmited(model, lenderID);

            }
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            model.TransAccessStatus = checkResult.status;
            TempData["FormAttachmentFileTypes"] = ConfigurationManager.AppSettings["FormAttachmentFileTypes"];

            return View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
        }
        //#873 harish Added 08052020
        public string DeleteFile(string FHANumber, Guid TaskInstanceId, string FileSectionName)
        {
            string APIStatus = "";
            try
            {
                RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
                RestfulWebApiResultModel APIresult = new RestfulWebApiResultModel();
                RestfulWebApiUpdateModel UpdateInfo = new RestfulWebApiUpdateModel();
                if (FileSectionName == "File9250")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.File9250.ToString()).ToString();
                }
                else if (FileSectionName == "File9250A")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.File9250A.ToString()).ToString();
                }
                else if (FileSectionName == "InvoiceFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Invoice.ToString()).ToString();
                }
                else if (FileSectionName == "ReceiptFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Receipt.ToString()).ToString();
                }
                else if (FileSectionName == "ContractFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Contract.ToString()).ToString();
                }
                else if (FileSectionName == "PictureFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Picture.ToString()).ToString();
                }
                else if (FileSectionName == "OtherFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Other.ToString()).ToString();
                }
                TaskFileModel taskFile = taskManager.GetTaskFileByTaskInstanceAndFileType(TaskInstanceId, FileSectionName);
                var success = 0;
                if (taskFile != null)
                {
                    //if (UserPrincipal.Current.UserId == taskFile.CreatedBy || (UserPrincipal.Current.UserRole == "BackupAccountManager" || UserPrincipal.Current.UserRole == "LenderAccountManager"))
                    //{

                    if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                    {
                        request = webApiTokenRequest.RequestToken();
                        APIresult = WebApiDelete.DeleteDocumentUsingWebApi(taskFile.DocId, request.access_token);
                        if (!string.IsNullOrEmpty(APIresult.status) && APIresult.key == "200")
                        {
                            success = taskManager.DeleteTaskFileByFileID(Convert.ToInt32(taskFile.FileId));
                            if (success == 1)
                                APIStatus = "Success";
                        }
                    }
                    //else
                    //{              
                    //    return "unauthorized";
                    //}                                     
                }
            }
            catch (Exception ex)
            {
                APIStatus = "failed";
            }
            return APIStatus;
        }

        [HttpPost]
        public string UploadFile(AttachFilesViewModel attachFilesViewModel, ReserveForReplacementFormModel model, string UploadedFileSection)
        {
            string isUploaded = "";
            try
            {
                var documentmodel = new AM_R4RAndNCREDocumentModel();//#868

                model.ServicerSubmissionDate = DateTime.UtcNow;
               // model.ServicerSubmissionDate = ControllerHelper.GetClientDateTime();
                PropertyInfoModel PropertyInfoModel = new PropertyInfoModel();
                PropertyInfoModel = _projectActionFormManager.GetAssetManagementPropertyInfo(attachFilesViewModel.FHANumber);
                model.PropertyId = PropertyInfoModel.PropertyId;
                model.PropertyName = PropertyInfoModel.PropertyName;
                model.ActionTypeId = 86;
                string FileSectionName = "";
                if (UploadedFileSection == "File9250")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.File9250.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;//#868
                }
                else if (UploadedFileSection == "File9250A")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.File9250A.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;//#868
                }
                else if (UploadedFileSection == "InvoiceFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Invoice.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;//#868
                }
                else if (UploadedFileSection == "ReceiptFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Receipt.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;//#868
                }
                else if (UploadedFileSection == "ContractFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Contract.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;//#868
                }
                else if (UploadedFileSection == "PictureFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Picture.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;//#868
                }
                else if (UploadedFileSection == "OtherFile")
                {
                    FileSectionName = Enum.Parse(typeof(FileType), FileType.Other.ToString()).ToString();
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId(FileSectionName).TransactionDocumentId;//#868
                }

                var uploadmodel = new RestfulWebApiUploadModel()
                {
                    //propertyID = _projectActionFormManager.GetProdPropertyInfo(attachFilesViewModel.FHANumber).PropertyId.ToString(),
                    propertyID = PropertyInfoModel.PropertyId.ToString(),
                    indexType = "1",
                    indexValue = attachFilesViewModel.FHANumber,
                    pdfConvertableValue = "false",
                    folderKey = 0,
                    folderNames = "/" + PropertyInfoModel.PropertyId.ToString() + "/" + attachFilesViewModel.FHANumber + "/" + "Asset Management /" + "R4R" + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy"),//#868
                    transactionType = model.ActionTypeId.ToString(),//#868
                    transactionStatus = "",//#868
                    transactionDate = DateTime.UtcNow.ToString("MM-dd-yyyy"),//#868
                    documentType = documentmodel.TransactionDocumentId.ToString()//#868
                    //folderNames = PropertyInfoModel.PropertyId.ToString() + "/" + attachFilesViewModel.FHANumber + "/Asset Management/R4R/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM/dd/yyyy") + "/" + FileSectionName,
                };
                var WebApiUploadResult = new RestfulWebApiResultModel();
                var request = new RestfulWebApiTokenResultModel();
                foreach (string Filename in Request.Files)
                {
                    HttpPostedFileBase myFile = Request.Files[Filename];
                    if (myFile != null && myFile.ContentLength != 0)
                    {
                        double fileSize = (myFile.ContentLength) / 1024;
                        Random randoms = new Random();
                        var UniqueId = randoms.Next(0, 99999);

                        var systemFileName = attachFilesViewModel.FHANumber + "_" +
                                             attachFilesViewModel.TaskInstanceId + "_" + UniqueId +
                                             Path.GetExtension(myFile.FileName);
                        string pathForSaving = Server.MapPath("~/Uploads");
                        if (this.CreateFolderIfNeeded(pathForSaving))
                        {
                            try
                            {
                                myFile.SaveAs(Path.Combine(pathForSaving, systemFileName));
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        request = webApiTokenRequest.RequestToken();
                        // WebApiUploadResult = uploadApiManager.UploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");
                        //#868
                        WebApiUploadResult = uploadApiManager.AssetManagementR4RAndNCREUploadDocumentUsingWebApi(uploadmodel, request.access_token, myFile, "");


                        if (WebApiUploadResult.status != null && WebApiUploadResult.status.ToLower() == "success")
                        {
                             System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));// commented because for safety purpose we are not deleting files from physical folder
                            /* Creting entry in Group Task and R4R Tables*/
                            var grouptaskmodel = RFORRANDNCR_GroupTasksManager.GetGroupTaskAByTaskInstanceId(model.TaskInstanceId.Value);
                            if (grouptaskmodel == null)
                            {
                                var prodgroupTaskModel = new RFORRANDNCR_GroupTaskModel();
                                //prodgroupTaskModel.TaskInstanceId = Guid.NewGuid();
                                prodgroupTaskModel.TaskInstanceId = model.TaskInstanceId.Value;
                                prodgroupTaskModel.InUse = UserPrincipal.Current.UserId;
                                prodgroupTaskModel.FHANumber = model.FHANumber;
                                prodgroupTaskModel.RequestStatus = (int)RequestStatus.Draft;
                                prodgroupTaskModel.PageTypeId = 1;
                                prodgroupTaskModel.IsDisclaimerAccepted = false;
                                prodgroupTaskModel.ModifiedOn = DateTime.UtcNow;
                               // prodgroupTaskModel.ModifiedOn = ControllerHelper.GetClientDateTime();
                                prodgroupTaskModel.ModifiedBy = UserPrincipal.Current.UserId;
                                prodgroupTaskModel.CreatedOn = DateTime.UtcNow;
                               // prodgroupTaskModel.CreatedOn = ControllerHelper.GetClientDateTime();
                                prodgroupTaskModel.CreatedBy = UserPrincipal.Current.UserId;
                                prodgroupTaskModel.ServicerComments = model.ServicerRemarks;
                                //prodgroupTaskModel.ServicerComments = parentdict["ServicerComments"] != null ? (string)parentdict["ServicerComments"] : "";
                                var ProdGroupTaskId = RFORRANDNCR_GroupTasksManager.AddR4RGroupTasks(prodgroupTaskModel);
                                model.TaskInstanceId = ProdGroupTaskId;
                                var R4RId = reserveForReplacementManager.SaveR4RForm(model);
                            }
                            else
                            {
                                var R4RId = reserveForReplacementManager.GetR4RbyTaskInstanceId(model.TaskInstanceId.Value);
                                model.R4RId = R4RId.R4RId;
                                reserveForReplacementManager.UpdateR4RForm(model);
                            }
                            /*End*/
                            var taskFile = ControllerHelper.PoupuLateGroupTaskFile(myFile, fileSize, UniqueId.ToString(),
                            DateTime.UtcNow.ToString(), systemFileName, model.TaskInstanceId.Value);
                            taskFile.GroupFileType = FileSectionName;
                            taskFile.DocId = WebApiUploadResult.docId;
                            taskFile.Version = Convert.ToInt32(WebApiUploadResult.version);
                            taskFile.API_upload_status = WebApiUploadResult.status;
                            taskFile.DocTypeID = WebApiUploadResult.documentType;
                            taskManager.SaveGroupTaskFileModel(taskFile);
                            isUploaded = "Success";
                        }
                        else
                        {
                            Exception ex = new Exception(WebApiUploadResult.message);
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            System.IO.File.Delete(Path.Combine(pathForSaving, systemFileName));
                            if (!string.IsNullOrEmpty(WebApiUploadResult.message))
                            {
                                isUploaded = WebApiUploadResult.message;
                            }
                            else
                            {
                                isUploaded = "failed";
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                //throw;
            }

            return isUploaded;
        }

        [HttpPost]
        public ActionResult SubmitForm(ReserveForReplacementFormModel model)
        {
            IList<TaskFileModel> taskFile = new List<TaskFileModel>();

            TaskFileModel taskFile1 = new TaskFileModel();
            //IList<TaskFileModel> taskFile = new List<TaskFileModel>();
            IList<TaskFileModel> taskFileList = new List<TaskFileModel>();
            string year = string.Empty;
            ViewAsPdf generatePdf;
            string submitDate = string.Empty;
            if (Session == null)
            {
                return RedirectToAction("Login", "Account");
            }

            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                model.IsLoggedInUserLender = true;
                model.IsCommentEntered = true;
            }
            model.IsEligibleForAutomaticApproval = true;
            model.IsNumberOfUnitsInRange = true;
            model.Is9250ARequired = true;
            model.Is9250Required = true;

            if (ModelState.IsValid)
            {
                #region 
                /* When upgrading file upload to cloud commented */
                //if (model.File9250 != null)
                //{
                //    TaskHelper.CacheFileData(TempData, model.File9250, "File9250");
                //}

                //if (model.File9250A != null)
                //{
                //    TaskHelper.CacheFileData(TempData, model.File9250A, "File9250A");
                //}
                //if (model.InvoiceFile != null)
                //{
                //    TaskHelper.CacheFileData(TempData, model.InvoiceFile, "InvoiceFile");
                //}
                //if (model.ReceiptFile != null)
                //{
                //    TaskHelper.CacheFileData(TempData, model.ReceiptFile, "ReceiptFile");
                //}

                //if (model.ContractFile != null)
                //{
                //    TaskHelper.CacheFileData(TempData, model.ContractFile, "ContractFile");
                //}

                //if (model.PictureFile != null)
                //{
                //    TaskHelper.CacheFileData(TempData, model.PictureFile, "PictureFile");
                //}

                //if (model.OtherFile != null)
                //{
                //    TaskHelper.CacheFileData(TempData, model.OtherFile, "OtherFile");
                //}

                //PopulateFileModelFromTempData(model);
                #endregion

                ActionResult submitForm;
                if (R4RFormValidation(model, out submitForm))
                {
                    model.IsAgreementAccepted = false;
                    return submitForm;
                }
                //if (model.IsLenderDelegate == false && (model.File9250 == null || model.File9250A == null))
                //if (model.IsLenderDelegate == false && (model.FileName9250 == null || model.FileName9250A == null))
                if (model.IsLenderDelegate == false && (model.FileName9250 == null))
                {
                    Session["Confirmation"] = "Please attach the required Evidential Information.";
                    PopulateFHANumberList(model);
                    model.IsAgreementAccepted = false;
                    return View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                }
                //if (model.IsSuspension == false && (model.File9250 == null || model.File9250A == null))
                //if (model.IsSuspension == false && (model.FileName9250 == null || model.FileName9250A == null))
                if (model.IsSuspension == false && (model.FileName9250 == null))
                {
                    Session["Confirmation"] = "Please attach the required Evidential Information.";
                    PopulateFHANumberList(model);
                    model.IsAgreementAccepted = false;
                    return View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                }
                if (model.IsDisplayAcceptPopup)
                {
                    model.IsAgreementAccepted = true;
                    PopulateFHANumberList(model);
                }
                if (model.NumberOfUnits < 10 || model.NumberOfUnits > 500)
                {
                    model.IsNumberOfUnitsInRange = false;
                    model.IsEligibleForAutomaticApproval = false;
                }
                int reacScore = 0;
                if (model.ReacScore != null)
                {
                    var splitStr = model.ReacScore.Split(' ');
                    if (int.TryParse(splitStr[0], out reacScore))
                    {
                        reacScore = int.Parse(splitStr[0]);
                    }
                    else
                    {
                        reacScore = 0;
                    }
                }

                var r4rFormList = reserveForReplacementManager.GetR4RFormByPropertyId(model.PropertyId);
                if (r4rFormList != null)
                {
                    var r4rList = r4rFormList as IList<ReserveForReplacementFormModel> ?? r4rFormList.ToList();
                    if (r4rList.OrderByDescending(p => p.ServicerSubmissionDate).Any())
                    {
                        var latestR4RForm = r4rList.OrderByDescending(p => p.ServicerSubmissionDate).ToList().Last();
                        if (latestR4RForm.ServicerSubmissionDate != null)
                        {
                            //var lastR4Rdate = (DateTime)latestR4RForm.ServicerSubmissionDate;
                            var lastR4Rdate = (DateTime)latestR4RForm.ServicerSubmissionDate;
                            var dCurrentDate = DateTime.Now;
                            var tDateDifference = dCurrentDate - lastR4Rdate;
                            if (tDateDifference.TotalDays < NoOfDaysLimitForPreviousR4RRequest)
                            {
                                model.IsR4RWithin90Days = true;
                            }
                        }
                    }
                }
                var calculatedValue = model.NumberOfUnits * 1000;
                if ((model.ReserveAccountBalance - model.TotalRequestedAmount) < calculatedValue ||
                    model.IsRequestForAdvance ||
                    model.IsPurchaseYearOlderThanR4RRequest ||
                    (model.TroubledCode == "P" || model.TroubledCode == "T") ||
                    model.IsR4RWithin90Days || model.IsRemodelingProposed || model.IsMortgagePaymentsCovered)
                {
                    model.IsEligibleForAutomaticApproval = false;
                }
                if (model.IsSuspension.HasValue ? model.IsSuspension.Value : false)
                    model.IsEligibleForAutomaticApproval = false;
                var propertyDetailsTxt = "";
                if (!string.IsNullOrEmpty(model.FHANumber))
                {
                    var propertyInfo = reserveForReplacementManager.GetPropertyInfo(model.FHANumber);
                    if (model.PropertyAddress != null)
                    {
                        if (model.PropertyAddress.AddressLine1 != null &&
                            model.PropertyAddress.AddressLine1 != propertyInfo.StreetAddress)
                        {
                            propertyDetailsTxt += "<p>Street Address: " + model.PropertyAddress.AddressLine1 + "</p>";
                        }
                        if (model.PropertyAddress.City != null &&
                            model.PropertyAddress.City != propertyInfo.City)
                        {
                            propertyDetailsTxt += "<p>City: " + model.PropertyAddress.City + "</p>";
                        }
                        if (model.PropertyAddress.StateCode != null &&
                            model.PropertyAddress.StateCode != propertyInfo.State)
                        {
                            propertyDetailsTxt += "<p>State: " + model.PropertyAddress.StateCode + "</p>";
                        }
                        if (model.PropertyAddress.ZIP != null &&
                            model.PropertyAddress.ZIP != propertyInfo.Zipcode)
                        {
                            propertyDetailsTxt += "<p>Zip: " + model.PropertyAddress.ZIP + "</p>";
                        }

                        if (!string.IsNullOrEmpty(propertyDetailsTxt))
                        {
                            model.IsUpdateiREMS = true;
                        }
                    }
                }
                var task = new TaskModel();
                var taskList = new List<TaskModel>();
                model.SubmitByUserId = UserPrincipal.Current.UserId;
                model.SubmitToUserId = reserveForReplacementManager.GetAeUserIdByFhaNumber(model.FHANumber);

                if (model.SubmitToUserId != 0)
                {
                    var toUser = accountManager.GetUserById(model.SubmitToUserId.Value);
                    task.AssignedTo = toUser.UserName;
                }
                else
                {
                    task.AssignedTo = reserveForReplacementManager.GetAeEmailByFhaNumber(model.FHANumber);
                }
                task.AssignedBy = UserPrincipal.Current.UserName;
                model.AssignedTo = task.AssignedTo;
                model.AssignedBy = task.AssignedBy;
                task.Notes = model.ServicerRemarks;
                task.StartTime = DateTime.UtcNow;
                //task.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(task.StartTime);
                task.MyStartTime = task.StartTime;
                // comented because task id is comming from grouptask-- By Venkatesh 
                //task.TaskInstanceId = Guid.NewGuid();
                task.TaskInstanceId = model.TaskInstanceId.Value;
                if (model.IsEligibleForAutomaticApproval)
                {
                    task.TaskStepId = (int)TaskStep.R4RComplete;
                }
                else
                {
                    task.TaskStepId = (int)TaskStep.R4RRequest;
                }
                task.SequenceId = 0;
                model.SequenceId = task.SequenceId;
                task.IsReAssigned = false;
                //model.ServicerSubmissionDate = TimezoneManager.GetUtcTimeFromPreferred((DateTime)model.ServicerSubmissionDate);
                model.ServicerSubmissionDate = DateTime.UtcNow;
                try
                {
                    if (model.IsUpdateiREMS)
                    {
                        //email notification to update iREMS
                        backgroundJobManager.SendUpdateiREMSNotificationEmail(emailManager, propertyDetailsTxt,
                            model.PropertyName, model.FHANumber);
                    }

                    taskFile = UpdateTaskFileModelForTask(model, task);
                    if (model.IsEligibleForAutomaticApproval)
                    {
                        var documentmodel = new AM_R4RAndNCREDocumentModel();
                        documentmodel.TransactionDocumentId = taskManager.GetTransactionId("R4RPDF").TransactionDocumentId;
                        if (model.ServicerSubmissionDate.HasValue)
                        {
                            year = model.ServicerSubmissionDate.Value.Year.ToString();
                            submitDate = model.ServicerSubmissionDate.Value.ToString("yyyyMMddHHmmss");
                        }
                        generatePdf = new ViewAsPdf("~/Views/AssetManagement/ReserveForReplacementFormReadOnlyView.cshtml", model);
                        var binary = generatePdf.BuildPdf(ControllerContext);
                        System.IO.File.WriteAllBytes(Server.MapPath("~/Templates/ReserveForReplacementForm.pdf"), binary);
                        // harish added below line to send Actiontype id to match with TA server ,TA team given Transaction id for R4R 06052020
                        model.ActionTypeId = 86;




                        var uploadmodel = new RestfulWebApiUploadModel()
                        {
                            propertyID = model.PropertyId.ToString(),
                            indexType = "1",
                            indexValue = model.FHANumber,
                            pdfConvertableValue = "false",
                            documentType = documentmodel.TransactionDocumentId.ToString(),
                            folderNames = "/" + model.PropertyId.ToString() + "/" + model.FHANumber + "/" + "Asset Management /" + "R4R" + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy"),
                            transactionType = model.ActionTypeId.ToString(),
                            transactionStatus = "",
                            transactionDate =DateTime.UtcNow.ToString("MM-dd-yyyy"),

                            //folderNames = "/" + model.PropertyId.ToString() + "/" + model.FHANumber + "/Asset Management/R4R/" + year + "/" + submitDate + "/",
                        };
                        var WebApiUploadResult = new RestfulWebApiResultModel();
                        var request = new RestfulWebApiTokenResultModel();
                        request = webApiTokenRequest.RequestToken();

                        var result = uploadApiManager.AssetManagementuploadSharepointPdfFile(uploadmodel, request.access_token, binary);
                        // var result = uploadApiManager.AssetManagementUploadDocumentUsingWebApi(uploadmodel, request.access_token, binary);
                        Random randoms = new Random();
                        var fileName = result.fileName == null ? "Reserve for Replacement Submission Form.pdf" : result.fileName;
                        fileName = fileName.Replace("SharePointData", "Reserve for Replacement Submission Form");
                        var fileSize = binary.Length;
                        var UniqueId = randoms.Next(0, 99999);
                        var systemFileName = model.FHANumber + "_" + model.TaskInstanceId.Value.ToString() + "_" + UniqueId + Path.GetExtension(fileName);
                        System.IO.File.WriteAllBytes(Server.MapPath("~/Uploads/" + systemFileName), binary);

                        taskFile1 = ControllerHelper.PopulateGroupTaskFileForSharepoint(fileName, fileSize, "Reserve for Replacement Submission Form", DateTime.UtcNow.ToString(), systemFileName.ToString(), model.TaskInstanceId.Value);
                        if (result.status != null)
                        {
                            if (result.status.ToLower() == "success")
                            {
                                taskFile1.API_upload_status = result.status;
                                taskFile1.Version = Convert.ToInt32(result.version);
                                taskFile1.DocId = result.docId;
                                taskFile1.DocTypeID = result.documentType;
                                taskFile1.FileType = FileType.R4RSubmissionForm;
                                taskManager.SaveGroupTaskFileModel(taskFile1);
                                var mappingmodel = new TaskFile_FolderMappingModel();
                                mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.R4R);
                                mappingmodel.TaskFileId = taskFile1.TaskFileId;
                                mappingmodel.CreatedOn = DateTime.UtcNow;
                                //mappingmodel.CreatedOn = DateTime.UtcNow;
                                taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                            }

                        }  //Store information locally and regenerate it agian
                        else
                        {
                            taskFile1.DocTypeID = DefaultDocID;
                            taskManager.SaveGroupTaskFileModel(taskFile1);
                            var mappingmodel = new TaskFile_FolderMappingModel();
                            mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.R4R);
                            mappingmodel.TaskFileId = taskFile1.TaskFileId;
                            mappingmodel.CreatedOn = DateTime.UtcNow;
                            taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                        }
                        //Prod_TaskXrefModel objProd_TaskXrefModel = new Prod_TaskXrefModel()
                        //{
                        //	TaskXrefid = Guid.NewGuid(),
                        //	TaskInstanceId = model.TaskInstanceId.Value,
                        //	TaskId = model.TaskId.Value,
                        //	AssignedBy = UserPrincipal.Current.UserId,
                        //	IsReviewer = false,
                        //	Status = Convert.ToInt32(TaskStep.R4RComplete),
                        //	AssignedDate = DateTime.UtcNow,
                        //	ViewId = Convert.ToInt32(ProductionView.WLM),
                        //	ModifiedOn = DateTime.UtcNow
                        //};
                        //prod_TaskXrefManager.AddTaskXref(objProd_TaskXrefModel);



                        model.RequestStatus = (int)RequestStatus.AutoApprove;

                        // R4R have entry in the system when upload the document itself we created on entry 
                        var R4RId = reserveForReplacementManager.GetR4RbyTaskInstanceId(model.TaskInstanceId.Value);
                        model.R4RId = R4RId.R4RId;
                        //model.R4RId = reserveForReplacementManager.SaveR4RForm(model);
                        reserveForReplacementManager.UpdateR4RForm(model);

                        task.DataStore1 = XmlHelper.Serialize(model, typeof(ReserveForReplacementFormModel),
                            Encoding.Unicode);
                        taskList.Add(task);
                        taskFileList.Add(taskFile1);
                        taskManager.SaveTask(taskList, taskFileList);
                        model.TaskId = reserveForReplacementManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        reserveForReplacementManager.UpdateTaskId(model);
                        //email notification for automatic approval
                        backgroundJobManager.SendR4RAutoApprovalNotificationEmail(emailManager, task.AssignedTo,
                            task.AssignedBy, model);
                        PopupHelper.ConfigPopup(TempData, 320, 200, "Confirmation", false, false, true, false, false, true,
                        "../ReserveForReplacement/ApprovePopup");
                    }
                    else
                    {
                        model.RequestStatus = (int)RequestStatus.Submit;
                        // R4R have entry in the system when upload the document itself we created on entry 
                        var R4RId = reserveForReplacementManager.GetR4RbyTaskInstanceId(model.TaskInstanceId.Value);
                        model.R4RId = R4RId.R4RId;
                        //model.R4RId = reserveForReplacementManager.SaveR4RForm(model);
                        reserveForReplacementManager.UpdateR4RForm(model);

                        task.DataStore1 = XmlHelper.Serialize(model, typeof(ReserveForReplacementFormModel),
                            Encoding.Unicode);
                        taskList.Add(task);
                        taskManager.SaveTask(taskList, taskFile);
                        model.TaskId = reserveForReplacementManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        reserveForReplacementManager.UpdateTaskId(model);
                        RFORRANDNCR_GroupTasksManager.DeleteGroupTask(model.TaskInstanceId.Value, model.FHANumber);
                        //email notification if the request is forwarded to AE
                        backgroundJobManager.SendR4RSubmitNotificationEmail(emailManager, task.AssignedTo,
                            task.AssignedBy, model);
                        PopupHelper.ConfigPopup(TempData, 320, 200, "Confirmation", false, false, true, false, false, true,
                            "../ReserveForReplacement/AcceptPopup");
                    }
                    TempData["R4RFormData"] = model;
                    TempData["File9250"] = null;
                    TempData["File9250A"] = null;
                    TempData["InvoiceFile"] = null;
                    TempData["ContractFile"] = null;
                    TempData["ReceiptFile"] = null;
                    TempData["PictureFile"] = null;
                    TempData["OtherFile"] = null;
                }
                //catch (DbUpdateConcurrencyException)
                //{
                //    HandleConcurrentUpdateException(model);
                //}
                catch (Exception e)
                {
                    ErrorSignal.FromCurrentContext().Raise(e);
                    throw new InvalidOperationException(
                        string.Format("Error r4r submit: {0}", e.InnerException), e.InnerException);
                }
            }
            else
            {
                model.IsAgreementAccepted = false;
                PopulateFHANumberList(model);
            }

            return View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
        }

        private bool R4RFormValidation(ReserveForReplacementFormModel model, out ActionResult submitForm)
        {
            //validating BorrowersRequestDate cannot be a future date
            if (model.BorrowersRequestDate != null)
            {
                try
                {
                    var dBorrowersDate = (DateTime)model.BorrowersRequestDate;
                    var dCurrentDate = DateTime.Now;
                    var tDateDifference = dCurrentDate - dBorrowersDate;
                    if (tDateDifference < new TimeSpan(0, 0, 0))
                    {
                        model.IsEligibleForAutomaticApproval = false;
                        Session["Confirmation"] = "Borrower's Request Date must not be in the future.";
                        PopulateFHANumberList(model);
                        {
                            submitForm = View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                            return true;
                        }
                    }
                    Session["Confirmation"] = "";
                }
                catch
                {
                    model.IsEligibleForAutomaticApproval = false;
                    Session["Confirmation"] = "Borrower's Request Date selected doesn't belong to Gregorian calendar year.";
                    PopulateFHANumberList(model);
                    {
                        submitForm = View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                        return true;
                    }
                }
            }

            //validating ReserveAccountBalanceAsOfDate cannot be a future date
            if (model.ReserveAccountBalanceAsOfDate != null)
            {
                try
                {
                    var dAsOfDate = (DateTime)model.ReserveAccountBalanceAsOfDate;
                    var dCurrentDate = DateTime.Now;
                    var tDateDifference = dCurrentDate - dAsOfDate;
                    if (tDateDifference < new TimeSpan(0, 0, 0))
                    {
                        model.IsEligibleForAutomaticApproval = false;
                        Session["Confirmation"] = "Reserve Account Balance As of Date must not be in the future.";
                        PopulateFHANumberList(model);
                        {
                            submitForm = View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                            return true;
                        }
                    }
                }
                catch
                {
                    model.IsEligibleForAutomaticApproval = false;
                    Session["Confirmation"] =
                        "Reserve Account Balance As of Date selected doesn't belong to Gregorian calendar year.";
                    PopulateFHANumberList(model);
                    submitForm = View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                    return true;
                }
            }
            //Checking for any empty fields in the R4R form
            if (!bizRuleManager.CheckForEmptyR4RFormFields(model))
            {
                Session["Confirmation"] = "Please fill in all empty fields.";
                PopulateFHANumberList(model);
                submitForm = View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", model);
                return true;

            }
            if (!model.IsLoggedInUserLender && !model.TotalApprovedAmount.HasValue)
            {
                Session["Confirmation"] = "Please enter Total Approved Amount.";
                PopulateFHANumberList(model);
                submitForm = View("~/Views/AssetManagement/ReserveForReplacementFormAeView.cshtml", model);
                return true;
            }
            submitForm = null;
            return false;
        }

        private void HandleConcurrentUpdateException(ReserveForReplacementFormModel model)
        {
            model.IsConcurrentUserUpdateFound = true;
        }

        private IList<TaskFileModel> UpdateTaskFileModelForTask(ReserveForReplacementFormModel model, TaskModel task)
        {
            IList<TaskFileModel> listTaskModel = new List<TaskFileModel>();

            //PopulateFileModelFromTempData(model);

            //if (model.File9250 != null)
            //{
            //    ControllerHelper.PopulateTaskFileModel(model.File9250, FileType.File9250, task, listTaskModel);
            //}

            //if (model.File9250A != null)
            //{
            //    ControllerHelper.PopulateTaskFileModel(model.File9250A, FileType.File9250A, task, listTaskModel);
            //}

            //if (model.InvoiceFile != null)
            //{
            //    ControllerHelper.PopulateTaskFileModel(model.InvoiceFile, FileType.Invoice, task, listTaskModel);
            //}

            //if (model.ReceiptFile != null)
            //{
            //    ControllerHelper.PopulateTaskFileModel(model.ReceiptFile, FileType.Receipt, task, listTaskModel);
            //}

            //if (model.ContractFile != null)
            //{
            //    ControllerHelper.PopulateTaskFileModel(model.ContractFile, FileType.Contract, task, listTaskModel);
            //}

            //if (model.PictureFile != null)
            //{

            //    ControllerHelper.PopulateTaskFileModel(model.PictureFile, FileType.Picture, task, listTaskModel);
            //}

            //if (model.OtherFile != null)
            //{
            //    ControllerHelper.PopulateTaskFileModel(model.OtherFile, FileType.Other, task, listTaskModel);
            //}
            return listTaskModel;
        }

        private void PopulateFileModelFromTempData(ReserveForReplacementFormModel model)
        {
            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.File9250 == null && TempData["File9250"] != null)
            {
                model.File9250 = (HttpPostedFileBase)TempData["File9250"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.File9250A == null && TempData["File9250A"] != null)
            {
                model.File9250A = (HttpPostedFileBase)TempData["File9250A"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.InvoiceFile == null && TempData["InvoiceFile"] != null)
            {
                model.InvoiceFile = (HttpPostedFileBase)TempData["InvoiceFile"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.ReceiptFile == null && TempData["ReceiptFile"] != null)
            {
                model.ReceiptFile = (HttpPostedFileBase)TempData["ReceiptFile"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.ContractFile == null && TempData["ContractFile"] != null)
            {
                model.ContractFile = (HttpPostedFileBase)TempData["ContractFile"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.PictureFile == null && TempData["PictureFile"] != null)
            {
                model.PictureFile = (HttpPostedFileBase)TempData["PictureFile"];
            }

            // If the model wasn't updated with the confirm submit, then the data is in TempData.
            if (model.OtherFile == null && TempData["OtherFile"] != null)
            {
                model.OtherFile = (HttpPostedFileBase)TempData["OtherFile"];
            }
            TempData["File9250"] = model.File9250;
            TempData["File9250A"] = model.File9250A;
            TempData["InvoiceFile"] = model.InvoiceFile;
            TempData["ContractFile"] = model.ContractFile;
            TempData["ReceiptFile"] = model.ReceiptFile;
            TempData["PictureFile"] = model.PictureFile;
            TempData["OtherFile"] = model.OtherFile;

        }
        //#872 Harish Added new logic to download 08052020
        [HttpGet]
        public FileResult DownloadTaskFile(Guid? taskInstanceId, FileType taskFileType)
        {

            TaskFileModel taskFile = taskManager.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId.Value, taskFileType);
            if (taskFile != null)
            {
                if (taskFile.DocId == null && taskFile.API_upload_status == null)
                {
                    return File(taskFile.FileData, "text/text", taskFile.FileName);
                }
                else
                {
                    string JsonSteing = "";
                    RestfulWebApiTokenResultModel request = new RestfulWebApiTokenResultModel();
                    //reviewFileStatusManager.UpdateReviewFileStatusForFileDownload(taskFile.TaskFileId);
                    string filePath = "";
                    FileInfo fileInfo = null;
                    filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                    fileInfo = new System.IO.FileInfo(filePath);
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", taskFile.FileName));
                    if (!string.IsNullOrEmpty(taskFile.API_upload_status) && taskFile.API_upload_status.ToLower() == "success")
                    {
                        request = webApiTokenRequest.RequestToken();
                        JsonSteing = "{\"docId\":" + taskFile.DocId + ",\"version\":" + taskFile.Version + "}";
                        Stream streamResult = WebApiDownload.DownloadDocumentUsingWebApi(JsonSteing, request.access_token, taskFile.FileName);
                        return new FileStreamResult(streamResult, "application/octet-stream");
                    }

                    else
                    {
                        filePath = Server.MapPath("~/Uploads/" + taskFile.SystemFileName);
                        fileInfo = new System.IO.FileInfo(filePath);
                        Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                        Response.WriteFile(filePath);
                    }
                    Response.End();

                }
            }

            return null;
        }

        public JsonResult GetPropertyInfo(string selectedFhaNumber)
        {
            var result = reserveForReplacementManager.GetPropertyInfo(selectedFhaNumber);
            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        public JsonResult GetApprovedAmountByFHANumber(string selectedFhaNumber)
        {
            var result = reserveForReplacementManager.GetApprovedAmountByFHANumber(selectedFhaNumber);
            JsonResult json = null;
            if (result != null)
            {
                json = Json(result, JsonRequestBehavior.AllowGet);
            }
            return json;
        }

        public ActionResult UpdateApprovedAmount(decimal changeApprovedAmount, decimal? TotalRequestedAmount, string ServicerSubmissionDate, int TaskId, string FHANumber, ReserveForReplacementFormModel model)
        {
            model = reserveForReplacementManager.GetR4RFormByFHANumber(FHANumber);
            model.ChangeApprovedAmount = changeApprovedAmount;
            ViewAsPdf generatePdf;
            TaskFileModel taskFile = new TaskFileModel();
            IList<TaskFileModel> taskFileList = new List<TaskFileModel>();
            string year = string.Empty;
            string submitDate = string.Empty;
            var task = new TaskModel();
            var taskList = new List<TaskModel>();

            string HudRemarks = "Your Reserve for Replacement request in the amount of " + string.Format("{0:c}", model.TotalRequestedAmount) + " Submitted on "
                  + ServicerSubmissionDate + " has been approved on "
                   + string.Format("{0:MM/dd/yyyy}", DateTime.UtcNow)
                   // + " by "+ UserPrincipal.Current.FullName
                   + " for the amount of " + string.Format("{0:c}", changeApprovedAmount)
                   + string.Format("{0} {1}", " . Please maintain a copy of this for proof of approval.", "\n Please feel free")
                   + " to contact your Account Executive " + UserPrincipal.Current.FullName + " at " + UserPrincipal.Current.UserName
                   + " if you should have any questions regarding this project.";

            if (model.TaskInstanceId == null)
            {
                TaskModel oTask = taskManager.GetTaskById(model.TaskId.Value);
                model.TaskInstanceId = oTask.TaskInstanceId;
            }

            if (model.ServicerSubmissionDate.HasValue)
            {
                year = model.ServicerSubmissionDate.Value.Year.ToString();
                submitDate = model.ServicerSubmissionDate.Value.ToString("yyyyMMddHHmmss");
            }
            generatePdf = new ViewAsPdf("~/Views/AssetManagement/R4RChangeApprovedAmountReadOnlyView.cshtml", model);
            var binary = generatePdf.BuildPdf(ControllerContext);
            var uploadmodel = new RestfulWebApiUploadModel()
            {
                propertyID = model.PropertyId.ToString(),
                indexType = "1",
                indexValue = model.FHANumber,
                pdfConvertableValue = "false",
                documentType = DefaultDocID,
                folderNames = "/" + model.PropertyId.ToString() + "/" + model.FHANumber + "/Asset Management/R4R/" + year + "/" + submitDate + "/Amendments/",
            };
            var WebApiUploadResult = new RestfulWebApiResultModel();
            var request = new RestfulWebApiTokenResultModel();
            request = webApiTokenRequest.RequestToken();

            var objRestfulWebApiResultModel = uploadApiManager.uploadSharepointPdfFile(uploadmodel, request.access_token, binary);
            Random randoms = new Random();
            var fileName = objRestfulWebApiResultModel.fileName == null ? "Change  of Loan Amount Approved by Admin.pdf" : objRestfulWebApiResultModel.fileName;
            fileName = fileName.Replace("SharePointData", "Change  of Loan Amount Approved by Admin");
            var fileSize = binary.Length;
            var UniqueId = randoms.Next(0, 99999);
            var systemFileName = model.FHANumber + "_" + model.TaskInstanceId.Value.ToString() + "_" + UniqueId + Path.GetExtension(fileName);
            System.IO.File.WriteAllBytes(Server.MapPath("~/Uploads/" + systemFileName), binary);

            taskFile = ControllerHelper.PopulateGroupTaskFileForSharepoint(fileName, fileSize, "Change  of Loan Amount Approved by Admin", DateTime.UtcNow.ToString(), systemFileName.ToString(), model.TaskInstanceId.Value);
            if (objRestfulWebApiResultModel.status != null)
            {
                if (objRestfulWebApiResultModel.status.ToLower() == "success")
                {
                    taskFile.API_upload_status = objRestfulWebApiResultModel.status;
                    taskFile.Version = Convert.ToInt32(objRestfulWebApiResultModel.version);
                    taskFile.DocId = objRestfulWebApiResultModel.docId;
                    taskFile.DocTypeID = objRestfulWebApiResultModel.documentType;
                    taskFile.FileType = FileType.R4RSubmissionForm;
                    taskManager.SaveGroupTaskFileModel(taskFile);
                    var mappingmodel = new TaskFile_FolderMappingModel();
                    mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.R4R);
                    mappingmodel.TaskFileId = taskFile.TaskFileId;
                    mappingmodel.CreatedOn = DateTime.UtcNow;
                    taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                }

            }  //Store information locally and regenerate it agian
            else
            {
                taskFile.DocTypeID = DefaultDocID;
                taskManager.SaveGroupTaskFileModel(taskFile);
                var mappingmodel = new TaskFile_FolderMappingModel();
                mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.R4R);
                mappingmodel.TaskFileId = taskFile.TaskFileId;
                mappingmodel.CreatedOn = DateTime.UtcNow;
                taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
            }

            // to update hud remarks
            reserveForReplacementManager.UpdateApprovedAmount(changeApprovedAmount, FHANumber, HudRemarks);

            taskManager.UpdateTaskNotes(TaskId, HudRemarks);

            ReserveForReplacementFormModel result = reserveForReplacementManager.GetApprovedAmountByFHANumber(FHANumber);

            task = taskManager.GetTaskById(TaskId);
            XDocument doc = XDocument.Parse(task.DataStore1);
            doc.Element("ReserveForReplacementFormModel").Element("HudRemarks").Value = HudRemarks;
            doc.Element("ReserveForReplacementFormModel").Element("TotalApprovedAmount").Value = changeApprovedAmount.ToString();
            task.DataStore1 = doc.ToString();



            taskManager.UpdateDataStore1(TaskId, doc.ToString());
            PopulateAllFHANumberListForR4R(model);
            ViewBag.Message = "Approved Amount is Successfully Updated";
            return View("~/Views/AssetManagement/R4RChangeApprovedAmountForm.cshtml", model);

        }

        [HttpGet]
        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult GetReserveForReplacementFormDetail(bool isEditMode)
        {
            var model = (ReserveForReplacementFormModel)(TempData["R4RFormData"] ?? new ReserveForReplacementFormModel());
            TempData["R4RFormData"] = model;
            GetDisclaimerText(model);
            model.IsDisplayAcceptPopup = false;
            model.IsAgreementAccepted = true;// User Story 1901 - Set Agreement accepted to True for Read only form
            model.IsCommentEntered = true;
            model.IsLoggedInUserLender = RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName);
            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
            if (model != null)
            {
                if (model.TaskGuid != null)
                {
                    taskManager.SetModelWithFileAttachmentsForR4R(model);
                }
                if (isEditMode)
                {
                    if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName))
                    {
                        if (model.SequenceId.HasValue && model.SequenceId > 0)
                        {
                            return View("~/Views/AssetManagement/ReserveForReplacementFormReadOnlyView.cshtml", model);
                        }
                        model.IsDisplayAcceptPopup = true;
                        if (model.IsEligibleForAutomaticApproval && (model.SequenceId.HasValue && model.SequenceId == 0))
                        {
                            return View("~/Views/AssetManagement/ReserveForReplacementFormReadOnlyView.cshtml", model);
                        }
                        TempData["FormAttachmentFileTypes"] = ConfigurationManager.AppSettings["FormAttachmentFileTypes"];
                        model.IsLoggedInUserLender = false;
                        if (model.TaskOpenStatus != null &&
                            !model.TaskOpenStatus.TaskOpenStatus.Exists(
                                p => p.Key.Equals(UserPrincipal.Current.UserName)))
                        {
                            model.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        }
                        model.HudRemarks = GetTextForHudRemarks(model);
                        return View("~/Views/AssetManagement/ReserveForReplacementFormAeView.cshtml", model);
                    }
                }
                else if (model.SequenceId.HasValue
                    && model.SequenceId < 2
                    && RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName)
                    && !model.IsPAMReport
                    && RoleManager.IsUserLenderRole(model.AssignedTo))
                {
                    var task = new TaskModel();
                    var taskList = new List<TaskModel>();
                    task.AssignedBy = model.AssignedBy;
                    task.AssignedTo = model.AssignedTo;
                    task.Notes = string.Format("{0}{1}{2}{3}", model.HudRemarks, System.Environment.NewLine, ". Additional Comments: ", model.AddnRemarks ?? "(None)");
                    task.StartTime = DateTime.UtcNow;
                   // task.StartTime = ControllerHelper.GetClientDateTime();
                    task.MyStartTime = task.StartTime;
                    task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                    task.IsReAssigned = model.IsReAssigend;
                    task.TaskStepId = (int)TaskStep.R4RComplete;
                    task.TaskOpenStatus = XmlHelper.Serialize(model.TaskOpenStatus, typeof(TaskOpenStatusModel), Encoding.Unicode);
                    task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                    var taskFile = new TaskFileModel();
                    try
                    {
                        task.DataStore1 = XmlHelper.Serialize(model, typeof(ReserveForReplacementFormModel), Encoding.Unicode);
                        taskList.Add(task);
                        taskManager.SaveTask(taskList, taskFile);
                        model.TaskId = reserveForReplacementManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                        reserveForReplacementManager.UpdateTaskId(model);
                    }
                    catch (Exception e)
                    {
                        ErrorSignal.FromCurrentContext().Raise(e);
                        throw new InvalidOperationException(
                            string.Format("Error r4r form detail: {0}", e.InnerException), e.InnerException);
                    }
                }
            }
            return View("~/Views/AssetManagement/ReserveForReplacementFormReadOnlyView.cshtml", model);
        }

        private static string GetTextForHudRemarks(ReserveForReplacementFormModel model)
        {
            return "Your Reserve for Replacement request in the amount of " + string.Format("{0:c}", model.TotalRequestedAmount) + " Submitted on "
                   + string.Format("{0:MM/dd/yyyy}", model.ServicerSubmissionDate) + " has been approved on "
                   + string.Format("{0:MM/dd/yyyy}", DateTime.UtcNow)
                   // + " by "+ UserPrincipal.Current.FullName
                   + " for the amount of " + string.Format("{0:c}", model.TotalApprovedAmount)
                   + string.Format("{0} {1}", " . Please maintain a copy of this for proof of approval.", "\n Please feel free")
                   + " to contact your Account Executive " + UserPrincipal.Current.FullName + " at " + UserPrincipal.Current.UserName
                   + " if you should have any questions regarding this project.";
        }

        public JsonResult AddApprovedAmountToRemarks(decimal approvedAmount)
        {
            var model = (ReserveForReplacementFormModel)TempData["R4RFormData"];
            if (approvedAmount == (decimal)0.1)
            {
                model.TotalApprovedAmount = null;
            }
            else
            {
                model.TotalApprovedAmount = approvedAmount;
            }

            model.HudRemarks = GetTextForHudRemarks(model);
            TempData["R4RFormData"] = model;
            return Json(model.HudRemarks, JsonRequestBehavior.AllowGet);
        }

        [MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        [SiteMapTitle("TaskName", Target = AttributeTarget.CurrentNode)]
        public ActionResult FormFinalApproval(ReserveForReplacementFormModel model)
        {
            // below condition used to display the disclaimerpopup, until agreement accepted, display popup should show
            model.IsAgreementAccepted = true;// User Story 1901 - Get Agreement accepted value AE View
            var isAgreementAccepted = model.IsAgreementAccepted;// User Story 1901 - Get Agreement accepted value AE View
            ViewAsPdf generatePdf;
            TaskFileModel taskFile = new TaskFileModel();
            IList<TaskFileModel> taskFileList = new List<TaskFileModel>();
            string year = string.Empty;
            string submitDate = string.Empty;
            if (!model.IsAgreementAccepted)
            {
                model.IsDisplayAcceptPopupForAe = false;
            }

            var r4rModel = (ReserveForReplacementFormModel)(TempData["R4RFormData"] ?? model);
            ControllerHelper.PopulateParentNodeRouteValuesForPageSort(
                    SessionHelper.SESSION_KEY_MY_TASKS_LINK_DICT);
            if (!string.IsNullOrEmpty(model.AddnRemarks))
            {
                model.IsAdditionalCommentsEntered = true;
                r4rModel.AddnRemarks = model.AddnRemarks;
            }
            r4rModel.TotalApprovedAmount = model.TotalApprovedAmount;
            r4rModel.HudRemarks = model.HudRemarks;
            //r4rModel.ReEnterTotalRequestedAmount = model.ReEnterTotalRequestedAmount;
            model = r4rModel;
            /*if (!model.IsDisplayAcceptPopupForAe)
            {
                model.IsCommentEntered = false;
            }*/

            if (ModelState.IsValid)
            {

                ActionResult submitForm;
                if (R4RFormValidation(model, out submitForm))
                {
                    TempData["R4RFormData"] = model;

                    return submitForm;
                }



                //if (submitForm == null && !model.IsDisplayAcceptPopupForAe)
                //{
                //    model.IsDisplayAcceptPopupForAe = true;

                //    PopulateFHANumberList(model);
                //    TempData["R4RFormData"] = model;
                //    PopupHelper.ConfigPopup(TempData, 720, 350, "Disclaimer", false, false, true, false, false, true, "../ReserveForReplacement/SubmitDisclaimerPopup");
                //    return View("~/Views/AssetManagement/ReserveForReplacementFormAeView.cshtml", model);
                //}
                if (isAgreementAccepted)
                {
                    model.IsDisplayAcceptPopupForAe = true;
                    model.IsAgreementAccepted = true;
                    TempData["R4RFormData"] = model;
                    ControllerHelper.SetMyTaskSessionRouteValuesToTempData(TempData);
                    PopupHelper.ConfigPopup(TempData, 320, 200, "Confirmation", false, false, true, false, false, true, "../ReserveForReplacement/ApprovePopup");
                }
                var task = new TaskModel();
                var taskList = new List<TaskModel>();
                var toEmail = model.AssignedBy;
                try
                {
                    if (model.AssignedBy != null && model.TaskGuid != null)
                    {
                        task.AssignedBy = UserPrincipal.Current.UserName;
                        task.AssignedTo = model.AssignedBy;
                        task.Notes = string.Format("{0}{1}{2}{3}", model.HudRemarks, System.Environment.NewLine, ". Additional Comments: ", model.AddnRemarks);
                        task.StartTime = DateTime.UtcNow;
                        task.MyStartTime = task.StartTime;
                        task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
                        task.TaskStepId = (int)TaskStep.R4RComplete;
                        var taskOpenStatusModel = new TaskOpenStatusModel();
                        taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
                        task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);

                        task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
                        task.IsReAssigned = model.IsReAssigend;
                    }
                }
                catch (Exception e)
                {
                    ErrorSignal.FromCurrentContext().Raise(e);
                    throw new InvalidOperationException(
                        string.Format("Error r4r final approval: {0}", e.InnerException), e.InnerException);
                }


                try
                {
                    if (!string.IsNullOrEmpty(model.HudRemarks))
                    {
                        model.IsHudRemarks = true;
                    }
                    else
                    {
                        model.IsHudRemarks = false;
                    }
                    if (model.ServicerSubmissionDate.HasValue)
                    {
                        year = model.ServicerSubmissionDate.Value.Year.ToString();
                        submitDate = model.ServicerSubmissionDate.Value.ToString("yyyyMMddHHmmss");
                    }
                    generatePdf = new ViewAsPdf("~/Views/AssetManagement/ReserveForReplacementFormReadOnlyView.cshtml", model);
                    var binary = generatePdf.BuildPdf(ControllerContext);
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Templates/ReserveForReplacementForm.pdf"), binary);
                    //#875 harish added for R4R 07052020
                    model.ActionTypeId = 86;
                    var documentmodel = new AM_R4RAndNCREDocumentModel();//#875 07052020
                    documentmodel.TransactionDocumentId = taskManager.GetTransactionId("R4RPDF").TransactionDocumentId;//#868 07052020
                    var uploadmodel = new RestfulWebApiUploadModel()
                    {
                        propertyID = model.PropertyId.ToString(),
                        indexType = "1",
                        indexValue = model.FHANumber,
                        pdfConvertableValue = "false",
                        documentType = documentmodel.TransactionDocumentId.ToString(),//#875 07052020
                        //documentType = DefaultDocID,
                        folderNames = "/" + model.PropertyId.ToString() + "/" + model.FHANumber + "/" + "Asset Management /" + "R4R" + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy"),//#875
                        transactionType = model.ActionTypeId.ToString(), //#875
                        transactionStatus = "",//#875
                        transactionDate =DateTime.UtcNow.ToString("MM-dd-yyyy")//#875
                        //folderNames = "/" + model.PropertyId.ToString() + "/" + model.FHANumber + "/Asset Management/R4R/" + year + "/" + submitDate + "/",
                    };
                    var WebApiUploadResult = new RestfulWebApiResultModel();
                    var request = new RestfulWebApiTokenResultModel();
                    request = webApiTokenRequest.RequestToken();
                    //#875
                    var result = uploadApiManager.AssetManagementuploadSharepointPdfFile(uploadmodel, request.access_token, binary);
                    Random randoms = new Random();
                    var fileName = result.fileName == null ? "Reserve for Replacement Submission Form.pdf" : result.fileName;
                    fileName = fileName.Replace("SharePointData", "Reserve for Replacement Submission Form");
                    var fileSize = binary.Length;
                    var UniqueId = randoms.Next(0, 99999);
                    var systemFileName = model.FHANumber + "_" + model.TaskGuid.Value.ToString() + "_" + UniqueId + Path.GetExtension(fileName);
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Uploads/" + systemFileName), binary);

                    taskFile = ControllerHelper.PopulateGroupTaskFileForSharepoint(fileName, fileSize, "Reserve for Replacement Submission Form", DateTime.UtcNow.ToString(), systemFileName.ToString(), model.TaskGuid.Value);
                    if (result.status != null)
                    {
                        if (result.status.ToLower() == "success")
                        {
                            taskFile.API_upload_status = result.status;
                            taskFile.Version = Convert.ToInt32(result.version);
                            taskFile.DocId = result.docId;
                            taskFile.DocTypeID = result.documentType;
                            taskFile.FileType = FileType.R4RSubmissionForm;
                            taskManager.SaveGroupTaskFileModel(taskFile);
                            var mappingmodel = new TaskFile_FolderMappingModel();
                            mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.R4R);
                            mappingmodel.TaskFileId = taskFile.TaskFileId;
                           
                            mappingmodel.CreatedOn = DateTime.UtcNow;
                            taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                        }

                    }  //Store information locally and regenerate it agian
                    else
                    {
                        taskFile.DocTypeID = DefaultDocID;
                        taskManager.SaveGroupTaskFileModel(taskFile);
                        var mappingmodel = new TaskFile_FolderMappingModel();
                        mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.R4R);
                        mappingmodel.TaskFileId = taskFile.TaskFileId;
                       
                        mappingmodel.CreatedOn = DateTime.UtcNow;
                        taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                    }
                    Prod_TaskXrefModel objProd_TaskXrefModel = new Prod_TaskXrefModel()
                    {
                        TaskXrefid = Guid.NewGuid(),
                        TaskInstanceId = model.TaskGuid.Value,
                        TaskId = model.TaskId.Value,
                        AssignedBy = UserPrincipal.Current.UserId,
                        IsReviewer = false,
                        Status = Convert.ToInt32(TaskStep.R4RComplete),
                        AssignedDate = DateTime.UtcNow,
                        ViewId = Convert.ToInt32(ProductionView.WLM),
                        ModifiedOn = DateTime.UtcNow
                    };
                    prod_TaskXrefManager.AddTaskXref(objProd_TaskXrefModel);
                    model.RequestStatus = (int)RequestStatus.Approve;
                    reserveForReplacementManager.UpdateR4RForm(model);
                    task.DataStore1 = XmlHelper.Serialize(model, typeof(ReserveForReplacementFormModel), Encoding.Unicode);
                    taskList.Add(task);
                    taskFileList.Add(taskFile);
                    taskManager.SaveTask(taskList, taskFileList);
                    model.TaskId = reserveForReplacementManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                    reserveForReplacementManager.UpdateTaskId(model);
                    //email notification for automatic approval
                    backgroundJobManager.SendR4RApprovalNotificationEmail(emailManager, toEmail, task.AssignedBy, model);


                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ExceptionManager.WriteToEventLog(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State), "Application", EventLogEntryType.Error);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ExceptionManager.WriteToEventLog(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage), "Application", EventLogEntryType.Error);
                        }
                    }
                    throw;
                }
                catch (Exception e)
                {
                    ErrorSignal.FromCurrentContext().Raise(e);
                    throw new InvalidOperationException(
                        string.Format("Error r4r final approval: {0}", e.InnerException), e.InnerException);
                }

            }

            TempData["R4RFormData"] = model;
            return View("~/Views/AssetManagement/ReserveForReplacementFormAeView.cshtml", model);


        }

        [HttpPost]
        public ActionResult SubmitDisclaimerPopup()
        {
            return PartialView("~/Views/AssetManagement/SubmitDisclaimerPopup.cshtml");
        }

        [HttpPost]
        public ActionResult AcceptPopup()
        {
            return PartialView("~/Views/AssetManagement/AcceptPopup.cshtml");
        }

        [HttpPost]
        public ActionResult ApprovePopup()
        {
            return PartialView("~/Views/AssetManagement/ApprovePopup.cshtml");
        }

        [HttpPost]
        public ActionResult MissingFhaNumberQueryForm()
        {
            return PartialView("~/Views/AssetManagement/MissingFhaNumberQueryForm.cshtml");
        }

        [HttpPost]
        public ActionResult SendMissingFHAInfo(MissingFhaNumberEmailModel model)
        {
            var r4rModel = new ReserveForReplacementFormModel();
            r4rModel.ServicerSubmissionDate = DateTime.Now;
            r4rModel.IsLoggedInUserLender = true;
            PopulateFHANumberList(r4rModel);
            GetDisclaimerText(r4rModel);// User Story 1901 
            r4rModel.IsAgreementAccepted = false;// User Story 1901 
            try
            {
                if (model != null && ModelState.IsValid)
                {
                    backgroundJobManager.SendMissingFhaNotificationEmail(emailManager, model);
                    r4rModel.IsFhaNumberMissing = true;
                }
            }
            catch (Exception e)
            {
                ErrorSignal.FromCurrentContext().Raise(e);
                throw new InvalidOperationException(
                    string.Format("Error Missing FHA Number : {0}", e.InnerException), e.InnerException);
            }
            return View("~/Views/AssetManagement/ReserveForReplacementFormView.cshtml", r4rModel);
        }

        private void PopulateFHANumberList(ReserveForReplacementFormModel model)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                fhaNumberList = uploadDataManager.GetAllowedFhaByLenderUserId(UserPrincipal.Current.UserId);
            }
            else
            {
                fhaNumberList = uploadDataManager.GetAllowedFhasForUser(UserPrincipal.Current.UserId).ToList();
            }

            foreach (var fhaNumber in fhaNumberList)
            {
                model.AvailableFHANumbersList.Add(fhaNumber);
            }
        }

        private void GetDisclaimerText(ReserveForReplacementFormModel model)
        {
            model.DisclaimerText = reserveForReplacementManager.GetDisclaimerMsgByPageType("R4R");

        }

        private void PopulateAllFHANumberListForR4R(ReserveForReplacementFormModel model)
        {
            try
            {
                var fhaNumberList = new List<string>();
                fhaNumberList = reserveForReplacementManager.GetAllFHANumbersListForR4R();
                foreach (var fhaNumber in fhaNumberList)
                {
                    model.AvailableFHANumbersList.Add(fhaNumber);
                }
            }
            catch (Exception e)
            {

                ErrorSignal.FromCurrentContext().Raise(e);
                throw new InvalidOperationException(
                    string.Format("Error r4r submit: {0}", e.InnerException), e.InnerException);
            }
        }

        public ActionResult R4RChangeApprovedAmount(ReserveForReplacementFormModel model)
        {
            PopulateAllFHANumberListForR4R(model);
            return View("~/Views/AssetManagement/R4RChangeApprovedAmountForm.cshtml", model);
        }

        public string R4RDeny(int taskId, string denyReason)
        {
            var model = (ReserveForReplacementFormModel)(TempData["R4RFormData"] ?? new ReserveForReplacementFormModel());
            if (model == null)
            {
                var task = taskManager.GetTaskById(taskId);
                if (task.DataStore1.Contains("ReserveForReplacementFormModel"))
                {
                    try
                    {
                        var formUploadData = XmlHelper.Deserialize(typeof(ReserveForReplacementFormModel), task.DataStore1, Encoding.Unicode) as ReserveForReplacementFormModel;
                        if (formUploadData != null)
                        {
                            formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus) ? new TaskOpenStatusModel() :
                                XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as TaskOpenStatusModel;
                            formUploadData.IsReAssigend = task.IsReAssigned;
                            formUploadData.TaskGuid = task.TaskInstanceId;
                            formUploadData.Concurrency = taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
                            formUploadData.SequenceId = task.SequenceId;
                            formUploadData.AssignedBy = task.AssignedBy;
                            formUploadData.AssignedTo = task.AssignedTo;
                            formUploadData.TaskId = task.TaskId;

                            model = formUploadData;
                        }
                    }
                    catch (Exception e)
                    {

                        ErrorSignal.FromCurrentContext().Raise(e);
                        throw new InvalidOperationException(
                            string.Format("Error r4r submit: {0}", e.InnerException), e.InnerException);
                    }
                }
            }
            if (UpdateR4RDenial(model, denyReason))
            {
                return "Success";
            }
            return "Fail";
        }

        private bool UpdateR4RDenial(ReserveForReplacementFormModel model, string reason)
        {
            //karri#S238
            model.HudRemarks = "Request Denied.";
            var task = new TaskModel();
            var taskList = new List<TaskModel>();
            ViewAsPdf generatePdf;
            TaskFileModel taskFile = new TaskFileModel();
            IList<TaskFileModel> taskFileList = new List<TaskFileModel>();
            string year = string.Empty;
            string submitDate = string.Empty;
            var toEmail = model.AssignedBy;
            task.AssignedBy = UserPrincipal.Current.UserName;
            task.AssignedTo = model.AssignedBy;
            task.Notes = reason;
            task.StartTime = DateTime.UtcNow;
            task.MyStartTime = task.StartTime;
            task.TaskInstanceId = model.TaskGuid ?? Guid.NewGuid();
            task.TaskStepId = (int)TaskStep.R4RComplete;
            var taskOpenStatusModel = new TaskOpenStatusModel();
            taskOpenStatusModel.AddTaskOpenByUser(UserPrincipal.Current.UserName);
            task.TaskOpenStatus = XmlHelper.Serialize(taskOpenStatusModel, typeof(TaskOpenStatusModel), Encoding.Unicode);

            task.SequenceId = model.SequenceId.HasValue ? model.SequenceId.Value + 1 : 0;
            task.IsReAssigned = model.IsReAssigend;


            try
            {
                model.RequestStatus = (int)RequestStatus.Deny;
                model.TotalApprovedAmount = (decimal)0.0;
                //commented out to correct Bug 3073
                //model.HudRemarks = reason; // reason of deny as Hud remarks     
                model.AddnRemarks = reason;
                if (model.ServicerSubmissionDate.HasValue)
                {
                    year = model.ServicerSubmissionDate.Value.Year.ToString();
                    submitDate = model.ServicerSubmissionDate.Value.ToString("yyyyMMddHHmmss");
                }

                generatePdf = new ViewAsPdf("~/Views/AssetManagement/ReserveForReplacementFormReadOnlyView.cshtml", model);
                var binary = generatePdf.BuildPdf(ControllerContext);
                System.IO.File.WriteAllBytes(Server.MapPath("~/Templates/ReserveForReplacementForm.pdf"), binary);

                //#876harish added 11052020 API call for R4R 

                model.ActionTypeId = 86;
                var documentmodel = new AM_R4RAndNCREDocumentModel();//#876 11052020
                documentmodel.TransactionDocumentId = taskManager.GetTransactionId("R4RPDF").TransactionDocumentId;//#876 11052020
                var uploadmodel = new RestfulWebApiUploadModel()
                {
                    propertyID = model.PropertyId.ToString(),
                    indexType = "1",
                    indexValue = model.FHANumber,
                    pdfConvertableValue = "false",
                    documentType = documentmodel.TransactionDocumentId.ToString(),//#876 11052020
                                                                                  //documentType = DefaultDocID,
                    folderNames = "/" + model.PropertyId.ToString() + "/" + model.FHANumber + "/" + "Asset Management /" + "R4R" + "/" + DateTime.UtcNow.Year + "/" + DateTime.UtcNow.ToString("MM-dd-yyyy"),//#876 11052020
                    transactionType = model.ActionTypeId.ToString(), //#876 11052020
                    transactionStatus = "",//#876 11052020
                    transactionDate = DateTime.UtcNow.ToString("MM-dd-yyyy")//#876 11052020
                                                                                                 //folderNames = "/" + model.PropertyId.ToString() + "/" + model.FHANumber + "/Asset Management/R4R/" + year + "/" + submitDate + "/",
                };
                //old Api Payload call commented below line for new implementation ----harish 
                //var uploadmodel = new RestfulWebApiUploadModel()
                //{
                //    propertyID = model.PropertyId.ToString(),
                //    indexType = "1",
                //    indexValue = model.FHANumber,
                //    pdfConvertableValue = "false",
                //    //documentType = DefaultDocID,
                //    //folderNames = "/" + model.PropertyId.ToString() + "/" + model.FHANumber + "/Asset Management/R4R/" + year + "/" + submitDate + "/",
                //};
                var WebApiUploadResult = new RestfulWebApiResultModel();
                var request = new RestfulWebApiTokenResultModel();
                request = webApiTokenRequest.RequestToken();

                //var result = uploadApiManager.uploadSharepointPdfFile(uploadmodel, request.access_token, binary);
                ////#876 11052020
                var result = uploadApiManager.AssetManagementuploadSharepointPdfFile(uploadmodel, request.access_token, binary);
                Random randoms = new Random();
                var fileName = result.fileName == null ? "Reserve for Replacement Submission Form.pdf" : result.fileName;
                fileName = fileName.Replace("SharePointData", "Reserve for Replacement Submission Form");
                var fileSize = binary.Length;
                var UniqueId = randoms.Next(0, 99999);
                var systemFileName = model.FHANumber + "_" + model.TaskGuid.Value.ToString() + "_" + UniqueId + Path.GetExtension(fileName);
                System.IO.File.WriteAllBytes(Server.MapPath("~/Uploads/" + systemFileName), binary);

                taskFile = ControllerHelper.PopulateGroupTaskFileForSharepoint(fileName, fileSize, "Reserve for Replacement Submission Form", DateTime.UtcNow.ToString(), systemFileName.ToString(), model.TaskGuid.Value);
                if (result.status != null)
                {
                    if (result.status.ToLower() == "success")
                    {
                        taskFile.API_upload_status = result.status;
                        taskFile.Version = Convert.ToInt32(result.version);
                        taskFile.DocId = result.docId;
                        taskFile.DocTypeID = result.documentType;
                        taskFile.FileType = FileType.R4RSubmissionForm;
                        taskManager.SaveGroupTaskFileModel(taskFile);
                        var mappingmodel = new TaskFile_FolderMappingModel();
                        mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.R4R);
                        mappingmodel.TaskFileId = taskFile.TaskFileId;
                        mappingmodel.CreatedOn = DateTime.UtcNow;
                        taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                    }

                }  //Store information locally and regenerate it agian
                else
                {
                    taskFile.DocTypeID = DefaultDocID;
                    taskManager.SaveGroupTaskFileModel(taskFile);
                    var mappingmodel = new TaskFile_FolderMappingModel();
                    mappingmodel.FolderKey = Convert.ToInt32(ProdFolderStructure.R4R);
                    mappingmodel.TaskFileId = taskFile.TaskFileId;
                     mappingmodel.CreatedOn = DateTime.UtcNow;
                    taskFile_FolderMappingManager.AddTaskFile_FolderMapping(mappingmodel);
                }
                Prod_TaskXrefModel objProd_TaskXrefModel = new Prod_TaskXrefModel()
                {
                    TaskXrefid = Guid.NewGuid(),
                    TaskInstanceId = model.TaskGuid.Value,
                    TaskId = model.TaskId.Value,
                    AssignedBy = UserPrincipal.Current.UserId,
                    IsReviewer = false,
                    Status = Convert.ToInt32(TaskStep.R4RRequest),
                    AssignedDate = DateTime.UtcNow,
                    ViewId = Convert.ToInt32(ProductionView.WLM),
                    ModifiedOn = DateTime.UtcNow
                };
                prod_TaskXrefManager.AddTaskXref(objProd_TaskXrefModel);

                reserveForReplacementManager.UpdateR4RForm(model);
                task.DataStore1 = XmlHelper.Serialize(model, typeof(ReserveForReplacementFormModel), Encoding.Unicode);
                taskList.Add(task);
                taskFileList.Add(taskFile);
                taskManager.SaveTask(taskList, taskFileList);
                model.TaskId = reserveForReplacementManager.GetLatestTaskId(task.TaskInstanceId).TaskId;
                reserveForReplacementManager.UpdateTaskId(model);



                //email notification for R4R denial
                // send Email
                //emailManager.SendR4RDenyNotificationEmail(model, reason);
                backgroundJobManager.SendR4RDenyNotificationEmail(emailManager, model, reason);
            }
            catch (Exception e)
            {
                //return false;

                ErrorSignal.FromCurrentContext().Raise(e);
                throw new InvalidOperationException(
                    string.Format("Error r4r submit: {0}", e.InnerException), e.InnerException);

            }
            return true;
        }

        private void FHAListExceptSubmited(ReserveForReplacementFormModel model, int lenderID)
        {
            var fhaNumberList = new List<string>();
            if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
            {
                try
                {
                    var submitedFhaNumbers = reserveForReplacementManager.GetSubmitedFHANumbers(lenderID).Distinct().ToList();
                    var allFHAs = uploadDataManager.GetAllowedFhaByLenderUserId(UserPrincipal.Current.UserId);
                    fhaNumberList = allFHAs.Where(m => !submitedFhaNumbers.Contains(m)).ToList();
                }
                catch (Exception e)
                {

                    ErrorSignal.FromCurrentContext().Raise(e);
                    throw new InvalidOperationException(
                        string.Format("Error r4r submit: {0}", e.InnerException), e.InnerException);
                }

            }
            else
            {
                fhaNumberList = uploadDataManager.GetAllowedFhasForUser(UserPrincipal.Current.UserId).ToList();
            }

            foreach (var fhaNumber in fhaNumberList)
            {
                model.AvailableFHANumbersList.Add(fhaNumber);
            }
        }

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return true;
        }
    }
}
