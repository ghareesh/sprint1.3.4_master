﻿/*
// **** Include the following lines of code in cshtml file ****

    <script>
        var autocompleteTextBox = $("#ddlFHANumbers");
        var possibleValues = @Html.Raw(Json.Encode(Model.AvailableFHANumbersList));
    </script>
    <script src="/Scripts/Shared/autocomplete.js"></script>

// ****
*/

var selectedIndex = -1;
var searchTextTyped = "";
var fhaNumberLoaded = "";
var selectFHATxt = "Select FHA Number";
var maxOptionsToShow = 650;

function autocompleteMatch(text, arrValues) {
    for (var i = 0; i < arrValues.length; i++) {
        //siddesh
        if (arrValues[i].toUpperCase().indexOf(text.toUpperCase()) == 0) {
            selectedIndex = i;
            return arrValues[i];
        }
    }
    selectedIndex = -1;
    return null;
};

$(document).ready(function () {
    autocompleteTextBox1.on('keyup', function (event) {
        var textBox = $(this);

        function textboxSelect(start, end) {
            if (textBox[0].setSelectionRange) {
                textBox[0].focus();
                textBox[0].setSelectionRange(start, end);
            } else if (textBox.createTextRange) {
                var range = textBox.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }
        };

        switch (event.which) {
            case 13: //enter
            case 38: //up arrow
            case 40: //down arrow
            case 37: //left arrow
            case 39: //right arrow
            case 33: //page up
            case 34: //page down
            case 36: //home
            case 35: //end
            case 9: //tab
            case 27: //esc
            case 16: //shift
            case 17: //ctrl
            case 18: //alt
            case 20: //caps lock
                break;
            case 8: //backspace
            case 46: //delete
                searchTextTyped = textBox.val();
                break;
            default:
                var length = textBox.val().length;
                searchTextTyped = textBox.val();
                var textMatch = autocompleteMatch(textBox.val(), possibleValues1);
                if (textMatch != null) {
                    textBox.val(textMatch);
                    textboxSelect(length, textBox.val().length);
                }
                textBox.autocomplete('search', searchTextTyped);
                break;
        }
    }).autocomplete({
        source: function (request, callback) {
            updateResponse(searchTextTyped, callback);

            function updateResponse(searchText, callback) {
                var response = [];
                var max = possibleValues1.length < maxOptionsToShow ? possibleValues1.length : maxOptionsToShow;
                for (var i = 0; i < max; i++) {
                    if (possibleValues1[i].indexOf(searchText) > -1) {
                        response.push(possibleValues1[i]);
                    }
                }
                callback(response);
            }
        },
        minLength: 0,
        scroll: true
    }).focus(function () {
        var textBox = $(this);
        if (textBox.val() == selectFHATxt) {
            textBox.val('');
        }
        searchTextTyped = textBox.val();
        textBox.autocomplete('search', textBox.val());
    }).on('autocompleteselect', function (event, ui) {
        searchTextTyped = ui.item.value;
        autocompleteMatch(searchTextTyped, possibleValues1);
    }).bind('mouseup', function (event) {
        var textBox = $(this);
        var oldValue = textBox.val();

        if (oldValue == "") {
            fhaNumberLoaded = oldValue;
            return;
        }

        setTimeout(function () {
            var newValue = textBox.val();

            if (newValue == "") {
                searchTextTyped = newValue;
                textBox.autocomplete('search', searchTextTyped);
            }
        }, 1);
    });
});

/*
    // **** To Override on autocompleteselect (on change) event, always include the following lines of code ****

    autocompleteTextBox.on("autocompleteselect", function(event, ui) {
        searchTextTyped = ui.item.value;
        autocompleteMatch(searchTextTyped, possibleValues);
    });

    // ****
*/