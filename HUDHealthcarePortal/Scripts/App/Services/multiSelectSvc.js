﻿multiSelectModule.factory('multiSelectSvc', ['$http', function ($http) {
    return {
        getRolesByCategory: function (userTypeID) {
            return $http.get('/Role/GetRolesByUserType', { params: { userTypeId: userTypeID } })
        },

        getAllLenders: function () {
            return $http.get('/Role/GetLenders');
        },

        getFHAsFromLenders: function () {
            return $http({
                url: '/Role/GetFHAsFromLenders',
                method: "GET",
                headers: { 'Content-Type': 'application/json' }
            });
        },

        GetSelectedAndAvailableFhasForOar: function () {
            return $http({
                url: '/Role/GetSelectedAndAvailableFhasForOar',
                method: "GET",
                headers: { 'Content-Type': 'application/json' }
            });
        },

        getFHAsFromLamOrBamOrAdmin: function () {
            return $http({
                url: '/Role/GetFhAsFromLamOrBamOrAdmin',
                method: "GET",
                headers: { 'Content-Type': 'application/json' }
            });
        },

        //karri#354
        getServicerFHAsFromLamOrBamOrAdmin: function (serviceruserid) {
            //alert(serviceruserid);

            return $http.get('/Role/GetServicerFHANumbersFromLamOrBamOrAdmin', { params: { servicerUserId: serviceruserid } })

            //return $http({
            //    url: '/Role/GetServicerFHANumbersFromLamOrBamOrAdmin',
            //    method: "GET",
            //    data: { servicerUserId: serviceruserid },
            //    headers: { 'Content-Type': 'application/json' }
            //});
        },

        getFHAsForWLM: function (wlmid) {
            //return $http({
            //    url: '/Role/GetFhasByWLMId',
            //    method: "GET",
            //    data: JSON.stringify(wlmid),
            //    headers: { 'Content-Type': 'application/json' }
            //});
            return $http.get('/Role/GetFhasByWLMId', { params: { wlmid: wlmid } })
        },


        saveSelectedRoles: function (userRoles) {
            return $http({
                url: '/Role/SaveUserRoleChanges',
                method: "POST",
                data: JSON.stringify(userRoles),
                headers: { 'Content-Type': 'application/json' }
            });
        },

        createUserRoles: function (userRoles) {
            return $http({
                url: '/Role/CreateUserRoles',
                method: "POST",
                data: JSON.stringify(userRoles),
                headers: { 'Content-Type': 'application/json' }
            });
        },

        saveSelectedLenders: function (selectedLenders) {
            return $http({
                url: '/Role/SaveSelectedLenders',
                method: "POST",
                data: JSON.stringify(selectedLenders),
                headers: { 'Content-Type': 'application/json' }
            });
        },

        createUserLARLinks: function (selectedFhas) {
            return $http({
                url: '/Role/CreateUserLARLinks',
                method: "POST",
                data: JSON.stringify(selectedFhas),
                headers: { 'Content-Type': 'application/json' }
            });
        },

        //naveen

        deletuserFHAs: function (selectedFhas, Userid) {
            return $http({
                url: '/Role/DeleteFHAs',
                method: "POST",
                //data: JSON.stringify(selectedFhas),
                data: { selectedFhas: selectedFhas, Userid: Userid },
                headers: { 'Content-Type': 'application/json' }
            });
        },

        createUserLARLinksForWLM: function (selectedFhas, wlmid) {
            debugger;
            return $http({
                url: '/Role/CreateUserLARLinksForWLM',
                method: "POST",
                // data: JSON.stringify(selectedFhas),
                data: { selectedFhas: selectedFhas, wlmid: wlmid },
                headers: { 'Content-Type': 'application/json' }
            });
        },

        saveUpdatedOarFhas: function (selectedFhas) {
            return $http({
                url: '/Role/SaveUpdatedOarFhas',
                method: "POST",
                data: JSON.stringify(selectedFhas),
                headers: { 'Content-Type': 'application/json' }
            });
        },
        getFhasReadyForApplication: function () {
            return $http({
                url: '/Role/GetFhasReadyForApplication',
                method: "GET",
                headers: { 'Content-Type': 'application/json' }
            });
        },
        saveSelectedFhasForIC: function (selectedFhas) {
            return $http({
                url: '/Role/SaveSelectedFhasForIC',
                method: "POST",
                data: JSON.stringify(selectedFhas),
                headers: { 'Content-Type': 'application/json' }
            });
        },
        getSelectedAndAvailableFhasForIC: function () {
            return $http({
                url: '/Role/GetSelectedAndAvailableFhasForIC',
                method: "GET",
                headers: { 'Content-Type': 'application/json' }
            });
        },
        saveUpdatedICFhas: function (selectedFhas) {
            return $http({
                url: '/Role/SaveUpdatedICFhas',
                method: "POST",
                data: JSON.stringify(selectedFhas),
                headers: { 'Content-Type': 'application/json' }
            });
        }
    }
}]);
