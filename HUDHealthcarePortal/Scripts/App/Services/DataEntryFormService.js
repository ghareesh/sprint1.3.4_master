﻿questionModule.factory('dataEntryFormService', [
    '$http', function($http) {
        return {

            GetAllProjectActions: function() {
                var response = $http({
                    method: 'GET',
                    url: '/Account/GetAllProjectActions'
                });
                return response;
            },

            GetAllQuestions: function(idvalue) {
                var response = $http({
                    method: 'POST',
                    url: '/Account/GetAllQuestions',
                    data: JSON.stringify({ projectActionTypeId: idvalue })
                });

                return response;
            },

           AddQuestion: function (questionViewModel) {
               var response = $http({
                   method: "post",
                   url: "/Account/AddQuestion",
                   data: JSON.stringify(questionViewModel),
                   dataType: "json"
               });
               return response;
           }
       }
    }
]);