﻿/*
// **** Include the following lines of code in cshtml file ****

    <script>
        var autocompleteTextBox = $("#ddlFHANumbers");
        var possibleValues = @Html.Raw(Json.Encode(Model.AvailableFHANumbersList));
    </script>
    <script src="/Scripts/Shared/autocomplete.js"></script>

// ****
*/

var selectedIndexs = -1;
var searchFhaNumber = "";
var selectedProjectName = "";
var fhaNumberLoad = "";
var selectFHATxt = "Select FHA Number";
var maxOptionsToShow = 650;

function autocompleteMatch(text, arrValues) {
    for (var i = 0; i < arrValues.length; i++) {
        if (arrValues[i].indexOf(text) == 0) {
        //if (arrValues[i].toUpperCase().indexOf(text.toUpperCase()) == 0) {

            selectedIndexs = i;
            return arrValues[i];
        }
    }
    selectedIndexs = -1;
    return null;
};

$(document).ready(function () {
    autocompleteTextBox.on('keyup', function (event) {
        var textBox = $(this);

        function textboxSelect(start, end) {
            if (textBox[0].setSelectionRange) {
                textBox[0].focus();
                textBox[0].setSelectionRange(start, end);
            } else if (textBox.createTextRange) {
                var range = textBox.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }
        };

        switch (event.which) {
            case 13: //enter
            case 38: //up arrow
            case 40: //down arrow
            case 37: //left arrow
            case 39: //right arrow
            case 33: //page up
            case 34: //page down
            case 36: //home
            case 35: //end
            case 9: //tab
            case 27: //esc
            case 16: //shift
            case 17: //ctrl
            case 18: //alt
            case 20: //caps lock
                break;
            case 8: //backspace
            case 46: //delete
                searchFhaNumber = textBox.val();
                break;
            default:
                var length = textBox.val().length;
                searchFhaNumber = textBox.val();
                var textMatch = autocompleteMatch(textBox.val(), FHApossibleValues);
                if (textMatch != null) {
                    textBox.val(textMatch);
                    textboxSelect(length, textBox.val().length);
                }
                textBox.autocomplete('search', searchFhaNumber);
                break;
        }
    }).autocomplete({
        source: function (request, callback) {
            updateResponse(searchFhaNumber, callback);

            function updateResponse(searchText, callback) {
                var response = [];
                var max = FHApossibleValues.length < maxOptionsToShow ? FHApossibleValues.length : maxOptionsToShow;
                for (var i = 0; i < max; i++) {
                    if (FHApossibleValues[i].indexOf(searchText) > -1) {
                        response.push(FHApossibleValues[i]);
                    }
                }
                callback(response);
            }
        },
        minLength: 0,
        scroll: true
    }).focus(function () {
        var textBox = $(this);
        if (textBox.val() == selectFHATxt) {
            textBox.val('');
        }
        searchFhaNumber = textBox.val();
        textBox.autocomplete('search', textBox.val());
    }).on('autocompleteselect', function (event, ui) {
        searchFhaNumber = ui.item.value;
        autocompleteMatch(searchFhaNumber, FHApossibleValues);
    }).bind('mouseup', function (event) {
        var textBox = $(this);
        var oldValue = textBox.val();

        if (oldValue == "") {
            fhaNumberLoad = oldValue;
            return;
        }

        setTimeout(function () {
            var newValue = textBox.val();

            if (newValue == "") {
                searchFhaNumber = newValue;
                textBox.autocomplete('search', searchFhaNumber);
            }
        }, 1);
    });
});

//hareesh 231 

function autocompleteMatchs(text, arrValues) {
    for (var i = 0; i < arrValues.length; i++) {
        if (arrValues[i].toUpperCase().indexOf(text.toUpperCase()) == 0) {
        //if (arrValues[i].indexOf(text) == 0) {
            selectedIndex = i;
            return arrValues[i];
        }
    }
    selectedIndex = -1;
    return null;
};

$(document).ready(function () {
    autocompleteTextBoxs.on('keyup', function (event) {
        var textBox = $(this);

        function textboxSelect(start, end) {
            if (textBox[0].setSelectionRange) {
                textBox[0].focus();
                textBox[0].setSelectionRange(start, end);
            } else if (textBox.createTextRange) {
                var range = textBox.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }
        };

        switch (event.which) {
            case 13: //enter
            case 38: //up arrow
            case 40: //down arrow
            case 37: //left arrow
            case 39: //right arrow
            case 33: //page up
            case 34: //page down
            case 36: //home
            case 35: //end
            case 9: //tab
            case 27: //esc
            case 16: //shift
            case 17: //ctrl
            case 18: //alt
            case 20: //caps lock
                break;
            case 8: //backspace
            case 46: //delete
                selectedProjectName = textBox.val();
                break;
            default:
                var length = textBox.val().length;
                selectedProjectName = textBox.val();
                var textMatch = autocompleteMatchs(textBox.val(), AllpossibleValues);
                if (textMatch != null) {
                    textBox.val(textMatch);
                    textboxSelect(length, textBox.val().length);
                }
                textBox.autocomplete('search', selectedProjectName);
                break;
        }
    }).autocomplete({
        source: function (request, callback) {
            updateResponse(selectedProjectName, callback);

            function updateResponse(searchText, callback) {
                var response = [];
                var max = AllpossibleValues.length < maxOptionsToShow ? AllpossibleValues.length : maxOptionsToShow;
                for (var i = 0; i < max; i++) {
                    if (AllpossibleValues[i].indexOf(searchText) > -1) {
                        response.push(AllpossibleValues[i]);
                    }
                }
                callback(response);
            }
        },
        minLength: 0,
        scroll: true
    }).focus(function () {
        var textBox = $(this);
        if (textBox.val() == selectFHATxt) {
            textBox.val('');
        }
        selectedProjectName = textBox.val();
        textBox.autocomplete('search', textBox.val());
    }).on('autocompleteselect', function (event, ui) {
        selectedProjectName = ui.item.value;
        autocompleteMatchs(selectedProjectName, AllpossibleValues);
    }).bind('mouseup', function (event) {
        var textBox = $(this);
        var oldValue = textBox.val();

        if (oldValue == "") {
            fhaNumberLoaded = oldValue;
            return;
        }

        setTimeout(function () {
            var newValue = textBox.val();

            if (newValue == "") {
                selectedProjectName = newValue;
                textBox.autocomplete('search', selectedProjectName);
            }
        }, 1);
    });
});








/*
    // **** To Override on autocompleteselect (on change) event, always include the following lines of code ****

    autocompleteTextBox.on("autocompleteselect", function(event, ui) {
        searchTextTyped = ui.item.value;
        autocompleteMatch(searchTextTyped, possibleValues);
    });

    // ****
*/