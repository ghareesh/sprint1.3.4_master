﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{


    public class ProductionReassignViewModel
    {
        public ProductionReassignViewModel()
        {

        }

        public string fhanumber { get; set; }
        public int Propertyid { get; set; }
        public string ProjectName { get; set; }
        public string LoanType { get; set; }
        public string Comment { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? DaysActive { get; set; }
        ////public bool IsProjectActionOpen { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? FhaRequestType { get; set; }

        public string LenderName { get; set; }
        public string Lenderrole { get; set; }
        public Guid? ParentChildInstanceId { get; set; }
        public string Reassignedto { get; set; }
        // public string Reassignedby { get; set; }
        public DateTime? ReassignedDate { get; set; }
        public int StatusId { get; set; }
        public string ProcessedBy { get; set; }
        //public int? AdditionalInfoCount { get; set; }
        public string UserRoleName { get; set; } // User Story 1904
        public string RAI { get; set; }

        public string ProductionTaskType { get; set; }
        //public string HudProdWorkloadManagerName { get; set; }
        public string viewname { get; set; }
        public Guid? Groupid { get; set; }
        public string TaskName { get; set; }
        public string Status { get; set; }
        //harish added 20042020
        public int CreatedBy { get; set; }
        public int Duration { get; set; }
    }
}
