﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;


namespace Model.Production
{
    public class Prod_OPAHistoryViewModel : OPAHistoryViewModel
    {
         #region Production Application
        public string id { get; set; }
        public string folderNodeName { get; set; }
        public int level { get; set; }
        public string parent { get; set; }
        public bool isLeaf { get; set; }
        public bool expanded { get; set; }
        public bool loaded { get; set; }
        public string DocTypeID { get; set; }
        public string DocTypesList { get; set; }
        public int FolderKey { get; set; }
        public int Status { get; set; }
        public string SubfolderSequence { get; set; }
        public int FolderSortingNumber { get; set; }
        public int CreatedBy { get; set; }

        public bool isUploadedByMe { get; set; }
        public int PageTypeId { get; set; }
        public bool active { get; set; }
        public string role { get; set; }
        public string RoleName { get; set; }
        public int? FolderStructureKey { get; set; }
        #endregion
    }
}
