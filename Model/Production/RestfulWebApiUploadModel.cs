﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Model.Production
{
  public class RestfulWebApiUploadModel
    {
        public string propertyID { get; set; }
        public string documentType { get; set; }
        public string indexType { get; set; }
        public string indexValue { get; set; }
        public string pdfConvertableValue { get; set; }
        public string ContentType { get; set; }
        //public HttpPostedFileBase document {get; set;}
        public byte[] documents { get; set; }
        public int folderKey { get; set; }
        public string folderNames { get; set; }
        // harish added new id for folder update
        public string transactionType { get; set; }
        public string transactionDate { get; set; }
        public string transactionStatus { get; set; }
    }
}
