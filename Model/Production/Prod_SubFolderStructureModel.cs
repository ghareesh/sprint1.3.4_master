﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class Prod_SubFolderStructureModel
    {
        public int? FolderKey { get; set; }
        public string FolderName { get; set; }
        public int? ParentKey { get; set; }
        public int? ViewTypeId { get; set; }
        public int? FolderSortingNumber { get; set; }
        public string SubfolderSequence { get; set; }
        public string FhaNo { get; set; }
        public string ProjectNo { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? PageTypeId { get; set; }
        public Guid GroupTaskInstanceId { get; set; }
    }
    public class Prod_Assainedto
    {
        public int? UserID { get; set; }
        public string UserName { get; set; }
    }
}


