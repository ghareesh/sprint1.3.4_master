﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Model.Production
{
    public class RestfulWebApiUpdateModel
    {
        public string propertyId { get; set; }
        public string folderName { get; set; }
        public string documentType { get; set; }
        public string documentStatus { get; set; }
        public string indexType { get; set; }
        public string indexValue { get; set; }
        public string docId { get; set; }
        public HttpPostedFileBase document { get; set; }

        // harish added new id for folder update
        public string transactionId { get; set; }
    }   
}
