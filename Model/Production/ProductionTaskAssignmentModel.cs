﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class ProductionTaskAssignmentModel : Prod_TaskXrefModel
    {
        public int CurrentUserId { get; set; }
        public int Role { get; set; }
        public int AssignedToUserId { get; set; }
        public string  SelectedViewIds { get; set; }
        public string UserName { get; set; }

        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public bool isWLM { get; set; }
        public bool IsParentAsssigned { get; set; }
        public bool IsChildAssigned { get; set; }
        public string TaskName { get; set; }
        public int AssignedToViewId { get; set; }
        public int PageTypeId { get; set; }

    }

    public class ProductionQueueLenderInfo
    {
        public Guid? TaskinstanceId { get; set; }
        public string projectName { get; set; }
        public string CreatedBy { get; set; }
        public string LenderName { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool IsLIHTC { get; set; }
        public string LoanType { get; set; }
        public decimal LoanAmount { get; set; }
        // naresh add prodqu2
        //public bool Opportunityzone { get; set; }

        public bool opportunity_zone { get; set; }

        public string Portfolio_Name { get; set; }



        //Addedd by naresh prodQ2 18/12/2019
        public Guid ClosingInstanceID { get; set; }
        public string PropertyName { get; set; }
        public int TaskStepId { get; set; }
    }
}
