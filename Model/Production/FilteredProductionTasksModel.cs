﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Model.Production
{
    public class FilteredProductionTasksModel
    {
        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public string TaskName { get; set; }
        public int SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public int PageTypeId { get; set; }
        public DateTime StartTime { get; set; }
        public string Lender { get; set; }
        public DateTime ModofiedOn { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public int Duration { get; set; }
        public string LoanType { get; set; }
        public decimal LoanAmountInt { get; set; }
        public bool? opportunity_zone { get; set; }
        public string Portfolio_Name { get; set; }
        public string projectName { get; set; }
        public bool? IsLIHTC { get; set; }
        public string DaysInQueue { get { return Duration > 1 ? string.Format("{0} Days", Duration) : string.Format("{0} Day", Duration); } }
        public string LoanAmount { get { return LoanAmountInt.ToString("C", CultureInfo.CurrentCulture); } }
        public string PropertyName { get; set; }
    }
    public class FhaSubmittedLendersModel
    {
        public int LenderID { get; set; }
        public string Lender_Name { get; set; }
    }

    //karri#672
    public class AssignedunderwriternamesModel
    {
        public int? UserId { get; set; }
        public string UserName { get; set; }
    }
}
