﻿using HUDHealthcarePortal.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Model.Production
{
    //class LenderProductionReasignReportModel
    //{
    //}
    public class LenderProductionReasignReportModel : ReportModel
    {
        public LenderProductionReasignReportModel(ReportType reportType)
            : base(reportType)
        {
            ReportType = reportType;
        }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public bool IsDateDefault { get; set; }
        public ReportType ReportType { get; set; }
        public ReportLevel ReportLevel { get; set; }
        public MultiSelectList Status { get; set; }
        public MultiSelectList ApplicationOrTask { get; set; }
        public IList<LenderProductionReassignViewModel> ReassignGridProductionlList { get; set; }
        public MultiSelectList ProgramTypes { get; set; }
        public MultiSelectList PamStatusList { get; set; }
        public MultiSelectList PageTypes { get; set; }
        public MultiSelectList ProductionUsers { get; set; }
        public MultiSelectList ProductionPageTypes { get; set; }
        public MultiSelectList ProductionProjecTypeList { get; set; }
        public string ApplicationTaskLevel { get; set; }
        // harish added 15042020
        public MultiSelectList LenderUser { get; set; }
    }
}
