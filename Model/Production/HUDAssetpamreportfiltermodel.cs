﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Collections;
using HUDHealthcarePortal.Model;

namespace Model.Production
{
    public class HUDAssetpamreportfiltermodel
    {
        public int FilterIDS { get; set; }
        public string wlmUsers { get; set; }
        public string aeUsers { get; set; }
        public string isoUsers { get; set; }
        public string Status { get; set; }
        public string projectAction { get; set; }
        public string propertyname { get; set; }
        public string fhanumber { get; set; }
        public string AssignedTo { get; set; }
        public string Actionopen { get; set; }
        public string daysopen { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedON { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedON { get; set; }
        public string UserRole { get; set; }
        //[StringLength(50)]

    }
}
