﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Model.Production
{
	public class ProdPAMReportStatusGridViewModel
	{
        //public string ReportDate { get { return DateTime.Now.ToString("MMMM dd, yyyy"); } }
        public string ReportDate { get { return DateTime.UtcNow.ToString("MMMM dd, yyyy"); } }
        public int TotalNumberOfProjects { get; set; }
		public int TotalNumberOf223f { get; set; }
		public int TotalNumberOf223a7 { get; set; }
		public int TotalNumberOf223d { get; set; }
		public int TotalNumberOf223i { get; set; }
		public int TotalNumberOf241a { get; set; }
		public int TotalNumberOfNC { get; set; }
		public int TotalNumberOfSR { get; set; }
		public int TotalNumberOfIRR { get; set; }
		public int TotalNumberOfFY { get; set; }
		public int TotalFHAInqueue { get; set; }
		public int TotalFHAInprocess { get; set; }
		public int TotalFHAComplete { get; set; }
		public int TotalWaitingApplication { get; set; }
		public int TotalApplicationInqueue { get; set; }
		public int TotalApplicationInprocess { get; set; }
		public int TotalApplicationComplete { get; set; }
		public int TotalWaitingClosing { get; set; }
		public int TotalClosingInqueue { get; set; }
		public int TotalClosingInprocess { get; set; }
		public int TotalClosingComplete { get; set; }
		public int TotalExecutedDocUploaded { get; set; }
		public int TotalAppraiserAssigned { get; set; }
		public int TotalEnvironmentalistAssigned { get; set; }
		public int TotalTitleSurveyAssigned { get; set; }
		public int Total290Assigned { get; set; }
		public int Total290Complete { get; set; }
		public int TotalClosing { get; set; }
		public int LoanOpened { get; set; }

		public int AmendmentInqueue { get; set; }
		public int AmendmentInProcess { get; set; }
		public int AmendmentComplete { get; set; }
		public HUDPamReportFiltersModel hudPamReportFilters { get; set; }
        [XmlIgnore]
        public List<String> TotalAvailableFHANumbersList { get; set; }
        [XmlIgnore]
        public List<String> TotalAvailableProjectNamesList { get; set; }
        //#701 Added by siddu//
        public HUDPamReportFiltersModel hudPamReportNEWFilters { get; set; }
    }
}
