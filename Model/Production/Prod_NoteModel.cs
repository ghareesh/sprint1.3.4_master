﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
   public class Prod_NoteModel
    {
        public int NoteId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public string Note { get; set; }
        public int CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedByUserName { get; set; }
    }
}
