﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
   public  class Prod_FolderStructureModel
    {
        public int FolderKey { get; set; }
        public string FolderName { get; set; }
        public int? ParentKey { get; set; }
        public int level { get; set; }
        public int ViewTypeId { get; set; }
        public string SubfolderSequence { get; set; }
    }
}
