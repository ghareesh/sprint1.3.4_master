﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
   public class Prod_MessageModel
    {
       public bool status { get; set; }
       public string  Message {get; set;}
       public string  HtmlRaw {get; set;}
                
    }
}
