﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Xml.Serialization;
using HUDHealthcarePortal.Model.ProjectAction;
using HUDHealthcarePortal.Model;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model;

//siddu #825
namespace HUDHealthcarePortal.Model
{
    public class Fedralholidaylist
    {
        [Key]
        public int Holiday_id { get; set; }
        public string FedralHoliday { get; set; }
        public DateTime Date { get; set; }
    }
}