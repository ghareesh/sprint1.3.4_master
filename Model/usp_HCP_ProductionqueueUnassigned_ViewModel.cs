﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public partial class usp_HCP_ProductionqueueUnassigned_ViewModel
    {

        public string projectName { get; set; }
        public string Portfolio_Name { get; set; }
        public Guid TaskInstanceId { get; set; }
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public int SequenceId { get; set; }
        public int? PageTypeId { get; set; }
        public System.DateTime StartTime { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }

        public string Lender { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Type { get; set; }
        public string DaysInQueue { get; set; }
        public bool? IsLIHTC { get; set; }
        public bool? opportunity_zone { get; set; }
        public string LoanType { get; set; }
        public decimal LoanAmount { get; set; }

    }
}
