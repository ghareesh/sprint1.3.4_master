﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using HUDHealthcarePortal.Model;
using Model;

namespace HUDHealthcarePortal.Model.AssetManagement
{
    [Serializable]
    public class NonCriticalRepairsViewModel
    {
        [XmlIgnore]
        public IList<String> AvailableFHANumbersList { get; set; }

        public Guid NonCriticalRequestId { get; set; }
        public int PropertyId { get; set; }
        public int LenderId { get; set; }
       
        public string FHANumber { get; set; }
        public string LenderName { get; set; }
        public string PropertyName { get; set; }
        public AddressModel PropertyAddress { get; set; }
        public bool? IsLenderDelegate { get; set; } 
        //[DataType(DataType.Currency)]
        public decimal PaymentAmountRequested { get; set;}
       
       
        public bool IsFinalDraw { get; set; }
      
        public DateTime? ClosingDate { get; set; }

        [XmlIgnore]
        [Display(Name = "92464")]
        public HttpPostedFileBase File92464 { get; set; }
        public string FileName92464 { get; set; }
        public string File92464FullName { get; set; }

        [XmlIgnore]
        [Display(Name = "92117")]
        public HttpPostedFileBase File92117 { get; set; }
        public string FileName92117 { get; set; }
        public string File92117FullName { get; set; }

        [XmlIgnore]
        [Display(Name = "PCNA")]
        public HttpPostedFileBase FilePCNA { get; set; }
        public string FileNamePCNA { get; set; }
        public string FilePCNAFullName { get; set; }

        [XmlIgnore]
        [Display(Name = "Scope Certification")]
        public HttpPostedFileBase FileScopeCertification { get; set; }
        public string FileScopeCertificationFileName { get; set; }
        public string FileScopeCertificationFullName { get; set; }

        [XmlIgnore]
        [Display(Name = "Clear Title")]
        public HttpPostedFileBase FileClearTitle { get; set; }
        public string FinalInspectionReport { get; set; }
        public string FileClearTitleFullName { get; set; }

        [XmlIgnore]
        [Display(Name = "Default RequestExtension")]
        public HttpPostedFileBase FileDefaultRequestExtension { get; set; }
        public string FileDefaultRequestExtensionFileName{ get; set; }
        public string FileDefaultRequestExtensionFullName { get; set; }

        [XmlIgnore]
        [Display(Name = "Contract")]
        public HttpPostedFileBase FileContract { get; set; }
        public string FileContractFileName { get; set; }
        public string FileContractFullName { get; set; }

        public int NumberDraw { get; set; }
        public bool CanProvideAllPhotosInvoices { get; set; }

        public string Reason { get; set; }
        public string ServicerRemarks { get; set; }

        // storing the serialized data, files, concurrency details
        public Guid? TaskGuid { get; set; }
        public int? SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public TaskOpenStatusModel TaskOpenStatus { get; set; }
        public byte[] Concurrency { get; set; }

        public NonCriticalRepairsViewModel()
        {
            
        }

     
        public int? SubmitByUserId { get; set; }
        public int? SubmitToUserId { get; set; }

        public DateTime? ApprovedDate { get; set; }
        public DateTime? SubmittedDate { get; set; }

        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }

        public string ManageMode { get; set; }
        public decimal? ApprovedAmount { get; set; }
        public decimal? NonCriticalCurrentBalance { get; set; }
      
      
        public bool? IsSubmitMode { get; set; }
        public int Decision { get; set; }
        public bool? IsApprove { get; set; }
        public bool? IsDeny { get; set; }
        public bool? IsConcurrentUserUpdateFound { get; set; }
        public int RequestStatus { get; set; }
        public bool IsDisplayAcceptPopup { get; set; }

        [Required(ErrorMessage = "This field is required")]// User Story 1901
        public bool IsAgreementAccepted { get; set; }
        public bool IsPAMReport { get; set; }
        public int? TaskId { get; set; }

        public decimal? NonCriticalAccountBalance { get; set; }
        public bool IsNCREAmountValid { get; set; }
        public bool IsScopeOfWorkChanged { get; set; }
        public bool IsThisAnAdvance { get; set; }
        public bool IsRequestAutoApproved { get; set; }
        public string TroubledCode { get; set; }
        public bool IsExtensionAttachmentRequired { get; set; }
        public bool IsFile92464Uploaded { get; set; }
        public bool IsFile92117Uploaded { get; set; }

        public bool IsFilePCNAUploaded { get; set; }
        public bool IsNCRRequestPending { get; set; }
         [XmlIgnore]
        public IList<NonCriticalRulesChecklistModel> RulesCheckList { get; set;}
        [XmlIgnore]
        public IList<NonCriticalReferReasonModel> ReferalReasons{ get; set; }
        public bool IsFileReqExtnUploaded { get; set; }
        public decimal? NCRECurrentBalance { get; set; }

        [XmlIgnore]
        public IList<TransactionModel> Transactions{ get; set; }
        public bool IsTransactionLedger { get; set; }
        public bool? IsReassigned { get; set; }
        public bool IsUpdateiREMS { get; set; }
        
        //User Story 1904
        public bool IsLenderPAMReport { get; set; }

        //User Story 1901 
        public string DisclaimerText { get; set; }

        // harish added 08052020
        public Guid? TaskInstanceId { get; set; }

        // harish added 08052020
        public int ActionTypeId { get; set; }
    }
}
