﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using Elmah;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.Model.AssetManagement
{
    /// <summary>
    /// This is the model for Reserve for Replacement form
    /// </summary>
    [Serializable]
    public class ReserveForReplacementFormModel
    {
        public Guid R4RId { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? BorrowersRequestDate { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public DateTime? ServicerSubmissionDate { get; set; }

        public string FHANumber { get; set; }

        [XmlIgnore]
        public IList<String> AvailableFHANumbersList { get; set; }

        public string PropertyName { get; set; }

        public int PropertyId { get; set; }

        public AddressModel PropertyAddress { get; set; }

        public bool IsRequestForAdvance { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public int? NumberOfUnits { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Currency)]
        public decimal? ReserveAccountBalance { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ReserveAccountBalanceAsOfDate { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Currency)]
        [System.ComponentModel.DataAnnotations.Compare("ReserveAccountBalance", ErrorMessage = "The Reserve account balance and confirmation reserve account balance do not match.")]
        public decimal? ConfirmReserveAccountBalance { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Currency)]
        public decimal? TotalPurchaseAmount { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Currency)]
        public decimal? TotalRequestedAmount { get; set; }
        
        [DataType(DataType.Currency)]
        public decimal? TotalApprovedAmount { get; set; }

		public decimal? ChangeApprovedAmount { get; set; }


		public bool IsSingleItemExceedsFiftyThousand { get; set; }

        public bool IsPurchaseYearOlderThanR4RRequest { get; set; }

        public bool IsCommentEntered { get; set; }

        public string ServicerRemarks { get; set; }

        public string HudRemarks { get; set; }

        public string AddnRemarks { get; set; }

        public string FileName9250 { get; set; }
        public string FileName9250A { get; set; }
        public string FileNameInvoices { get; set; }
        public string FileNameReceipts { get; set; }
        public string FileNameContracts { get; set; }
        public string FileNamePictures { get; set; }
        public string FileNameOthers { get; set; }

        [XmlIgnore]
        [Display(Name = "9250")]
        public HttpPostedFileBase File9250 { get; set; }

        [XmlIgnore]
        [Display(Name = "9250A")]
        public HttpPostedFileBase File9250A { get; set; }

        [XmlIgnore]
        [Display(Name = "Invoice")]
        public HttpPostedFileBase InvoiceFile { get; set; }

        [XmlIgnore]
        [Display(Name = "Receipt")]
        public HttpPostedFileBase ReceiptFile { get; set; }

        [XmlIgnore]
        [Display(Name = "Contract")]
        public HttpPostedFileBase ContractFile { get; set; }

        [XmlIgnore]
        [Display(Name = "Picture")]
        public HttpPostedFileBase PictureFile { get; set; }

        [XmlIgnore]
        [Display(Name = "Other")]
        public HttpPostedFileBase OtherFile { get; set; }

        public string AttachmentsDescription { get; set; }

        public bool IsLoggedInUserLender { get; set; }

        public bool IsFhaNumberMissing { get; set; }

        public bool IsEligibleForAutomaticApproval { get; set; }

        public bool Is9250Required { get; set; }

        public bool Is9250ARequired { get; set; }

        public bool IsEntirePackageRequired { get; set; }

        public string ReacScore { get; set; }

        public string TroubledCode { get; set; }

        public bool IsNumberOfUnitsInRange { get; set; }

        public bool IsR4RWithin90Days { get; set; }

        public string ActiveDecCaseInd { get; set; }

        public bool IsDisplayAcceptPopup { get; set; }

         [Required(ErrorMessage = "This field is required")]
        public bool IsAgreementAccepted { get; set; }

        public bool IsDisplayAcceptPopupForAe { get; set; }

        public bool IsAdditionalCommentsEntered { get; set; }

        public int RequestStatus { get; set; }

        public bool IsDisplayFhaPopup { get; set; }

        [XmlIgnore]
        public int? SubmitByUserId { get; set; }
        [XmlIgnore]
        public int? SubmitToUserId { get; set; }

        // task related fields
        public Guid? TaskGuid { get; set; }
        public int? SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public TaskOpenStatusModel TaskOpenStatus { get; set; }
        public byte[] Concurrency { get; set; }

        public bool? IsConcurrentUserUpdateFound { get; set; }

        public int? TaskId { get; set; }
        public bool IsPAMReport { get; set; }
        public bool IsUpdateiREMS { get; set; }
        public bool IsRemodelingProposed { get; set; }
        public bool IsMortgagePaymentsCovered { get; set; }
        public ReserveForReplacementFormModel()
        {
            AvailableFHANumbersList = new List<string>();
        }

        public bool? IsReAssigend { get; set; }
        public bool? IsLenderDelegate { get; set; }
        public bool? IsSuspension { get; set; }
        public string DisclaimerText { get; set; }
        public bool IsLenderPAMReport { get; set; }
        public bool TransAccessStatus { get; set; }
        public Guid? TaskInstanceId { get; set; }
		public bool? IsHudRemarks { get; set; }
        //harish added new transactionid 06052020
        public int ActionTypeId { get; set; }

    }


}
