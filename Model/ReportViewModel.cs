﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

namespace HUDHealthcarePortal.Model
{
    public class ReportViewModel
    {
        public string ProjectName { get; set; }
        public string LenderName { get; set; }
        public string FhaNumber { get; set; }
        public decimal? DebtCoverageRatio { get; set; }
        public decimal? WorkingCapital { get; set; }
        public decimal? DaysCashOnHand { get; set; }
        public decimal? DaysInAcctReceivable { get; set; }
        public decimal? AvgPaymentPeriod { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public decimal? UnpaidPrincipalBalance { get; set; }
        [Display(Name="Reporting Period: ")]
        public string ReportingPeriod { get; set; }
        public HUDManagerModel HudAEModel { get; set; }
        public HUDManagerModel HudWLMModel { get; set; }


        public ReportViewModel()
        {
            HudAEModel = new HUDManagerModel();
            HudWLMModel = new HUDManagerModel();
        }

        public string HUD_Workload_Manager_Name
        {
            get { return HudWLMModel.Manager_Name; }
            set { HudWLMModel.Manager_Name = value; }
        }

        public int HUD_Workload_Manager_ID
        {
            get { return HudWLMModel.Manager_ID; }
            set { HudWLMModel.Manager_ID = value; }
        }

        public int HUD_Project_Manager_ID
        {
            get { return HudAEModel.Manager_ID; }
            set { HudAEModel.Manager_ID = value; }
        }

        public string HUD_Project_Manager_Name
        {
            get { return HudAEModel.Manager_Name; }
            set { HudAEModel.Manager_Name = value; }
        }
    }

    public class ReportModelUI
    {
        public IList<ReportViewModel> Reports { get; set; }
        public ReportModelUI ()
        {
            Reports = new List<ReportViewModel>();
        }

        
        public string ProjectReportingPeriod
        {
            get
            {
                if (Reports.Any())
                {
                    return Reports[0].ReportingPeriod;
                }
                else
                {
                    return null;
                }
            }
        }

        public string ProjectCount
        {
            get { return Reports.Count.ToString(CultureInfo.InvariantCulture); }
        }

    }
}
