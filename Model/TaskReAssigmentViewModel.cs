﻿using HUDHealthcarePortal.Core;
using System;
using Core;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;

namespace HUDHealthcarePortal.Model
{
     [Serializable]
    public class TaskReAssigmentViewModel
    {
        public int TaskReAssignId { get; set; }
        public int TaskId { get; set; }
        public string ReAssignedTo { get; set; }
        public string AssignedFrom { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
    }
}
