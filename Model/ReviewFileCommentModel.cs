﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class ReviewFileCommentModel
    {
        public Guid ReviewFileCommentId { get; set; }
        public Guid FileTaskId { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int? ReviewerProdViewId { get; set; }     
    }
}
