﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class OPAHistoryViewModel
    {
        
        public string actionTaken { get; set; }
        public string name { get; set; }
        public string userRole { get; set; }
        public string comment { get; set; }
        public DateTime? submitDate { get; set; }
        public bool isFileHistory { get; set; }
        public bool isFileComment { get; set; }
        public string taskFileId { get; set; }
        public int? fileId { get; set; }
        public int? downloadFileId { get; set; }
        public int? childFileId { get; set; }
        public string childFileName { get; set; }
        public string fileName { get; set; }
        public string fileRename { get; set; }
        public bool? isFileRenamed { get; set; }
        public string fileExt { get; set; }
        public double? fileSize { get; set; }
        public DateTime? uploadDate { get; set; }
        public bool? isReqAddInfo { get; set; }
        public bool? isApprove { get; set; }
        public int Approve { get; set; }
        public string raiComments { get; set; }
        public int? fileReviewStatusId { get; set; }
        public string fileReviewStatusName { get; set; }

        public bool editable { get; set; }
        public DateTime actionDate { get; set; }
        public string nfrComment { get; set; }

        // harish added to get doctype name 
        public string DocTypeID { get; set; }
        public string DocTypesList { get; set; }

        // harish added new columns in table and in model 24-01-2020
        public int? IsOPAUpload { get; set; }
        public int? IsRAIUpload { get; set; }
        public string ReviewFileStatusId { get; set; }
        public int? FolderKey { get; set; }
        public string rolename { get; set; }
        public int? FolderStructureKey { get; set; }
    }
}
