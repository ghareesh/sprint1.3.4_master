﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Model
{
    public class YearQuarterModel
    {
        public List<SelectListItem> ListOfYears { get; private set; }
        public List<SelectListItem> ListOfQuarters { get; private set; }
        public int SelectedYear { get; set; }
        public string SelectedQuarter { get; set; }

        public string SelectedQrtToken
        {
            // returns e.g. "2014Q3"
            get { return SelectedYear.ToString() + SelectedQuarter; }
        }

        public YearQuarterModel(int yearsToShow, string quarterToken)
        {
            var currentYear = DateTime.UtcNow.Year;
            ListOfYears = new List<SelectListItem>();
            for (var i = 0; i < yearsToShow; i++)
            {
                int year = currentYear - i;
                ListOfYears.Add(new SelectListItem() { Text = year.ToString(), Value = year.ToString() });
            }
            ListOfQuarters = new List<SelectListItem>
            {
                new SelectListItem() {Text = "Q1", Value = "Q1"},
                new SelectListItem() {Text = "Q2", Value = "Q2"},
                new SelectListItem() {Text = "Q3", Value = "Q3"},
                new SelectListItem() {Text = "Q4", Value = "Q4"}
            };
            if (string.IsNullOrEmpty(quarterToken))
            {
                // initialize selected year and quarter to current year and quarter
                var currentQuarter = GetQuarter(DateTime.UtcNow);
                SelectedYear = currentYear;
                SelectedQuarter = "Q" + currentQuarter.ToString();
            }
            else
            {
                SelectedYear = int.Parse(quarterToken.Substring(0,4));
                SelectedQuarter = quarterToken.Substring(4,2);
            }
        }

        private int GetQuarter(DateTime dt)
        {
            return (dt.Month - 1) / 3 + 1;
        }
    }
}
