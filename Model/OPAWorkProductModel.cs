﻿using HUDHealthcarePortal.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class OPAWorkProductModel
    {

        public int FileId { get; set; }
        public Guid TaskFileId { get; set; }
        [Required]
        public string FileName { get; set; }
        public FileType FileType { get; set; }
        //[Required]
        public byte[] FileData { get; set; }
        public Guid TaskInstanceId { get; set; }
        public Guid? GroupTaskInstanceId { get; set; }
        public DateTime UploadTime { get; set; }
        public string SystemFileName { get; set; }
        public string UploadedBy { get; set; }
       public string RoleName { get; set; }
       public Guid TaskId { get; set; }
       public double FileSize { get; set; }

        // #605 Siddu
        public int? FolderKey { get; set; }



    }
}
