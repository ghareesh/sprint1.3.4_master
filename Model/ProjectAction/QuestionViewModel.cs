﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model.ProjectAction
{
    [Serializable]
    public class QuestionViewModel
    {
        public Guid CheckListId { get; set; }
        public int QuestionId { get; set; }
        public string Question { get; set; }
        public int Level { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set;}
        public int DisplayOrderId { get; set; }
        public int ProjectActionId { get; set; }
        public bool IsAttachmentRequired { get; set; }
        public Guid ParentQuestionId { get; set; }

        /// <summary>
        /// this is used to check  lender uploaded a document for this question.
        /// </summary>
        public bool IsLenderUploadedDocument { get; set;}
        public string FileName { get; set;}
        public string SystemFileName { get; set; }

        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public string StartDateString
        {
            get { return StartDate.ToShortDateString(); }
        }

        public bool IsCheckboxEnabled { get; set; }
        public string Status { get; set; }


        public int? SectionId { get; set; }
        public string SectionName { get; set; }
        public string Shortnames { get; set; }
        public bool IsNARequired { get; set; }
        public bool IsNAForSection { get; set; }
        public int PageTypeId { get; set; }
    }
}
