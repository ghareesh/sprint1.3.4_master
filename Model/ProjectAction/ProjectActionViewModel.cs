﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Xml.Serialization;
using HUDHealthcarePortal.Model.ProjectAction;

namespace HUDHealthcarePortal.Model
{
    public class ProjectActionViewModel
    {
        public Guid ProjectActionFormId { get; set; }
        public int PropertyId { get; set; }
        public int LenderId { get; set; }
        [XmlIgnore]
        public string LenderName { get; set; }
        public string FhaNumber { get; set; }
        public string PropertyName { get; set;}
        public DateTime? RequestDate { get; set;}
        public DateTime? ServicerSubmissionDate{ get; set;}
        public DateTime ProjectActionDate{ get; set; }
        public int ProjectActionTypeId { get; set; }
        public int MytaskId { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string RequesterName { get; set; }

        [XmlIgnore]
        public IList<QuestionViewModel> Questions { get; set; }
        [XmlIgnore]
        public IList<String> AvailableFHANumbersList { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> ProjectActionTypeList { get; set; }

          // task related fields
        public Guid? TaskGuid { get; set; }
        public int? SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public TaskOpenStatusModel TaskOpenStatus { get; set; }
        public byte[] Concurrency { get; set; }

        public bool? IsConcurrentUserUpdateFound { get; set; }

        public int? TaskId { get; set; }
        public bool IsPAMReport { get; set; }
        public bool IsMortgagePaymentsCovered { get; set; }
        public ContactAddressViewModel ContactAddressViewModel { get; set; }
        public bool IsAgreementAccepted { get; set; }
        public bool IsApprovedPopupDisplayedForAe { get; set; }
        public ProjectActionViewModel()
        {
            AvailableFHANumbersList = new List<string>();
            ProjectActionTypeList = new List<SelectListItem>();
            ContactAddressViewModel = new ContactAddressViewModel();
        }

        public int RequestStatus { get; set; }
        public int? GroupTaskId { get; set; }
        public Guid? GroupTaskInstanceId { get; set;}
        public DateTime GroupTaskCreatedOn { get; set;}
        
        public bool IsEditMode { get; set; }
        
        public string ProjectActionName { get; set; }
        [XmlIgnore]
        public bool IsDisplayCheckoutBtn { get; set; }
        [XmlIgnore]
        public bool IsViewFromGroupTask { get; set; }
        [XmlIgnore]
        public bool IsDisplayApproveBtn { get; set; }
        [XmlIgnore]
        public bool IsLoggedInUserLender { get; set; }
        public bool? IsReassigned { get; set; }
        public string ServicerComments { get; set; }
        public string AEComments { get; set; }

        [XmlIgnore]
        public IDictionary<string, IList<QuestionViewModel>> QuestionsWithSections { get; set; }
    }
}
