﻿using System;

namespace Model
{
    public class EmailTemplateMessagesModel
    {
        public int MessageTemplateId { get; set; }
        public int EmailTypeId { get; set; }
        public string MessageSubject { get; set; }
        public string MessageBody { get; set; }
        public string KeyWords { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; } 
    }
}