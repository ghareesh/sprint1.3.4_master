﻿using HUDHealthcarePortal.Core;
using System;
using Core;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;

namespace HUDHealthcarePortal.Model
{
    [Serializable]
    public class TaskModel
    {
        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public int SequenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public System.DateTime? DueDate { get; set; }
        public System.DateTime StartTime { get; set; }      // utc time
        public System.DateTime MyStartTime { get; set; }    // user preferred time
        public string Notes { get; set; }
        public int TaskStepId { get; set; }
        public int? DataKey1 { get; set; }
        public int? DataKey2 { get; set; }
        public string DataStore1 { get; set; }
        public string DataStore2 { get; set; }
        public string ItemName { get; set; }
        public string TaskOpenStatus { get; set; }
        public string FHANumber { get; set; }
        public byte[] Concurrency { get; set; }
        public bool IsChild { get; set; }
        public string PropertyName { get; set; }
        // naresh add prodqu2
        //public bool opportunity_zone { set; get; }
        public bool Opportunityzone { get; set; }

        public string Portfolio_Name { get; set; }

        public string projectName { get; set; }


        //Addedd by naresh prodQ2 18/12/2019
        public string HasOpenedByMe
        {
            get
            {
                if (string.IsNullOrEmpty(TaskOpenStatus))
                    return "No";
                var taskOpenStatusModel = XmlHelper.Deserialize(typeof(TaskOpenStatusModel), TaskOpenStatus, Encoding.Unicode) as TaskOpenStatusModel;
                if (taskOpenStatusModel == null)
                    return "No";
                return taskOpenStatusModel.TaskOpenStatus.Exists(p => p.Key.Equals(UserPrincipal.Current.UserName)) ? "Yes" : "No";
            }
        }
       
        public int RealSeqId
        {
            get { return SequenceId + 1; }
        }

       public string TaskName { get; set; }

        public FormUploadStatus Status { get; set; }

        public DateTime WaitingStartTime { get; set; }

        public DateTime CompletedStartTime { get; set;  }

        public bool? IsReAssigned { get; set; }

        public int? PageTypeId { get; set; }
       public System.Guid ParentchildId { get; set; }
    }

    /// <summary>
    /// Production queue customization
    /// </summary>
    public class AnonymousTaskMode
    {
        public Guid TaskInstanceId { get; set; }
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public int SequenceId { get; set; }
        public int? PageTypeId { get; set; }
        public DateTime StartTime { get; set; }
        public decimal LoanAmount { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }

        public DateTime ModofiedOn { get; set; }

        public string Lender { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string DaysInQueue { get; set; }
        public string LoanType { get; set; }
        public int TaskStepId { get; set; }
        public string ShowStatus { get; set; }
        public string PropertyName { get; set; }
        //public bool opportunity_zone { get; set; }
        public bool? opportunity_zone { set; get; }
        public bool? IsLIHTC { get; set; }
        public string Portfolio_Name { get; set; }
        public string projectName { get; set; }

        //Addedd by naresh prodQ2 18/12/2019

    }
}
