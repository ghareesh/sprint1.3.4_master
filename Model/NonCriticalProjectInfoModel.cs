﻿using System;

namespace Model
{
    public class NonCriticalProjectInfoModel
    {
        public string PropertyName { get; set; }
        public int PropertyID { get; set; }
        public string FHANumber { get; set; }
        public int? LenderID { get; set; }
        public DateTime? ClosingDate { get; set; }
        public DateTime? EndingDate { get; set; }
        public decimal? IntialNCREBalance { get; set; }
        public decimal? CurrentBalance { get; set; }
        public bool? IsAmountConfirmed { get; set; }
        public bool? AmountConfirmationStatus { get; set; }
        public DateTime? ExtensionApprovalDate { get; set; }
        public decimal? SuggestedAmount { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
       
    }
}