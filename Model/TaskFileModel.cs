﻿using HUDHealthcarePortal.Core;
using System;
using Core;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;

namespace HUDHealthcarePortal.Model
{
    [Serializable]
    public class TaskFileModel
    {
        public int FileId { get; set; }
        public System.Guid TaskInstanceId { get; set; }
        public string FileName { get; set; }
        public double FileSize { get; set; }
        public FileType FileType { get; set; }
        public byte[] FileData { get; set; }
        public System.Guid TaskFileId { get; set; }
        public System.DateTime UploadTime { get; set; }
        public string SystemFileName { get; set; }
        public string GroupFileType { get; set; }
        public int? CreatedBy { get; set; }
        public string API_upload_status { get; set; }
        public int? Version { get; set; }
        public string DocId { get; set; }
        public string DocTypeID { get; set; }
        public string RoleName { get; set; }
        // harish added new method to save foldername in db 31-01-2020
        public string FolderName { get; set; }
        // harish added new method to save folderkey in db 18-02-2020
        public int? FolderStructureKey { get; set; }
       
        //Added by siddu #841 @21042020
        public bool IsProdQueuQueue { get; set; }
        //Added by siddu #841 @21042020
        public bool IsProdQueuInprocess { get; set; }
    }
}
