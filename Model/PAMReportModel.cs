﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Core;
using Model;
using System.Web.Mvc;
using System.Linq;
using System.Collections;
using System.Xml.Serialization;
using Model.Production;

namespace HUDHealthcarePortal.Model
{
    public class PAMReportModel : ReportModel
    {
        public PAMReportModel(ReportType reportType)
            : base(reportType)
        {
            ReportType = reportType;

            //karri:D#610
            PropNames = new List<SelectListItem>();
            FHANumbersList = new List<SelectListItem>();
            AssignedToList = new List<SelectListItem>();
            ProjActionDaysToCompList = new List<SelectListItem>();
            HasOpenedList = new List<SelectListItem>();
        }

        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public bool IsDateDefault { get; set; }
        public ReportType ReportType { get; set; }
        public ReportLevel ReportLevel { get; set; }
        public IList<PAMReportViewModel> PAMReportGridlList { get; set; }
        public PaginateSortModel<PAMReportViewModel> PAMReportGridModel { get; set; }
        public MultiSelectList AEListItems { get; set; }
        public MultiSelectList ISOUListItems { get; set; }
        public MultiSelectList WLMListItems { get; set; }
        public MultiSelectList Status { get; set; }
        public MultiSelectList ProjectAction { get; set; }
        // User Story 1904 - begin
        public MultiSelectList LAMListItems { get; set; }
        public MultiSelectList BAMListItems { get; set; }
        public MultiSelectList LARListItems { get; set; }
        public MultiSelectList RoleListItems { get; set; }
        public string LenderName { get; set; }
        public string LenderUserFullName { get; set; }

        //karri:D#610
        public string FHANumber { get; set; }
        public string PropertyName { get; set; }
        public int IsProjectActionOpen { get; set; }
        public string ReAssignedTo { get; set; }
        public int ProjectActionDaysToComplete { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> PropNames { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> FHANumbersList { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> AssignedToList { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> HasOpenedList { get; set; }
        [XmlIgnore]
        public IList<SelectListItem> ProjActionDaysToCompList { get; set; }

        /// <summary>
        /// selected serach criteria LAM names list
        /// </summary>
        public IList<string> SelectedLAMNameList { get; set; }
        /// <summary>
        /// selected serach criteria BAM names list
        /// </summary>
        public IList<string> SelectedBAMNameList { get; set; }
        /// <summary>
        /// selected serach criteria LAR names list
        /// </summary>
        public IList<string> SelectedLARNameList { get; set; }
        public IList<string> SelectedProjectActionList { get; set; }
        public IList<string> SelectedRolesList { get; set; }
        /// <summary>
        /// selected serach criteria Roles names list
        /// </summary>
        public string SelectedRolesNameList { get; set; }
        // User story 1904 - end

        public PamProjectActionTypeViewModel PamProjectActionTypes { get; set; }
        /// <summary>
        /// selected serach criteria AE names list
        /// </summary>
        public IList<string> SelectedAENameList { get; set; }

        /// <summary>
        /// selected serach criteria ISOU names list
        /// </summary>
        public IList<string> SelectedISOUNameList { get; set; }

        /// <summary>
        /// Slected Workload managers names list
        public IList<string> SelectedWLMNameList { get; set; }
        //naresh-new 28012020
        public HUDAssetpamreportfiltermodel AsstMgmtRptFilters { get; set; }
        /// <summary>
        /// get selected Status  Name,  as of now used in Excel export
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// get selected ProjectAction  Name,  as of now used in Excel export
        /// </summary>
        public string ProjectActionName { get; set; }

        public string ReportViewName { get; set; }


        public int NoOfOpenRequests
        {
            get
            {
                if (this.PAMReportGridlList != null)
                {
                    return this.PAMReportGridlList.Count(r => r.IsProjectActionOpen == true);
                }
                return 0;
            }
        }
        public int NoOfClosedRequests
        {
            get
            {
                if (this.PAMReportGridlList != null)
                {
                    return this.PAMReportGridlList.Count(r => r.IsProjectActionOpen == false);
                }
                return 0;
            }
        }
        /// <summary>
        /// Gets or sets the mean. calculate only for closed resquests
        /// </summary>
        /// <value>
        /// The mean.
        /// </value>
        public decimal Mean
        {
            get
            {
                var closedRequestCount = this.PAMReportGridlList.Count(r => r.IsProjectActionOpen == false && r.ProjectActionName != Core.ProjectActionName.NCRExtension.ToString());

                if (this.PAMReportGridlList != null && this.PAMReportGridlList.Any(r => r.ProjectActionName != Core.ProjectActionName.NCRExtension.ToString()) && closedRequestCount > 0)
                {
                    return Math.Round(((decimal)TotalRequestCompletionTime(this.PAMReportGridlList.Where(r => r.IsProjectActionOpen == false && r.ProjectActionName != Core.ProjectActionName.NCRExtension.ToString()).ToList()) / (decimal)closedRequestCount), 2);
                }
                return 0;
            }
        }
        /// <summary>
        /// medain Calculated on both open and closed requests
        /// </summary>
        /// <value>
        /// The median.
        /// </value>
        public int Median
        {
            get
            {
                if (this.PAMReportGridlList != null && this.PAMReportGridlList.Any(r => r.ProjectActionName != Core.ProjectActionName.NCRExtension.ToString()))
                {
                    return GetMedianOnAvailableRequests(this.PAMReportGridlList.Where(r => r.ProjectActionName != Core.ProjectActionName.NCRExtension.ToString()).ToList());
                }
                return 0;
            }
        }
        /// <summary>
        /// Gets the average completion time. amount of total number of days open and close
        /// </summary>
        /// <value>
        /// The average completion time.
        /// </value>
        public decimal AvgCompletionTime
        {
            get
            {
                if (this.PAMReportGridlList != null && this.PAMReportGridlList.Any(r => r.ProjectActionName != Core.ProjectActionName.NCRExtension.ToString()))
                {
                    return Math.Round(TotalRequestCompletionTime(this.PAMReportGridlList.Where(r => r.ProjectActionName != Core.ProjectActionName.NCRExtension.ToString()).ToList()) / Convert.ToDecimal(this.PAMReportGridlList.Count(r => r.ProjectActionName != Core.ProjectActionName.NCRExtension.ToString())), 2);
                }
                return 0;
            }
        }

        /// <summary>
        /// Totals the request completion time.
        /// </summary>
        /// <returns></returns>
        public int TotalRequestCompletionTime(IList<PAMReportViewModel> gridList)
        {
            int sum = 0;
            gridList.ToList().ForEach(r => {
                if (r.IsProjectActionOpen)
                {
                    //sum = sum + (int)(DateTime.Now.Date - r.ProjectActionSubmitDate.Date).TotalDays;
                    sum = sum + (int)(DateTime.UtcNow.Date - r.ProjectActionSubmitDate.Date).TotalDays;
                }
                else
                {
                    sum = sum + (r.ProjectActionCompletionDate == DateTime.MinValue ? 0 : (int)(r.ProjectActionCompletionDate.Date - r.ProjectActionSubmitDate.Date).TotalDays);
                }
            });

            return sum;

        }

        /// <summary>
        /// Totals the request completion time.
        /// </summary>
        /// <returns></returns>
        public int GetMedianOnAvailableRequests(IList<PAMReportViewModel> gridList)
        {
            int[] medainArray = new int[gridList.Count()];
            int i = 0;
            gridList.ToList().ForEach(r => {
                if (r.IsProjectActionOpen)
                {
                    //medainArray[i] = (int)(DateTime.Now.Date - r.ProjectActionSubmitDate.Date).TotalDays;
                    medainArray[i] = (int)(DateTime.UtcNow.Date - r.ProjectActionSubmitDate.Date).TotalDays;
                }
                else
                {
                    medainArray[i] = r.ProjectActionCompletionDate == DateTime.MinValue ? 0 : (int)(r.ProjectActionCompletionDate.Date - r.ProjectActionSubmitDate.Date).TotalDays;
                }
                i++;
            });

            if (i > 0)
            {
                Array.Sort(medainArray);
                int size = medainArray.Length;
                int mid = size / 2;
                decimal median = (size % 2 != 0) ? (decimal)medainArray[mid] : ((decimal)medainArray[mid] + (decimal)medainArray[mid - 1]) / 2;
                return (int)Math.Ceiling(median);
            }

            return 0;
        }

    }

}
