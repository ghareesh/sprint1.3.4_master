﻿namespace HUDHealthcarePortal.Model
{
	/// <summary>
	/// Encapsulates HUDSignature info of a HUD user
	/// </summary>
	public class HUDSignatureModel
	{
		public int? Id { get; set; }
		public int UserId { get; set; }
		public byte[] Signature { get; set; }
		
	}
}

