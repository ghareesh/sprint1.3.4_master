﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ChildTaskNewFileModel
    {
        
            public Guid ChildTaskNewFileId { get; set; }
            public Guid ChildTaskInstanceId { get; set; }
            public Guid TaskFileInstanceId { get; set; }
        
    }
}
