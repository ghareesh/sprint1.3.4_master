﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
	public class SpecialConditionTypeModel
	{
		public int SpecialConditionTypeId { get; set; }
		public string SpecialCondition { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}
