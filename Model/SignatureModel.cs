﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model
{
	public class SignatureModel
	{
		[UIHint("SignaturePad")]
		public byte[] MySignature { get; set; }

		[UIHint("SignaturePad")]
		public byte[] CurrentSignature { get; set; }

		[Display(Name = "Do you need to update signature?")]
		public bool IsNewSignatureRequired { get; set; }
		public string AgentPictureLocation { get; set; }
		public string AgentSignatureFileName { get; set; }
		public int CreatedBy { get; set; }
		public int ModifiedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }
	}
}

