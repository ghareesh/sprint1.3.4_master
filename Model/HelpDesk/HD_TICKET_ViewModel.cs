﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Xml.Serialization;
using HUDHealthcarePortal.Model.ProjectAction;
using HUDHealthcarePortal.Model;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model;

//karri:#149, #214
namespace HUDHealthcarePortal.Model
{
    public class HD_TICKET_ViewModel
    {
        [Key]
        public int TICKET_ID { get; set; }

        public int? STATUS { get; set; }//open or closed etc
        public int? PREVIOUS_STATUS { get; set; }
        //[StringLength(255)]//title
        public string ISSUE { get; set; }//MAPS TO TITLE ON THE UI
        //[Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }

        public int? CATEGORY { get; set; }//type of issue
        public string USERCOMMENTS { get; set; }

        public int? SUBCATEGORY { get; set; }//type of issue

        public int? PRIORITY { get; set; }
        public int? PREVIOUS_PRIORITY { get; set; }

        //[Column(TypeName = "text")]
        public string RECREATION_STEPS { get; set; }
        //[Column(TypeName = "text")]
        public string CONVERSATION { get; set; }
        public int? CREATED_BY { get; set; }
        public DateTime CREATED_ON { get; set; }

        public int? ASSIGNED_TO { get; set; }
        public int? PREVIOUS_ASSIGNED_TO { get; set; }

        public DateTime? ASSIGNED_DATE { get; set; }
        public DateTime? PREVIOUS_ASSIGNMENTDATE { get; set; }

        public int? LAST_MODIFIED_BY { get; set; }
        public DateTime? LAST_MODIFIED_ON { get; set; }

        public int? ONACTION { get; set; }//hdnew
        public string ROLENAME { get; set; }

        public string CREATED_BY_USERNAME { get; set; }//emailid
        public string ASSIGNED_TO_USERNAME { get; set; }//userid, email

        public string CREATED_BY_USERFIRSTNAME { get; set; }
        public string CREATED_BY_USERLASTNAME { get; set; }
        public string CREATED_BY_USERDISPLAYNAME { get; set; }

        public string ASSIGNED_TO_USERFIRSTNAME { get; set; }
        public string ASSIGNED_TO_USERLASTNAME { get; set; }
        public string ASSIGNED_TO_USERDISPLAYNAME { get; set; }

        public string LAST_MODIFIED_BY_USERNAME { get; set; }
        public bool IS_HELPDESK_USER { get; set; }
        public string HELPDESK_UPLOADS_PATH { get; set; }
        //we fill attachment files data here
        public string File1 { get; set; }
        public string File2 { get; set; }
        public string File3 { get; set; }
        public string File4 { get; set; }
        public string File5 { get; set; }
        public string File6 { get; set; }

        public string MAILNOTIFICATION_STATUS { get; set; } //open/close/rejected etc
        public string MAILNOTIFICATION_ISSUECATEGORY { get; set; } //DATA ISSUE, TECHINICAL ISSUE ETC
        public int MAILNOTIFICATION_TICKETMODE { get; set; }//CREATE/UPDATE-1/2
        public string MAILNOTIFICATION_NUMBEROFFILESATTACHED { get; set; }
        public int? TEMPERORY_FILE_UNIQUEPATH { get; set; }//TO HOLD THE A RANDOMLY GENERATED UNIQUID REFERRED FOR FILES STORAGE TEMPORORILY

        public string MAILNOTIFICATION_TOEMAIL { get; set; }//HUDHELPDESK
        public string MAILNOTIFICATION_CCEMAIL { get; set; }//HUD ADMIN OR ANY
        public string MAILNOTIFICATION_REQUESTOREMAIL { get; set; }//WHO LOGGED IN, ASSIGNED TO
        public string MAILNOTIFICATION_FROMBASEEMAIL { get; set; }//BASE MONITORING FROM EMAIL ADDRESS
        public string MAILNOTIFICATION_SUBJECT { get; set; }

        //29082019
        public string CATEGORY_TEXT { get; set; }
        public string SUBCATEGORY_TEXT { get; set; }

        public int Closed { get; set; }

        public int Fixed { get; set; }

        public int Open { get; set; }

        public int PSLResponse { get; set; }

        public int Recheck { get; set; }

        public int RFI { get; set; }

        public int GrandTotal { get; set; }

        public DateTime? REPORT_START_DATE { get; set; }

        public DateTime? REPORT_END_DATE { get; set; }

        public int Numberofdays { get; set; }

        public int Numberofweeks { get; set; }

        //hudenhancements3
        public string FHANumber { get; set; }
        public string PropertyName { get; set; }
        public int? PropertyId { get; set; }
        public string OPENEDDAYS { get; set; }
        public string EXTERNALMAILID { get; set; }
        public bool ATTACHFILES_EXTERNALMAILID { get; set; }
        public string ONACTION_TEXT { get; set; }
        public string STATUS_TEXT { get; set; }
        public string ASSIGNED_DATE_TEXT { get; set; }

        [XmlIgnore]
        public IList<SelectListItem> TicketCategoryList { get; set; }

        [XmlIgnore]
        public IList<SelectListItem> TicketStatusList { get; set; }

        [XmlIgnore]
        public IList<SelectListItem> TicketAsigneeList { get; set; }

        [XmlIgnore]
        public IList<SelectListItem> TicketSubCategoryList { get; set; }

        [XmlIgnore]
        public IList<SelectListItem> TicketPriorityList { get; set; }

        [XmlIgnore]
        public IList<SelectListItem> TicketOnActionList { get; set; }

        [XmlIgnore]
        public IList<SelectListItem> TicketUsernameList { get; set; }
        //Added by siddesh 11102019
        [XmlIgnore]
        public IList<SelectListItem> TicketHelpdesknameList { get; set; }
        [XmlIgnore]
        public IList<String> AvailableFHANumbersList { get; set; }

        public HD_TICKET_ViewModel()
        {
            TicketCategoryList = new List<SelectListItem>();
            TicketSubCategoryList = new List<SelectListItem>();
            TicketStatusList = new List<SelectListItem>();
            TicketAsigneeList = new List<SelectListItem>();
            TicketPriorityList = new List<SelectListItem>();
            TicketOnActionList = new List<SelectListItem>();
            TicketUsernameList = new List<SelectListItem>();
            TicketHelpdesknameList = new List<SelectListItem>();
        }
    }
}