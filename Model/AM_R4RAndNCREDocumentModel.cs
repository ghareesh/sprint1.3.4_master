﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AM_R4RAndNCREDocumentModel
    {
        public int DocumentId { get; set; }

        public string FileName { get; set; }
        public string ActionName { get; set; }
        public int TransactionDocumentId { get; set; }
        public string DocumentDescription { get; set; }
    }
   
   
}
