﻿using System;
using System.Collections.Generic;
using SpreadsheetGear;

namespace HUDHealthcarePortal.Model
{
    public class AdhocReportExcelModel
    {
        public List<AdhocReportColumnInfo> GetAdhocReportColumns(AdhocReportModel adhocReportModel)
        {
            return (new List<AdhocReportColumnInfo>()
            {
                new AdhocReportColumnInfo()
                {
                    ColumnName = "FHA Number",Selected = true,ColumnWidth = 10,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.FHA_Number
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "SOA Code",Selected = true,ColumnWidth = 5,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.SOA_Code
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Property Name",Selected = true,ColumnWidth = 45,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Property_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Property Id",Selected = true,ColumnWidth = 10,NumberFormat = "@",
                    Data = adhocReportModel == null ? 0 : adhocReportModel.Property_Id
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Servicing Mortgagee Name",Selected = true,ColumnWidth = 45,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Servicing_Mortgagee_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Servicer ID",Selected = true, ColumnWidth = 10, NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Servicer_ID
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Amortized Unpaid Principal Bal",Selected = false,ColumnWidth = 20,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? 0 : adhocReportModel.Amortized_Unpaid_Principal_Bal
                },
                
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Account Executive Name",Selected = true,ColumnWidth = 25,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Account_Executive_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Account Executive Email",Selected = true,ColumnWidth = 30,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Account_Executive_eMail
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Account Executive Phone",Selected = false,ColumnWidth = 25,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Account_Executive_Primary_Phone
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Date Uploaded",Selected = true,ColumnWidth = 15,NumberFormat = "mm/dd/yyyy",
                    Data = adhocReportModel == null ? new DateTime() : adhocReportModel.DateInserted
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Is Active DEC Case Ind",Selected = true,ColumnWidth = 10,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Is_Active_Dec_Case_Ind
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Lender ID",Selected = false, ColumnWidth = 10, NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.LenderID
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Lender Name",Selected = false,ColumnWidth = 45,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Lender_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Lender Email",Selected = false,ColumnWidth = 30,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Lender_eMail
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Lender Phone",Selected = false,ColumnWidth = 25,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Lender_Primary_Phone
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Organization Name",Selected = false,ColumnWidth = 55,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Organization_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Email",Selected = false,ColumnWidth = 35,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_eMail
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Phone",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Primary_Phone
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Address Line1",Selected = false,ColumnWidth = 30,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Address_Line1
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Address Line2",Selected = false,ColumnWidth = 20,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Address_Line2
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner City",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_City
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner State",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_State
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Zip",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : 
                           adhocReportModel.Owner_Zip_4==null ? adhocReportModel.Owner_Zip : adhocReportModel.Owner_Zip+"-"+adhocReportModel.Owner_Zip_4
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact Name",Selected = false,ColumnWidth = 25,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Contact_Indv_Fullname
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact Title",Selected = false,ColumnWidth = 45,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Contact_Indv_Title
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact Email",Selected = false,ColumnWidth = 35,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Contact_eMail
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact Phone",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Contact_Primary_Phone
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact Address Line1",Selected = false,ColumnWidth = 30,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Contact_Address_Line1
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact Address Line2",Selected = false,ColumnWidth = 20,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Contact_Address_Line2
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact City",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Contact_City
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact State",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Owner_Contact_State
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Contact Zip",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : 
                           adhocReportModel.Owner_Contact_Zip_4==null ? adhocReportModel.Owner_Contact_Zip : 
                               adhocReportModel.Owner_Contact_Zip+"-"+adhocReportModel.Owner_Contact_Zip_4
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Agent Org Name",Selected = false,ColumnWidth = 55,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Mgmt_Agent_org_name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Agent Email",Selected = false,ColumnWidth = 35,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Agent_eMail
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Agent Phone",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Agent_Primary_Phone
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Agent Address Line1",Selected = false,ColumnWidth = 30,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Agent_Address_Line1
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Agent Address Line2",Selected = false,ColumnWidth = 20,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Agent_Address_Line2
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Agent City",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Agent_City
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Agent State",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Agent_State
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Agent Zip",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : 
                           adhocReportModel.Management_Agent_Zip_4==null ? adhocReportModel.Management_Agent_Zip : 
                               adhocReportModel.Management_Agent_Zip+"-"+adhocReportModel.Management_Agent_Zip_4
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact Name",Selected = false,ColumnWidth = 25,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Mgmt_Contact_FullName
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact Title",Selected = false,ColumnWidth = 45,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Mgmt_Contact_Indv_Title
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact Email",Selected = false,ColumnWidth = 35,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Contact_eMail
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact Phone",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Contact_Primary_Phone
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact Address Line1",Selected = false,ColumnWidth = 30,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Contact_Address_Line1
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact Address Line2",Selected = false,ColumnWidth = 20,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Contact_Address_Line2
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact City",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Contact_City
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact State",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Management_Contact_State
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Management Contact Zip",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : 
                           adhocReportModel.Management_Contact_Zip_4==null ? adhocReportModel.Management_Contact_Zip : 
                               adhocReportModel.Management_Contact_Zip+"-"+adhocReportModel.Management_Contact_Zip_4
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Portfolio Name",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Portfolio_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Portfolio Number",Selected = false,ColumnWidth = 10,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Portfolio_Number
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Project Name",Selected = false,ColumnWidth = 45,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Project_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Servicing Mortgagee Organization",Selected = false,ColumnWidth = 25,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Servicer_Organization
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Servicing Mortgagee Address_Line1",Selected = false,ColumnWidth = 30,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Servicer_Address_Line1
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Servicing Mortgagee Address_Line2",Selected = false,ColumnWidth = 20,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Servicer_Address_Line2
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Servicing Mortgagee City",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Servicer_City
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Servicing Mortgagee Zip",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : 
                           adhocReportModel.Servicer_Zip_4==null ? adhocReportModel.Servicer_Zip : adhocReportModel.Servicer_Zip+"-"+adhocReportModel.Servicer_Zip_4
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "SOA Numeric Name",Selected = true,ColumnWidth = 20,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Soa_Numeric_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Troubled Code",Selected = true,ColumnWidth = 10,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.Troubled_Code
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "WorkLoad Manager Name",Selected = true,ColumnWidth = 25,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.WorkLoad_Manager_Name
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "WorkLoad Manager Email",Selected = true,ColumnWidth = 30,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.WorkLoad_Manager_eMail
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "WorkLoad Manager Phone",Selected = false,ColumnWidth = 25,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.WorkLoad_Manager_Primary_Phone
                },
                
                
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Owner Operator",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.OperatorOwner
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Period Ending Date",Selected = false,ColumnWidth = 15,NumberFormat = "mm/dd/yyyy",
                    Data = adhocReportModel == null ? new DateTime() : adhocReportModel.PeriodEnding
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Months in Period",Selected = false,ColumnWidth = 10,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.MonthsInPeriod
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Financial Statment Type",Selected = false,ColumnWidth = 15,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.FinancialStatementType
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Units In Facility",Selected = false,ColumnWidth = 10,NumberFormat = "@",
                    Data = adhocReportModel == null ? null : adhocReportModel.UnitsInFacility
                },

                new AdhocReportColumnInfo()
                {
                    ColumnName = "Accounts Receivable",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.AccountsReceivable
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Amortization Expense",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.AmortizationExpense
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Current Assets",Selected = false,ColumnWidth = 12,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.CurrentAssets
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Current Liabilities",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.CurrentLiabilities
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Depreciation Expense",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.DepreciationExpense
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "FHA Insured Interest Payment",Selected = false,ColumnWidth = 16,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.FHAInsuredInterestPayment
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "FHA Insured Principal Payment",Selected = false,ColumnWidth = 17,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.FHAInsuredPrincipalPayment
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Investments",Selected = false,ColumnWidth = 10,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.Investments
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Mortgage Insurance Premium",Selected = false,ColumnWidth = 18,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.MortgageInsurancePremium
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Net Income",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.NetIncome
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Operating Cash",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.OperatingCash
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Rent Lease Expense",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.RentLeaseExpense
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Reserve For Replacement Deposit",Selected = false,ColumnWidth = 20,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.ReserveForReplacementDeposit
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Reserve For Replacement Escrow Balance",Selected = false,ColumnWidth = 25,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.ReserveForReplacementEscrowBalance
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Total Expenses",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.TotalExpenses
                },
                new AdhocReportColumnInfo()
                {
                    ColumnName = "Total Revenues",Selected = false,ColumnWidth = 11,NumberFormat = "#,###0.00",HorizontalAlignment = HAlign.Right,
                    Data = adhocReportModel == null ? null : adhocReportModel.TotalRevenues
                }
            });
        }
    }

    public class AdhocReportColumnInfo
    {
        public string ColumnName { get; set; }
        public bool Selected { get; set; }
        public double ColumnWidth { get; set; }
        public string NumberFormat { get; set; }
        public HAlign HorizontalAlignment { get; set; }
        public object Data { get; set; }
    }
}
