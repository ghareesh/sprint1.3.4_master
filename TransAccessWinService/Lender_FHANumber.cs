//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TransAccessWinService
{
    using System;
    using System.Collections.Generic;
    
    public partial class Lender_FHANumber
    {
        public int LenderID { get; set; }
        public string FHANumber { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<int> OnBehalfOfBy { get; set; }
        public Nullable<System.DateTime> FHA_StartDate { get; set; }
        public Nullable<System.DateTime> FHA_EndDate { get; set; }
    }
}
